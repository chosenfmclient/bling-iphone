#!/bin/bash
echo Enter a version number - leave blank to keep current version: `python3 find-version.py`
read version
if [ -z "$version" ]; then
    version=`python3 find-version.py`
else
    python3 version.py -v "$version"
fi

echo Building version $version

date=`date +%Y-%m-%d__%H_%M_%S`
path="$HOME/Desktop/Bling App Versions/$version $date"
echo "Outputting to: $path"
mkdir -p  "$path"
xcodebuild -workspace "BlingIphoneApp/BlingIphoneApp.xcworkspace" -scheme BlingIphoneApp -configuration Release clean archive -archivePath  "BlingIphoneApp/build/Release-iphoneos/BlingIphoneApp.xcarchive" DEVELOPMENT_TEAM="E4PK242MK6" > "$path/archive-log.txt"
if [ $? -eq 0 ]; then
    echo Archive created

    xcodebuild -exportArchive -archivePath "BlingIphoneApp/build/Release-iphoneos/BlingIphoneApp.xcarchive" -exportPath "$path" -exportOptionsPlist exportOptions.plist > "$path/xport-log.txt"

    if [ $? -eq 0 ]; then
        cp BlingIphoneApp.plist "$path/"
        echo $version build successful
        terminal-notifier -message "BUILD $version SUCCEEDED"
    else
        echo $version build failed
        terminal-notifier -message "BUILD $version FAILED OH NO"
    fi

else
    echo Archive failed
    terminal-notifier -message "ARCHIVE $version FAILED OH NO"
    exit 1
fi
