#!/usr/bin/env python3
import fileinput
import re
import sys, getopt

version = "0.2.2.0.5s"

def main(argv):
    version = ''
    if len(sys.argv) < 2:
        print('You must supply a version argument')
        print('usage: version.py -v "version number"')
        sys.exit(2)

    try:
        opts, args = getopt.getopt(argv,"v:",["version="])
    except getopt.GetoptError:
        print('usage: version.py -v "version number"')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-v', "--version"):
            version = arg

    if not version:
        print('usage: version.py -v "version number"')
        sys.exit(2)


    if version.endswith('d'):
        env = 'dev'
    elif version.endswith('q'):
        env = 'test'
    elif version.endswith('s'):
        env = 'stage'
    else:
        env = 'prod'

    print('Changing version to:', version, '--', env)
    env_names = ['BLINGY_DEV_ENV', 'BLINGY_TEST_ENV', 'BLINGY_STAGE_ENV', 'BLINGY_PROD_ENV']
    env_vars = {
                'dev': {'env_name': env_names[0], 'fb_app_id': '330208183988944'},
                'test': {'env_name': env_names[1], 'fb_app_id': '840285579442154'},
                'stage': {'env_name': env_names[2], 'fb_app_id': '328089844192445'},
                'prod': {'env_name': env_names[3], 'fb_app_id': '187223965041408'},
            }

    fbIdKey = "<key>FacebookAppID</key>"
    newFbAppId = "<string>{0}</string>".format(env_vars[env]['fb_app_id'])

    fbNameKey = "<key>FacebookDisplayName</key>"

    versionNumberKey = "<key>CFBundleVersion</key>"
    versionNumberKey2 = "<key>CFBundleShortVersionString</key>"

    fbUrlSchemePattern = re.compile(r'<string>fb[0-9]+')

    with fileinput.FileInput('BlingIphoneApp.plist', inplace=True) as file:
        versionKey = False
        for line in file:
            if versionKey:
                print('\t\t\t\t<string>' + version + '</string>\n', end='')
                versionKey = False
                continue
            if line.strip() == '<key>bundle-version</key>':
                versionKey = True
            print(line, end='')


    with fileinput.FileInput('BlingIphoneApp/BlingIphoneApp/BlingIphoneApp-Prefix.pch', inplace=True) as file:
        replace = True
        for line in file:
            lineOutPut = False
            if replace:
                if line.strip() == "#ifdef BLINGY_DEV_ENV":
                   replace = False
                   print(line, end='')
                   continue
                for name in env_names:
                    if line.strip().find(name) > -1 and not line.strip().startswith('//') and line.find(env_vars[env]['env_name']) < 0:
                        if not line.strip().startswith(env_vars[env]['env_name']):
                            print(" //" + line, end='')
                            lineOutPut = True

                if line.find(env_vars[env]['env_name']) > -1 and line.strip().startswith('//'):
                    print(' #define ' + env_vars[env]['env_name'] + '\t//' + env_vars[env]['fb_app_id'], end='\n')
                    lineOutPut = True
                    continue

            if not lineOutPut:
                print(line, end='')


    with fileinput.FileInput('BlingIphoneApp/BlingIphoneApp/Info.plist', inplace=True) as file:
        fbId = False
        versionNumber = False
        fbName = False
        for line in file:
            replaced = False
            if not replaced and line.strip() == versionNumberKey or line.strip() == versionNumberKey2:
                versionNumber = True
                print(line, end='')
                continue

            if not replaced and versionNumber:
                print("\t<string>{0}</string>".format(version), end='\n')
                versionNumber = False
                replaced = True
                continue

            if not replaced and fbId:
                print('\t' + newFbAppId, end='\n')
                replaced = True
                fbId = False
                continue

            if not replaced and line.strip() == fbIdKey:
                fbId = True
                print (line, end='')
                continue

            if not replaced and fbUrlSchemePattern.search(line) is not None:
                print("\t\t\t<string>fb{0}</string>".format(env_vars[env]['fb_app_id']), end='\n')
                replaced = True
                continue

            if not replaced and line.strip() == fbNameKey:
                fbName = True
                print(line, end='')
                continue

            if not replaced and fbName:
                if env == 'prod':
                    print("\t<string>Blingy</string>")
                else:
                    print("\t<string>Blingy [{0}]</string>".format(env[0].upper() + env[1:]), end='\n')
                replaced = True
                fbName = False
                continue

            print(line, end='')

if __name__ == "__main__":
   main(sys.argv[1:])
