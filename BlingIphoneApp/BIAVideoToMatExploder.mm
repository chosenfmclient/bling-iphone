//
//  BIAVideoToMatExploder.mm
//  
//
//  Created by Michael Biehl on 11/13/16.
//
//

#import "BIAVideoToMatExploder.h"



@interface BIAVideoToMatExploder ()
{
    std::map<Float64, cv::Mat> videoMap;
}

@property (strong, nonatomic) AVAssetReader *reader;
@property (strong, nonatomic) AVAssetReaderTrackOutput *readerOutput;
@property (strong, nonatomic) NSMutableDictionary *framesDict;

@end

@implementation BIAVideoToMatExploder

- (id)initWithAsset:(AVAsset *)asset {
    if (self = [super init]) {
        
        self.reader = [AVAssetReader assetReaderWithAsset:asset
                                                    error:nil];
        self.readerOutput = [[AVAssetReaderTrackOutput alloc] initWithTrack:[asset tracksWithMediaType:AVMediaTypeVideo].firstObject
                                                             outputSettings:@{(id)kCVPixelBufferPixelFormatTypeKey : [NSNumber numberWithInt:kCVPixelFormatType_32BGRA]}];
        self.videoSize = CGSizeZero;
        [self.reader addOutput:self.readerOutput];
        self.framesDict = [NSMutableDictionary dictionary];
    }
    return self;
}

- (cv::Mat)getFrameAtTime:(CMTime)time {
    
    if (CMTimeGetSeconds(time) == 0) {
        cv::Mat image = videoMap[0];
        return image;
    }
    
    return cv::Mat();
}

- (void)explodeVideoWithCompletion:(void (^)(BOOL completed))handler {
    
    if ([self.reader startReading]) {
        
        BOOL continueReading = YES;
        while (continueReading) {
            std::pair<Float64, cv::Mat> frame = [self readNextFrame];
            if (frame.first == -1) {
                continueReading = NO;
            }
            else {
                videoMap.insert(frame);
            }
        }
        
        handler(self.reader.status == AVAssetReaderStatusCompleted);
        
    }
    else {
        NSLog(@"VideoExploder: Start Reading Failed!!!");
    }
}

- (std::pair<Float64, cv::Mat>)readNextFrame {
    
    CMSampleBufferRef sampleBuffer = [self.readerOutput copyNextSampleBuffer];
    
    
    if (sampleBuffer == NULL) {
        return std::pair<Float64, cv::Mat>(-1, cv::Mat());
    }
    
    CMTime pst = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
    CVImageBufferRef imageBuffer = NULL;
    imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    CVPixelBufferLockBaseAddress(imageBuffer, 0);

    CIImage *ciImage = [CIImage imageWithCVImageBuffer:imageBuffer];

    
    if (imageBuffer == NULL) return std::pair<Float64, cv::Mat>(-1, cv::Mat());
    
    if (CGSizeEqualToSize(self.videoSize, CGSizeZero)) {
        self.videoSize = CGSizeMake(CVPixelBufferGetWidth(imageBuffer),
                                    CVPixelBufferGetHeight(imageBuffer));
    }
    
    cv::Mat image = [self cvMatFromImageBuffer:imageBuffer];
    //UIImage *uiIMage = [self UIImageFromCVMat:image];
    Float64 time = CMTimeGetSeconds(pst);
    //CFRelease(imageBuffer);
    if (time == 0) {
//        .copyTo(image);
        //UIImage *uiImage = [self UIImageFromCVMat:image];
        NSLog(@"we gots an image");
    }
    
    
    return std::pair<Float64, cv::Mat>(time, image);
}

- (cv::Mat)cvMatFromImageBuffer:(CVImageBufferRef)imageBuffer {
    
    void* bufferAddress;
    size_t width;
    size_t height;
    size_t bytesPerRow;
    
    int format_opencv;
    
    OSType format = CVPixelBufferGetPixelFormatType(imageBuffer);
    if (format == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) {
        
        format_opencv = CV_8UC1;
        
        bufferAddress = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
        width = CVPixelBufferGetWidthOfPlane(imageBuffer, 0);
        height = CVPixelBufferGetHeightOfPlane(imageBuffer, 0);
        bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0);
        
    } else { // expect kCVPixelFormatType_32BGRA
        
        format_opencv = CV_8UC4;
        
        bufferAddress = CVPixelBufferGetBaseAddress(imageBuffer);
        width = CVPixelBufferGetWidth(imageBuffer);
        height = CVPixelBufferGetHeight(imageBuffer);
        bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    }
    
    cv::Mat image((int)height, (int)width, format_opencv, bufferAddress, bytesPerRow);
    
    cvtColor(image, image, CV_BGRA2RGBA);

//    UIImage *uiImage = [self UIImageFromCVMat:image];
//    NSLog(@"IMAGE TIME");
//    uiImage = nil;
    
    return image;
}

-(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat {
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    
    CGColorSpaceRef colorSpace;
    CGBitmapInfo bitmapInfo;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
        bitmapInfo = kCGImageAlphaNone | kCGBitmapByteOrderDefault;
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
        bitmapInfo = kCGBitmapByteOrder32Little | (
                                                   cvMat.elemSize() == 3? kCGImageAlphaNone : kCGImageAlphaNoneSkipLast
                                                   );
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(
                                        cvMat.cols,                 //width
                                        cvMat.rows,                 //height
                                        8,                          //bits per component
                                        8 * cvMat.elemSize(),       //bits per pixel
                                        cvMat.step[0],              //bytesPerRow
                                        colorSpace,                 //colorspace
                                        bitmapInfo,                 // bitmap info
                                        provider,                   //CGDataProviderRef
                                        NULL,                       //decode
                                        false,                      //should interpolate
                                        kCGRenderingIntentDefault   //intent
                                        );
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return finalImage;
}

@end
