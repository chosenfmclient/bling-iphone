//
//  LeaderboardDataModel.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 2/27/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

import UIKit
import RestKit

@objc class LeaderboardDataModel: NSObject {

    var userList: [LeaderboardUser]?
    
    static func objectMapping() -> RKObjectMapping {
        
        let mapping = RKObjectMapping(for: LeaderboardDataModel.self)
        
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "data",
                                                          toKeyPath: "userList",
                                                          with: LeaderboardUser.objectMapping()))
        return mapping!
    }
}

@objc class LeaderboardUser: SIAUser {
    
    var totalLikes: NSNumber?
    var rank: NSNumber?
    
    override static func objectMapping() -> RKObjectMapping {
        
        let mapping = RKObjectMapping(for: LeaderboardUser.self)
        let userAttributes = NSArray(array: SIAUser.objectMapping().attributeMappings, copyItems: true)
        mapping?.addAttributeMappings(from: userAttributes as! [Any])
        mapping?.addAttributeMappings(from: ["likes" : "totalLikes",
                                             "rank"  : "rank"])
        return mapping!
    }
}
