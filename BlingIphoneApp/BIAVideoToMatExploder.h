//
//  BIAVideoToMatExploder.h
//  
//
//  Created by Michael Biehl on 11/13/16.
//
//

#import <Foundation/Foundation.h>
#import <opencv2/opencv.hpp>
#import <opencv2/imgproc/imgproc.hpp>

@interface BIAVideoToMatExploder : NSObject

@property (strong, nonatomic) NSURL *outputURL;
@property (nonatomic) CGSize videoSize;

- (id)initWithAsset:(AVAsset *)asset;
- (void)explodeVideoWithCompletion:(void (^)(BOOL completed))handler;
- (cv::Mat)getFrameAtTime:(CMTime)time ;

@end
