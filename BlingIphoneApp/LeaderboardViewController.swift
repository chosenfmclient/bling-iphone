//
//  LeaderboardViewController.swift
//  
//
//  Created by Michael Biehl on 2/27/17.
//
//

import UIKit

class LeaderboardViewController: SIABaseViewController, UITableViewDelegate, UITableViewDataSource, PageRequestHandlerDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    
    var pageRequestHandler: PageRequestHandler?
    var userList: [Any?] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView() // h8 u appl
        pageRequestHandler = PageRequestHandler(apiPath: "api/v1/leaderboard",
                                                delegate: self)
        
//        let user = BIALoginManager.sharedInstance().myUser!
//        var userz = [user]
//        for _ in 0...100 {
//            userz += [user]
//        }
//        userList = userz
        pageRequestHandler?.fetchData()
        navigationController?.setNavigationItemTitle("Top Users")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dataWasLoaded(page: Int, data: Any?, requestIdentifier: Any?) {
        guard let leaderboardModel = data as? LeaderboardDataModel,
                let _ = leaderboardModel.userList else {
            pageRequestHandler?.didReachEndOfContent = true
            return
        }
        if userList.count > 0 && userList.last! == nil {
            userList.removeLast()
        }
        userList = userList + leaderboardModel.userList!
        userList.append(nil)
        tableView.reloadData()
    }
    
    func endOfContentReached(at page: Int, requestIdentifier: Any?) {
        guard userList.count > 0 else { return }
        userList.removeLast()
        let path = IndexPath(row: userList.count, section: 0)
        tableView.deleteRows(at: [path], with: .bottom)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier(for: indexPath.row),
                                                 for: indexPath)
        
        guard let userCell = cell as? LeaderboardTableViewCell, let user = userList[indexPath.row] as? LeaderboardUser else {
            if let loadingCell = cell as? SIALoadingTableViewCell {
                loadingCell.spinnerTintColor = UIColor(hex: "808080")
                return loadingCell
            }
            return cell
        }
        userCell.rankLabel.text = String(indexPath.row + 1)
        userCell.nameLabel.text = user.fullName
        userCell.likesLabel.text = likesLabelText(for: user)
        userCell.userImageView.sd_setImage(with: user.imageURL)
        
        return userCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let user = userList[indexPath.row] as? SIAUser, let _ = user.user_id else { return }
        performSegue(withIdentifier: "profileSegue",
                     sender: user)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell is SIALoadingTableViewCell {
            pageRequestHandler?.fetchData()
        }
    }
    
    func cellIdentifier(for row: Int) -> String {
        if userList[row] == nil {
            return "loadingCell"
        }
        return "leaderboardCell"
    }
    
    func likesLabelText(for user: LeaderboardUser) -> String {
        guard let totalLikes = user.totalLikes else { return "TOTAL LIKES MISSING" }
        
        let likesStr = String.shorthandNumber(from: totalLikes.uintValue)
        let likeOrLikes = totalLikes == 1 ? " like" : " likes"
        return likesStr + likeOrLikes + " yesterday"
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let user = sender as? SIAUser, let _ = user.user_id {
            (segue.destination as! ProfileViewController).userId = user.user_id
        }
    }

}

class LeaderboardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    
    override func awakeFromNib() {
        userImageView.bia_rounded()
    }
}
