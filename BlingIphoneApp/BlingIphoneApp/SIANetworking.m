//
//  STVViewController.m
//  SingrTV
//
//  Created by Lior Lasry on 20/02/13.
//  Copyright (c) 2013 Lev G. All rights reserved.
//

#import "SIANetworking.h"
#import "Reachability.h"
#import <RestKit/RestKit.h>

#pragma mark -
#pragma mark NETWORKING IMPLEMENTATION
#pragma mark

@implementation SIANetworking


+(void) showAlert:(NSString*) inTitle  message:(NSString*) inMessage
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:inTitle
                              message:inMessage
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [alertView show];
   
}

+(BOOL) checkConnection{
    if ([SIANetworking internetConnectionAvailable])
        return YES;
    else {
        AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        [delegate showNetworkDialogFor: 0];

        return NO;
    }
    
    
    
}

+(BOOL) internetConnectionAvailable
{
    Reachability  *reach  = [ Reachability reachabilityForInternetConnection];
    NetworkStatus status  = [ reach currentReachabilityStatus ];
    
    return ( status != NotReachable ) ;
    
}

+(BOOL) wifiAvailable
{
    Reachability  *reach  = [ Reachability reachabilityForLocalWiFi ];
    NetworkStatus status  = [ reach currentReachabilityStatus ];
    
    if (status != ReachableViaWiFi){
        dispatch_async(dispatch_get_main_queue(), ^{
       /* [[[UIAlertView alloc] initWithTitle:@"Wifi connection" message:@"You are not connected to Wifi" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show]; */
           //TODO: replace popupview
//            [[PopUpView popUpView] show:NSLocalizedString(@"You are not connected to Wifi", nil) withTitle:NSLocalizedString(@"Wifi connection", nil) optionalButton:nil];
        });
    }
    
    return ( status == ReachableViaWiFi ) ;
}

+(void) handleJsonError: (NSError*)error withJson:(NSString*)json;
{
    //NSLog(@"\nJSON: %@ \n Error: %@", json, error);
    if(![SIANetworking internetConnectionAvailable])
    {
        [SIANetworking showAlert:@"Error" message:@"The server could not be reached"];
    }
    else
    {
        // @@@  lior@singr.fm - todo if the json is in the middle of longer operation you may want to wait for the operation to finish
        // before alerting the user. maybe add to this method BOOL to disply/not disply alert to user?
        //[STVNetworking showAlert:@"Error" message:@"Please check that the phone is connected to WiFi"];
    }
}


+(id) getJSON:(NSString*)urlStr;
{
    __block NSObject* res = nil;

        NSError *error = nil;
    NSString* webStringURL = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:webStringURL];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSString *jsonWitnNewline = [NSString stringWithContentsOfURL:url
                                                             encoding:NSASCIIStringEncoding
                                                                error:&error];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSString *json = [[jsonWitnNewline componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
    
    //NSLog(@"\n\njson: %@", json);
    
        if(!error) {
            NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
            //NSLog(@"\n\njsonData: %@", jsonData);
            //see http://stackoverflow.com/questions/8356842/how-to-use-nsjsonserialization
            
            res = [NSJSONSerialization JSONObjectWithData:jsonData
                                                  options:NSJSONReadingMutableContainers
                                                    error:&error];
            if (error) {
                [SIANetworking handleJsonError:error withJson:json];
            }
        } else {
            [SIANetworking handleJsonError:error withJson:json];
        }
       return res;
}

//
+(void) getAsset:(NSString*)stringURL assetName:(NSString*)nameStr
{
    //NSString *stringURL = @"http://www.somewhere.com/thefile.png";
    NSURL  *url = [NSURL URLWithString:stringURL];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    if ( urlData )
    {
        NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = paths[0];
        
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,nameStr];//@"filename.png"
        [urlData writeToFile:filePath atomically:YES];
    }
}


static const char _base64EncodingTable[64] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const short _base64DecodingTable[256] = {
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -1, -1, -2, -1, -1, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-1, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, 62, -2, -2, -2, 63,
	52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -2, -2, -2, -2, -2, -2,
	-2,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
	15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -2, -2, -2, -2, -2,
	-2, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2
};

+ (NSString *) base64EncodeString: (NSString *) strData {
	return [self base64EncodeData: [strData dataUsingEncoding: NSUTF8StringEncoding] ];
}

+ (NSString *) base64EncodeData: (NSData *) objData {
	const unsigned char * objRawData = [objData bytes];
	char * objPointer;
	char * strResult;
    
	// Get the Raw Data length and ensure we actually have data
	int intLength = (int)[objData length];
	if (intLength == 0) return nil;
    
	// Setup the String-based Result placeholder and pointer within that placeholder
	strResult = (char *)calloc(((intLength + 2) / 3) * 4, sizeof(char));
	objPointer = strResult;
    
	// Iterate through everything
	while (intLength > 2) { // keep going until we have less than 24 bits
		*objPointer++ = _base64EncodingTable[objRawData[0] >> 2];
		*objPointer++ = _base64EncodingTable[((objRawData[0] & 0x03) << 4) + (objRawData[1] >> 4)];
		*objPointer++ = _base64EncodingTable[((objRawData[1] & 0x0f) << 2) + (objRawData[2] >> 6)];
		*objPointer++ = _base64EncodingTable[objRawData[2] & 0x3f];
        
		// we just handled 3 octets (24 bits) of data
		objRawData += 3;
		intLength -= 3;
	}
    
	// now deal with the tail end of things
	if (intLength != 0) {
		*objPointer++ = _base64EncodingTable[objRawData[0] >> 2];
		if (intLength > 1) {
			*objPointer++ = _base64EncodingTable[((objRawData[0] & 0x03) << 4) + (objRawData[1] >> 4)];
			*objPointer++ = _base64EncodingTable[(objRawData[1] & 0x0f) << 2];
			*objPointer++ = '=';
		} else {
			*objPointer++ = _base64EncodingTable[(objRawData[0] & 0x03) << 4];
			*objPointer++ = '=';
			*objPointer++ = '=';
		}
	}
    
	// Terminate the string-based result
	*objPointer = '\0';
    
	// Return the results as an NSString object
    NSString *returnString = [NSString stringWithUTF8String:strResult];
    free(strResult);
    return returnString;
}

+ (NSData *) base64DecodeString: (NSString *) strBase64 {
	const char * objPointer = [strBase64 cStringUsingEncoding:NSASCIIStringEncoding];
	int intLength = (int)strlen(objPointer);
	int intCurrent;
	int i = 0, j = 0, k;
    
	unsigned char * objResult;
	objResult = calloc(intLength, sizeof(unsigned char));
    
	// Run through the whole string, converting as we go
	while ( ((intCurrent = *objPointer++) != '\0') && (intLength-- > 0) ) {
		if (intCurrent == '=') {
			if (*objPointer != '=' && ((i % 4) == 1)) {// || (intLength > 0)) {
				// the padding character is invalid at this point -- so this entire string is invalid
				free(objResult);
				return nil;
			}
			continue;
		}
        
		intCurrent = _base64DecodingTable[intCurrent];
		if (intCurrent == -1) {
			// we're at a whitespace -- simply skip over
			continue;
		} else if (intCurrent == -2) {
			// we're at an invalid character
			free(objResult);
			return nil;
		}
        
		switch (i % 4) {
			case 0:
				objResult[j] = intCurrent << 2;
				break;
                
			case 1:
				objResult[j++] |= intCurrent >> 4;
				objResult[j] = (intCurrent & 0x0f) << 4;
				break;
                
			case 2:
				objResult[j++] |= intCurrent >>2;
				objResult[j] = (intCurrent & 0x03) << 6;
				break;
                
			case 3:
				objResult[j++] |= intCurrent;
				break;
		}
		i++;
	}
    
	// mop things up if we ended on a boundary
	k = j;
	if (intCurrent == '=') {
		switch (i % 4) {
			case 1:
				// Invalid state
				free(objResult);
				return nil;
                
			case 2:
				k++;
				// flow through
			case 3:
				objResult[k] = 0;
		}
	}
    
	// Cleanup and setup the return NSData
	NSData * objData = [[NSData alloc] initWithBytes:objResult length:j];
	free(objResult);
	return objData;
}

@end

