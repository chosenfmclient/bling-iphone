//
//  RateTheAppViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 04/12/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
import MessageUI

class RateTheAppViewController: UIViewController {
    //MARK: Properties
    @IBOutlet weak var yepButton: UIButton!
    @IBOutlet weak var nopeButton: UIButton!
    @IBOutlet weak var rateView: UIView!
    
    let appID = "1200290697"
    let appstoreURL = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software"
    
    static let kDidShow = "did_show_rate"
    static let kNoRateDate = "no_rate_date"
    static let kHomeOpens = "home_opens"
    static let kVideoViews = "video_views"
    
    static func create() -> RateTheAppViewController {
        return UIStoryboard(name: "RateTheApp", bundle: nil).instantiateInitialViewController() as! RateTheAppViewController
    }
    
    //MARK: VC Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(0, forKey: RateTheAppViewController.kHomeOpens)
        UserDefaults.standard.set(0, forKey: RateTheAppViewController.kVideoViews)
        UserDefaults.standard.set(true, forKey: RateTheAppViewController.kDidShow)
        AmplitudeEvents.log(event: "ratetheapp:view")
    }
    
    override func viewDidLayoutSubviews() {
        yepButton.bia_rounded()
        rateView.layer.cornerRadius = 5.0
    }
    
    //MARK: Actions
    @IBAction func yepButtonWasTapped(_ sender: Any) {
        UIApplication.shared.openURL(
            URL(string: String(format: appstoreURL, appID))!
        )
        dismiss(animated: true, completion: nil)
        AmplitudeEvents.log(event: "ratetheapp:rate")
    }
    
    @IBAction func nopeButtonWasTapped(_ sender: Any) {
        UserDefaults.standard.set(Date(), forKey: RateTheAppViewController.kNoRateDate)
        dismiss(animated: true, completion: nil)
        AmplitudeEvents.log(event: "ratetheapp:no")
    }
    
    @IBAction func sendFeedbackWasTapped(_ sender: Any) {
        UserDefaults.standard.set(Date(), forKey: RateTheAppViewController.kNoRateDate)
        AmplitudeEvents.log(event: "ratetheapp:support")
        
        guard MFMailComposeViewController.canSendMail() else {
            return
        }
        let mc = MFMailComposeViewController()
        mc.setSubject("Feedback")
        mc.setToRecipients(["support@blin.gy"])
        mc.mailComposeDelegate = (UIApplication.shared.delegate as! AppDelegate).rootNavController() as! MFMailComposeViewControllerDelegate?
        dismiss(animated: true, completion: {
            (UIApplication.shared.delegate as! AppDelegate).rootNavController()!.present(mc, animated: true, completion: nil)
        })
    }
    
    
    //MARK: Show criteria
    static func shouldShowOnHome() -> Bool {
        guard is24HoursSinceInstall() else {
            return false
        }
        return !hasShown() && hasViewedTenVideos()
    }
    
    private static func hasViewedTenVideos() -> Bool {
        return UserDefaults.standard.integer(forKey: kVideoViews) >= 10
    }
    
    static func hasShown() -> Bool {
        return UserDefaults.standard.bool(forKey: kDidShow)
    }
    
    static func shouldResurface() -> Bool {
        return dateCriteriaMet() && hasOpenedHome(times: 10) && hasViewedTenVideos()
    }
    
    static func didOpenHome() {
        var nOpens = UserDefaults.standard.integer(forKey: kHomeOpens)
        nOpens += 1
        UserDefaults.standard.set(nOpens, forKey: kHomeOpens)
    }
    
    static func viewedVideo() {
        var nOpens = UserDefaults.standard.integer(forKey: kVideoViews)
        nOpens += 1
        UserDefaults.standard.set(nOpens, forKey: kVideoViews)
    }
    
    private static func dateCriteriaMet() -> Bool {
        //check they said no 7 days ago
        guard let met = (UserDefaults.standard.object(forKey: kNoRateDate) as? Date)?.isLessThanDate(dateToCompare: Date().addingTimeInterval(-7*24*60*60)) else {
            return false
        }
        return met
    }
    
    static func is24HoursSinceInstall() -> Bool {
        guard let is24Hours = (UserDefaults.standard.object(forKey: "install_date") as? Date)?.isLessThanDate(dateToCompare: Date().addingTimeInterval(-24 * 60 * 60)) else {
            return false
        }
        return is24Hours;
    }
    
    private static func hasOpenedHome(times : Int) -> Bool {
        return UserDefaults.standard.integer(forKey: kHomeOpens) >= times
    }
}
