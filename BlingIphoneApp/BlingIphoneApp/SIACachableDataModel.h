//
//  SIACachableDataModel.h
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 12/15/15.
//  Copyright © 2015 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SIACachableDataModel <NSObject>
+ (NSURLRequestCachePolicy) cachePolicy;
@end
