//
//  SIAObjectManager.h
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 12/15/15.
//  Copyright © 2015 SingrFM. All rights reserved.
//

#import <RestKit/RestKit.h>
#import "SIACachableDataModel.h"

@interface SIAObjectManager : RKObjectManager
+ (NSURLRequestCachePolicy)cachePolicyForRequest:(NSURLRequest*)request responseDescriptors:(NSArray *)responseDescriptors;
@end
