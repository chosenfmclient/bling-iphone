//
//  SIARecordButton.h
//  
//
//  Created by Michael Biehl on 2/7/16.
//
//

#import <UIKit/UIKit.h>

@interface SIARecordButton : UIButton

- (void) setRedViewForRecord;
- (void) setRedViewForPhoto;
- (void) setRedViewForPause;
- (void) setRedViewForStop;

@end
