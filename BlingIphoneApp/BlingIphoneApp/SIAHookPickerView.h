//
//  SIAHookPickerView.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/24/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SIAHookPickerDelegate <NSObject>
@required
- (void)hookTimeChangedWithStart:(CGFloat)start end:(CGFloat)end;

@end


@interface SIAHookPickerView : UIView

//@property (weak, nonatomic) IBOutlet UICollectionView *thumbsCollectionView;
@property (weak, nonatomic) IBOutlet UISlider *pickSlider;
@property (weak, nonatomic) SIARecordingItem *recordingItem;
@property (strong, nonatomic) id <SIAHookPickerDelegate> delegate;

- (void)setupForRecordingItem:(SIARecordingItem *)recordingItem delegate:(id <SIAHookPickerDelegate>)delegate;

@end
