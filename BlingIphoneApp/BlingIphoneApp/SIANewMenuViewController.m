//
//  SIANewMenuViewController.m
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 7/28/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import "SIANewMenuViewController.h"
#import "SIAMenuTableViewCell.h"
@import Photos;

#define CELL_LABEL_0 @"Find Friends"
#define CELL_LABEL_1 ANONYMOUS_USER ? @"Connect" : @"My Profile"
#define CELL_LABEL_2 @"My Videos & Photos"
#define CELL_LABEL_3 @"Settings"
#define CELL_LABEL_4 @"Prizes"
#define CELL_LABEL_5 @"Support"
#define CELL_LABEL_6 @"Reset onboarding"


#define ACCESSIBILITY_LABEL_RECORD @"menu_button_record"
#define ACCESSIBILITY_LABEL_TOP_PLAYERS @"menu_button_topPlayers"
#define ACCESSIBILITY_LABEL_FIND_FRIENDS @"menu_button_findFriends"
#define ACCESSIBILITY_LABEL_PROFILE_CONNECT ANONYMOUS_USER ? @"menu_button_connect" : @"menu_button_myProfile"
#define ACCESSIBILITY_LABEL_MY_VIDEOS @"menu_button_myVideos"
#define ACCESSIBILITY_LABEL_SETTINGS @"menu_button_settings"
#define ACCESSIBILITY_LABEL_PRIZES @"menu_button_prizes"
#define ACCESSIBILITY_LABEL_SUPPORT @"menu_button_support"
#define ACCESSIBILITY_LABEL_ONBOARDING @"menu_button_resetOnboarding"

#define MENU_CELL_ID @"menuCell"

#define GRAY_ARROW_IMAGE  @"icon_menu_arrow.png"

#define DELAY_AFTER_SELECTING_CELL (0.25)
#define NUMBER_OF_ROWS (7)
#define CONNECT_CELL_ROW (1)
#define ROW_HEIGHT (45)

@interface SIANewMenuViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property NSMutableArray *cellIdentifiers;
@property NSMutableArray *accessibilitycellIdentifiers;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;

@end

@implementation SIANewMenuViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _menuTableView.delegate = self;
    _menuTableView.dataSource = self;
    
    //_cellIdentifiers = [[NSMutableArray alloc] initWithArray:@[@"settingsCell", @"editProfileCell", @"findFriendsCell", @"uploadVideoCell", @"prizesCell", @"supportCell"]];//CELL_LABEL_7]];
    
    _cellIdentifiers = [[NSMutableArray alloc] initWithArray:@[@"settingsCell", @"editProfileCell", @"findFriendsCell", @"uploadVideoCell", @"supportCell"]];//CELL_LABEL_7]];
    _accessibilitycellIdentifiers = [NSMutableArray arrayWithArray:@[//ACCESSIBILITY_LABEL_RECORD,
                                                                     //ACCESSIBILITY_LABEL_TOP_PLAYERS,
                                                                     ACCESSIBILITY_LABEL_SETTINGS,
                                                                 ACCESSIBILITY_LABEL_FIND_FRIENDS,
                                                                 //ACCESSIBILITY_LABEL_PRIZES,
                                                                 ACCESSIBILITY_LABEL_SUPPORT,
                                                                 ACCESSIBILITY_LABEL_ONBOARDING]];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.menuTableView.userInteractionEnabled = YES;
    
//    [self.navigationBar setBackgroundImage:[UIImage new]
//                             forBarMetrics:UIBarMetricsDefault];
//    self.navigationBar.shadowImage = [UIImage new];
    
//    SIAMenuTableViewCell *cell = (SIAMenuTableViewCell *)[_menuTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:CONNECT_CELL_ROW inSection:0]];
//    if (cell) {
//        cell.cellLabel.text = CELL_LABEL_1;
//    }
    
 //   [self.navigationController clearNavigationItemLeftButton];
    [self.navigationController setNavigationItemTitle:@"Menu"];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController setBottomBarHidden:NO animated: NO];

}

- (void) viewDidAppear:(BOOL)animated {
   // [Flurry logEvent:FLURRY_VIEW_MENU];
    self.menuTableView.tableFooterView = [UIView new];
    [self.navigationController setBottomBarHidden:NO animated: NO];
}

- (void)viewDidLayoutSubviews
{
//    _menuTableView.frame = CGRectMake(0, 82, self.view.bounds.size.width, _cellIdentifiers.count * ROW_HEIGHT);
}

#pragma mark - table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _cellIdentifiers.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ROW_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SIAMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifiers[indexPath.row]
                                                            forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    
//    cell.cellLabel.text = _cellIdentifiers[indexPath.row];
//    cell.accessibilityLabel = _accessibilitycellIdentifiers[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell sia_setBackgroundColorAnimated:[[UIColor alloc] initWithRed:191./255. green:192./255. blue:193./255. alpha:1]];
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell sia_setBackgroundColorAnimated: [UIColor whiteColor]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row)
    {
        case 1:
        {
            if (![BIALoginManager sharedInstance].myUser) {
                [SIAUser getUserWithId:nil completionHandler:^(SIAUser *user) {
                    [BIALoginManager sharedInstance].myUser = user;
                    NSMutableArray *activeViewControllers = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
                    EditProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"editProfileViewController"];
                    vc.user = user;
                    vc.delegate = self.editProfileDelegate;
                    [activeViewControllers addObject:vc];
                    CATransition *transition = [[CATransition alloc] init];
                    transition.duration = 0.1;
                    transition.type = kCATransitionFade;
                    [self.navigationController.view.layer addAnimation:transition forKey:@"transition"];
                    [self.navigationController setViewControllers:activeViewControllers animated: false];
                }];
                return;
            }
            
            NSMutableArray *activeViewControllers = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
            EditProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"editProfileViewController"];
            vc.user = [BIALoginManager sharedInstance].myUser;
            vc.delegate = self.editProfileDelegate;
            [activeViewControllers addObject:vc];
            CATransition *transition = [[CATransition alloc] init];
            transition.duration = 0.1;
            transition.type = kCATransitionFade;
            [self.navigationController.view.layer addAnimation:transition forKey:@"transition"];
            [self.navigationController setViewControllers:activeViewControllers animated: false];

            break;
        }
        case 2:
        {
            NSMutableArray *activeViewControllers = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
            InviteContainerViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"inviteContainerViewController"];
            [activeViewControllers addObject:vc];
            CATransition *transition = [[CATransition alloc] init];
            transition.duration = 0.1;
            transition.type = kCATransitionFade;
            [self.navigationController.view.layer addAnimation:transition forKey:@"transition"];
            [self.navigationController setViewControllers:activeViewControllers animated: false];
            [self.navigationController setNavigationBarHidden:YES animated:NO];

            break;
        }
        case 3:
        {
            [self goToUploadVideo];
            // [Flurry logEvent:FLURRY_TAP_SETTINGS_MENU];
            //  [self.navigationController switchToSettingsViewController];
            break;
        }
        //case 4:
        //{
            // [Flurry logEvent:FLURRY_TAP_PRIZES_MENU];
        //    [self gotoPrizesWebpage];
        //    break;
        //}
        case 4:
        {
            // [Flurry logEvent:FLURRY_TAP_SUPPORT_MENU];
            [self goToSupport];
            break;
        }
        default:
            break;
    }
}

- (void)goToUploadVideo {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Submit a video?"
                                                                             message:@"Please make sure your video is 16:9 or 4:3 aspect ratio" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [self presentUploadFlow];
                                                      }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                                        style:UIAlertActionStyleCancel
                                                      handler:nil]];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}

- (void)presentUploadFlow {
    __weak SIANewMenuViewController *wSelf = self;
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        
        switch (status) {
            case PHAuthorizationStatusAuthorized:
            {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = wSelf;
                imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                imagePicker.mediaTypes = @[(NSString *) kUTTypeMovie];
                imagePicker.navigationBar.barStyle = UIBarStyleBlack;
                imagePicker.navigationBar.tintColor = [UIColor whiteColor];
                imagePicker.navigationBar.translucent = NO;
                imagePicker.navigationBar.barTintColor = [[SIAStyling defaultStyle] blingMainColor];
                [Utils dispatchOnMainThread:^{
                    [wSelf presentViewController:imagePicker animated:YES completion:nil];
                }];
            }
                break;
            case PHAuthorizationStatusRestricted:
                break;
            case PHAuthorizationStatusDenied:
            {
                [Utils dispatchOnMainThread:^{
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Camera Roll Permission"
                                                                                             message:@"You have denied Blingy access to your camera roll. Please allow access in your phone's privacy settings in order to upload a video." preferredStyle:UIAlertControllerStyleAlert];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleCancel
                                                             handler:nil]];
                    [self presentViewController:alertController
                                       animated:YES
                                     completion:nil];
                }];
                break;
            }
            default:
                break;
        }
    }];
}

// for setting the nav bar properties of the image picker thing
- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated {
    
    viewController.navigationItem.title = @"Videos";
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    NSURL *videoURL = [info objectForKey:UIImagePickerControllerReferenceURL];
    
    SIARecordingItem *item = [[SIARecordingItem alloc] init];
    item.recordURL = videoURL;
    item.status = @"secret";
    item.recordStatus = item.status;
    __weak SIANewMenuViewController *wSelf = self;
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 //present upload screen here
                                 [wSelf performSegueWithIdentifier:@"uploadSegue"
                                                            sender:item];
                             }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([sender isKindOfClass:[SIARecordingItem class]] && [segue.destinationViewController isKindOfClass:[UploadVideoViewController class]])
    {
        UploadVideoViewController *vc = segue.destinationViewController;
        vc.recordingItem = sender;
    }
}

/** \brief go to support web page
 *
 * \param none
 *
 * \return none
 *
 * ...
 *
 */

- (IBAction)goToSupport {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://blingy.zendesk.com"]];
    self.menuTableView.userInteractionEnabled = YES;

}

- (IBAction)dismiss:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES
                                                      completion:nil];
}

/** \brief go to prizes web pages
 *
 * \param none
 *
 * \return none
 *
 * ...
 *
 */

- (void) dealloc {
    self.menuTableView.dataSource = nil;
    self.menuTableView.delegate = nil;
    self.menuTableView = nil;
}

- (IBAction)gotoPrizesWebpage {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://blin.gy/prizes"]];
    self.menuTableView.userInteractionEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
