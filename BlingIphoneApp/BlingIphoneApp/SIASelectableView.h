//
//  SIASelectableView.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 3/24/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIASelectableView : UIView

@property (strong, nonatomic) UIButton *button;
@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) UIImageView *imageView;

- (id)initWithImageURL:(NSURL *)imageURL
                 title:(NSString *)title
          buttonTarget:(id)target
        buttonSelector:(SEL)selector
             buttonTag:(NSUInteger)tag
        placementIndex:(NSUInteger)idx;

@end
