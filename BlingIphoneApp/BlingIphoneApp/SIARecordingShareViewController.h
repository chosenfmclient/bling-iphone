//
//  SIARecordingShareViewController.h
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 18/09/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIAPostRecordViewController.h"
@class SIARecorderPreviewPlayerViewController;

@interface SIARecordingShareViewController : SIABasePostRecordViewController <SIAUploadVideoDelegate, UIAlertViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet UIButton *goToButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (strong, nonatomic) SIAShare *share;
//@property (weak, nonatomic) SIARecorderPreviewPlayerViewController *previewParent;
@property (weak, nonatomic) SIAPostRecordViewController *previewParent;


- (void) setOnboardingStyle:(UIView *)cmView textLabel:(UILabel *)textLabel;
- (void) showCoachMark;
- (void)updateFacebookUploadProgress:(CGFloat)progress;

@end
