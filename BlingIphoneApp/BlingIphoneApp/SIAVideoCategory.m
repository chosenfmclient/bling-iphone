//
//  SIAVideoCategory.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/10/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAVideoCategory.h"

@implementation SIAVideoCategory

+ (RKObjectMapping *)objectMapping {
    
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[self class]];
    
    [objectMapping addAttributeMappingsFromArray:@[@"label", @"types"]];
    
    return objectMapping;
}

@end
