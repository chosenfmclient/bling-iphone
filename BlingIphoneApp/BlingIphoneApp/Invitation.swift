//
//  Invitation.swift
//  BlingIphoneApp
//
//  Created by Zach on 23/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class Invitation: NSObject {
    var name: String?
    var email: String?
    var phone: String?
    
    init(inviteName: String, inviteEmail: String?, invitePhone: String?) {
        name = inviteName
        email = inviteEmail
        phone = invitePhone
    }
    
    static func objectMapping() -> (RKObjectMapping) {
        let mapping = RKObjectMapping(for: Invitation.self)
        mapping!.addAttributeMappings(
            from:[
                "name":"name",
                "email":"email",
                "phone":"phone",
            ]
        )
        return mapping!.inverse()
    }
}
