//
//  SIAUser.m
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 20/07/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAUser.h"
#import "SIAObjectManager.h"
#import "SIANetworking.h"

#define USER_WITHOUT_KEY @"api/v1/users/me"
#define USER_WITH_ID @"api/v1/users/%@"
#define FOLLOW_API @"/api/v1/users/%@/follow"
#define ACCESS_TOKEN      @"access_token"

#define kUserId             @"user_id"
#define kFirstName          @"first_name"
#define kLastName           @"last_name"
#define kUsername           @"username"
#define kEmail              @"email"
#define kBio                @"bio"
#define kLocation           @"location"
#define kTotalFollowers     @"total_followers"
#define kTotalFollowing     @"total_following"
#define kFollowedByMe       @"followed_by_me"
#define kFollowingMe        @"following_me"
#define kExperience         @"experience"
#define kLevel              @"level"
#define kPerformerRank      @"performer_rank"
#define kJudgeRank          @"judge_rank"
#define kTotalBadges        @"total_badges"
#define kBadges             @"badges"
#define kTotalPerformances  @"total_prformances"
#define kPrformances        @"prformances"
#define kTotalResponses     @"total_responses"
#define kResponses          @"responses"
#define kActivities         @"activities"
#define kTotalVotes         @"total_votes"
#define kPhoto              @"photo"
#define kExperience         @"experience"
#define kCurrent            @"current"
#define kRequired           @"required"
#define kRequiredTotal      @"required_total"
#define kEarned             @"earned"
#define kLacked             @"lacked"

#define kPlaceholderImage [UIImage imageNamed:@"extra_large_userpic_placeholder.png"]

NSString *const SIAUserDidLoadImageNotification = @"SIAUserDidLoadImage";

@interface SIAUser ()

@property(strong, readwrite, nonatomic) NSString *username;
@property(nonatomic, readwrite, strong) NSArray *prformances;
@property(nonatomic, readwrite) NSNumber *total_responses;
@property(nonatomic, readwrite, strong) NSArray *responses;
@property(nonatomic, readwrite, strong) NSArray *activities;


@end


@implementation ExperiencePoints

@end


@implementation SIAUser

@dynamic fullName;

- (void)imageWithCompletionHandler:(void (^)(UIImage *))handler {
    if (self.image) {
        if (handler) {
            handler(self.image);
        }
    } else {
        
        [[SDWebImageManager sharedManager] loadImageWithURL:self.imageURL options:0 progress:nil completed:^(UIImage *image, NSData *data, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            self.image = image ? image : kPlaceholderImage;
            if (handler) {
                handler(self.image);
            }
        }];
    }
}

- (id)copyWithZone:(NSZone *)zone {
    
    SIAUser *newUser = [[[self class] allocWithZone:zone] init];
    
    if (newUser)
    {
        newUser.user_id = _user_id;
        newUser.twitter_id = _twitter_id;
        newUser.username = _username;
        newUser.first_name = _first_name;
        newUser.last_name = _last_name;
        newUser.bio = _bio;
        newUser.email = _email;
        newUser.location = _location;
        newUser.imageURL = _imageURL;
        newUser.image = _image;
        newUser.total_performances = _total_performances;
        newUser.total_followers = _total_followers;
        newUser.total_following = _total_following;
        newUser.total_responses = _total_responses;
        newUser.following_me = _following_me;
        newUser.followedByMe = _followedByMe;
        newUser.twitter_handle = _twitter_handle;
        newUser.facebook_handle = _facebook_handle;
        newUser.instagram_handle = _instagram_handle;
        newUser.youtube_handle = _youtube_handle;
    }
    return newUser;
}


+ (RKObjectMapping *)objectMapping {

    RKObjectMapping *userMapping = [RKObjectMapping mappingForClass:[SIAUser class]];

    // get field names when server is ready
    [userMapping addAttributeMappingsFromDictionary:@{@"user_id" : @"user_id",
            @"twitter_id" : @"twitter_id",
            @"username" : @"username",
            @"first_name" : @"first_name",
            @"last_name" : @"last_name",
            @"bio" : @"bio",
            @"email" : @"email",
            @"location" : @"location",
            @"photo" : @"imageURL",
            @"total_performances" : @"total_performances",
            @"total_responses" : @"total_responses",
            @"total_followers" : @"total_followers",
            @"total_following" : @"total_following",
            @"twitter_handle" : @"twitter_handle",
            @"youtube_handle" : @"youtube_handle",
            @"instagram_handle" : @"instagram_handle",
            @"facebook_handle" : @"facebook_handle",
    }];

    return userMapping;
}

+(RKObjectMapping*) requestMapping {
    return [[SIAUser objectMapping] inverseMapping];
}
/**
 getUserWithId: (NSString*)userId completionHandler:(void (^)(SIAUser *user))handler
 get a user by user_id
 if user_id is null it returns the current user
 */
+ (void)getUserWithId: (NSString*) userId  completionHandler:(void (^)(SIAUser *user))handler {
    NSString* api = userId ? [NSString stringWithFormat:USER_WITH_ID, userId] : USER_WITHOUT_KEY;
    [RequestManager
     getRequest:api
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         handler((SIAUser *) mappingResult.firstObject);
         if (!userId) {
             [BIALoginManager.sharedInstance fetchLikesFromServer];
             [BIALoginManager.sharedInstance fetchFollowsFromServer];
         }
     }
     failure:^(RKObjectRequestOperation *operation, NSError *error) {
         NSLog(@"What do you mean by 'there are no videos?': %@", error);
         handler(nil);
         
     }
     showFailureDialog:YES];

}

- (void)followWithHandler:(void (^)(BOOL success))handler
    withAmplitudePageName:(NSString*) pageName{
    [self followWithHandler:handler
      withAmplitudePageName:pageName
           withListPosition:NSNotFound];
}

- (void)followWithHandler:(void (^)(BOOL success))handler
    withAmplitudePageName:(NSString*) pageName
         withListPosition:(NSInteger)position {

    [CoreDataManager.shared saveFollowFor:_user_id];
    NSString *followApi = [NSString stringWithFormat:FOLLOW_API, self.user_id];
    [RequestManager
     putRequest:followApi
     object:nil
     queryParameters:nil
     success:^(RKObjectRequestOperation * _Nullable request, RKMappingResult * _Nullable result) {
        NSLog(@"success");
         if (handler) {
             handler(YES);
         }
         NSNumber *totalFollowing = @([CoreDataManager.shared countTotalFollowingWithDidFollow: YES]);
         NSMutableDictionary *props = @{@"followed_id":_user_id,
                                        @"pagename":pageName,
                                        @"following_count" : totalFollowing}.mutableCopy;
         if (position != NSNotFound) {
             props[@"pos"] = @(position);
         }
         [AmplitudeEvents logWithEvent:@"follow" with:props];
         [AmplitudeEvents logUserWithProperty:@{@"following":totalFollowing}];
    } failure:^(RKObjectRequestOperation * _Nullable request, NSError * _Nullable error) {
        NSLog(@"fail");
        [CoreDataManager.shared deleteFollowFor:_user_id];
        if (handler) {
            handler(NO);
        }
    }
     showFailureDialog:YES];
}

- (void)unfollowWithHandler:(void (^)(BOOL success))handler
      withAmplitudePageName:(NSString*) pageName{
    [self unfollowWithHandler:handler
        withAmplitudePageName:pageName
             withListPosition:NSNotFound];
}

- (void)unfollowWithHandler:(void (^)(BOOL success))handler
      withAmplitudePageName:(NSString*) pageName
           withListPosition:(NSInteger)position {

    [CoreDataManager.shared deleteFollowFor:_user_id];
    NSString *followApi = [NSString stringWithFormat:FOLLOW_API, self.user_id];
    [RequestManager
     deleteRequest:followApi
     object: nil
     queryParameters:nil
     success:^(RKObjectRequestOperation * _Nullable request, RKMappingResult * _Nullable result) {
         NSLog(@"success");
         if (handler) {
             handler(YES);
         }
         NSNumber *totalFollowing = @([CoreDataManager.shared countTotalFollowingWithDidFollow: NO]);
         NSMutableDictionary *props = @{@"unfollowed_id":_user_id,
                                        @"pagename":pageName,
                                        @"following_count" : totalFollowing}.mutableCopy;
         if (position != NSNotFound) {
             props[@"pos"] = @(position);
         }
         [AmplitudeEvents logWithEvent:@"unfollow" with:props];
         [AmplitudeEvents logUserWithProperty:@{@"following":totalFollowing}];
     } failure:^(RKObjectRequestOperation * _Nullable request, NSError * _Nullable error) {
         NSLog(@"fail");
         [CoreDataManager.shared saveFollowFor:_user_id];
         if (handler) {
             handler(NO);
         }
     }
     showFailureDialog:YES];
}

- (BOOL)followedByMe {
    BOOL followed = [CoreDataManager.shared userIsFollowedByMeWithUserID:_user_id];
    return followed;

}

- (NSString *)fullName {
    return [NSString stringWithFormat:@"%@ %@", self.first_name, self.last_name];
}

@end
