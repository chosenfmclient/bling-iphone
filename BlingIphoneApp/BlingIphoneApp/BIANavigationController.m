//
//  BIANavigationController.m
//  
//
//  Created by Zach on 18/09/2016.
//
//

#import "BIANavigationController.h"
#import "BIALoginManager.h"
#import "SIAVideo.h"
#import <MessageUI/MessageUI.h>

#define VIDEO_PLAYER_VIEW_CONTROLLER @"VideoHomeViewController"

#define NAV_BUTTON_LABEL_BACK_CLOSE @"navBar_button_backClose"
#define NAV_BUTTON_LABEL_NEXT @"navBar_button_next"
#define NAV_BUTTON_LABEL_MENU @"navBar_button_mainMenu"

#define NAV_BAR_IMAGE_POS_X (140)
#define NAV_BAR_IMAGE_POS_Y (10)

@implementation UINavigationBar (customNav)
- (CGSize)sizeThatFits:(CGSize)size {
    CGSize newSize = CGSizeMake(self.superview.bounds.size.width, 53);
    return newSize;
}

@end

@interface BIANavigationController() <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UIImageView *navBarImage;
@property (nonatomic) BOOL cameFromNotifications;
@property (weak, nonatomic) IBOutlet BottomBarButtonView *bottomBarHomeButton;
@property (weak, nonatomic) IBOutlet BottomBarButtonView *bottomBarDiscoverButton;
@property (weak, nonatomic) IBOutlet BottomBarButtonView *bottomBarRecordButton;
@property (weak, nonatomic) IBOutlet BottomBarButtonView *bottomBarNotificationsButton;
@property (weak, nonatomic) IBOutlet BottomBarButtonView *bottomBarProfileButton;

@property (strong, nonatomic) IBOutletCollection(BottomBarButtonView) NSArray *bottomBarButtons;

@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *bottomBarButtonWidthConstraints;


@property (strong, nonatomic) NSLayoutConstraint *bottomBarYPosConstraint;

@end

@implementation BIANavigationController 

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder])
    {
        _shouldShowSplashScreen = YES;
    }
    return self;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    NSString *className = NSStringFromClass(self.topViewController.class);
    NSString *finalStr = [[className componentsSeparatedByString:@"."] lastObject];
    if ([finalStr isEqualToString:VIDEO_PLAYER_VIEW_CONTROLLER] && !self.topViewController.presentedViewController)
    {
        return  UIInterfaceOrientationMaskAllButUpsideDown;
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIViewController*) getInitialViewController {
    return [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateInitialViewController];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
    _tabViewControllers = [NSMutableArray array];
    //self.transitionDelegate = [SIATransitionDelegate new];
    //[Flurry logAllPageViewsForTarget:self];
    if (_shouldShowSplashScreen)
    {
        UIViewController* firstViewController = [self getInitialViewController];
        [self pushViewController:firstViewController animated:NO];
    }
    [self addBottomBarToView];
    [self setBottomBarHidden:_shouldShowSplashScreen
                    animated:NO];
    [self setNavigationBarHidden:NO animated:NO];
    self.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    [self clearBottomButtonSelection];
    
    [self updateNotificationBadge];
}

- (void)updateNotificationBadge {
    [NotificationBadgeModel getNotificationBadgeWithCompletion:^(NotificationBadgeModel *badge) {
        if (!badge
            || [badge.unread isEqualToString:@""]
            || [badge.unread isEqualToString:@"0"])
        {
            return;
        }
        
        self.notificationBadgeLabel.text = badge.unread;
        [self.notificationBadgeLabel sizeToFit];
        
        if (self.notificationBadgeLabel.text.length > 1) {
            self.notificationBadgeViewWidth.constant = self.notificationBadgeLabel.bounds.size.width + 10.0;
        } else {
            self.notificationBadgeViewWidth.constant = 18.0;
        }
        
        self.notificationBadgeView.alpha = 1;
        self.notificationBadgeView.hidden = false;
    }];
}

- (void)hideNotificationBadge {
    [UIView animateWithDuration:0.3 animations:^{
        self.notificationBadgeView.alpha = 0;
    }];
}

- (void)viewDidLayoutSubviews {
    for (NSLayoutConstraint *width in self.bottomBarButtonWidthConstraints) {
        width.constant = self.view.bounds.size.width / self.bottomBarButtonWidthConstraints.count;
        [self.notificationBadgeView bia_rounded];
    }
}

- (void)clearNavigationItemRightButton{
    self.topViewController.navigationItem.rightBarButtonItem = nil;
}

- (void)clearNavigationItemRightButtons {
    self.topViewController.navigationItem.rightBarButtonItems = nil;
}

- (void) setNavigationBarLeftButtonImage:(UIImage *)image action:(SEL)selector{
    [self setNavigationBarLeftButtonImage:image target:self action:selector];
}

- (void) setNavigationBarLeftButtonImage:(UIImage *)image target:(id)target action:(SEL)selector{
    UIImage *buttonImage = image;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(15, 37, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    if (selector == @selector(back)) {
        button.accessibilityLabel = NAV_BUTTON_LABEL_BACK_CLOSE;
    }
    else if (selector == @selector(xButton) || selector == @selector(xButtonWasTapped:)) {
        button.accessibilityLabel = NAV_BUTTON_LABEL_BACK_CLOSE;
    }
    else if (selector == @selector(openMainMenu)) {
        button.accessibilityLabel = NAV_BUTTON_LABEL_MENU;
    }
    else if (selector == @selector(backFromMenu)) {
        button.accessibilityLabel = NAV_BUTTON_LABEL_BACK_CLOSE;
    }
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.topViewController.navigationItem.leftBarButtonItem = customBarItem;
    _cameFromNotifications = NO;
}

- (void) setNavigationBarRightButtonImage:(UIImage *)image target:(id)target action:(SEL)selector{
    UIImage *buttonImage = image;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(15, 37, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    if (selector == @selector(next)) {
        button.accessibilityLabel = NAV_BUTTON_LABEL_NEXT;
    }
    else if (selector == @selector(rotateCamera)) {
        button.accessibilityLabel = @"recorder_button_switchCamera";
    }
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.topViewController.navigationItem.rightBarButtonItem = customBarItem;
    _cameFromNotifications = NO;
}

- (void) setNavigationBarRightButtons:(NSArray <UIButton *>*)buttons{
    
    NSMutableArray *rightBarButtonItems = [NSMutableArray array];
    for (UIButton *button in buttons)
    {
        UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        [rightBarButtonItems addObject:customBarItem];
    }
    
    [self.topViewController.navigationItem setRightBarButtonItems:rightBarButtonItems];
    _cameFromNotifications = NO;
}

- (void)setNavigationBarRightButtonNextTarget:(id)target
                                       action:(SEL)selector {
    [self setNavigationBarRightButtonTarget:target
                                      title:@"NEXT"
                                     action:selector];
}

- (void)setNavigationBarRightButtonDoneTarget:(id)target
                                       action:(SEL)selector {
    [self setNavigationBarRightButtonTarget:target
                                      title:@"DONE"
                                     action:selector];
}

- (void)setNavigationBarRightButtonTarget:(id)target
                                    title:(NSString *)title
                                   action:(SEL)selector {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.titleLabel.font = kAppFontRegular(16.0);
    
    [button setTitle:title
            forState:UIControlStateNormal];
    [button sizeToFit];
    button.frame = CGRectMake(15, 37, button.frame.size.width, button.frame.size.height);
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    if (selector == @selector(next)) {
        button.accessibilityLabel = NAV_BUTTON_LABEL_NEXT;
    }
    else if (selector == @selector(rotateCamera)) {
        button.accessibilityLabel = @"recorder_button_switchCamera";
    }
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.topViewController.navigationItem.rightBarButtonItem = customBarItem;
    _cameFromNotifications = NO;
}

- (void) setNavigationBarLeftButtonMenu{
    [self setNavigationBarLeftButtonImage:[UIImage imageNamed:@"navbar_navigation_icon.png"] action:@selector(openMainMenu)] ;
    _cameFromNotifications = NO;
}

- (void) setNavigationBarLeftButtonBack{
    [self setNavigationBarLeftButtonImage:[UIImage imageNamed:@"back.png"] action:@selector(back)] ;
    _cameFromNotifications = NO;
}

- (void) setNavigationBarLeftButtonBackFromMenu{
    [self setNavigationBarLeftButtonImage:[UIImage imageNamed:@"navbar_navigation_close_arrow_icon.png"] action:@selector(backFromMenu)] ;
    _cameFromNotifications = NO;
}

- (void) setNavigationBarLeftButtonBackForVideoHome{
    [self setNavigationBarLeftButtonImage:[UIImage imageNamed:@"back.png"] action:nil] ;
    _cameFromNotifications = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self setNavigationBarLeftButtonImage:[UIImage imageNamed:@"back.png"] action:@selector(back)];
    });
}

- (void)setNavigationLeftButtonXImageBackAction {
    [self setNavigationBarLeftButtonImage:[UIImage imageNamed:@"close.png"] action:@selector(back)];
}

- (void) setNavigationBarLeftButtonXForNotifications {
    [self setNavigationBarLeftButtonImage:[UIImage imageNamed:@"close.png"] action:@selector(xButton)] ;
    _cameFromNotifications = YES;
}


- (void) setNavigationBarLeftButtonXActionBack{
    [self setNavigationBarLeftButtonImage:[UIImage imageNamed:@"close.png"] action:@selector(back)] ;
    _cameFromNotifications = NO;
}

- (void) setNavigationBarLeftButtonXWithAction:(SEL)selector{
    UIImage *buttonImage = [UIImage imageNamed:@"close.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.accessibilityLabel = NAV_BUTTON_LABEL_BACK_CLOSE;
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(15, 37, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self.topViewController action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.topViewController.navigationItem.leftBarButtonItem = customBarItem;
    _cameFromNotifications = NO;
}

- (void) setNavigationBarLeftButtonBackWithAction:(SEL)selector{
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.accessibilityLabel = NAV_BUTTON_LABEL_BACK_CLOSE;
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(15, 37, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self.topViewController action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.topViewController.navigationItem.leftBarButtonItem = customBarItem;
    _cameFromNotifications = NO;
}

- (void) setNavigationBarLeftButtonForEditProfile:(SEL)selector target:(id)target {
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.accessibilityLabel = NAV_BUTTON_LABEL_BACK_CLOSE;
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(15, 37, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.topViewController.navigationItem.leftBarButtonItem = customBarItem;
    _cameFromNotifications = NO;
}



- (void)clearNavigationItemLeftButton{
    self.topViewController.navigationItem.leftBarButtonItem = nil;
    _cameFromNotifications = NO;
}


//- (void) setNavigationBarLeftButtonTarget:(id)targetObject action:(SEL)buttonAction{
//    UIButton *button = (UIButton *)self.topViewController.navigationItem.leftBarButtonItem.customView;
//    [button addTarget:targetObject action:buttonAction forControlEvents:UIControlEventTouchUpInside];
//}

-(void)back{
    
    //[self flurryBackEventForClassString:NSStringFromClass(self.topViewController.class)];
    if ([self.topViewController respondsToSelector:@selector(backEvent)]) {
        [self.topViewController performSelector:@selector(backEvent)];
    }
    if (self.shouldPopRight) {
        [self reversePopViewController];
    }
    else {
        [self popViewControllerAnimated:YES];
    }
}

-(void)reversePopViewController {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    
    [self.view.layer addAnimation:transition forKey:kCATransition];
    [self popViewControllerAnimated:NO];
    self.shouldPopRight = NO;
}

-(void)xButton{
    if (_cameFromNotifications) {
        //AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        //appDelegate.cameFromNotifications = YES;
        //[Flurry logEvent:FLURRY_TAP_BACK_NOTIFICATIONS];
    }
    [self popToRootViewControllerAnimated:YES];
}

- (void)clearNavigationBarImage{
    [_navBarImage removeFromSuperview];
    _navBarImage = nil;
}

- (void)setNavigationBarImage:(UIImage *)theImage{
    if (theImage) {
        _navBarImage = [[UIImageView alloc] initWithFrame:CGRectMake(NAV_BAR_IMAGE_POS_X, NAV_BAR_IMAGE_POS_Y, theImage.size.width, theImage.size.height)];
        _navBarImage.image = theImage;
        
        [self.navigationBar setShadowImage:[[UIImage alloc] init]];
        [self.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
        [self.navigationBar addSubview:_navBarImage];
    }
}

- (void)setNavigationItemTitle:(NSString *)newTitle{
    self.topViewController.navigationItem.title = newTitle;
    NSDictionary *titleAttributes = @{NSFontAttributeName: [UIFont fontWithName:@"Roboto-Medium" size:18.], NSForegroundColorAttributeName: [UIColor whiteColor]};
    [self.navigationBar setTitleTextAttributes: titleAttributes];
}

- (void)setTitleToTruncateMiddle {
    NSMutableDictionary *attrs = self.navigationBar.titleTextAttributes.mutableCopy;
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineBreakMode = NSLineBreakByTruncatingMiddle;
    [attrs addEntriesFromDictionary:@{NSParagraphStyleAttributeName : style}];
    [self.navigationBar setTitleTextAttributes:attrs];
}

- (void)setNavigationItemRightButtonTitle:(NSString *)newTitle target:(id)targetObject action:(SEL)buttonAction{
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:newTitle style:UIBarButtonItemStylePlain target:targetObject action:buttonAction];
    rightButton.width = 30.;
    [rightButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor],  NSForegroundColorAttributeName ,kAppFontRegular(16.0), NSFontAttributeName,nil] forState:UIControlStateNormal];
    
    // for 15px padding
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spacer.width = 10;
    self.topViewController.navigationItem.rightBarButtonItems = @[spacer, rightButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    self.tabViewControllers = [NSMutableArray array];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}


-(void)switchToViewController:(UIViewController *)newViewController{
    
    /*if ([self.topViewController isKindOfClass:[SIANewMenuViewController class]] || [self.topViewController isKindOfClass:[SIAHomeViewController class]]) {
        
        [self pushViewController:newViewController animated:YES];
    }
    else {
        [self pushViewController:newViewController animated:NO];
    }*/
}

-(void)switchToHomeViewController {
    [self popToRootViewControllerAnimated:NO];
}

- (void)switchToProfileViewController{
    //[self switchToViewController:[[UIStoryboard storyboardWithName:PROFILE_STORYBOARD_NAME bundle:nil] instantiateInitialViewController]];
}

- (void) switchToProfileViewControllerWithUserId:(NSString*) userId {
    if ([self.topViewController isKindOfClass:[ProfileViewController class]] && [((ProfileViewController *)self.topViewController).userId isEqualToString:userId]) {
        return;
    }
    ProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.userId = userId;
    [self pushViewController:vc
                    animated:YES];
}

- (void)switchToVideoHomeForVideoId:(NSString *) videoId {
    [self switchToVideoHomeForVideoId:videoId
                      fromCameoInvite:NO
                         showComments:NO];
}

- (void)switchToVideoHomeFromCameoInviteForVideoId:(NSString *)videoId {
    [self switchToVideoHomeForVideoId:videoId
                      fromCameoInvite:YES
                         showComments:NO];
}

- (void)switchToVideoHomeForVideoId:(NSString *)videoId
                       showComments:(BOOL)showComments {
    [self switchToVideoHomeForVideoId:videoId
                      fromCameoInvite:NO
                         showComments:showComments];
}

- (void)switchToVideoHomeForVideoId:(NSString *)videoId
                    fromCameoInvite:(BOOL)CameoInvited
                       showComments:(BOOL)showComments {
    if ([self.topViewController isKindOfClass:[VideoHomeViewController class]] && [((VideoHomeViewController *)self.topViewController).videoId isEqualToString:videoId]) {
        return;
    }
    VideoHomeViewController* videoHomeVC = [[UIStoryboard storyboardWithName:@"VideoHomeStoryboard" bundle:nil] instantiateInitialViewController];
    videoHomeVC.videoId = videoId;
    videoHomeVC.setForCameoInvite = CameoInvited;
    NSMutableArray *viewControllers = self.viewControllers.mutableCopy;
    [viewControllers addObject:videoHomeVC];
    if (showComments)
    {
        VideoHomeListsViewController *videoListsVC = [[UIStoryboard storyboardWithName:@"VideoHomeStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"videoLists"];
        videoListsVC.videoId = videoId;
        videoListsVC.tab = videoListsVC.kCommentTab;
        [viewControllers addObject:videoListsVC];
    }
    [self setViewControllers:viewControllers
                    animated:YES];
    
}

- (void) switchToInvite {
    if ([self.topViewController isKindOfClass:[InviteContainerViewController class]]) {
        return;
    }
    
    InviteContainerViewController* inviteVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"inviteContainerViewController"];
    
    NSMutableArray *viewControllers = self.viewControllers.mutableCopy;
    [viewControllers addObject:inviteVC];
    [self setViewControllers:viewControllers
                    animated:YES];
}

- (void) switchToLeaderboard {
    if ([self.topViewController isKindOfClass:[LeaderboardViewController class]]) {
        return;
    }
    
    LeaderboardViewController *leaderboardVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]
                                                instantiateViewControllerWithIdentifier:@"leaderboard"];
    [self pushViewController:leaderboardVC
                    animated:YES];
}


- (void)switchToDiscoverForVideoId:(NSString *)videoId {
    if ([self.topViewController isKindOfClass:[DiscoverViewController class]] && [((DiscoverViewController *)self.topViewController).videoID isEqualToString:videoId]) {
        return;
    }
    DiscoverViewController* discoverVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"discoverViewController"];
    discoverVC.videoID = videoId;
    NSMutableArray *viewControllers = self.viewControllers.mutableCopy;
    [viewControllers addObject:discoverVC];
    [self setViewControllers:viewControllers
                    animated:YES];
}

- (void)switchToDiscoverForClipId:(NSString *)clipId {
    if ([self.topViewController isKindOfClass:[DiscoverViewController class]] && [((DiscoverViewController *)self.topViewController).clipID isEqualToString:clipId]) {
        return;
    }
    DiscoverViewController* discoverVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"discoverViewController"];
    discoverVC.clipID = clipId;
    NSMutableArray *viewControllers = self.viewControllers.mutableCopy;
    [viewControllers addObject:discoverVC];
    [self setViewControllers:viewControllers
                    animated:YES];
}

- (void)switchToRecordWithSearchTerm:(NSString *)search {
    SIAChooseVideoViewController* chooseVC = [[UIStoryboard storyboardWithName:@"BlingStoryboard" bundle:nil] instantiateInitialViewController];
    chooseVC.search = search;
    NSMutableArray *viewControllers = self.viewControllers.mutableCopy;
    [viewControllers addObject:chooseVC];
    [self setViewControllers:viewControllers
                    animated:YES];

}

#pragma MARK Bottom Bar Appearance

- (void)setBottomBarHidden:(BOOL)hidden
                  animated:(BOOL)animated {
    
    self.bottomBarYPosConstraint.constant = hidden ? self.bottomBar.bounds.size.height : 0;
    CGFloat alpha = hidden ? 0 : 1;
    _bottomBarIsHidden = hidden;
    [UIView animateWithDuration:animated ? 0.25 : 0
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.view layoutIfNeeded];
                         self.bottomBar.alpha = alpha;
                     } completion:^(BOOL finished) {
                     }];
}

- (BOOL)bottomBarIsHidden {
    return self.bottomBarIsHidden;
}



/* Adds the bottom bar as a subview NOTE: Bottom bar is set to hidden by default.
 */
- (void)addBottomBarToView {

    [self.view addSubview:self.bottomBar];
    // constrain to left, bottom, and right sides. Also width.
    [self.bottomBar.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor].active = YES;
    [self.bottomBar.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor].active = YES;
    [self.bottomBar.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
    self.bottomBarYPosConstraint = [self.bottomBar.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor];
    self.bottomBarYPosConstraint.active = YES;
    
}

#pragma MARK Bottom Bar Button Actions
- (IBAction)bottomBarButtonWasTapped:(BottomBarButtonView*)sender {
    [self clearBottomButtonSelection];
    sender.selected = YES;
    if (sender == _bottomBarHomeButton) {
        [self bottomBarHomeButtonWasTapped: sender];
    } else if (sender == _bottomBarDiscoverButton) {
        [self bottomBarDiscoverButtonWasTapped: sender];
    } else if (sender == _bottomBarRecordButton) {
        [self bottomBarRecordButtonWasTapped: sender];
    } else if (sender == _bottomBarNotificationsButton) {
        [self bottomBarNotificationsButtonWasTapped: sender];
    } else if (sender == _bottomBarProfileButton) {
        [self bottomBarProfileButtonWasTapped: sender];
    }
}

- (void)bottomBarHomeButtonWasTapped:(BottomBarButtonView *)sender {
    
}
- (void)bottomBarDiscoverButtonWasTapped:(BottomBarButtonView *)sender {
}
- (void)bottomBarRecordButtonWasTapped:(BottomBarButtonView *)sender {
}
- (void)bottomBarNotificationsButtonWasTapped:(BottomBarButtonView *)sender {
}
- (void)bottomBarProfileButtonWasTapped:(BottomBarButtonView *)sender {
}

- (void)setSelected:(BOOL)selected forBottomButton:(BottomBarButtonType)type {
    BottomBarButtonView *button = [self getButtonForType:type];
    if (button.isSelected == selected) {
        return;
    }
    
    [self clearBottomButtonSelection];
    button.selected = selected;
    button.enabled = false;
}

- (BottomBarButtonView*)getButtonForType: (BottomBarButtonType) type {
    BottomBarButtonView *button;
    switch (type) {
        case BottomBarButtonTypeHomeButton:
            button = self.bottomBarHomeButton;
            break;
        case BottomBarButtonTypeDiscoverButton:
            button = self.bottomBarDiscoverButton;
            break;
        case BottomBarButtonTypeRecordButton:
            button = self.bottomBarRecordButton;
            break;
        case BottomBarButtonTypeNotificationsButton:
            button =self.bottomBarNotificationsButton;
            break;
        case BottomBarButtonTypeProfileButton:
            button = self.bottomBarProfileButton;
            break;
        default:
            break;
    }

    return button;
}

- (void) clearBottomButtonSelection {
    for (BottomBarButtonView *butt in self.bottomBarButtons) {
        butt.selected = false;
        butt.enabled = true;
    }
}

- (void) setNumberOfNotifications: (NSUInteger *)number {
    
}
@end
