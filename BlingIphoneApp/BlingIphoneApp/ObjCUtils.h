//
//  ObjCUtils.h
//  BlingIphoneApp
//
//  Created by Michael Biehl on 3/14/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjCUtils : NSObject

+ (BOOL)catchException:(void(^)())tryBlock error:(__autoreleasing NSError **)error;

@end
