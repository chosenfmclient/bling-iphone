//
//  SIALoginManager.m
//  BlingIphoneApp
//
//  Created by Zach on 15/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import "BIALoginManager.h"
#import "SIAEmailRegistrationdataModel.h"
#import "KeychainWrapper.h"
#import "SIANetworking.h"
#import "SIAObjectManager.h"
#import "SIAApnSettingsDataModel.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Flurry_iOS_SDK/Flurry.h>
#import <Appsee/Appsee.h>
@import Amplitude_iOS;

@interface BIALoginManager()
@property (strong, nonatomic) NSMutableArray* handlersArray;
@property (nonatomic) BOOL fetchingLikes;
@property (nonatomic) BOOL fetchingFollows;
@end

@implementation BIALoginManager
NSString* const kEmailTokenApi = @"api/v1/token";
NSString* const kAccessTokenExpireKey = @"expires_in";
NSString* const kFBPublicProfile = @"public_profile";
NSString* const kFBEmailPermission = @"email";
NSString* const kFBUserFriendsPermission = @"user_friends";

+ (instancetype) sharedInstance
{
    static BIALoginManager *loginManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        loginManager = [[BIALoginManager alloc] init];
    });
    return loginManager;
}

- (void) refreshAccessTokenForCompletion: (void(^)(BOOL success))completion  {
    if (_isRefreshingToken) {
        [_handlersArray addObject:completion];
        return;
    }
    self.handlersArray = [[NSMutableArray alloc] init];
    _isRefreshingToken = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    NSString *loginType = [[NSUserDefaults standardUserDefaults] objectForKey:kLoginType];
    
    if (!loginType) {
        loginType = kEmailLoginType;
    }
    
    if ([loginType isEqualToString:kFacebookLoginType]) {
        [FBSDKAccessToken refreshCurrentAccessToken:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            NSSet *permissions = [FBSDKAccessToken currentAccessToken].permissions;
            NSLog(@"permissions: %@", permissions);
            
            if ([permissions containsObject:kFBPublicProfile]) {
                _isRefreshingToken = NO;
                [defaults setObject:[FBSDKAccessToken currentAccessToken].tokenString forKey:@"access_token"];
                completion(YES);
                for (int i=0; i < _handlersArray.count; i++) {
                    ((void (^)(BOOL result)) _handlersArray[i])(YES);
                }
            } else {
                FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                [login logOut];
                [Utils dispatchOnMainThread:^{
                    UIViewController *vc = ((AppDelegate *)[UIApplication sharedApplication].delegate).rootNavController.topViewController;
                    [login
                     logInWithReadPermissions: @[kFBPublicProfile, kFBEmailPermission]
                     fromViewController:vc
                     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                         if (result.isCancelled) {
                             //what does this mean?
                         }
                         
                         if (error){
                             //what does this mean?
                         } else {
                             _isRefreshingToken = NO;
                             if ([result.grantedPermissions containsObject:kFBPublicProfile]) {
                                 [defaults setObject:[FBSDKAccessToken currentAccessToken].tokenString forKey:@"access_token"];
                                 completion(YES);
                                 for (int i=0; i < _handlersArray.count; i++) {
                                     ((void (^)(BOOL result)) _handlersArray[i])(YES);
                                 }
                                 
                             } else {
                                 //what does this mean?
                                 // what does "what does this mean" mean?
                             }
                         }
                     }];
                }];
            }
        }];
    } else {
        // email registerd user
        SIAEmailRegistrationdataModel* dataModel = [[SIAEmailRegistrationdataModel alloc] init];
        dataModel.app_id = APP_ID;
        dataModel.secret = SECRET;

        KeychainWrapper *keychainItem = [[KeychainWrapper alloc] init];
        dataModel.email = [keychainItem myObjectForKey:(__bridge id)(kSecAttrAccount)];
        dataModel.password = [keychainItem myObjectForKey:(__bridge id)(kSecValueData)];
        if (!dataModel.email || !dataModel.password)
        {    // no email or password in keychain - show connect view
            //TODO: show connect view
            //                [((BIANavigationController *)self.window.rootViewController.navigationController) showConnectView];
            return;
        }
        
        
        [RequestManager
         postRequest:kEmailTokenApi
         object:dataModel
         queryParameters:nil
         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
             // store user data
             NSDictionary *data = mappingResult.firstObject;
             NSString *access_token = [[data objectForKey:@"access_token"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
             NSString *user_id  = [data objectForKey:@"user_id"];
             NSLog(@"access_token: %@",access_token);
             NSDate *currentDate = [NSDate date];
             NSDate *expireDate = [currentDate dateByAddingTimeInterval:[((NSString *)[data objectForKey:kAccessTokenExpireKey]) doubleValue]];
             [defaults setObject:user_id forKey:kChosenUserIdKeyStr];
             [defaults setObject:access_token forKey:kFBAccessTokenKeyStr];
             [defaults setObject:expireDate forKey:kAccessTokenDate];
             
             _isRefreshingToken = NO;
             
             //                              if (self.tuneShouldMeasure){
             //                                  NSString *userIdForTracking = [[NSUserDefaults standardUserDefaults] objectForKey:kChosenUserIdKeyStr];
             //                                  [Tune setUserId:userIdForTracking];
             //                                  [Flurry setUserID:userIdForTracking];
             //                                  [Tune measureSession];
             //                              }
             //
             completion(YES);
             for (int i=0; i < _handlersArray.count; i++) {
                 ((void (^)(BOOL result)) _handlersArray[i])(YES);
             }
         }
         failure:^(RKObjectRequestOperation *operation, NSError *error) {
             _isRefreshingToken = NO;
             completion(NO);
             for (int i=0; i < _handlersArray.count; i++) {
                 ((void (^)(BOOL result)) _handlersArray[i])(NO);
             }
             
         }
         showFailureDialog:YES];
    }
}

-(void) FBLoginFromVC:(UIViewController *)vc
          withHandler:(void (^)(BOOL)) handler {
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    if ([[FBSDKAccessToken currentAccessToken].permissions containsObject:FB_PUBLIC_PROFILE_PERMISSION_STR] && [[FBSDKAccessToken currentAccessToken].permissions containsObject:FB_EMAIL_PERMISSION_STR]) {
        [[NSUserDefaults standardUserDefaults] setObject:[FBSDKAccessToken currentAccessToken].tokenString forKey:@"access_token"];
        [[NSUserDefaults standardUserDefaults] setObject:kFacebookLoginType forKey:kLoginType];
        handler(YES);
        return;
    }
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithReadPermissions:@[FB_PUBLIC_PROFILE_PERMISSION_STR,
                                      FB_EMAIL_PERMISSION_STR]
                 fromViewController:vc
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (result.isCancelled) {
                                    handler(NO);
                                } else {
                                    if ([result.grantedPermissions containsObject:FB_PUBLIC_PROFILE_PERMISSION_STR]) {
                                        [[NSUserDefaults standardUserDefaults] setObject:[FBSDKAccessToken currentAccessToken].tokenString forKey:@"access_token"];
                                        [[NSUserDefaults standardUserDefaults] setObject:kFacebookLoginType forKey:kLoginType];
                                        handler(YES);
                                        AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                        [appDel userDidLogin];
                                        [Utils dispatchAsync:^{
                                            [SIAUser getUserWithId:nil completionHandler:^(SIAUser *user) {
                                                self.myUser = user;
                                            }];
                                        }];
                                    } else {
                                        handler(NO);
                                    }
                                }
                            }];
}

- (void) createEmailUserWithEmail:(NSString*) email
                         password:(NSString*) password
                        firstName:(NSString*) firstName
                         lastName:(NSString*) lastName
                completionHandler:(void (^)(BOOL, NSString*)) completion {
    SIAEmailRegistrationdataModel* dataModel = [[SIAEmailRegistrationdataModel alloc] init];
    dataModel.app_id = APP_ID;
    dataModel.secret = SECRET;
    dataModel.email = email;
    dataModel.password = password;
    dataModel.first_name = firstName;
    dataModel.last_name = lastName;
    
    [self validateDataModel:dataModel withHandler:^(BOOL valid, NSString *explanation) {
        if (!valid) {
            completion(NO, explanation);
        } else {
            __weak BIALoginManager* wSelf = self;
            [RequestManager
             postRequest:@"api/v1/signup"
             object:dataModel
             queryParameters:nil
             success:^(RKObjectRequestOperation * _Nullable request, RKMappingResult * _Nullable result) {
                 completion(YES, nil);
                 [[NSUserDefaults standardUserDefaults] setObject:kEmailLoginType forKey:kLoginType];
                 AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                 [appDel userDidLogin];
                 [Utils dispatchAsync:^{
                     [SIAUser getUserWithId:nil completionHandler:^(SIAUser *user) {
                         wSelf.myUser = user;
                     }];
                 }];

             }
             failure:^(RKObjectRequestOperation * _Nullable request, NSError * _Nullable error) {
                 completion(NO, nil);
             }
             showFailureDialog:YES];
        }
    }];
}

- (void) sendNewPassword:(NSString*) email
                completionHandler:(void (^)(BOOL, NSString*)) completion {
    SIAEmailRegistrationdataModel* dataModel = [[SIAEmailRegistrationdataModel alloc] init];
    dataModel.app_id = APP_ID;
    dataModel.secret = SECRET;
    dataModel.email = email;
    

    [RequestManager
     postRequest:@"api/v1/password"
     object:dataModel
     queryParameters:nil
     success:^(RKObjectRequestOperation * _Nullable request, RKMappingResult * _Nullable result) {
         completion(YES, nil);
     }
     failure:^(RKObjectRequestOperation * _Nullable request, NSError * _Nullable error) {
         NSInteger code = request.HTTPRequestOperation.response.statusCode;
         NSString* explanation = nil;
         switch (code) {
             case 404:
                 explanation = @"There is no account associated with that email";
                 break;
             case 400:
                 explanation = @"This email is not valid";
                 break;
             default:
                 break;
         }
         completion(NO, explanation);
     }
     showFailureDialog: NO];
    
}

- (void) validateDataModel: (SIAEmailRegistrationdataModel*) dataModel
               withHandler: (void (^)(BOOL, NSString*)) handler
{
    [RequestManager
     postRequest:@"api/v1/email"
     object:dataModel queryParameters:nil
     success:^(RKObjectRequestOperation * _Nullable request, RKMappingResult * _Nullable result) {
         handler(YES, nil);
     }
     failure:^(RKObjectRequestOperation * _Nullable request, NSError * _Nullable error) {
         NSString *jsonString = [error localizedRecoverySuggestion];
         NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
         if (data) {
             
             id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
             NSDictionary *type = [json objectForKey:@"email"];
             
             if ([type objectForKey:@"recordFound"] && ![type objectForKey:@"emailAddressInvalidFormat"])
             {     // used email
                 handler(NO, @"This email is already in use.");
             }
             else
             {   // invalid email
                 handler(NO, @"This email is not valid.");
             }
         }
     }
     showFailureDialog: NO];
}

- (void) setApnSettingsWithDeviceToken:(NSData *)deviceToken {
    [RequestManager putRequest:@"api/v1/apns-settings"
                        object:[SIAApnSettingsDataModel modelWithDeviceToken:deviceToken]
               queryParameters:nil
                       success:^(RKObjectRequestOperation * _Nullable operation, RKMappingResult * _Nullable result) {
                           NSLog(@"Apns success");
                       } failure:^(RKObjectRequestOperation * _Nullable operation, NSError * _Nullable error) {
                           NSLog(@"APNS FAIL");
                       } showFailureDialog:NO];
    
}

+ (BOOL) userIsRegistered {
    return [[NSUserDefaults standardUserDefaults] objectForKey:kLoginType] != nil;
}

+ (BOOL) fbAccessTokenDisappeared {
    NSString *loginType = [[NSUserDefaults standardUserDefaults] objectForKey:kLoginType];
    return [loginType isEqualToString:kFacebookLoginType] && [FBSDKAccessToken currentAccessToken] == nil;
}

- (void) setMyUser:(SIAUser *)myUser {
    if (!myUser) {
        return;
    }
    _myUser = myUser.copy;
    [Flurry setUserID:myUser.user_id];
    [Appsee setUserID:myUser.user_id];
    [[Amplitude instance] setUserId:myUser.user_id];
    
    [[NSUserDefaults standardUserDefaults] setObject:myUser.user_id forKey:kUserIdKeyStr];
}

+ (NSString*) myUserId {
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:kUserIdKeyStr];
    if (!userId) {
        userId = @"0";
    }
    return userId;
}

- (void) fetchLikesFromServer {
    NSNumber *didFetchLikes = [[NSUserDefaults standardUserDefaults] objectForKey:@"didFetchLikes"];
    if (didFetchLikes.boolValue || self.fetchingLikes)
        return;
    
    _fetchingLikes = YES;
    [RequestManager getPrimitiveObjectAt:@"api/v1/users/me/likes"
                         queryParameters:nil
                                 success:^(AFRKHTTPRequestOperation * _Nullable operation, id _Nullable result) {
                                     if (result) {
                                         [CoreDataManager.shared loadLikesFor:result];
                                     }
                                     _fetchingLikes = NO;
                                     [[NSUserDefaults standardUserDefaults] setObject:@(YES)
                                                                               forKey:@"didFetchLikes"];
                                 } failure:^(AFRKHTTPRequestOperation * _Nullable operation, NSError * _Nullable error) {
                                     _fetchingLikes = NO;
                                 }];
}

- (void) fetchFollowsFromServer {
    NSNumber *didFetchFollows = [[NSUserDefaults standardUserDefaults] objectForKey:@"didFetchFollows"];
    if (didFetchFollows.boolValue || self.fetchingFollows)
        return;
    
    _fetchingFollows = YES;
    [RequestManager getPrimitiveObjectAt:@"api/v1/following"
                         queryParameters:nil
                                 success:^(AFRKHTTPRequestOperation * _Nullable operation, id _Nullable result) {
                                     if (result) {
                                         [CoreDataManager.shared loadFollowsFor:result];
                                     }
                                     _fetchingFollows = NO;
                                     [[NSUserDefaults standardUserDefaults] setObject:@(YES)
                                                                               forKey:@"didFetchFollows"];
                                 } failure:^(AFRKHTTPRequestOperation * _Nullable operation, NSError * _Nullable error) {
                                     _fetchingFollows = NO;
                                 }];
}
@end

