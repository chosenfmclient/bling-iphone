//
//  SIAShare.h
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 13/07/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "SIAShareMapping.h"
#import "SIAVideo.h"
#import "SIAInstagramVideoEditViewController.h"
#import "SIAUser.h"

typedef void(^myCompletion)(BOOL);

@interface SIANativeVideo : NSObject

@property (nonatomic, strong) NSString *video_id;
@property (nonatomic, strong) NSString *facebook_token;
@property (nonatomic, strong) NSString *text;

@end

@interface SIAShare : NSObject <UIAlertViewDelegate>

@property (nonatomic, strong) UIViewController *viewController;
@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, strong) NSString *initialText;
@property (nonatomic, strong) SIAVideo *video;
@property (nonatomic, strong) SIAUser *user;
@property (nonatomic, strong) NSString *gameId;
@property (nonatomic, strong) NSString *gameName;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *badgeId;
@property (nonatomic, strong) NSString *badgeName;
@property (nonatomic, strong) NSString *share_id;
@property (nonatomic, strong) NSString *object_id;
@property (nonatomic, strong) NSString *shareType;
@property (nonatomic, strong) NSString *shareURL;
@property (nonatomic, strong) NSString *shareURLWithToken;
@property (nonatomic, strong) NSString *branchURL;
@property (nonatomic, strong) NSString *platform;
@property (nonatomic, strong) NSURL *compositedAssetURL;
// Instagram Stuff
@property (nonatomic, strong) NSURL *assetURL;
@property (nonatomic) CMTimeRange exportTimeRange;
@property (weak, nonatomic) BIAVideoPlayer *effectPlayer;
@property (strong, nonatomic) UIImage *image;

@property (nonatomic, strong) UIImage *shareImage;
@property (nonatomic) NSUInteger interfaceOrientationForTwitterComposer;
@property (nonatomic) bool  native_video;
@property (nonatomic) BOOL isMyVideo;
@property (nonatomic) BOOL isMyProfile;
@property (nonatomic) BOOL postVideoToFacebook;
@property (nonatomic) BOOL postDismissed;
@property (nonatomic) BOOL isRecordingShare;
@property (nonatomic) BOOL isReviewRecordingShare;
@property (nonatomic) BOOL assetHasNoRecordedAudio;
@property (strong, nonatomic) NSString *shareText;
@property (nonatomic) BOOL lockOrientation;
@property (strong, nonatomic) UIView *recordingPreviewHeaderView;
@property (strong, nonatomic) UIView *videoHomeScrubberView;
@property (nonatomic) BOOL shareToEllenTube;
@property (nonatomic, strong) NSString *shareTags;
@property (nonatomic, strong) NSString *starterShareText;
@property (nonatomic, strong) NSString *mailSubject;

@property (nonatomic, strong) AVAsset *asset;

@property (strong, nonatomic) NSData *photoData;


-(void)shareOnFBWithCompletionBlock:(myCompletion)completed;
-(void)shareOnTwitterWithCompletionBlock:(myCompletion)completed;
-(void)shareByMailWithCompletionBlock:(myCompletion)completed;
-(void)shareBySMSWithCompletionBlock:(myCompletion)completed;
- (void)shareOnPlatformWithoutCameraRoll:(NSString *)platform
                          withCompletion:(myCompletion)completion;
- (void)shareOnPlatform:(NSString *)platform
              withAsset: (AVAsset *)asset
         withCompletion:(myCompletion)completion;
- (void)shareonPlatform:(NSString *)platform
         withCompletion:(myCompletion)completion;
-(void)shareOnInstagramWithCompletionBlock:(myCompletion)completed;
-(void)shareByCopyLinkWithCompletionBlock:(myCompletion)completed;
-(void)shareByOther: (UICollectionViewCell*)cell;

-(void) publishVideoToFacebook;
- (void)uploadVideoToFacebookAfterProcessingFinished:(NSURL*)localURL;
- (void) checkIfVideoIsReadyToShare;
- (void) composePost;
+(RKObjectMapping *)shareMapping;
@end
