//
//  SIATermAndConditionsViewController.m
//  ChosenIphoneApp
//
//  Created by Roni Shoham on 19/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIATermAndConditionsViewController.h"

@interface SIATermAndConditionsViewController ( )
@end

@implementation SIATermAndConditionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIFont *font = [UIFont fontWithName:@"Roboto-Regular" size:12.0f];
    _termsText.font = font;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationItemRightButtonTitle:NSLocalizedString(@"Print", nil) target:self action:@selector(print)];

    // Text styling
//    NSString *text = _termsText.text;
//
//    // Defining attributes for the whole text
//    NSDictionary *textAtrributes = @{ NSFontAttributeName : [UIFont fontWithName:@"Roboto-Regular" size:12.0f],
//        NSForegroundColorAttributeName : [UIColor colorWithRed:44.0f / 255.0f green:48.0f / 255.0f blue:63.0f / 255.0f alpha:1] };
//    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:textAtrributes];
//
    // Defining attributes for bold text
//    NSDictionary *boldTextAttributes = @{ NSFontAttributeName : [UIFont fontWithName:@"Roboto-Bold" size:12.0f] };
//    NSArray *boldText = @[ @"Site", @"Services", @"Blingy.fm Account", @"Third Party Materials", @"Take Down Notifications", @"Counter-Notice", @"Repeat Infringers" ];
//    for(id str in boldText)
//        [string addAttributes:boldTextAttributes range:[text rangeOfString:str]];

    // Defining attributes for the subtiltes
//    NSDictionary *subtitleAttributes = @{ NSFontAttributeName : [UIFont fontWithName:@"Roboto-Bold" size:12.0f],
//        NSUnderlineStyleAttributeName : [NSNumber numberWithInt:NSUnderlineStyleSingle] };
//    NSArray *subtitles = @[ @"Eligibility and Access", @"Third Party Content and Other Materials", @"Privacy", @"Ownership", @"User Content", @"Copyright Policy", @"General Prohibitions", @"Termination", @"Disclaimers", @"Indemnity", @"Limitation of Liability", @"Proprietary Rights Notices", @"Notices.", @"Governing Law", @"No Assignment", @"Entire Agreement, Modification, Waiver." ];
//    for(id str in subtitles)
//        [string addAttributes:subtitleAttributes range:[text rangeOfString:str]];

    // Defining attributes for the e-mail adresses
//    NSDictionary *adressAttributes = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:35.0f / 255.0f green:130.0f / 255.0f blue:186.0f / 255.0f alpha:1] };
//    NSArray *adresses = @[ @"copyrights@chosen.fm" ];
//    for(id str in adresses)
//        [string addAttributes:adressAttributes range:[text rangeOfString:str]];
//
//    _termsText.attributedText = string;
//    _termsText.textAlignment = 0; // left alignment
//
//    _termsText.delegate = self;
//    _termsText.dataDetectorTypes = UIDataDetectorTypeLink;
//
    _termsText.showsVerticalScrollIndicator = NO; // disabling scroll bar
}

- (void)viewDidAppear:(BOOL)animated
{
//    [Flurry logEvent:FLURRY_VIEW_SETTINGS_T_C];
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    return YES;
}

- (void)print
{
    if(![UIPrintInteractionController isPrintingAvailable])
        return;
    

    NSMutableString *printBody = [NSMutableString stringWithFormat:@"%@", _termsText.text];

    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    pic.delegate = self;

    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = @"title: ";
    pic.printInfo = printInfo;

    UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc] initWithText:printBody];
    textFormatter.startPage = 0;
    textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
    textFormatter.maximumContentWidth = 6 * 72.0;
    pic.printFormatter = textFormatter;
    pic.showsPageRange = YES;

    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
        if(!completed && error)
        {
            NSLog(@"Printing could not complete because of error: %@", error);
        }
    };

    [pic presentAnimated:YES completionHandler:completionHandler];
}

- (IBAction)closeWasTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
