    //
//  SIAHorizontalSelectionScrollView.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 5/4/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAHorizontalSelectionScrollView.h"

@interface SIAHorizontalSelectionScrollView ()

@property (nonatomic) SIAHorizontalSelectionScrollViewStyle style;

@end

@implementation SIAHorizontalSelectionScrollView


- (id)initWithFrame:(CGRect)frame
              views:(NSArray <SIASelectableView *> *)views
              style:(SIAHorizontalSelectionScrollViewStyle)style{
    
    self = [super initWithFrame:frame];
    if (self)
    {
        self.style = style;
        self.contentSize = CGSizeMake(0, frame.size.height);
        self.views = views;
    }
    return self;
}

- (void)setViews:(NSArray<SIASelectableView *> *)views
{
    _views = views;
    
    CGSize contentSize = self.contentSize;
    BOOL setBorder = self.style == SIAHorizontalSelectionScrollViewStyleButtonBorderHighlights;

    for (SIASelectableView *view in views)
    {
        [self addSubview:view];
        contentSize.width += view.bounds.size.width;
        if (setBorder)
        {
            view.button.layer.borderWidth = 4.0;
            view.button.layer.borderColor = [[UIColor clearColor] CGColor];
        }
    }
    
    self.contentSize = contentSize;
}

- (void)scrollScrollViewToNextItemForSelectedView:(SIASelectableView *)view
{
    NSUInteger nextViewIdx = [self.views indexOfObject:view] + 1;
    SIASelectableView *nextView;
    SIASelectableView *nextNextView;
    
    if (nextViewIdx < self.views.count) {
        nextView = self.views[nextViewIdx];
    }
    if (nextViewIdx + 1 < self.views.count) {
        nextNextView = self.views[nextViewIdx + 1];
    }
    
    CGRect nextViewFrame = nextView.frame;
    nextViewFrame.origin.x -= self.bounds.origin.x;
    CGRect nextNextViewFrame = nextNextView.frame;
    nextNextViewFrame.origin.x -= self.bounds.origin.x;
    
    if (nextNextViewFrame.origin.x > self.frame.size.width) {
        CGFloat distToScroll = nextNextViewFrame.origin.x - self.frame.size.width;
        [self setContentOffset:CGPointMake(self.contentOffset.x + distToScroll, 0)
                      animated:YES];
    }
    else if (CGRectGetMaxX(nextViewFrame) > self.frame.size.width || nextViewIdx == self.views.count) {
        [self setContentOffset:CGPointMake(self.contentSize.width - self.frame.size.width, 0)
                      animated:YES];
    }
}

- (void)selectButtonAtIndex:(NSUInteger)index {
    
    if (self.views.count <= index) return;
    
    SIASelectableView *view = self.views[index];
    SIASelectableView *nextView = index + 1 < self.views.count ? self.views[index + 1] : nil;
    UIButton *button = view.button;
    
    button.selected = YES;
    [self deselectButtonsExcept:button];
    
    if (CGRectGetMaxX(nextView.frame) > SCREEN_WIDTH)
        [self scrollScrollViewToNextItemForSelectedView:view];
    
    if (self.style == SIAHorizontalSelectionScrollViewStyleButtonBorderHighlights)
        [self setImageButtonBorder:button];
}

- (void)deselectButtonsExcept:(UIButton *)button {
    
    BOOL setBorder = self.style == SIAHorizontalSelectionScrollViewStyleButtonBorderHighlights;
    
    for (SIASelectableView *view in self.views)
    {
        UIButton *buttonToDeselect = view.button;
        if (buttonToDeselect != button)
        {
            buttonToDeselect.selected = NO;
            if (setBorder)
                [self setImageButtonBorder:buttonToDeselect];
        }
    }
}

- (void)setImageButtonBorder:(UIButton *)button {
    
    CABasicAnimation *borderAnimation = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    borderAnimation.duration = 0.1;
    borderAnimation.fillMode = kCAFillModeForwards;
    borderAnimation.removedOnCompletion = NO;
    
    CGColorRef borderColor = button.selected ? [[[SIAStyling defaultStyle] blingMainColor] CGColor] : [[UIColor clearColor]CGColor];
    borderAnimation.toValue = (__bridge id)borderColor;
    
    [CATransaction begin];
    {
        [CATransaction setCompletionBlock:^{
            button.layer.borderColor = borderColor;
            [button.layer removeAllAnimations];
        }];
        [button.layer addAnimation:borderAnimation
                            forKey:@"borderAnimation"];
    }
    [CATransaction commit];
}

@end
