//
//  SIAApnSettingsDataModel.h
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 8/12/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SIAApnSettingsDataModel : NSObject

@property NSString *device_token;
@property NSString *bundle_id;
@property NSString *app_version;

+(SIAApnSettingsDataModel *)modelWithDeviceToken:(NSData *)deviceToken;
+(RKObjectMapping *)requestMapping;
+(RKObjectMapping *)responseMapping;

@end
