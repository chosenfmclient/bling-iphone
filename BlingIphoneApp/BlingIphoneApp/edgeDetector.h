//
// Created by Ben on 27/09/2016.
//

#ifndef BLINGY_OCV_NDK_EDGEDETECTOR_H
#define BLINGY_OCV_NDK_EDGEDETECTOR_H

//#include <android/log.h>

#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <stdlib.h>
#include <stdio.h>

using namespace cv;

typedef struct multiDetectionStructTypeDef
{
    Mat& bg;
    Mat& dss;
    Mat& dst;
    int faceLeft;
    int faceTop;
    int faceRight;
    int faceBottom;
} multiDetectionStruct;



class edgeDetector
{
    public:
//        Mat stam;

        const static int

//              WHITE_PIXEL = 255,
                DISTANCE_FROM_LINE = 40 //8
            , DISTANCE_FROM_LINE_HALF = DISTANCE_FROM_LINE / 2
            , DISTANCE_FROM_LINE_HALF_X_4 = 4 * DISTANCE_FROM_LINE_HALF
            , DISTANCE_FROM_LINE_X_4 = DISTANCE_FROM_LINE * 4
            , DISTANCE_FROM_LINE_HALF_X_3 = 3 * DISTANCE_FROM_LINE_HALF
            , DISTANCE_FROM_LINE_X_3 = DISTANCE_FROM_LINE * 3

            , HUE_BEN_THRESHOLD = 17
            , SATURATION_BEN_THRESHOLD = 17
            , VALUE_BEN_THRESHOLD = 17

//            , HUE_THRESHOLD = 5
//            , SATURATION_THRESHOLD = 10
//            , VALUE_THRESHOLD = 10
    
    , HUE_THRESHOLD =  4 // 1000 //5
    , SATURATION_THRESHOLD =   25 // 1000 //25
    , VALUE_THRESHOLD = 30 //1000 //30


            , SKIN_HUE_THRESHOLD = 10
            , SKIN_SATURATION_THRESHOLD = 30
            , SKIN_VALUE_THRESHOLD = 50
    
            , NEXT_HUE_THRESHOLD = 5
            , NEXT_SATURATION_THRESHOLD = 5
            , NEXT_VALUE_THRESHOLD = 5

        ;

        float skinHueValue, skinSaturationValue, skinValueValue;

        bool foundSkinColor;
    
        bool pointOriginal;
    
        int countTrue, countFalse;

    cv::Size defBlurSize;

        edgeDetector();

        virtual ~edgeDetector();

//        void getEdgesBuffer(Mat bg, Mat dss, Mat dst, unsigned int faceLeft, unsigned int faceTop);//, unsigned int faceRight, unsigned int faceBottom);
        void getEdgesBuffer(Mat bg, Mat dss, Mat dst, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom);

//        void getEdgesBuffer(Mat dss, Mat dst, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom);

//        void *getEdgesBufferThread();//Mat bg, Mat dss, Mat dst, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom, bool booli);

        void fillEdges(uchar* bg, uchar* dss, uchar* mask, uchar* hsv, uchar* edgesBuffer, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom);

        pthread_t startThread(Mat bg, Mat dss, Mat dst, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom);

        /**
         * find the edges in the supplied image
         *
         * src - 32bit
         *
         * dst - 8 bit
         *
         * hsv - 24 bit
         */
        void findBensEdges(uchar* src, uchar* dst, uchar* hsv);

        bool checkScreen(Mat screenMat, float accuracy, float resizeFactor);

    private:

        const static int edgeThresh = 1, lowThreshold = 20, max_lowThreshold = 60;
        const static int ratio = 3, kernel_size = 3;
        const static int laplacianMergingThreshHold = 199;




        void getTopOfHead(uchar* bg, uchar* mask, uchar *hsv, unsigned int faceLeft, unsigned int faceTop, unsigned int *topColumn, unsigned int *topRow);

        void getFirstObject(uchar* bg, uchar* mask, unsigned int *topColumn, unsigned int *topRow);

        void getToTop(uchar* bg, uchar* mask, uchar* hsv, unsigned int column, unsigned int row, bool isGoingRight, unsigned int *topCol, unsigned int *topRow);

        void fillShape(uchar* bg, uchar* dss, uchar* mask, uchar* hsv, uchar* edgesBuffer, unsigned int topCol, unsigned int topRow, unsigned int lastCol, int *lastLeftRow, int *lastRightRow);

        bool isNotInRange(uchar hue1, uchar hue2, uchar sat1, uchar sat2, uchar val1, uchar val2);

//        bool isInRange(uchar hue1, )
    
        int goToSameRangeIndexLeft(uchar* mask, uchar* hsv, unsigned int column, unsigned int row);
    
        int goToSameRangeIndexRight(uchar* mask, uchar* hsv, unsigned int column, unsigned int row);
    
        bool sameHsvRange(uchar* hsv, unsigned int currentIndex, unsigned int lastIndex);

        /**
         * returns the left side edge (left row)
         */
        int findLeftSideRow(uchar* bg, uchar* dss, uchar* mask, uchar* hsv, uchar* edgesBuffer, int leftCol, int leftRow);

        int findRightSideRow(uchar* bg, uchar* dss, uchar* mask, uchar* hsv, uchar* edgesBuffer, int rightCol, int rightRow);
    
        void saveSkinColor(uchar* bg, uchar*  mask, uchar*  hsv, unsigned int faceLeft, unsigned int faceTop, unsigned int faceWidth, unsigned int faceHight);

//        void* getEdgesBufferThread(void *id);

};

#endif //BLINGY_OCV_NDK_HELPER_H
