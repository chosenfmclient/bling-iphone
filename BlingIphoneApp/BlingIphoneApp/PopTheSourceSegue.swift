//
//  PopTheSourceSegue.swift
//  BlingIphoneApp
//
//  Created by Zach on 19/12/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class PopTheSourceSegue: UIStoryboardSegue {
    override func perform() {
        let navigationController = self.source.navigationController
        var vcs = navigationController!.viewControllers
        vcs.removeLast(1)
        vcs.append(destination)
        navigationController?.pushViewController(destination, animated: true)
        navigationController?.viewControllers = vcs
    }
}
