//
//  PushNotificationModel.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 2/22/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

import UIKit
import RestKit
@objc class PushNotificationModel: NSObject {
    
    var type: String
    var notification_type: String
    var notification_id: String
    var mass_notification_id: String
    var object: String
    var object_id: String
    var alert: String
    
    required init(withData data: [String : Any]) {
        self.type = (data["type"] as? String) ?? "NULL"
        self.notification_type = (data["notification_type"] as? String) ?? "NULL"
        self.notification_id = (data["notification_id"] as? String) ?? "NULL"
        self.mass_notification_id = (data["mass_notification_id"] as? String) ?? "NULL"
        self.object = (data["object"] as? String) ?? "NULL"
        self.object_id = (data["object_id"] as? String) ?? "NULL"
        self.alert = (data["alert"] as? String) ?? "NULL"
    }
    
    static func requestMapping() -> RKMapping {
        let mapping = RKObjectMapping.request()
        
        mapping?.addAttributeMappings(from:
            ["type",
             "notification_type",
             "notification_id",
             "mass_notification_id",
             "object",
             "object_id",
             "alert"]
        )
        
        return mapping!
    }

}
