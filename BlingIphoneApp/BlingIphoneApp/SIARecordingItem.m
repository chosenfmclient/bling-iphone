//
//  SIARecordingItem.m
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 09/09/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIARecordingItem.h"

#define UPALOAD_FAILURE_MESSAGE_STR     @"Failed to upload the video's thumbnail. Do you want to retry?"
#define RETRY_BUTTON_INDEX              (1)

@interface SIARecordingItem ()

@property (nonatomic, strong, readwrite) AVPlayer *playbackPlayer;
@property (strong, nonatomic) NSString* thumbName;
@property (strong, nonatomic) UIAlertView *uploadAlert;
@property (strong, readwrite) UIImage *thumbnailImage;

@end

@implementation SIARecordingItem

-(id)init{
    self = [super init];
    if (self)
    {
        self.sholudPlayPlayback = NO;
        self.shouldRecordAudio = YES;
        self.shouldRecordVideo = YES;
        self.thumbName = @"thumb.png";
        self.recordStatus = @"public";
        self.recordingIsReadyForPreview = YES;
    }
    self.uploader = [SIAUploadVideo sharedUploadManager];
    
    return self;
}

- (id) initForRecorder {
    self = [self init];
    if (self)
    {
        self.recordURL = [SIARecordingItem recordURLForRecord];
        self.recordingTime = 600.0f;
        self.recordingSpeedScale = 1.0;
        self.playbackStartOffset = kCMTimeZero;
    }
    
    return self;
}

+ (NSURL *)recordURLForRecord {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"record_1.mov"];
    return [NSURL fileURLWithPath:filePath];
}

- (void) setPlaybackURL:(NSURL *)playbackURL {
    playbackURL = playbackURL;
    if (playbackURL)
        _playbackPlayer = [[AVPlayer alloc] initWithURL:playbackURL];
}

- (void)tearDownPlaybackPlayer {
    _playbackPlayer = nil;
    self.sholudPlayPlayback = NO;
}

//-(AVPlayer *)playbackPlayer{
//    if (!_playbackPlayer && self.playbackURL){
//        _playbackPlayer = [[AVPlayer alloc] initWithURL:self.playbackURL];
//    }
//    return _playbackPlayer;
//}

-(CGFloat)recordingTime{
    if (self.playbackPlayer && [self.recordType isEqualToString:KARAOKE_TYPE]){
        _recordingTime = CMTimeGetSeconds(CMTimeSubtract(self.playbackPlayer.currentItem.asset.duration, CMTimeMakeWithSeconds(KARAOKE_FILE_SCLIENT_TIME, 60)));
    }
    return _recordingTime;
}


- (void)setDataFromMusicElement:(SIAItunesMusicElement *)musicElement {
    
    self.song_name = musicElement.trackName;
    self.playbackURL = musicElement.previewUrl;
    self.soundtrackURL = musicElement.previewUrl;
    self.artist_name = musicElement.artistName;
    self.shouldRecordAudio = YES;
    
}

- (void)setDataFromSoundEffect:(SIASoundEffect *)soundEffect {
    self.song_name = soundEffect.name;
    self.playbackURL = soundEffect.url;
    self.soundtrackURL = soundEffect.url;
    self.shouldRecordAudio = YES;
}

- (void)setParentId:(NSString *)parentId {
    _parentId = parentId;
    if (parentId) {
        _recordType = @"cameo";
    }
}

- (void)startUploadVideoWithConvertHandler:(void(^)(BOOL converted))convertHandler
                             uploadHandler:(void (^)(BOOL uploaded))handler {
    
    self.uploader.type = self.recordType;
    self.uploader.status = self.recordStatus ?: self.status;
    self.uploader.tags = self.tags;
    self.uploader.frameIndex = self.frameIndex;

    
    if ([self.uploader.videoFileNameType isEqualToString:@"asset.mp4"]) {
        self.uploader.videoFileNameType = @"asset.mp4";
    }
    __weak SIARecordingItem *wSelf = self;
    [self.uploader uploadVideoWithCompletionHandler:^(NSString *videoId) {
        wSelf.videoId = videoId;
        wSelf.finishedUploadingVideo = YES;
        if (handler)
            handler(videoId != nil);
    } convertHandler:convertHandler];
}

- (void)prepareForUploadWithCompletionHandler: (void(^_Nonnull)(BOOL completed))handler{
    self.addSong = @"1";
    [self.uploader resetForNewUpload];
    self.uploader.artist = self.artist_name;
    self.uploader.song = self.song_name;
    if (self.image) {
        self.uploader.videoFileNameType = @"image.jpg";
        self.uploader.image = self.image;
    }
    else if ([self.recordURL.pathExtension isEqualToString:@"mp4"] ||
             [self.recordURL.pathExtension isEqualToString:@"mov"] ||
             [self.recordURL.pathExtension isEqualToString:@"MOV"]) {
        self.uploader.videoFileNameType = [@"record_1." stringByAppendingString:self.recordURL.pathExtension.lowercaseString];
    }
    self.uploader.thumbnailFileNameType = @"thumbnail.png";
    self.uploader.genre = self.genre.lowercaseString;
    self.uploader.type = self.recordType;
    self.uploader.parent_id = self.parentId;
    self.uploader.song_id = self.playbackId;
    self.uploader.recordURL = self.recordURL;
    self.uploader.add_song = self.addSong;
    self.uploader.soundtrack_url = self.soundtrackURL;
    self.uploader.song_clip_url = self.musicVideo.previewUrl.absoluteString ?: self.song_clip_url.absoluteString;
    self.uploader.clip_id = self.musicVideo.clip_id;
    self.uploader.song_image_url = self.musicVideo.thumbnailURL.absoluteString ?: self.song_image_url.absoluteString;
    self.uploader.status = self.status;
    
    __weak typeof(self) wSelf = self;
    [_uploader createVideoIdWithSetter:^(NSString *videoId) {
        [Utils dispatchOnMainThread:^{
            wSelf.videoId = videoId;
            handler(videoId.length > 0);
        }];
    }];
}


-(void)uploadThumbnail:(UIImage *)thumbnail{
    
    _uploader.thumbnailImage = thumbnail;
    [self.uploader uploadThumbnail:thumbnail];
}

- (void)updateFilters{
    self.uploader.videoFilters = self.videoFilters;
    self.uploader.audioFilters = self.audioFilters;
    self.uploader.pickTheHookTimeRange = self.pickTheHookTimeRange;
    self.uploader.status = self.recordStatus;
    if (self.uploadData)
        self.uploader.filtersUpdated = YES;
}

/** \brief catch upload retry answer from user (in case of upload failure)
 *
 * \param alert and button pressed
 *
 * \return none
 *
 * ...
 *
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // retry to upload video
    if (buttonIndex == RETRY_BUTTON_INDEX) {
        
        [self uploadThumbnail:self.thumbnailImage];
        
    } else {
        
        [_uploadAlert dismissWithClickedButtonIndex:RETRY_BUTTON_INDEX animated:YES];
    }
}


-(void)setTimeOfPlayback:(CMTime)timeOfPlayback{
    _timeOfPlayback = timeOfPlayback;
    
}

+(NSString *)getFilePathForFileName:(NSString *)fileName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, fileName];
    return filePath;
}

-(void)generateThumbnails:(void(^)(UIImage *image))handler {
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:self.recordURL
                                            options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    
    CMTime duration = asset.duration;
    CMTime firstTime = CMTimeMakeWithSeconds(0,30);
    CMTime middleTime = CMTimeMakeWithSeconds(CMTimeGetSeconds(duration)/2, 30);
    CMTime lastTime = duration;
    //
    //    CGSize maxSize = CGSizeMake(640, 400);
    //    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:@[[NSValue valueWithCMTime:firstTime]] completionHandler:^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){
        if (result != AVAssetImageGeneratorSucceeded) {
            NSLog(@"couldn't generate thumbnail, error:%@", error);
        }else{
            UIImage *image = [UIImage imageWithCGImage:im];
            [Utils dispatchOnMainThread:^{
                handler(image);
            }];
        }
    }];
}

@end
