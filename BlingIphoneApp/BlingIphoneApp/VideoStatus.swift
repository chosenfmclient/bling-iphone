//
//  VideoStatus.swift
//  BlingIphoneApp
//
//  Created by Zach on 25/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
import RestKit

@objc class VideoStatus: NSObject {
    var status: String
    
    init(status: String) {
        self.status = status
        super.init()
    }
    
    static func objectMapping() -> RKObjectMapping {
        let mapping = RKObjectMapping.request()
        mapping?.addAttributeMappings(from: ["status"])
        return mapping!
    }
}
