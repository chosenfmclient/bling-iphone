//
//  SIAUploadVideo.m
//  SingrIphoneApp
//
//  Created by SingrFM on 10/30/13.
//  Copyright (c) 2013 SingrFM. All rights reserved.
//

#import "SIAUploadVideo.h"
#import "SIANetworking.h"
#import "SIAVideoUploadDataModel.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <RestKit/RestKit.h>
#import "SIAAlertController.h"

#define VIDEO_PATH               @"/media"
#define VIDEOS_PATH              @"api/v1/videos"
#define VIDEOS_PATH_WITH_ID      @"api/v1/videos/%@"
#define VIDEOS_PUBLISH_PATH_WITH_ID @"api/v1/videos/%@/publish"
#define TAGS_PATH                @"api/v1/videos/%@/tags"
#define VIDEOS_PATH_WITH_TOKEN   @"/api/v1/videos?access_token=%@"
#define ACCESS_TOKEN             @"access_token"
#define CONTENT_TYPE             @"content-type"
#define VIDEO_URL                @"chosen_video.mp4"
#define THUMBNAIL_URL            @"chosen_thumbnail.png"
#define UPLOAD_TIMEOUT           (10)
#define THUMBNAIL_UPLOAD_RETRIES (3)
#define SERVER_HEADER_CONTENT_LENGTH_KEY_STR @"x-goog-stored-content-length"

#define FINISH_UPLOADING_VIDEO_KEYPATH  @"finishedUploadingVideo"
#define FINISH_UPLOADING_THUMB_KEYPATH  @"finishedUploadingThumb"
#define FINISH_UPDATING_FILTERS_KEYPATH @"filtersUpdated"
#define DATA_MODEL_KEYPATH              @"dataModel"

#define SUCCESS_VIDEO_UPLOAD_MESSAGE   @"%@ has been uploaded successfully"
#define UPLOAD_VIDEO_FAILURE_MESSAGE   @"Your %@ failed to upload"
#define SUCCESS_PICTURE_UPLOAD_MESSAGE @"Profile picture has been uploaded successfully"
#define UPLOAD_PICTURE_FAILURE_MESSAGE @"Your profile picture failed to upload"
#define UPLOAD_FAILURE_ALERT_TITLE  @"Upload Error!"
#define UPLOAD_FAILURE_ARE_YOU_SURE_TITLE @"Are you sure?"
#define UPLOAD_FAILURE_ARE_YOU_SURE_MESSAGE @"This will delete the %@ you just took"
#define STILL_UPLOADING_MESSAGE @"Previous video is still being uploaded.\n %@%% has been uploaded.\n Please wait and try again later."
#define UPLOAD_MESSAGE_TITLE     @"UPLOAD STATUS"
#define UPLOAD_FAILURE_MESSAGE_STR     @"Failed to upload the video. Do you want to retry?"
#define SERVER_ERROR_MESSAGE     @"A server error occured.\n Please try again later."

typedef NS_ENUM(NSUInteger, SIAUploadType) {
    SIAUploadTypeNone,
    SIAUploadTypeVideo,
    SIAUploadTypePicture
};

@interface SIAUploadVideo ()

@property (strong, nonatomic) NSMutableDictionary *operations;
@property (strong, nonatomic) NSMutableArray *operationsArray;
@property (strong, nonatomic) UIAlertView *uploadAlert;
@property (strong, nonatomic) UIAlertView *areYouSureAlert;
@property (strong, nonatomic) NSString *uploadErrorMessage;
@property (strong, nonatomic) NSString *uploadErrorTitle;
@property (nonatomic) SIAUploadType uploadType;
@property (nonatomic) BOOL videoUploadFailed;
@property (nonatomic) BOOL thumbnailUploadFailed;
@property (nonatomic) BOOL videoPublishFailed;
@property (nonatomic) BOOL videoUploadObserverFlag;
@property (nonatomic) BOOL thumbnailUploadObserverFlag;
@property (nonatomic) BOOL isAlertShows;
@property (nonatomic) BOOL inBackground;
@property (nonatomic) int thumbnailUploadRetryCounter;
@property (nonatomic, strong) void (^dataModelReturnHandler)();
@end

@implementation SIAUploadVideo

/** \brief creating singleton popUpView class
 *
 * \param none
 *
 * \return none
 *
 * ...
 *
 */

// Added by Hen

+ (SIAUploadVideo *)sharedUploadManager {
    static SIAUploadVideo *sharedInstance = nil;
    @synchronized(self) {
        if(!sharedInstance)
            sharedInstance = [[SIAUploadVideo alloc] init];
    }
    return sharedInstance;
}

- (id)init {
    if(self = [super init]) {
        self.thumbnailUploadRetryCounter = 0;
    }
    return self;
}

// Commented by Hen

//+(id) sharedUploadManager {
//    
//    static SIAUploadVideo *uploadManager = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        uploadManager = [[SIAUploadVideo alloc] init];
//        uploadManager.thumbnailUploadRetryCounter = 0;
//    });
//    return uploadManager;
//}
//
//- (id) init {
//    
//    self = [super init];
//    
//    if (!self)
//        return nil;
//    else
//        return self;
//}

/** \brief handle upload when app goes to background
 *
 * \param notification
 *
 * \return none
 *
 * ...
 *
 */
- (void) appWillResignActive:(NSNotification *)notification {
    
    _inBackground = YES;
}




//-(id)initWithFile:(NSString *)path fileName:(NSString *)name{
//    self = [super initWithFile:path fileName:name serverPath:VIDEO_PATH];
//    if (self) {
//        self.fileName = [self addDateToFileName:name];
//    }
//    return self;
//
//}

- (void)setArtist:(NSString *)artist SongName:(NSString *)song andScore:(NSInteger)score{
    _artist = artist;
    _song = song;
    _score = score;
}

#pragma mark - Video Upload -

+ (RKObjectMapping *)startUploadObjectMapping{
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{@"song" : @"song_name",
                                                         @"artist" : @"artist_name",
                                                         @"genre" : @"genre",
                                                         @"type"  : @"type",
                                                         @"videoFileNameType" : @"url" ,
                                                         @"thumbnailFileNameType" : @"thumbnail",
                                                         @"parent_id" : @"parent_id",
                                                         @"song_id" : @"song_id",
                                                         @"soundtrack_url":@"soundtrack_url",
                                                         @"question_id" : @"question_id",
                                                         @"frameIndex" : @"frame",
                                                         @"song_clip_url": @"song_clip_url",
                                                         @"song_image_url": @"song_image_url",
                                                         @"clip_id" : @"clip_id",
                                                         @"status" : @"status"}];
    
    return requestMapping;
}

-(NSString *)status{
    if (!_status){
        _status = @"public";
    }
    return _status;
}

/** \brief request for signed url for uploading video and thumbnail
 *
 * \param videoPath and thumbnailPath
 *
 * \return none
 *
 * ...
 *
 */
-(void)uploadVideoWithCompletionHandler:(void(^)(NSString *videoId))handler
                         convertHandler:(void(^)(BOOL converted))convertHandler{
    if (![SIANetworking checkConnection]) {
        self.videoUploadFailed = YES;
        self.uploadCancelled = YES;
        self.isUploading = NO;
        return;
    }
    
    self.uploadType = SIAUploadTypeVideo;
    if (self.dataModel) {
        // we already got the url for uploading but we try to upload again according to error
        
        self.videoId = self.dataModel.video_id;
        
        [self uploadVideoFromPath:self.recordURL
                      toSignedUrl:self.dataModel.video_signed_url
                       withMethod:self.dataModel.video_method
                      contentType:self.dataModel.video_content_type
                    uploadHandler:^(BOOL success) {
                        NSLog(@"upload video %@", success ? @"success": @"failed");
                        if (success) {
                            NSLog(@"video id: %@",self.videoId);
                            self.finishedUploadingVideo = YES;
                        }
                        else {
                            self.videoUploadFailed = YES;
                            [self shouldRetryUpload];
                        }
                        handler(self.videoId);
                    }
                   convertHandler:convertHandler];
        return;
    }
    
    _isVideoReady = NO;
    self.uploadedPercent = 0;
    
    if (!self.finishedUploadingVideo) {
        self.videoUploadObserverFlag = YES;
        [self addObserver:self forKeyPath:@"finishedUploadingVideo" options:NSKeyValueObservingOptionNew context:nil];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidComeFromBackground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    NSLog(@"start uploading");
    _isUploading = YES;
    // request for signed URLs
    __weak SIAUploadVideo* wSelf = self;
//    [RequestManager
//     postRequest:VIDEOS_PATH
//     object:self
//     queryParameters:nil
//     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
//         
//         wSelf.dataModel = (SIAVideoUploadDataModel *)mappingResult.firstObject;
//         wSelf.videoId = wSelf.dataModel.video_id;
    
         if (wSelf.image) {
             wSelf.uploadType = SIAUploadTypePicture;
             NSData *jpeg = UIImageJPEGRepresentation(wSelf.image, 0.9);
             if (convertHandler)
             {
                 convertHandler(YES);
             }
             [self uploadData:jpeg
                  toSignedUrl:wSelf.dataModel.video_signed_url
                   withMethod:wSelf.dataModel.video_method
                  contentType:wSelf.dataModel.video_content_type
                 withProgress:YES
        withThumbnailProgress:NO
            completionHandler:^(BOOL success) {
                if (success)
                {
                    wSelf.finishedUploadingVideo = YES;
                    wSelf.image = nil;
                }
                else {
                    wSelf.videoUploadFailed = YES;
                    [wSelf shouldRetryUpload];
                }
                handler(wSelf.videoId);
            }];
         }
         else {
             [self checkForDataModel:^{
                 
                 [self uploadVideoFromPath:wSelf.recordURL
                               toSignedUrl:wSelf.dataModel.video_signed_url
                                withMethod:wSelf.dataModel.video_method
                               contentType:wSelf.dataModel.video_content_type
                             uploadHandler:^(BOOL success) {
                                 NSLog(@"upload video %@", success ? @"success": @"failed");
                                 if (success) {
                                     NSLog(@"video id: %@",wSelf.videoId);
                                     wSelf.finishedUploadingVideo = YES;
                                     
                                 }
                                 else {
                                     wSelf.videoUploadFailed = YES;
                                     [wSelf shouldRetryUpload];
                                 }
                                 handler(wSelf.videoId);
                             } convertHandler:convertHandler];
             }];
         }
//     }
    
//     failure:^(RKObjectRequestOperation *operation, NSError *error) {
//         
//         wSelf.videoUploadFailed = YES;
//         [wSelf shouldRetryUpload];
//         
//         NSLog(@"No data : %@", error);
//         handler(NO);
//         
//     }
//     showFailureDialog:YES];
}

- (void)checkForDataModel:(void (^)(void))handler {
    if (self.dataModel) {
        handler();
    }
    else {
        self.dataModelReturnHandler = handler;
    }
}

//TODO: make this use requestmanager
-(void) createVideoIdWithSetter:(void (^_Nonnull)(NSString *videoId))setter {
    RKObjectManager *objectManager = [SIAObjectManager sharedManager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* accessToken = [defaults objectForKey:ACCESS_TOKEN];
    NSDictionary *queryParams = @{ACCESS_TOKEN: accessToken,
                                  API_VERSION_KEY_OBJECT};
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[SIAVideoUploadDataModel objectMapping]
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:VIDEOS_PATH
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    
    
    RKRequestDescriptor *requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[SIAUploadVideo startUploadObjectMapping]
                                                                                   objectClass:[SIAUploadVideo class]
                                                                                   rootKeyPath:nil
                                                                                        method:RKRequestMethodPOST];
    [objectManager addRequestDescriptor:requestDescriptor];
    [objectManager addResponseDescriptor:responseDescriptor];
    
    if (![SIANetworking checkConnection]) {
        setter(nil);
        return;
    }
    
    [objectManager postObject:self
                         path:[NSString stringWithFormat:VIDEOS_PATH]
                   parameters:queryParams
                      success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                          
                          self.dataModel = (SIAVideoUploadDataModel *)mappingResult.firstObject;
                          self.videoId = self.dataModel.video_id;
                          setter(_videoId);
                          if (self.dataModelReturnHandler) {
                              self.dataModelReturnHandler();
                              self.dataModelReturnHandler = nil;
                          }
                      }
     
                      failure:^(RKObjectRequestOperation *operation, NSError *error) {
                          
                          self.videoUploadFailed = YES;
                          NSLog(@"No data : %@", error);
                          setter(@"");
                      }];
}

-(void) uploadProfilePicture:(UIImage *)profilePicture
                   imageData:(SIAUploadUserImage *)data
           completionHandler:(void(^)(BOOL success))handler {
    
    self.uploadType = SIAUploadTypePicture;
    
    [self uploadData:UIImagePNGRepresentation(profilePicture)
         toSignedUrl:data.image_signed_url
          withMethod:data.image_method
         contentType:data.image_content_type
        withProgress:NO
     withThumbnailProgress:NO
   completionHandler:^(BOOL success) {
       handler(success);
   }];
}

/** \brief observe for uploading finish
 *
 * \param ...
 *
 * \return ...
 *
 * ...
 *
 */
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (object == self) {
        
        [object removeObserver:self forKeyPath:keyPath];
        
        if ([keyPath isEqualToString:FINISH_UPLOADING_VIDEO_KEYPATH]) {
            
            self.videoUploadObserverFlag = NO;
        }
        
        else if ([keyPath isEqualToString:FINISH_UPLOADING_THUMB_KEYPATH]) {
            
            self.thumbnailUploadObserverFlag = NO;
        }
        
        if ([keyPath isEqualToString:DATA_MODEL_KEYPATH]) {
            
            [self uploadThumbnail:self.thumbnailImage];
        }
        
    }
}

- (NSDictionary *)paramsFromURL:(NSString *)url{
    NSArray *comp1 = [url componentsSeparatedByString:@"?"];
    NSString *query = [comp1 lastObject];
    NSArray *queryElements = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *retDic = [[NSMutableDictionary alloc] init];
    for (NSString *element in queryElements) {
        NSArray *keyVal = [element componentsSeparatedByString:@"="];
        if (keyVal.count > 0) {
            NSString *variableKey = [keyVal objectAtIndex:0];
            NSString *value = (keyVal.count == 2) ? [keyVal lastObject] : @"";
            [retDic addEntriesFromDictionary:@{variableKey: value}];
        }
    }
    return retDic;
    
}

- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL
                                   outputURL:(NSURL*)outputURL
                                     handler:(void (^)(void))handler
{
    
    [self convertVideoToLowQuailtyWithInputURL:inputURL outputURL:outputURL timeRange:kCMTimeRangeInvalid handler:handler];
}


/** \brief converts video to low quality and fixes sync problems
 *  Export cannot be done in the background, so this method cancels when app resigns active and resumes upon becoming active again
 *
 *
 *
 *
 */
- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL
                                   outputURL:(NSURL*)outputURL
                                   timeRange:(CMTimeRange)range
                                     handler:(void (^)(void))handler
{
    // KEEP IT HERE
    if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive) {
        
        __block id obs = [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification
                                                                           object:nil
                                                                            queue:nil
                                                                       usingBlock:^(NSNotification *note) {
                                                                           
                                                                           [self convertVideoToLowQuailtyWithInputURL:inputURL
                                                                                                            outputURL:outputURL
                                                                                                            timeRange:range
                                                                                                              handler:handler];
                                                                           [[NSNotificationCenter defaultCenter] removeObserver:obs];
                                                                       }];
    }
    
    else {
    
    
        [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
        AVAsset *asset;
        
        asset = self.uploadAsset ?: [AVURLAsset URLAssetWithURL:inputURL options:nil];
        
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
        exportSession.outputURL = outputURL;
        exportSession.outputFileType = AVFileTypeMPEG4;
        exportSession.timeRange = range;
        exportSession.shouldOptimizeForNetworkUse = YES;
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            if (exportSession.status == AVAssetExportSessionStatusCompleted) {
                handler();
                self.uploadAsset = nil;
            }
            else {
                [self convertVideoToLowQuailtyWithInputURL:inputURL
                                                 outputURL:outputURL
                                                 timeRange:range
                                                   handler:handler];
            }
        }];
        
        
        __block id obs2 = [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                                            object:nil
                                                                             queue:nil
                                                                        usingBlock:^(NSNotification *note) {
                                                                            [[NSNotificationCenter defaultCenter] removeObserver:obs2];
                                                                            if (exportSession.status == AVAssetExportSessionStatusExporting) {
                                                                                [exportSession cancelExport];
                                                                            }
                                                                        }];
    }
}

- (void)convertAfterAppReturnsFromBackground:(NSNotification *)notification {
}


/** \brief upload file to GCS signe url
 *
 * \param   uploadVideoFromPath:    the path of the video to upload
 *          toSignedUrl:            the signe url to upload to (url on GCS)
 *          withMethod:             HTTP mthod type to upload by  (eg. PUT, POST)
 *          contentType:            type of content (eg. video, image)
 *          completionHandler:      handler to call on compeletion
 *
 * \return none
 *
 * returns success via completion handler
 *
 */
- (void)uploadVideoFromPath:(NSURL *)dataPath
                toSignedUrl:(NSString *)signedURL
                 withMethod:(NSString *)httpMethod
                contentType:(NSString *)contentType
              uploadHandler:(void(^)(BOOL uploaded))uploadHandler
             convertHandler:(void(^)(BOOL converted))convertHandler
{
    
    
    // get data from server
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *lowQualityFile = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"LOW_record_1.mp4"];
    
    __strong void (^converted)(BOOL) = convertHandler;
    
    
    NSLog(@"---start convert");
    [self convertVideoToLowQuailtyWithInputURL:dataPath outputURL:[NSURL fileURLWithPath:lowQualityFile] timeRange:self.videoRange  handler:^{
        NSLog(@"---end convert");
        if (convertHandler) {
            dispatch_async(dispatch_get_main_queue(), ^{
                converted(YES);
            });
        }
        self.finishedUpladQualityConvert = YES;
        
//        if ([[UIApplication sharedApplication] backgroundRefreshStatus] != UIBackgroundRefreshStatusAvailable) {
//            
//            NSLog(@"---background refresh status is inactive");
//            
//            while ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive) {
//                
//            }
//            
//            NSLog(@"---start convert");
//            [self convertVideoToLowQuailtyWithInputURL:dataPath outputURL:[NSURL fileURLWithPath:lowQualityFile] timeRange:self.videoRange handler:^{
//                
//                NSLog(@"---end convert");
//                
//                _finishedUpladQualityConvert = YES;
//                
//                NSData *data = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:lowQualityFile]];
//                
//                
//                [self uploadData:data
//                     toSignedUrl:signedURL
//                      withMethod:httpMethod
//                     contentType:contentType
//                    withProgress:YES
//               completionHandler:^(BOOL success) {
//                   handler(success);
//               }];
//            }];
//            
//        }
//        
//        else {
        
//            NSLog(@"---background refresh status is active");
        
            NSData *data = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:lowQualityFile]];
        
        if (data) {
            [self uploadData:data
                 toSignedUrl:signedURL
                  withMethod:httpMethod
                 contentType:contentType
                withProgress:YES
             withThumbnailProgress:NO
           completionHandler:^(BOOL success) {
               uploadHandler(success);
           }];
        }
//        }
        
    }];
}

- (void) uploadData:(NSData *)data
        toSignedUrl:(NSString *)signedURL
         withMethod:(NSString *)httpMethod
        contentType:(NSString *)contentType
       withProgress:(BOOL)withProgress
withThumbnailProgress:(BOOL)withThumbProgress // if uploading video AND thumbnail, we need to add progress proportionally to give accurate display
  completionHandler:(void(^)(BOOL success))handler
{
    if (data.length < 1){
        AppDelegate *appDelegate = ((AppDelegate*)[[UIApplication sharedApplication] delegate]);
        NSString *photoOrVideo = self.uploadType == SIAUploadTypePicture ? @"photo" : @"video";
        [[[UIAlertView alloc] initWithTitle:@"Error"
                                    message:[NSString stringWithFormat:@"Something went wrong while processing your %@, please try again", photoOrVideo]
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil] show];
        return;
    }
    NSURL *url = [NSURL URLWithString:signedURL];
    if (withProgress) {
        self.uploadedPercent = @"0";
        self.thumbnailUploadedPercent = @"0";
    }
    AFRKHTTPClient *httpClient = [[AFRKHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@://%@" ,url.scheme, url.host]]];
    
    NSString *params = [[signedURL componentsSeparatedByString:@"?"] lastObject];
    
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:httpMethod
                                                            path:[url.path stringByAppendingString:[NSString stringWithFormat:@"?%@", params]]
                                                      parameters:nil];
    [request setTimeoutInterval:90.];
    [request setHTTPBody:data];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    AFRKHTTPRequestOperation *operation = [[AFRKHTTPRequestOperation alloc] initWithRequest:request];
    
    [self.operationsArray addObject:operation];
    NSUInteger operationIndex = [self.operationsArray indexOfObject:operation];
    
    [self.operations addEntriesFromDictionary:@{operation : @{@"bytes_written":@0, @"total_bytes" : @0, @"percent" : @0}}];
    
    // enable upload when app is in background
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{
        NSLog(@"expiration handler");
    }];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
        int percent = ((double)totalBytesWritten / (double)totalBytesExpectedToWrite) * 100.;
        NSString* percentStr = [NSString stringWithFormat:@"%d", percent];
        NSLog(@"percent uploaded: %@", percentStr);
        if (withProgress) {
            self.uploadedPercent = percentStr;
            [self updatePercent:percentStr
               thumbnailPercent:nil];
        }
        if (withThumbProgress) {
            self.thumbnailUploadedPercent = percentStr;
            [self updatePercent:nil
               thumbnailPercent:percentStr];
        }
        //NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
        AFRKHTTPRequestOperation *currentOperation = self.operationsArray[operationIndex];
        self.operations[currentOperation][@"bytes_written"] = [NSNumber numberWithLongLong:totalBytesWritten];
        self.operations[currentOperation][@"total_bytes"]   = [NSNumber numberWithLongLong:totalBytesExpectedToWrite];
        self.operations[currentOperation][@"percent"]       = [NSNumber numberWithFloat:(double)totalBytesWritten/totalBytesExpectedToWrite];
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFRKHTTPRequestOperation *theOperation, id responseObject) {
        NSLog(@"Success:%@,%@",theOperation.response.allHeaderFields, theOperation.responseString);
        NSString *contentSizeStr = [NSString stringWithFormat:@"%@", [theOperation.response.allHeaderFields objectForKey:SERVER_HEADER_CONTENT_LENGTH_KEY_STR]];
        NSInteger contentSize = contentSizeStr.integerValue;
        if (contentSize > 0){
            handler(YES);
        } else {
            handler(NO);
        }
        
        
    } failure:^(AFRKHTTPRequestOperation *theOperation, NSError *error) {
        NSLog(@"Error:%@ ,%@,%@",error,theOperation.response.allHeaderFields,theOperation.responseString);
        
        //[self shouldRetryUpload];
        handler (NO);
        
    }];
    [operation start];
}

-(void)uploadThumbnail:(UIImage *)thumbnail{
    if (!thumbnail) return; 
    self.thumbnailImage = thumbnail;
    if (!self.finishedUploadingThumb) {
        self.thumbnailUploadObserverFlag = YES;
        [self addObserver:self forKeyPath:@"finishedUploadingThumb" options:NSKeyValueObservingOptionNew context:nil];
    }
    
    if (self.dataModel) {
        [self uploadData:UIImageJPEGRepresentation(thumbnail, 1.0)
             toSignedUrl:self.dataModel.thumbnail_signed_url
              withMethod:self.dataModel.thumbnail_method
             contentType:self.dataModel.thumbnail_content_type
            withProgress:NO
         withThumbnailProgress:YES
       completionHandler:^(BOOL success) {
           NSLog(@"upload thumbnail %@", success ? @"success": @"failed");
           if (success){
               self.finishedUploadingThumb = YES;
               
           }
           else {
               self.thumbnailUploadFailed = YES;
               [self shouldRetryUpload];
           }
           
           if (self.finishedUploadingVideo)
           {
               [self publishVideo];
           }
       }];
    }
    
    else if (_isUploading) {
        
        [self addObserver:self forKeyPath:DATA_MODEL_KEYPATH options:0 context:nil];
    }
}

- (void)sendTags {
    
//    Tags* tags = [[Tags alloc] initWithTags: self.tags];
//    
//    [RequestManager
//     postRequest:[NSString stringWithFormat:TAGS_PATH, self.videoId ]
//     object:tags
//     queryParameters:nil
//     success:nil
//     failure:nil];
}

- (void)removeFileNamed:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [SIARecordingItem getFilePathForFileName:fileName];
    [fileManager removeItemAtPath:filePath error:nil];
}

/** \brief ask the user whether to retry upload or not via alert view
 *
 *
 * if first upload fails, ask the user.
 *
 */
- (void) shouldRetryUpload {
    
    
    if (!self.uploadCancelled && self.thumbnailUploadFailed && _thumbnailUploadRetryCounter < THUMBNAIL_UPLOAD_RETRIES) {     // thumbnail upload failed
        
        _thumbnailUploadRetryCounter++;
        
        if (self.thumbnailUploadObserverFlag) {
            [self removeObserver:self forKeyPath:FINISH_UPLOADING_THUMB_KEYPATH];
        }
        
        self.thumbnailUploadObserverFlag = NO;
        self.thumbnailUploadFailed = NO;
        [self uploadThumbnail:self.thumbnailImage];
    }
    
    else if (!self.uploadCancelled && !self.isAlertShows) {
        NSString *alertMessage;
        NSString *photoOrVideo = self.uploadType == SIAUploadTypePicture ? @"photo" : @"video";
        switch (self.uploadType) {
            case SIAUploadTypeVideo:
                alertMessage = [NSString stringWithFormat:UPLOAD_VIDEO_FAILURE_MESSAGE, photoOrVideo] ;
                break;
            case SIAUploadTypePicture:
                alertMessage = UPLOAD_PICTURE_FAILURE_MESSAGE;
                break;
            default:
                alertMessage = [NSString stringWithFormat:UPLOAD_VIDEO_FAILURE_MESSAGE, photoOrVideo];
                break;
        }
        
        _uploadAlert = [[UIAlertView alloc] initWithTitle:UPLOAD_FAILURE_ALERT_TITLE
                                                  message:alertMessage
                                                 delegate:self
                                        cancelButtonTitle:@"Cancel"
                                        otherButtonTitles:@"Retry", nil];
        [Utils dispatchOnMainThread:^{
            [_uploadAlert show];
            self.isAlertShows = YES;
        }];
    }
}

/** \brief catch upload retry answer from user (in case of upload failure)
 *
 * \param alert and button pressed
 *
 * \return none
 *
 * ...
 *
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    self.isAlertShows = NO;
    // retry to upload video
    if (buttonIndex == 1) {
        
        if (self.videoUploadFailed && !self.finishedUploadingVideo) {   // video upload failed
            
//            if (self.videoUploadObserverFlag) {
//                [self removeObserver:self forKeyPath:FINISH_UPLOADING_VIDEO_KEYPATH];
//            }
            self.videoUploadObserverFlag = NO;
            self.videoUploadFailed = NO;
            [self uploadVideoWithCompletionHandler:^(NSString *videoId) {
                if (!videoId)
                    [self shouldRetryUpload];
            } convertHandler:nil];
             
        }
        
        if (self.thumbnailUploadFailed) {     // thumbnail upload failed
            
            if (self.thumbnailUploadObserverFlag) {
                [self removeObserver:self forKeyPath:FINISH_UPLOADING_THUMB_KEYPATH];
            }
            
            _thumbnailUploadRetryCounter = 0;
            self.thumbnailUploadObserverFlag = NO;
            self.thumbnailUploadFailed = NO;
            [self uploadThumbnail:self.thumbnailImage];
        }
        
    } else if ((alertView == _uploadAlert) && buttonIndex == 0 && self.uploadType != SIAUploadTypePicture) {
        
        NSString *photoOrVideo = self.uploadType == SIAUploadTypePicture ? @"photo" : @"video";
        _areYouSureAlert = [[UIAlertView alloc] initWithTitle:UPLOAD_FAILURE_ARE_YOU_SURE_TITLE
                                                      message:[NSString stringWithFormat:UPLOAD_FAILURE_ARE_YOU_SURE_MESSAGE, photoOrVideo]
                                                     delegate:self
                                            cancelButtonTitle:@"Yes, delete"
                                            otherButtonTitles:@"No, retry", nil];
        [_areYouSureAlert show];
    }
    
    else {     // cancel upload
        
        self.uploadCancelled = YES;
        self.isUploading = NO;
        //TODO: replace popupview with alertview
//        [[PopUpView popUpView] show:@"Upload cancelled" withTitle:UPLOAD_MESSAGE_TITLE optionalButton:nil];
        [self removeObservers];
        
    }
}

- (void) showStillUploadingMessage {
    
    NSString *uploadStatus = [[NSString alloc] init];
    if (self.uploadedPercent) {
        
        if ([self.uploadedPercent  isEqualToString:@"100"])
            uploadStatus = [NSString stringWithFormat:STILL_UPLOADING_MESSAGE, @"99"];
        else
            uploadStatus = [NSString stringWithFormat:STILL_UPLOADING_MESSAGE, self.uploadedPercent];
    }
    
    else {
        
        uploadStatus = [NSString stringWithFormat:STILL_UPLOADING_MESSAGE, @"0"];
    }
    
    SIAAlertController *alertController = [SIAAlertController alertControllerWithTitle:UPLOAD_MESSAGE_TITLE
                                                                               message:uploadStatus];
    
    [alertController addAction:[SIAAlertAction actionWithTitle:@"OK"
                                                         style:SIAAlertActionStyleCancel
                                                       handler:nil]];
    
//    [[((AppDelegate*)[[UIApplication sharedApplication] delegate]) getTopViewController] presentViewController:alertController
//                                                         animated:YES
//                                                       completion:nil];
}


- (void) removeObservers {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    if (!self.finishedUploadingVideo && self.videoUploadObserverFlag) {
        [self removeObserver:self forKeyPath:FINISH_UPLOADING_VIDEO_KEYPATH];
        self.videoUploadObserverFlag = NO;
    }
    if (!self.finishedUploadingThumb && self.thumbnailUploadObserverFlag) {
        [self removeObserver:self forKeyPath:FINISH_UPLOADING_THUMB_KEYPATH];
        self.thumbnailUploadObserverFlag = NO;
    }
    
    self.videoUploadFailed = NO;
    self.thumbnailUploadFailed = NO;
    self.videoPublishFailed = NO;
    self.finishedUploadingThumb = NO;
    self.finishedUploadingVideo = NO;
    self.finishedUpladQualityConvert = NO;
    self.filtersUpdated = NO;
    self.uploadCancelled = NO;
    self.isUploading = NO;
    self.thumbnailUploadRetryCounter = 0;
//    self.dataModel = nil;
}

- (void) handleServerError {
    
    if (!self.isAlertShows) {
        SIAAlertController* alertController = [Utils alertWithOkButton:SERVER_ERROR_MESSAGE titleText:@"Error" confirmHandler:nil];
        //TODO: present alert controller
    }
    self.isUploading = NO;
    self.videoUploadFailed = YES;
    self.uploadCancelled = YES;
}

- (void) updatePercent:(NSString *)percentString thumbnailPercent:(NSString *)thumbPercent{
    if (percentString)
        self.uploadedPercent = percentString;
    if (thumbPercent)
        self.thumbnailUploadedPercent = thumbPercent;
    
    NSNumber *totalPercent = @(self.uploadedPercent.integerValue * 0.8 + self.thumbnailUploadedPercent.integerValue * 0.2);
    NSLog(@"TOTAL UPLOAD: %@", totalPercent.stringValue);
    if ([self.delegate respondsToSelector:@selector(updateUploadProgress:)]) {
        [self.delegate updateUploadProgress:totalPercent.stringValue];
    }
}
+ (RKObjectMapping *)updateVideoObjectMapping{
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{@"audioFilters" : @"audio_filters",
                                                         @"videoFilters" : @"video_filters",
                                                         @"pickTheHookTimeRange" : @"clip",
                                                         @"status" : @"status",
                                                         @"add_song":@"add_song"}];
    return requestMapping;
}

- (void) publishVideo {
    
    if (![SIANetworking checkConnection]) {
        self.videoPublishFailed = YES;
        self.uploadCancelled = YES;
        self.isUploading = NO;
        return;
    }
    
    // initialize RestKit
    RKObjectManager *objectManager = [SIAObjectManager sharedManager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* accessToken = [defaults objectForKey:ACCESS_TOKEN];
    NSDictionary *queryParams = @{ACCESS_TOKEN: accessToken,
                                  API_VERSION_KEY_OBJECT};
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[SIAVideo objectMapping]
                                                                                            method:RKRequestMethodPUT
                                                                                       pathPattern:[[VIDEOS_PATH stringByAppendingString:@"/"] stringByAppendingString:self.videoId]
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    
    
    RKRequestDescriptor *requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[SIAUploadVideo updateVideoObjectMapping]
                                                                                   objectClass:[SIAUploadVideo class]
                                                                                   rootKeyPath:nil
                                                                                        method:RKRequestMethodPUT];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    [objectManager addResponseDescriptor:responseDescriptor];
    
    [objectManager putObject:self
                        path:[[VIDEOS_PATH stringByAppendingString:@"/"] stringByAppendingString:self.videoId]
                  parameters:queryParams
                     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                         //[[SIARating defaultScenario] addEvent:SIARatingUserDidUploadVideoEvent];
                         // show upload finished message
                         [self sendTags];
                         NSString *photoOrVideo = self.uploadType == SIAUploadTypePicture ? @"Photo" : @"Video";
                         SIAAlertController *alert = [SIAAlertController alertControllerWithTitle:UPLOAD_MESSAGE_TITLE
                                                                                          message:[NSString stringWithFormat:SUCCESS_VIDEO_UPLOAD_MESSAGE, photoOrVideo]];
                         [alert addAction:[SIAAlertAction actionWithTitle:@"OK"
                                                                    style:SIAAlertActionStyleCancel
                                                                  handler:nil]];
                         // TODO: show dat alert
                         
                         //    [[PopUpView popUpView] show:SUCCESS_VIDEO_UPLOAD_MESSAGE withTitle:UPLOAD_MESSAGE_TITLE optionalButton:nil];
                         
                         _isVideoReady = YES;
                         _isUploading = NO;
                         
                         if ([_delegate respondsToSelector:@selector(finishUploading)]){
                             [_delegate finishUploading];
                         }
                         [self removeObservers];
                         self.uploadType = SIAUploadTypeNone;
                     }
                     failure:^(RKObjectRequestOperation *operation, NSError *error) {
                         self.videoPublishFailed = YES;
                         [self removeObservers];
                     }];
    
    [self removeFileNamed:@"LOW_record_1.mp4"];
}

- (void)resetForNewUpload {
    self.dataModel = nil;
    self.finishedUploadingThumb = NO;
    self.finishedUploadingVideo = NO;
    self.isVideoReady = NO;
    self.finishedUpdatingVideo = NO;
    self.finishedUpladQualityConvert = NO;
}

//-(void)publishVideo:(NSString *)videoid{
////    [SIAServerActions publishVideo:videoid handler:^(BOOL success, NSError *error, id info, BOOL lastConnection) {
////        if (success) {
//    [RequestManager putRequest:[NSString stringWithFormat:VIDEOS_PUBLISH_PATH_WITH_ID, videoid]
//                        object:nil
//               queryParameters:nil
//                       success:^(RKObjectRequestOperation * operation, RKMappingResult * result) {
//
//                       } failure:^(RKObjectRequestOperation * operation, NSError * error) {
//                           //TODO: FAIL'D
//                       }];
//    
////        }
////    }];
//}

@end
