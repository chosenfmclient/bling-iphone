//
//  SIATableViewContainedViewController.h
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 22/07/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIAPaginator.h"
#import "SIALoadingTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define LOADING_CELL_ID @"loadingCell"

//#import "SIAUsersListModel.h"

@interface SIATableViewContainedViewController : SIABaseViewController <SIAPaginatorDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong ,nonatomic) NSMutableArray *listFromModel;
@property (strong, nonatomic) NSMutableArray *cellImagesArray;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UIImageView *loadingspinnigImageView;
@property (strong, nonatomic) SIAPaginator *paginator;


- (NSString *)navigationTitleString;
- (void) showData;
- (CGFloat)tableHeaderSize;
- (NSString *)tableHeaderString;
- (UIColor *)headerBackgroundColor;
- (UIColor *)headerTextColor;
- (void)setImageForRow:(NSUInteger)row withHandler:(void (^)(UIImage* image))handler;
- (NSString *)cellIdentifierForRow:(NSInteger)row;
- (NSString *)imageURLStringForRow:(NSInteger)row;
- (NSURL *)imageURLForRow:(NSInteger)row;
- (NSAttributedString *)textLabelStringForRow:(NSInteger)row;
- (NSAttributedString *)detailedTextStringForRow:(NSInteger)row;
- (UIView *)viewForAccessoryViewForRow:(NSUInteger)row;
- (UIImage *)placeholderForRow:(NSUInteger)row;
- (void)downloadImagesFrom:(NSUInteger)row to:(NSUInteger)lastRow;
- (void)startLoadingAnimation;
- (void)finishLoadingWithAnimation;
- (void)downloadImagesForPage:(NSNumber *)page;
- (void)refreshView;
// this is called when the first page returns no data
- (void)noData;
// this is called when the first page returns data, in order to remove no data screens when the page reloads
- (void)hasData;
- (void)noMorePages;

@end
