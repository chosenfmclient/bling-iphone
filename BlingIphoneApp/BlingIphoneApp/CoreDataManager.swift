 //
//  CoreDataManager.swift
//  BlingIphoneApp
//
//  Created by Zach on 07/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import CoreData

@objc class CoreDataManager: NSObject {
    
    deinit {
        saveContext()
    }
    
    static let shared : CoreDataManager = {
        let instance = CoreDataManager()
        return instance
    }()
    
    let kLikeEntityName = "Like"
    let kFollowEntityName = "Follow"
    
    //MARK: Core data stack
    ///Boiler plate code copy pasted from the interwebs
    // Applications default directory address
    fileprivate lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    fileprivate lazy var managedObjectModel: NSManagedObjectModel = {
        // 1
        let modelURL = Bundle.main.url(forResource: "Blingy", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    fileprivate lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Blingy.sqlite")
        do {
            // If your looking for any kind of migration then here is the time to pass it to the options
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch let  error as NSError {
            print("Ops there was an error \(error.localizedDescription)")
            abort()
        }
        return coordinator
    }()
    
    fileprivate lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the
        // application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to
        // fail.
        let coordinator = self.persistentStoreCoordinator
        var context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.persistentStoreCoordinator = coordinator
        return context
    }()
    
    // if there is any change in the context save it
    fileprivate func saveContext() {
        guard managedObjectContext.hasChanges else { return }
        
        do {
            try managedObjectContext.save()
        } catch let error as NSError {
            print("failed to save context!!!\n**`¯\\ø.o/¯`**")
            print("\(error.localizedDescription)")
            abort()
        }
    }
    
    //MARK: Like Utility methods
    func saveLike(for videoID: String) {
        guard let id = Int(videoID) else { return }
        defer { saveContext() }
        
        let entityDescription = NSEntityDescription.entity(forEntityName: kLikeEntityName, in: managedObjectContext)
        let likeObject = NSManagedObject(entity: entityDescription!, insertInto: managedObjectContext)
        
        likeObject.setValuesForKeys(["video_id" : id, "date": Date()])
    }
    
    
    func loadLikes(for videoIDs: [Int]) {
        for videoID in videoIDs {
            let entityDescription = NSEntityDescription.entity(forEntityName: kLikeEntityName, in: managedObjectContext)
            let likeObject = NSManagedObject(entity: entityDescription!, insertInto: managedObjectContext)
            likeObject.setValuesForKeys(["video_id" : videoID, "date": Date()])
        }
        saveContext()
    }
    
    func deleteLike(for videoID: String) {
        guard let id = Int(videoID) else { return }
        defer { saveContext() }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: kLikeEntityName)
        fetchRequest.predicate = NSPredicate(format: "video_id = %d", id)
        do {
            let objects = try managedObjectContext.fetch(fetchRequest) as? [NSManagedObject]
            guard let _ = objects else { return }
            for like in objects! {
                managedObjectContext.delete(like)
            }
        } catch {
            print("failed to delete like")
        }
    }
    
    func videoIsLikedByMe(videoID: String) -> Bool {
        guard let id = Int(videoID) else { return false }
        defer { saveContext() }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: kLikeEntityName)
        fetchRequest.predicate = NSPredicate(format: "video_id = %d", id)
        do {
            let objects = try managedObjectContext.fetch(fetchRequest) as? [NSManagedObject]
            guard let _ = objects else { return false }
            return objects!.count > 0
        } catch {
            print("failed to fetch like")
            return false
        }
    }
    
    func countTotalLikes() -> Int {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: kLikeEntityName)
        fetchRequest.includesSubentities = false
        
        do {
            let count = try managedObjectContext.count(for:fetchRequest) as Int
            return count
        } catch {
            return 0
        }
    }
    
    //MARK: Follow Utility methods
    func saveFollow(for userID: String) {
        guard let id = Int(userID) else { return }
        defer { saveContext() }
        
        let entityDescription = NSEntityDescription.entity(forEntityName: kFollowEntityName, in: managedObjectContext)
        let followObject = NSManagedObject(entity: entityDescription!, insertInto: managedObjectContext)
        
        followObject.setValuesForKeys(["user_id" : id, "date": Date()])
    }
    
    func saveFollowAll(for userIDs: [String]) {
        for userID in userIDs {
            guard let id = Int(userID) else { continue }
            let entityDescription = NSEntityDescription.entity(forEntityName: kFollowEntityName, in: managedObjectContext)
            let followObject = NSManagedObject(entity: entityDescription!, insertInto: managedObjectContext)
            followObject.setValuesForKeys(["user_id" : id, "date": Date()])
        }
        saveContext()
    }
    
    func loadFollows(for userIDs: [Int]) {
        for userID in userIDs {
            let entityDescription = NSEntityDescription.entity(forEntityName: kFollowEntityName, in: managedObjectContext)
            let followObject = NSManagedObject(entity: entityDescription!, insertInto: managedObjectContext)
            followObject.setValuesForKeys(["user_id" : userID, "date": Date()])
        }
        saveContext()
    }

    
    func deleteFollow(for userID: String) {
        guard let id = Int(userID) else { return }
        defer { saveContext() }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: kFollowEntityName)
        fetchRequest.predicate = NSPredicate(format: "user_id = %d", id)
        do {
            let objects = try managedObjectContext.fetch(fetchRequest) as? [NSManagedObject]
            guard let _ = objects else { return }
            for follow in objects! {
                managedObjectContext.delete(follow)
            }
        } catch {
            print("failed to delete follow")
        }
    }
    
    func deleteFollowAll(for userIDs: [String]) {
        defer { saveContext() }

        var ids = [Int]()
        for userID in userIDs {
            guard let id = Int(userID) else { continue }
            ids.append(id)
        }
        
        guard ids.count > 0 else { return }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: kFollowEntityName)
        fetchRequest.predicate = NSPredicate(format: "user_id IN %d", ids)
        do {
            let objects = try managedObjectContext.fetch(fetchRequest) as? [NSManagedObject]
            guard let _ = objects else { return }
            for follow in objects! {
                managedObjectContext.delete(follow)
            }
        } catch {
            print("failed to delete follow")
        }

    }
    
    func countTotalFollowingWith(didFollow: Bool) -> Int {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: kFollowEntityName)
        fetchRequest.includesSubentities = false
        
        do {
            let count = try managedObjectContext.count(for:fetchRequest) as Int
            
            //surface rate the app if more than 3 following
            if didFollow && count % 3 == 0 && RateTheAppViewController.is24HoursSinceInstall() && (!RateTheAppViewController.hasShown() || RateTheAppViewController.shouldResurface()) {
                (UIApplication.shared.delegate as! AppDelegate).showRateTheApp()
            }
            
            return count
        } catch {
            return 0
        }
    }
    
    func userIsFollowedByMe(userID: String) -> Bool {
        guard let id = Int(userID) else { return false }
        defer { saveContext() }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: kFollowEntityName)
        fetchRequest.predicate = NSPredicate(format: "user_id = %d", id)
        do {
            let objects = try managedObjectContext.fetch(fetchRequest) as? [NSManagedObject]
            guard let _ = objects else { return false }
            return objects!.count > 0
        } catch {
            print("failed to fetch follow")
            return false
        }
    }
}
