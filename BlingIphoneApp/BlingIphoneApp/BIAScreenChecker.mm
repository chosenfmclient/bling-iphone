//
//  BIAScreenChecker.m
//  BlingIphoneApp
//
//  Created by Joseph Nahmias on 21/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import "BIAScreenChecker.h"
#import <opencv2/opencv.hpp>
//#import <opencv2/objdetect/objdetect_c.h>
#import <opencv2/imgproc/imgproc.hpp>
//#import <opencv2/highgui/highgui.hpp>

@implementation BIAScreenChecker

+(BOOL)checkScreen:(CVPixelBufferRef)screen forAccuracyRate:(CGFloat)accuracy resizeFactor:(CGFloat)resizeFactor{
    
    cv::Mat cannyMat, screenMat, grayscaleMat;
    
    screenMat = [BIAScreenChecker matFromPixelBuffer:screen];
    cv::resize(screenMat, screenMat, cv::Size(), resizeFactor, resizeFactor, CV_INTER_AREA);
    grayscaleMat = screenMat;
    cv::Canny(grayscaleMat, cannyMat, 20, 60, 3);
    
    int count_white = 0;
    for( int y = 0; y < cannyMat.rows; y++ ) {
        for( int x = 0; x < cannyMat.cols; x++ ) {
                if ( cannyMat.at<uchar>(y,x) == 255 ) {
                    count_white++;
                }
        }
    }
    
    printf("%d white pixels, rate: %f\n", count_white, (float)count_white/cannyMat.size().area());
    
    if (((float)count_white / cannyMat.size().area()) < accuracy) {
        return true;
    }
    
    return false;
}


+(cv::Mat)matFromPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    
    OSType format = CVPixelBufferGetPixelFormatType(pixelBuffer);

    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    void *baseaddress = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
    
    CGFloat width = CVPixelBufferGetWidth(pixelBuffer);
    CGFloat height = CVPixelBufferGetHeight(pixelBuffer);
    
    cv::Mat mat(height, width, CV_8UC1, baseaddress, 0);
    
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    return mat;
}

@end
