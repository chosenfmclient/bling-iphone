//
//  SIAConstants.m
//  SingrIphoneapp
//
//  Created by Lior Lasry on 26/02/13.
//  Copyright (c) 2013 Lior Lasry. All rights reserved.
//

#import "SIAConstants.h"

NSString * const kAPIActionGetUTubeFeed             = @"http://www.youtube.com/get_video_info?video_id=";

NSString * const kAPIActionGetClipFeed              = @"/?action=clip&q=";

// User - info taken from facebook
NSString * const kChosenUserKeyStr                  = @"user";
NSString * const kChosenUserIdKeyStr                = @"user_id";
NSString * const kChosenUserNameKeyStr              = @"userName";
NSString * const kChosenUserEmailKeyStr             = @"userEmail";
NSString * const kChosenUserFirstNameKeyStr         = @"userFirstName";
NSString * const kChosenUserLastNameKeyStr          = @"userLastName";
NSString * const kChosenUserBirthdayKeyStr          = @"userBirthday";

// facebook
#define FACEBOOK_DEVELOPMENT_APPID                  @"768721359886958"
#define FACEBOOK_TESTING_APPID                      @"757882907637729"
#define FACEBOOK_DEMO_APPID                         @"933128396780391"
#define FACEBOOK_STAGE_APPID                        @"720297134674955"
#define FACEBOOK_PROD_APPID                         @"171204453047499" 

// Environments
#ifdef CHOSEN_DEV_ENV
NSString * const kFBAppId                           = FACEBOOK_DEVELOPMENT_APPID;
#endif

#ifdef CHOSEN_TEST_ENV
NSString * const kFBAppId                           = FACEBOOK_TESTING_APPID;
#endif

#ifdef CHOSEN_DEMO_ENV
NSString * const kFBAppId                           = FACEBOOK_DEMO_APPID;
#endif

#ifdef CHOSEN_STAGE_ENV
NSString * const kFBAppId                           = FACEBOOK_STAGE_APPID;
#endif

#ifdef CHOSEN_PROD_ENV
NSString * const kFBAppId                           = FACEBOOK_PROD_APPID;
#endif


// General
NSString * const kFBUserIdKeyStr                    = @"id";
NSString * const kFBAccessTokenKeyStr               = @"access_token";
NSString * const kFBUserNameKeyStr                  = @"FB_userName";
NSString * const kFBUserEmailKeyStr                 = @"FB_userEmail";
NSString * const kFBUserFirstNameKeyStr             = @"FB_userFirstName";
NSString * const kFBUserLastNameKeyStr              = @"FB_userLastName";
NSString * const kFBUserBirthdayKeyStr              = @"FB_userBirthday";
NSString * const kFBUserGenderKeyStr                = @"FB_userGender";
NSString * const kFBUserImagesKeyStr                = @"FB_userImage";

NSString * const kAccessTokenKeyStr                 = @"access_token";
NSString * const kAccessTokenDate                   = @"access_token_date";
NSString * const kLoginType                         = @"login_type";
NSString * const kFacebookLoginType                 = @"facebook";
NSString * const kEmailLoginType                    = @"email";
NSString * const kSignInViewDismissed               = @"signInDismissed";

NSString * const kSave2CameraRoll                   = @"save2_camera_roll";
NSString * const kFirstTimeSave2CameraRoll          = @"1st_save2_camera_roll";

// preset data
NSString * const kChosenVideoKeyStr                 = @"video";
NSString * const kChosenProfileKeyStr               = @"profile";
NSString * const kChosenGameKeyStr                  = @"game";
NSString * const kChosenBadgeKeyStr                 = @"badge";
NSString * const kTracksKeyStr                      = @"tracks";
NSString * const kUserIdKeyStr                      = @"user_id";
NSString * const kContentIdKeyStr                   = @"id";
NSString * const kVideoIdKeyStr                     = @"video_id";
NSString * const kShareIdKeyStr                     = @"share_id";
NSString * const kGameIdKeyStr                      = @"game_id";
NSString * const kNotificationIdKeyStr              = @"notification_id";
NSString * const kActivityIdKeyStr                  = @"activity_id";
NSString * const kSongIdKeyStr                      = @"song_id";
NSString * const kTypeKeyStr                        = @"type";
NSString * const kUrlKeyStr                         = @"url";
NSString * const kImageKeyStr                       = @"thumbnail";
NSString * const kLinkKeyStr                        = @"link";
NSString * const kMyVideoKeyStr                     = @"is_my_video";
NSString * const kIsSelectedIntroKeyStr             = @"currentintro";
NSString * const kNewKeyStr                         = @"new";
NSString * const kPromotedKeyStr                    = @"promoted";
NSString * const kVideoPointsKeyStr                 = @"points";
NSString * const kAlbumKeyStr                       = @"album";
NSString * const kArtistKeyStr                      = @"artist";
NSString * const kScoreKeyStr                       = @"score";
NSString * const kLikesKeyStr                       = @"likes";
NSString * const kCommentsKeyStr                    = @"comments";
NSString * const kSongNameKeyStr                    = @"name";
NSString * const kPlayingKeyStr                     = @"Playing Now";
NSString * const kNextKeyStr                        = @"Coming next...";
NSString * const kSkipKeyStr                        = @"Skip";
NSString * const kVoteKeyStr                        = @"Vote";
NSString * const kPersonalBoardAction               = @"PersonalBoard";
NSString * const kUserNotificationAction            = @"Notifications";
NSString * const kAppDelegateOpenURLNotification    = @"AppDelegateOpenUrl";
NSString * const kRecordTypeDanceOff                = @"dance_off";
NSString * const kRecordTypeBio                     = @"bio";
NSString * const kRecordTypeReview                  = @"response";
NSString * const kApiPageStr                        = @"page";

/* Video Player Keys */
NSString * const kTracksKey                         = @"tracks";
NSString * const kPlayableKey                       = @"playable";
NSString * const kStatusKey                         = @"status";
NSString * const kBufferEmptyKey                    = @"playbackBufferEmpty";
NSString * const kLikelyToKeepUpKey                 = @"playbackLikelyToKeepUp";
NSString * const kRateKey                           = @"rate";
NSString * const kCurrentItemKey                    = @"currentItem";

//instructions
NSString * const kRecordHereInstruction             = @"record";
NSString * const kLookHereInstruction               = @"look";
NSString * const kPrepareMusicInstruction           = @"prepare";
NSString * const kPerformFeedbackInstruction        = @"feedback";
NSString * const kTimerInstruction                  = @"timer";
NSString * const kLyricsHereInstruction             = @"lyrics";
NSString * const kFrameYourShotInstruction          = @"frame";
NSString * const kTextInstruction                   = @"text";
NSString * const kQuestionInstruction               = @"question";

//share methods
NSString * const kFacebookShareMethod               = @"facebook";
NSString * const kTwitterShareMethod                = @"twitter";
NSString * const kInstagramShareMethod              = @"instagram";
NSString * const kSMSShareMethod                    = @"sms";
NSString * const kEmailShareMethod                  = @"mail";
NSString * const kEllentubeShareMethod              = @"ellentube";


const int kArtistNameOffset = 0;

NSInteger const kUserVideoIntroType = 0;
NSInteger const kUserVideoAuditionType = 1;


// custom colors
UIColor * SingrGreen                                = nil;
UIColor * SingrYellow                               = nil;
UIColor * SingrPink                                 = nil;
UIColor * SingrBlue                                 = nil;
UIColor * SingrRed                                  = nil;
UIColor * SingrGray                                 = nil;
UIColor * SingrDarkGray                             = nil;
UIColor * SingrDirtyWhite                           = nil;
UIColor * ChosenLightBlue                           = nil;

//custom fonts
