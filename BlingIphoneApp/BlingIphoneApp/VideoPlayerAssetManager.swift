//
//  VideoPlayerDownloader.swift
//  BlingIphoneApp
//
//  Created by Zach on 01/12/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc enum VideoPlayerAssetManagerOptions: Int {
    case stream = 0
    case download
}

@objc class VideoPlayerAssetManager: NSObject {
    
    static let shared : VideoPlayerAssetManager = {
        let instance = VideoPlayerAssetManager()
        instance.cache.countLimit = 5
        return instance
    }()
    
    private let cache = NSCache<NSString, AVAsset>()
    
    func prepareVideoAt(_ url: URL, with completion: @escaping (_ asset: AVAsset?) -> Void, usingOption option: VideoPlayerAssetManagerOptions = .stream) {
        
        if let asset = cache.object(forKey: NSString(string: url.absoluteString).md5() as NSString) {
            return completion(asset)
        }
        
        guard let filePath = getDiskPathFor(url) else {
            return completion(nil)
        }
        
        let assetWasCreated: (AVAsset?) -> Void = {[weak self] (asset) in
            if let _ = asset {
                self?.cache.setObject(asset!, forKey: NSString(string: url.absoluteString).md5() as NSString)
            }
            completion(asset)
        }
        
        guard !(FileManager.default.fileExists(atPath: filePath)) else {
            return createAssetFrom(URL(fileURLWithPath: filePath), withCompletion: assetWasCreated)
        }
        
        guard option != .stream  else {
            createAssetFrom(url, withCompletion: assetWasCreated)
            return
        }
        
        downloadFileAt(url, with: {[weak self] (filepath, fileExisted) in
            guard let _ = filepath else {
                return completion(nil)
            }
            self?.createAssetFrom(filepath!, withCompletion: assetWasCreated)
        })
        
    }
    
    private func createAssetFrom(_ url: URL, withCompletion completion: ((_ asset: AVAsset?) -> Void)?) {
        let asset = AVURLAsset(url: url)
        var nullableCompletion = completion // so we can nil it once it's been called once
        asset.loadValuesAsynchronously(forKeys: ["playable"]) { [weak self] in
            guard let _ = self, let _ = nullableCompletion else { return }
            var error: NSError? = nil
            let status = asset.statusOfValue(forKey: "playable", error: &error)
            switch status {
            case .loaded:
                nullableCompletion!(asset)
                nullableCompletion = nil
                break
            case .failed: fallthrough
            case .cancelled: fallthrough
            default:
                //we're fucked
                nullableCompletion!(nil)
                nullableCompletion = nil
                break
            }
        }
    }
    
    func downloadFileAt(_ url: URL, with completion: @escaping (_ filepath: URL?, _ fileExisted: Bool) -> Void) {
        let downloadTask = AFRKHTTPRequestOperation(request: NSURLRequest(url: url as URL) as URLRequest!)
        
        guard let filePath = getDiskPathFor(url) else {
            return completion(nil, false)
        }
        
        guard !(FileManager.default.fileExists(atPath: filePath)) else {
            return completion(URL(fileURLWithPath: filePath), true)
        }
        
        downloadTask?.outputStream = OutputStream(toFileAtPath: filePath + ".dlblin", append: false)
        downloadTask?.setCompletionBlockWithSuccess(
            {[weak self] (operation, response)  in
                defer {
                    try? FileManager.default.removeItem(atPath: filePath + "dl.blin")
                }
                guard let task = downloadTask else {
                    return
                }
                guard !task.isCancelled else {
                    return
                }
                let url = URL(fileURLWithPath: filePath)
                Utils.dispatchAsync {
                    completion(url, false)
                }
                
                let fileMan = FileManager.default
                do {
                    try fileMan.moveItem(atPath: filePath + ".dlblin", toPath: filePath)
                }
                catch {
                    print("couldnt move downloaded video to final path")
                }
                
            },
            failure: { (operation, error) in
                try? FileManager.default.removeItem(atPath: filePath + "dl.blin")
                print("video file could not be downloaded")
                completion(nil, false)
            }
        )
        downloadTask?.start()
    }
    
    private func getDiskPathFor(_ url: URL) -> String? {
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        guard let cacheDir = paths.first else {
            print("failed to fetch cache dir :s")
            return nil
        }
        
//        var isDir: ObjCBool = ObjCBool(false)
//        if let preloadedPath = Bundle.main.resourcePath?.appending("/preloadedVideos"),
//            FileManager.default.fileExists(atPath: preloadedPath, isDirectory: &isDir),
//            isDir.boolValue {
//            for path in try! FileManager.default.contentsOfDirectory(atPath: preloadedPath) {
//                if url.absoluteString.contains(path) {
//                    return "\(preloadedPath)/\(path)"
//                }
//            }
//        }
        
        return cacheDir.appendingFormat("/\(NSString(string: url.absoluteString).md5()).\(url.pathExtension)")
    }
}
