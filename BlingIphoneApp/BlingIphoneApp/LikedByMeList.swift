//
//  LikedByMeList.swift
//  BlingIphoneApp
//
//  Created by Zach on 07/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit
@objc class LikedByMeList: NSObject {
    var liked_by_me: [Int]?
    
    static func objectMapping() -> RKObjectMapping {
        let objectMapping = RKObjectMapping(for: LikedByMeList.self)
        objectMapping?.addAttributeMappings(from: ["liked_by_me"])
        return objectMapping!
    }
}
