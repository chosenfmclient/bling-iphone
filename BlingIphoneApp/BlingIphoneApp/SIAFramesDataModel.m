//
//  SIAFramesDataModel.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 3/24/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAFramesDataModel.h"

@implementation SIAFrame
// NOTHING HERE HAHA SUCK IT
@end

@implementation SIAFramesDataModel

+ (RKObjectMapping *)objectMapping {
    
    RKObjectMapping *frameMapping = [RKObjectMapping mappingForClass:[SIAFrame class]];
    [frameMapping addAttributeMappingsFromArray:@[@"frame_id",
                                                  @"name",
                                                  @"texture",
                                                  @"frames"]];
    
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[SIAFramesDataModel class]];
    [objectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data"
                                                                                  toKeyPath:@"frames"
                                                                                withMapping:frameMapping]];
    return objectMapping;
}

+ (void)getFrames:(void(^)(SIAFramesDataModel *model))completion {
    
    RKObjectManager *objectManager = [SIAObjectManager sharedManager];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[SIAFramesDataModel objectMapping]
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:@"api/v1/video-frames"
                                                                                           keyPath:nil
                                                                                       statusCodes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 5)]];
    [objectManager addResponseDescriptor:responseDescriptor];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* accessToken = [defaults objectForKey:@"access_token"];
    NSDictionary *params = @{@"access_token": accessToken,
                             API_VERSION_KEY_OBJECT};
    
    [objectManager getObjectsAtPath:@"api/v1/video-frames"
                         parameters:params
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                if (mappingResult.firstObject)
                                    completion(mappingResult.firstObject);
                            } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                            }];
}

+ (NSString *)aspectRatioKeyForAspectRatio:(CGFloat)aspectRatio {
    if (aspectRatio < (CGFloat)(10.0/16.0))
    {
        return @"9x16";
    }
    else if (aspectRatio < (CGFloat)(3.0/4.0))
    {
        return @"10x16";
    }
    else if (aspectRatio < (CGFloat)1)
    {
        return @"3x4";
    }
    else if (aspectRatio < (4.0/3.0))
    {
        return @"1x1";
    }
    else if (aspectRatio < (CGFloat)(16.0/10.0))
    {
        return @"4x3";
    }
    else if (aspectRatio < (CGFloat)(16.0/9.0))
    {
        return @"16x10";
    }
    else
    {
        return @"16x9";
    }

}

@end
