//
//  ThirdPartyAPIKeyWrapper.h
//  BlingIphoneApp
//
//  Created by Michael Biehl on 12/5/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCHStringDefinesWrapper : NSObject
+ (NSString *)appseeAPIKey;
+ (NSString *)amplitudeAPIKey;
+ (NSString *)flurryAPIKey;
+ (NSString *)preloadedVideosPath;
@end
