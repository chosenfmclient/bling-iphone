//
//  SIAVideo.h
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 20/07/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "SIAUser.h"
#import <RestKit/RestKit.h>

/** video info */
#define kVideoId        @"video_id"
#define kType           @"type"
#define kArtistName     @"artist_name"
#define kGenre          @"genre"
#define kSongName       @"song_name"
#define kThumbnail      @"thumbnail"
#define kUrl            @"url"
#define kClip           @"clip"
#define kScore          @"score"
#define kTotalViews     @"total_views"
#define kTotalShares    @"total_shares"
#define kTotalResponses @"total_responses"
#define kResponses      @"responses"
#define kActivities     @"activities"
#define kTotalVotes     @"total_votes"
#define kRank           @"rank"

@protocol SIAVideoDelegate <NSObject>
- (void)videoTotalsDidChange:(SIAVideo * _Nonnull)video;

@end
@class VideoDownloadOperation;
@interface SIAVideo : NSObject

@property (weak, nonatomic) _Nullable id <SIAVideoDelegate> delegate;

@property (strong, nonatomic) NSString   *videoId;
@property (strong, nonatomic) NSString   *type;
@property (nonatomic, strong) NSString  *mediaType;
@property (strong, nonatomic) NSString   *status;
@property (strong, nonatomic) NSDate     *date;
@property (strong, nonatomic) NSString   *song_name;
@property (strong, nonatomic) NSString   *artist_name;
@property (strong, nonatomic) NSString   *genre;
@property (strong, nonatomic) NSURL      *thumbnailUrl;
@property (strong, nonatomic, nullable) NSURL      *thumbnailURL;
@property (strong, nonatomic) NSURL      *animatedURL;
@property (strong, readonly, nonatomic) UIImage *thumbnailImage;
@property (strong, nonatomic) NSURL *videoUrl;
@property (strong, nonatomic) NSURL *song_clip_url;
@property (strong, nonatomic) NSURL *song_image_url;
@property (strong, nonatomic, nullable) NSString *clip_id;
@property (strong, nonatomic, nullable) NSArray* stupidBlingTimes;
@property(strong, nonatomic) NSURL      *firstFrameUrl;
@property(strong, nonatomic) NSArray <NSString *> *tags;

@property(strong, nonatomic) SIAVideo *parent;

@property(strong, nonatomic) SIAUser    *user;
@property(strong, nonatomic) AVPlayerItem *playerItem;

@property(strong, nonatomic) NSURL      *playbackURL;

// likes
@property (strong, nonatomic) NSString *total_likes;
@property (strong, nonatomic) NSString *total_comments;
@property (strong, nonatomic, readonly) NSString *totalLikesStr;
@property (strong, nonatomic, readonly) NSString *totalCommentsStr;

@property(nonatomic) BOOL isLikedByMe;
@property (nonatomic) BOOL videoIsBeingLiked;

// downloadable video protocol
@property (strong, nonatomic, nullable) NSURL *downloadedFilePathURL;
@property (strong, nonatomic, nullable) VideoDownloadOperation *operation;


- (void) deleteVideoWithCompletionHandler: (void(^_Nullable)(BOOL success))completion;
- (void) changeVideoStatusTo: (NSString * _Nonnull)status
       withCompletionHandler: (void(^_Nullable)(BOOL success))completion;
+(RKDynamicMapping  * _Nonnull )objectMapping;
+(RKObjectMapping * _Nonnull)requestMapping;
+ (NSDictionary*) getMappingDict;
+ (void) getDataForVideoId:(NSString * _Nonnull)videoId withCompletionHandler:(void(^ _Nonnull)(SIAVideo *_Nullable video))handler;

- (BOOL)isPhoto;
- (BOOL)isVideo;
- (BOOL)isFeatured;
- (BOOL)isDailyPick;
- (BOOL) isMyVideo;
- (BOOL) isPrivate;
- (BOOL)isCameo;
- (void)toggleLikeWithCompletion: (void (^)())completion
          shouldChangeLikesValue: (BOOL)shouldChangeValue
           withAmplitudePageName: (NSString*) pageName;
@end
