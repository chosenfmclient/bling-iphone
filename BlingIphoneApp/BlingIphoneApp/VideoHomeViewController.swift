//
//  VideoHomeViewController.swift
//  ChosenIphoneApp
//
//  Created by Zach on 29/08/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

import UIKit
import CoreMedia
import RestKit
import SDWebImage

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


@objc class VideoHomeViewController : SIABaseViewController, VideoPlayerManagerDelegate, VideoUserBarDelegate,
    CameoPopupDelegate, SIAVideoDelegate
{
    //MARK: outlets
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var commentsButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var playerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var playerViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var infoBarBottomLandscapeConstraint: NSLayoutConstraint!
    @IBOutlet var infoBarBackgroundView: UIView!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var backGroundTintView: UIView!
    
    @IBOutlet weak var cameoIcon: UIImageView!
    @IBOutlet weak var shootNowButtonContainer: UIView!
    @IBOutlet weak var shootNowButton: UIButton!
    @IBOutlet weak var moreLikeThisButton: UIButton!
    @IBOutlet weak var userBarView: VideoUserBarView!
    @IBOutlet weak var infoBarView: UIView!
    @IBOutlet weak var buttonBarView: UIView!
    @IBOutlet var videoPortraitTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var spinnerView: SIASpinner!
    @IBOutlet weak var songDataButton: UIButton!
    
    @IBOutlet weak var shootNowBackgroundView: UIView!
    @IBOutlet weak var videoInfoToCameoButtonConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cameoButtonContainer: UIView!
    @IBOutlet weak var newCameoButton: UIButton!
    
    //MARK: properties
    
    var setForCameoInvite = false
    var setUIForInviteToCameo: Bool {
        guard let _ = video else { return false }
        return video!.isMyVideo() && video!.status != "secret" && !video!.isCameo()
    }
    
    var videoCollectionHandler: VideoCollectionHandler?
    fileprivate var backgroundAlphaLayerColor: UIColor?
    
    fileprivate var layedOutVideo = false
    fileprivate var interactionIsEnabled = false
    fileprivate var videoInitialized = false
    var backgroundSet = false
    var landscape = false
    var fromRecordFlow = false
    
    //MARK: video
    var video: SIAVideo? = nil {
        didSet {
            video?.delegate = self
        }
    }
    var videoId: String? = nil
    var timer: Timer?
    var didPlay = false
    var appIsInForeground = true
    var cameoPopupIsDisplayed = false
    var didSendView = false
    
    var cameoPopup: CameoPopup?
    
    //MARK: view controller methods
    override func viewDidLoad()
    {
        interactionEnabled(false)
        infoBarBottomLandscapeConstraint.isActive = false
        //Set nav bar color
        navigationController.setNavigationBarLeftButtonBack()
        playerViewHeightConstraint.constant = UIScreen.main.bounds.width / (4/3)
        playerViewWidthConstraint.constant = UIScreen.main.bounds.width
        Utils.dispatchAsync({[weak self] in
            guard let sSelf = self else { return }
            if (sSelf.video != nil) {
                sSelf.prepareDataFromVideo(sSelf.video!)
            } else if (sSelf.videoId != nil) {
                sSelf.mainView.alpha = 0
                sSelf.spinnerView.startAnimation()
                sSelf.getVideoDataFromApi(sSelf.videoId!)
            }
        })
        if setForCameoInvite {
            shootNowBackgroundView.backgroundColor = SIAStyling.defaultStyle().cameoPink()
        }
        userBarView.delegate = self
        userBarView.video = video
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)

    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        shootNowButton?.bia_rounded()
        newCameoButton.bia_rounded()
        if setUIForInviteToCameo {
            videoInfoToCameoButtonConstraint.isActive = true
        }
    }
    
    func applicationWillEnterForground(_ notification: Notification) {
        appIsInForeground = true
        if (VideoPlayerManager.shared.currentPlayerView == thumbnailImage) {
            Utils.dispatchOnMainThread {[weak self] in
                VideoPlayerManager.shared.removeVideo()
                self?.playButton.setImage(UIImage(named: "video_play"), for: .normal)
            }
        }
    }
    
    func applicationWillResignActive(_ notification: Notification) {
        appIsInForeground = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setBottomBarHidden(landscape || cameoPopupIsDisplayed, animated: false)
        navigationController?.setNavigationBarHidden(landscape, animated: false)
        navigationController?.setNavigationBarLeftButtonBack()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        VideoPlayerManager.shared.shouldPlay = true
        if let video = video {            
            if !didPlay {
                didPlay = true
                //VideoPlayerManager.shared.setNewPlayerView(view: thumbnailImage, videoURL: video.videoUrl, withDelegate: self)
                prepareToPlay(video)
                //AmplitudeEvents.shared.logVideoView(forVideoID: video.videoId, forPageName: AmplitudeEvents.kVideoHomePageName)
                playButton.setImage(nil, for: .normal)
            }
            else {
                VideoPlayerManager.shared.layoutVideo()
            }
        }
        
        navigationController?.setNavigationBarHidden(landscape, animated: true)
        navigationController?.setBottomBarHidden(landscape || cameoPopupIsDisplayed, animated: true)
        navigationController?.setNavigationBarLeftButtonBack()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        playButton.setImage(UIImage(named: "video_play"), for: .normal)
        VideoPlayerManager.shared.removeVideo()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey:"orientation")
        navigationController?.clearNavigationItemLeftButton()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if (size.width > size.height)
        {
            //Landscape
//            userBarHeightConstraint.constant = 0
            videoPortraitTopConstraint.isActive = false
            UIApplication.shared.setStatusBarHidden(true, with: .fade)
            navigationController.setNavigationBarHidden(true, animated: true)
            navigationController.setBottomBarHidden(true, animated: true)
            infoBarBottomLandscapeConstraint.isActive = true
            infoBarBackgroundView.isHidden = false
            fadeOutViews([userBarView, buttonBarView])
            if VideoPlayerManager.shared.player.rate > 0 {
                fadeOutView(infoBarView)
            }
            landscape = true
        }
        else
        {
            //Portrait
//            userBarHeightConstraint.constant = 60
            videoPortraitTopConstraint.isActive = true
            fade(in: [userBarView, buttonBarView, infoBarView])
            infoBarBottomLandscapeConstraint.isActive = false
            infoBarBackgroundView.isHidden = true
            videoInfoToCameoButtonConstraint.isActive = true
            UIApplication.shared.setStatusBarHidden(false, with: .fade)
            navigationController.setNavigationBarHidden(false, animated: true)
            navigationController.setBottomBarHidden(cameoPopupIsDisplayed, animated: true)
            landscape = false
        }
        
        let fullHeight = size.width / (4/3)
        if fullHeight < size.height {
            playerViewHeightConstraint.constant = fullHeight
            playerViewWidthConstraint.constant = size.width
        } else {
            playerViewHeightConstraint.constant = size.height
            playerViewWidthConstraint.constant = size.height * (4/3)
        }
        
        self.view.layoutIfNeeded()
        
        coordinator.animate(
            alongsideTransition: {[weak self] (coordinatorContext) in
                guard VideoPlayerManager.shared.currentPlayerView == self?.thumbnailImage else {
                    return
                }
                VideoPlayerManager.shared.layoutVideo()
            },
            completion: {[weak self] (x) in
                self?.view.layoutIfNeeded()
            }
        )
    }
    
    override var prefersStatusBarHidden: Bool {
        return landscape
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "shareSegue" {
            playButton.setImage(UIImage(named: "video_play"), for: .normal)
            VideoPlayerManager.shared.player.pause()
            AmplitudeEvents.shared.pendingView = nil
            
            let dest = segue.destination as! SIAShareSheetViewController
            dest.video = video!
            dest.objectType = "video"
            dest.objectId = video!.videoId
            
            return
        }
        
        if segue.identifier == "listSegue" {
            let dest = segue.destination as! VideoHomeListsViewController
            dest.video = video!
            if ((sender as! UIButton) == likeButton) {
                dest.tab = dest.kLikeTab
            } else if ((sender as? UIButton) == commentsButton){
                dest.tab = dest.kCommentTab
            }
            
            print("switching to lists. background set:", backgroundSet)
            dest.background = backgroundImage.image
            dest.backgroundTintColor = backgroundAlphaLayerColor
            return
        }
        
        if let dest = segue.destination as? DiscoverViewController {
            dest.clipID = video?.clip_id
            return
        }
        
        if segue.identifier == "shootNowSegue" {
            let dest = segue.destination as! SIABlingRecordViewController
            let musicVideo = MusicVideo(video: video!)
            dest.musicVideo = musicVideo
            AmplitudeEvents.log(event: "record:shootnow",
                                with: ["pagename":AmplitudeEvents.kVideoHomePageName,
                                       "object_id":video!.videoId,
                                       "artist_name":video!.artist_name,
                                       "video_name":video!.song_name]
            )
            return
        }
        
        if segue.identifier == "recordCameoSegue" {
            let dest = segue.destination as! SIABlingRecordViewController
            dest.cameoParent = video!
            AmplitudeEvents.log(event: "duet:make",
                                with: ["pagename":AmplitudeEvents.kVideoHomePageName,
                                       "object_id":video!.videoId,
                                       "clip_id": video!.clip_id ?? "null",
                                       "artist_name":video!.artist_name,
                                       "video_name":video!.song_name,
                                       "type" : setForCameoInvite ? "invitee" : "other"])
        }
        
        if segue.identifier == "cameoInviteSegue" {
            let dest = segue.destination as! InviteContainerViewController
            dest.cameoId = video!.videoId
            dest.cameoVideo = video!
            dest.isDuetInvite = true
            return
        }
        
        if segue.identifier == "profileSegue" {
            let dest = segue.destination as! ProfileViewController
            dest.userId = (video!.user.user_id)!
            return
        }
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion)
        VideoPlayerManager.shared.layoutVideo()
    }
    
    //MARK: Cameo Popup Delegort
    
    func cameoPopupWasDismissed(_ popup: CameoPopup?) {
        navigationController.setBottomBarHidden(false, animated: true)
    }
    
//    override func segueWasCancelled() {
//        shootNowButton.isEnabled = true
//    }
    
    //MARK: button actions
    
    @IBAction func shootNowButtonWasTapped(_ sender: UIButton) {
        if setForCameoInvite {
            showShootNowCameoAlert()
        }
        else {
            performSegue(withIdentifier: "shootNowSegue",
                         sender: nil)
        }
        
    }

    func showShootNowCameoAlert() {
        
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      sourceView: shootNowButton)
        
        alert.addAction(UIAlertAction(title: "Record a Duet with \(video!.user.fullName!)",
                                      style: .default,
                                      handler: {[weak self] (action) in
                                        self?.performSegue(withIdentifier: "recordCameoSegue",
                                                           sender:nil)
        }))
        alert.addAction(UIAlertAction(title: "Record a Solo",
                                      style: .default,
                                      handler: {[weak self] (action) in
                                        self?.performSegue(withIdentifier: "shootNowSegue",
                                                           sender: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel",
                                      style: .cancel,
                                      handler: nil))
        
        present(alert,
                animated: true,
                completion: nil)
    }
    
    @IBAction func videoWasTapped(_ sender: AnyObject)
    {
        if let _ = timer {
            timer?.invalidate()
            timer = nil
            likeVideo(canUnlike: false)
        } else {
            timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(playPauseVideo), userInfo: nil, repeats: false)
        }
    }
    
    func playPauseVideo() {
        timer = nil
        AmplitudeEvents.shared.pendingView = nil
        guard let video = video else {
            return
        }
        if (VideoPlayerManager.shared.isPlaying(video.videoUrl, on: thumbnailImage)) {
            VideoPlayerManager.shared.player.pause()
            playButton.setImage(UIImage(named: "video_play"), for: .normal)
            if landscape {
                fade(in: infoBarView)
            }
        } else {
            //VideoPlayerManager.shared.setNewPlayerView(view: thumbnailImage, videoURL: video.videoUrl, withDelegate: self)
            prepareToPlay(video)
            //AmplitudeEvents.shared.logVideoView(forVideoID: video.videoId, forPageName: AmplitudeEvents.kVideoHomePageName)
            playButton.setImage(nil, for: .normal)
            if landscape {
                fadeOutView(infoBarView)
            }
        }
    }
    
    func playerDidReachEnd() {
        guard let video = video, appIsInForeground else { return }
        Utils.dispatchOnMainThread {[weak self] in
            guard let sSelf = self else { return }
            //VideoPlayerManager.shared.setNewPlayerView(view: sSelf.thumbnailImage, videoURL: video.videoUrl, withDelegate: sSelf)
            sSelf.prepareToPlay(video)
        }
    }
    
    
    func playerDidPlay() {
        guard let _ = video, !didSendView else { return }
        didSendView = true
        AmplitudeEvents.shared.logVideoView(videoID: video!.videoId,
                                            pageName: AmplitudeEvents.kVideoHomePageName,
                                            videoName: video!.song_name,
                                            artistName: video!.artist_name)
    }
    
    func prepareToPlay(_ video: SIAVideo!) {
       
        let playVideoClosure = {[weak self] in
            guard let _ = self else { return }
            VideoPlayerManager.shared.setNewPlayerView(view: self!.thumbnailImage,
                                                       videoURL: video.videoUrl,
                                                       withDelegate: self)
            self!.view.setNeedsLayout()
        }
        if SIAPushRegistrationHandler.shared().interstitialIsShown {
            SIAPushRegistrationHandler.shared().registrationCompletionHandler = playVideoClosure
        }
        else {
            playVideoClosure()
        }
    }
    
    func likeVideo(canUnlike: Bool = true) {
        guard let video = video, video.status != "secret" else {
            return
        }
        
        if (!canUnlike || !video.isLikedByMe) {
            LikeAnimator.likeAnimation(on: thumbnailImage)
        }
        
        if (!canUnlike && video.isLikedByMe) {
            return // cant unlike a video by double tap
        }
        
        if video.isLikedByMe {
            //change to white like image
            video.total_likes = String(Int((video.total_likes)!)! - 1)
            likeButton.setImage(UIImage(named:"icon_like.png"), for: .normal)
        } else {
            //change to red like image
            likeButton.setImage(UIImage(named:"icon_liked.png"), for: .normal)
            video.total_likes = String(Int((video.total_likes)!)! + 1)
        }
        likeButton.setTitle(video.totalLikesStr, for: .normal)
        
        video.toggleLike(completion: nil,
                         shouldChangeLikesValue: false,
                         withAmplitudePageName: AmplitudeEvents.kHomePageName)
    }
    
    @IBAction func likeTapped(_ sender: AnyObject) {
        likeVideo()
    }
    
    func profileButtonWasTapped(_ user: SIAUser) {
        performSegue(withIdentifier: "profileSegue", sender: 0)
    }
    
    func presentUserBarAlert(_ viewController: UIViewController) {
        present(viewController, animated: true, completion: nil)
    }
    
    func videoWasMadePrivate(_ video: SIAVideo) {
        //dont care
    }
    
    func videoWasDeleted(_ video: SIAVideo) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func makeCameoWasTapped(_ video: SIAVideo) {
        performSegue(withIdentifier: "recordCameoSegue",
                     sender: video)
    }
    
    func interactionEnabled(_ enabled: Bool) {
        interactionIsEnabled = enabled
    }
    
    func goToCameoInvite() {
        self.performSegue(withIdentifier: "cameoInviteSegue", sender: self)
    }
    
    //MARK: init view methods
    func prepareDataFromVideo(_ video: SIAVideo) {
        video.delegate = self
        //Set profile image
        songNameLabel.text = video.song_name
        artistNameLabel.text = video.artist_name
        commentsButton.setTitle(video.totalCommentsStr, for: .normal)
        likeButton.setTitle(video.totalLikesStr, for: .normal)
        //Set title
        let videoIsSecret = video.status == "secret"
        // this can't be nil because who knows why
        var title = videoIsSecret ? "Submission" : video.song_name ?? ""
        if video.isCameo() {
            title += " Duet"
            navigationController?.setNavigationItemTitle(title)
            navigationController?.setTitleToTruncateMiddle()
        }
        else {
            navigationController?.setNavigationItemTitle(title)
        }
        songDataButton.isEnabled = !videoIsSecret
        shootNowButtonContainer.isHidden = videoIsSecret
        buttonBarView.isHidden = videoIsSecret
        songNameLabel.isHidden = videoIsSecret
        artistNameLabel.isHidden = videoIsSecret
        cameoIcon.isHidden = !video.isCameo()
        
        if setUIForInviteToCameo {
            cameoButtonContainer.isHidden = false
            videoInfoToCameoButtonConstraint.isActive = true
            shootNowButtonContainer.removeFromSuperview()
            if (fromRecordFlow && (video.user.total_performances.intValue - 1) % 5 == 0)
            {
                cameoPopup = CameoPopup(forView: view,
                                        delegate: self)
                cameoPopup?.show(onView: view)
                cameoPopupIsDisplayed = true
                navigationController.setBottomBarHidden(true, animated: false)
            }
        }
        
        SDWebImageManager.shared().loadImage(
            with: video.firstFrameUrl,
            options: .refreshCached,
            progress: nil) { (image, data, error, cacheType, complete, url) in
                if image != nil {
                    self.thumbnailImage.image = image
                }
                Utils.dispatchOnMainThread({ [weak self] in
                    self?.spinnerView.stopAnimation()
                    UIView.animate(
                        withDuration: 0.3,
                        delay: 0,
                        options: .curveEaseInOut,
                        animations: {
                            self?.mainView.alpha = 1
                        },
                        completion: nil)
                    guard let sSelf = self, let _ = image else { return }
                    UIView.transition(with: sSelf.backgroundImage, duration: 1.4, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                            sSelf.backgroundImage.image = image
                        }, completion: nil)
                })
        }
 
        //setPhotoAndBackground(video.firstFrameUrl != nil ? video.firstFrameUrl : video.thumbnailUrl)
           
        if (video.isLikedByMe)
        {
            likeButton.imageView?.image = UIImage(named: "icon_liked")
        }
        else {
            likeButton.imageView?.image = UIImage(named: "icon_like")
        }
        
        if video.status != "secret" && fromRecordFlow && RateTheAppViewController.is24HoursSinceInstall() && (!RateTheAppViewController.hasShown() || RateTheAppViewController.shouldResurface()) {
            (UIApplication.shared.delegate as! AppDelegate).showRateTheApp()
        }
    }
    
    func getVideoDataFromApi(_ videoId: String) {
            SIAVideo.getDataForVideoId(videoId, withCompletionHandler: {[weak self] (video: SIAVideo?) -> Void in
                guard let sSelf = self else {
                    return
                }
                sSelf.video = video
                if (video?.status == "secret") {
                    sSelf.shootNowButton?.isHidden = true
                }
                if let _ = video {
                    sSelf.didPlay = true
                    //VideoPlayerManager.shared.setNewPlayerView(view: sSelf.thumbnailImage, videoURL: video!.videoUrl, withDelegate: sSelf)
                    sSelf.prepareToPlay(video!)
                    //AmplitudeEvents.shared.logVideoView(forVideoID: video!.videoId, forPageName: AmplitudeEvents.kVideoHomePageName)
                    Utils.dispatchOnMainThread({
                        sSelf.prepareDataFromVideo(sSelf.video!)
                        sSelf.userBarView.video = sSelf.video
                    })
                }
            })
    }
    
    func videoTotalsDidChange(_ video: SIAVideo) {
        commentsButton.setTitle(video.totalCommentsStr, for: .normal)
        likeButton.setTitle(video.totalLikesStr, for: .normal)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        print("**** ´\\\\_|O _ O|_//` **** \n**** oh me so deinit ****\n*************************")
    }
    
}
