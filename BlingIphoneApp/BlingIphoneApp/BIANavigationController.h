//
//  BIANavigationController.h
//  
//
//  Created by Zach on 18/09/2016.
//
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, BottomBarButtonType) {
    BottomBarButtonTypeHomeButton = 0,
    BottomBarButtonTypeDiscoverButton = 1,
    BottomBarButtonTypeRecordButton = 2,
    BottomBarButtonTypeNotificationsButton = 3,
    BottomBarButtonTypeProfileButton = 4
};

@class SIAVideo;
@interface BIANavigationController : UINavigationController

@property (strong, nonatomic) IBOutlet UIView *bottomBar;
@property (strong, nonatomic) NSMutableArray<UIViewController*> *tabViewControllers;
@property (strong, nonatomic) IBOutlet UIView *notificationBadgeView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *notificationBadgeViewWidth;
@property (strong, nonatomic) IBOutlet UILabel *notificationBadgeLabel;
@property (nonatomic) BOOL bottomBarIsHidden;
@property (nonatomic) BOOL shouldPopRight;

@property (nonatomic) BOOL shouldShowSplashScreen;
- (void)setSelected:(BOOL)selected forBottomButton:(BottomBarButtonType)button;
- (void)setBottomBarHidden:(BOOL)hidden
                  animated:(BOOL)animated;

- (BOOL)bottomBarIsHidden;

- (void)back;

- (void)setNavigationItemTitle:(NSString *)newTitle;
- (void)setTitleToTruncateMiddle;
- (void) switchToProfileViewControllerWithUserId: (NSString*) userId;
- (void)switchToVideoHomeForVideoId:(NSString *) videoId;
- (void)switchToVideoHomeFromCameoInviteForVideoId:(NSString *)videoId;
- (void)switchToVideoHomeForVideoId:(NSString *)videoId
                       showComments:(BOOL)showComments;
- (void)switchToDiscoverForVideoId:(NSString *)videoId;
- (void)switchToDiscoverForClipId:(NSString *)clipId;
- (void)switchToRecordWithSearchTerm:(NSString *)search;
- (void)switchToInvite;
- (void)switchToLeaderboard;
- (void)setNavigationBarLeftButtonMenu;
- (void)setNavigationBarLeftButtonBack;
/////////
- (void) setNavigationBarLeftButtonBackForVideoHome;
/////////
- (void)clearNavigationItemLeftButton;
- (void)setNavigationBarLeftButtonXActionBack;
- (void) setNavigationBarLeftButtonBackFromMenu;
- (void) setNavigationBarLeftButtonForEditProfile:(SEL)selector target:(id)target;
- (void) setNavigationBarLeftButtonXForNotifications;
- (void) setNavigationBarLeftButtonXWithAction:(SEL)selector;
- (void) setNavigationBarLeftButtonBackWithAction:(SEL)selector;

- (void) setNavigationBarLeftButtonImage:(UIImage *)image target:(id)target action:(SEL)selector;

- (void)setNavigationBarRightButtonImage:(UIImage *)image target:(id)target action:(SEL)selector;
- (void)setNavigationBarRightButtonNextTarget:(id)target
                                       action:(SEL)selector;
- (void)setNavigationBarRightButtonDoneTarget:(id)target
                                       action:(SEL)selector;
- (void) setNavigationBarRightButtons:(NSArray <UIButton *>*)buttons;
- (void)setNavigationLeftButtonXImageBackAction;
- (void)setNavigationBarRightButtonNotification;
- (void)setNavigationItemTitle:(NSString *)newTitle;
- (void)setNavigationItemRightButtonTitle:(NSString *)newTitle target:(id)targetObject action:(SEL)buttonAction;
- (void)reversePopViewController;
- (void)clearNavigationItemRightButton;
- (void)clearNavigationItemRightButtons;

- (void)clearNavigationBarImage;
- (void)setNavigationBarImage:(UIImage *)theImage;

- (void)hideNotificationBadge;
- (void)updateNotificationBadge;

@end
