//
//  SIANewMenuViewController.h
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 7/28/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import "SIABaseViewController.h"
@protocol ProfileWasEditedDelegate;

@interface SIANewMenuViewController : SIABaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UIImageView *closeMenuImageView;
@property (weak, nonatomic) SIAUser *user;
@property (weak, nonatomic) id <ProfileWasEditedDelegate> editProfileDelegate;

@end
