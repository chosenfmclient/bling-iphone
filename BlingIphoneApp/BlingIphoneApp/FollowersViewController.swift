//
//  FollowersViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 14/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class FollowersViewController: SIABaseViewController, UpdateFollowingDelegate, UITableViewDelegate, UITableViewDataSource, SearchableUserList, PageRequestHandlerDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var totalUsersLabel: UILabel!
    @IBOutlet weak var userTableView: UITableView!
    //MARK: properties
    var usersList: [SIAUser?] = [nil]
    var filteredUsers: [SIAUser?] = [nil]
    var userID: String? = "me"
    var isFollowers = true
    var currentSearch = ""
    var totalUsers: Int?
    
    var pageRequestHandler: PageRequestHandler?
    var delegate: UpdateFollowingDelegate?
    
    //MARK: View controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var path = "api/v1/users/%@/%@"
        if userID == nil {
            userID = "me"
        }
        if isFollowers {
            path = String(format: path, userID!, "followers")
        } else {
            path = String(format: path, userID!, "following")
        }
        pageRequestHandler = PageRequestHandler(apiPath: path, delegate: self)
        pageRequestHandler?.fetchData()
        
        setHeaderText()
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        userTableView.isUserInteractionEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setHeaderText() {
        guard let _ = totalUsersLabel else { return }
        if let _ = totalUsers {
            var baseText = "%@ %d people"
            if userID == "me" || userID == BIALoginManager.myUserId() {
                baseText = "You are %@ %d people"
            }
            if isFollowers {
                totalUsersLabel.text = String(format: baseText, "followed by", totalUsers!).capitalizingFirstLetter()
            } else {
                totalUsersLabel.text = String(format: baseText, "following", totalUsers!).capitalizingFirstLetter()
            }
        }
    }
    
    func dataWasLoaded(page: Int, data: Any?, requestIdentifier: Any?) {
        if (usersList.count > 0 && usersList.last! == nil) {
            usersList.removeLast()
        }
        guard let followers = data as? Users, followers.data.count > 0 else {
            endOfContentReached(at: page, requestIdentifier: requestIdentifier)
            return
        }
        for user in followers.data {
            usersList.append(user)
        }
        usersList.append(nil)
        reloadSearch()
        userTableView?.reloadData()
    }
    
    func endOfContentReached(at page: Int, requestIdentifier: Any?) {
        if (usersList.count > 0 && usersList.last! == nil) {
            usersList.removeLast()
        }
        reloadSearch()
        userTableView?.reloadData()
        if page == 0 {
            //TODO: display no data view
        }
    }
    
    //MARK: Table view delegate methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let user = filteredUsers[indexPath.row] else {
            return tableView.dequeueReusableCell(withIdentifier: "loading", for: indexPath) as! SIALoadingTableViewCellBlack
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "user", for: indexPath) as! FollowerTableViewCell
        cell.delegate = self
        cell.position = indexPath.row
        
        if let imageURL = user.imageURL {
            cell.userImageView.sd_setImage(
                with: imageURL,
                placeholderImage: UIImage(named: "extra_large_userpic_placeholder.png")
            )
        } else {
            cell.userImageView.image = UIImage(named: "extra_large_userpic_placeholder.png")
        }
        
        cell.userNameLabel.text = user.fullName
        cell.user = user
        
        let followButtonImage = UIImage(
            named: user.followedByMe ? "icon_followed_grey" : "icon_follow_green"
        )
        if user.user_id == "me" || user.user_id == BIALoginManager.myUserId() {
            cell.followButton.setImage(nil, for: .normal)
            cell.followButton.isEnabled = false
        } else {
            cell.followButton.setImage(followButtonImage, for: .normal)
            cell.followButton.isEnabled = true
        }
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredUsers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let _ = cell as? SIALoadingTableViewCellBlack {
            pageRequestHandler?.fetchData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? FollowerTableViewCell, let user = cell.user else { return }
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "profileViewController") as! ProfileViewController
        vc.userId = user.user_id
        if let nav = parent?.navigationController {
            tableView.isUserInteractionEnabled = false
            nav.pushViewController(vc, animated: true)
        }
    }
    
    func userWasFollowed(_ user: SIAUser) {
        reloadSearch()
        userTableView?.reloadData()
        
        if !isFollowers && (userID == "me" || userID == BIALoginManager.myUserId())  {
            if let total = totalUsers {
                totalUsers = total + 1
            } else {
                totalUsers = 1
            }
            setHeaderText()
        }
        
        delegate?.userWasFollowed(user)
    }
    
    func userWasUnfollowed() {
        reloadSearch()
        userTableView?.reloadData()
        
        if !isFollowers && (userID == "me" || userID == BIALoginManager.myUserId()) {
            if let total = totalUsers, total > 0 {
                totalUsers = total - 1
            } else {
                totalUsers = 0
            }
            setHeaderText()
        }
        
        delegate?.userWasUnfollowed()
    }

    
    func reloadSearch() {
        guard !currentSearch.isEmpty else {
            filteredUsers = usersList
            return
        }
        filteredUsers = []
        for user in usersList {
            guard let _ = user else { continue }
            if user!.fullName.lowercased().contains(currentSearch.lowercased()) {
                filteredUsers.append(user!)
            }
        }
        userTableView?.reloadData()
    }
    
    func newSearch(_ search: String) {
        guard search != currentSearch else { return }
        guard !search.isEmpty else { return }
        
        currentSearch = search
        reloadSearch()
    }
    
    func searchCanceled() {
        currentSearch = ""
        filteredUsers = usersList
        userTableView?.reloadData()
    }
}
protocol UpdateFollowingDelegate: class {
    func userWasUnfollowed()
    func userWasFollowed(_ user: SIAUser)
}

//MARK: cell class
class FollowerTableViewCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    weak var delegate: UpdateFollowingDelegate?
    var position: Int?
    
    weak var user: SIAUser?
    
    @IBAction func followButtonWasTapped(_ sender: AnyObject) {
        guard let user = user else { return }
        
        if user.followedByMe
        {
            user.unfollow(handler: { (💀) in },
                          withAmplitudePageName: AmplitudeEvents.kFollowersPageName,
                          withListPosition: position ?? NSNotFound)
            delegate?.userWasUnfollowed()
        }
        else
        {
            user.follow(handler: { (💀) in },
                        withAmplitudePageName: AmplitudeEvents.kFollowersPageName,
                        withListPosition: position ?? NSNotFound)
            delegate?.userWasFollowed(user)
        }
        
    }
    
    override func layoutSubviews() {
        userImageView.bia_rounded()
    }
}
