//
//  ThirdPartyAPIKeyWrapper.m
//  BlingIphoneApp
//
//  Created by Michael Biehl on 12/5/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import "PCHStringDefinesWrapper.h"

@implementation PCHStringDefinesWrapper

+ (NSString *)appseeAPIKey {
    return APPSEE_API_KEY;
}

+ (NSString *)amplitudeAPIKey {
    return AMPLITUDE_API_KEY;
}

+ (NSString *)flurryAPIKey {
    return FLURRY_API_KEY;
}

+ (NSString *)preloadedVideosPath {
    return PRELOADED_VIDEO_LIST_URL;
}

@end
