//
//  SIASettingsDataModel.h
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 8/7/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <RestKit/RestKit.h>
#import <Foundation/Foundation.h>


@interface SIASettingsDataModel : NSObject

@property bool push_notifications;
@property bool email_notifications;
@property bool high_quality_wifi_only;

@end
