//
//  VideoCollectionHandler.swift
//  BlingIphoneApp
//
//  Created by Zach on 10/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
import SDWebImage

protocol VideoCollectionHandlerDelegate: class {
    func collectionViewMovedUp()
    func collectionViewMovedDown()
    func lastCollectionCellWillDisplay()
}

//move up and down are optional
extension VideoCollectionHandlerDelegate {
    func collectionViewMovedUp() {
        
    }
    func collectionViewMovedDown() {
        
    }
}

@objc class VideoCollectionHandler: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK: properties
    weak var collectionView: UICollectionView?
    weak var viewController: UIViewController?
    fileprivate var videoList = [SIAVideo]()
    weak var collectionLoweredTopConstraint: NSLayoutConstraint?
    weak var collectionRaisedTopConstraint: NSLayoutConstraint?
    var collectionIsRaised = false
    var collectionIsMoving = false
    weak var delegate: VideoCollectionHandlerDelegate?
    
    var _shouldDisplayFooter = true
    var shouldDisplayFooter: Bool {
        get {
            return _shouldDisplayFooter
        }
        set(newValue) {
            _shouldDisplayFooter = newValue
            if (!newValue) {
                collectionIsMoving = true
                Utils.dispatchAfter(0.2) {[weak self] in
                    self?.collectionIsMoving = false
                }
            }
        }
    }
    
    var gestureRecognizerView = UIView()
    var didAddGestureRecognizer = false

    required init(
        collectionView: UICollectionView,
        viewController: UIViewController,
        loweredTopConstraint: NSLayoutConstraint,
        raisedTopConstraint: NSLayoutConstraint,
        delegate: VideoCollectionHandlerDelegate? = nil)
    {
        super.init()
        self.collectionView = collectionView
        self.viewController = viewController
        collectionLoweredTopConstraint = loweredTopConstraint
        collectionRaisedTopConstraint = raisedTopConstraint
        self.delegate = delegate
        
        //make sure that the collection is initialized as lowered
        collectionIsRaised = false
        collectionLoweredTopConstraint?.isActive = true
        collectionRaisedTopConstraint?.isActive = false
        collectionView.bounces = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    //MARK: CollectionView delegate methods
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let videoHomeVC = UIStoryboard(name: "VideoHomeStoryboard", bundle: nil).instantiateInitialViewController() as! VideoHomeViewController
        videoHomeVC.video = videoList[indexPath.item]
        viewController?.navigationController?.pushViewController(videoHomeVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if (shouldDisplayFooter) {
            return CGSize(width: UIScreen.main.bounds.size.width, height: 75)
        }
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reusableview: UICollectionReusableView?
        if (kind == UICollectionElementKindSectionFooter) {
            let footerview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footer", for: indexPath)
            reusableview = footerview
        }
        return reusableview!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "video", for: indexPath) as! VideoCollectionViewCell
        cell.video = videoList[indexPath.item]
        cell.thumbnail.image = nil
        if let webPURL = cell.video?.animatedURL
        {
            BIAWebPImageManager.shared().getImageFor(webPURL, withCompletion: { (image) in
                guard collectionView.cellForItem(at: indexPath) == cell else {
                    return
                }
                cell.thumbnail.image = image
            })
        }
        else
        {
            cell.thumbnail.sd_setImage(with: cell.video?.thumbnailUrl) { (image, error, cacheType, url) in
                if cacheType == .none { // if image was downloaded, fade it in
                    cell.thumbnail.alpha = 0
                    Utils.dispatchOnMainThread {[weak cell] in
                        UIView.animate(withDuration: 0.15, animations: {
                            cell?.thumbnail.alpha = 1
                        })
                    }
                }
            }
            cell.thumbnail.contentMode = .scaleAspectFill
        }
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.item == videoList.count - 1) {
            //Last cell displaying
            delegate?.lastCollectionCellWillDisplay()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let length = (UIScreen.main.bounds.width / 3) - 2
        return CGSize(width: length, height: length)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (collectionIsRaised && scrollView.contentOffset.y <= 0) {
            //at top
            lowerCollection()
        } else {
            if (!collectionIsRaised) {
                raiseCollection()
            }
        }
    }
    
    func setVideoList(_ videos: [SIAVideo]) {
        guard let collection = collectionView else { return }
        self.videoList = []
        self.videoList.append(contentsOf: videos)
        collectionView?.reloadData()
        
        //gesture recognizer
        if (!didAddGestureRecognizer) {
            didAddGestureRecognizer = true
            collection.superview?.addSubview(gestureRecognizerView)
            let upRecognizer = UISwipeGestureRecognizer.init(target: self, action: #selector(collectionWasSwipped))
            upRecognizer.direction = .up
            gestureRecognizerView.addGestureRecognizer(upRecognizer)
            let downRecognizer = UISwipeGestureRecognizer.init(target: self, action: #selector(collectionWasSwipped))
            downRecognizer.direction = .down
            gestureRecognizerView.addGestureRecognizer(downRecognizer)
            let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(collectionWasTapped))
            gestureRecognizerView.addGestureRecognizer(tapRecognizer)
            gestureRecognizerView.isUserInteractionEnabled = false
        }
        
    }
    
    func appendToVideoList(_ videos: [SIAVideo]) {
        self.videoList.append(contentsOf: videos)
        collectionView?.reloadData()
        enableGestureRecognizerIfNeeded()
    }
    
    func enableGestureRecognizerIfNeeded() {
        guard let collection = collectionView else { return }
        gestureRecognizerView.frame = collection.frame
        
        let cellHeight = (UIScreen.main.bounds.width / 3)
        let numRows = ceil(Double(Float(collection.numberOfItems(inSection: 0)) / Float(3)))
        let contentHeight = cellHeight * CGFloat(numRows)
        
        gestureRecognizerView.isUserInteractionEnabled =
            (contentHeight > 0 &&
             contentHeight <= collection.bounds.height)
    }
    
    func collectionWasSwipped(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .up) {
            raiseCollection()
        } else if (sender.direction == .down) {
            lowerCollection()
        }
    }
    
    func collectionWasTapped(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: collectionView?.superview)
        guard let collection = collectionView, let cells = collectionView?.visibleCells else { return }
        for cell in cells{
            if point.x > cell.frame.origin.x &&
                point.x < cell.frame.origin.x + cell.frame.size.width &&
            point.y > cell.frame.origin.y &&
                point.y < cell.frame.origin.y + cell.frame.size.width {
                //did select cell
                guard let indexPath = collection.indexPath(for: cell) else { continue }
                collectionView(collection, didSelectItemAt: indexPath)
                break
            }
        }
    }
    
    //MARK: rasing / lowering the collection
    func lowerCollection() {
        guard collectionIsRaised else {
            return
        }
        
        moveCollection(raise: false)
    }
    
    func raiseCollection() {
        guard !collectionIsRaised else {
            return
        }
        collectionView?.setContentOffset(CGPoint(x:0, y:0), animated: false)
        collectionView?.decelerationRate = 0
        
        moveCollection(raise: true)
    }
    
    private func moveCollection(raise: Bool) {
        guard !collectionIsMoving else {
            return
        }
        collectionIsMoving = true
        collectionLoweredTopConstraint?.isActive = !raise
        collectionRaisedTopConstraint?.isActive = raise
        UIView.animate(
            withDuration: 0.25,
            delay: 0,
            options: .curveEaseInOut,
            animations: {[weak self] in
                self?.viewController?.view.layoutIfNeeded()
            },
            completion: {[weak self] (🦁) in
                Utils.dispatchOnMainThread {
                    self?.collectionIsRaised = raise
                    self?.collectionView?.setContentOffset(CGPoint(x:0, y: raise ? 1 : 0), animated: false)
                    self?.collectionIsMoving = false
                    if (raise) {
                        self?.delegate?.collectionViewMovedUp()
                        self?.enableGestureRecognizerIfNeeded()
                    } else {
                        self?.gestureRecognizerView.isUserInteractionEnabled = false
                        self?.delegate?.collectionViewMovedDown()
                    }
                }
            })
    }
}

class VideoCollectionViewCell: UICollectionViewCell {
    var video: SIAVideo?
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var privateIcon: UIImageView?
    @IBOutlet weak var cameoIcon: UIImageView!
    
    override func layoutSubviews() {
        if let _ = video {
            privateIcon?.isHidden = video!.status != "private" &&
                video!.status != "secret"
            cameoIcon?.isHidden = !video!.isCameo()
        }
        
        super.layoutSubviews()
    }

}

class VideoCollectionViewFooter: UICollectionReusableView {
    @IBOutlet weak var spinner: SIASpinner!

    override func layoutSubviews() {
        spinner.startAnimation()
    }
}
