//
//  SIAUploadVideo.h
//  SingrIphoneApp
//
//  Created by SingrFM on 10/30/13.
//  Copyright (c) 2013 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "SIAVideoUploadDataModel.h"
#import "SIARecordingItem.h"
#import "SIAUploadUserImage.h"


#define VIDEO_NAME @"chosen_video.mp4"

@protocol SIAUploadVideoDelegate <NSObject>

-(void)finishUploading;
-(void)updateUploadProgress:(NSString *)percentString;
@optional

@end

@interface SIAUploadVideo : NSObject

@property (nonatomic, strong) NSString *parent_id;
@property (strong, nonatomic) NSString *song_id;
@property (strong, nonatomic) NSURL *soundtrack_url;
@property (strong, nonatomic) NSString *question_id;
@property (strong, nonatomic) NSString *artist;
@property (strong, nonatomic) NSString *song;

// bling video properties
@property (strong, nonatomic) NSString *song_clip_url;
@property (strong, nonatomic) NSString *song_image_url;
@property (strong, nonatomic) NSString *clip_id;
//


@property (nonatomic) NSInteger score;
@property (weak, nonatomic) id<SIAUploadVideoDelegate> delegate;
@property (strong, nonatomic) NSString *videoFileNameType;
@property (strong, nonatomic) NSString *thumbnailFileNameType;
@property (strong, nonatomic) NSString *genre;
@property (strong, nonatomic) NSString *type;
@property (nonatomic, strong) NSData   *videoData;
@property (nonatomic, strong) UIImage  *thumbnailImage;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSArray  *videoFilters;
@property (nonatomic, strong) NSArray  *audioFilters;
@property (nonatomic, strong) NSArray  *pickTheHookTimeRange;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *add_song;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic) NSUInteger frameIndex;
@property (nonatomic) CMTimeRange videoRange;
@property (nonatomic) BOOL isUploading;
@property (nonatomic) BOOL showProgressPopup;
@property (nonatomic) BOOL isVideoReady;
@property (nonatomic) BOOL filtersUpdated;
@property (strong, nonatomic) NSURL *recordURL;
@property (strong, nonatomic) NSString *video_signed_url;
@property (strong, nonatomic) NSString *video_method;
@property (strong, nonatomic) NSString *video_content_type;
@property (nonatomic) NSString *videoId;
@property (strong, nonatomic) SIAVideoUploadDataModel *dataModel;
@property (strong, nonatomic) AVAsset *uploadAsset;

// obsereved members
//@property (nonatomic) NSInteger uploadingProgressInPercents;
@property (nonatomic) BOOL finishedUploadingVideo;
@property (nonatomic) BOOL finishedUploadingThumb;
@property (nonatomic) BOOL finishedUpdatingVideo;
@property (nonatomic) BOOL finishedUpladQualityConvert;
@property (nonatomic) BOOL uploadCancelled;
@property (nonatomic, strong) NSString* uploadedPercent; // percent number of the uploaded video data - when uploading a video after recording, this var will be updated with the current percent of bytes-uploaded from bytes-to-be-upload. the uploading callback method will push values to this var, and the uploading screen will pull from this var to display the percent on the uploading screen.
@property (nonatomic, strong) NSString* thumbnailUploadedPercent;

+(SIAUploadVideo *) sharedUploadManager;
+ (RKObjectMapping *)startUploadObjectMapping;
-(void)uploadVideoWithCompletionHandler:(void(^)(NSString *videoId))handler
                         convertHandler:(void(^)(BOOL converted))convertHandler;

- (void)uploadVideoFromPath:(NSURL *)dataPath
                toSignedUrl:(NSString *)signedURL
                 withMethod:(NSString *)httpMethod
                contentType:(NSString *)contentType
              uploadHandler:(void(^)(BOOL uploaded))uploadHandler
             convertHandler:(void(^)(BOOL converted))convertHandler;

-(void) createVideoIdWithSetter:(void (^_Nonnull)(NSString *videoId))setter;

- (void) uploadData:(NSData *)data
        toSignedUrl:(NSString *)signedURL
         withMethod:(NSString *)httpMethod
        contentType:(NSString *)contentType
       withProgress:(BOOL)withProgress
withThumbnailProgress:(BOOL)withThumbProgress
  completionHandler:(void(^)(BOOL success))handler;

-(void) uploadProfilePicture:(UIImage *)profilePicture
                   imageData:(SIAUploadUserImage *)data
           completionHandler:(void(^)(BOOL success))handler;

-(void)uploadThumbnail:(UIImage *)thumbnail;
- (void) showStillUploadingMessage;
- (void) removeObservers;
- (void)removeFileNamed:(NSString *)fileName;

- (void)resetForNewUpload;
+ (RKObjectMapping *)updateVideoObjectMapping;

@end
