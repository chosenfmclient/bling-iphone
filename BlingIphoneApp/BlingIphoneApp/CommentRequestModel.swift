//
//  CommentRequestModel.swift
//  BlingIphoneApp
//
//  Created by Zach on 09/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class CommentRequestModel: NSObject {
    let video_id: String
    let text: String
    
    init(video_id: String, text: String) {
        self.video_id = video_id
        self.text = text
        super.init()
    }
    
    static func objectMapping() -> RKObjectMapping {
        let requestMapping = RKObjectMapping.request()
        requestMapping?.addAttributeMappings(from: ["video_id":"video_id", "text": "text"])
        return requestMapping!
    }
}
