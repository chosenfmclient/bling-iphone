//
//  SIAHookPickerView.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/24/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAHookPickerView.h"

#define PICK_THE_HOOK_SLIDER_HEIGHT (50.0)
#define NUM_OF_FRAMES (8)
#define THUMBNAIL_SIZE CGSizeMake(40,50)
#define PICK_THE_HOOK_TIME_INTERVAL (15.)

@interface SIAHookPickerView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) NSMutableArray *thumbsArray;
@property (nonatomic) CGSize thumbImageSize;
@property (strong, nonatomic) UIView *minChangingView;
@property (strong, nonatomic) UIView *maxChangingView;
@property (strong, nonatomic) AVAsset *videoAsset;
@property (nonatomic) CGFloat hookBegin;
@property (nonatomic) CGFloat hookEnd;
@property (nonatomic) CMTime assetDuration;

@end

@implementation SIAHookPickerView



- (void)setupForRecordingItem:(SIARecordingItem *)recordingItem delegate:(id <SIAHookPickerDelegate>)delegate {
    
    self.recordingItem = recordingItem;
    self.delegate = delegate;
    
    UIImage *minImage = [[UIImage imageNamed:@"select-hook-dark-bg.png"]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(5, 0, 0, 0)];
    UIImage *maxImage = [[UIImage imageNamed:@"select-hook-dark-bg.png"]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
    
    AVURLAsset *asset = [AVURLAsset assetWithURL:recordingItem.recordURL];
    CGFloat duration = CMTimeGetSeconds(asset.duration);
    CGFloat hookFrameWidth = ceilf(15.0 / duration * SCREEN_WIDTH);
    
    hookFrameWidth = MAX(hookFrameWidth, 30); // don't let it go below 30 that's just too dang small
    
    UIView *hookFrame = [[UIView alloc] initWithFrame:CGRectMake(0,0, hookFrameWidth, 50)];
    hookFrame.layer.borderColor = [[[SIAStyling defaultStyle] recordingRedColor] CGColor];
    hookFrame.layer.borderWidth = 3.0;
    hookFrame.layer.backgroundColor = [[UIColor clearColor] CGColor];
    UIGraphicsBeginImageContextWithOptions(hookFrame.bounds.size, NO, 0);
    [hookFrame.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *thumbImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
//    if (SCREEN_WIDTH != 568) {
//        CGSize imageSize = CGSizeMake(thumbImage.size.width - (568 - SCREEN_WIDTH) / 2,
//                                      PICK_THE_HOOK_SLIDER_HEIGHT);
//        UIGraphicsBeginImageContext(imageSize);
//        [thumbImage drawInRect:CGRectMake(0,
//                                          0,
//                                          imageSize.width,
//                                          imageSize.height)];
//        thumbImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//    }
    
    _thumbImageSize = thumbImage.size;
    
    [self.pickSlider setMaximumTrackImage:minImage
                                 forState:UIControlStateNormal];
    [self.pickSlider setMinimumTrackImage:maxImage
                                 forState:UIControlStateNormal];
    [self.pickSlider setThumbImage:thumbImage
                          forState:UIControlStateNormal];
    
    [self.pickSlider setFrame:self.bounds];
    
    [self getVideoThumbs:self.recordingItem.recordURL];
   // [self.previewParent.player setRate:0];
    
    _minChangingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, (SCREEN_WIDTH-_thumbImageSize.width)*self.pickSlider.value, PICK_THE_HOOK_SLIDER_HEIGHT)];
    _maxChangingView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width - (SCREEN_WIDTH-_thumbImageSize.width)*(1-self.pickSlider.value), 0, (self.pickSlider.frame.size.width-_thumbImageSize.width)*(1-self.pickSlider.value), PICK_THE_HOOK_SLIDER_HEIGHT)];
    _minChangingView.backgroundColor = _maxChangingView.backgroundColor = [UIColor blackColor];
    _minChangingView.alpha = _maxChangingView.alpha = 0.7;
    [self addSubview:_minChangingView];
    [self addSubview:_maxChangingView];
    //[self.previewParent stopRepeatingPlayback];
    
    // set initial hook
    if (self.hookBegin == 0) {
        CMTime sliderTime = CMTimeMultiplyByFloat64(self.assetDuration, self.pickSlider.value);
        self.hookBegin = CMTimeGetSeconds(sliderTime) - PICK_THE_HOOK_TIME_INTERVAL / 2;
    }
    if (self.hookBegin < 0){
        self.hookBegin = 0;
    } else if (self.hookBegin > (CMTimeGetSeconds(self.assetDuration) - PICK_THE_HOOK_TIME_INTERVAL) ){
        self.hookBegin = CMTimeGetSeconds(self.assetDuration) - PICK_THE_HOOK_TIME_INTERVAL;
    }
    self.hookEnd = self.hookBegin + PICK_THE_HOOK_TIME_INTERVAL < CMTimeGetSeconds(self.assetDuration) ?
    self.hookBegin + PICK_THE_HOOK_TIME_INTERVAL :
    CMTimeGetSeconds(self.assetDuration);
}

//- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
//{
//    return 1;
//}
//
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
//{
//    return _thumbsArray.count;
//}
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"thumbCell" forIndexPath:indexPath];
//    if (_thumbsArray.count >= indexPath.row){
//        if (![_thumbsArray[indexPath.row] isKindOfClass:[NSNull class]] ){
//            CGRect cellBounds = cell.bounds;
//            UIImage *smallImage = _thumbsArray[indexPath.row];//[self imageWithImage:_thumbsArray[indexPath.row]scaledToSize:cellSize];
//            cellBounds.size.width = smallImage.size.width;
//            cell.bounds = cellBounds;
//            cell.backgroundColor = [UIColor colorWithPatternImage:smallImage];
//        }
//    }
//    return cell;
//}
//

- (void)addImages {
    for (NSInteger imageIndex = 0; imageIndex < _thumbsArray.count; imageIndex++) {
        id thumb = _thumbsArray[imageIndex];
        
        if(![thumb isKindOfClass:[UIImage class]])
            continue;
        UIImageView *thumbView = [[UIImageView alloc] initWithImage:thumb];
        CGRect frame = thumbView.frame;
        frame.size = CGSizeMake(frame.size.width / 2, frame.size.height / 2);
        frame.origin.x = frame.size.width * imageIndex;
        thumbView.frame = frame;
        thumbView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:thumbView];
        [self sendSubviewToBack:thumbView];
    }
}

//- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
//    
//    if (image.size.width < image.size.height) {
//        UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//        CGContextRef context = UIGraphicsGetCurrentContext();
//        UIColor *fillColor = [UIColor blackColor];
//        [fillColor setFill];
//        CGContextFillRect(context, CGRectMake(0, 0, newSize.width, newSize.height));
//        UIImage *blackBorder = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//        [blackBorder drawInRect:CGRectMake(0, 0, blackBorder.size.width, blackBorder.size.height)];
//        [image drawInRect:CGRectMake(blackBorder.size.width / 2 - image.size.width / 2, 0, image.size.width, image.size.height)];
//        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//        return newImage;
//    }
//    
//    else {
//        UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//        [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
//        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        return newImage;
//    }
//    
//    
//}

- (IBAction)sliderChanged:(UISlider *)sender {
    _minChangingView.frame = CGRectMake(self.pickSlider.frame.origin.x, 0, (self.pickSlider.frame.size.width-_thumbImageSize.width)*self.pickSlider.value, PICK_THE_HOOK_SLIDER_HEIGHT);
    _maxChangingView.frame = CGRectMake(self.frame.size.width - (self.pickSlider.frame.size.width-_thumbImageSize.width)*(1-self.pickSlider.value), 0, (self.pickSlider.frame.size.width-_thumbImageSize.width)*(1-self.pickSlider.value), PICK_THE_HOOK_SLIDER_HEIGHT);
}

- (IBAction)finishPicking:(UISlider *)sender {
    //[Flurry logEvent:FLURRY_DRAG_HOOKSELECTOR_POSTRECORD_EDITVIDEO];
    CMTime sliderTime = CMTimeMultiplyByFloat64(self.assetDuration, sender.value);
    self.hookBegin = CMTimeGetSeconds(sliderTime) - PICK_THE_HOOK_TIME_INTERVAL / 2;
    if (self.hookBegin < 0){
        self.hookBegin = 0;
    } else if (self.hookBegin > (CMTimeGetSeconds(self.assetDuration) - PICK_THE_HOOK_TIME_INTERVAL) ){
        self.hookBegin = CMTimeGetSeconds(self.assetDuration) - PICK_THE_HOOK_TIME_INTERVAL;
    }
    NSLog(@"\nduration: %f;\nvalue: %f;\nhook begin: %f", CMTimeGetSeconds(self.assetDuration), sender.value, self.hookBegin);
    
    
    self.hookEnd = self.hookBegin + PICK_THE_HOOK_TIME_INTERVAL < CMTimeGetSeconds(self.assetDuration) ?
    self.hookBegin + PICK_THE_HOOK_TIME_INTERVAL :
    CMTimeGetSeconds(self.assetDuration);
    if (self.hookEnd == CMTimeGetSeconds(self.assetDuration)) {
        self.hookBegin = self.hookEnd - PICK_THE_HOOK_TIME_INTERVAL;
        if (self.hookBegin < 0) {
            self.hookBegin = 0;
        }
    }
    
    [self.delegate hookTimeChangedWithStart:self.hookBegin
                                        end:self.hookEnd];
    
  //  [Flurry logEvent:FLURRY_DRAG_HOOKSELECTOR_POSTRECORD_EDITVIDEO];
}


-(void)setHookBegin:(CGFloat)hookBegin{
    NSLog(@"duration: %f", CMTimeGetSeconds(self.videoAsset.duration));
    _hookBegin = hookBegin;
}

- (CGSize)getScaledVideoSizeForTrack:(AVAssetTrack *)track {
    CGSize size = track.naturalSize;
    CGAffineTransform preferredTransform = track.preferredTransform;
    CGRect rectForTransform = CGRectZero;
    rectForTransform.size = size;
    rectForTransform = CGRectApplyAffineTransform(rectForTransform, preferredTransform);
    size = rectForTransform.size; // we must apply the preferred transform to the size of the video in order to get the true width / height for the video
    
    CGFloat scale = fabs(self.bounds.size.height / size.height); // double and then put them in half-sized frames later
    
    return CGSizeApplyAffineTransform(size, CGAffineTransformMakeScale(scale, scale));
}


-(void)getVideoThumbs:(NSURL *)videoURL
{
    _videoAsset = [AVURLAsset assetWithURL:videoURL];
    //    self.assetDuration = _videoAsset.duration;
    AVAssetTrack *track = [[_videoAsset tracksWithMediaType:AVMediaTypeVideo] lastObject];
    CGSize scaledSize = [self getScaledVideoSizeForTrack:track];
    NSInteger numOfFrames = ceilf(self.frame.size.width / scaledSize.width);
    CMTime duration = CMTimeConvertScale(_videoAsset.duration, 1, kCMTimeRoundingMethod_QuickTime);
    self.assetDuration = duration;
    CMTime timeBetweenThumbs = CMTimeMultiplyByRatio(duration, 1, numOfFrames - 1);
    NSMutableArray *timesArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < numOfFrames; i++){
        CMTime temp;
        if (i == 0){
            temp = CMTimeMakeWithSeconds(0, 1);
        } else {
            temp = CMTimeMultiplyByRatio(timeBetweenThumbs, i, 1);
        }
        [timesArray addObject:[NSValue valueWithCMTime:temp]];
    }
    NSLog(@"thumbs time array: %@", timesArray);
    AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:_videoAsset];
    generator.appliesPreferredTrackTransform = TRUE;
    _thumbsArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < timesArray.count; i++)
    {
        [_thumbsArray addObject:[NSNull null]];
    }
    
    __block NSInteger i = 0;
    generator.maximumSize = CGSizeMake(scaledSize.width*2, scaledSize.height*2);
    [generator generateCGImagesAsynchronouslyForTimes:timesArray completionHandler:^(CMTime requestedTime, CGImageRef image, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error) {
        
        if (result != AVAssetImageGeneratorSucceeded) {
            return;
        }
        
        UIImage *generatedImage = [UIImage imageWithCGImage:image];
        
        NSInteger idx = [timesArray indexOfObject:[NSValue valueWithCMTime:requestedTime]];
        if (generatedImage) {
            [_thumbsArray replaceObjectAtIndex:idx withObject:generatedImage];
            ++i;
            if (i >= numOfFrames) {
                dispatch_async(dispatch_get_main_queue(), ^{
//                    self.thumbsCollectionView.delegate = self;
//                    self.thumbsCollectionView.dataSource = self;
//                    [self.thumbsCollectionView reloadData];
                    [self addImages];
                });
            }
        }
    }];
}

@end
