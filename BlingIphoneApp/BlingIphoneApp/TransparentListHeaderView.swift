//
//  TransparentListHeaderView.swift
//  BlingIphoneApp
//
//  Created by Zach on 27/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

@objc class TransparentListHeaderView: UIView {
    
    @IBOutlet weak var label: UILabel!
    
    var view: UIView?
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        view = (Bundle.main.loadNibNamed("TransparentListHeader", owner: self, options: nil)?[0] as! UIView)
        view?.frame = frame
        self.addSubview(view!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
