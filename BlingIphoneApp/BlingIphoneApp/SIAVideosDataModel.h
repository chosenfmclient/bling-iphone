//
//  SIAPerformancesModel.h
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 03/08/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "SIAPaginatorDataModel.h"

#define PERFORMANCES_TYPE @"performances"
#define RESPONSES_TYPE @"responses"
#define BIOS_TYPE @"bios"

typedef NS_ENUM(NSUInteger, SIAVideoSortType) {
    SIAVideoSortTypeRankHighToLow,
    SIAVideoSortTypeDateAdded,
    SIAVideoSortTypeViews
};

typedef NS_ENUM(NSUInteger, SIAVideoFilterType) {
    SIAVideoFilterTypeAll,
    SIAVideoFilterTypePublic,
    SIAVideoFilterTypePrivate
};

@interface SIAVideosDataModel : SIAPaginatorDataModel

@property (strong, readonly, nonatomic) NSArray *sortedByDateAdded;
@property (strong, readonly, nonatomic) NSArray *sortedByMostViewd;
@property (strong, readonly, nonatomic) NSArray *sortedByRankHighToLow;
@property (strong, nonatomic) NSString *videoType;
@property (strong, nonatomic) NSString *userId;
@property (nonatomic) SIAVideoSortType sortType;
@property (nonatomic) SIAVideoFilterType filterType;

@end
