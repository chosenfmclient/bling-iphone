//
//  HomeResponseModel.swift
//  BlingIphoneApp
//
//  Created by Zach on 06/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class HomeResponseModel: NSObject {
    var video_feed = [SIAVideo]()
    
    static func objectMapping() -> RKObjectMapping {
        let responseMapping = RKObjectMapping(for: HomeResponseModel.self)
        
        responseMapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "video_feed", toKeyPath: "video_feed", with: SIAVideo.objectMapping()))
        
        return responseMapping!
    }
}
