//
//  Spinner.swift
//  ChosenIphoneApp
//
//  Created by Hen Levy on 28/06/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

import UIKit
import QuartzCore

public class SIASpinner: UIImageView {
    
   // @IBInspectable public var animating: Bool = false
    
    var containerLayer = CAShapeLayer()
    
    var rotationAnimation: CABasicAnimation = {
        var tempAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        tempAnimation.toValue = Float(M_PI)
        tempAnimation.duration = CFTimeInterval(0.5)
        tempAnimation.isCumulative = true
        tempAnimation.repeatCount = 10000
        tempAnimation.isRemovedOnCompletion = false
        return tempAnimation
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        image = UIImage(named: "extra_large_icon_loading")?.withRenderingMode(.alwaysTemplate)
        contentMode = .scaleAspectFit
        layer.shouldRasterize = true
        layer.addSublayer(containerLayer)
    }
    
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupView()
    }
    
    private func setupView() {
        backgroundColor = UIColor.clear
        draw(frame)
        if isAnimating {
            startAnimation(shouldLayout: false)
        } else {
            stopAnimation()
        }
    }
    
    public override func draw(_ rect: CGRect) {
        image = UIImage(named: "extra_large_icon_loading")?.withRenderingMode(.alwaysTemplate)
        contentMode = .scaleAspectFit
        layer.shouldRasterize = true
        layer.addSublayer(containerLayer)
    }
    
    // MARK: Actions
    
    func startAnimation(shouldLayout: Bool = true) {
        guard isHidden else {
            return
        }
        isHidden = false
        layer.add(rotationAnimation, forKey: "")
        if (shouldLayout) {
            self.superview?.layoutIfNeeded()
        }
    }
    
    func stopAnimation() {
        isHidden = true
        layer.removeAllAnimations()
    }
}
