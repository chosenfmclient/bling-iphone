//
//  SIAFollowListModel.m
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 03/08/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAFollowListModel.h"
#import "SIAUser.h"

@interface SIAFollowListModel ()

@property (strong, readwrite, nonatomic) NSArray *usersList;

@end

@implementation SIAFollowListModel

- (RKObjectMapping *)objectMapping{
    
    return [SIAUser objectMapping];
}

- (NSString *)pagePath {
    
    return nil;
}

- (NSDictionary *)requestParams {
    
    return nil;
}

@end
