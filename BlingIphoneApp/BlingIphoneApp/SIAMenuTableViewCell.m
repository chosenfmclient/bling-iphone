//
//  SIAMenuTableViewCell.m
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 7/28/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import "SIAMenuTableViewCell.h"

#define GRAY_ARROW_IMAGE  @"menu-arrow-grey.png"
#define WHITE_ARROW_IMAGE @"menu-arrow-white.png"

@implementation SIAMenuTableViewCell
@end
