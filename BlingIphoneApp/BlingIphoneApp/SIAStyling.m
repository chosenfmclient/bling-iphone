//
//  SIAStyling.m
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/3/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAStyling.h"

@implementation UIColor (HtmlColor)

+ (UIColor *) colorFromHexString:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

@end

@implementation SIAStyling

+ (instancetype)defaultStyle {
    static dispatch_once_t once;
    static SIAStyling *shared;
    dispatch_once(&once, ^{
        shared = [[SIAStyling alloc] init];
    });
    return shared;
};

+ (void)view:(UIView *)view withRoundedCorners:(UIRectCorner)corners radii:(CGSize)radii {
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                     byRoundingCorners:corners
                                           cornerRadii:radii];

    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

- (UIColor *)blingMainColor {
    return [UIColor colorWithRed:39. / 255 green:167. / 255 blue:222. / 255 alpha:1.];
}

- (UIColor *)registrationFieldPlaceholderColor {
    return [UIColor colorWithRed:139.0 / 255.0 green:144.0 / 255.0 blue:164.0 / 255.0 alpha:1.0];
}

- (UIColor *)registrationFieldTextColor {
    return [UIColor colorWithRed:56.0 / 255.0 green:61.0 / 255.0 blue:80.0 / 255.0 alpha:1.0];
}

- (UIColor *)blingModeColor {
    return [UIColor colorFromHexString:@"c50080"];
}

- (UIColor *)recordingRedColor {
    return [UIColor colorFromHexString:@"ed1f24"];
}

- (UIColor *)cameoPink {
    return [UIColor colorFromHexString:@"e40084"];
}

- (UIColor *)cameoNotificationPink {
    return [UIColor colorFromHexString:@"ffd6e8"];
}

- (UIColor *)recordingOptionsButtonsRed {
    return [UIColor colorFromHexString:@"eb2227"];
}

- (UIColor *)recordingModeHighlightColor {
    return [UIColor colorFromHexString:@"FDDB13"];
}

- (void)textField:(UITextField *)textField withRegistrationStyleAndImage:(UIImage *)image {
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName : [self registrationFieldPlaceholderColor]}];
    textField.textColor = [self registrationFieldTextColor];
    textField.backgroundColor = [UIColor whiteColor];
    textField.textAlignment = NSTextAlignmentLeft;
    textField.font = kAppFontBold(15.0);

    textField.autocorrectionType = UITextAutocorrectionTypeNo;

    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0, 0, image.size.width + 20, image.size.height);
    imageView.contentMode = UIViewContentModeCenter;
    textField.leftView = imageView;
    textField.leftViewMode = UITextFieldViewModeAlways;

    textField.rightViewMode = UITextFieldViewModeAlways;
    textField.rightView.hidden = YES;
}

- (UIImage *)imageFromView:(UIView*)view
{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage*)blurredImageFromView:(UIView *)view
{
    UIImage *image = [self imageFromView:view];
    return [self blurredImageFromImage:image];
}

- (UIImage*)blurredImageFromImage:(UIImage*)image withRadius:(CGFloat)radius
{
    if(!image)
        return nil;
    
//    GPUImageiOSBlurFilter *blurFilter = [[GPUImageiOSBlurFilter alloc] init];
//    blurFilter.blurRadiusInPixels = radius;
    
    CIImage *ciImage = [CIImage imageWithCGImage:image.CGImage];
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:ciImage forKey:kCIInputImageKey];
    [filter setValue:@(radius) forKey:kCIInputRadiusKey];
    
    CIImage *outputCIImage = filter.outputImage;
    CIContext *context = [CIContext contextWithOptions:nil];
    
    return [UIImage imageWithCGImage:
            [context createCGImage:outputCIImage
                          fromRect:CGRectMake(0,
                                              0,
                                              ciImage.extent.size.width - 30,
                                              ciImage.extent.size.height - 30)
             ]];
}

- (UIImage *)blurredImageFromImage:(UIImage *)image
{
    return [self blurredImageFromImage:image withRadius:15.0f];
}

- (UIColor *)averageColorFromImage:(UIImage*) image {
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char rgba[4];
    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), image.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    if(rgba[3] > 0) {
        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
        CGFloat multiplier = alpha/255.0;
        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
                               green:((CGFloat)rgba[1])*multiplier
                                blue:((CGFloat)rgba[2])*multiplier
                               alpha:1];
    }
    else {
        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
                               green:((CGFloat)rgba[1])/255.0
                                blue:((CGFloat)rgba[2])/255.0
                               alpha:((CGFloat)rgba[3])/255.0];
    }
}

@end

@implementation UIView (SIAStyling)
- (void)spinWithDuration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat {
    CABasicAnimation *rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2.0 /* full rotation*/ * rotations * duration];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    rotationAnimation.removedOnCompletion = NO;

    [self.layer setShouldRasterize:YES];
    [self.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}
@end
