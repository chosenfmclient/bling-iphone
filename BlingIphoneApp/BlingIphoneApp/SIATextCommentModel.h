//
//  SIATextCommentModel.h
//  ChosenIphoneApp
//
//  Created by Joseph Nahmias on 12/01/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SIATextComment.h"

@interface SIATextCommentModel : SIAPaginatorDataModel

@property (nonatomic) NSUInteger totalComments;
@property (nonatomic) NSString *videoId;
@property (nonatomic, strong) NSArray<SIATextComment *> *commentsArray;
+ (RKObjectMapping *)commentsMapping;
+ (void)getCommentsForVideoId:(NSString *)videoId withCompletionHandler:(void(^)(SIATextCommentModel *model))handler;
- (void)getNextCommentsPageWithCompetionHandler:(void(^)(BOOL success, NSArray *newComments))handler;

+ (void)postComment:(NSString *)text forVideo:(NSString *)videoId withCompletionHandler:(void(^)(SIATextComment *model))handler;
+ (void)editComment:(NSString *)commentId newText:(NSString *)text withCompletionHandler:(void(^)(BOOL success))handler;
+ (void)flagComment:(NSString *)commentId asType:(SIATextCommetFlagType)type withCompletionHandler:(void(^)(BOOL success))handler;
+ (void)deleteComment:(NSString *)commentId withCompletionHandler:(void(^)(BOOL success))handler;
- (instancetype) initWithVideoId:(NSString *)videoId;
+ (void)testMe:(NSString *)videoId;
- (instancetype) initWithObjectId:(NSString*) objectId;
@end
