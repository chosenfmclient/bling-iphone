//
//  SIAPaginator.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 5/11/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SIAPaginatorDataModel.h"

typedef enum {
    RequestStatusNone,
    RequestStatusInProgress,
    RequestStatusDone // request succeeded or failed
} RequestStatus;

@protocol SIAPaginatorDelegate
@required
- (void)paginator:(id)paginator didReceiveResults:(NSArray *)results;
- (void)pageLoaded;
- (void)cacheLoaded;
- (void)noMorePages;
@optional
- (void)pageLoaded:(NSArray *)results page:(NSInteger)page;
- (void)pageLoadedFromPaginator:(id)paginator;
- (void)noMorePagesForPaginator:(id)paginator;
- (void)paginatorDidFailToRespond:(id)paginator;
- (void)paginatorDidReset:(id)paginator;
@end

@interface SIAPaginator : NSObject {
id <SIAPaginatorDelegate> __weak delegate;
}

@property (weak) id delegate;
@property (nonatomic) NSInteger currentPageSize; // number of results per page
@property (assign, readonly) NSInteger page; // number of pages already fetched
@property (assign, readonly) NSInteger total; // total number of results
@property (assign, readonly) RequestStatus requestStatus;
@property (nonatomic, strong, readonly) NSMutableArray *results;

@property (strong, nonatomic) SIAPaginatorDataModel *paginatorModel;
@property (strong, nonatomic) NSMutableArray *requestedPages;
@property (nonatomic) BOOL shouldSetImages;


-(void)fetchResultsWithPage:(NSInteger)page pageSize:(NSInteger)pageSize andSecondPage:(NSInteger)secondPage;

- (id)initWithPageSize:(NSInteger)pageSize delegate:(id<SIAPaginatorDelegate>)paginatorDelegate;
- (void)reset;
- (void)setResultsForResponses:(NSArray *)array;

- (void)fetchFirstPage;
- (void)fetchNextPage;

// call these from subclass when you receive the results
- (void)receivedResults:(NSArray *)results total:(NSInteger)total;
- (void)failed;
- (void)setCachedResults:(NSArray *)cachedArray pageNumber:(NSNumber *)page;
- (void)updatePageNumber:(NSInteger)page;
- (NSArray *)indexPathForCurrentPage;
- (NSArray *)indexPathForPage:(NSNumber *)page;
- (id)initForModelClass:(Class)className delegate:(id<SIAPaginatorDelegate>)paginatorDelegate;
- (id)initWithModelClass:(Class)className objectId:(NSString*)objectId delegate:(id<SIAPaginatorDelegate>)paginatorDelegate;
- (id)initWithModelClass:(Class)className delegate:(id<SIAPaginatorDelegate>)paginatorDelegate;

@end
