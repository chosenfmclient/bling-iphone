//
//  SIAPlaceholderTextView.h
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 2/21/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIAPlaceholderTextView : UITextView
@property (nonatomic, retain) NSString *placeholder;
@property (nonatomic, retain) UIColor *placeholderColor;
@end
