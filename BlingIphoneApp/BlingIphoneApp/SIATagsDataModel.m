//
//  SIATagsDataModel.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/10/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIATagsDataModel.h"

@implementation SIATagsDataModel

- (id) init {
    self = [super init];
    if (self){
        self.tagsArray = [NSMutableArray arrayWithObject:@"#CustomTag"];
    }
    return self;
}

@end
