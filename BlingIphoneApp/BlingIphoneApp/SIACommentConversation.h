//
//  SIACommentConversation.h
//  ChosenIphoneApp
//
//  Created by Zach on 08/09/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SIATextComment.h"

@class SIACommentConversation;

@interface SIACommentCell : UITableViewCell
@property (weak, nonatomic) SIATextCommentView *commentView;
@end

@interface SIACommentConversation : NSObject <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic, nullable) NSMutableArray<SIATextComment*>* comments;
@property (strong, nonatomic, nullable) NSMutableArray<SIATextComment*>* commentsForTable;
@property (weak, nonatomic, nullable) UITableView* chatTableView;
@property (nonatomic) NSUInteger currentCommentIndex;
@property (strong, nonatomic, nullable) NSTimer* simulationTimer;
@property BOOL simulationStarted;
@property BOOL simulationPaused;


- (void)resumeSimulatedCommentChat;

- (void)pauseSimulatedCommentChat;

- (void)stopSimulatedCommentChat;

- (void)startSimulatedCommentChat;

- (void)commentFromArray:(NSArray* _Nonnull)comments;

- (void)appendComment:(SIATextComment * _Nonnull)comment;

- (void)replaceComment:(SIATextComment* _Nonnull)comment;

- (void)insertComment:(SIATextComment * _Nonnull)comment atIndex:(NSUInteger)index;

- (instancetype) initWithTableView: (UITableView*) tableView comments: (NSMutableArray*)comments;
@end
