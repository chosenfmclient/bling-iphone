//
//  SIAEmailRegistrationdataModel.m
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 4/14/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import "SIAEmailRegistrationdataModel.h"

@implementation SIAEmailRegistrationdataModel


+(RKObjectMapping *)objectMapping{
    
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{@"first_name":@"first_name",
                                                  @"last_name":@"last_name",
                                                  @"email": @"email",
                                                  @"password" :@"password",
                                                  @"app_id" : @"app_id",
                                                  @"secret":@"secret",
                                                  @"access_token":@"access_token",
                                                  @"user_id":@"user_id",
                                                  @"expires_in":@"expires_in",
                                                  @"device_id":@"device_id"
                                                  }];
    
    return mapping;
}

@end
