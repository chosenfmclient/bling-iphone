//
//  SIAPushRegistrationInterstitial.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 8/18/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAPushRegistrationInterstitial.h"

@implementation SIAPushRegistrationInterstitial 

- (void)awakeFromNib {
    [super awakeFromNib];
    self.interstitialView.layer.cornerRadius = 5.0;
//    UITapGestureRecognizer *dismissTap = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                                 action:@selector(dismiss)];
//    dismissTap.delegate = self;
//    [self addGestureRecognizer:dismissTap];
}

- (void)layoutSubviews {
    [self.confirmButton bia_rounded];
}

- (IBAction)buttonWasTapped:(UIButton *)sender {
    [self dismiss];
    self.completionHandler(sender == _confirmButton);
}

- (void)show {
    
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.alpha = 0;
    self.frame = [delegate window].bounds;
    
    [[delegate window] addSubview:self];
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.alpha = 1;
                     }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         [[SIAPushRegistrationHandler sharedHandler] interstitialDidDismiss];
                     }];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch {
    return !CGRectContainsPoint(self.interstitialView.frame, [touch locationInView:self]);
}


@end
