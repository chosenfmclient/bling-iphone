//
//  DiscoverResponseModel.swift
//  BlingIphoneApp
//
//  Created by Zach on 28/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit
@objc class DiscoverResponseModel: VideoListDataModel {
    var featured_clip: MusicVideo?
    var trending_videos = [SIAVideo]()
    
    static func objectMapping() -> RKObjectMapping {
        let responseMapping = RKObjectMapping(for: DiscoverResponseModel.self)
        
        responseMapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "featured_clip", toKeyPath: "featured_clip", with: MusicVideo.objectMapping()))
        
        responseMapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "trending_videos", toKeyPath: "trending_videos", with: SIAVideo.objectMapping()))
            
        return responseMapping!
    }
}
