//
//  DiscoverViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 26/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
import RestKit
import SDWebImage

class DiscoverViewController: SIABaseViewController,
DiscoverSearchBarViewDelegate,
UITableViewDelegate,
VideoPlayerManagerDelegate,
VideoCollectionHandlerDelegate,
PageRequestHandlerDelegate
{

    //MARK: Outlets
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var backgroundTintLayerView: UIView!
    @IBOutlet weak var collectionViewTopSeperatorView: UIView!
    @IBOutlet weak var videoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var shootNowButton: UIButton!
    @IBOutlet weak var loadingIcon: SIASpinner!
    @IBOutlet weak var trendingCollectionView: UICollectionView!
    
    @IBOutlet weak var trendingCollectionViewLoweredTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var trendingCollectionViewRaisedTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var trendingCollectionViewTopSeperator: UIView!
    
    @IBOutlet weak var noSearchTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var noSearchBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var leaderboardButtonHeightConstraint: NSLayoutConstraint!
    
//    @IBOutlet weak var searchBarActiveConstraint: NSLayoutConstraint!
//    @IBOutlet weak var searchBarInactiveConstraint: NSLayoutConstraint!
    @IBOutlet weak var leaderboardButton: UIButton!
    @IBOutlet weak var leaderboardButtonContainer: UIView!
    @IBOutlet weak var searchBarView: DiscoverSearchBarView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet var tabHightLightLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var peopleTabButton: UIButton!
    @IBOutlet weak var videoTabButton: UIButton!
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var noResultsView: UIView!
    
    @IBOutlet weak var featuredDataView: UIView!
    @IBOutlet weak var featuredTitleLabel: UILabel!
    @IBOutlet weak var featuredArtistLabel: UILabel!
    @IBOutlet weak var videoThumbnailImageView: UIImageView!
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataReloadButton: UIButton!
    @IBOutlet weak var noVideosLabel: UILabel!
    
    //MARK: properties
    var searchMode = false
    var isPeopleTab = false
    var thumbnailURL: URL?
    var videoURL: URL?
    var backgroundSet = false
    var viewFadedIn = false
    var videoHeight: CGFloat =  0
    var featuredClip: MusicVideo?
    var videoID: String?
    var trackURL: String?
    var clipID: String?
    
    var searchManager = SearchManager()
    
    var collectionViewHandler: VideoCollectionHandler?
    var pageRequestHandler: PageRequestHandler?
    var interactionIsEnabled = true
    var featuredBanner: FeaturedBannerView?
    
    //MARK: View controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
//        let width = UIScreen.main.bounds.width
//        videoHeight = width / (4/3) //video is 4:3
        
        weak var wself = self
        searchBarView.delegate = wself
        searchView.isUserInteractionEnabled = false
        searchView.alpha = 0
        featuredDataView.alpha = 0
        videoThumbnailImageView.alpha = 0
        trendingCollectionView.alpha = 0
        collectionViewTopSeperatorView.alpha = 0
        leaderboardButtonContainer.alpha = 0
        playButton.setImage(nil, for: .normal)
        loadingIcon.startAnimation(shouldLayout: false)
        
        collectionViewHandler =
            VideoCollectionHandler(
                collectionView: trendingCollectionView,
                viewController: self,
                loweredTopConstraint: trendingCollectionViewLoweredTopConstraint,
                raisedTopConstraint: trendingCollectionViewRaisedTopConstraint,
                delegate: self
        )
        
        trendingCollectionView.dataSource = collectionViewHandler
        trendingCollectionView.delegate = collectionViewHandler
        
        trackURL = featuredClip?.previewUrl?.absoluteString
        
        var query = [String: String]()
        var path: String
        if let _ = videoID
        { // set view controller for video
            query = ["video_id":videoID!]
            searchView.removeFromSuperview()
            searchBarView.removeFromSuperview()
            navigationController.setNavigationBarLeftButtonBack()
            path = "api/v1/discover"
        }
        else if let _ = trackURL
        {
            if let urlEncodedTerm = trackURL!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                query = ["song_clip_url":urlEncodedTerm]
            }
            searchView.removeFromSuperview()
            searchBarView.removeFromSuperview()
            navigationController.setNavigationBarLeftButtonBack()
            path = "api/v1/discover"
        }
        else if let _ = clipID
        {
            searchView.removeFromSuperview()
            searchBarView.removeFromSuperview()
            navigationController.setNavigationBarLeftButtonBack()
            path = "api/v1/song-home/\(clipID!)"
        }
        else
        { // set view controller for discover
            searchTable.dataSource = searchManager
            searchManager.tableView = searchTable
            searchManager.noDataView = noResultsView
            leaderboardButton.isHidden = false
            leaderboardButtonHeightConstraint.constant = 60
            path = "api/v1/discover"
        }
        
        self.pageRequestHandler = PageRequestHandler(
            apiPath: path,
            delegate: self,
            requestIdentifier: nil,
            queryParameters: query
        )
        self.pageRequestHandler?.fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isSeedVideo() {
            navigationController?.clearNavigationItemLeftButton()
        }
        navigationController?.setBottomBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.setNavigationBarHidden(!isSeedVideo(), animated: true)
        navigationController?.setBottomBarHidden(false, animated: false)
        AmplitudeEvents.log(event: isSeedVideo() ? "seedhome:pageview" : "discover:pageview")
    }
    
    override func viewDidLayoutSubviews() {
        shootNowButton?.bia_rounded()
        noDataReloadButton?.bia_rounded()
        setBackgroundWith(imageUrl: thumbnailURL)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        playButton.setImage(UIImage(named: "video_play"), for: .normal)
        Utils.dispatchInBackground {
            VideoPlayerManager.shared.removeVideo()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        shootNowButton?.isEnabled = true
        trendingCollectionView?.isUserInteractionEnabled = true
    }
    
    func makeViewFadeIn() {
        guard !viewFadedIn else {
            return
        }
        featuredDataView.isHidden = false
        trendingCollectionView.isHidden = false
        loadingIcon.stopAnimation()
        

        Utils.dispatchOnMainThread {[weak self] in
            UIView.animate(
                withDuration: 0.6,
                delay: 0.5,
                options: .curveEaseInOut,
                animations: {
                    self?.featuredDataView.alpha = 1
                    self?.videoThumbnailImageView.alpha = 1
                    self?.trendingCollectionView.alpha = 1
                    self?.collectionViewTopSeperatorView.alpha = 1
                    self?.featuredBanner?.alpha = 1
                    self?.leaderboardButtonContainer.alpha = 1
                },
                completion: {(🎅🏼: Bool) in
                    self?.playButton.setImage(UIImage(named: "video_play"), for: .normal)
                    self?.viewFadedIn = true
            })
        }

    }
    
    
    func loadPage(from discoverResponse: DiscoverResponseModel) {
        if featuredClip == nil {
            featuredClip = discoverResponse.featured_clip
        }
        
        Utils.dispatchOnMainThread {[weak self] in
            guard let _ = self?.featuredClip else { return }
            self?.loadVideo(self!.featuredClip!)
        }
        
        guard discoverResponse.trending_videos.count > 0 else {
            noVideosLabel.isHidden = false
            trendingCollectionView.removeFromSuperview()
            trendingCollectionViewTopSeperator.removeFromSuperview()
            return
        }
        
        noVideosLabel.isHidden = true
        
        collectionViewHandler?.setVideoList(discoverResponse.trending_videos)
        trendingCollectionView.reloadData()
    }
    
    //MARK: video
    func isSeedVideo() -> Bool {
        return !(videoID == nil && trackURL == nil && clipID == nil)
    }
    
    func loadVideo(_ featuredClip: MusicVideo?) {
        guard let clip = featuredClip, let _ = navigationController else {
            return
        }
        thumbnailURL = clip.correctSizeThumb(for: videoHeight) as URL?
        featuredTitleLabel.text = clip.trackName
        featuredArtistLabel.text = clip.artistName
        videoURL = clip.previewUrl as URL?
        
        if (isSeedVideo()) {
            navigationController?.setNavigationItemTitle(clip.trackName)
        }
        
        if let bannerText = clip.bannerLabel {
            featuredBanner = FeaturedBannerView.addBanner(to: videoThumbnailImageView, withText: bannerText, andHexColor: clip.bannerColor)
            featuredBanner?.alpha = 0
        }
        
        videoThumbnailImageView.sd_setImage(with: thumbnailURL) {[weak self] (image, error, cacheType, url) in
            self?.makeViewFadeIn()
        }
    }
    
    func playerDidReachEnd() {
        playButton?.setImage(UIImage(named: "video_play"), for: .normal)
    }
    
    @IBAction func playButtonWasTapped(_ sender: AnyObject) {
        if (VideoPlayerManager.shared.currentPlayerView == videoThumbnailImageView && VideoPlayerManager.shared.player.rate > 0) {
            playButton.setImage(UIImage(named: "video_play"), for: .normal)
            VideoPlayerManager.shared.player.pause()
            return
        }
        
        guard let url = videoURL  else { return }
        playButton.setImage(nil, for: .normal)
        VideoPlayerManager.shared.setNewPlayerView(view: videoThumbnailImageView, videoURL: url, withDelegate: self)
        featuredBanner?.hideBanner(after: 5)
    }
    
    func setBackgroundWith(imageUrl: URL?) {
        guard let url = imageUrl, !backgroundSet else {
            return
        }
        backgroundSet = true
        Utils.dispatchInBackground {
            SDWebImageManager.shared().loadImage(with: url, options: SDWebImageOptions.refreshCached, progress: nil) {
                [weak self] (image, data, error, cacheType, complete, url) in
                guard let sSelf = self, complete && error == nil else {
                    return
                }
                var blurredImage = SIAStyling.defaultStyle().blurredImage(from: image)
                let backgroundAlphaLayerColor = SIAStyling.defaultStyle().averageColor(from: blurredImage)
                blurredImage = Utils.changeBrightnessOfImage(blurredImage!, byValue: 0.035)
                
                Utils.dispatchOnMainThread({
                    UIView.transition(with: sSelf.backgroundImage, duration: 1.4, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                        sSelf.backgroundImage.image = blurredImage
                        sSelf.backgroundTintLayerView.backgroundColor = backgroundAlphaLayerColor
                        }, completion: nil)
                })
            }
        }
    }
    
    @IBAction func shootNowButtonWasTapped(_ sender: AnyObject) {
        shootNowButton?.isEnabled = false
        guard let _ = featuredClip else { return }
        AmplitudeEvents.log(event: "record:shootnow",
                            with: ["pagename": AmplitudeEvents.kDiscoverPageName,
                                   "artist_name": featuredClip!.artistName,
                                   "video_name": featuredClip!.trackName]
        )

    }
    
    override func segueWasCancelled() {
        shootNowButton?.isEnabled = true
    }
    
    
    //MARK: Collection delegate methods
    func collectionViewMovedUp() {
        VideoPlayerManager.shared.player.pause()
        playButton.setImage(UIImage(named: "video_play"), for: .normal)

    }
    
    func lastCollectionCellWillDisplay() {
        pageRequestHandler?.fetchData()
    }
    
    func dataWasLoaded(page: Int, data: Any?, requestIdentifier: Any?) {
        //first page
        guard (page > 0)  else {
            guard let discover = data as? DiscoverResponseModel else {
                print("WARNING: no discover data returned from api")
                return
            }
            
            loadPage(from: discover)
            return
        }
        
        //video pages
        guard let videos = (data as? DiscoverResponseModel)?.trending_videos else {
            pageRequestHandler?.didReachEndOfContent = true
            return
        }
        collectionViewHandler?.appendToVideoList(videos)
        collectionViewHandler?.collectionView?.reloadData()
    }
    
    func endOfContentReached(at page: Int, requestIdentifier: Any?) {
        if page == 0 {
            noDataView.isHidden = false
            playButton.setImage(nil, for: .normal)
            loadingIcon.stopAnimation()
        }
        collectionViewHandler?.shouldDisplayFooter = false
        trendingCollectionView.reloadData()
    }
    
    @IBAction func noDataReloadWasTapped(_ sender: Any) {
        noDataView.isHidden = true
        loadingIcon.startAnimation()
        pageRequestHandler?.page = 0
        pageRequestHandler?.didReachEndOfContent = false
        pageRequestHandler?.fetchData()
    }
    
    //MARK: search
    @IBAction func tabButtonWasTapped(_ sender: AnyObject) {
        isPeopleTab = sender as! NSObject == peopleTabButton
        searchQuery(searchManager.currentQuery)

        tabHightLightLeftConstraint.isActive = !isPeopleTab
        peopleTabButton.isEnabled = !isPeopleTab
        videoTabButton.isEnabled = isPeopleTab
        
        if (isPeopleTab) {
            videoTabButton.alpha = 0.4
            peopleTabButton.alpha = 1
        } else {
            videoTabButton.alpha = 1
            peopleTabButton.alpha = 0.4
        }
        
        UIView.animate(
            withDuration: 0.3     ,
            delay: 0,
            options: .curveEaseInOut,
            animations: { [weak self] in
                self?.view.layoutIfNeeded()
            },
            completion: nil)
    }
    
    //MARK: search delegate methods
    func searchFieldWasEntered() {
        guard !searchMode else {
            return
        }
        VideoPlayerManager.shared.removeVideo()
        playButton.setImage(UIImage(named: "video_play"), for: .normal)
        searchMode = true
        searchView.isUserInteractionEnabled = true
        noSearchTopConstraint.constant = UIScreen.main.bounds.height
        noSearchBottomConstraint.constant = -UIScreen.main.bounds.height
//        searchBarActiveConstraint.isActive = true
//        searchBarInactiveConstraint.isActive = false
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: { [weak self] in
                        self?.view.layoutIfNeeded()
                        self?.searchView.alpha = 1
                        self?.leaderboardButtonContainer.alpha = 0
            })
    }
    
    func cancelWasTapped() {
        guard searchMode else {
            return
        }
        searchMode = false
        searchView.isUserInteractionEnabled = false
        noSearchTopConstraint.constant = 0
        noSearchBottomConstraint.constant = 0
//        searchBarActiveConstraint.isActive = false
//        searchBarInactiveConstraint.isActive = true
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: { [weak self] in
                        self?.view.layoutIfNeeded()
                        self?.searchView.alpha = 0
                        self?.leaderboardButtonContainer.alpha = 1
            })
    }
    
    func searchQuery(_ query: String) {
        guard searchMode else {
            return
        }
        searchManager.search(query: query, isUserSearch: isPeopleTab)
    }
    
    func searchWasCleared() {
    }
    
    //MARK: Search Table view tings
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (!isPeopleTab) {
            let discoverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "discoverViewController") as! DiscoverViewController
            //stupid shit because swift gets confused when having an array of optionals
            var videos = [MusicVideo]()
            for video in searchManager.videoSearchList {
                if let _ = video {
                    videos.append(video!)
                }
            }
            if indexPath.row < videos.count {
                discoverVC.featuredClip = videos[indexPath.row]
                navigationController?.pushViewController(discoverVC, animated: true)
            }
        } else {
            let profileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "profileViewController") as! ProfileViewController
            var users = [SIAUser]()
            for user in searchManager.userSearchList {
                if let _ = user {
                    users.append(user!)
                }
            }
            if indexPath.row < users.count {
                profileVC.userId = users[indexPath.row].user_id
                navigationController?.pushViewController(profileVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (isPeopleTab) {
            return 48
        }
        return 53
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        searchManager.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let recordVC = segue.destination as? SIABlingRecordViewController {
            recordVC.musicVideo = featuredClip
        }
    }
}

class UserSearchTableViewCell: UITableViewCell {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    weak var user: SIAUser?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //userImage.sd_setImage(with: user?.imageURL)
        userImage.bia_rounded()
    }
}

class VideoSearchTableViewCell: UITableViewCell {
    @IBOutlet weak var videoThumbnailImage: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var songNameLabel: UILabel!
    weak var musicVideo: MusicVideo?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let url = musicVideo?.thumbnailURL {
            videoThumbnailImage.sd_setImage(with: url)
        }
    }

}

