//
//  BottomBarButtonView.swift
//  BlingIphoneApp
//
//  Created by Zach on 25/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class BottomBarButtonView: UIButton {

    @IBInspectable var selectedColor: UIColor?
    @IBInspectable var defaultColor: UIColor?
    @IBInspectable var selectedImage: UIImage?
    @IBInspectable var defaultImage: UIImage?
    
    override var isSelected: Bool {
        didSet {
            if (isSelected) {
                backgroundColor = selectedColor
                setImage(selectedImage, for: .normal)
            } else {
                backgroundColor = defaultColor
                setImage(defaultImage, for: .normal)
            }
            isEnabled = !isSelected
        }
    }

    override var backgroundColor: UIColor? {
        get {
            return super.backgroundColor
        } set {
            UIView.animate(
                withDuration: 0.2,
                delay: 0,
                options: .curveEaseInOut,
                animations: {
                    super.backgroundColor = newValue
                },
                completion: nil)
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
