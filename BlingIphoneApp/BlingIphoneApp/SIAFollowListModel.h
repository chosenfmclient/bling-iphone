//
//  SIAFollowListModel.h
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 03/08/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "SIAPaginatorDataModel.h"

@interface SIAFollowListModel : SIAPaginatorDataModel

@property (strong, nonatomic) NSString *userId;
- (RKObjectMapping *) objectMapping;
- (NSString *)pagePath;
- (NSDictionary *)requestParams;

@end
