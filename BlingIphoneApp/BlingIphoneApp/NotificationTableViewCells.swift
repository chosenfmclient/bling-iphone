//
//  NotificationTableViewCells.swift
//  BlingIphoneApp
//
//  Created by Zach on 25/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class UserNotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var notificationTextlabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet var labelViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        userImageView.bia_rounded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class VideoNotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var notificationTextlabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet var labelViewHeightConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
