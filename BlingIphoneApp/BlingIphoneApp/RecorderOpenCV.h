//
//  RecorderOpenCV.h
//  BlingIphoneApp
//
//  Created by Joseph Nahmias on 25/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BIAPausableCVCamera.h"
#import <opencv2/objdetect/objdetect.hpp>
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/imgproc/imgproc.hpp>
#include "globals.h"

using namespace cv;

@protocol RecorderDetectionDelegate <NSObject>
- (void)detectionFinishedWithResponse:(DetectionResponse) response;
- (void)recorderNeedsRedetect;
- (void)recorderDidFailToReadBackgroundVideo:(NSError *)error;
@end


@interface RecorderOpenCV : NSObject<CvVideoCameraDelegate>

{
    UIView* recorderView;
    
    BIAPausableCVCamera* videoCamera;
    CascadeClassifier faceCascade;
    CascadeClassifier eyeCascade;
    
}

@property (nonatomic, weak) id <RecorderDetectionDelegate> detectionDelegate;
@property (nonatomic, retain) BIAPausableCVCamera* videoCamera;
@property (weak, nonatomic) AVPlayerLayer *backgroundPlayerLayer;
@property (nonatomic) BOOL usingOldDevice;
@property (nonatomic) BOOL usingFrontCamera;
@property (nonatomic, readonly) BOOL isDetecting;
@property (nonatomic, readonly) BOOL didDetect;

- (instancetype)initWithView:(UIView *)view;
- (void)setPlayer:(BIAVideoPlayer *)player;
- (void)startCamera;
- (void)stopCamera;
- (void)startRecording;
- (void)stopRecording;
- (void)pauseRecording;
- (void)resumeRecording;
- (void)cancelRecording;
- (void)setRecordingSpeedScale:(Float64)scale;
- (void)startDetection;

- (void)reset;
- (void)resetForRedetect;
- (void)resetAndResetCapSession:(BOOL)resetCapSession;

@end
