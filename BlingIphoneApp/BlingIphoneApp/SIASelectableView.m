//
//  SIASelectableView.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 3/24/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIASelectableView.h"
#import <SDWebImage/UIImageView+WebCache.h>
@implementation SIASelectableView

- (id)initWithImageURL:(NSURL *)imageURL
                 title:(NSString *)title
          buttonTarget:(id)target
        buttonSelector:(SEL)selector
             buttonTag:(NSUInteger)tag
        placementIndex:(NSUInteger)idx
{
    self = [super initWithFrame:CGRectMake(0 + idx * 78, 0, 78, 85)];
    if (self) {
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.tag = tag;
        if (title.length > 0)
            [self.button setFrame:CGRectMake(10, 10, 48, 48)];
        else
            [self.button setFrame:CGRectMake(10, 10, 60, 60)];
        [self.button addTarget:target
                        action:selector
              forControlEvents:UIControlEventTouchUpInside];
        self.button.layer.cornerRadius = 2.0;
        self.button.layer.masksToBounds = YES;
        self.imageView = [[UIImageView alloc] initWithFrame:self.button.frame];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.imageView.clipsToBounds = YES;
        self.imageView.layer.cornerRadius = 2.0;
        [self.imageView sd_setImageWithURL:imageURL];
        [self addSubview:self.imageView];
        [self addSubview:self.button];
        
        if (title.length > 0)
        {
            self.label = [[UILabel alloc] initWithFrame:CGRectMake(10, 63, 48, 12)];
            self.label.text = title;
            self.label.font = kAppFontRegular(12.0);
            self.label.textColor = [UIColor whiteColor];
            self.label.textAlignment = NSTextAlignmentCenter;
        }
        
        [self addSubview:self.label];
    }
    return self;
}

@end
