//
//  SIAMenuTableViewCell.h
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 7/28/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIAMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellArrowImageVIew;
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@end
