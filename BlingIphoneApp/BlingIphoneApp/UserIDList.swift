//
//  UserIDList.swift
//  BlingIphoneApp
//
//  Created by Zach on 28/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class UserIDList: NSObject {
    var user_ids = [String]()
    
    init(list: [String]) {
        user_ids = list
    }
    
    static func objectMapping() -> RKObjectMapping {
        var objectMapping = RKObjectMapping.request()
        objectMapping?.addAttributeMappings(from: ["user_ids":"user_ids"])
        return objectMapping!
    }
}
