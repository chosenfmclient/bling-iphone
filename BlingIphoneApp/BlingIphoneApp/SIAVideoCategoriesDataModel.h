//
//  SIAVideoCategoriesDataModel.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/10/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SIAVideoCategory.h"

@interface SIAVideoCategoriesDataModel : NSObject

@property (strong, nonatomic) NSArray <SIAVideoCategory *> *categories;

+ (void) loadCategories:(void(^)(SIAVideoCategoriesDataModel *dataModel))handler;

@end
