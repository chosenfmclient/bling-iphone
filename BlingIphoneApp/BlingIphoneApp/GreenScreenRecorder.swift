 //
//  GreenScreenRecorder.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 11/16/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
typealias HSV = (h: Float, s: Float, v: Float)

protocol GreenScreenRecorderDelegate : class {
    func backgroundWasDetected()
    func backgroundDetection(failed: DetectionResponseS)
    func recorderNeedsRedetect()
    func recordingDidStop()
    func backgroundPlaybackDidStart(withTimeOffset offset: CMTime)
    func cameraPermissionWasDenied()
}

@objc class GreenScreenRecorder: NSObject, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    weak var delegate: GreenScreenRecorderDelegate!
    let sampleBufferQueue = DispatchQueue(label: "gy.blin.sampleBufferQueue", attributes: .concurrent)
    let sampleWritingQueue = DispatchQueue(label: "gy.blin.sampleWritingQueue")
    let frameProcessingQueue = DispatchQueue(label: "gy.blin.frameProcessingQueue")
    weak var captureView: UIView!
    let colorCube = CIFilter(name: "CIColorCube")!
    let sourceOver = CIFilter(name: "CISourceOverCompositing")!
    let inputBgImage = CIImage(image: UIImage(named: "blingBgImageTest.jpg")!)
    let ciContext = CIContext(eaglContext: EAGLContext(api: EAGLRenderingAPI.openGLES2)) //(options: [kCIContextUseSoftwareRenderer : true])
    var captureSession = AVCaptureSession()
    var currentDeviceInput: AVCaptureDeviceInput?
    var captureSessionPreviewLayer: AVCaptureVideoPreviewLayer?
    var backCamera = false
    var accuracy: Float = 0.65
    var uniformBgInImagesNeeded: UInt = 6
    var uniformBgInImagesCount: UInt = 0
    var groupColors = [Int: (count: UInt, min_h: Float, max_h: Float, min_s: Float, max_s: Float)]()
    var maxGroupKey: Int = 0
    var minGroupHue: Float = 0
    var maxGroupHue: Float = 0
    var minGroupSat: Float = 0
    var maxGroupSat: Float = 0
    let colorCubeSize = 32
    var isInWhiteRange: Bool = false
    var minHSV: HSV = (h: Float(1), s: Float(1), v: Float(1))
    var maxHSV: HSV = (h: Float(0), s: Float(0), v: Float(0))
    
    var bothMode = true //false
    var didSetup = false
    var isCapturing = false
    var shouldDetectBackground = false
    var backgroundWasDetected = false
    var isDetectingBackground = false
    var cancelSuccessMessage = false
    var isRecording = false
    var isPaused = false
    var needsToAdjustOffset = false
    var didStartRecording = false
    var detectionStartDate: Date?
    var pauseOffset = kCMTimeZero
    var lastOffsetSampleTime = kCMTimeInvalid
    var playerLagOffset = kCMTimeInvalid
    
    var recordingSpeedScale = Float64(1.0)
    weak var player: BIAVideoPlayer?
    weak var playerLayer: AVPlayerLayer?
    let chromaKeyLayer = CALayer()
    weak var chromaKeyCameraView: UIView!
    
    weak var musicVideo: MusicVideo!
    var encoder: SIAVideoEncoder?
    var cameraPosition: AVCaptureDevicePosition = .front
    var videoOutput: AVCaptureVideoDataOutput!
    var videoOutputSize = CGSize.zero
    var imageGenerator: AVAssetImageGenerator?
    var startTimeInterval: TimeInterval = 0
    var didFinish = false
    var recordedFilePath: String {
        let documentDirectoryURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        return documentDirectoryURL.appendingPathComponent("recordVideo1.mp4").path
    }
    var outputURL: URL?
    var playerTimeObserver: Any?
    var detectionAttemptCount = 0
    
    var detectionResizeFactor = CGFloat(0.5)
    var frameDetectStartDate: Date?
    var backgroundVideoSize: CGSize?
    
    
    init(musicVideo: MusicVideo!,
         cameraView: UIView!,
         captureView: UIView!,
         delegate: GreenScreenRecorderDelegate!) {
        self.chromaKeyCameraView = cameraView
        self.musicVideo = musicVideo
        self.captureView = captureView
        self.delegate = delegate
        self.captureSession.sessionPreset = AVCaptureSessionPresetHigh
        super.init()
    }
    
    func startBackgroundDetection() {
        shouldDetectBackground = true
        cancelSuccessMessage = false
        detectionAttemptCount += 1
    }
    
    func startRecording() {
        isRecording = true
    }
    
    func pauseRecording() {
        needsToAdjustOffset = true
        isPaused = true
    }
    
    func resumeRecording() {
        isPaused = false
    }
    
    func stopRecording() {
        let wasRecording = isRecording
        isRecording = false
        isPaused = false
        if (wasRecording) {
            frameProcessingQueue.async {[weak self] in
                guard let _ = self else { return }
                self!.encoder?.finish(at: self!.lastOffsetSampleTime,
                                withCompletionHandler: {[weak self] (outputURL) in
                                    Utils.dispatchOnMainThread {
                                        self?.outputURL = outputURL
                                        self?.delegate.recordingDidStop()
                                        self?.captureSession.stopRunning()
                                        //self?.resetForRedetect()
                                        self?.encoder = nil
                                        if #available(iOS 10.0, *) {
                                            self?.player?.automaticallyWaitsToMinimizeStalling = true
                                        }
                                    }
                })
            }
        }
        
    }
    
    func switchCamera() {
        let devicePos = currentDeviceInput?.device.position
        let newDevicePos = devicePos == .front ? AVCaptureDevicePosition.back : .front
        var oldInput: AVCaptureDeviceInput?
        var newInput: AVCaptureDeviceInput?
        for input in captureSession.inputs {
            guard let deviceInput = input as? AVCaptureDeviceInput else { continue }
            if deviceInput.device.hasMediaType(AVMediaTypeVideo) {
                oldInput = deviceInput
            }
        }
        
        if let camera = videoDevice(position: newDevicePos) {
            if camera.isFocusModeSupported(.continuousAutoFocus) {
                try? camera.lockForConfiguration()
                camera.focusMode = .continuousAutoFocus
                camera.unlockForConfiguration()
            }
            newInput = try! AVCaptureDeviceInput(device: camera)
        }
        if let _ = newInput, let _ = oldInput {
            captureSession.beginConfiguration()
            captureSession.removeInput(oldInput)
            if captureSession.canAddInput(newInput) {
                captureSession.addInput(newInput)
                currentDeviceInput = newInput
                backCamera = newDevicePos == .back
            }
            else {
                captureSession.addInput(oldInput)
                currentDeviceInput = oldInput
            }
            captureSession.commitConfiguration()
        }
        
        videoOutput.connection(withMediaType: AVMediaTypeVideo).videoOrientation = .portrait
        
        resetForRedetect()
        delegate.recorderNeedsRedetect()
    }
    
    func resetForRedetect() {
        sampleBufferQueue.sync {
            backgroundWasDetected = false
            shouldDetectBackground = false
            cancelSuccessMessage = true
        }
        guard let _ = captureSessionPreviewLayer else {
            print("Capture session preview layer is missing!?!?!?!")
            return
        }
        
        playerLayer?.isHidden = true
        captureView.layer.addSublayer(captureSessionPreviewLayer!)
        uniformBgInImagesCount = 0
        clearChromaKeyCameraViewContents()
    }
    
    func clearChromaKeyCameraViewContents() {
        sampleBufferQueue.async {[weak self] in
            guard let _ = self else { return }
            DispatchQueue.main.sync {[weak self] in
                guard let _ = self, let _ = self!.chromaKeyCameraView else { return }
                UIGraphicsBeginImageContextWithOptions(self!.chromaKeyCameraView.bounds.size, false, CGFloat(0))
                self!.chromaKeyCameraView.layer.contents = UIGraphicsGetImageFromCurrentImageContext()?.cgImage
                UIGraphicsEndImageContext()
            }
        }
    }
    
    func videoDevice(position: AVCaptureDevicePosition) -> AVCaptureDevice? {
        let devices = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo)
        for device in devices! {
            guard let captureDevice = device as? AVCaptureDevice else { continue }
            if captureDevice.position == position {
                return captureDevice
            }
        }
        return nil
    }
    
    func setupCaptureSession() {
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo,
                                      completionHandler: {[weak self] (granted) in
                                        Utils.dispatchOnMainThread {
                                            if (granted) {
                                                self?.setupCapSessionAfterPermissionCheck()
                                            }
                                            else {
                                                self?.delegate?.cameraPermissionWasDenied()
                                            }
                                        }
        })
    }
    
    fileprivate func setupCapSessionAfterPermissionCheck() {
        isCapturing = true
        //captureSession.startRunning()
        
        var captureDevice: AVCaptureDevice! = nil
        if backCamera {
            accuracy = 0.625
            captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo) // back camera
            
        } else {
            accuracy = 0.725
            captureDevice = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) // front camera
                .map { $0 as! AVCaptureDevice }
                .filter { $0.position == .front}
                .first!
        }
        
        guard let deviceInput = try? AVCaptureDeviceInput(device: captureDevice) else {
            print("no capture input device")
            return
        }
        if captureSession.canAddInput(deviceInput) {
            captureSession.addInput(deviceInput)
            currentDeviceInput = deviceInput
        }
        
        captureSessionPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        captureSessionPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        captureSessionPreviewLayer?.frame = captureView.bounds
        captureView.layer.addSublayer(captureSessionPreviewLayer!)
        
        videoOutput = AVCaptureVideoDataOutput()
        videoOutput.alwaysDiscardsLateVideoFrames = true
        videoOutput.setSampleBufferDelegate(self, queue: sampleBufferQueue)
        if captureSession.canAddOutput(videoOutput) {
            captureSession.addOutput(videoOutput)
        }
        if let connection = videoOutput.connections.first as? AVCaptureConnection {
            connection.videoOrientation = .portrait
        }
        
        if let asset = self.player?.currentItem?.asset {
            imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator!.requestedTimeToleranceAfter = kCMTimeZero
            imageGenerator!.requestedTimeToleranceBefore = kCMTimeZero
            backgroundVideoSize = asset.tracks(withMediaType: AVMediaTypeVideo).first?.naturalSize
        }
        else {
            print("GREEN SCREEN RECORDER: WHERES THE ASSET FOR RECORDING?!?!?!?!?!?!?!?")
        }
        
        // add filter view
        chromaKeyCameraView.backgroundColor = UIColor.clear
        chromaKeyCameraView.layer.backgroundColor = UIColor.clear.cgColor
        chromaKeyLayer.frame = chromaKeyCameraView.layer.bounds
        captureSession.startRunning()
        player?.masterClock = captureSession.masterClock
        
    }
    
    // MARK: AVCaptureVideoDataOutputSampleBufferDelegate
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didDrop sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        //print("Dropped sample at time: \(CMTimeGetSeconds(CMSampleBufferGetPresentationTimeStamp(sampleBuffer)))")
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        
        // get a CMSampleBuffer's core video image buffer for the media data
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        
        // turn buffer into an image we can manipulate but also crop it so we aren't using parts of the camera image we don't want!!!!
        
        var inputImage = CIImage(cvPixelBuffer: imageBuffer)
        if !backCamera {
            inputImage = inputImage.applyingOrientation(2)
        }
        guard let _ = chromaKeyCameraView else { return }
        let imageCropFactor = chromaKeyCameraView.bounds.size.width / chromaKeyCameraView.bounds.size.height
        let imageHeight = inputImage.extent.size.width / imageCropFactor
        let imageRect = CGRect(x: 0, y: (inputImage.extent.size.height - imageHeight) / 2,
                               width: inputImage.extent.size.width, height: imageHeight)
        inputImage = inputImage.cropping(to: imageRect)

        
        // first, detect a uniform background
        // then - start render the chroma key effect
        if backgroundWasDetected {
            renderChromaKey(inputImage, imageBuffer, sampleBuffer)
        } else if shouldDetectBackground {
            if (!isDetectingBackground) {
                isDetectingBackground = true
                detectionStartDate = Date()
                print("Dispatch 10 second failure thing")
                let detectionNumber = detectionAttemptCount
                Utils.dispatchAfter(10, closure: { [weak self] in
                    guard let strongSelf = self, detectionNumber == self?.detectionAttemptCount else {
                        return
                    }
                    if (!strongSelf.backgroundWasDetected && strongSelf.isDetectingBackground) {
                        strongSelf.uniformBgInImagesCount = 0
                        Utils.dispatchOnMainThread {
                            print("detection failed")
                           // strongSelf.delegate.backgroundDetection(failed: DetectionResponseSFailure)
                        }
                    }
                    strongSelf.isDetectingBackground = false
                    strongSelf.shouldDetectBackground = false
                    })
            }
            detectionStartDate = Date()
            haveUniformBgToFilm(inputImage: inputImage, inputBuffer: imageBuffer)
            if Date().timeIntervalSince(detectionStartDate!) > 1.2 {
                detectionResizeFactor -= 0.15
                print("TOO LONG! Reducing scale factor to \(detectionResizeFactor)")
            }
        }
    }
    
    // MARK: Image Process
    
    func renderChromaKey(_ inputImage: CIImage, _ imageBuffer: CVImageBuffer!, _ sampleBuffer: CMSampleBuffer!) {
        
        colorCube.setValue(inputImage, forKey: kCIInputImageKey)
        
        guard let cubeOutputImage = colorCube.outputImage else {
            return
        }
        guard let _ = chromaKeyCameraView else { return }
        let ciimage = cubeOutputImage
        //        let cameraViewAspectRatio = chromaKeyCameraView.bounds.size.width / chromaKeyCameraView.bounds.size.height
        let outputImageRef = ciContext.createCGImage(ciimage, from: ciimage.extent)
        
        DispatchQueue.main.sync { [weak self] in
            guard let strongSelf = self, strongSelf.backgroundWasDetected else {
                return
            }
            strongSelf.chromaKeyCameraView?.layer.contents = outputImageRef
        }
        
        if  !isRecording || isPaused  {
            pausePlayback(time: CMSampleBufferGetPresentationTimeStamp(sampleBuffer))
            return
        }
        
        if (self.player?.rate == 0 && !didStartRecording) {
            let firstAudioTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
            if #available(iOS 10.0, *) {
                self.player?.automaticallyWaitsToMinimizeStalling = false
            }
            self.player?.currentItem?.audioTimePitchAlgorithm = AVAudioTimePitchAlgorithmSpectral
            self.player?.setRate(Float(recordingSpeedScale),
                                 time: kCMTimeInvalid,
                                 atHostTime: firstAudioTime)
            let offset = CMTimeSubtract(CMClockGetTime(captureSession.masterClock),
                                        firstAudioTime)
            delegate.backgroundPlaybackDidStart(withTimeOffset: offset)
            
        }
        didStartRecording = true
        
        if (needsToAdjustOffset) {
            var timeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
            needsToAdjustOffset = false
            if pauseOffset.isValid {
                timeStamp = CMTimeSubtract(timeStamp, pauseOffset)
            }
            let offset = CMTimeSubtract(timeStamp, lastOffsetSampleTime)
            
            if (pauseOffset.value == 0) {
                pauseOffset = offset
            }
            else {
                pauseOffset = CMTimeAdd(pauseOffset, offset)
            }
        }
        
        var timeStampForEncoding = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
        timeStampForEncoding = CMTimeSubtract(timeStampForEncoding, pauseOffset)
        
        if (self.player?.rate == 0 && CMTimeCompare(lastOffsetSampleTime, timeStampForEncoding) != 0) {
            // resume here at time: sample buffer pst
            resumePlayback(time: CMSampleBufferGetPresentationTimeStamp(sampleBuffer))
        }
        //let playerTime = player?.currentTime()
        if CMTimeCompare(lastOffsetSampleTime, timeStampForEncoding) != 0 && CMSampleBufferDataIsReady(sampleBuffer) { // if they are not equal
            lastOffsetSampleTime = timeStampForEncoding
            frameProcessingQueue.async { [weak self] in
                guard let _ = self else { return }
                
                var pbuff: CVPixelBuffer?
                let size = inputImage.extent.size
                CVPixelBufferCreate(nil,
                                    Int(size.width),
                                    Int(size.height),
                                    kCVPixelFormatType_32ARGB,
                                    nil,
                                    &pbuff)
                guard let _ = pbuff else { return }
                self!.ciContext.render(inputImage,
                                       to: pbuff!,
                                       bounds: inputImage.extent,
                                       colorSpace: CGColorSpaceCreateDeviceRGB())
                
                //let imageImage = CIImage(cvPixelBuffer:pbuff!)
                
                if self!.encoder == nil, let encoderSize = self!.backgroundVideoSize {
                    self!.encoder = SIAVideoEncoder(forPath: self!.recordedFilePath,
                                                    height: Int32(encoderSize.height),
                                                    width: Int32(encoderSize.width),
                                                    pixelFormat: CVPixelBufferGetPixelFormatType(pbuff!))
                }
                if let _ = pbuff {
                    self!.encoder?.encodeVideoFrame(pbuff,
                                                    presentationTime: timeStampForEncoding)
                }
                // }
            }
        }
    }
    
    func pausePlayback(time: CMTime) {
        if (player?.rate)! > Float(0) {
            var pauseTime = time
            if playerLagOffset.isValid {
                //pauseTime = CMTimeAdd(time, playerLagOffset)
            }
            let pauseTimeDiff = CMTimeGetSeconds(pauseTime) - CMTimeGetSeconds(CMClockGetTime((player?.masterClock)!))
            let playerTime = CMTimeSubtract((player?.currentTime())!, playerLagOffset)
            print("Pause Time Diff: \(pauseTimeDiff) \nplayer time: \(CMTimeGetSeconds(playerTime))")
            self.player?.setRate(0,
                                 time: kCMTimeInvalid,//playerTime,
                                 atHostTime: pauseTime)
        }
    }
    
    func resumePlayback(time: CMTime) {
        
        let tinyTime = CMTimeMakeWithSeconds(0.5, (player?.currentTime().timescale)!)
        let boundaryTime = NSValue(time: CMTimeAdd((player?.currentTime())!, tinyTime))
        
        playerTimeObserver = player?.addBoundaryTimeObserver(forTimes: [boundaryTime],
                                                             queue: sampleBufferQueue,
                                                             using: {[weak self] in
                                                                self?.playerLagOffset = CMTimeSubtract(CMTimeSubtract(CMClockGetTime((self?.player?.masterClock)!), time), tinyTime)
                                                                if let offset = self?.playerLagOffset {
                                                                    print("playerLagOffset: \(CMTimeGetSeconds(offset))")
                                                                }
                                                                //                                                    player?.removeTimeObserver(obs)
            })
        let resumeTime = kCMTimeInvalid
        let playerTime = kCMTimeInvalid
        if (playerLagOffset.isValid) {
            //resumeTime = CMTimeAdd(time, playerLagOffset)
            //playerTime = CMTimeSubtract(player?.currentTime())!, playerLagOffset)
        }
        let resumeTimeDiff = CMTimeGetSeconds(resumeTime) - CMTimeGetSeconds(CMClockGetTime((player?.masterClock)!))
        print("Resume time diff:\(resumeTimeDiff) \nplayer time:\(CMTimeGetSeconds(playerTime)) ")
        player?.setRate(Float(recordingSpeedScale),
                             time: playerTime,
                             atHostTime: resumeTime)
    }
    
    func createColorCube() {
        // create color cube and 'remove' color if in the color range (of saturation & value)
        // by setting alpha to 0 ('removed' color value) or 1 (color value stays on)
        let size = colorCubeSize
        let amount = size * size * size
        var cubeData = [Float](repeating: 0, count: amount * 4)
        var rgb: [Float] = [0, 0, 0]
        var hsv: HSV
        var offset = 0
        
        for z in 0..<size {
            rgb[2] = Float(z) / (Float(size)-1)  // blue value
            for y in 0..<size {
                rgb[1] = Float(y) / (Float(size)-1) // green value
                for x in 0..<size {
                    rgb[0] = Float(x) / (Float(size)-1) // red value
                    hsv = RGBtoHSV(r: rgb[0], g: rgb[1], b: rgb[2]) // convert RGB to HSV
                    
                    var alpha: Float = 1.0
                    if isInWhiteRange {
                        alpha = (hsv.s <= 0.25 && hsv.v >= 0.2) ? 0 : 1
                    } else {
                        alpha = (hsv.h >= minGroupHue && hsv.h <= maxGroupHue && (hsv.h > 0.04 || hsv.h < 0) && (hsv.h < 0.16 || hsv.h > 0.24) && (hsv.h > 0.0875 || hsv.h < 0.1125)) ? 0 : 1 //hsv.s >= 0.2 && hsv.v >= 0.3
                    }
                    
                    // calculate premultiplied alpha values for the cube
                    cubeData[offset] = rgb[0] * alpha
                    cubeData[offset+1] = rgb[1] * alpha
                    cubeData[offset+2] = rgb[2] * alpha
                    cubeData[offset+3] = alpha
                    offset += 4 // advance our pointer into memory for the next color value
                }
            }
        }
        
        let data = NSData(bytes: cubeData, length: cubeData.count * MemoryLayout<Float>.size)
        colorCube.setValue(size, forKey: "inputCubeDimension")
        colorCube.setValue(data, forKey: "inputCubeData")
    }
    
    func HSVtoRGB(h : Float, s : Float, v : Float) -> (r : Float, g : Float, b : Float) {
        var r : Float = 0
        var g : Float = 0
        var b : Float = 0
        
        let c = s * v
        let hs = h * 6.0
        let x = c * (1.0 - fabsf(fmodf(hs, 2.0) - 1.0))
        
        switch hs {
        case 0..<1:
            r = c
            g = x
            b = 0
        case 1..<2:
            r = x
            g = c
            b = 0
        case 2..<3:
            r = 0
            g = c
            b = x
        case 3..<4:
            r = 0
            g = x
            b = c
        case 4..<5:
            r = x
            g = 0
            b = c
        case 5..<6:
            r = c
            g = 0
            b = x
        default:
            break
        }
        
        let m = v - c
        r += m
        g += m
        b += m
        return (r, g, b)
    }
    
    func RGBtoHSV(r : Float, g : Float, b : Float) -> HSV {
        var h : CGFloat = 0
        var s : CGFloat = 0
        var v : CGFloat = 0
        
        var col: UIColor?
        do {
            col = try? UIColor(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: 1.0)
            col?.getHue(&h, saturation: &s, brightness: &v, alpha: nil)
        }
        
        
        let hsv: HSV = (h: Float(h), s: Float(s), v: Float(v))
        return hsv
    }
    
    func haveUniformBgToFilm(inputImage: CIImage, inputBuffer: CVPixelBuffer) -> Bool {
        
        guard isCapturing else {
            return false
        }

        let isUniq = BIAScreenCheckerWrapper.checkScreen(inputBuffer, forAccuracyRate: 0.08, resizeFactor: detectionResizeFactor)
        uniformBgInImagesCount = isUniq ? uniformBgInImagesCount+1 : 0;
                
        print("uniformBG Count: " + String(uniformBgInImagesCount))
        let resizedInput = inputImage.applying(CGAffineTransform(scaleX: detectionResizeFactor, y: detectionResizeFactor))
        let image = UIImage(ciImage: resizedInput)
        let cgImage = ciContext.createCGImage(resizedInput, from: resizedInput.extent)
        
        // get each pixel in image and check its color,
        // divide pixels colors to groups.
        
        var maxGroupCount: UInt = 0
        maxGroupHue = 0
        minGroupHue = 1
        maxGroupSat = 0
        minGroupSat = 1
        groupColors.removeAll()
        
        for x in 0..<Int(image.size.width) {
            for y in 0..<Int(image.size.height) {
                guard let rgb = getRGBPixelColorAtPoint(cgImage: cgImage!, point: CGPoint(x: CGFloat(x), y: CGFloat(y))) else {
                    continue
                }
                let red = Float(rgb.r) / Float(255.0)
                let green = Float(rgb.g) / Float(255.0)
                let blue = Float(rgb.b) / Float(255.0)
                let hsv = RGBtoHSV(r: red, g: green, b: blue)
                
                let groupKey = Int(hsv.h * 10) // + Int(hsv.s * 10) + Int(hsv.v * 10)
                
                if let tuple = groupColors[groupKey] {
                    var count = tuple.0
                    var minHue = tuple.1
                    var maxHue = tuple.2
                    var minSat = tuple.3
                    var maxSat = tuple.4
                    count += 1
                    minHue = min(hsv.h, minHue)
                    maxHue = max(hsv.h, maxHue)
                    minSat = min(hsv.s, minSat)
                    maxSat = max(hsv.s, maxSat)
                    
                    groupColors[groupKey] = (count, minHue, maxHue, minSat, maxSat)
                    
                    if count > maxGroupCount {
                        maxGroupKey = groupKey
                    }
                    
                    // decide which colors group is the biggest
                    maxGroupCount = max(count, maxGroupCount)
                } else {
                    groupColors[groupKey] = (UInt(1), Float(1), Float(0), Float(1), Float(0))
                }
            }
        }
        
        // order groups in array by it size
        let biggestGroup = groupColors[maxGroupKey]!
        
        minGroupHue = biggestGroup.min_h
        maxGroupHue = biggestGroup.max_h
        minGroupSat = biggestGroup.min_s
        maxGroupSat = biggestGroup.max_s
        
        isInWhiteRange = maxGroupSat <= 0.3
        
        if !isInWhiteRange && (maxGroupHue - minGroupHue) < 0.125 {
            let diff = 0.125 - (maxGroupHue - minGroupHue)
            minGroupHue -= diff * 0.5
            maxGroupHue += diff * 0.5
        }
        
        print("\n\n is in white range \(isInWhiteRange)")
        
        print("H: \(minGroupHue) - \(maxGroupHue)\nS: \(minGroupSat) - \(maxGroupSat)")
        
        // check if colors are uniform relative to all pixels in image
        //let pixelsCount = image.size.width * image.size.height
        //let colorsAreUniform = (Float(maxGroupCount) / Float(pixelsCount)) >= accuracy
        
        //uniformBgInImagesCount = colorsAreUniform ? uniformBgInImagesCount+1 : 0
        
        if uniformBgInImagesCount >= uniformBgInImagesNeeded {
            createColorCube()
        }
        
        Utils.dispatchAsync { [weak self] in
            guard let cameraView = self?.chromaKeyCameraView else {
                return
            }
            cameraView.isHidden = false
        }
        
        if uniformBgInImagesCount >= uniformBgInImagesNeeded { // DETECTION SUCCESS HAHA
            print("detection real success")
            shouldDetectBackground = false
            isDetectingBackground = false
            var timeSinceDetectionStart = TimeInterval(0)
            if let _ = detectionStartDate {
                timeSinceDetectionStart = Date().timeIntervalSince(detectionStartDate!)
            }
            let delay = timeSinceDetectionStart < 4 ? 4 - timeSinceDetectionStart : 0
            let currentCount = detectionAttemptCount
            Utils.dispatchAfter(delay, closure: { [weak self] in
                guard let strongSelf = self else { return }
                guard currentCount == self!.detectionAttemptCount, !self!.cancelSuccessMessage else { return }
                strongSelf.backgroundWasDetected = true
                Utils.dispatchOnMainThread {
                    strongSelf.playerLayer?.isHidden = false
                    strongSelf.captureSessionPreviewLayer?.removeFromSuperlayer()
//                    strongSelf.captureView.layer.sublayers?.removeLast()
                    strongSelf.delegate.backgroundWasDetected()
                    print("Detection fake success after delay")
                }
            })
        }
        
        //print("max range count: \(maxGroupCount)  pixels in image: \(pixelsCount)")
        return isUniq
    }
    
    
    func getRGBPixelColorAtPoint(cgImage: CGImage, point: CGPoint) -> (r: UInt8, g: UInt8, b: UInt8)? {
        
        let width = cgImage.width
        let height = cgImage.height
        //        let x = Int(point.x)
        //        let y = height - Int(point.y)
        //        guard let provider = CGImageGetDataProvider(cgImage),
        //        bitmapData = CGDataProviderCopyData(provider) else {
        //            return nil
        //        }
        //        let data = CFDataGetBytePtr(bitmapData)
        //
        //        let offset = ((width * y) + x) * 4
        //        let red = data[offset]
        //        let green = data[offset+1]
        //        let blue = data[offset+2]
        //
        //        return (r: red, g: green, b: blue)
        
        guard let pixelData = cgImage.dataProvider!.data else {
            return nil
        }
        let data = CFDataGetBytePtr(pixelData)
        let x = Int(point.x)
        let y = Int(point.y)
        let index = Int(width) * y + x
        let expectedLengthA = Int(width * height)
        let expectedLengthRGB = 3 * expectedLengthA
        let expectedLengthRGBA = 4 * expectedLengthA
        let numBytes = CFDataGetLength(pixelData)
        switch numBytes {
        case expectedLengthA:
            return (r: 0, g: 0, b: 0)
        case expectedLengthRGB:
            return (r: UInt8(CGFloat((data?[3*index])!)/255.0), g: UInt8(CGFloat((data?[3*index+1])!)/255.0), b: UInt8(CGFloat((data?[3*index+2])!)/255.0))
        case expectedLengthRGBA:
            return (r: UInt8(CGFloat((data?[4*index])!)/255.0), g: UInt8(CGFloat((data?[4*index+1])!)/255.0), b: UInt8(CGFloat((data?[4*index+2])!)/255.0))
        default:
            // unsupported format
            return nil
        }
    }
    
    func CGImageToCVPixelBuffer(image: CGImage) -> (pixelBuffer: CVPixelBuffer?, status: CVReturn) {
        var pbuff: CVPixelBuffer?
        
        // CFDictionary stuff
        let keys: [CFString] = [kCVPixelBufferCGImageCompatibilityKey, kCVPixelBufferCGBitmapContextCompatibilityKey]
        let values: [CFTypeRef] = [kCFBooleanTrue, kCFBooleanTrue]
        let keysPointer = UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 1)
        let valuesPointer = UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 1)
        keysPointer.initialize(to: keys, count: 1)
        valuesPointer.initialize(to: values, count: 1)
        let options = CFDictionaryCreate(kCFAllocatorDefault, keysPointer, valuesPointer, keys.count, nil , nil )
        
        let newWidth = image.width
        let newHeight = image.height
        
        let imageData = image.dataProvider?.data
        let imageDataPointer = CFDataGetMutableBytePtr(imageData as! CFMutableData!)
        let bytesPerRow = image.bytesPerRow
        let status = CVPixelBufferCreateWithBytes(kCFAllocatorDefault,
                                     newWidth,
                                     newHeight,
                                     kCVPixelFormatType_32ARGB,
                                     imageDataPointer!,
                                     bytesPerRow,
                                     nil,
                                     nil,
                                     options,
                                     &pbuff);
        
//        let status: CVReturn = CVPixelBufferCreate(kCFAll ocatorDefault,
//                                                   Int(newWidth),
//                                                   Int(newHeight),
//                                                   kCVPixelFormatType_32ARGB,
//                                                   options,
//                                                   &pbuff);
//        
//        guard let strongPxbuff = pbuff else {
//            return (nil, status)
//        }
//        
//        CVPixelBufferLockBaseAddress(strongPxbuff, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
//        let pxdata = CVPixelBufferGetBaseAddress(strongPxbuff)
//        
//        let context = CGContext(data: pxdata, width: newWidth, height: newHeight, bitsPerComponent: 8, bytesPerRow: 4*newWidth, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: CGImageAlphaInfo(rawValue: 2)!.rawValue)
//        context!.interpolationQuality = CGInterpolationQuality.low
//        context!.draw(image, in: CGRect(x: 0, y: 0, width: CGFloat(newWidth), height: CGFloat(newHeight)))
        
        return (pbuff, status)
    }
    
    deinit {
        if let _ = playerTimeObserver {
            player?.removeTimeObserver(playerTimeObserver!)
        }
    }
}
