//
//  SIAInstagramManager.m
//  SingrIphoneApp
//
//  Created by Roni Shoham on 29/12/13.
//  Copyright (c) 2013 SingrFM. All rights reserved.
//

#import "SIAInstagramManager.h"
#define KAUTHURL @"https://api.instagram.com/oauth/authorize/"
#define kAPIURl @"https://api.instagram.com/v1/users/"
#define KCLIENTID @"8e903240e77e4f209229d01343b10e94"
#define KCLIENTSERCRET @"15c54e0217c44552900b864339487afa"
#define kREDIRECTURI @"http://singr.fm"

@implementation SIAInstagramManager
@synthesize docFile = _docFile;

-(void)saveToInstagram:(UIViewController *) view{
    CGRect rect = CGRectMake(0 ,0 , 0, 0);
    UIGraphicsBeginImageContextWithOptions(view.view.bounds.size, view.view.opaque, 0.0);
    [view.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIGraphicsEndImageContext();
    NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/test.igo"];
    //NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/IMG_0044.igo"];
    
    NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", jpgPath]];
    //    NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", screenshot]];
    self.docFile.UTI = @"com.instagram.photo";
    self.docFile = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
    self.docFile=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
    [self.docFile presentOpenInMenuFromRect: rect    inView: view.view animated: YES ];
   
    
}
- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

- (void)documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller {
    
}




@end
