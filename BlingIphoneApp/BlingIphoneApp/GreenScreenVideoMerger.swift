//
//  GreenScreenVideoMerger.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 12/6/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class GreenScreenVideoMerger: NSObject {

    private var backgroundImageGenerator: AVAssetImageGenerator
    private var recordedAssetReader: AVAssetReader
    private var recordedAssetReaderOutput: AVAssetReaderTrackOutput
    private var encoder: SIAVideoEncoder
    private let sampleWritingQueue = DispatchQueue(label: "gy.blin.sampleWritingQueue")
    private let compositingFilter = CIFilter(name: "CISourceOverCompositing")!
    private let ciContext = CIContext(eaglContext: EAGLContext(api: EAGLRenderingAPI.openGLES2))
    weak private var colorCube: CIFilter?
    private let isInWhiteRange: Bool
    private let minGroupHue: Float
    private let maxGroupHue: Float
    private let recordingSpeedScale: Float64
    
    init(backgroundAsset: AVAsset!, recordedAsset: AVAsset!, recordingSpeedScale: Float64 = 1,
         colorCube: CIFilter, isInWhiteRange: Bool, minGroupHue: Float, maxGroupHue: Float) {
        
        self.colorCube = colorCube;
        self.isInWhiteRange = isInWhiteRange
        self.minGroupHue = minGroupHue
        self.maxGroupHue = maxGroupHue
        self.recordingSpeedScale = recordingSpeedScale
        
        backgroundImageGenerator = AVAssetImageGenerator(asset: backgroundAsset)
        backgroundImageGenerator.requestedTimeToleranceAfter = kCMTimeZero
        backgroundImageGenerator.requestedTimeToleranceBefore = kCMTimeZero
        
        recordedAssetReader = try! AVAssetReader(asset: recordedAsset)
        
        let recordedTrack = recordedAsset.tracks(withMediaType: AVMediaTypeVideo).first!
        
        recordedAssetReaderOutput = AVAssetReaderTrackOutput(track: recordedTrack,
                                                             outputSettings: [String(kCVPixelBufferPixelFormatTypeKey) : NSNumber(value: kCVPixelFormatType_32BGRA)])
        recordedAssetReaderOutput.alwaysCopiesSampleData = false
        recordedAssetReader.add(recordedAssetReaderOutput)
        
        let documentDirectoryURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let filePath = documentDirectoryURL.appendingPathComponent("mergedVideo\(Int(Date().timeIntervalSince1970)).mp4").path
        
        encoder = SIAVideoEncoder(forPath: filePath,
                                  height: Int32(recordedTrack.naturalSize.height),
                                  width: Int32(recordedTrack.naturalSize.width),
                                  pixelFormat: kCVPixelFormatType_32ARGB)
        
        super.init()
    }
    
    func mergeVideos(completion: @escaping (_ outputURL: URL) -> ()) {
        
        recordedAssetReader.startReading()
        while let recordedBuff = recordedAssetReaderOutput.copyNextSampleBuffer() {
            
            var recordedTime = CMSampleBufferGetPresentationTimeStamp(recordedBuff)
            let backgroundTime = CMTimeMultiplyByFloat64(recordedTime, recordingSpeedScale)
            guard let bottomImage = try? backgroundImageGenerator.copyCGImage(at: backgroundTime,
                                                                              actualTime: nil) else {
                                                                                print("no background frame at time \(CMTimeGetSeconds(backgroundTime))")
                                                                                continue
            }
            
            if recordingSpeedScale != 1 {
                recordedTime = CMTimeMultiplyByFloat64(recordedTime, 1.0 / (recordingSpeedScale * 2.0))
            }
            
            
            guard let topPxBuff = CMSampleBufferGetImageBuffer(recordedBuff) else { continue }
            autoreleasepool {
                if let mergedPxBuff = mergeFrames(topPxBuff: topPxBuff,
                                                  bottomImage: bottomImage) {
                    // let mergedImage = CIImage(cvPixelBuffer: mergedPxBuff)
                    encoder.encodeVideoFrame(mergedPxBuff,
                                             presentationTime: recordedTime)
                } else {
                    print("NO MERGED BUFFER")
                }
            }
        }
        encoder.finish { (outputURL) in
            completion(outputURL!)
        }
    }
    
    private func mergeFrames(topPxBuff: CVPixelBuffer, bottomImage: CGImage) -> CVPixelBuffer? {
        //                let bottomImage = UIImage(cgImage: imageRef)
        //                let topImage = UIImage(cgImage: outputImageRef!)
        //
        //                // merge 2 images to one
        //                let size = self!.backgroundVideoSize!
        //
        
//        let topCIImage = CIImage(cvPixelBuffer: topPxBuff)
//        let bottomCIImage = CIImage(cgImage: bottomImage)
//        let topImage = UIImage(ciImage: topCIImage)
//        let bottomImage = UIImage(ciImage: bottomCIImage)
//        let size = bottomImage.size
//        UIGraphicsBeginImageContext(size)
//        let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
//        bottomImage.draw(in: areaSize)
//        topImage.draw(in: areaSize, blendMode: CGBlendMode.normal, alpha: 1.0)
//        let mergedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
        var mergedPxBuff: CVPixelBuffer?
        CVPixelBufferCreate(nil,
                            CVPixelBufferGetWidth(topPxBuff),
                            CVPixelBufferGetHeight(topPxBuff),
                            kCVPixelFormatType_32ARGB,
                            nil,
                            &mergedPxBuff)
        let topImage = CIImage(cvPixelBuffer:topPxBuff)
        colorCube?.setValue(topImage,
                           forKey: "inputImage")
        let topImageWithAlpha = colorCube?.outputImage
        compositingFilter.setValue(topImageWithAlpha,
                                    forKey: "inputImage")
        compositingFilter.setValue(CIImage(cgImage: bottomImage),
                                   forKey: "inputBackgroundImage")
        if let outputImage = compositingFilter.outputImage {
            ciContext.render(outputImage,
                             to: mergedPxBuff!,
                             bounds: outputImage.extent,
                             colorSpace: CGColorSpaceCreateDeviceRGB())
        }
        return mergedPxBuff
    }
    
//    private func CGImageToCVPixelBuffer(image: CGImage) -> CVPixelBuffer? {
//        var pbuff: CVPixelBuffer?
//        
//        // CFDictionary stuff
//        let keys: [CFString] = [kCVPixelBufferCGImageCompatibilityKey, kCVPixelBufferCGBitmapContextCompatibilityKey]
//        let values: [CFTypeRef] = [kCFBooleanTrue, kCFBooleanTrue]
//        let keysPointer = UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 1)
//        let valuesPointer = UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 1)
//        keysPointer.initialize(to: keys, count: 1)
//        valuesPointer.initialize(to: values, count: 1)
//        let options = CFDictionaryCreate(kCFAllocatorDefault, keysPointer, valuesPointer, keys.count, nil , nil )
//        
//        let newWidth = image.width
//        let newHeight = image.height
//        
//        let imageData = image.dataProvider?.data
//        let imageDataPointer = CFDataGetMutableBytePtr(imageData as! CFMutableData!)
//        let bytesPerRow = image.bytesPerRow
//        let status = CVPixelBufferCreateWithBytes(kCFAllocatorDefault,
//                                                  newWidth,
//                                                  newHeight,
//                                                  kCVPixelFormatType_32ARGB,
//                                                  imageDataPointer!,
//                                                  bytesPerRow,
//                                                  nil,
//                                                  nil,
//                                                  options,
//                                                  &pbuff);
//        
//        //        let status: CVReturn = CVPixelBufferCreate(kCFAllocatorDefault,
//        //                                                   Int(newWidth),
//        //                                                   Int(newHeight),
//        //                                                   kCVPixelFormatType_32ARGB,
//        //                                                   options,
//        //                                                   &pbuff);
//        //
//        //        guard let strongPxbuff = pbuff else {
//        //            return (nil, status)
//        //        }
//        //
//        //        CVPixelBufferLockBaseAddress(strongPxbuff, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
//        //        let pxdata = CVPixelBufferGetBaseAddress(strongPxbuff)
//        //
//        //        let context = CGContext(data: pxdata, width: newWidth, height: newHeight, bitsPerComponent: 8, bytesPerRow: 4*newWidth, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: CGImageAlphaInfo(rawValue: 2)!.rawValue)
//        //        context!.interpolationQuality = CGInterpolationQuality.low
//        //        context!.draw(image, in: CGRect(x: 0, y: 0, width: CGFloat(newWidth), height: CGFloat(newHeight)))
//        
//        return pbuff
//    }
//    
//    func createColorCube() {
//        // create color cube and 'remove' color if in the color range (of saturation & value)
//        // by setting alpha to 0 ('removed' color value) or 1 (color value stays on)
//        let size = 32
//        let amount = size * size * size
//        var cubeData = [Float](repeating: 0, count: amount * 4)
//        var rgb: [Float] = [0, 0, 0]
//        var hsv: HSV
//        var offset = 0
//        
//        for z in 0..<size {
//            rgb[2] = Float(z) / (Float(size)-1)  // blue value
//            for y in 0..<size {
//                rgb[1] = Float(y) / (Float(size)-1) // green value
//                for x in 0..<size {
//                    rgb[0] = Float(x) / (Float(size)-1) // red value
//                    hsv = RGBtoHSV(r: rgb[0], g: rgb[1], b: rgb[2]) // convert RGB to HSV
//                    
//                    let alpha: Float = hsv.v == 0 ? 0.0 : 1.0
////                    if isInWhiteRange {
////                        alpha = (hsv.s <= 0.25 && hsv.v >= 0.2) ? 0 : 1
////                    } else {
////                        alpha = (hsv.h >= minGroupHue && hsv.h <= maxGroupHue && (hsv.h > 0.04 || hsv.h < 0) && (hsv.h < 0.16 || hsv.h > 0.24) && (hsv.h > 0.0875 || hsv.h < 0.1125)) ? 0 : 1 //hsv.s >= 0.2 && hsv.v >= 0.3
////                    }
//                    
//                    // calculate premultiplied alpha values for the cube
//                    cubeData[offset] = rgb[0] * alpha
//                    cubeData[offset+1] = rgb[1] * alpha
//                    cubeData[offset+2] = rgb[2] * alpha
//                    cubeData[offset+3] = alpha
//                    offset += 4 // advance our pointer into memory for the next color value
//                }
//            }
//        }
//        
//        let data = NSData(bytes: cubeData, length: cubeData.count * MemoryLayout<Float>.size)
//        colorCube.setValue(size, forKey: "inputCubeDimension")
//        colorCube.setValue(data, forKey: "inputCubeData")
//    }
//    
//    func RGBtoHSV(r : Float, g : Float, b : Float) -> HSV {
//        var h : CGFloat = 0
//        var s : CGFloat = 0
//        var v : CGFloat = 0
//        
//        var col: UIColor?
//        do {
//            col = try? UIColor(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: 1.0)
//            col?.getHue(&h, saturation: &s, brightness: &v, alpha: nil)
//        }
//        
//        let hsv: HSV = (h: Float(h), s: Float(s), v: Float(v))
//        return hsv
//    }
}
