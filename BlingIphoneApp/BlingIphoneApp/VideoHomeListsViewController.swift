//
//  VideoHomeListsViewController.swift
//  ChosenIphoneApp
//
//  Created by Zach on 04/09/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

import UIKit
import RestKit
import SDWebImage

class VideoHomeListsViewController : SIATableViewContainedViewController, SIATextCommentViewDelegate
{
    //MARK: constants
    fileprivate let LOADING_ID  = "loadingCell"
    fileprivate let LIKE_CELL_ID = "userCell"
    fileprivate let COMMENT_CELL_ID = "commentCell"
    fileprivate let separatorColor = UIColor.white.withAlphaComponent(0.15)

    let kLikeTab = 1
    let kCommentTab = 0
    
    //MARK: properties
    var tab = 1
    var background: UIImage? = nil
    var video: SIAVideo?
    var videoId: String?
    var backgroundSet = false
    var backgroundTintColor: UIColor?
    var longPressRecognizer: UILongPressGestureRecognizer?
    var selectedComment: SIATextComment?
    fileprivate var paginators = [SIAPaginator?](repeating: nil, count: 3)
    fileprivate var commentUpdateObserver: NSObjectProtocol?

    
    //MARK: outlets
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var commentsTabButton: UIButton!
    @IBOutlet weak var likesTabButton: UIButton!
    @IBOutlet weak var loadingImage: UIImageView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet var noDataImage: UIImageView!
    @IBOutlet var noDataView: UIView!
    @IBOutlet weak var textCommentView: SIATextCommentView!
    @IBOutlet weak var backgroundAlphaLayer: UIView!
    @IBOutlet var tabHighlightUnderCommentConstraint: NSLayoutConstraint!
    @IBOutlet weak var tabView: UIView!
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        longPressRecognizer = UILongPressGestureRecognizer(target: self,
                                                           action: #selector(longPress(_:)))
    }
    
    
    //MARK: Comments
    func textCommentView(_ commentView: SIATextCommentView, userDidSendComment newComment: String)
    {
        guard let video = video else { return }
        guard !newComment.isEmpty else { return }
        if (selectedComment != nil) { // update action
            selectedComment?.edit(newComment,
                                  withCompletionHandler: {[weak self] (completed) in
                                    self?.selectedComment!.text = newComment
                                    self?.selectedComment = nil
            })
        }
        else {
            SIATextCommentModel.postComment(newComment, forVideo: video.videoId, withCompletionHandler: {[weak self] (model: SIATextComment?) -> Void in
                self?.textCommentView.comment = "Say something...";
                var totalComments = Int(video.total_comments)!
                totalComments += 1
                video.total_comments = String(totalComments)
                })
        }

    }
    
    func textCommentViewDidEndEditing(_ commentView: SIATextCommentView) {
        commentView.comment = "Say something...";
    }
    
    func textCommentViewShouldBeginEditing(_ commentView: SIATextCommentView) -> Bool
    {
        return true
    }
    
    func setCommentUpdateObserver() {
        self.commentUpdateObserver = NotificationCenter.default.addObserver(
            forName: NSNotification.Name.SIACommentUpdate,
            object: nil,
            queue: nil,
            using: { [weak self] (note: Notification) in
                let comment = note.object as! SIATextComment
                let edited = note.userInfo!["edited"] as! Bool
                if (comment.videoId == self?.video?.videoId) {
                    print("observed comment for this video \\(o0)/")
                    if (edited) {
                        self?.replaceComment(comment)
                    }
                    else {
                        self?.addComment(comment)
                    }
                }
            })
    }
    
    func addComment(_ comment: SIATextComment) {
        //        comment.user = _currentUser;
        
//            self.totalCommentsLabel.text = self.totalCommentsLabel.text == "" ? "1" : String(Int(self.totalCommentsLabel.text!)! + 1)
            //add comment
            let firstPageArray = paginators[kCommentTab]!.paginatorModel.pageDict.object(forKey: "0")
            if (firstPageArray != nil) {
                (firstPageArray! as AnyObject).insert(comment, at:0)
            }
            paginators[kCommentTab]!.paginatorModel.pageDict["0"] = firstPageArray
            paginators[kCommentTab]!.paginatorModel.totalObjects = ((paginators[kCommentTab]!.paginatorModel.totalObjects as Int) + 1) as NSNumber
            paginators[kCommentTab]!.results.insert(comment, at: 0)
            tableView.reloadData()
            hasData()
    }

    func replaceComment(_ newComment: SIATextComment) {
//        let commentsArray = paginators[kCommentTab]!.results as Array
//        let index = paginators[kCommentTab]!.results
//            .index(of: commentsArray.first(where: {$0.commentId == newComment.commentId}))
//        
//        paginators[kCommentTab]!.results.replaceObject(at: index, with: newComment)
        tableView.reloadData()
    }
    
    @IBAction func switchToLikes(_ sender: AnyObject) {
        if (kLikeTab == tab) {
            return
        }
        tab = kLikeTab
        initTab(sender)
        moveTabHighlight()
        tableView.removeGestureRecognizer(longPressRecognizer!)
    }
    
    @IBAction func swicthToComments(_ sender: AnyObject) {
        if (kCommentTab == tab) {
            return
        }
        tab = kCommentTab
        initTab(sender)
        moveTabHighlight()
        tableView.addGestureRecognizer(longPressRecognizer!)
    }
    
    func moveTabHighlight() {
        tabHighlightUnderCommentConstraint.isActive = (tab == kCommentTab)
        
        UIView.animate(
            withDuration: 0.3,
            delay: 0,
            options: .curveEaseInOut,
            animations: { [weak self] in
                self?.tabView.layoutIfNeeded()
            },
            completion: nil
        )
    }
    
    //MARK: view controller methods
    override func viewDidLoad() {
        tabView.alpha = 0
        navigationController.navigationBar.tintColor = UIColor.white
        if let _ = video {
            videoId = video?.videoId
        }
        else
        { // if lists VC was opened on top of video home (i.e. from a push notification) we need to load the video here
            SIAVideo.getDataForVideoId(
                videoId!,
                withCompletionHandler: {[weak self] (video) in
                    guard let sSelf = self else { return }
                    sSelf.video = video
                    sSelf.navigationController.setNavigationItemTitle(video?.song_name)
                    
                    //if previous video is video home
                    let idx = sSelf.navigationController.viewControllers.index(of: sSelf)
                    if let videoHomeVC = sSelf.navigationController.viewControllers[idx! - 1] as? VideoHomeViewController {
                        videoHomeVC.video = video?.copy() as! SIAVideo?
                    }
            })
        }
        if (background != nil) {
            print("background was passed into list view")
            backgroundImage.image = background
            //backgroundImage.blur(.dark)
            //backgroundSet = true
        }
        if (backgroundTintColor != nil) {
            backgroundAlphaLayer.backgroundColor = backgroundTintColor
        }
        
        paginators = [
            SIAPaginator(modelClass: SIATextCommentModel.self as AnyClass, objectId: videoId, delegate: self as SIAPaginatorDelegate),
            SIAPaginator(modelClass: SIAVideoLikesDataModel.self as AnyClass, objectId: videoId, delegate: self as SIAPaginatorDelegate),
        ]
        
        //commentview
        textCommentView.alpha = 0
        textCommentView.backgroundIdleColor = UIColor.white
        textCommentView.setBackgroundColor(UIColor.white)
        textCommentView.layer.cornerRadius = 0.0
        textCommentView.floatTextInput = false
        textCommentView.name = "You!"
        textCommentView.comment = "Say something..."
        textCommentView.keyboardAppearance = UIKeyboardAppearance.dark
        textCommentView.delegate = self
        textCommentView.commentColor = "black"
        textCommentView.addAction(withType: SIATextCommentActionSendType)
        BIALoginManager.sharedInstance().myUser?.image(completionHandler: {[weak self](image: UIImage?) -> Void in self?.textCommentView.image = image})
        
        initTab(nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController.setBottomBarHidden(true, animated: false)
        navigationController.setNavigationItemTitle(video?.song_name)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController.setNavigationBarHidden(false, animated: true)
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.tabView.alpha = 1
        }
    }
    
    override func viewDidLayoutSubviews() {
        guard textCommentView.frame.size.height != 1000 else { return }
        tableView.contentInset = tab == kCommentTab ? UIEdgeInsetsMake(0, 0, textCommentView.frame.size.height, 0) : UIEdgeInsets()
    }
    
    func setBlurredBackground(_ backgroundImage: UIImage, animated: Bool, tintColor: UIColor?) {
        backgroundSet = true
        print("background set for list view")
        background = backgroundImage
        Utils.dispatchOnMainThread({[weak self] in
            guard let sSelf = self else { return }
            if (animated) {
            UIView.transition(with: sSelf.backgroundImage, duration: 1.4, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                sSelf.backgroundImage.image = sSelf.background
                sSelf.backgroundAlphaLayer.backgroundColor = tintColor
                }, completion: nil)
            } else {
                sSelf.backgroundImage.image = sSelf.background
                sSelf.backgroundAlphaLayer.backgroundColor = tintColor
            }
        })
            
    }
    
    fileprivate func initTab(_ tabButton: AnyObject?) {
        tableView.reloadData()
        noDataView.isHidden = true
        
        if (tab != kLikeTab) {
            likesTabButton.alpha = 0.4
        }
        if (tab != kCommentTab) {
            (self as SIABaseViewController).fadeOutView(textCommentView)
            commentsTabButton.alpha = 0.4
        } else {
//            view.addSubview(textCommentView)
        }
        
        if (tabButton != nil && tabButton is UIButton) {
            (tabButton as! UIButton).alpha = 1
        }
        
        if (tab == kCommentTab) {
            (self as SIABaseViewController).fade(in: textCommentView)
            tableView.addGestureRecognizer(longPressRecognizer!)
            if (commentUpdateObserver == nil) {
                setCommentUpdateObserver()
            }
        }

        if (currentPaginator().results.count < 1) {
            currentPaginator().fetchFirstPage()
            startLoadingAnimation()
        } else {
            tableView.reloadData()
        }
    }
    
    override func startLoadingAnimation() {
        super.startLoadingAnimation()
        loadingImage.spin(withDuration: 0.5, rotations: 1, repeat: HUGE)
    }
    
    override func finishLoadingWithAnimation() {
        self.tableView.isHidden = false
        super.finishLoadingWithAnimation()
        UIView.animate(withDuration: 0.1, animations: {[weak self] in
            self?.loadingImage.alpha = 0.0
        })
    }
    
    override func noData() {
        if tab == kCommentTab {
            noDataLabel.text = "Be the first to Comment"
            noDataImage.image = UIImage(named: "extra_large_icon_nodata_comments")
        } else if tab == kLikeTab {
            noDataLabel.text = "Be the first to Like"
            noDataImage.image = UIImage(named: "extra_large_icon_nodata_likes")
        }
        noDataView.isHidden = false
    }
    
    override func hasData() {
        noDataView.isHidden = true
    }
    
    //MARK: table view methods
    fileprivate func placeholderForRow(_ row:Int?) -> UIImage? {
        return UIImage.init(named: "extra_large_userpic_placeholder.png")
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (tab == kCommentTab) {
            return 80.0
        } else {
            return 58.0
        }
    }
    
    override func imageURL(forRow row:NSInteger) -> URL? {
        if (paginators[tab]?.results[row] is SIALike) {
            return (paginators[tab]?.results[row] as! SIALike).user?.imageURL
        }
        if (paginators[tab]?.results[row] is SIATextComment) {
            return (paginators[tab]?.results[row] as! SIATextComment).user?.imageURL
        } else { return nil }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = TransparentListHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
        header.label.text = tableHeaderString()
        return header
    }
    
    override func tableHeaderSize() -> CGFloat {
        return 30.0
    }
    
     override func tableHeaderString() -> String? {
        if (currentPaginator().paginatorModel?.totalObjects == nil) {
            return ""
        }
        switch tab {
        case kCommentTab:
            let commentComments = currentPaginator().paginatorModel?.totalObjects == 1 ? "comment" : "comments"
            return String.init(format: "%@ %@",(currentPaginator().paginatorModel?.totalObjects)!, commentComments)
        default:
            let likeLikes = currentPaginator().paginatorModel?.totalObjects == 1 ? "like" : "likes"
            return String.init(format: "%@ %@", (currentPaginator().paginatorModel?.totalObjects)!, likeLikes)
        }
    }
    
    override func cellIdentifier(forRow row:NSInteger) -> String? {
        if (currentPaginator().results[row] is NSNull) {
            return LOADING_ID
        }
        
        switch tab {
        case kLikeTab:
            return LIKE_CELL_ID
        case kCommentTab:
            return COMMENT_CELL_ID
        default:
            return ""
        }
    }
    
    override func textLabelString(forRow row:NSInteger) -> NSAttributedString {
        if (tab != kCommentTab) {
            return NSAttributedString.init(string: "")
        } else {
            return NSAttributedString.init(string: "")
        }
    }
    
    override func headerBackgroundColor() -> UIColor {
        return UIColor.white.withAlphaComponent(0.15)
    }
    
    override func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath) {
        if (currentPaginator().results[(indexPath as NSIndexPath).row] is SIALike)
        {
            let user = (currentPaginator().results[(indexPath as NSIndexPath).row] as! SIALike).user
            (self.navigationController as BIANavigationController).switchToProfileViewController(withUserId: user?.user_id)
        }
        else
        {
            let user = (currentPaginator().results[(indexPath as NSIndexPath).row] as! SIATextComment).user
            (self.navigationController as BIANavigationController).switchToProfileViewController(withUserId: user?.user_id)
        }
    }
    
    override func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return currentPaginator().results.count
    }
    
    override func tableView(_ tableView:UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath:IndexPath) {
        if (cell is SIALoadingTableViewCell) {
            currentPaginator().fetchNextPage()
        }
    }
    
    
    override func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier(forRow: (indexPath as NSIndexPath).row)!, for: indexPath)
        
        if (cell is SIALoadingTableViewCell)
        {
            return cell
        }
        
        cell.backgroundColor = UIColor.clear
        cell.textLabel!.numberOfLines = 0
        cell.textLabel!.textColor = UIColor.white
        cell.textLabel!.font = UIFont.init(name: "Roboto-Regular", size: 15.0)
        cell.detailTextLabel?.textColor = UIColor.init(hex: "#939598")
        cell.imageView!.contentMode = UIViewContentMode.scaleAspectFill
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.imageView?.backgroundColor = UIColor.black
        cell.accessoryView = viewForAccessoryView(forRow: UInt((indexPath as NSIndexPath).row))
        
        var object = nil as AnyObject?
        //init cell according to cell class
        if (currentPaginator().results[(indexPath as NSIndexPath).row] is SIALike) // LIKE
        {
            object = (currentPaginator().results[(indexPath as NSIndexPath).row] as! SIALike).user
            cell.textLabel!.text = object?.fullName
        }
        else if (currentPaginator().results[(indexPath as NSIndexPath).row] is SIATextComment) // COMMENTS
        {
            object = (currentPaginator().results[(indexPath as NSIndexPath).row] as! SIATextComment).user
            if (object == nil) {
                cell.textLabel!.text = "You"
                (cell as? SIACommentTableViewCell)?.nameLabel!.text = "You"
            } else {
                cell.textLabel!.text = object!.fullName
                (cell as? SIACommentTableViewCell)?.nameLabel!.text = object!.fullName
            }
            (cell as? SIACommentTableViewCell)?.commentLabel!.text = (currentPaginator().results[(indexPath as NSIndexPath).row] as? SIATextComment)!.text
        }
        
        //round image for users
        if (object is SIAUser)
        {
            cell.imageView!.layer.cornerRadius = (cell.imageView?.bounds.size.width)! / 2.0
        } else {
            cell.imageView!.layer.cornerRadius = 0
        }
        
        //load images
        if ((indexPath as NSIndexPath).row < self.currentPaginator().results.count) {
//            SDWebImage.sharedDownloader()
            
            cell.imageView?.sd_setImage(with: imageURL(forRow: (indexPath as NSIndexPath).row),
                                                placeholderImage: placeholderForRow((indexPath as NSIndexPath).row),
                                                options: [],
                                                completed: ({(image: Optional<UIImage>, error: Optional<Error>, cacheType: SDImageCacheType, imageUrl: Optional<URL>) in
                if (image != nil) {
                    UIView.transition(with: (cell.imageView)!,
                        duration: 0.1,
                        options: UIViewAnimationOptions.transitionCrossDissolve,
                        animations: {cell.imageView!.image = image},
                        completion: nil
                    )
                }
            }))
        }
        
        return cell
    }
    
    override func pageLoaded(fromPaginator paginator:Any?) {
        if ((paginator as! SIAPaginator) != currentPaginator()) {
            return
        }
        if ((currentPaginator().page == 0) && (currentPaginator().results.count < 1)) {
            noData()
        } else {
            hasData()
        }
        
//        if (tab != kCommentTab) {
//            textCommentView.removeFromSuperview()
//        }
        
        refreshView()
    }
    
    override func refreshView() {
        refreshView(false)
    }
    
    func refreshView(_ tabChange: Bool) {
        if (currentPaginator().paginatorModel.currentPage == 0 || tabChange) {
            tableView.beginUpdates()
            tableView.reloadData()
            tableView.reloadSections(IndexSet.init(integer: 0), with: UITableViewRowAnimation.automatic)
            tableView.endUpdates()
            finishLoadingWithAnimation()
        } else {
            let indexPaths = currentPaginator().indexPathForCurrentPage()
            let lastIndex = indexPaths?.last
            let firstIndex  = indexPaths?.first
            
            if ((lastIndex! as AnyObject).row < currentPaginator().results.count) {
                tableView.beginUpdates()
                tableView.deleteRows(at: [firstIndex as! IndexPath], with: UITableViewRowAnimation.top)
                tableView.insertRows(at: indexPaths as! [IndexPath], with: UITableViewRowAnimation.top)
                tableView.endUpdates()
            }
        }
    }
    
    override func noMorePages(forPaginator paginator:Any?) {
        if ((paginator as! SIAPaginator) != currentPaginator()) {
            return
        }
        let indexOfLastRow = IndexPath.init(row: currentPaginator().results.count, section: 0)
        Utils.dispatchOnMainThread({[weak self] in
            self?.tableView.deleteRows(at: [indexOfLastRow], with: UITableViewRowAnimation.automatic)
        })
    }
    
    func currentPaginator() -> SIAPaginator {
        return paginators[tab]!
    }
    
    //MARK: Long press editing
   
    
    func longPress(_ sender: UILongPressGestureRecognizer) {
        guard let indexPath = tableView.indexPathForRow(at: sender.location(in: tableView)),
              let comment = currentPaginator().results[indexPath.row] as? SIATextComment else {
            return
        }
        
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Cancel",
                                      style: .cancel,
                                      handler: {[weak self] (action) in
                                    self?.selectedComment = nil
        }))
        
        let actionClosure =  {[weak self] (action: UIAlertAction) in
            //edit stuff
            let commentActionController =
                SIATextCommentActionsController(user: BIALoginManager.sharedInstance().myUser)
                    .actionsAlert(for: comment,
                                  presenting: self)
            self?.present(commentActionController!,
                          animated: true,
                          completion: nil)
        }
        let isMe = comment.user.user_id == BIALoginManager.sharedInstance().myUser.user_id
        alert.addAction(UIAlertAction(title: isMe ? "Edit" : "Report",
                                      style: .default,
                                      handler: actionClosure))
        if (isMe) {
            selectedComment = comment // selected comment for editing
        }
        
        alert.popoverPresentationController?.sourceView = cell
        alert.popoverPresentationController?.sourceRect = cell.bounds
        
        present(alert,
                animated: true,
                completion: nil)
    }
    
    deinit {
        print("**** ´\\\\_|O _ O|_//` **** \nooh me so dealloc lists \n*************************")
        commentUpdateObserver = nil
    }
    
}
