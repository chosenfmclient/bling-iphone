//
//  SIAPaginator.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 5/11/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import "SIAPaginator.h"

@interface SIAPaginator() {
}

// protected properties
@property (assign, readwrite) NSInteger page;
@property (assign, readwrite) NSInteger total;
@property (nonatomic, strong, readwrite) NSMutableArray *results;
@property (assign, readwrite) RequestStatus requestStatus;

@end

@implementation SIAPaginator


- (id)initWithPageSize:(NSInteger)pageSize delegate:(id<SIAPaginatorDelegate>)paginatorDelegate
{
    if(self = [super init])
    {
        [self setDefaultValues];
        self.currentPageSize = pageSize;
        self.delegate = paginatorDelegate;
        self.requestedPages = [[NSMutableArray alloc] init];
        
    }
    
    return self;
}

- (id)initWithModelClass:(Class)className objectId:(NSString*)objectId delegate:(id<SIAPaginatorDelegate>)paginatorDelegate {
    self = [super init];
    if (self) {
        [self setDefaultValues];
        self.delegate = paginatorDelegate;
        self.paginatorModel = [[className alloc] initWithObjectId:objectId];
        self.requestedPages = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (id)initWithModelClass:(Class)className delegate:(id<SIAPaginatorDelegate>)paginatorDelegate {
    return [self initForModelClass:className delegate:paginatorDelegate];
}

- (id)initForModelClass:(Class)className delegate:(id<SIAPaginatorDelegate>)paginatorDelegate {
    self = [super init];
    if (self) {
        [self setDefaultValues];
        self.delegate = paginatorDelegate;
        self.paginatorModel = [[className alloc] init];
        self.requestedPages = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)setDefaultValues
{
    self.total = 0;
    self.page = 0;
    self.results = [NSMutableArray array];
    self.requestStatus = RequestStatusNone;
}

- (void)reset
{
    [self setDefaultValues];
    
    // send message to delegate
    if([self.delegate respondsToSelector:@selector(paginatorDidReset:)])
        [self.delegate paginatorDidReset:self];
}

- (void)reachedLastPage
{
    if ([self.results.lastObject isKindOfClass:[[NSNull null] class]]) {
        [self.results removeObject:self.results.lastObject];
    }
    if ([_delegate respondsToSelector:@selector(noMorePagesForPaginator:)]) {
        [_delegate noMorePagesForPaginator:self];
    }
    else if ([_delegate respondsToSelector:@selector(noMorePages)]) {
        [_delegate noMorePages];
    }
}

# pragma - fetch results

- (void)fetchFirstPage
{
    // reset paginator
    [self reset];
    [self fetchNextPage];
}

- (void)fetchNextPage
{
    // don't do anything if there's already a request in progress
    if (self.requestStatus == RequestStatusInProgress){
       return;
    }
    self.paginatorModel.currentPage = [NSNumber numberWithInteger:self.page];
    self.requestStatus = RequestStatusInProgress;
    [self.paginatorModel getPageForPath:[self.paginatorModel pagePath]
                          requestParams:[self.paginatorModel requestParams]
                  withCompletionHandler:^(SIAPaginatorDataModel *model)
     {
         if (self.results.count)
         {
             [self.results removeObjectAtIndex:[self.results indexOfObject:self.results.lastObject]];
         }
         [self.results addObjectsFromArray:model.pageArray];
         if (self.page == 0 && model)
             self.paginatorModel.totalObjects = model.totalObjects;
         if (model.pageArray.count)
             [self.results addObject:[NSNull null]];
         self.currentPageSize = model.pageArray.count;
         self.paginatorModel.pageSize = model.pageSize;
         if (model.pageArray.count) {
             self.requestStatus = RequestStatusDone;
             self.page++;
             if ([self.delegate respondsToSelector:@selector(pageLoadedFromPaginator:)]) {
                 [self.delegate pageLoadedFromPaginator:self];
             }
             else {
                 [self.delegate pageLoaded];
             }
         }
         else
         {
             if (self.paginatorModel.currentPage.integerValue == 0)
             {
                 if ([self.delegate respondsToSelector:@selector(pageLoadedFromPaginator:)]) {
                     [self.delegate pageLoadedFromPaginator:self];
                 }
                 else {
                     [self.delegate pageLoaded];
                 }
             }
             else
                 [self reachedLastPage];
         }
     }
                           errorHandler:^(NSError *error) {
                               NSLog(@"%@", error);
                               if ([self.delegate respondsToSelector:@selector(pageLoadedFromPaginator:)]) {
                                   [self.delegate pageLoadedFromPaginator:self];
                               }
                               else {
                                   [self.delegate pageLoaded];
                               }
                               self.requestStatus = RequestStatusDone;
                           }];
}

#pragma mark - Sublclass methods

- (void)fetchResultsWithPage:(NSInteger)page pageSize:(NSInteger)pageSize
{
    
}

- (void) updatePageNumber:(NSInteger)page {
    self.page = page;
}

#pragma mark received results

// call these from subclass when you receive the results

- (void)receivedResults:(NSArray *)results total:(NSInteger)total {
    // [self.results addObjectsFromArray:results];
    //self.page++;
    self.total = total;
    self.requestStatus = RequestStatusDone;
    
    [self.delegate paginator:self didReceiveResults:results];
}

- (void)setCachedResults:(NSArray *)cachedArray pageNumber:(NSNumber *)page {
    self.results = cachedArray.mutableCopy;
    self.page = page.integerValue;
    [_delegate cacheLoaded];
}

- (void)failed
{
    self.requestStatus = RequestStatusDone;
    
    if([self.delegate respondsToSelector:@selector(paginatorDidFailToRespond:)])
        [self.delegate paginatorDidFailToRespond:self];
}

- (void)setResultsForResponses:(NSArray *)array {
    self.results = array.mutableCopy;
}

- (NSArray *)indexPathForCurrentPage {
    return [self indexPathForPage:self.paginatorModel.currentPage];
}


- (NSArray *)indexPathForPage:(NSNumber *)page{
    
    int index = 0;
    for (int i = 0; i < page.integerValue; i++) {
        NSArray *pageArray = [self.paginatorModel.pageDict objectForKey:[NSString stringWithFormat:@"%i", i]];
        index += pageArray.count;
    }
    
    NSInteger firstIndexPage = index;
    NSInteger lastIndex = ((NSArray *)[self.paginatorModel.pageDict objectForKey:page.stringValue]).count + firstIndexPage;
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (long i = firstIndexPage; i <= lastIndex; i++) {
        NSIndexPath *row = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPaths addObject:row];
    }
    return [NSArray arrayWithArray:indexPaths];
}


@end
