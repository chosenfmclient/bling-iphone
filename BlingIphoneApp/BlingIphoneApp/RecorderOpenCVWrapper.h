//
//  RecorderOpenCVWrapper.h
//  BlingIphoneApp
//
//  Created by Joseph Nahmias on 26/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, DetectionResponseS) {
    DetectionResponseSSuccess = 0,
    DetectionResponseSFailure = 1,
    DetectionResponseSHighLight = 2,
    DetectionResponseSLowLight = 3,
    DetectionResponseSTooManyColors = 4,
    DetectionResponseSNoChromaColors = 5,
    DetectionResponseSTooManyEdges = 6,
    DetectionResponseSNoFaceDetection = 7
};


@protocol RecorderOpenCVDelegate <NSObject>
@optional
- (void)recordingDidStart;
- (void)backgroundPlaybackDidStartWithTimeOffset:(CMTime)offset;
- (void)recordingDidStop;
- (void)recorderReadyToRecord;
- (void)recorderNeedsRedetect;
- (void)detectionFinishedWithResponse:(DetectionResponseS) response;
- (void)recorderDidFailToInitialize:(NSError  * _Nullable )error;
- (void)recorderDidFailToReadBackgroundVideo:(NSError * _Nullable)error;
@end

@interface RecorderOpenCVWrapper : NSObject

@property (weak, nonatomic) AVCaptureSession *captureSession;
@property (weak, nonatomic) NSURL *recordURL;
@property (nonatomic) CMTime currentTime;
@property (weak, nonatomic) id <RecorderOpenCVDelegate> delegate;
@property (readonly, nonatomic) BOOL isRecording;
@property (readonly, nonatomic) BOOL isDetecting;
@property (readonly, nonatomic) BOOL isRunning;
@property (readonly, nonatomic) BOOL didDetect;
@property (nonatomic) BOOL usingOldDevice;
@property (nonatomic) BOOL usingFrontCamera;

-(void)setupNewRecorderWithView:(UIView *)view;
-(void)setPlayer:(BIAVideoPlayer *)player;
-(void)switchCamera;
-(void)startRecording;
-(void)stopRecording;
-(void)pauseRecording;
-(void)resumeRecording;
-(void)cancelRecording;
-(void)setRecordingSpeedScale:(Float64)scale;
-(void)startDetection;

-(void)reset;
-(void)resetForRedetect;

-(BOOL)isSetUp;


@end
