//
//  SIANotificationsViewController.h
//  SingrIphoneApp
//
//  Created by SingrFM on 10/6/13.
//  Copyright (c) 2013 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIANotificationsDataModel.h"
#import <RestKit/RestKit.h>
#import "SIATableViewContainedViewController.h"

@interface SIANotificationsViewController : SIATableViewContainedViewController <UITableViewDelegate, UITableViewDataSource>


@property (strong, nonatomic) IBOutlet UIView *noDataView;

- (void) showData;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableTopConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *loadingSpinningImageView;
@property (weak, nonatomic) NSString *status;

@end
