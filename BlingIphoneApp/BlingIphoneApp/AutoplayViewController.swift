//
//  AutoplayViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 08/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
@objc class AutoplayViewController: SIABaseViewController, UITableViewDelegate {
    var indexOfPlayingVideo = -2
    @IBOutlet weak var videoTable: UITableView!

    
    //MARK: View Controller Lifecycle
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        VideoPlayerManager.shared.player.pause()
        VideoPlayerManager.shared.shouldPlay = false
        VideoPlayerManager.shared.player.cancelPendingPrerolls()
        indexOfPlayingVideo = -2
        for cell in videoTable.visibleCells {
            (cell as? HomeVideoTableViewCell)?.isPlayingVideo = false
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        VideoPlayerManager.shared.shouldPlay = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if navigationController.viewControllers.last == self {
           // Utils.dispatchInBackground {[weak self] in
                playVideo(for: getCellOfVideoToPlay())
           // }
        }
    }
    
    //MARK: scroll view delegate
    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        guard videoTable.visibleCells.count > 0 else {
            return
        }
        playVideo(for: videoTable.visibleCells[0] as? HomeVideoTableViewCell)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (!decelerate) {
            playVideo(for: getCellOfVideoToPlay())
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        playVideo(for: getCellOfVideoToPlay())
    }
    
    //MARK: table view delegate
    func tableView(_ tableView: UITableView,
                   didEndDisplaying cell: UITableViewCell,
                   forRowAt indexPath: IndexPath) {
        if (indexPath.section - 1 == indexOfPlayingVideo) {
            VideoPlayerManager.shared.removeVideo()
            (cell as? HomeVideoTableViewCell)?.isPlayingVideo = false
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: Autoplay
    func getCellOfVideoToPlay() -> HomeVideoTableViewCell? {
        
        let visibleCells = videoTable.visibleCells.filter() { $0 is HomeVideoTableViewCell }
        var cells = visibleCells as! [HomeVideoTableViewCell]
        guard cells.count > 0 else {
            return nil
        }
        guard cells.count > 1 else {
            return cells[0]
        }
        
        let rectOne = cells[0].convert(cells[0].videoThumbImage.frame, to: videoTable).intersection(videoTable.bounds)
        let rectTwo = cells[1].convert(cells[1].videoThumbImage.frame, to: videoTable).intersection(videoTable.bounds)
        
        if (rectOne.height >= rectTwo.height) {
            return cells[0]
        }
        
        return cells[1]
    }
    
    func playVideo(for cell: HomeVideoTableViewCell?) {
        guard let _ = cell else {
            return
        }
        let newIndex = (self.videoTable.indexPath(for: cell!)?.section)!
        cell?.layoutIfNeeded()
        //if (newIndex != self.indexOfPlayingVideo) {
            self.indexOfPlayingVideo = newIndex
        VideoPlayerManager.shared.shouldPlay = true
            Utils.dispatchOnMainThread {
                guard let video = cell?.video else { return }
                VideoPlayerManager.shared.setNewPlayerView(view: cell!.videoThumbImage, videoURL: video.videoUrl, withDelegate: cell!)
            }
            cell?.isPlayingVideo = true
            
            if let banner = cell?.banner {
                banner.hideBanner(after: 5)
            }
        //}
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //required for subclass
    }
}
