//  SwiftyJSON.swift
//
//  Copyright (c) 2014 Ruoyu Fu, Pinglin Tang
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation

// MARK: - Error

///Error domain
public let ErrorDomain: String = "SwiftyJSONErrorDomain"

///Error code
public let ErrorUnsupportedType: Int = 999
public let ErrorIndexOutOfBounds: Int = 900
public let ErrorWrongType: Int = 901
public let ErrorNotExist: Int = 500
public let ErrorInvalidJSON: Int = 490

// MARK: - SJSON Type

/**
JSON's type definitions.

See http://www.json.org
*/
public enum Type :Int{

    case Number
    case String
    case Bool
    case Array
    case Dictionary
    case Null
    case Unknown
}

// MARK: - SJSON Base

public struct SJSON {

    /**
    Creates a SJSON using the data.

    - parameter data:  The NSData used to convert to SJSON.Top level object in data is an NSArray or NSDictionary
    - parameter opt:   The SJSON serialization reading options. `.AllowFragments` by default.
    - parameter error: error The NSErrorPointer used to return the error. `nil` by default.

    - returns: The created SJSON
    */
    public init(data:NSData, options opt: NSJSONReadingOptions = .AllowFragments, error: NSErrorPointer = nil) {
        do {
            let object: AnyObject = try NSJSONSerialization.JSONObjectWithData(data, options: opt)
            self.init(object)
        } catch let aError as NSError {
            if error != nil {
                error.memory = aError
            }
            self.init(NSNull())
        }
    }

    /**
     Create a SJSON from SJSON string
    - parameter string: Normal SJSON string like '{"a":"b"}'

    - returns: The created SJSON
    */
    public static func parse(string:String) -> SJSON {
        return string.dataUsingEncoding(NSUTF8StringEncoding)
            .flatMap({SJSON(data: $0)}) ?? SJSON(NSNull())
    }

    /**
    Creates a SJSON using the object.

    - parameter object:  The object must have the following properties: All objects are NSString/String, NSNumber/Int/Float/Double/Bool, NSArray/Array, NSDictionary/Dictionary, or NSNull; All dictionary keys are NSStrings/String; NSNumbers are not NaN or infinity.

    - returns: The created SJSON
    */
    public init(_ object: AnyObject) {
        self.object = object
    }

    /**
    Creates a SJSON from a [JSON]

    - parameter SJSONArray: A Swift array of SJSON objects

    - returns: The created SJSON
    */
    public init(_ jsonArray:[SJSON]) {
        self.init(jsonArray.map { $0.object })
    }

    /**
    Creates a SJSON from a [String: SJSON]

    - parameter SJSONDictionary: A Swift dictionary of SJSON objects

    - returns: The created SJSON
    */
    public init(_ SJSONDictionary:[String: SJSON]) {
        var dictionary = [String: AnyObject](minimumCapacity: SJSONDictionary.count)
        for (key, SJSON) in SJSONDictionary {
            dictionary[key] = SJSON.object
        }
        self.init(dictionary)
    }

    /// Private object
    private var rawArray: [AnyObject] = []
    private var rawDictionary: [String : AnyObject] = [:]
    private var rawString: String = ""
    private var rawNumber: NSNumber = 0
    private var rawNull: NSNull = NSNull()
    /// Private type
    private var _type: Type = .Null
    /// prviate error
    private var _error: NSError? = nil

    /// Object in SJSON
    public var object: AnyObject {
        get {
            switch self.type {
            case .Array:
                return self.rawArray
            case .Dictionary:
                return self.rawDictionary
            case .String:
                return self.rawString
            case .Number:
                return self.rawNumber
            case .Bool:
                return self.rawNumber
            default:
                return self.rawNull
            }
        }
        set {
            _error = nil
            switch newValue {
            case let number as NSNumber:
                if number.isBool {
                    _type = .Bool
                } else {
                    _type = .Number
                }
                self.rawNumber = number
            case  let string as String:
                _type = .String
                self.rawString = string
            case  _ as NSNull:
                _type = .Null
            case let array as [AnyObject]:
                _type = .Array
                self.rawArray = array
            case let dictionary as [String : AnyObject]:
                _type = .Dictionary
                self.rawDictionary = dictionary
            default:
                _type = .Unknown
                _error = NSError(domain: ErrorDomain, code: ErrorUnsupportedType, userInfo: [NSLocalizedDescriptionKey: "It is a unsupported type"])
            }
        }
    }

    /// SJSON type
    public var type: Type { get { return _type } }

    /// Error in SJSON
    public var error: NSError? { get { return self._error } }

    /// The static null SJSON
    @available(*, unavailable, renamed: "null")
    public static var nullJSON: SJSON { get { return null } }
    public static var null: SJSON { get { return SJSON(NSNull()) } }
}

// MARK: - CollectionType, SequenceType, Indexable
extension SJSON : Swift.CollectionType, Swift.SequenceType, Swift.Indexable {

    public typealias Generator = SJSONGenerator

    public typealias Index = SJSONIndex

    public var startIndex: SJSON.Index {
        switch self.type {
        case .Array:
            return SJSONIndex(arrayIndex: self.rawArray.startIndex)
        case .Dictionary:
            return SJSONIndex(dictionaryIndex: self.rawDictionary.startIndex)
        default:
            return SJSONIndex()
        }
    }

    public var endIndex: SJSON.Index {
        switch self.type {
        case .Array:
            return SJSONIndex(arrayIndex: self.rawArray.endIndex)
        case .Dictionary:
            return SJSONIndex(dictionaryIndex: self.rawDictionary.endIndex)
        default:
            return SJSONIndex()
        }
    }

    public subscript (position: SJSON.Index) -> SJSON.Generator.Element {
        switch self.type {
        case .Array:
            return (String(position.arrayIndex), SJSON(self.rawArray[position.arrayIndex!]))
        case .Dictionary:
            let (key, value) = self.rawDictionary[position.dictionaryIndex!]
            return (key, SJSON(value))
        default:
            return ("", SJSON.null)
        }
    }

    /// If `type` is `.Array` or `.Dictionary`, return `array.isEmpty` or `dictonary.isEmpty` otherwise return `true`.
    public var isEmpty: Bool {
        get {
            switch self.type {
            case .Array:
                return self.rawArray.isEmpty
            case .Dictionary:
                return self.rawDictionary.isEmpty
            default:
                return true
            }
        }
    }

    /// If `type` is `.Array` or `.Dictionary`, return `array.count` or `dictonary.count` otherwise return `0`.
    public var count: Int {
        switch self.type {
        case .Array:
            return self.rawArray.count
        case .Dictionary:
            return self.rawDictionary.count
        default:
            return 0
        }
    }

    public func underestimateCount() -> Int {
        switch self.type {
        case .Array:
            return self.rawArray.underestimateCount()
        case .Dictionary:
            return self.rawDictionary.underestimateCount()
        default:
            return 0
        }
    }

    /**
    If `type` is `.Array` or `.Dictionary`, return a generator over the elements like `Array` or `Dictionary`, otherwise return a generator over empty.

    - returns: Return a *generator* over the elements of SJSON.
    */
    public func generate() -> SJSON.Generator {
        return SJSON.Generator(self)
    }
}

public struct SJSONIndex: Comparable, _Incrementable, Equatable, Comparable {

    let arrayIndex: Int?
    let dictionaryIndex: DictionaryIndex<String, AnyObject>?

    let type: Type

    init(){
        self.arrayIndex = nil
        self.dictionaryIndex = nil
        self.type = .Unknown
    }

    init(arrayIndex: Int) {
        self.arrayIndex = arrayIndex
        self.dictionaryIndex = nil
        self.type = .Array
    }

    init(dictionaryIndex: DictionaryIndex<String, AnyObject>) {
        self.arrayIndex = nil
        self.dictionaryIndex = dictionaryIndex
        self.type = .Dictionary
    }

    public func successor() -> SJSONIndex {
        switch self.type {
        case .Array:
            return SJSONIndex(arrayIndex: self.arrayIndex!.successor())
        case .Dictionary:
            return SJSONIndex(dictionaryIndex: self.dictionaryIndex!.successor())
        default:
            return SJSONIndex()
        }
    }
}

public func ==(lhs: SJSONIndex, rhs: SJSONIndex) -> Bool {
    switch (lhs.type, rhs.type) {
    case (.Array, .Array):
        return lhs.arrayIndex == rhs.arrayIndex
    case (.Dictionary, .Dictionary):
        return lhs.dictionaryIndex == rhs.dictionaryIndex
    default:
        return false
    }
}

public func <(lhs: SJSONIndex, rhs: SJSONIndex) -> Bool {
    switch (lhs.type, rhs.type) {
    case (.Array, .Array):
        return lhs.arrayIndex < rhs.arrayIndex
    case (.Dictionary, .Dictionary):
        return lhs.dictionaryIndex < rhs.dictionaryIndex
    default:
        return false
    }
}

public func <=(lhs: SJSONIndex, rhs: SJSONIndex) -> Bool {
    switch (lhs.type, rhs.type) {
    case (.Array, .Array):
        return lhs.arrayIndex <= rhs.arrayIndex
    case (.Dictionary, .Dictionary):
        return lhs.dictionaryIndex <= rhs.dictionaryIndex
    default:
        return false
    }
}

public func >=(lhs: SJSONIndex, rhs: SJSONIndex) -> Bool {
    switch (lhs.type, rhs.type) {
    case (.Array, .Array):
        return lhs.arrayIndex >= rhs.arrayIndex
    case (.Dictionary, .Dictionary):
        return lhs.dictionaryIndex >= rhs.dictionaryIndex
    default:
        return false
    }
}

public func >(lhs: SJSONIndex, rhs: SJSONIndex) -> Bool {
    switch (lhs.type, rhs.type) {
    case (.Array, .Array):
        return lhs.arrayIndex > rhs.arrayIndex
    case (.Dictionary, .Dictionary):
        return lhs.dictionaryIndex > rhs.dictionaryIndex
    default:
        return false
    }
}

public struct SJSONGenerator : GeneratorType {

    public typealias Element = (String, SJSON)

    private let type: Type
    private var dictionayGenerate: DictionaryGenerator<String, AnyObject>?
    private var arrayGenerate: IndexingGenerator<[AnyObject]>?
    private var arrayIndex: Int = 0

    init(_ sjson: SJSON) {
        self.type = sjson.type
        if type == .Array {
            self.arrayGenerate = sjson.rawArray.generate()
        }else {
            self.dictionayGenerate = sjson.rawDictionary.generate()
        }
    }

    public mutating func next() -> SJSONGenerator.Element? {
        switch self.type {
        case .Array:
            if let o = self.arrayGenerate?.next() {
                let i = self.arrayIndex
                self.arrayIndex += 1
                return (String(i), SJSON(o))
            } else {
                return nil
            }
        case .Dictionary:
            if let (k, v): (String, AnyObject) = self.dictionayGenerate?.next() {
                return (k, SJSON(v))
            } else {
                return nil
            }
        default:
            return nil
        }
    }
}

// MARK: - Subscript

/**
*  To mark both String and Int can be used in subscript.
*/
public enum SJSONKey {
    case Index(Int)
    case Key(String)
}

public protocol SJSONSubscriptType {
    var jsonKey:SJSONKey { get }
}

extension Int: SJSONSubscriptType {
    public var jsonKey:SJSONKey {
        return SJSONKey.Index(self)
    }
}

extension String: SJSONSubscriptType {
    public var jsonKey:SJSONKey {
        return SJSONKey.Key(self)
    }
}

extension SJSON {

    /// If `type` is `.Array`, return SJSON whose object is `array[index]`, otherwise return null SJSON with error.
    private subscript(index index: Int) -> SJSON {
        get {
            if self.type != .Array {
                var r = SJSON.null
                r._error = self._error ?? NSError(domain: ErrorDomain, code: ErrorWrongType, userInfo: [NSLocalizedDescriptionKey: "Array[\(index)] failure, It is not an array"])
                return r
            } else if index >= 0 && index < self.rawArray.count {
                return SJSON(self.rawArray[index])
            } else {
                var r = SJSON.null
                r._error = NSError(domain: ErrorDomain, code:ErrorIndexOutOfBounds , userInfo: [NSLocalizedDescriptionKey: "Array[\(index)] is out of bounds"])
                return r
            }
        }
        set {
            if self.type == .Array {
                if self.rawArray.count > index && newValue.error == nil {
                    self.rawArray[index] = newValue.object
                }
            }
        }
    }

    /// If `type` is `.Dictionary`, return SJSON whose object is `dictionary[key]` , otherwise return null SJSON with error.
    private subscript(key key: String) -> SJSON {
        get {
            var r = SJSON.null
            if self.type == .Dictionary {
                if let o = self.rawDictionary[key] {
                    r = SJSON(o)
                } else {
                    r._error = NSError(domain: ErrorDomain, code: ErrorNotExist, userInfo: [NSLocalizedDescriptionKey: "Dictionary[\"\(key)\"] does not exist"])
                }
            } else {
                r._error = self._error ?? NSError(domain: ErrorDomain, code: ErrorWrongType, userInfo: [NSLocalizedDescriptionKey: "Dictionary[\"\(key)\"] failure, It is not an dictionary"])
            }
            return r
        }
        set {
            if self.type == .Dictionary && newValue.error == nil {
                self.rawDictionary[key] = newValue.object
            }
        }
    }

    /// If `sub` is `Int`, return `subscript(index:)`; If `sub` is `String`,  return `subscript(key:)`.
    private subscript(sub sub: SJSONSubscriptType) -> SJSON {
        get {
            switch sub.jsonKey {
            case .Index(let index): return self[index: index]
            case .Key(let key): return self[key: key]
            }
        }
        set {
            switch sub.jsonKey {
            case .Index(let index): self[index: index] = newValue
            case .Key(let key): self[key: key] = newValue
            }
        }
    }

    /**
    Find a SJSON in the complex data structuresby using the Int/String's array.

    - parameter path: The target SJSON's path. Example:

    let SJSON = SJSON[data]
    let path = [9,"list","person","name"]
    let name = SJSON[path]

    The same as: let name = SJSON[9]["list"]["person"]["name"]

    - returns: Return a SJSON found by the path or a null SJSON with error
    */
    public subscript(path: [SJSONSubscriptType]) -> SJSON {
        get {
            return path.reduce(self) { $0[sub: $1] }
        }
        set {
            switch path.count {
            case 0:
                return
            case 1:
                self[sub:path[0]].object = newValue.object
            default:
                var aPath = path; aPath.removeAtIndex(0)
                var nextJSON = self[sub: path[0]]
                nextJSON[aPath] = newValue
                self[sub: path[0]] = nextJSON
            }
        }
    }

    /**
    Find a SJSON in the complex data structures by using the Int/String's array.

    - parameter path: The target SJSON's path. Example:

    let name = SJSON[9,"list","person","name"]

    The same as: let name = SJSON[9]["list"]["person"]["name"]

    - returns: Return a SJSON found by the path or a null SJSON with error
    */
    public subscript(path: SJSONSubscriptType...) -> SJSON {
        get {
            return self[path]
        }
        set {
            self[path] = newValue
        }
    }
}

// MARK: - LiteralConvertible

extension SJSON: Swift.StringLiteralConvertible {

    public init(stringLiteral value: StringLiteralType) {
        self.init(value)
    }

    public init(extendedGraphemeClusterLiteral value: StringLiteralType) {
        self.init(value)
    }

    public init(unicodeScalarLiteral value: StringLiteralType) {
        self.init(value)
    }
}

extension SJSON: Swift.IntegerLiteralConvertible {

    public init(integerLiteral value: IntegerLiteralType) {
        self.init(value)
    }
}

extension SJSON: Swift.BooleanLiteralConvertible {

    public init(booleanLiteral value: BooleanLiteralType) {
        self.init(value)
    }
}

extension SJSON: Swift.FloatLiteralConvertible {

    public init(floatLiteral value: FloatLiteralType) {
        self.init(value)
    }
}

extension SJSON: Swift.DictionaryLiteralConvertible {

    public init(dictionaryLiteral elements: (String, AnyObject)...) {
        self.init(elements.reduce([String : AnyObject](minimumCapacity: elements.count)){(dictionary: [String : AnyObject], element:(String, AnyObject)) -> [String : AnyObject] in
            var d = dictionary
            d[element.0] = element.1
            return d
            })
    }
}

extension SJSON: Swift.ArrayLiteralConvertible {

    public init(arrayLiteral elements: AnyObject...) {
        self.init(elements)
    }
}

extension SJSON: Swift.NilLiteralConvertible {

    public init(nilLiteral: ()) {
        self.init(NSNull())
    }
}

// MARK: - Raw

extension SJSON: Swift.RawRepresentable {

    public init?(rawValue: AnyObject) {
        if SJSON(rawValue).type == .Unknown {
            return nil
        } else {
            self.init(rawValue)
        }
    }

    public var rawValue: AnyObject {
        return self.object
    }

    public func rawData(options opt: NSJSONWritingOptions = NSJSONWritingOptions(rawValue: 0)) throws -> NSData {
        guard NSJSONSerialization.isValidJSONObject(self.object) else {
            throw NSError(domain: ErrorDomain, code: ErrorInvalidJSON, userInfo: [NSLocalizedDescriptionKey: "JSON is invalid"])
        }

        return try NSJSONSerialization.dataWithJSONObject(self.object, options: opt)
    }

    public func rawString(encoding: UInt = NSUTF8StringEncoding, options opt: NSJSONWritingOptions = .PrettyPrinted) -> String? {
        switch self.type {
        case .Array, .Dictionary:
            do {
                let data = try self.rawData(options: opt)
                return NSString(data: data, encoding: encoding) as? String
            } catch _ {
                return nil
            }
        case .String:
            return self.rawString
        case .Number:
            return self.rawNumber.stringValue
        case .Bool:
            return self.rawNumber.boolValue.description
        case .Null:
            return "null"
        default:
            return nil
        }
    }
}

// MARK: - Printable, DebugPrintable

extension SJSON: Swift.Printable, Swift.DebugPrintable {

    public var description: String {
        if let string = self.rawString(options:.PrettyPrinted) {
            return string
        } else {
            return "unknown"
        }
    }

    public var debugDescription: String {
        return description
    }
}

// MARK: - Array

extension SJSON {

    //Optional [JSON]
    public var array: [SJSON]? {
        get {
            if self.type == .Array {
                return self.rawArray.map{ SJSON($0) }
            } else {
                return nil
            }
        }
    }

    //Non-optional [JSON]
    public var arrayValue: [SJSON] {
        get {
            return self.array ?? []
        }
    }

    //Optional [AnyObject]
    public var arrayObject: [AnyObject]? {
        get {
            switch self.type {
            case .Array:
                return self.rawArray
            default:
                return nil
            }
        }
        set {
            if let array = newValue {
                self.object = array
            } else {
                self.object = NSNull()
            }
        }
    }
}

// MARK: - Dictionary

extension SJSON {

    //Optional [String : SJSON]
    public var dictionary: [String : SJSON]? {
        if self.type == .Dictionary {
            return self.rawDictionary.reduce([String : SJSON](minimumCapacity: count)) { (dictionary: [String : SJSON], element: (String, AnyObject)) -> [String : SJSON] in
                var d = dictionary
                d[element.0] = SJSON(element.1)
                return d
            }
        } else {
            return nil
        }
    }

    //Non-optional [String : SJSON]
    public var dictionaryValue: [String : SJSON] {
        return self.dictionary ?? [:]
    }

    //Optional [String : AnyObject]
    public var dictionaryObject: [String : AnyObject]? {
        get {
            switch self.type {
            case .Dictionary:
                return self.rawDictionary
            default:
                return nil
            }
        }
        set {
            if let v = newValue {
                self.object = v
            } else {
                self.object = NSNull()
            }
        }
    }
}

// MARK: - Bool

extension SJSON: Swift.BooleanType {

    //Optional bool
    public var bool: Bool? {
        get {
            switch self.type {
            case .Bool:
                return self.rawNumber.boolValue
            default:
                return nil
            }
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(bool: newValue)
            } else {
                self.object = NSNull()
            }
        }
    }

    //Non-optional bool
    public var boolValue: Bool {
        get {
            switch self.type {
            case .Bool, .Number, .String:
                return self.object.boolValue
            default:
                return false
            }
        }
        set {
            self.object = NSNumber(bool: newValue)
        }
    }
}

// MARK: - String

extension SJSON {

    //Optional string
    public var string: String? {
        get {
            switch self.type {
            case .String:
                return self.object as? String
            default:
                return nil
            }
        }
        set {
            if let newValue = newValue {
                self.object = NSString(string:newValue)
            } else {
                self.object = NSNull()
            }
        }
    }

    //Non-optional string
    public var stringValue: String {
        get {
            switch self.type {
            case .String:
                return self.object as? String ?? ""
            case .Number:
                return self.object.stringValue
            case .Bool:
                return (self.object as? Bool).map { String($0) } ?? ""
            default:
                return ""
            }
        }
        set {
            self.object = NSString(string:newValue)
        }
    }
}

// MARK: - Number
extension SJSON {

    //Optional number
    public var number: NSNumber? {
        get {
            switch self.type {
            case .Number, .Bool:
                return self.rawNumber
            default:
                return nil
            }
        }
        set {
            self.object = newValue ?? NSNull()
        }
    }

    //Non-optional number
    public var numberValue: NSNumber {
        get {
            switch self.type {
            case .String:
                let decimal = NSDecimalNumber(string: self.object as? String)
                if decimal == NSDecimalNumber.notANumber() {  // indicates parse error
                    return NSDecimalNumber.zero()
                }
                return decimal
            case .Number, .Bool:
                return self.object as? NSNumber ?? NSNumber(int: 0)
            default:
                return NSNumber(double: 0.0)
            }
        }
        set {
            self.object = newValue
        }
    }
}

//MARK: - Null
extension SJSON {

    public var null: NSNull? {
        get {
            switch self.type {
            case .Null:
                return self.rawNull
            default:
                return nil
            }
        }
        set {
            self.object = NSNull()
        }
    }
    public func exists() -> Bool{
        if let errorValue = error , errorValue.code == ErrorNotExist{
            return false
        }
        return true
    }
}

//MARK: - URL
extension SJSON {

    //Optional URL
    public var URL: NSURL? {
        get {
            switch self.type {
            case .String:
                if let encodedString_ = self.rawString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
                    return NSURL(string: encodedString_)
                } else {
                    return nil
                }
            default:
                return nil
            }
        }
        set {
            self.object = newValue?.absoluteString ?? NSNull()
        }
    }
}

// MARK: - Int, Double, Float, Int8, Int16, Int32, Int64

extension SJSON {

    public var double: Double? {
        get {
            return self.number?.doubleValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(double: newValue)
            } else {
                self.object = NSNull()
            }
        }
    }

    public var doubleValue: Double {
        get {
            return self.numberValue.doubleValue
        }
        set {
            self.object = NSNumber(double: newValue)
        }
    }

    public var float: Float? {
        get {
            return self.number?.floatValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(float: newValue)
            } else {
                self.object = NSNull()
            }
        }
    }

    public var floatValue: Float {
        get {
            return self.numberValue.floatValue
        }
        set {
            self.object = NSNumber(float: newValue)
        }
    }

    public var int: Int? {
        get {
            return self.number?.longValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(integer: newValue)
            } else {
                self.object = NSNull()
            }
        }
    }

    public var intValue: Int {
        get {
            return self.numberValue.integerValue
        }
        set {
            self.object = NSNumber(integer: newValue)
        }
    }

    public var uInt: UInt? {
        get {
            return self.number?.unsignedLongValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(unsignedLong: newValue)
            } else {
                self.object = NSNull()
            }
        }
    }

    public var uIntValue: UInt {
        get {
            return self.numberValue.unsignedLongValue
        }
        set {
            self.object = NSNumber(unsignedLong: newValue)
        }
    }

    public var int8: Int8? {
        get {
            return self.number?.charValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(char: newValue)
            } else {
                self.object =  NSNull()
            }
        }
    }

    public var int8Value: Int8 {
        get {
            return self.numberValue.charValue
        }
        set {
            self.object = NSNumber(char: newValue)
        }
    }

    public var uInt8: UInt8? {
        get {
            return self.number?.unsignedCharValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(unsignedChar: newValue)
            } else {
                self.object =  NSNull()
            }
        }
    }

    public var uInt8Value: UInt8 {
        get {
            return self.numberValue.unsignedCharValue
        }
        set {
            self.object = NSNumber(unsignedChar: newValue)
        }
    }

    public var int16: Int16? {
        get {
            return self.number?.shortValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(short: newValue)
            } else {
                self.object =  NSNull()
            }
        }
    }

    public var int16Value: Int16 {
        get {
            return self.numberValue.shortValue
        }
        set {
            self.object = NSNumber(short: newValue)
        }
    }

    public var uInt16: UInt16? {
        get {
            return self.number?.unsignedShortValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(unsignedShort: newValue)
            } else {
                self.object =  NSNull()
            }
        }
    }

    public var uInt16Value: UInt16 {
        get {
            return self.numberValue.unsignedShortValue
        }
        set {
            self.object = NSNumber(unsignedShort: newValue)
        }
    }

    public var int32: Int32? {
        get {
            return self.number?.intValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(int: newValue)
            } else {
                self.object =  NSNull()
            }
        }
    }

    public var int32Value: Int32 {
        get {
            return self.numberValue.intValue
        }
        set {
            self.object = NSNumber(int: newValue)
        }
    }

    public var uInt32: UInt32? {
        get {
            return self.number?.unsignedIntValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(unsignedInt: newValue)
            } else {
                self.object =  NSNull()
            }
        }
    }

    public var uInt32Value: UInt32 {
        get {
            return self.numberValue.unsignedIntValue
        }
        set {
            self.object = NSNumber(unsignedInt: newValue)
        }
    }

    public var int64: Int64? {
        get {
            return self.number?.longLongValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(longLong: newValue)
            } else {
                self.object =  NSNull()
            }
        }
    }

    public var int64Value: Int64 {
        get {
            return self.numberValue.longLongValue
        }
        set {
            self.object = NSNumber(longLong: newValue)
        }
    }

    public var uInt64: UInt64? {
        get {
            return self.number?.unsignedLongLongValue
        }
        set {
            if let newValue = newValue {
                self.object = NSNumber(unsignedLongLong: newValue)
            } else {
                self.object =  NSNull()
            }
        }
    }

    public var uInt64Value: UInt64 {
        get {
            return self.numberValue.unsignedLongLongValue
        }
        set {
            self.object = NSNumber(unsignedLongLong: newValue)
        }
    }
}

//MARK: - Comparable
extension SJSON : Swift.Comparable {}

public func ==(lhs: SJSON, rhs: SJSON) -> Bool {

    switch (lhs.type, rhs.type) {
    case (.Number, .Number):
        return lhs.rawNumber == rhs.rawNumber
    case (.String, .String):
        return lhs.rawString == rhs.rawString
    case (.Bool, .Bool):
        return lhs.rawNumber.boolValue == rhs.rawNumber.boolValue
    case (.Array, .Array):
        return lhs.rawArray as NSArray == rhs.rawArray as NSArray
    case (.Dictionary, .Dictionary):
        return lhs.rawDictionary as NSDictionary == rhs.rawDictionary as NSDictionary
    case (.Null, .Null):
        return true
    default:
        return false
    }
}

public func <=(lhs: SJSON, rhs: SJSON) -> Bool {

    switch (lhs.type, rhs.type) {
    case (.Number, .Number):
        return lhs.rawNumber <= rhs.rawNumber
    case (.String, .String):
        return lhs.rawString <= rhs.rawString
    case (.Bool, .Bool):
        return lhs.rawNumber.boolValue == rhs.rawNumber.boolValue
    case (.Array, .Array):
        return lhs.rawArray as NSArray == rhs.rawArray as NSArray
    case (.Dictionary, .Dictionary):
        return lhs.rawDictionary as NSDictionary == rhs.rawDictionary as NSDictionary
    case (.Null, .Null):
        return true
    default:
        return false
    }
}

public func >=(lhs: SJSON, rhs: SJSON) -> Bool {

    switch (lhs.type, rhs.type) {
    case (.Number, .Number):
        return lhs.rawNumber >= rhs.rawNumber
    case (.String, .String):
        return lhs.rawString >= rhs.rawString
    case (.Bool, .Bool):
        return lhs.rawNumber.boolValue == rhs.rawNumber.boolValue
    case (.Array, .Array):
        return lhs.rawArray as NSArray == rhs.rawArray as NSArray
    case (.Dictionary, .Dictionary):
        return lhs.rawDictionary as NSDictionary == rhs.rawDictionary as NSDictionary
    case (.Null, .Null):
        return true
    default:
        return false
    }
}

public func >(lhs: SJSON, rhs: SJSON) -> Bool {

    switch (lhs.type, rhs.type) {
    case (.Number, .Number):
        return lhs.rawNumber > rhs.rawNumber
    case (.String, .String):
        return lhs.rawString > rhs.rawString
    default:
        return false
    }
}

public func <(lhs: SJSON, rhs: SJSON) -> Bool {

    switch (lhs.type, rhs.type) {
    case (.Number, .Number):
        return lhs.rawNumber < rhs.rawNumber
    case (.String, .String):
        return lhs.rawString < rhs.rawString
    default:
        return false
    }
}

private let trueNumber = NSNumber(bool: true)
private let falseNumber = NSNumber(bool: false)
private let trueObjCType = String.fromCString(trueNumber.objCType)
private let falseObjCType = String.fromCString(falseNumber.objCType)

// MARK: - NSNumber: Comparable

extension NSNumber {
    var isBool:Bool {
        get {
            let objCType = String.fromCString(self.objCType)
            if (self.compare(trueNumber) == NSComparisonResult.OrderedSame && objCType == trueObjCType)
                || (self.compare(falseNumber) == NSComparisonResult.OrderedSame && objCType == falseObjCType){
                    return true
            } else {
                return false
            }
        }
    }
}

func ==(lhs: NSNumber, rhs: NSNumber) -> Bool {
    switch (lhs.isBool, rhs.isBool) {
    case (false, true):
        return false
    case (true, false):
        return false
    default:
        return lhs.compare(rhs) == NSComparisonResult.OrderedSame
    }
}

func !=(lhs: NSNumber, rhs: NSNumber) -> Bool {
    return !(lhs == rhs)
}

func <(lhs: NSNumber, rhs: NSNumber) -> Bool {

    switch (lhs.isBool, rhs.isBool) {
    case (false, true):
        return false
    case (true, false):
        return false
    default:
        return lhs.compare(rhs) == NSComparisonResult.OrderedAscending
    }
}

func >(lhs: NSNumber, rhs: NSNumber) -> Bool {

    switch (lhs.isBool, rhs.isBool) {
    case (false, true):
        return false
    case (true, false):
        return false
    default:
        return lhs.compare(rhs) == NSComparisonResult.OrderedDescending
    }
}

func <=(lhs: NSNumber, rhs: NSNumber) -> Bool {

    switch (lhs.isBool, rhs.isBool) {
    case (false, true):
        return false
    case (true, false):
        return false
    default:
        return lhs.compare(rhs) != NSComparisonResult.OrderedDescending
    }
}

func >=(lhs: NSNumber, rhs: NSNumber) -> Bool {

    switch (lhs.isBool, rhs.isBool) {
    case (false, true):
        return false
    case (true, false):
        return false
    default:
        return lhs.compare(rhs) != NSComparisonResult.OrderedAscending
    }
}
