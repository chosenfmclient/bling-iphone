//
//  SIAShadowTableCell.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 10/28/15.
//  Copyright © 2015 SingrFM. All rights reserved.
//

#import "SIAShadowTableCell.h"
#define LINE_VIEW_TAG (100)

@implementation SIAShadowTableCell

- (void)awakeFromNib {
    //[self setShadow];
}

- (void) roundCorners:(NSInteger)corners {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        CAShapeLayer *roundTopLayer = [CAShapeLayer layer];
        
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.contentView.bounds
                                                   byRoundingCorners:corners
                                                         cornerRadii:CGSizeMake(6.0, 6.0)];
        
        roundTopLayer.frame = self.contentView.bounds;
        roundTopLayer.path = path.CGPath;
        
        self.layer.mask = roundTopLayer;
        self.layer.masksToBounds = YES;
    });
}

- (void) setSeparator {
    if (![self viewWithTag:LINE_VIEW_TAG]) {
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                    self.frame.size.height - 1,
                                                                    self.frame.size.width,
                                                                    1)];
        lineView.backgroundColor = [UIColor colorWithWhite:0.88
                                                     alpha:1.0];
        lineView.tag = LINE_VIEW_TAG;
        [self addSubview:lineView];
    }
}

- (void) removeSeparator {
    [[self viewWithTag:LINE_VIEW_TAG] removeFromSuperview];
}

//- (void) setShadow {
//    CAShapeLayer *shadowLayer = [CAShapeLayer layer];
//    CGRect shadowFrame = CGRectMake(0, -1, self.contentView.bounds.size.width, self.contentView.bounds.size.height + 2);
//    shadowLayer.frame = shadowFrame;
//    shadowLayer.shadowOffset = CGSizeMake(0,0);
//    shadowLayer.shadowColor = [[UIColor blackColor] CGColor];
//    shadowLayer.shadowRadius = 1.8;
//    shadowLayer.shadowOpacity = 0.4f;
//    
//    //shadowLayer.shadowPath = [[self shadowPath] CGPath];
//    shadowLayer.shadowPath = [[UIBezierPath bezierPathWithRect:shadowFrame] CGPath];
//    
//    //UIView *mask = [[UIView alloc] initWithFrame:CGRectMake(-3, 0, self.contentView.frame.size.width + 6, self.contentView.frame.size.height)];
//    CAShapeLayer *maskLayer = [CAShapeLayer layer];
//    maskLayer.path = [[UIBezierPath bezierPathWithRect:CGRectMake(-3, 0, shadowFrame.size.width + 6, shadowFrame.size.height)] CGPath];
//    //mask.backgroundColor = [UIColor blackColor];
//    shadowLayer.mask = maskLayer;
//    
//    UIView *shadowView = [[UIView alloc] initWithFrame:shadowFrame];
//    [shadowView.layer addSublayer:shadowLayer];
//    //shadowView.layer.mask = mask.layer;
//    shadowView.clipsToBounds = NO;
//    self.clipsToBounds = NO;
//    
//    shadowView.tag = SHADOW_VIEW_TAG;
//    
//    if ([self viewWithTag:SHADOW_VIEW_TAG]) {
//        [[self viewWithTag:SHADOW_VIEW_TAG] removeFromSuperview];
//    }
//    
//    [self addSubview:shadowView];
//    [self sendSubviewToBack:shadowView];
//}
//
//
//- (UIBezierPath *)shadowPath {
//    UIBezierPath *path = [UIBezierPath bezierPath];
//    
//    CGFloat width = self.contentView.frame.size.width;
//    CGFloat height = self.contentView.frame.size.height;
//    
//    // make a path that only includes shadows on left and right side, not on top and bottom
//    // start at origin
//    [path moveToPoint:CGPointZero];
//    
//    // remove shadow from top
//    
//    
//    [path addLineToPoint:CGPointMake(5, 0)];
//    
//    [path addLineToPoint:CGPointMake(5, 10)];
//    
//    [path addLineToPoint:CGPointMake(width - 5, 10)];
//    
//    [path addLineToPoint:CGPointMake(width - 5, 0)];
//    
//
//    //move to top right corner
//    [path addLineToPoint:CGPointMake(width, 0)];
//    //move to bottom right corner
//    [path addLineToPoint:CGPointMake(width, height)];
//    
//    [path addLineToPoint:CGPointMake(width - 5, height)];
//    
//    [path addLineToPoint:CGPointMake(width - 5, height - 10)];
//    
//    [path addLineToPoint:CGPointMake(5, height - 10)];
//    
//    [path addLineToPoint:CGPointMake(5, height)];
//
//    //bottom left corner
//    [path addLineToPoint:CGPointMake(0, height)];
//    //finish it up!!!!
//    [path closePath];
//    
//    return path;
//}


@end
