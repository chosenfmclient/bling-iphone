//
//  VideoPlayerManagerPlayerItem.m
//  BlingIphoneApp
//
//  Created by Michael Biehl on 11/1/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import "VideoPlayerManagerPlayerItem.h"

@implementation VideoPlayerManagerPlayerItem

- (id)initWithAsset:(AVAsset *)asset automaticallyLoadedAssetKeys:(NSArray<NSString *> *)automaticallyLoadedAssetKeys {
    if (self = [super initWithAsset:asset
       automaticallyLoadedAssetKeys:automaticallyLoadedAssetKeys]) {
        
    }
    return self;
}

- (void)dealloc {
    @try {
        [self removeObserver:[VideoPlayerManager shared]
                  forKeyPath:@"status"];
    } @catch (NSException *exception) {
        NSLog(@"Tried to remove observer from %@ while no observers were registered", self.description);
    }
}

@end
