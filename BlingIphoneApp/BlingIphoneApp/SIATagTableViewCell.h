//
//  SIATagTableViewCell.h
//  
//
//  Created by Michael Biehl on 2/10/16.
//
//

#import <UIKit/UIKit.h>

@interface SIATagTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *tagField;

@end
