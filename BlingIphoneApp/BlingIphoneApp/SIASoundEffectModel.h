//
//  SIASoundEffectModel.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 5/5/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SIASoundEffect : NSObject

@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *name;

+ (RKObjectMapping *)objectMapping;

@end


@interface SIASoundEffectModel : NSObject

@property (strong, nonatomic) NSArray <SIASoundEffect *> *effectsArray;
@property (strong, nonatomic) NSMutableArray <SIASoundEffect *> *filteredArray;

+ (void)getSoundEffectsWithCompletion:(void(^)(SIASoundEffectModel *model))handler;
+ (RKObjectMapping *)objectMapping;

@end

