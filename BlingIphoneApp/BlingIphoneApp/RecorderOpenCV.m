//
//  RecorderOpenCV.m
//  BlingIphoneApp
//
//  Created by Joseph Nahmias on 25/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import "RecorderOpenCV.h"

#import <opencv2/opencv.hpp>
#import <opencv2/objdetect/objdetect_c.h>
#import <opencv2/imgproc/imgproc.hpp>
#import <opencv2/highgui/highgui.hpp>
#import "BIAVideoToMatExploder.h"
#import <Foundation/Foundation.h>
#import "edgeDetector.h"
#import "chromaKey.h"
#import "imageUtil.h"
#import "faceDetector.h"
#import <sys/sysctl.h>
#import <Appsee/Appsee.h>

#define ADD_SATURATION 20

NSString* const faceCascadeFilename = @"lbpcascade_frontalface";
NSString* const eyeCascadeFilename = @"haarcascade_eye_tree_eyeglasses";
//NSString* const faceCascadeFilename = @"haarcascade_frontalface_alt2";
NSString* const upperBodyCascade = @"haarcascade_mcs_upperbody";
NSString* const hand = @"hand";

edgeDetector eUtil;
//chromaKey cUtil;
//imageUtil iUtil;

NSString* faceCascadePath = [[NSBundle mainBundle] pathForResource:faceCascadeFilename ofType:@"xml"];
NSString* eyeCascadePath = [[NSBundle mainBundle] pathForResource:eyeCascadeFilename ofType:@"xml"];
FaceDetector faceDetector([faceCascadePath UTF8String],
                          [eyeCascadePath UTF8String]);

Mat maskMinus5, maskMinus4, maskMinus3, maskMinus2, maskMinus1, maskCurrent;

static int waitFrames = 9;
static int frameCounter = 0;
const static int framesToDetect = 15;
static int whenToDetect = framesToDetect;

static int whitePointAverage = 0;

bool startedRecording = false;
bool frontCamera;
bool oldDevice;

DetectionResponse detectionResponse = DetectionResponseFailure;

typedef struct threadingInfo_ {
    Mat& bg;
    Mat dss;
    Mat mask;
    int faceTop;
    int faceBottom;
    int faceLeft;
    int faceRight;
    cv::Rect face;
    int brigtnessFix;
} threadingInfo;

typedef struct backgroundFrameInfo_ {
    RecorderOpenCV* recorder;
    Mat* backgroundImg;
    CMTime time;
} backgroundFrameInfo;

//const int HaarOptions = CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_DO_ROUGH_SEARCH;

@interface RecorderOpenCV () <AVCaptureVideoDataOutputSampleBufferDelegate, BIACvVideoCameraDelegate>

{
    Mat maskImage;
    Mat background;
    double backgroundAspectRatio;
    VideoCapture cap;
    CMTime currentTime;
    
    std::vector<cv::Rect> lastFaceAndEyes;
    dispatch_queue_t detectionQueue;
    dispatch_queue_t chromaKeyQueue;
    dispatch_queue_t checkingQueue;
    BOOL detectionQueueIsReady;
    int failsCounter;
}

@property (strong, nonatomic) UIView *rectView;
@property (strong, nonatomic) NSURL *backgroundVideo;
@property (strong, nonatomic) BIAVideoToMatExploder *exploder;
@property (nonatomic) NSUInteger detectionAttemptCount;
@property (nonatomic) BOOL shouldDetect;
@property (nonatomic) BOOL detectDidSucceed;
@property (nonatomic) BOOL cancelSuccessMessage;
@property (nonatomic) BOOL didDetect;
@property (nonatomic) BOOL isWaitingForCameraExposure;
@property (strong, nonatomic) NSOperationQueue *operationQueue;


@end

@implementation RecorderOpenCV

@synthesize videoCamera;

const int edgeThresh = 1, lowThreshold = 20, max_lowThreshold = 100;
const int ratio = 3, kernel_size = 3;

- (void)startDetection {
    waitFrames = 50;
    __weak RecorderOpenCV *wSelf = self;
    NSLog(@"start exposure adjustment for detection");
    self.isWaitingForCameraExposure = YES;
    [self.videoCamera adjustExposureWithCompletion:^{
        NSLog(@"exposure adjustment completed ::: starting detection!");
        wSelf.isWaitingForCameraExposure = NO;
        wSelf.shouldDetect = YES;
        wSelf.didDetect = YES;
    }];
    self.cancelSuccessMessage = NO;
    self.detectionAttemptCount++;
    failsCounter = 0;
    faceDetector.lastFace = cv::Rect(0,0,1,1);
   // [self.videoCamera unlockCurrentDeviceAutoAdjustment];
    [self startDetectionFailureTimer];
}

- (BOOL)isDetecting {
    return self.shouldDetect;
}

- (instancetype)initWithView:(UIView *)view
{
    self = [super init];
    if (self) {
        
        self.usingOldDevice = [self areUsingOldDevice];
        oldDevice = self.usingOldDevice ? true : false;
        if (self.usingOldDevice) {
            iUtil.setImageWidthHeight(240, 320, 1);
        }
        recorderView = view;
        self.videoCamera = [[BIAPausableCVCamera alloc] initWithParentView:recorderView];
        self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
        self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
        self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
        self.videoCamera.captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [self.videoCamera adjustLayoutToInterfaceOrientation:UIInterfaceOrientationPortrait];
        [self.videoCamera layoutPreviewLayer];
        self.videoCamera.defaultFPS = 30;
        self.videoCamera.grayscaleMode = NO;
        self.videoCamera.cameraDelegate = self;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(willGoToBackground:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didGoToForeground:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(sceneDidChange:)
//                                                     name:AVCaptureDeviceSubjectAreaDidChangeNotification
//                                                   object:nil];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.videoCamera unlockCurrentDeviceAutoAdjustment];
        });

        
        detectionQueue = dispatch_queue_create("gy.blin.detectionQueue", DISPATCH_QUEUE_SERIAL);
        detectionQueueIsReady = YES;
        chromaKeyQueue = dispatch_queue_create("gy.blin.chomaKeyQueue", DISPATCH_QUEUE_CONCURRENT);
        checkingQueue = dispatch_queue_create("gy.blin.checkingQueue", DISPATCH_QUEUE_CONCURRENT);
        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            AVCaptureDevice *device =
//            [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
//            AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint = {
//                .temperature = 3500,
//                .tint = 40,
//            };
//            NSLog(@"***3500");
//            
//            AVCaptureWhiteBalanceGains wbGains = [device deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint];
//            
//            
//            [device lockForConfiguration:nil];
//            
//            printf("\nr: %f, g: %f, b: %f, max: %f", wbGains.redGain, wbGains.greenGain, wbGains.blueGain, self.videoCamera.captureDevice.maxWhiteBalanceGain);
//            
//            [device deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint];
//            
//            [device setWhiteBalanceModeLockedWithDeviceWhiteBalanceGains:[device deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint] completionHandler:^(CMTime syncTime) {
//                ;
//            }];
//            
//            [device unlockForConfiguration];
//        });
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            AVCaptureDevice *device =
//            [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
//            AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint = {
//                .temperature = 8000,
//                .tint = 80,
//            };
//            NSLog(@"***8000");
//            
//            AVCaptureWhiteBalanceGains wbGains = [device deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint];
//            
//            
//            [device lockForConfiguration:nil];
//            
//            printf("\nr: %f, g: %f, b: %f, max: %f", wbGains.redGain, wbGains.greenGain, wbGains.blueGain, self.videoCamera.captureDevice.maxWhiteBalanceGain);
//            
//            [device deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint];
//            
//            [device setWhiteBalanceModeLockedWithDeviceWhiteBalanceGains:[device deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint] completionHandler:^(CMTime syncTime) {
//                ;
//            }];
//            
//            [device unlockForConfiguration];
//        });
        
        //[self.videoCamera start];
        // }];
    }
    
    return self;
}

-(void)sceneDidChange:(NSNotification *)notification {
//    NSLog(@"BIG OL' SCENE CHANGE!!!");
}

-(void)willGoToBackground:(NSNotification *)notification {
    
   // [self.videoCamera.captureSession stopRunning];
    
}

-(void)didGoToForeground:(NSNotification *)notification {
    [self.videoCamera.captureSession startRunning];
}

- (void)setPlayer:(BIAVideoPlayer *)player {
    self.videoCamera.player = player;
    AVAsset *asset = player.currentItem.asset;
    if (asset) { // make sure the asset exists and has loaded tracks before initializing the AVAssetReader in BIAVideoToMatExploder
        __weak RecorderOpenCV *wSelf = self;
        [asset loadValuesAsynchronouslyForKeys:@[@"tracks"] completionHandler:^{
            NSError *error;
            if ([asset statusOfValueForKey:@"tracks" error:&error] == AVKeyValueStatusLoaded) {
                wSelf.exploder = [[BIAVideoToMatExploder alloc] initWithAsset:asset];
                if (wSelf.exploder.error) {
                    [wSelf.detectionDelegate recorderDidFailToReadBackgroundVideo:wSelf.exploder.error];
                    return;
                }
                background = [wSelf.exploder getFrameAtTime:kCMTimeZero];
                backgroundAspectRatio = (float)background.cols / (float)background.rows;
                currentTime = kCMTimeZero;
            }
            else {
                [wSelf.detectionDelegate recorderDidFailToReadBackgroundVideo:error];
            }
        }];
    }
}

-(BOOL)areUsingOldDevice{
        
        size_t size;
        sysctlbyname("hw.machine", NULL, &size, NULL, 0);
        char *machine = (char *)malloc(size);
        sysctlbyname("hw.machine", machine, &size, NULL, 0);
        NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
        free(machine);
        
    if ([platform isEqualToString:@"iPhone1,1"] ||    //return @"iPhone 1G";
        [platform isEqualToString:@"iPhone1,2"] ||    //return @"iPhone 3G";
        [platform isEqualToString:@"iPhone2,1"] ||    //return @"iPhone 3GS";
        [platform isEqualToString:@"iPhone3,1"] ||    //return @"iPhone 4";
        [platform isEqualToString:@"iPhone3,2"] ||    //return @"iPhone 4";
        [platform isEqualToString:@"iPhone3,3"] ||    //return @"Verizon iPhone 4";
        [platform isEqualToString:@"iPhone4,1"] ||    //return @"iPhone 4S";
        [platform isEqualToString:@"iPhone5,1"] ||    //return @"iPhone 5 (GSM)";
        [platform isEqualToString:@"iPhone5,2"] ||    //return @"iPhone 5 (GSM+CDMA)";
        [platform isEqualToString:@"iPhone5,3"] ||    //return @"iPhone 5c (GSM)";
        [platform isEqualToString:@"iPhone5,4"] ||    //return @"iPhone 5c (GSM+CDMA)";
        [platform isEqualToString:@"iPhone6,1"] ||    //return @"iPhone 5s (GSM)";
        [platform isEqualToString:@"iPhone6,2"] ||    //return @"iPhone 5s (GSM+CDMA)";
        [platform isEqualToString:@"iPhone7,1"] ||    //return @"iPhone 6 Plus";
        [platform isEqualToString:@"iPhone7,2"] ||    //return @"iPhone 6";
        
        ///IPOD
        
        [platform isEqualToString:@"iPod1,1"] ||     //return @"iPod Touch 1G";
        [platform isEqualToString:@"iPod2,1"] ||     //return @"iPod Touch 2G";
        [platform isEqualToString:@"iPod3,1"] ||     //return @"iPod Touch 3G";
        [platform isEqualToString:@"iPod4,1"] ||     //return @"iPod Touch 4G";
        [platform isEqualToString:@"iPod5,1"] ||     //return @"iPod Touch 5G";
        [platform isEqualToString:@"iPod7,1"] ||     //return @"iPod Touch 6G";
        
        ///IPAD
        
        [platform isEqualToString:@"iPad1,1"] ||     //return @"iPad";
        [platform isEqualToString:@"iPad2,1"] ||     //return @"iPad 2 (WiFi)";
        [platform isEqualToString:@"iPad2,2"] ||     //return @"iPad 2 (GSM)";
        [platform isEqualToString:@"iPad2,3"] ||     //return @"iPad 2 (CDMA)";
        [platform isEqualToString:@"iPad2,4"] ||     //return @"iPad 2 (WiFi)";
        [platform isEqualToString:@"iPad2,5"] ||     //return @"iPad Mini (WiFi)";
        [platform isEqualToString:@"iPad2,6"] ||     //return @"iPad Mini (GSM)";
        [platform isEqualToString:@"iPad2,7"] ||     //return @"iPad Mini (GSM+CDMA)";
        [platform isEqualToString:@"iPad3,1"] ||     //return @"iPad 3 (WiFi)";
        [platform isEqualToString:@"iPad3,2"] ||     //return @"iPad 3 (GSM+CDMA)";
        [platform isEqualToString:@"iPad3,3"] ||     //return @"iPad 3 (GSM)";
        [platform isEqualToString:@"iPad3,4"] ||     //return @"iPad 4 (WiFi)";
        [platform isEqualToString:@"iPad3,5"] ||     //return @"iPad 4 (GSM)";
        [platform isEqualToString:@"iPad3,6"] ||     //return @"iPad 4 (GSM+CDMA)";//
        [platform isEqualToString:@"iPad4,1"] ||     //mini 2
        [platform isEqualToString:@"iPad4,2"] ||     //mini 2
        [platform isEqualToString:@"iPad4,3"] ||     //mini 2
        [platform isEqualToString:@"iPad4,4"] ||     //mini 2
        [platform isEqualToString:@"iPad4,5"] ||
        [platform isEqualToString:@"iPad4,6"] ||
        [platform isEqualToString:@"iPad4,7"] ||    //mini 3
        [platform isEqualToString:@"iPad4,8"] ||
        [platform isEqualToString:@"iPad4,9"] ||
        [platform isEqualToString:@"iPad5,1"] ||
        [platform isEqualToString:@"iPad5,2"])
        
            return YES;
        
        return NO;
    
}


#pragma mark - Protocol CvVideoCameraDelegate

#ifdef __cplusplus

static NSMutableArray *profileTimes = [NSMutableArray array];


BOOL isFaceInPlace(cv::Rect faceRect, cv::Rect faceArea){
    if(faceRect.x >= faceArea.x && faceRect.y >= faceArea.y && faceRect.width <= faceArea.width){
        return YES;
    }
    return NO;
}

-(void)processImage:(Mat&)imageFull atTime:(CMTime)time
{

    frameCounter++;
    
    //printf("\n*exposure duration: %f", self.videoCamera.exposureDuration);
    //printf("\n*ISO: %f", self.videoCamera.iso);
    //printf(" *Bias: %f", self.videoCamera.exposureTargetBias);
    //printf(" *Offset: %f", self.videoCamera.exposureTargetOffset);
    
    NSString *camIsoStr = [NSString stringWithFormat:@"ISO: %f", self.videoCamera.iso];
    NSString *brightnessStr = [NSString stringWithFormat:@"brightness: %f", self.videoCamera.currentFrameBrightness];
    cv::String(isoText) = [camIsoStr UTF8String];
    cv::String(brightnessText) = [brightnessStr UTF8String];
    

//    int brightnessTaget = self.usingFrontCamera ? 120 : 80;
    NSDate *start = [NSDate date];
    int brightnessFix = 0;//brightnessTaget - checkBrightness(imageFull);

    NSDate *startDate = [NSDate date];
    Mat image, imageForDetect;
    
    //bitwise_and(imageFull, Vec4b(0b10101010,0b11111111,0b10101010,255), imageFull);

//    Mat blurred; double sigma = 3, threshold = 3, amount = 4;
//    //GaussianBlur(imageFull, imageFull, cv::Size(3,3), 1);
//    GaussianBlur(imageFull, blurred, cv::Size(), sigma, sigma);
//    Mat lowContrastMask = abs(imageFull - blurred) < threshold;
//    Mat sharpened = imageFull*(1+amount) + blurred*(-amount);
//    imageFull.copyTo(sharpened, lowContrastMask);
//    
//    imageFull = sharpened;
//
    int cameraExpectedHeight = self.usingOldDevice ? 240 : 480,
        cameraExpectedWidth = self.usingOldDevice ? 320 : 640,
        backgroundHeight = cameraExpectedHeight,
        backgroundWidth = self.usingOldDevice ? 320 : 640;
    
    
        Mat resizedImage;
        cv::resize(imageFull, resizedImage, cv::Size(cameraExpectedWidth, imageFull.rows * (float)cameraExpectedWidth/imageFull.cols));//, (double)4/3, (double)3/4);
        cv::Rect croppedImage(0, (resizedImage.rows - cameraExpectedHeight) / 2, cameraExpectedWidth, cameraExpectedHeight);
        image = resizedImage(croppedImage);
    
    cvtColor(image, image, CV_BGRA2RGBA); //incoming pixel buffers are BGRA
    
    if (waitFrames > 0 || (!self.shouldDetect && !self.detectDidSucceed)){
        if (waitFrames == 10) {
//            [self.videoCamera lockFocus];
//            [self.videoCamera lockExposure];
//            [self.videoCamera lockBalance];
            //[self.videoCamera lockCurrentDeviceAutoAdjustment];
            maskMinus5 = maskMinus4 = maskMinus3 = maskMinus2 = maskMinus1 = maskCurrent = Mat::zeros(cv::Size(backgroundHeight, backgroundWidth), CV_8UC1);
        }
        
//        putText(image, isoText, cv::Point(15, image.size().height - 30), FONT_HERSHEY_PLAIN, 1.25, Scalar(255, 0, 255));
//        putText(image, brightnessText, cv::Point(15, image.size().height - 15), FONT_HERSHEY_PLAIN, 1.25, Scalar(255, 0, 255));
        
        
        waitFrames -= waitFrames == 0 ? 0 : 1;//cant be 0?
        imageFull = image;
        Mat argbImage(imageFull.size(), imageFull.type());
        int from_to[] = { 0,1, 1,2, 2,3, 3,0 };
        cv::mixChannels(&imageFull,1,&argbImage,1,from_to,4);
        imageFull = argbImage;
        return;
    }
    
    if (whenToDetect == framesToDetect){
//        if (self.usingOldDevice) {
//            iUtil.setImageWidthHeight(320, 240, 1);
//        } else {
//            iUtil.setImageWidthHeight(640, 480, 1);
//        }
            iUtil.setImageWidthHeight(cameraExpectedHeight, cameraExpectedWidth, 1);
    }
    if (whenToDetect == 0){
//        if (self.usingOldDevice) {
//            iUtil.setImageWidthHeight(240, 320, 1);
//        } else {
//            iUtil.setImageWidthHeight(480, 640, 1);
//        }
        [self detectionSuccess];
    }
    
    std::vector<cv::Rect> faceAndEyes = {cv::Rect(), cv::Rect(), cv::Rect()};
    
//    if (whenToDetect <= framesToDetect && whenToDetect > 0){
//        if (self.usingOldDevice) {
//            resize(imageFull, imageFull, cv::Size(240,320));
//        }
//        face = [self getFaceRect:imageFull];
//        cvtColor(imageFull, imageForDetect, CV_BGRA2RGBA);
//        Mat timage(cv::Size(imageFull.rows, imageFull.cols), imageFull.depth(), imageFull.channels());
//        transpose(imageForDetect, timage);
//        flip(timage, imageForDetect, 1);
//    }
//    
//    else {
    __weak RecorderOpenCV *wSelf = self;
    if (detectionQueueIsReady && whenToDetect < 0)
    {
        Mat imageForFaceDet;
        image.copyTo(imageForFaceDet); // need this for retaining or else the img data is null sometimes during detect :)
        dispatch_async(detectionQueue, ^{
            detectionQueueIsReady = NO;
            faceDetector.getFaceRect(imageForFaceDet);
            detectionQueueIsReady = YES;
        });
    }
    else
    {
        Mat imageForFaceDet;
        image.copyTo(imageForFaceDet); // need this for retaining or else the img data is null sometimes during detect :)
        detectionQueueIsReady = NO;
        faceDetector.getFaceRect(imageForFaceDet);
        detectionQueueIsReady = YES;
    }
    
    cv::Rect face = faceDetector.lastFace;
    
    
    Mat timage(cv::Size(image.rows, image.cols), image.depth(), image.channels());
    transpose(image, timage);
    flip(timage, timage, 1);
    
    if (imageForDetect.data == NULL && timage.data == NULL) {
        NSLog(@"NULL NULL NULL!!!!!~!~!~!");
        return;
    }
    
    Mat tbg(background.size(), background.type());
    backgroundFrameInfo bgInfo{self, &tbg, time};
//    pthread_t backgroundThread;
    dispatch_async(chromaKeyQueue, ^{
        [wSelf backgroundWorker:bgInfo];
    });
    //pthread_create(&backgroundThread, NULL, backgroundWorker, (void*)&bgInfo);
//    background = [self getBackgroundFrameAtTime:time];
    
    Mat dss;
    if (whenToDetect <= framesToDetect && whenToDetect > 0){
        dss = timage;//imageForDetect;
    }else{
        dss = timage;
    }
    
    Mat edgesBuffer(dss.size(), CV_8UC1);
    Mat chromaKeyBuffer(dss.size(), CV_8UC1);
    
    //crop face for clustering skin colors
    int faceLeft = face.x;
    int faceRight = face.x + face.width;
    int faceTop = face.y;
    int faceBottom =  face.y + face.width;

     faceLeft = face.x;
     faceTop = WIDTH - face.y - face.height;
     faceRight = face.x + face.width;
     faceBottom =  WIDTH - face.y;
    
    if (dss.data == NULL) {// oh no!
        return;
    }
    //iUtil.drawFace270(dss, edgesBuffer, 180, 20, 460, 300, 255, 0, 0);
    //iUtil.drawFace270(dss, edgesBuffer, 100, 100, 540, 400, 0, 255, 0);
    
    //threadingInfo edgeInfo{tbg, dss, edgesBuffer, faceTop, faceBottom, faceLeft, faceRight, face, brightnessFix, faceImage};
    
//    pthread_t edgeThread, chromaKeyThread;
    
    
    //pthread_create(&edgeThread, NULL, edgeWorker, (void *)&edgeInfo);
    threadingInfo chromaKeyInfo{tbg, dss, chromaKeyBuffer, faceTop, faceBottom, faceLeft, faceRight, face, brightnessFix};
    //NSLog(@"before thread: %f",[[NSDate date] timeIntervalSinceDate:startDate]);
    dispatch_async(chromaKeyQueue, ^{
        [wSelf chromakeyWorker:chromaKeyInfo];
    });
   // pthread_create(&chromaKeyThread, NULL, chromakeyWorker, (void *)&chromaKeyInfo);
    
    //pthread_join(edgeThread, NULL);
   // pthread_join(backgroundThread, NULL);
   // pthread_join(chromaKeyThread, NULL);
    dispatch_barrier_sync(chromaKeyQueue, ^{
        //This waits until both the background image and chromakey tasks are finished
    });
    if (whenToDetect <= framesToDetect && whenToDetect >= 0){
        imageFull = image;
//        putText(imageFull, isoText, cv::Point(15, image.size().height - 30), FONT_HERSHEY_PLAIN, 1.25, Scalar(255, 0, 255));
//        putText(imageFull, brightnessText, cv::Point(15, image.size().height - 15), FONT_HERSHEY_PLAIN, 1.25, Scalar(255, 0, 255));
        Mat argbImage(imageFull.size(), imageFull.type());
        int from_to[] = { 0,1, 1,2, 2,3, 3,0 };
        cv::mixChannels(&imageFull,1,&argbImage,1,from_to,4);
        imageFull = argbImage;
        return;
    }

    // cUtil.drawSkinColor(timage);

    //remove noise
    Mat mask = chromaKeyBuffer;
    //bitwise_or(chromaKeyBuffer, edgesBuffer, orBuffer);
    NSDate *startNoiseDate = [NSDate date];
    if (frameCounter%20 == 0) {
        iUtil.noiseCleaningIII(dss, mask, face.width * face.height / 4);
    }
    else {
        iUtil.noiseCleaningII(mask, face.width * face.height / 4);
    }
    
//    NSBundle *mainB = [NSBundle mainBundle];
//    NSString *path = [mainB pathForResource:@"some" ofType: @"txt"];
//    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *docs_dir = [paths objectAtIndex:0];
//    NSString* aFile = [docs_dir stringByAppendingPathComponent: @"some.doc"];
//    
//    path = aFile;
//    FILE *file = fopen([path cStringUsingEncoding:1],"a+");
//    if (file == NULL) NSLog(@"%@", [path stringByAppendingString:@" not found"]);
//
//    fputs("\n*********************\n", file);
//
//    fputs("\n*5*\n", file);
//   print(maskMinus5, file);
//    fputs("\n*4*\n", file);
//    print(maskMinus4, file);
//    fputs("\n*3*\n", file);
//    print(maskMinus3, file);
//    fputs("\n*2*\n", file);
//    print(maskMinus2, file);
//    fputs("\n*1*\n", file);
//    print(maskMinus1, file);
//    fputs("\n*0*\n", file);
//    print(maskCurrent, file);
//    fputs("\n*new mask*\n", file);
//    print(mask, file);
//
//    fclose(file);
    
//    maskMinus4.copyTo(maskMinus5);
//    maskMinus3.copyTo(maskMinus4);
//    maskMinus2.copyTo(maskMinus3);
//    maskMinus1.copyTo(maskMinus2);
//    maskCurrent.copyTo(maskMinus1);
//    mask.copyTo(maskCurrent);
    
    maskMinus2 = maskMinus1;
    maskMinus1 = maskCurrent;
    maskCurrent = mask;
    
    mask = 0.1*maskMinus2 + 0.2*maskMinus1 + 0.7*maskCurrent;
    //mask = newMask;
    //dont remove head
//    NSDate *startHeadDate = [NSDate date];
//    iUtil.keepHeadFull(dss, mask, face);
//    NSLog(@"head: %f",[[NSDate date] timeIntervalSinceDate:startHeadDate]);

    
    Mat croppedBG = tbg(cv::Rect(0, (tbg.rows - cameraExpectedWidth)/2, cameraExpectedHeight, cameraExpectedWidth));
    blur(mask,mask,cv::Size(10,10));
    
    ////add saturartion
    Mat hsvImage;
    cvtColor(timage, hsvImage, CV_RGB2HSV);
    hsvImage += Scalar(0,ADD_SATURATION,0);
    cvtColor(hsvImage, timage, CV_HSV2RGB);
    
    iUtil.mergeBufferAccordingToBufferIIWithAlpha(timage, mask, croppedBG);
    transpose(tbg, imageFull);
    flip(imageFull, imageFull, 0);
    
//    putText(imageFull, isoText, cv::Point(15, image.size().height - 30), FONT_HERSHEY_PLAIN, 1.25, Scalar(255, 0, 255));
//    putText(imageFull, brightnessText, cv::Point(15, image.size().height - 15), FONT_HERSHEY_PLAIN, 1.25, Scalar(255, 0, 255));
    //cvtColor(image, image, CV_RGBA2BGRA);
    //convert RGBA to ARGB
    Mat argbImage(imageFull.size(), imageFull.type());
    int from_to[] = { 0,1, 1,2, 2,3, 3,0 };
    cv::mixChannels(&imageFull,1,&argbImage,1,from_to,4);
    imageFull = argbImage;

   // [self trackTime:start];
}

- (void)trackTime:(NSDate *)startDate {
    [profileTimes addObject:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSinceDate:startDate]]];
    if (profileTimes.count % 30 == 0) {
        double totalTime = 0.0;
        for (NSNumber *time in profileTimes) {
            totalTime += time.doubleValue;
        }
        NSLog(@"Avg image process time: %f", totalTime / profileTimes.count);
        [profileTimes removeAllObjects];
    }
}

- (cv::Mat)getCommonColors:(cv::Mat)image {
    
    cvtColor(image, image, CV_RGBA2RGB);
    //UIImage *orgImage = [self UIImageFromCVMat:image];
    int k = 10;
    int rows = image.rows;
    int cols = image.cols;
    Mat samples(rows * cols, 3, CV_32F);
    for( int y = 0; y < rows; y++ )
        for( int x = 0; x < cols; x++ )
            for( int z = 0; z < 3; z++)
                samples.at<float>(y + x*rows, z) = image.at<Vec3b>(y,x)[z];
    Mat labels;
    Mat centers;
    int attempts = 5;
    TermCriteria criteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 5, 10.0);
    
    kmeans(samples, k, labels, criteria, attempts, KMEANS_RANDOM_CENTERS, centers);
    Mat cluster_image(image.size(), image.type());
    
    for (int y = 0; y < rows; y++)
        for (int x = 0; x < cols; x++)
        {
            int cluster_idx = labels.at<int>(y + x*rows,0);
            cluster_image.at<Vec3b>(y,x)[0] = centers.at<float>(cluster_idx, 0);
            cluster_image.at<Vec3b>(y,x)[1] = centers.at<float>(cluster_idx, 1);
            cluster_image.at<Vec3b>(y,x)[2] = centers.at<float>(cluster_idx, 2);
        }
   // UIImage *clusterImage = [self UIImageFromCVMat:cluster_image];
    return cluster_image;
}

int checkBrightness(Mat mat) {
    
    const int BRIGHTNESS_THRESHOLD = 110;
    int avg = 0, count = 0;
    
    for (int r = 0; r < mat.rows; r++){
        for (int c = 0; c < mat.cols; c++){
            Vec4b currentPixel = mat.at<Vec4b>(r,c);
            avg += (currentPixel[0] + currentPixel[1] + currentPixel[2])/3;
            count ++;
        }
    }
    
    int brightness = avg/count;
    if (brightness < BRIGHTNESS_THRESHOLD){
      //  printf("not enought Brightness\n");
    }
  //  printf("Brightness = %d\n", brightness);
    return brightness;
}

- (void)startDetectionFailureTimer {
    NSNumber *detectionNumber = @(self.detectionAttemptCount);
    NSLog(@"START DETECT");
    [self performSelector:@selector(detectionFailed:)
               withObject:detectionNumber
               afterDelay:10];
}

- (void)detectionFailed:(NSNumber *)detectionNumber {
    
    if (detectionNumber.integerValue == self.detectionAttemptCount && (self.shouldDetect || self.isWaitingForCameraExposure)) {
        NSLog(@"FAIL BLOCK CALLED");
//        if (detectionResponse == DetectionResponseFailure || detectionResponse == DetectionResponseLowLight || DetectionResponseHighLight) {
//            [self checkExposure];
//        }
        self.shouldDetect = NO;
        self.detectDidSucceed = YES;
        self.isWaitingForCameraExposure = NO;
        self.cancelSuccessMessage = YES;
        //[self.videoCamera unlockCurrentDeviceAutoAdjustment];
        whenToDetect = 0;
        __weak RecorderOpenCV *wSelf = self;
        [Appsee addEvent:@"camera:detection:failed" withProperties:@{@"reason":@(detectionResponse)}];
        [Utils dispatchOnMainThread:^{
            [wSelf.detectionDelegate detectionFinishedWithResponse:detectionResponse];
        }];
    }
}

- (void)checkExposure {
    if (self.videoCamera.exposureDuration < 0.02) {
        detectionResponse = DetectionResponseHighLight;
    }
    if (self.videoCamera.iso > 800) {
        detectionResponse = DetectionResponseLowLight;
    }
}

- (void)detectionSuccess {
    self.shouldDetect = NO;
    self.detectDidSucceed = YES;
    detectionResponse = DetectionResponseSuccess;
    if (!self.cancelSuccessMessage) {
        __weak RecorderOpenCV *wSelf = self;
        [Utils dispatchOnMainThread:^{
            [wSelf.detectionDelegate detectionFinishedWithResponse:detectionResponse];
        }];
    }
}

void* edgeWorker(void *info) {
    threadingInfo* data = (threadingInfo *)info;
    eUtil.getEdgesBuffer(data->bg, data->dss, data->mask, data->faceLeft, data->faceBottom, data->faceRight, data->faceTop);
    return NULL;
}

- (void) backgroundWorker:(backgroundFrameInfo)info {
    
    backgroundFrameInfo data = info;
    RecorderOpenCV *recorder = data.recorder;
//    Mat tbg = *data->backgroundImg;
    CMTime time = data.time;
    Mat background = [recorder getBackgroundFrameAtTime:time];
    Mat tbg(cv::Size(background.rows, background.cols), background.depth(), background.channels());
    transpose(background, tbg);
    flip(tbg, tbg, 1);
    tbg.copyTo(*data.backgroundImg);
   // return NULL;
}

- (void) chromakeyWorker:(threadingInfo)info {
    
    threadingInfo data = info;
    
    //white point fix;
    Mat fixedMat = data.dss;//iUtil.reduceWhitePoint(data.dss, faceDetector.lastFace);
    //printf("\naverage white point : %d, local white point : %d, fix : %d", whitePointAverage, localWhitePoint, whitePointFix);
    
    if (whenToDetect > 0  ){//&& isFaceInPlace(data->face , cv::Rect(180,180,280,280))) {
      //  printf("\nwhen to detect: %d", whenToDetect);
        if (whenToDetect == framesToDetect) {
            //if (data.faceLeft == 0 ) {
            //    data.faceLeft = 220; data.faceRight = 420; data.faceBottom = 350; data.faceTop = 150;
            //}
            DetectionResponse res = cUtil.findChromaKeys(fixedMat, data.faceLeft, data.faceTop, data.faceRight, data.faceBottom, frontCamera);
            if(res != DetectionResponseSuccess && failsCounter < 10){
                printf("\n\n*******************\n**********not find**************\n**********************\n\n");
                detectionResponse = res;
                failsCounter++;
            }
            else {
                //res = cUtil.checkFaceLightness(fixedMat ,data.faceLeft, data.faceTop, data.faceRight, data.faceBottom);
                if (/*self.videoCamera.captureDevice.exposureTargetOffset < -0.5 ||*/ self.videoCamera.currentFrameBrightness < -0.9) {
                    res = DetectionResponseLowLight;
                } else if (/*self.videoCamera.captureDevice.exposureTargetOffset > 0.5 ||*/ self.videoCamera.currentFrameBrightness > 6.0) {
                    res = DetectionResponseHighLight;
                }
                
                if (res != DetectionResponseSuccess){
                    printf("-------bad lightning-------\n");
                    detectionResponse = res;
                   // whenToDetect--;
                } else {
//                cUtil.skinColorClustering(data->faceImage);
//                cUtil.saveSkinColorHsl();
//                cUtil.saveSkinColorHsl(fixedMat.data, data->faceLeft, data->faceTop, data->faceRight - data->faceLeft, data->faceBottom - data->faceTop);
                    whenToDetect--;
                }
            }
            [Appsee addEvent:@"record:startDetect"
              withProperties:@{@"frame" : @(frameCounter),
                               @"whenToDetect" : @(whenToDetect),
                               @"detectionResponse" : @(detectionResponse)}];
//            [Appsee addEvent:[NSString stringWithFormat:@"start Detection. frame: %d, whenToDetect: %d result: %d", frameCounter, whenToDetect, detectionResponse]];
        }
        else {
            DetectionResponse res = cUtil.addChromaKeys(fixedMat, data.faceLeft, data.faceTop, data.faceRight, data.faceBottom, frontCamera);
            if(res != DetectionResponseSuccess){
                printf(" not add %d", whenToDetect);
                //detectionResponse = res;
                whenToDetect--;
            }   else {
                whenToDetect--;
            }
        }
        return; //NULL;
        
    }
    else if (whenToDetect == 0) {
        //cUtil.convertArraysToScalars();
        //[Appsee addEvent:[NSString stringWithFormat:@"finished Detection. frame: %d, whenToDetect: %d", frameCounter, whenToDetect]];
        [Appsee addEvent:@"record:finishDetect"
          withProperties:@{@"frame" : @(frameCounter),
                           @"whenToDetect" : @(whenToDetect),
                           @"detectionResponse" : @(detectionResponse)}];
        whenToDetect--;
        return;// NULL;
     //   printf("no detect %d", whenToDetect);
    }
    
//    dispatch_async(checkingQueue, ^{
//        cUtil.checkAndAddChromaKeysTopArea(fixedMat);
//    });
//    dispatch_async(checkingQueue, ^{
//        cUtil.checkAndAddChromaKeysLeftArea(fixedMat);
//    });
//    dispatch_async(checkingQueue, ^{
//        cUtil.checkAndAddChromaKeysRightArea(fixedMat);
//    });
//    dispatch_barrier_sync(checkingQueue, ^{
//        
//    });
//    cUtil.checkAndDeductChromaKeysAgainstSkinColor();
    
   // NSLog(@"check and add: %f",[[NSDate date] timeIntervalSinceDate:startDate]);
//    clock_t begin = clock();
    
    //cUtil.getChromaKeysBuffer(fixedMat, data.mask, data.faceLeft, data.faceBottom, data.faceRight, data.faceTop);
    if (whenToDetect == -1 ){
        //[Appsee addEvent:[NSString stringWithFormat:@"start bg substraction. frame: %d, whenToDetect: %d", frameCounter, whenToDetect]];
    }
    cUtil.getChromaKeysSectionBufferThreaded(fixedMat, data.mask);
        
    return; //NULL;
}
- (cv::Mat)getBackgroundFrameAtTime:(CMTime)time {
    
    if (CMTIME_COMPARE_INLINE(time, ==, currentTime) ||
        CMTIME_COMPARE_INLINE(time, <=, self.exploder.currentFrameTime)) { // gotta make sure we don't get ahead of the times during FAST MODE
        return background;
    }
    currentTime = time;
   // NSLog(@"current time: %f", CMTimeGetSeconds(currentTime));
    cv::Mat newFrame = [self.exploder getFrameAtTime:time];
    if (newFrame.total() > 0) {
        background = newFrame;
        return newFrame;
    }
    return background;
}



- (BOOL)setUpVideoCaptureWithURL:(NSString *)url {
    
    cv::String cvUrl = [url UTF8String];
    
    BOOL isOpened = cap.open(cvUrl);
    
    return isOpened;
}

#endif


#pragma mark - UI Actions

- (void)startCamera
{
    [self.videoCamera start];
    //[self.view bringSubviewToFront:self.rectView];
}

- (void)stopCamera
{
    
}

- (void)startRecording {
    [self.videoCamera startRecording];
    startedRecording = true;
}

- (void)stopRecording {
    [self.videoCamera stopRecording];
    startedRecording = false;
}

- (void)pauseRecording {
    if (!self.videoCamera.isPaused && self.videoCamera.isRecording)
    {
        [self.videoCamera pauseRecording];
    }
}

- (void)resumeRecording {
    if (self.videoCamera.isPaused)
    {
        [self.videoCamera resumeRecording];
    }
    else
    {
        NSLog(@"WARNING: You are trying to resume while the recorder is not paused!");
    }
}

- (void)cancelRecording {
    [self.videoCamera cancelRecording];
}

- (void)setRecordingSpeedScale:(Float64)scale {
    [self.videoCamera setRecordingSpeedScale:scale];
}

- (cv::Mat)cvMatFromUIImage:(UIImage *)image
{
    return [self cvMatFromCGImage:image.CGImage];
}

- (cv::Mat)cvMatFromCGImage:(CGImageRef)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image);
    CGFloat cols = CGImageGetWidth(image);
    CGFloat rows = CGImageGetHeight(image);
    //int numOfComponents = CGColorSpaceGetNumberOfComponents(colorSpace);
    int cvFormat = CV_8UC4;
    
    cv::Mat cvMat(rows, cols, cvFormat); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image);
    CGContextRelease(contextRef);
    
    return cvMat;
}

-(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat {
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    
    CGColorSpaceRef colorSpace;
    CGBitmapInfo bitmapInfo;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
        bitmapInfo = kCGImageAlphaNone | kCGBitmapByteOrderDefault;
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
        bitmapInfo = kCGBitmapByteOrder32Little | (
                                                   cvMat.elemSize() == 3? kCGImageAlphaNone : kCGImageAlphaNoneSkipFirst
                                                   );
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(
                                        cvMat.cols,                 //width
                                        cvMat.rows,                 //height
                                        8,                          //bits per component
                                        8 * cvMat.elemSize(),       //bits per pixel
                                        cvMat.step[0],              //bytesPerRow
                                        colorSpace,                 //colorspace
                                        bitmapInfo,                 // bitmap info
                                        provider,                   //CGDataProviderRef
                                        NULL,                       //decode
                                        false,                      //should interpolate
                                        kCGRenderingIntentDefault   //intent
                                        );
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return finalImage;
}

-(void)setUsingFrontCamera:(BOOL)usingFrontCamera{
    _usingFrontCamera = usingFrontCamera;
    frontCamera = usingFrontCamera;
}

- (void)reset {
    [self resetForRedetect];
    [self.videoCamera reset];
}

- (void)resetAndResetCapSession:(BOOL)resetCapSession {
    [self resetForRedetect];
    [self.videoCamera reset:resetCapSession];
}

-(void)resetForRedetect {
//    waitFrames = 50;
    self.shouldDetect = NO;
    self.detectDidSucceed = NO;
    self.isWaitingForCameraExposure = NO;
    self.cancelSuccessMessage = YES;
    whenToDetect = framesToDetect;
//    [self.videoCamera unlockFocus];
//    [self.videoCamera unlockExposure];
//    [self.videoCamera unlockBalance];
    [self.videoCamera unlockCurrentDeviceAutoAdjustment];
    detectionResponse = DetectionResponseFailure;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
