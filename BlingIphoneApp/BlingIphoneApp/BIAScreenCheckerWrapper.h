//
//  BIAScreenCheckerWrapper.h
//  BlingIphoneApp
//
//  Created by Joseph Nahmias on 21/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BIAScreenCheckerWrapper : NSObject

+(BOOL)checkScreen:(CVPixelBufferRef)screen forAccuracyRate:(CGFloat)accuracy resizeFactor:(CGFloat)resizeFactor;

@end
