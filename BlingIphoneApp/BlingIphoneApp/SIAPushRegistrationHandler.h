//
//  SIANotificationsRegistrationHandler.h
//  ChosenIphoneApp
//
//  Created by michael on 8/3/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SIAPushRegisterCriteria) {
    SIAPushRegisterCriteriaFiveVotes,
    SIAPushRegisterCriteriaTwoGames,
    SIAPushRegisterCriteriaCompleteRecording,
    SIAPushRegisterCriteriaFourMinutes,
    SIAPushRegisterCriteriaReentry
};

static NSString *kGameVotesDictionary = @"game_votes";
static NSString *kVotesInSingleGame = @"votes_in_game"; // the user votes 5 times in a single game
static NSString *kGameIdForVotes = @"game_id";
static NSString *kGamesEnteredArray = @"games_entered";  // the user enters their second unique game
static NSString *kLoginTime = @"login_time"; // the app has been open for 4 minutes and is not viewing a game, recording, or video home
static NSString *kInstallTime = @"install_time"; // time the app was first open - surface push registration if opening more than 12 hours after install
static NSString *kNeedsRegisterForPushNotifications = @"needs_to_register"; // key to keep a boolean about whether we need have registered or not
static NSString *kUserChoseNo = @"chose_no"; // if the user chooses the "nope" option on the popup
static NSString *kChoseNoTimestamp = @"chose_no_timestamp"; // to record the time when the user chose "nope"

@interface SIAPushRegistrationHandler : NSObject

@property (nonatomic, readonly) BOOL interstitialIsShown;
@property (nonatomic, copy, nullable) void (^registrationCompletionHandler)(void);

+ (SIAPushRegistrationHandler *)sharedHandler;
- (void)recordingCompleted;
- (void)followedUser;
- (void)commented;
- (void)homeScreenDidAppear;
- (void)notificationScreenDidAppear;
- (void)pauseTimer;
- (void)resumeTimer;
- (void)saveCurrentPushDict;
- (void)registerNotifications:(void (^)(void))handler;
- (void)applicationDidRegister;
- (void)interstitialDidDismiss;

@end
