//
//  SIAItunesSoundtrakModel.m
//  ChosenIphoneApp
//
//  Created by Joseph Nahmias on 11/04/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAItunesSoundtrakModel.h"
#import "SIAObjectManager.h"
#import "SIANetworking.h"

#define ITUNES_API_FOR_SONGS @"https://itunes.apple.com/search?term=%@&media=music"
#define FIRST_TRACKS_API @"api/v1/soundtracks"

@implementation SIAItunesSoundtrakModel

+(RKObjectMapping *)objectMapping{
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[SIAItunesSoundtrakModel class]];
    [objectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"results" toKeyPath:@"tracksArray" withMapping:[SIAItunesMusicElement objectMapping]]];
    
    return objectMapping;
}

+(void)getFirstTracksWithCompletionHandler:(void (^)(SIAItunesSoundtrakModel *model))handler{
    [RequestManager
     getRequest:FIRST_TRACKS_API
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         if(mappingResult.firstObject)
             handler(mappingResult.firstObject);
     } failure:^(RKObjectRequestOperation *operation, NSError *error) {
         handler(nil);
     }
     showFailureDialog:YES];
}

-(void)searchTerm:(NSString *)term withCompletionHandler:(void (^)(BOOL))handler{
    
    term = [term stringByReplacingOccurrencesOfString:@" "
                                           withString:@"+"];
    
    NSString *keyPath = [NSString stringWithFormat:ITUNES_API_FOR_SONGS, term];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[SIAItunesSoundtrakModel objectMapping]
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];

    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:keyPath ]];
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:urlRequest responseDescriptors:@[responseDescriptor]];

    [operation.HTTPRequestOperation setAcceptableContentTypes:[NSSet setWithObject:@"text/javascript"]];
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/javascript"];
    
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        self.tracksArray = [(SIAItunesSoundtrakModel *)mappingResult.firstObject tracksArray];
        handler(YES);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        handler(NO);
    }];
    
    [operation start];
}


@end
