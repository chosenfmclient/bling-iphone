//
//  SIAShareMapping.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 12/9/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SIAShareMapping : NSObject

@property (strong, nonatomic) NSString *share_id;
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *object_id;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *platform;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *mailSubject;
@property (strong, nonatomic) NSString *shareTags;

+(RKObjectMapping *)shareObjectMapping;
+(RKObjectMapping *)objectMapping;

@end


@interface SIAShareMappingWrapper : NSObject

@property (strong, nonatomic) SIAShareMapping *data;
+(RKObjectMapping *)objectMapping;
@end
