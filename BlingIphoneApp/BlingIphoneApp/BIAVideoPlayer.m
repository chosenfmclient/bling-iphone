//
//  BIAVideoPlayer.m
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 07/09/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "BIAVideoPlayer.h"

#define AUDIO_EFFECT_REVERB_STR @"reverb"
#define AUDIO_EFFECT_ECHO_STR @"echo"

#define VIDEO_EFFECT_B_W_STR @"black_white"
#define VIDEO_EFFECT_SATURATION_STR @"saturation"

#define IMPLEMENTS_EFFECT_WITH_VIDEO_COMPOSITION [AVVideoComposition respondsToSelector:@selector(videoCompositionWithAsset:applyingCIFiltersWithHandler:)]


static void *playerRateObservationContext = &playerRateObservationContext;
static void *playerLayerObservationContext = &playerLayerObservationContext;
static void *playerItemObservationContext = &playerItemObservationContext;

// This struct is used to pass along data between the MTAudioProcessingTap callbacks.
typedef struct AVAudioTapProcessorContext {
    Boolean supportedTapProcessingFormat;
    Boolean isNonInterleaved;
    Float64 sampleRate;
    AudioUnit reverbUnit;
    AudioUnit delayUnit;
    AUGraph processingGraph;
    Float64 sampleCount;
    float leftChannelVolume;
    float rightChannelVolume;
    void *self;
} AVAudioTapProcessorContext;


// MTAudioProcessingTap callbacks.
static void tap_InitCallback(MTAudioProcessingTapRef tap, void *clientInfo, void **tapStorageOut);
static void tap_FinalizeCallback(MTAudioProcessingTapRef tap);
static void tap_PrepareCallback(MTAudioProcessingTapRef tap, CMItemCount maxFrames, const AudioStreamBasicDescription *processingFormat);
static void tap_UnprepareCallback(MTAudioProcessingTapRef tap);
static void tap_ProcessCallback(MTAudioProcessingTapRef tap, CMItemCount numberFrames, MTAudioProcessingTapFlags flags, AudioBufferList *bufferListInOut, CMItemCount *numberFramesOut, MTAudioProcessingTapFlags *flagsOut);

// Audio Unit callbacks.
static OSStatus AU_RenderCallback(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *ioData);



@interface BIAVideoPlayer ()

{
//    __strong GPUImageMovie *movieFile;
//    __strong GPUImageOutput<GPUImageInput> *filter;
//    __strong GPUImageMovieWriter *movieWriter;
//    
//    //    GPUImageGrayscaleFilter* luminanceFilter;
//    //    GPUImagePixellateFilter* pixelFilter;
//    //    GPUImageEmbossFilter* embossFilter;
//    __strong GPUImageSaturationFilter* saturationFilter;
//    __strong GPUImageSwirlFilter* swirlFilter;
    
    CGFloat pixelFraction, saturation, swirlAngle;
    
    __strong AVPlayerItem* playerItem;
    
    BOOL isPlaying;
    BOOL isAudioEffect;
    BOOL isAudioPressed, isVideoPressed;
    float mRestoreAfterScrubbingRate;
    dispatch_queue_t playQueue;
}

@property (nonatomic) BOOL audioPlayerReady;
@property (nonatomic) BOOL videoPlayerReady;
@property (nonatomic, strong) AVPlayer *playbackPlayer;

//audio filter bool
@property (nonatomic) BOOL enableAudioEffectDelay;
@property (nonatomic) BOOL enableAudioEffectReverb;

@property (nonatomic, strong) AVAssetTrack *recordedAudioTrack;

@property (nonatomic)  BOOL videoIsReady;
@property (nonatomic)  BOOL playbackIsReady;

@property (nonatomic, strong) NSTimer *seekTimer;
@property (strong, nonatomic) AVMutableCompositionTrack *compositionPlaybackAudioTrackForRecordPreview;
@property (strong, nonatomic) AVAsset *originalAsset;
@property (nonatomic) CGFloat videoAspectRatio;

@property (strong, nonatomic) id playerItemFinishObserver;
@property (nonatomic) BOOL shouldRepeatPlayback;


@end

@implementation BIAVideoPlayer

- (instancetype) initWithRecordingItem:(SIARecordingItem *)recordingItem delegate:(id <BIAVideoPlayerDelegate>)delegate {
    self = [self initWithRecordingItem:recordingItem];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}

- (instancetype)initWithRecordingItem:(SIARecordingItem *)recordingItem{
    self = [super initWithPlayerItem:[AVPlayerItem playerItemWithAsset:[self createCompositionFromRecorded:recordingItem.recordURL
                                                                                             playbackAsset:recordingItem.assetForPlayback
                                                                                              playbackTime:recordingItem.timeOfPlayback
                                                                                            playbackOffset:recordingItem.playbackStartOffset
                                                                                             playbackScale:recordingItem.recordingSpeedScale]]];
    if (self){
        _playAfterAppBecomesActive = NO;
        _originalAsset = [AVURLAsset URLAssetWithURL:recordingItem.recordURL options:nil];
        //if (recordingItem.recordingSpeedScale != 1) // set the audio playback to scale with pitch correction
        self.currentItem.audioTimePitchAlgorithm = AVAudioTimePitchAlgorithmSpectral;
       // self.transform = ((AVAssetTrack *) [[_originalAsset tracksWithMediaType:AVMediaTypeVideo] lastObject]).preferredTransform;
        
        if (IMPLEMENTS_EFFECT_WITH_VIDEO_COMPOSITION) {
            self.filterView = [[SIAVideoPlayerView alloc] initWithPlayer:self];
            //self.currentItem.videoComposition = [self videoCompositionForEffectType:SIAVideoEffectTypeNone];
            //[self setUpPlayerLayer];
            
        }
        //[self setupAudioMix];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:self.currentItem];
        [self addObserver:self forKeyPath:kRateKey options:NSKeyValueObservingOptionNew context:playerRateObservationContext];
    }
    NSLog(@"volume: %f",self.volume);
    return self;
}

- (id)initWithURL:(NSURL *)URL {
    if (self = [super initWithURL:URL]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:self.currentItem];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    }
    return self;
}

- (void)observeStatusForReadyToPlay {
    if (self.status == AVPlayerStatusReadyToPlay && self.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        [self.delegate playerReadyToPlay];
        return;
    }
    [self addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    [self.currentItem addObserver:self
                       forKeyPath:@"status"
                          options:NSKeyValueObservingOptionNew
                          context:playerItemObservationContext];
}

- (void)replaceCurrentItemWithPlayerItem:(AVPlayerItem *)item {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:AVPlayerItemDidPlayToEndTimeNotification
                                                  object:self.currentItem];
    [super replaceCurrentItemWithPlayerItem:item];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.currentItem];
}

- (void) setUpPlayerLayer {
    [((SIAVideoPlayerView *)self.filterView).playerLayer addObserver:self
                                                          forKeyPath:@"readyForDisplay"
                                                             options:NSKeyValueObservingOptionNew
                                                             context:playerLayerObservationContext];
}
- (instancetype)initWithVideo: (SIAVideo *)video {
    self = [self initWithVideo:video andPlayerView:nil];
    return self;
}

- (instancetype)initWithVideo: (SIAVideo *)video andPlayerView: (SIAVideoPlayerView *)filterView {
    
    NSURL *videoUrl = video.videoUrl;
    video.playbackURL = nil;
    AVPlayerItem *newItem = [AVPlayerItem playerItemWithURL:videoUrl];
    self = [super initWithPlayerItem:newItem];
    video.playerItem = newItem;
    
    if (self){
        self.filterView = filterView;
        if (filterView) {
            self.filterView.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self];
            [self.filterView.layer addSublayer:self.filterView.playerLayer];
//            [self.playerLayer setVideoGravity:AVLayerVideoGravityResizeAspect];
            self.filterView.playerLayer.frame = self.filterView.bounds;
        }
        _playAfterAppBecomesActive = NO;
        _originalAsset = [AVURLAsset URLAssetWithURL:videoUrl
                                             options:nil];
        
        self.recordedAudioTrack = [[_originalAsset tracksWithMediaType:AVMediaTypeAudio] lastObject];
        
        if (IMPLEMENTS_EFFECT_WITH_VIDEO_COMPOSITION) {
            if (!filterView) {
                self.filterView = [[SIAVideoPlayerView alloc] initWithPlayer:self];
            }
            self.transform = CGAffineTransformIdentity;
            
//            CGAffineTransform xform = self.transform;
//            CIContext *cicontext = nil;//[CIContext contextWithOptions:@{kCIContextWorkingColorSpace:[NSNull null]} ];
//            AVVideoComposition *videoComposition = [AVVideoComposition videoCompositionWithAsset:_originalAsset applyingCIFiltersWithHandler:^(AVAsynchronousCIImageFilteringRequest * _Nonnull request) {
//                CIImage *sourceImage = request.sourceImage.imageByClampingToExtent;
//                //                sourceImage = [sourceImage imageByApplyingFilter:@"CIAffineTransform" withInputParameters:@{kCIInputTransformKey:[NSValue valueWithBytes:&xform objCType:@encode(CGAffineTransform)]}];
//                sourceImage = [sourceImage imageByCroppingToRect:request.sourceImage.extent];
//                [request finishWithImage:sourceImage context:cicontext];
//                
//            }];
            //            self.currentItem.videoComposition = videoComposition;
            
        }
//        for (NSString *videoFilter in video.video_filters){
//            if ([videoFilter isEqualToString:VIDEO_EFFECT_B_W_STR]) {
//                [self setVideoFilter:SIAVideoEffectTypeBlackAndWhite];
//            } else if ([videoFilter isEqualToString:VIDEO_EFFECT_SATURATION_STR]) {
//                [self setVideoFilter:SIAVideoEffectTypeSaturation];
//            }
//        }
//        if (video.audio_filters.count > 0) {
//            [self setupAudioMix];
//        }
//        for (NSString *audioFilter in video.audio_filters) {
//            if ([audioFilter isEqualToString:AUDIO_EFFECT_ECHO_STR]) {
//                [self setAudioFilter:SIAAudioEffectTypeDelay];
//            } else if ([audioFilter isEqualToString:AUDIO_EFFECT_REVERB_STR]){
//                [self setAudioFilter:SIAAudioEffectTypeReverb];
//            }
//        }
        
        [self addObserver:self forKeyPath:kRateKey options:NSKeyValueObservingOptionNew context:playerRateObservationContext];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:self.currentItem];
        
        [self observeStatusForReadyToPlay];

    }
    NSLog(@"volume: %f",self.volume);
    return self;
    
}

-(void)seekToTime:(CMTime)time{
 //   if (self.playbackPlayer){
        
        [self.playbackPlayer seekToTime:time];
//    }else{
        [super seekToTime:time];
//    }
}

-(void)seekToTime:(CMTime)time completionHandler:(void (^)(BOOL))completionHandler{
    [super seekToTime:time completionHandler:completionHandler];
    if (self.playbackPlayer) {
        [self.playbackPlayer seekToTime: CMTimeAdd(time, CMTimeMakeWithSeconds(7, 64))];
    }
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (context == playerRateObservationContext){
        if (self.rate == 0){
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
        } else {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
            if ([self.delegate respondsToSelector:@selector(playerWillPlay:)]){
                [self.delegate playerWillPlay:object];
            }
            if ([self.delegate respondsToSelector:@selector(playerDidPlay:)]){
                if (CMTIME_COMPARE_INLINE(self.currentTime, <, self.currentItem.duration)){
                    
                    __weak BIAVideoPlayer* blockPlayer = self;
                    __block id obs;
                    
                    CMTime timeToObserve = CMTimeAdd(self.currentTime, @(0.1).CMTimeValue);
                    obs = [blockPlayer addBoundaryTimeObserverForTimes:
                           @[[NSValue valueWithCMTime:timeToObserve]]
                                                                 queue:NULL
                                                            usingBlock:^{
                                                                
                                                                // Raise a notificaiton when playback has started
                                                                [blockPlayer.delegate playerDidPlay:object];
                                                                
                                                                // Remove the boundary time observer
                                                                [blockPlayer removeTimeObserver:obs];
                                                            }];
                    
                }
            }
        }
    }
    else if ([keyPath isEqualToString:@"status"]){
        if (object == self && self.status == AVPlayerStatusReadyToPlay && self.currentItem.status == AVPlayerItemStatusReadyToPlay){
            [object removeObserver:self forKeyPath:keyPath];
            [self prerollAtRate:1.0 completionHandler:^(BOOL finished) {
                [self play];
            }];
        }
        else if (context == playerItemObservationContext && self.currentItem.status == AVPlayerItemStatusReadyToPlay) {
            if([self.delegate respondsToSelector:@selector(playerReadyToPlay)])
            {
                [self.delegate playerReadyToPlay];
            } else {

                [self play];
            }
            [object removeObserver:self
                        forKeyPath:keyPath
                           context:context];
        }
        else if (!context){
            //        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
            [object removeObserver:self
                        forKeyPath:keyPath
                           context:context];
        }
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    if([self.delegate respondsToSelector:@selector(playerDidFinishPlaying:)])
        [self.delegate playerDidFinishPlaying:self];
}

//-(void)setRate:(float)rate{
//    if (self.rate == rate) {
//        if (!self.playbackPlayer || self.playbackPlayer.rate == rate) {
//            return;
//        }
//        
//    }
//    if (rate == 0 || self.playbackPlayer == nil) {
//        
//        [super setRate:rate];
//        [self.playbackPlayer setRate:rate];
//        
//    } else  {
//        
//        [super setRate:rate];
//        [self.playbackPlayer setRate:rate];
//        
//        if (self.status == AVPlayerStatusReadyToPlay) {
//            if (self.playbackPlayer.status && self.playbackPlayer.status != AVPlayerStatusReadyToPlay) {
//                return;
//            }
//            [self prerollAtRate:0.1f completionHandler:^(BOOL finished) {
//                dispatch_sync(playQueue, ^{
//                    self.videoIsReady = YES;
//                    if (self.playbackIsReady) {
//                        self.videoIsReady = self.playbackIsReady = NO;
//                        CMClockRef syncTime = CMClockGetHostTimeClock();
//                        CMTime hostTime = CMTimeAdd(CMClockGetTime(syncTime), CMTimeMakeWithSeconds(1, 64));
//                        [self setRate:rate time:kCMTimeInvalid atHostTime:hostTime];
//                        [self.playbackPlayer setRate:rate time:kCMTimeInvalid atHostTime:hostTime];//CMTimeSubtract(hostTime, CMTimeMakeWithSeconds(KARAOKE_FILE_SCLIENT_TIME, 60))];
//                        //[self addObserver:self forKeyPath:@"rate" options:NSKeyValueObservingOptionNew context:@"rateChange"];
//                        CGFloat videoTime = CMTimeGetSeconds(self.currentTime);
//                        CGFloat playbasckTime = CMTimeGetSeconds(self.playbackPlayer.currentTime);
//                        NSLog(@"video: %f\nplayback: %f\ndifference: %f", videoTime, playbasckTime, playbasckTime - 7 - videoTime);
//                    }
//                });
//            }];
//        }
//        if (self.playbackPlayer.status == AVPlayerStatusReadyToPlay) {
//            [self.playbackPlayer prerollAtRate:0.1f completionHandler:^(BOOL finished) {
//                dispatch_sync(playQueue, ^{
//                    self.playbackIsReady = YES;
//                    if (self.videoIsReady) {
//                        self.videoIsReady = self.playbackIsReady = NO;
//                        CMClockRef syncTime = CMClockGetHostTimeClock();
//                        CMTime hostTime = CMTimeAdd(CMClockGetTime(syncTime), CMTimeMakeWithSeconds(1, 64));
//                        [self setRate:rate time:kCMTimeInvalid atHostTime:hostTime];
//                        [self.playbackPlayer setRate:rate time:kCMTimeInvalid atHostTime:hostTime];//CMTimeSubtract(hostTime, CMTimeMakeWithSeconds(KARAOKE_FILE_SCLIENT_TIME, 60))];
//                        //[self addObserver:self forKeyPath:@"rate" options:NSKeyValueObservingOptionNew context:@"rateChange"];
//                        CGFloat videoTime = CMTimeGetSeconds(self.currentTime);
//                        CGFloat playbasckTime = CMTimeGetSeconds(self.playbackPlayer.currentTime);
//                        NSLog(@"video: %f\nplayback: %f\ndifference: %f", videoTime, playbasckTime, playbasckTime - 7 - videoTime);
//                    }
//                });
//            }];
//        }
//    }
//}

/**
 *  Creates an AVMutableComposition asset for playback that merges the recorded video with a playback audio track from asset used during recording playback, such as a soundtrack.
 *
 *  @param recordedURL   NSURL The file URL for the recorded video
 *  @param playbackAsset AVAsset The asset containing the audio to merge with the recording
 *  @param offset        CMTime the amount to offset the audio track to sync the audio and video
 *
 *  @return AVAsset the merged video/audio asset.
 */
- (AVAsset *)createCompositionFromRecorded:(NSURL *)recordedURL
                             playbackAsset:(AVAsset *)playbackAsset
                              playbackTime:(CMTime)timeOfPlayback
                            playbackOffset:(CMTime)offset
                             playbackScale:(Float64)playbackScale{
    
    
    AVMutableComposition* composition = [AVMutableComposition composition];
    AVURLAsset* recordedAsset = [[AVURLAsset alloc]initWithURL:recordedURL
                                                       options:@{AVURLAssetPreferPreciseDurationAndTimingKey:@YES}];
    
    AVAssetTrack *videoTrack = [recordedAsset tracksWithMediaType:AVMediaTypeVideo].firstObject;
    self.videoAspectRatio = videoTrack.naturalSize.width / videoTrack.naturalSize.height;
    
     CMTimeRange range = CMTimeRangeMake(kCMTimeZero, recordedAsset.duration);
    

    NSError* error = NULL;
    
    [composition insertTimeRange:range
                         ofAsset:recordedAsset
                          atTime:kCMTimeZero
                           error:&error];

    
    AVAssetTrack *recordedVideoTrack = [recordedAsset tracksWithMediaType:AVMediaTypeVideo].firstObject;
    self.transform = recordedVideoTrack.preferredTransform;
    
    CMTime videoPlaybackDiff = CMTimeSubtract(timeOfPlayback, CMTimeMultiplyByFloat64(range.duration, playbackScale));
    // don't let it go negative plz
    offset = CMTimeMaximum(kCMTimeZero, offset);
    
    [composition scaleTimeRange:range
                     toDuration:CMTimeMultiplyByFloat64(range.duration, playbackScale)];
    
    if (playbackAsset != nil && [playbackAsset tracksWithMediaType:AVMediaTypeAudio].count > 0) {
        AVMutableCompositionTrack *compositionPlaybackAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        range.duration = CMTimeSubtract(range.duration, offset);
        range.start = offset;
        // don't let the range duration be longer than the asset duration
        range.duration = CMTimeMinimum(playbackAsset.duration, CMTimeMultiplyByFloat64(range.duration, playbackScale));
        self.compositionPlaybackAudioTrackForRecordPreview = compositionPlaybackAudioTrack;
        [compositionPlaybackAudioTrack insertTimeRange:range
                                               ofTrack:[[playbackAsset tracksWithMediaType:AVMediaTypeAudio]objectAtIndex:0]
                                                atTime:kCMTimeZero
                                                 error:&error];
        if (error)
            NSLog(@"Error inserting playback track: %@", error);
        NSLog(@"offset now: %f", CMTimeGetSeconds(offset));
    }
    
    return composition;
}

-(void)releaseAudioMix{
    self.currentItem.audioMix = nil;
}

-(void)dealloc{
    @try {
        [self removeObserver:self forKeyPath:@"status"];
    }
    @catch (NSException *exception) {
        ;
    }
    if (self.playbackPlayer.status != AVPlayerStatusReadyToPlay){
        @try {
            [self.playbackPlayer removeObserver:self forKeyPath:@"status"];
        }
        @catch (NSException *exception) {
            ;
        }
    }
    @try {
        [self removeObserver:self forKeyPath:kRateKey];
        [self.currentItem removeObserver:self forKeyPath:@"status"];
    }
    @catch (NSException *exception) {
        ;
    }
    self.currentItem.audioMix = nil;
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


/** \brief handle upload when app goes to background
 *
 * \param none
 *
 * \return none
 *
 * ...
 *
 */
- (void) appWillResignActive:(NSNotification *)notification {
    
    [self pause];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    }


/** \brief handle playing when app goes to background
 *
 * \param none
 *
 * \return none
 *
 * ...
 *
 */
- (void) appDidBecomeActive:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];

    if (_playAfterAppBecomesActive)
    {
        [self play];
    }
    NSLog(@"BECOME ACTIVE (effect queue) %@", notification.description);
}

- (void)setupAudioMix{
    AVMutableAudioMix *audioMix = [AVMutableAudioMix audioMix];
    if (audioMix && self.recordedAudioTrack)
    {
        AVAssetTrack *audioTrack = self.recordedAudioTrack;
        AVMutableAudioMixInputParameters *audioMixInputParameters = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:audioTrack];
        if (audioMixInputParameters)
        {
            MTAudioProcessingTapCallbacks callbacks;
            
            callbacks.version = kMTAudioProcessingTapCallbacksVersion_0;
            callbacks.clientInfo = (__bridge void *)self,
            callbacks.init = tap_InitCallback;
            callbacks.finalize = tap_FinalizeCallback;
            callbacks.prepare = tap_PrepareCallback;
            callbacks.unprepare = tap_UnprepareCallback;
            callbacks.process = tap_ProcessCallback;
            
            MTAudioProcessingTapRef audioProcessingTap;
            if (noErr == MTAudioProcessingTapCreate(kCFAllocatorDefault, &callbacks, kMTAudioProcessingTapCreationFlag_PreEffects, &audioProcessingTap))
            {
                audioMixInputParameters.audioTapProcessor = audioProcessingTap;
                
                CFRelease(audioProcessingTap);
                
                if (self.compositionPlaybackAudioTrackForRecordPreview) {
                    AVMutableAudioMixInputParameters *volumeMixParameters = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:self.compositionPlaybackAudioTrackForRecordPreview];
                    [volumeMixParameters setVolume:0.2 atTime:kCMTimeZero];
                    // Attach the input parameters to the audio mix.
                    audioMix.inputParameters = @[audioMixInputParameters, volumeMixParameters];
                }
                
                
                else {
                    audioMix.inputParameters = @[audioMixInputParameters];
                }
                self.currentItem.audioMix = audioMix;
            }
        }
    }
    
}

/** \brief add new filter to the base filters group
 *
 * \param theFilter: index of the new filter to set
 *
 * \return none
 *
 * ...
 *
 */
- (void)setVideoFilter:(SIAVideoEffectType)theFilter
{
    self.currentItem.videoComposition = [self videoCompositionForEffectType:theFilter];
    if (self.videoEffects == nil) self.videoEffects = [NSMutableArray array];
    NSMutableArray *effectsArray = [self.videoEffects mutableCopy];
    [effectsArray addObject:[NSNumber numberWithInteger:theFilter]];
    self.videoEffects = effectsArray;
}

- (void)unsetVideoFilter:(SIAVideoEffectType)theFilter {
    
    self.currentItem.videoComposition = [self videoCompositionForEffectType:SIAVideoEffectTypeNone];
  
    NSMutableArray *effectsArray = [self.videoEffects mutableCopy];
    for (NSNumber *effect in effectsArray) {
        if (effect.integerValue == theFilter){
            [self.videoEffects removeObject:effect];
        }
    }
}


- (AVVideoComposition *)videoCompositionForEffectType:(SIAVideoEffectType)effectType {
//    return nil;
    CIFilter *videoFilter = [self filterForEffectType:effectType];
    CGAffineTransform imageTransfrom = CGAffineTransformInvert(self.transform);
    
    imageTransfrom.ty *= self.videoAspectRatio; // when taking the invert transform, the translation for portrait videos is relative to the x value, but the invert transform needs to be scaled for the y translation.
    
    NSLog(@"image transform: %@", NSStringFromCGAffineTransform(imageTransfrom));
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoCompositionWithAsset:self.currentItem.asset
                                                                   applyingCIFiltersWithHandler:^(AVAsynchronousCIImageFilteringRequest * _Nonnull request)
                                                   {
                                                       CIImage *sourceImage = [request.sourceImage imageByApplyingTransform:imageTransfrom];
                                                       [videoFilter setValue:sourceImage.imageByClampingToExtent
                                                                      forKey:kCIInputImageKey];
                                                       CIImage *output = videoFilter.outputImage;
//                                                       output = [output imageByApplyingTransform:];
                                                       output = [output imageByCroppingToRect:sourceImage.extent];
                                                       [request finishWithImage:output
                                                                        context:nil];
                                                   }];
    CGRect videoRect = CGRectMake(0, 0, videoComposition.renderSize.width, videoComposition.renderSize.height);
    videoComposition.renderSize = CGRectApplyAffineTransform(videoRect, self.transform).size;
    
    return videoComposition;
}
                        
- (CIFilter *)filterForEffectType:(SIAVideoEffectType)effectType {
    
    CGFloat saturationValue = 1.0;
    switch (effectType) {
        case SIAVideoEffectTypeBlackAndWhite: {
            saturationValue = 0.0;
            break;
        }
        case SIAVideoEffectTypeSaturation: {
            saturationValue = [[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion: (NSOperatingSystemVersion){10, 0, 0}] ? 1.25 : 2.0;
            break;
        }
        default:
            break;
    }

    return [CIFilter filterWithName:@"CIColorControls"
                withInputParameters:@{kCIInputSaturationKey:@(saturationValue)}];
    
}
    
- (void)setAudioFilter:(SIAAudioEffectType)theFilter
{
    switch (theFilter) {
        case SIAAudioEffectTypeDelay:
            self.enableAudioEffectDelay = YES;
            break;
        case SIAAudioEffectTypeReverb:
            self.enableAudioEffectReverb = YES;
            break;
    }
    if (self.audioEffects == nil) self.audioEffects = @[].mutableCopy;
    [self.audioEffects addObject:[NSNumber numberWithInteger:theFilter]];
}

- (void)unsetAudioFilter:(SIAAudioEffectType)theFilter
{
    switch (theFilter) {
        case SIAAudioEffectTypeDelay:
            self.enableAudioEffectDelay = NO;
            break;
        case SIAAudioEffectTypeReverb:
            self.enableAudioEffectReverb = NO;
            break;
    }
    NSMutableArray *effectsArray = [self.audioEffects mutableCopy];
    for (NSNumber *effect in effectsArray) {
        if (effect.integerValue == theFilter){
            [self.audioEffects removeObject:effect];
        }
    }
}

#pragma mark - Video Effects -
/** \brief remove all current working filters
 * *
 *
 */
- (void) removeCurrentFilters
{

}


- (void)repeatPlayback {
    
    self.shouldRepeatPlayback = YES;
    
    if (self.rate == 0)
    {
        [self.currentItem seekToTime:kCMTimeZero];
        [self play];
    }
    
    self.playerItemFinishObserver = [[NSNotificationCenter defaultCenter] addObserverForName:AVPlayerItemDidPlayToEndTimeNotification
                                                                                      object:nil
                                                                                       queue:nil
                                                                                  usingBlock:^(NSNotification *note) {
                                                                                      [self _repeatPlayback];
                                                                                  }];
}

- (void)stopRepeatingPlayback {
    self.shouldRepeatPlayback = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self.playerItemFinishObserver
                                                    name:AVPlayerItemDidPlayToEndTimeNotification
                                                  object:nil];
    self.playerItemFinishObserver = nil;
    [self pause];
}

- (void)repeatPlaybackFrom:(CGFloat)startTime to:(CGFloat)endTime {
    
    self.shouldRepeatPlayback = YES;
    [self.currentItem seekToTime:CMTimeMakeWithSeconds(startTime, 60)];
    [self.currentItem setForwardPlaybackEndTime:CMTimeMakeWithSeconds(endTime, 60)];
    [self setRate:1.0f];
    
    if (self.playerItemFinishObserver != nil) {
        [[NSNotificationCenter defaultCenter] removeObserver:self.playerItemFinishObserver];
        self.playerItemFinishObserver = nil;
    }
    self.playerItemFinishObserver = [[NSNotificationCenter defaultCenter] addObserverForName:AVPlayerItemDidPlayToEndTimeNotification
                                                                                      object:nil // any object can send
                                                                                       queue:nil // the queue of the sending
                                                                                  usingBlock:^(NSNotification *note) {
                                                                                      // holding a pointer to avPlayer to reuse it
                                                                                      if (self.shouldRepeatPlayback) {
                                                                                          [self.currentItem seekToTime:CMTimeMakeWithSeconds(startTime, 44100)];
                                                                                          [self.currentItem setForwardPlaybackEndTime:CMTimeMakeWithSeconds(endTime, 44100)];
                                                                                          [self play];
                                                                                      }
                                                                                  }];
    
}

- (void)_repeatPlayback {
    if (self.shouldRepeatPlayback) {
        [self seekToTime:kCMTimeZero];
        if (self.actionAtItemEnd == AVPlayerActionAtItemEndPause)
        {
            [self play];
        }
    }
}

@end

static void tap_InitCallback(MTAudioProcessingTapRef tap, void *clientInfo, void **tapStorageOut)
{
    AVAudioTapProcessorContext *context = calloc(1, sizeof(AVAudioTapProcessorContext));
    
    // Initialize MTAudioProcessingTap context.
    context->supportedTapProcessingFormat = false;
    context->isNonInterleaved = false;
    context->sampleRate = NAN;
    context->delayUnit = NULL;
    context->reverbUnit = NULL;
    context->processingGraph = NULL;
    context->sampleCount = 0.0f;
    context->leftChannelVolume = 0.0f;
    context->rightChannelVolume = 0.0f;
    context->self = clientInfo;
    
    *tapStorageOut = context;
}

static void tap_FinalizeCallback(MTAudioProcessingTapRef tap)
{
    AVAudioTapProcessorContext *context = (AVAudioTapProcessorContext *)MTAudioProcessingTapGetStorage(tap);
    
    // Clear MTAudioProcessingTap context.
    context->self = NULL;
    
    free(context);
}

static void tap_PrepareCallback(MTAudioProcessingTapRef tap, CMItemCount maxFrames, const AudioStreamBasicDescription *processingFormat)
{
    AVAudioTapProcessorContext *context = (AVAudioTapProcessorContext *)MTAudioProcessingTapGetStorage(tap);
    
    // Store sample rate for -setCenterFrequency:.
    context->sampleRate = processingFormat->mSampleRate;
    
    /* Verify processing format (this is not needed for Audio Unit, but for RMS calculation). */
    
    context->supportedTapProcessingFormat = true;
    
    if (processingFormat->mFormatID != kAudioFormatLinearPCM)
    {
        NSLog(@"Unsupported audio format ID for audioProcessingTap. LinearPCM only.");
        context->supportedTapProcessingFormat = false;
    }
    
    if (!(processingFormat->mFormatFlags & kAudioFormatFlagIsFloat))
    {
        NSLog(@"Unsupported audio format flag for audioProcessingTap. Float only.");
        context->supportedTapProcessingFormat = false;
    }
    
    if (processingFormat->mFormatFlags & kAudioFormatFlagIsNonInterleaved)
    {
        context->isNonInterleaved = true;
    }
    
    /* Create bandpass filter Audio Unit */
    
    AudioUnit delayUnit;
    AudioUnit reverbUnit;
    
    AudioComponentDescription delayComponentDescription;
    delayComponentDescription.componentType = kAudioUnitType_Effect;
    delayComponentDescription.componentSubType = kAudioUnitSubType_Delay;
    delayComponentDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    delayComponentDescription.componentFlags = 0;
    delayComponentDescription.componentFlagsMask = 0;
    
    AudioComponentDescription reverbComponentDescription;
    reverbComponentDescription.componentType = kAudioUnitType_Effect;
    reverbComponentDescription.componentSubType = kAudioUnitSubType_Reverb2;
    reverbComponentDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    reverbComponentDescription.componentFlags = 0;
    reverbComponentDescription.componentFlagsMask = 0;
    
    // Add nodes to the audio processing graph.
    NSLog (@"Adding nodes to audio processing graph");
    
    AUNode   delayNode;         // node for I/O unit
    AUNode   reverbNode;      // node for Multichannel Mixer unit
    AUGraph  processingGraph;
    OSStatus result = noErr;
    result = NewAUGraph (&processingGraph);
    if (noErr != result) {NSLog(@"NewAUGraph");}
    
    // Add the nodes to the audio processing graph
    result =    AUGraphAddNode (
                                processingGraph,
                                &delayComponentDescription,
                                &delayNode);
    
    if (noErr != result) {NSLog(@"AUGraphNewNode failed for delay unit");}
    
    
    result =    AUGraphAddNode (
                                processingGraph,
                                &reverbComponentDescription,
                                &reverbNode
                                );
    
    if (noErr != result) {NSLog(@"AUGraphNewNode failed for reverb unit");}
    
    result = AUGraphOpen (processingGraph);
    
    if (noErr != result) {NSLog(@"AUGraphOpen");}
    
    result =    AUGraphNodeInfo (
                                 processingGraph,
                                 delayNode,
                                 NULL,
                                 &delayUnit
                                 );
    
    if (noErr != result) {NSLog(@"AUGraphNodeInfoDelay");}
    
    result =    AUGraphNodeInfo (
                                 processingGraph,
                                 reverbNode,
                                 NULL,
                                 &reverbUnit
                                 );
    
    if (noErr != result) {NSLog(@"AUGraphNodeInfoReverb");}
    
    
    result = AudioUnitSetProperty(delayUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, processingFormat, sizeof(AudioStreamBasicDescription));
    if (noErr != result) {NSLog(@"AudioUnitSetProperty");}
    
    result = AudioUnitSetProperty(delayUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 0, processingFormat, sizeof(AudioStreamBasicDescription));
    if (noErr != result) {NSLog(@"AudioUnitSetProperty");}
    
    result = AudioUnitSetProperty(reverbUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, processingFormat, sizeof(AudioStreamBasicDescription));
    if (noErr != result) {NSLog(@"AudioUnitSetProperty");}
    
    result = AudioUnitSetProperty(reverbUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 0, processingFormat, sizeof(AudioStreamBasicDescription));
    if (noErr != result) {NSLog(@"AudioUnitSetProperty");}
    
    AURenderCallbackStruct renderCallbackStruct;
    renderCallbackStruct.inputProc = AU_RenderCallback;
    renderCallbackStruct.inputProcRefCon = (void *)tap;
    
    result = AudioUnitSetProperty(delayUnit, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input, 0, &renderCallbackStruct, sizeof(AURenderCallbackStruct));
    if (noErr != result) {NSLog(@"AudioUnitSetProperty");}
    
    result = AudioUnitSetProperty(reverbUnit, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input, 0, &renderCallbackStruct, sizeof(AURenderCallbackStruct));
    if (noErr != result) {NSLog(@"AudioUnitSetProperty");}
    
    UInt32 maximumFramesPerSlice = (UInt32)maxFrames;
    result = AudioUnitSetProperty(delayUnit, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 0, &maximumFramesPerSlice, (UInt32)sizeof(UInt32));
    if (noErr != result) {NSLog(@"AudioUnitSetProperty");}
    
    result = AudioUnitSetProperty(reverbUnit, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 0, &maximumFramesPerSlice, (UInt32)sizeof(UInt32));
    if (noErr != result) {NSLog(@"AudioUnitSetProperty");}
    
    result = AudioUnitSetParameter(delayUnit, kDelayParam_DelayTime, kAudioUnitScope_Global, 0, 0.3, 0); // Global, Cents, 100->12000, 600
    if (noErr != result) {NSLog(@"AudioUnitSetParameter");}
    
    result = AudioUnitSetParameter(delayUnit, kDelayParam_LopassCutoff, kAudioUnitScope_Global, 0, 10000, 0);
    if (noErr != result) {NSLog(@"AudioUnitSetParameter");}
    
    result = AudioUnitSetParameter(reverbUnit, kReverb2Param_DryWetMix, kAudioUnitScope_Global, 0, 50, 0);
    if (noErr != result) {NSLog(@"AudioUnitSetParameter");}
    
    result = AudioUnitSetParameter(reverbUnit, kReverb2Param_MinDelayTime, kAudioUnitScope_Global, 0, 0.2, 0);
    if (noErr != result) {NSLog(@"AudioUnitSetParameter");}
    
    NSLog (@"Initializing the audio processing graph");
    
    result = AudioUnitInitialize(delayUnit);
    if (noErr != result) {NSLog(@"AUDelayGraphInitialize");}
    
    result = AudioUnitInitialize(reverbUnit);
    if (noErr != result) {NSLog(@"AUreverbGraphInitialize");}
    
    context->delayUnit = delayUnit;
    context->reverbUnit = reverbUnit;
    context->processingGraph = processingGraph;
    
    
}

static void tap_UnprepareCallback(MTAudioProcessingTapRef tap)
{
    AVAudioTapProcessorContext *context = (AVAudioTapProcessorContext *)MTAudioProcessingTapGetStorage(tap);
    
    /* Release bandpass filter Audio Unit */
    
    if (context->delayUnit) {
        AudioUnitUninitialize(context->delayUnit);
    }
    
    if (context->reverbUnit) {
        AudioUnitUninitialize(context->reverbUnit);
    }
    
    if (context->processingGraph)
    {
        NSLog (@"Stopping audio processing graph");
        Boolean isRunning = false;
        OSStatus result = AUGraphIsRunning (context->processingGraph, &isRunning);
        if (noErr != result) {NSLog (@"AUGraphIsRunning" );}
        
        if (isRunning) {
            
            result = AUGraphStop (context->processingGraph);
            if (noErr != result) {NSLog(@"AUGraphStop" );}
        }
    }
}



static void tap_ProcessCallback(MTAudioProcessingTapRef tap, CMItemCount numberFrames, MTAudioProcessingTapFlags flags, AudioBufferList *bufferListInOut, CMItemCount *numberFramesOut, MTAudioProcessingTapFlags *flagsOut)
{
    AVAudioTapProcessorContext *context = (AVAudioTapProcessorContext *)MTAudioProcessingTapGetStorage(tap);
    
    OSStatus status;
    
    // Skip processing when format not supported.
    if (!context->supportedTapProcessingFormat)
    {
        NSLog(@"Unsupported tap processing format.");
        return;
    }
    
    BIAVideoPlayer *self = ((__bridge BIAVideoPlayer *)context->self);
    
    if (self.enableAudioEffectDelay || self.enableAudioEffectReverb)
    {
        if (self.enableAudioEffectDelay){
            AudioUnit audioUnit = context->delayUnit;
            if (audioUnit)
            {
                AudioTimeStamp audioTimeStamp;
                audioTimeStamp.mSampleTime = context->sampleCount;
                audioTimeStamp.mFlags = kAudioTimeStampSampleTimeValid;
                
                status = AudioUnitRender(audioUnit, 0, &audioTimeStamp, 0, (UInt32)numberFrames, bufferListInOut);
                if (noErr != status)
                {
                    NSLog(@"AudioUnitRender(): %d", (int)status);
                    return;
                }
                
                // Increment sample count for audio unit.
                context->sampleCount += numberFrames;
                
                // Set number of frames out.
                *numberFramesOut = numberFrames;
            }
        }
        
        if (self.enableAudioEffectReverb){
            AudioUnit audioUnit = context->reverbUnit;
            if (audioUnit)
            {
                AudioTimeStamp audioTimeStamp;
                audioTimeStamp.mSampleTime = context->sampleCount;
                audioTimeStamp.mFlags = kAudioTimeStampSampleTimeValid;
                
                status = AudioUnitRender(audioUnit, 0, &audioTimeStamp, 0, (UInt32)numberFrames, bufferListInOut);
                if (noErr != status)
                {
                    NSLog(@"AudioUnitRender(): %d", (int)status);
                    return;
                }
                
                // Increment sample count for audio unit.
                context->sampleCount += numberFrames;
                
                // Set number of frames out.
                *numberFramesOut = numberFrames;
            }
        }
    }
    else
    {
        // Get actual audio buffers from MTAudioProcessingTap (AudioUnitRender() will fill bufferListInOut otherwise).
        status = MTAudioProcessingTapGetSourceAudio(tap, numberFrames, bufferListInOut, flagsOut, NULL, numberFramesOut);
        if (noErr != status)
        {
            NSLog(@"MTAudioProcessingTapGetSourceAudio: %d", (int)status);
            return;
        }
    }
    
}

#pragma mark - Audio Unit Callbacks

OSStatus AU_RenderCallback(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *ioData)
{
    // Just return audio buffers from MTAudioProcessingTap.
    return MTAudioProcessingTapGetSourceAudio(inRefCon, inNumberFrames, ioData, NULL, NULL, NULL);
}


