//
//  SIANotificationsDataModel.h
//  SingrIphoneApp
//
//  Created by SingrFM on 10/8/13.
//  Copyright (c) 2013 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SIAPaginatorDataModel.h"
#import "SIATextComment.h"


@interface SIANotificationsDataModel : SIAPaginatorDataModel

@property (nonatomic, strong) NSNumber *notification_id;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSAttributedString *textFromHTML;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSDate   *date;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *viewed;
@property (nonatomic, strong) NSNumber *unread;
@property (nonatomic, strong) UIImage  *image;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *image_type;
@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) SIAUser  *user;
@property (nonatomic, strong) SIAVideo *video;

@property (nonatomic, strong) MusicVideo *musicVideo;
@property (nonatomic, strong) SIATextComment *comment;

+ (RKDynamicMapping*) objectMapping;
- (void) setNotificationStatus;
+ (void) setNotoficationStatus:(NSString *)status forNotification:(SIANotificationsDataModel *)notification;
- (void) deleteNotification;

@end
