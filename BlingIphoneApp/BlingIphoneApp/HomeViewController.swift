//
//  HomeViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 15/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
import RestKit
import SDWebImage

protocol HomeTableDelegate: class {
    func videoWasPlayedAtIndex(_ i: Int)
    func pushViewController(_ viewController: UIViewController)
    func presentViewController(_ viewController: UIViewController)
}

class HomeViewController: AutoplayViewController, UITableViewDataSource, HomeTableDelegate, PageRequestHandlerDelegate, VideoUserBarDelegate {
    
    //MARK: Constants
    ///Bar heights
    static let songInfoBarHeight: CGFloat = 65.0
    static let buttonBarHeight: CGFloat = 65.0
    static let videoAspectRatio: CGFloat = (4/3)

    //MARK: Outlets:
    @IBOutlet weak var spinner: SIASpinner!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet var statusBarBackgroundView: UIView!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataReloadButton: UIButton!
    //MARK: Properties
    var videos: [SIAVideo?] = []
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var interactionIsEnabled = true
    var pageRequestHandler: PageRequestHandler?
    var didAppear = false
    var firstPageLoadedInBackground = false
    var homeListWasPreloaded = false
    var deferredDeeplonk: DeepLinkDataModel?

    
    //MARK: View controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //(UIApplication.shared.delegate as! AppDelegate).didLoadHomeScreen = true
        RateTheAppViewController.didOpenHome()
        
        let deeplinkClosure = {[weak self] (deeplink: DeepLinkDataModel?) in
            guard let _ = self else { return }
            (UIApplication.shared.delegate as! AppDelegate).didLoadHomeScreen = true
            if let _ = deeplink {
                (UIApplication.shared.delegate as! AppDelegate).pushDeeplink(deeplink!)
            }
            if !self!.homeListWasPreloaded {
                self!.loadHomeList()
            }
        }
        
        if let _ = deferredDeeplonk {
            deeplinkClosure(deferredDeeplonk)
        }
        else {
            DeepLinkDataModel.checkForDeferredDeeplinks(deeplinkClosure)
        }
    }
    
    func preloadHomeList() {
        homeListWasPreloaded = true
        loadHomeList()
    }
    
    func loadHomeList() {
        Utils.dispatchAsync { [weak self] in
            guard let sSelf = self else { return }
            sSelf.pageRequestHandler = PageRequestHandler(apiPath: "api/v1/home", delegate: sSelf)
            sSelf.pageRequestHandler?.fetchData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController?.setBottomBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        didAppear = true
        super.viewDidAppear(animated)
        navigationController.setSelected(true, forBottomButton: .homeButton)
        if firstPageLoadedInBackground {
            loadFirstPage()
        }
        else {
            spinner?.startAnimation()
        }
        SIAPushRegistrationHandler.shared().homeScreenDidAppear()
        (UIApplication.shared.delegate as! AppDelegate).refreshUser()
        AmplitudeEvents.log(event: "home:pageview")
        
        if RateTheAppViewController.shouldShowOnHome() || RateTheAppViewController.shouldResurface() {
            Utils.dispatchAfter(4) {
                (UIApplication.shared.delegate as! AppDelegate).showRateTheApp()
            }
            
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        interactionIsEnabled = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        noDataReloadButton?.bia_rounded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let recordVC = segue.destination as? SIABlingRecordViewController {
            if segue.identifier == "shootNowSegue" {
                recordVC.musicVideo = (sender as! MusicVideo)
            }
            else if segue.identifier == "recordCameoSegue" {
                guard let video = sender as? SIAVideo else { return }
                recordVC.cameoParent = video
                AmplitudeEvents.log(event: "duet:make",
                                    with: ["pagename":AmplitudeEvents.kHomePageName,
                                           "object_id":video.videoId,
                                           "clip_id": video.clip_id ?? "null",
                                           "artist_name":video.artist_name,
                                           "video_name":video.song_name,
                                           "type":"other"])
            }
        }
        
        if segue.identifier == "cameoInviteSegue" {
            let video = videos[(sender as! UIButton).tag]
            let dest = segue.destination as! InviteContainerViewController
            dest.cameoId = video!.videoId
            dest.cameoVideo = video!
            dest.isDuetInvite = true
            return
        }
        
        if let profileVC = segue.destination as? ProfileViewController {
            profileVC.userId = sender as! String
        }
        
        if let discoverVC = segue.destination as? DiscoverViewController {
            discoverVC.clipID = (sender as? String)
        }
    }
    
    override func segueWasCancelled() {
        interactionIsEnabled = true
    }
    
    func setBackground(with url: URL) {
        SDWebImageManager.shared().loadImage(with: url, options: .refreshCached, progress: nil,
                                             completed: {(image, data, error, cacheType, complete, url) in
                guard let _ = image else { return }
                let blurred = SIAStyling.defaultStyle().blurredImage(from: image, withRadius: 10)
                let image = Utils.changeBrightnessOfImage(blurred!, byValue: -0.05)
                let statusBarBackgroundColor = SIAStyling.defaultStyle().averageColor(from: blurred)
                
                Utils.dispatchOnMainThread {[weak self] in
                    guard let sSelf = self else { return }
                    UIView.transition(
                        with: sSelf.backgroundImageView,
                        duration: 0.3,
                        options: .transitionCrossDissolve,
                        animations: {
                            sSelf.backgroundImageView.image = image
                        },
                        completion: nil
                    )
                    UIView.transition(
                        with: sSelf.statusBarBackgroundView,
                        duration: 0.3,
                        options: .transitionCrossDissolve,
                        animations: {
                            sSelf.statusBarBackgroundView.backgroundColor = statusBarBackgroundColor
                        },
                        completion: nil
                    )
                }
        })

    }
    
    //MARK: Page request methods
    func dataWasLoaded(page: Int, data: Any?, requestIdentifier: Any?) {
        guard let home = data as? HomeResponseModel else {
            pageRequestHandler?.didReachEndOfContent = true
            return
        }
        if page == 0 {
            spinner?.stopAnimation()
            videos = home.video_feed
            if didAppear {
                loadFirstPage()
            }
            else {
                firstPageLoadedInBackground = true
            }
            
        } else {
            if videos.last! == nil {
                videos.removeLast()
            }
            for video in home.video_feed {
                videos.append(video)
            }
            videos.append(nil)
            videoTable.reloadData()
        }
    }
    
    func endOfContentReached(at page: Int, requestIdentifier: Any?) {
        if page == 0 {
            spinner?.stopAnimation()
            noDataView?.isHidden = false
        }
        if let last = videos.last {
            if last == nil {
                videos.removeLast()
                videoTable.reloadData()
            }
        }
    }
    
    func loadFirstPage() {
        firstPageLoadedInBackground = false
        setBackground(with: videos[0]!.thumbnailUrl)
        
        videos.append(nil)
        videoTable.reloadData()
        
        if (videoTable.visibleCells.count > 1 && indexOfPlayingVideo < 0) {
            let topCell = (videoTable.visibleCells[1] as! HomeVideoTableViewCell)
            indexOfPlayingVideo = 0
            topCell.isPlayingVideo = true
            topCell.layoutIfNeeded()
            Utils.dispatchAsync {
                guard let video = topCell.video else { return }
                VideoPlayerManager.shared.setNewPlayerView(view: topCell.videoThumbImage,
                                                           videoURL: (video.videoUrl)!,
                                                           withDelegate: topCell)
            }
            if let banner = topCell.banner {
                banner.hideBanner(after: 5)
            }
            Utils.dispatchInBackground { [weak self] in
                self?.preloadVideosInRange(1, 10)
            }
            
        }
    }
    
    func preloadVideosInRange(_ first: Int, _ last: Int) {
        guard videos.count > first && videos[first] != nil else {
            return
        }
        VideoPlayerAssetManager.shared.downloadFileAt(videos[first]!.videoUrl) {[weak self]
            (filePath, fileExisted) in
            if first + 1 <= last {
                self?.preloadVideosInRange(first + 1, last + (fileExisted ? 1 : 0))
            }
        }
    }
    
    @IBAction func noDataReloadButtonWasTapped(_ sender: Any) {
        spinner?.startAnimation()
        noDataView.isHidden = true
        pageRequestHandler?.page = 0
        pageRequestHandler?.didReachEndOfContent = false
        pageRequestHandler?.fetchData()
    }
    
    
    func removeVideoFromList(_ video: SIAVideo) {
        for (i, v) in videos.enumerated() {
            guard let _ = v else { continue }
            if v!.videoId == video.videoId {
                if indexOfPlayingVideo == i {
                    VideoPlayerManager.shared.removeVideo()
                }
                videos.remove(at: i)
                break
            }
        }
        videoTable.reloadData()
    }
    
    func videoWasDeleted(_ video: SIAVideo) {
        removeVideoFromList(video)
    }
    
    func videoWasMadePrivate(_ video: SIAVideo) {
        removeVideoFromList(video)
    }
    
    func profileButtonWasTapped(_ user: SIAUser) {
        performSegue(withIdentifier: "profile", sender: user.user_id)
    }
    
    func presentUserBarAlert(_ viewController: UIViewController) {
        presentViewController(viewController)
    }
    
    func makeCameoWasTapped(_ video: SIAVideo) {
        performSegue(withIdentifier: "recordCameoSegue",
                     sender: video)
    }
    
    //MARK: Table View Methods
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.section > 0 else {
            return 45.0
        }
        guard let _ = videos[indexPath.section - 1] else {
            return 75
        }
        
        return HomeViewController.songInfoBarHeight + HomeViewController.buttonBarHeight + (UIScreen.main.bounds.width / HomeViewController.videoAspectRatio)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section > 0 else {
            return nil
        }
        let header = VideoUserBarView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 53))
        header.isHomeScreen = true
        header.video = videos[section - 1]
        header.delegate = self
        header.position = section
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard section > 0 else {
            return 0
        }
        return 53
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.section > 0 else {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "topCell", for: indexPath)
            cell.backgroundColor = UIColor.clear
            return cell
        }
        
        guard let video = videos[indexPath.section - 1] else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "loading", for: indexPath) as! SIALoadingTableViewCell
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "video", for: indexPath) as! HomeVideoTableViewCell
        cell.position = indexPath.section - 1
        cell.video = video
        cell.backgroundThumb.image = nil
        cell.videoThumbImage.image = nil
        video.delegate = cell
        
        SDWebImageManager.shared().loadImage(with: video.firstFrameUrl, options: .refreshCached, progress: nil) { (image, data, error, cacheType, completed, url) in
                if url == cell.video?.firstFrameUrl {
                    cell.backgroundThumb.image = image
                    cell.videoThumbImage.image = image
                }
        }
        
        cell.likeButton.setImage(
            UIImage(named:
                video.isLikedByMe ? "icon_liked.png" : "icon_like.png"),
            for: .normal)
        
        cell.homeTableDelegate = self
        
        cell.isPlayingVideo = false
        if (indexPath.section - 1 == indexOfPlayingVideo) {
            cell.isPlayingVideo = true
        } else if (cell.videoThumbImage == VideoPlayerManager.shared.currentPlayerView) {
            //playing cell was recycled (remove the video)
            VideoPlayerManager.shared.removeVideo()
        }
        
        if let banner = cell.banner {
            banner.removeFromSuperview()
        }
        
        if (video.isFeatured()) {
            cell.banner = FeaturedBannerView.addBanner(
                to: cell.videoThumbImage,
                ofType: .featured)
        } else if (video.isDailyPick()) {
            cell.banner = FeaturedBannerView.addBanner(
                to: cell.videoThumbImage,
                ofType: .dailyPick)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return videos.count + 1
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        super.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
        if let _ = cell as? SIALoadingTableViewCell {
            pageRequestHandler?.fetchData()
        }
    }
    
    @IBAction func shootNowButtonWasTapped(_ sender: UIButton) {
        guard interactionIsEnabled else { return }
        guard let video = videos[sender.tag] else { return }

        interactionIsEnabled = false
        //TODO: move this back to prepare for segue
        let musicVideo = MusicVideo(video: video)
        self.performSegue(withIdentifier: "shootNowSegue", sender: musicVideo)
        
        AmplitudeEvents.log(event: "record:shootnow",
                            with: ["pagename": AmplitudeEvents.kHomePageName,
                                   "object_id":video.videoId,
                                   "artist_name":video.artist_name,
                                   "video_name":video.song_name,
                                   "pos":String(sender.tag as Int)]
        )
    }

    //MARK: IBActions
    @IBAction func seeSimilarWasTapped(_ sender: AnyObject) {
        guard interactionIsEnabled else { return }
        guard let video = videos[sender.tag] else { return }

        interactionIsEnabled = false
        let clipID = video.clip_id
        self.performSegue(withIdentifier: "discoverSegue", sender: clipID)
    }
    
    func currentPlayingCell() -> HomeVideoTableViewCell {
        return videoTable.cellForRow(at: IndexPath(row: indexOfPlayingVideo, section: 0)) as! HomeVideoTableViewCell
    }
    
    func videoWasPlayedAtIndex(_ i: Int) {
        indexOfPlayingVideo = i
    }
    
    func pushViewController(_ viewController: UIViewController) {
        guard interactionIsEnabled else { return }
        interactionIsEnabled = false
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func presentViewController(_ viewController: UIViewController) {
        navigationController.present(viewController, animated: true, completion: nil)
    }
}

class HomeVideoTableViewCell: UITableViewCell, VideoPlayerManagerDelegate, SIAVideoDelegate {
    //MARK: Outlets
    @IBOutlet weak var buttonBarHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var songInfoBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoThumbnailHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoInfoToCameoInviteConstraint: NSLayoutConstraint!

    @IBOutlet weak var backgroundThumb: UIImageView!
    
    @IBOutlet weak var shootNowButton: UIButton!
    @IBOutlet weak var seeSimilarButton: UIButton!
    
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var songDataButton: UIButton!
    @IBOutlet weak var cameoInviteButton: UIButton!
    
    @IBOutlet weak var downloadingSpinner: SIASpinner!
    @IBOutlet weak var videoThumbImage: UIImageView!
    @IBOutlet weak var cameoIcon: UIImageView!
    
    @IBOutlet weak var cameoInviteButtonContainer: UIView!
    //MARK: Properties
    weak var video: SIAVideo?
    var timer: Timer?
    weak var homeTableDelegate: HomeTableDelegate?
    var isPlayingVideo = false
    var didSendVideoView = false
    var banner: FeaturedBannerView?
    var position: Int? {
        didSet {
            shootNowButton.tag = position!
            seeSimilarButton.tag = position!
            songDataButton.tag = position!
            cameoInviteButton.tag = position!
        }
    }
    
    //MARK: View methods
    override func layoutSubviews() {
        super.layoutSubviews()
        if let _ = video {
            songNameLabel.text = video!.song_name
            artistNameLabel.text = video!.artist_name
            likeButton.setTitle(video!.totalLikesStr,
                                for: .normal)
            commentButton.setTitle(video!.totalCommentsStr,
                                   for: .normal)
            cameoIcon.isHidden = !video!.isCameo()
            
            if video!.isMyVideo() && !video!.isCameo() {
                cameoInviteButtonContainer.isHidden = false
                videoInfoToCameoInviteConstraint.isActive = true
            }
            else {
                cameoInviteButtonContainer.isHidden = true
                videoInfoToCameoInviteConstraint.isActive = false
            }
        }
        
        buttonBarHeightContraint.constant = HomeViewController.buttonBarHeight
        songInfoBarHeightConstraint.constant = HomeViewController.songInfoBarHeight
        videoThumbnailHeightConstraint.constant = UIScreen.main.bounds.width / HomeViewController.videoAspectRatio
        
        
        shootNowButton.bia_rounded()
    }
    
    
    func playPauseVideo() {
        timer = nil
        AmplitudeEvents.shared.pendingView = nil
        guard let video = video else { return }
        if (VideoPlayerManager.shared.isPlaying(video.videoUrl, on: videoThumbImage )) {
            VideoPlayerManager.shared.player.pause()
            isPlayingVideo = false
        } else {
            isPlayingVideo = true
            didSendVideoView = false
            VideoPlayerManager.shared.setNewPlayerView(view: videoThumbImage, videoURL: video.videoUrl, withDelegate: self)
            if let banner = banner {
                banner.hideBanner(after: 5)
            }
        }
    }
    
    func playerDidReachEnd() {
        if isPlayingVideo && VideoPlayerManager.shared.playerIsInsideTopViewController() {
            VideoPlayerManager.shared.player.play()
        }
    }
    
    func playerDidPlay() {
        guard let _ = video, !didSendVideoView else { return }
        didSendVideoView = true
        AmplitudeEvents.shared.logVideoView(videoID: video!.videoId,
                                            pageName: AmplitudeEvents.kHomePageName,
                                            position: String(position!),
                                            videoName: video!.song_name,
                                            artistName: video!.artist_name)
    }
    
    func performLike(canUnlike: Bool = true) {
        guard let video = video else {
            return
        }
        guard !video.videoIsBeingLiked else {
            return
        }
        
        if (!canUnlike || !video.isLikedByMe) {
            LikeAnimator.likeAnimation(on: videoThumbImage)
        }
        
        if (video.isLikedByMe && !canUnlike) {
            return // cant unlike by double tap
        }
        
        Utils.dispatchOnMainThread({[weak self] in
            guard let sSelf = self else {
                return
            }
            if (sSelf.video?.isLikedByMe)! {
                //change to white like image
                sSelf.video?.total_likes = String(Int((sSelf.video?.total_likes)!)! - 1)
                sSelf.likeButton.setImage(UIImage(named:"icon_like.png"), for: .normal)
            } else {
                //change to red like image
                sSelf.likeButton.setImage(UIImage(named:"icon_liked.png"), for: .normal)
               sSelf.video?.total_likes = String(Int((sSelf.video?.total_likes)!)! + 1)
            }
            sSelf.likeButton.setTitle(sSelf.video?.totalLikesStr, for: .normal)
        })
        
        video.toggleLike(completion: nil,
                         shouldChangeLikesValue: false,
                         withAmplitudePageName:  AmplitudeEvents.kHomePageName)
    }
    

    
    @IBAction func commentButtonWasTapped(_ sender: AnyObject) {
        let listVC = UIStoryboard.init(name: "VideoHomeStoryboard", bundle: nil).instantiateViewController(withIdentifier: "videoLists") as! VideoHomeListsViewController
        listVC.background = backgroundThumb.image
        listVC.video = video!
        listVC.tab = listVC.kCommentTab
        homeTableDelegate?.pushViewController(listVC)
    }
    
    @IBAction func shareButtonWasTapped(_ sender: AnyObject) {
        let shareVC = UIStoryboard.init(name: "SIAShare", bundle: nil).instantiateInitialViewController() as! SIAShareSheetViewController
        shareVC.video = video!
        shareVC.objectId = video!.videoId
        shareVC.objectType = "video"
        shareVC.modalPresentationStyle = .overCurrentContext
        shareVC.modalTransitionStyle = .crossDissolve
        
        if (isPlayingVideo) {
            VideoPlayerManager.shared.removeVideo()
        }
        
        homeTableDelegate?.presentViewController(shareVC)
    }
    
    @IBAction func videoWasTapped(_ sender: AnyObject) {
        if let _ = timer {
            timer?.invalidate()
            timer = nil
            performLike(canUnlike: false)
        } else {
            timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(HomeVideoTableViewCell.playPauseVideo), userInfo: nil, repeats: false)
        }
    }
    
    @IBAction func likeButtonWasTapped(_ sender: AnyObject) {
        performLike()
    }
    
    func videoTotalsDidChange(_ video: SIAVideo) {
        commentButton.setTitle(video.totalCommentsStr,
                               for: .normal)
        likeButton.setTitle(video.totalLikesStr,
                            for: .normal)
    }
}

