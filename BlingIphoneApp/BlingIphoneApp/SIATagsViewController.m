//
//  SIATagsViewController.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/10/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIATagsViewController.h"
#import "SIATagTableViewCell.h"
#import "SIAVideoCategoryCell.h"
#import "SIALoadingTableViewCell.h"

typedef NS_ENUM(NSInteger, SIATagsRequirement) {
    SIATagsRequirementCategory = 1236,
    SIATagsRequirementArtist,
    SIATagsRequirementTitle,
    SIATagsRequirementTags,
    SIATagsRequirementMultipleFields,
    SIATagsRequirementNone
};

static NSString *categoryCellID = @"categoryCell";
static NSString *videoTitleCellID = @"titleCell";
static NSString *originalArtistCellID = @"artistCell";
static NSString *tagCellID = @"tagCell";
static SIAStaticTagFields staticFields = SIACategoryField | SIATitleField | SIAArtistField;

@interface SIATagsViewController () <UITextFieldDelegate>

@property (nonatomic) BOOL showcategoriesTable;
@property (nonatomic) NSUInteger selectedCategoryIndex;
@property (nonatomic) NSUInteger numberOfRemovedStaticCells;
@property (nonatomic, strong) NSIndexPath *indexOfCurrentTextField;
@property (nonatomic) CGPoint originalOffset;

@property (nonatomic) BOOL categoryWasChosen;

@end

@implementation SIATagsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIAVideoCategoriesDataModel loadCategories:^(SIAVideoCategoriesDataModel *dataModel){
        [Utils dispatchAfter:3.0 closure: ^{
            self.categoriesModel = dataModel;
            if (_showcategoriesTable)
                [self showCategoriesTableAnimationAfterLoading:YES];
            [self setCategoryFromRecordType];
        }];
    }];
    
    if (self.recordingItem.artist_name.length < 1)
        self.recordingItem.removeArtistTag = YES; // REMOVE THIS FOR NOW BECAUSE OF DUMB SPEC
    
    if (self.recordingItem.removeArtistTag) {
        staticFields &= ~(SIAArtistField);
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController clearNavigationItemRightButton];
    [self.navigationController clearNavigationItemLeftButton];
    [self.navigationController setNavigationBarLeftButtonImage:[UIImage imageNamed:@"back.png"]
                                                        target:self
                                                        action:@selector(back)];
    if (self.recordingItem.isPhoto) {
        [self.navigationItem setTitle:@"Tag Photo"];
    }
    else {
        [self.navigationItem setTitle:@"Tag Video"];
    }
    SEL doneButtonSelector;
    if (self.tagsViewType == SIATagsViewTypePostRecord)
        doneButtonSelector = @selector(next);
    else
        doneButtonSelector = @selector(back);
    [self.navigationController setNavigationBarRightButtonDoneTarget:self
                                                              action:doneButtonSelector];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    
    // If user deletes one of these fields but then choose a song type that has the fields locked in, this re-adds them
    if (self.recordingItem.lockedFields & SIALockTitle) {
        staticFields |= SIATitleField;
    }
    if (self.recordingItem.lockedFields & SIALockArtist) {
        staticFields |= SIAArtistField;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    [self.view sendSubviewToBack:self.tagsTable];
    [self.view bringSubviewToFront:self.addTagButton];
    
    //[Flurry logEvent:FLURRY_VIEW_TAGS];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)next {
    
    if ([self metadataRequirementsMet])
    {
        if (!(self.recordingItem.lockedFields & SIALockCategory)) // this prevents changing the recordtype if it was pre-set
            self.recordingItem.recordType = self.categoriesModel.categories[_selectedCategoryIndex].types.firstObject;
        self.recordingItem.artist_name = self.originalArtist ?: CURRENT_USER_FULL_NAME ;
        self.recordingItem.song_name = self.videoTitle;
        self.recordingItem.tags = self.tagsArray;
        if ([self.delegate respondsToSelector:@selector(recordingItemWasChanged:)])
        {
            [self.delegate recordingItemWasChanged:self.recordingItem];
        }
        [super next];
    }
}

- (void)back {
    
    if (!(self.recordingItem.lockedFields & SIALockCategory) && _categoryWasChosen)
        self.recordingItem.recordType = self.categoriesModel.categories[_selectedCategoryIndex].types.firstObject;
    self.recordingItem.artist_name = self.originalArtist;
    self.recordingItem.song_name = self.videoTitle;
    self.recordingItem.tags = self.tagsArray;

    if (self.tagsViewType == SIATagsViewTypePostRecord)
    {
        if ([self.delegate respondsToSelector:@selector(back)])
        {
            [self.delegate back];
        }
    }
        
    else
    {
        [self dismissViewControllerAnimated:YES
                                 completion:nil];
        if (self.completionHandler)
            self.completionHandler(self.recordingItem);
    }
    
    //[Flurry logEvent:FLURRY_TAP_BACK_TAGS];
}

- (void)setRecordingItem:(SIARecordingItem *)recordingItem {
    _recordingItem = recordingItem;
    self.videoTitle = recordingItem.song_name;
    self.originalArtist = recordingItem.artist_name;
    if (recordingItem.tags) {
        self.tagsArray = recordingItem.tags.mutableCopy;
    }
    else {
        //self.tagsArray = [NSMutableArray arrayWithObject:@""]; removing the initial empty "custom tag" field for now
        self.tagsArray = [NSMutableArray array];
    }
}

#pragma mark Table View Delegate 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.tagsTable)
    {
      return [self cellForTagsTableAtIndexPath:indexPath];
    }
    else
    {
        return [self cellForCategoriesTableAtIndexPath:indexPath];
    }
}

- (SIATagTableViewCell *)cellForTagsTableAtIndexPath:(NSIndexPath *)indexPath {
    SIATagTableViewCell *cell = [self.tagsTable dequeueReusableCellWithIdentifier:[self cellIdentifierForRow:indexPath.row]];
    
    NSInteger tagIndex = indexPath.row - [self numberOfStaticCells];
    NSInteger switchIndex = indexPath.row;
    switchIndex = switchIndex == SIAStaticTagFieldTitle ? [self titleFieldIndex] : switchIndex;
    switchIndex = switchIndex == SIAStaticTagFieldArtist ? [self artistFieldIndex] : switchIndex;
    
    if (indexPath.row > 0)
    {
        cell.tagField.delegate = self;
        cell.tagField.tag = indexPath.row;
    }
    switch (switchIndex) {
        case SIAStaticTagFieldCategory: { // category cell
            if (self.recordingItem.lockedFields & SIALockCategory) {
                cell.userInteractionEnabled = NO;
            }
            UILabel *categoryLabel = ((SIAVideoCategoryCell *)cell).categoryLabel;
            categoryLabel.text = _categoryWasChosen ? self.categoriesModel.categories[_selectedCategoryIndex].label : [NSString string];
            categoryLabel.textColor = [UIColor whiteColor];
            categoryLabel.userInteractionEnabled = YES;
            categoryLabel.accessibilityLabel = @"recorder_button_category";
            if (categoryLabel.text.length < 1) {
                categoryLabel.text = @"Select category...";
                categoryLabel.textColor = [UIColor colorWithWhite:1.0 alpha:0.7];
            }
            UIView *categoryBackground = ((SIAVideoCategoryCell *)cell).categoryBackground;
            if (categoryBackground.gestureRecognizers.count < 1)
            {
                [categoryBackground addGestureRecognizer:[self categoryLabelTapRecognizer]];
            }
            break;
        }
        case SIAStaticTagFieldTitle: { // title cell
            if (self.recordingItem.lockedFields & SIALockTitle)
            {
                cell.tagField.userInteractionEnabled = NO;
                staticFields |= SIATitleField;
            }
            cell.tagField.text = self.videoTitle;
            [self adjustTextFieldWidth:cell.tagField];
            break;
        }
        case SIAStaticTagFieldArtist: { // original artist cell
            if (self.recordingItem.lockedFields & SIALockArtist)
            {
                cell.tagField.userInteractionEnabled = NO;
                staticFields |= SIAArtistField;
            }
            else
            {
                cell.tagField.rightView = [self tagCellXButton];
                cell.tagField.rightViewMode = UITextFieldViewModeAlways;
            }
            cell.tagField.text = self.originalArtist;
            [self adjustTextFieldWidth:cell.tagField];
            break;
        }
        default: { // tag cells
            cell.tagField.text = self.tagsArray[tagIndex];
            cell.tagField.placeholder = @"#CustomTag";
            if ([self.recordingItem.lockedTagIndexes containsIndex:tagIndex])
            {
                cell.tagField.userInteractionEnabled = NO; // do not allow user to change "locked tags" like "#Karaoke"
            }
            else
            {
                cell.tagField.rightView = [self tagCellXButton];
                cell.tagField.rightViewMode = UITextFieldViewModeAlways;
            }
            [self adjustTextFieldWidth:cell.tagField];
            break;
        }
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (UIButton *)tagCellXButton {
    UIButton *xButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [xButton setImage:[UIImage imageNamed:@"remove_tag.png"]
             forState:UIControlStateNormal];
    xButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 24);
    [xButton sizeToFit];

    [xButton addTarget:self
                action:@selector(tagCellXButtonWasTapped:)
      forControlEvents:UIControlEventTouchUpInside];
    
    return xButton;
}

- (void)tagCellXButtonWasTapped:(UIButton *)xButton {

    NSIndexPath *indexPath = [self.tagsTable indexPathForRowAtPoint:[xButton convertPoint:xButton.superview.frame.origin
                                                                                   toView:self.tagsTable]];
    [self removeTagAtIndexPath:indexPath];
    //[Flurry logEvent:FLURRY_TAP_DELETETAG_TAGS];
}

- (void)removeTagAtIndexPath:(NSIndexPath *)indexPath {
    SIATagTableViewCell *cell = [self.tagsTable cellForRowAtIndexPath:indexPath];
    if ([cell.tagField isFirstResponder]) {
        [cell.tagField resignFirstResponder];
    }
    
    if (indexPath.row == [self artistFieldIndex])
    {
        staticFields &= ~ SIAArtistField;
        self.recordingItem.removeArtistTag = YES;
        self.originalArtist = nil;
        [self.tagsTable deleteRowsAtIndexPaths:@[indexPath]
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (self.tagsArray.count > indexPath.row - [self numberOfStaticCells])
    {
        [self.tagsArray removeObjectAtIndex:indexPath.row - [self numberOfStaticCells]];
        [self.tagsTable deleteRowsAtIndexPaths:@[indexPath]
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    
}

- (UITableViewCell *)cellForCategoriesTableAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.categoriesModel.categories.count == 0) {
        SIALoadingTableViewCell *loadingCell = [self.categoriesTable dequeueReusableCellWithIdentifier:@"loadingCell"];
        return loadingCell;
    }
    
    SIAVideoCategoryCell *cell = [self.categoriesTable dequeueReusableCellWithIdentifier:@"categoryCell"];
    
    cell.categoryLabel.text = self.categoriesModel.categories[indexPath.row].label;
    
    // round bottom corners of last cell
    if (indexPath.row == self.categoriesModel.categories.count - 1)
    {
        [self roundBottomCorners:cell];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.categoriesTable)
    {
        self.selectedCategoryIndex = indexPath.row;
        self.categoryWasChosen = YES;
        [self hidecategoriesTable];
        [self.tagsTable reloadRowsAtIndexPaths:@[[self categoryCellIndexPath]]
                              withRowAnimation:UITableViewRowAnimationNone];
//        [Flurry logEvent:FLURRY_TAP_CATEGORY_TAGS
//          withParameters:@{@"category":self.categoriesModel.categories[_selectedCategoryIndex].label}];
    }
}


- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.categoriesTable && !_showcategoriesTable) {
        return 0;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tagsTable)
        return [self numberOfStaticCells] + self.tagsArray.count;
    else if (_showcategoriesTable)
        return self.categoriesModel.categories.count ?: 1;
    return 0;
}

- (NSString *)cellIdentifierForRow:(NSInteger)row {
    switch (row) {
        case 0:
            return categoryCellID;
            break;
        case 1:
            return videoTitleCellID;
            break;
        case 2:
            return originalArtistCellID;
            break;
        default:
            return tagCellID;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == self.categoriesTable)
        return 45 / 2; // the height of the table view cell rounded backgrounds / 2 for padding from mid-point.
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView == self.categoriesTable)
    {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                      0,
                                                                      tableView.frame.size.width,
                                                                      [self tableView:tableView heightForHeaderInSection:section])];
        headerView.alpha = 0;
        
        return headerView;
    }
    return nil;
}

- (NSIndexPath *)categoryCellIndexPath {
    return [NSIndexPath indexPathForRow:0 inSection:0];
}

#pragma mark Text Field Delegate

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (_showcategoriesTable)
        [self hidecategoriesTable];
    
    CGPoint fieldPoint = [textField convertPoint:textField.center
                                          toView:self.tagsTable];
    NSIndexPath *indexPath = [self.tagsTable indexPathForRowAtPoint:fieldPoint];
    self.indexOfCurrentTextField = indexPath;
    if (textField.text.length == 0 && textField.tag >= [self numberOfStaticCells]) {
        textField.text = @"#";
        [self adjustTextFieldWidth:textField];
    }
    
    NSInteger switchIndex = indexPath.row;
    switchIndex = switchIndex == SIAStaticTagFieldTitle ? [self titleFieldIndex] : switchIndex;
    switchIndex = switchIndex == SIAStaticTagFieldArtist ? [self artistFieldIndex] : switchIndex;
    
//    switch (switchIndex) {
//        case SIAStaticTagFieldTitle:
//            [Flurry logEvent:FLURRY_TAP_TITLE_TAGS];
//            break;
//        case SIAStaticTagFieldArtist:
//            [Flurry logEvent:FLURRY_TAP_ARTIST_TAGS];
//            break;
//        default:
//            [Flurry logEvent:FLURRY_TAP_CUSTOMTAG_TAGS];
//            break;
//    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    CGPoint fieldPoint = [textField convertPoint:textField.center
                                          toView:self.tagsTable];
    NSIndexPath *indexPath = [self.tagsTable indexPathForRowAtPoint:fieldPoint];
    NSInteger switchIndex = indexPath.row;
    switchIndex = switchIndex == SIAStaticTagFieldTitle ? [self titleFieldIndex] : switchIndex;
    switchIndex = switchIndex == SIAStaticTagFieldArtist ? [self artistFieldIndex] : switchIndex;
    switch (switchIndex) {
        case SIAStaticTagFieldTitle:
            self.videoTitle = textField.text;
            break;
        case SIAStaticTagFieldArtist:
            self.originalArtist = textField.text;
            break;
        default:
            if (textField.text.length > 1)
            {
                [self.tagsArray replaceObjectAtIndex:indexPath.row - [self numberOfStaticCells]
                                          withObject:textField.text];
            }
            else
            {
                [self removeTagAtIndexPath:indexPath];
            }
            break;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {    
    // 35 char limit!
    CGFloat stringWidth = [string sizeWithAttributes:@{NSFontAttributeName:textField.font}].width;
    CGPoint textFieldOrigin = [textField convertPoint:textField.frame.origin toView:self.view];
    CGFloat maxTextFieldWidth = SCREEN_WIDTH - textFieldOrigin.x * 2;
    CGFloat newTextFieldWidth = textField.frame.size.width  + stringWidth;
    if ((textField.text.length >= 100 || newTextFieldWidth >= maxTextFieldWidth) && string.length > 0)
    {
        return NO;
    }
    
    if (range.location == 0 && string.length > 0 && textField.tag >= [self numberOfStaticCells]) {
        textField.text = [@"#" stringByAppendingString:string];
        [self adjustTextFieldWidth:textField];
        return NO;
    }
    return YES;
}

- (void)textFieldTextDidChange:(NSNotification *)notification {
    UITextField *textField = notification.object;
    NSIndexPath *indexPath = [self indexPathForTextField:textField];
    NSUInteger switchIndex = indexPath.row;
    switchIndex = switchIndex == SIAStaticTagFieldTitle ? [self titleFieldIndex] : switchIndex;
    switchIndex = switchIndex == SIAStaticTagFieldArtist ? [self artistFieldIndex] : switchIndex;
    [self adjustTextFieldWidth:textField];
    
    switch (switchIndex) {
        case SIAStaticTagFieldTitle:
            self.videoTitle = textField.text;
            break;
        case SIAStaticTagFieldArtist:
            self.originalArtist = textField.text;
            break;
        default:
            [self.tagsArray replaceObjectAtIndex:indexPath.row - [self numberOfStaticCells]
                                      withObject:textField.text];
            break;
    }
}

- (NSIndexPath*)indexPathForTextField:(UITextField*)textField
{
    CGPoint fieldPoint = [textField convertPoint:textField.center
                                          toView:self.tagsTable];
    NSIndexPath *indexPath = [self.tagsTable indexPathForRowAtPoint:fieldPoint];

    return indexPath;
}

- (void)adjustTextFieldWidth:(UITextField *)textField {
    
    CGSize textSize = [textField.text sizeWithAttributes:@{NSFontAttributeName:textField.font}];
    
    if (textSize.width == 0) {
        textSize = [textField.placeholder sizeWithAttributes:@{NSFontAttributeName:textField.font}];
    }
    
    CGRect textFieldRect = textField.frame;
    CGFloat rightViewAdjustment = textField.rightView.bounds.size.width;
    textFieldRect.size.width = textSize.width + rightViewAdjustment + 46; // 46 for 23px padding on left and right
    
    CGRect rightViewRect = textField.rightView.frame;
    rightViewRect.origin.x += textFieldRect.size.width - textField.frame.size.width; // move the button by the amount width changes
    
    if(textFieldRect.size.width > self.tagsTable.frame.size.width)
        return;
    
    [Utils dispatchOnMainThread:^{
        [UIView animateWithDuration:0.25
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             textField.frame = textFieldRect;
                             if (!CGPointEqualToPoint(rightViewRect.origin,
                                                     CGPointZero))
                             {
                                 textField.rightView.frame = rightViewRect;
                             }
                         } completion:nil];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)addTag:(id)sender {
    
    if (_showcategoriesTable)
        [self hidecategoriesTable];
    
    if (self.tagsArray.count > 0 && self.tagsArray.lastObject.length < 1)  // if last tag cell is empty, start editing instead of adding a new cell
    {
//        [Flurry logEvent:FLURRY_TAP_ADDTAG_TAGS];
        NSIndexPath *indexOfEmptyTagCell = [NSIndexPath indexPathForRow:self.tagsArray.count + [self numberOfStaticCells] - 1
                                                              inSection:0];
        // check to see if row is not visible, and therefore the textfield is not available
        [UIView animateWithDuration:0.25
                         animations:^{
                             [self.tagsTable scrollRectToVisible:[self.tagsTable rectForRowAtIndexPath:indexOfEmptyTagCell]
                                                        animated:NO];
                         } completion:^(BOOL finished) {
                             SIATagTableViewCell *cell = [self.tagsTable cellForRowAtIndexPath:indexOfEmptyTagCell];
                             [cell.tagField becomeFirstResponder];
                         }];
        return;
    }
    
    [self.tagsArray addObject:@""];
    NSIndexPath *indexOfNewTagCell = [NSIndexPath indexPathForRow:self.tagsArray.count + [self numberOfStaticCells] - 1
                                                        inSection:0];
    NSIndexPath *indexOfLastTagCell = [NSIndexPath indexPathForRow:indexOfNewTagCell.row - 1
                                                         inSection:0];
    CGRect frameOfLastCell = [self.tagsTable rectForRowAtIndexPath:indexOfLastTagCell];
    frameOfLastCell = [self.tagsTable convertRect:frameOfLastCell
                                           toView:self.view];
    CGFloat distanceToBottomOfScreen = self.view.frame.size.height - CGRectGetMaxY(frameOfLastCell);
    
   // [self.tagsTable beginUpdates];
    [CATransaction begin]; // this guarantees the first responder call happens AFTER the cell insert animation is completed.
    [CATransaction setCompletionBlock:^{
        if (distanceToBottomOfScreen < frameOfLastCell.size.height)
        {
            [UIView animateWithDuration:0.25
                             animations:^{
                                 [self.tagsTable scrollToRowAtIndexPath:indexOfNewTagCell
                                                       atScrollPosition:UITableViewScrollPositionBottom
                                                               animated:NO];
                             } completion:^(BOOL finished) {
                                 SIATagTableViewCell *cell = [self.tagsTable cellForRowAtIndexPath:indexOfNewTagCell];
                                 [cell.tagField becomeFirstResponder];
                             }];
        }
        else
        {
            SIATagTableViewCell *cell = [self.tagsTable cellForRowAtIndexPath:indexOfNewTagCell];
            [cell.tagField becomeFirstResponder];
        }
    }];
    [self.tagsTable insertRowsAtIndexPaths:@[indexOfNewTagCell]
                          withRowAnimation:UITableViewRowAnimationFade];
    [CATransaction commit];
}

- (void) keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    self.originalOffset = self.tagsTable.contentOffset;
    
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect tableFrameMinusKeyboard = self.tagsTable.frame;
    tableFrameMinusKeyboard.size.height -= keyboardFrame.size.height;
    
    CGRect activeCellFrame = [self.tagsTable rectForRowAtIndexPath:self.indexOfCurrentTextField];
    activeCellFrame = [self.tagsTable convertRect:activeCellFrame
                                           toView:self.view];
    CGFloat bottomOfActiveCell = CGRectGetMaxY(activeCellFrame);
    CGFloat amountToMoveContent = bottomOfActiveCell - tableFrameMinusKeyboard.size.height;
    CGPoint contentOffset = CGPointMake(0, self.tagsTable.contentOffset.y + amountToMoveContent);
    
    [Utils dispatchOnMainThread:^{
        if (amountToMoveContent > 0) {
            [self.tagsTable setContentOffset:contentOffset
                                    animated:YES];
        }
    }];
}

- (void) keyboardWillHide:(NSNotification *)notification {
    [Utils dispatchOnMainThread:^{
        [self.tagsTable setContentOffset:self.originalOffset
                                animated:YES];
    }];
}

- (void)resignActiveTextField {
    UITextField *activeTextField = ((SIATagTableViewCell *)[self.tagsTable cellForRowAtIndexPath:_indexOfCurrentTextField]).tagField;
    [activeTextField resignFirstResponder];
}

#pragma mark Categories Table

- (UITapGestureRecognizer *)categoryLabelTapRecognizer {
    return  [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(toggleCategoriesMenu:)];
}

- (void)toggleCategoriesMenu:(UITapGestureRecognizer *)tapRecognizer {
    if (self.showcategoriesTable) {
        [self hidecategoriesTable];
    }
    else {
        [self showCategoriesTableUnderView:tapRecognizer.view];
    }
}

- (void) showCategoriesTableUnderView:(UIView *)view {
//    [Flurry logEvent:FLURRY_TAP_CATEGORIES_TAGS];

    _showcategoriesTable = YES;
    
    [self resignActiveTextField];
    
//    CGRect categoryCellFrame = [self.view convertRect:[self.tagsTable rectForRowAtIndexPath:indexPath]
//                                             fromView:self.tagsTable];
    
    CGRect adjustedCellFrame = [self.tagsTable convertRect:view.frame
                                                    toView:self.view];
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *cellImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGRect categoriesTableFrame = self.categoriesTable.frame;
    categoriesTableFrame.origin.x = adjustedCellFrame.origin.x;
    categoriesTableFrame.origin.y = CGRectGetMidY(adjustedCellFrame);
    self.categoriesTable.frame = categoriesTableFrame;
    
    UIImageView *cellCopyImageView = [[UIImageView alloc] initWithImage:cellImage];
    cellCopyImageView.userInteractionEnabled = NO;
    cellCopyImageView.tag = 1101;
    cellCopyImageView.frame = adjustedCellFrame;
    
    CGRect backgroundFrame = categoriesTableFrame;
    backgroundFrame.size.height = 0;
    UIView *backgroundView = [[UIView alloc] initWithFrame:backgroundFrame];
    backgroundView.backgroundColor = [UIColor colorFromHexString:@"202020"];
    backgroundView.layer.cornerRadius = 8.0;
    backgroundView.alpha = 0;
    backgroundView.tag = 1102;
    
    [self.view addSubview:self.categoriesTable];
    [self.view addSubview:cellCopyImageView];
    [self.view insertSubview:backgroundView
                belowSubview:self.categoriesTable];
    
    [self showCategoriesTableAnimationAfterLoading:NO];
    
    self.tagsTable.scrollEnabled = NO;
    
    
}

- (void) hidecategoriesTable
{
    _showcategoriesTable = NO;
    [self.categoriesTable deleteSections:[NSIndexSet indexSetWithIndex:0]
                        withRowAnimation:UITableViewRowAnimationBottom];
    UIView *backgroundView = [self.view viewWithTag:1102];
    CGRect backgroundViewFrame = backgroundView.frame;
    backgroundViewFrame.size.height = 0;
    [UIView animateWithDuration:0.33
                     animations:^{
                         self.categoriesTable.alpha = 0;
                         backgroundView.alpha = 0;
                         backgroundView.frame = backgroundViewFrame;
                     } completion:^(BOOL finished) {
                         [[self.view viewWithTag:1101] removeFromSuperview];
                         [backgroundView removeFromSuperview];
                         self.tagsTable.scrollEnabled = YES;
                     }];
}

- (void)showCategoriesTableAnimationAfterLoading:(BOOL)afterLoading {
    
    
    [CATransaction begin];
    [self.categoriesTable beginUpdates];
    if (afterLoading)
        [self.categoriesTable reloadSections:[NSIndexSet indexSetWithIndex:0]
                            withRowAnimation:UITableViewRowAnimationAutomatic];
    else
        [self.categoriesTable insertSections:[NSIndexSet indexSetWithIndex:0]
                            withRowAnimation:UITableViewRowAnimationTop];
    UIView *backgroundView = [self.view viewWithTag:1102];
    CGRect backgroundFrame = backgroundView.frame;
    if (self.categoriesModel.categories.count)
    {
        backgroundFrame.size.height = self.categoriesModel.categories.count * 45 + 45/2;
    }
    else backgroundFrame.size.height = 45 + 45/2;
    [UIView animateWithDuration:0.33
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.categoriesTable.alpha = 1;
                         backgroundView.alpha = 1;
                         backgroundView.frame = backgroundFrame;
                     }
                     completion:nil];
    [self.categoriesTable endUpdates];
    [CATransaction commit];
}

- (BOOL) metadataRequirementsMet {
    
    return YES;
    
    SIATagsRequirement requirement = SIATagsRequirementNone;
    
    NSInteger missingRequirementsCount = 0;
    
    if (!self.categoryWasChosen)
    {
        requirement = SIATagsRequirementCategory;
        missingRequirementsCount++;
    }
//    if (self.originalArtist.length < 1) {
//        if (missingRequirementsCount > 0)
//        {
//            requirement = SIATagsRequirementMultipleFields;
//        }
//        else
//        {
//            requirement = SIATagsRequirementArtist;
//            missingRequirementsCount ++;
//        }
//    }
//    if (self.videoTitle.length < 1) {
//        if (missingRequirementsCount > 0)
//        {
//            requirement = SIATagsRequirementMultipleFields;
//        }
//        else
//        {
//            requirement = SIATagsRequirementTitle;
//            missingRequirementsCount ++;
//        }
//    }
//    if (self.tagsArray.count < 1 && self.originalArtist.length < 1) {
//        if (missingRequirementsCount > 0)
//        {
//            requirement = SIATagsRequirementMultipleFields;
//        }
//        else
//        {
//            requirement = SIATagsRequirementTags;
//            missingRequirementsCount ++;
//        }
//    }
    
    if (requirement != SIATagsRequirementNone)
    {
        [self showAlert:requirement];
    }
    
    return missingRequirementsCount == 0;
}

- (void)showAlert:(SIATagsRequirement)tagRequirement {
   
    NSString *alertTitle;
    NSString *alertMessage = @"Please %@ before continuing";
    
    switch (tagRequirement) {
        case SIATagsRequirementCategory:
            alertTitle = @"No category";
            alertMessage = [NSString stringWithFormat:alertMessage, @"choose a category"];
            break;
        case SIATagsRequirementTitle:
            alertTitle = @"No title";
            alertMessage = [NSString stringWithFormat:alertMessage, @"enter a title"];
            break;
        case SIATagsRequirementArtist:
            alertTitle = @"No artist";
            alertMessage = [NSString stringWithFormat:alertMessage, @"enter an artist name"];
        case SIATagsRequirementTags:
            alertTitle = @"Not enough tags";
            alertMessage = [NSString stringWithFormat:alertMessage, @"add more tags"];
        case SIATagsRequirementMultipleFields:
            alertTitle = @"Not enough info";
            alertMessage = [NSString stringWithFormat:alertMessage, @"enter more information"];
        default:
            alertTitle = @"Not enough info";
            alertMessage = [NSString stringWithFormat:alertMessage, @"enter more information"];
            break;
    }
    
    
    SIAAlertController *noCategoryAlert = [SIAAlertController alertControllerWithTitle:alertTitle
                                                                               message:alertMessage];
    [noCategoryAlert addAction:[SIAAlertAction actionWithTitle:@"OK"
                                                         style:SIAAlertActionStyleCancel
                                                       handler:nil]];
    [self presentViewController:noCategoryAlert
                       animated:YES
                     completion:nil];
}

- (void)setCategoryFromRecordType {
    if ([self.recordingItem.recordType isEqualToString:kRecordTypeReview]) { // this is temporary until we get spec for review tags OMG
        SIAVideoCategory *reviewCategory = [[SIAVideoCategory alloc] init];
        reviewCategory.label = @"Review";
        NSMutableArray *tempArray = self.categoriesModel.categories.mutableCopy;
        [tempArray addObject:reviewCategory];
        self.categoriesModel.categories = [NSArray arrayWithArray:tempArray];
        self.categoryWasChosen = YES;
        self.selectedCategoryIndex = [self.categoriesModel.categories indexOfObject:reviewCategory];
        [self.tagsTable reloadData];
    }
    else if (self.recordingItem.recordType) {
        NSUInteger idx = 0;
        for (SIAVideoCategory *category in self.categoriesModel.categories) {
            for (NSString *recordType in category.types) {
                if ([self.recordingItem.recordType isEqualToString:recordType]) {
                    self.categoryWasChosen = YES;
                    self.selectedCategoryIndex = idx;
                    [self.tagsTable reloadData];
                    break;
                }
            }
            if (self.selectedCategoryIndex == idx && self.categoryWasChosen) break;
            idx++;
        }
    }
}

- (void) scrollToRowIfNotVisible:(NSIndexPath *)indexPath
                  scrollFinished:(void(^)(BOOL finished))handler {
    NSUInteger idx = [self.tagsTable.indexPathsForVisibleRows indexOfObjectPassingTest:^BOOL(NSIndexPath * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.row == indexPath.row) return YES;
        else return NO;
    }];
    // if not visible, scroll so it is
    if (idx == NSNotFound) {
        [UIView animateWithDuration:0.25
                         animations:^{
                             [self.tagsTable scrollToRowAtIndexPath:indexPath
                                                   atScrollPosition:UITableViewScrollPositionBottom
                                                           animated:NO];
                         } completion:handler];
    }
    else handler(YES);
}

- (void)roundBottomCorners:(UITableViewCell *)cell {
    [Utils dispatchOnMainThread:^{
        
        CAShapeLayer *roundedCornerLayer = [CAShapeLayer layer];
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:cell.contentView.bounds
                                                   byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                                         cornerRadii:CGSizeMake(10.0, 10.0)];
        roundedCornerLayer.frame = cell.contentView.bounds;
        roundedCornerLayer.path = path.CGPath;
        cell.contentView.layer.masksToBounds = YES;
        cell.contentView.layer.mask = roundedCornerLayer;
    }];
}

- (NSUInteger)categoryFieldIndex {
    return SIAStaticTagFieldCategory;
}

- (NSUInteger)titleFieldIndex {
    NSUInteger idx = SIAStaticTagFieldTitle;
    return (staticFields & SIATitleField) ? idx : -1;
}

- (NSUInteger)artistFieldIndex {
    NSUInteger idx = SIAStaticTagFieldArtist;
    return (staticFields & SIAArtistField) ? idx : -1;
}

- (NSUInteger) numberOfStaticCells {
    return ((staticFields & SIACategoryField) > 0) + ((staticFields & SIAArtistField) > 0) + ((staticFields & SIATitleField) > 0);
}

@end
