//
//  CameoInviteSegue.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 3/26/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

import UIKit

class CameoInviteSegue: UIStoryboardSegue {

    override func perform() {
        guard let video = (source as? VideoHomeViewController)?.video else { return }
        if video.isPrivate() {
            showCameoPublishAlert({ (publish) in
                if publish {
                    video.changeStatus(to: "public",
                                       withCompletionHandler: { (completed) in
                                        super.perform()
                    })
                }
            })
        }
        else {
            super.perform()
        }
    }
    
    private func showCameoPublishAlert(_ completed: @escaping (Bool) -> ()) {
        let alert = UIAlertController(title: "Video is private",
                                      message: "Please publish your video to invite to a Duet")
        alert.addAction(UIAlertAction(title: "Publish",
                                      style: .default,
                                      handler: { (action) in
                                        completed(true)
        }))
        alert.addAction(UIAlertAction(title: "Cancel",
                                      style: .cancel,
                                      handler: { (action) in
                                        completed(false)
        }))
        source.present(alert, animated: true)
    }
}
