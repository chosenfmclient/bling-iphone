//
//  SIANotificationsDataModel.m
//  SingrIphoneApp
//
//  Created by SingrFM on 10/8/13.
//  Copyright (c) 2013 SingrFM. All rights reserved.
//

#import "SIANotificationsDataModel.h"
#import "SIAItunesMusicElement.h"

#define NOTIFICATIONS_PATH                 @"api/v1/notifications"
#define ACCESS_TOKEN                       @"access_token"
#define GAME                               @"game"
#define TYPE                               @"type"
#define DATA                               @"data"
#define DATE                               @"date"
#define NOTIFICATION_ID                    @"notification_id"
#define TITLE                              @"title"
#define TEXT                               @"text"
#define IMAGE                              @"image"
#define STATUS                             @"status"
#define OBJECT                             @"object"
#define OBJECT_USER                        @"object.user"
#define USER                               kChosenUserKeyStr
#define VIDEO                              kChosenVideoKeyStr

@interface SIANotificationsDataModel()

@end

@implementation SIANotificationsDataModel

-(SIANotificationsDataModel *)initWithUserId:(NSInteger)userId{
    self = [super init];
    if (self) {
    }
    return self;
}

- (NSString *)pagePath {
    return NOTIFICATIONS_PATH;
}

+ (NSString *)pathPattern {
    return @"api/v1/notifications";
}

+ (RKMapping *)pageMapping {
    return [SIAPaginatorDataModel responseMappingForTotalKey:@"total_objects"
                                               objectMapping:[SIANotificationsDataModel objectMapping]
                                                  modelClass:[SIANotificationsDataModel class]];
}
+ (RKDynamicMapping*) objectMapping {
    
    NSDictionary *baseNotificationMapping = @{NOTIFICATION_ID:NOTIFICATION_ID,
                                              TITLE:TITLE,
                                              TEXT:TEXT,
                                              DATE:DATE,
                                              TYPE:TYPE,
                                              IMAGE:@"imageURL",
                                              STATUS:STATUS,
                                              @"image_type":@"image_type"};
    
    RKObjectMapping *defaultNotificationMapping = [[RKObjectMapping alloc] initWithClass:[SIANotificationsDataModel class]];
    [defaultNotificationMapping addAttributeMappingsFromDictionary:baseNotificationMapping];
    
    RKObjectMapping *videoNotificationMapping = [[RKObjectMapping alloc] initWithClass:[SIANotificationsDataModel class]];
    [videoNotificationMapping addAttributeMappingsFromDictionary:baseNotificationMapping];
    
    [videoNotificationMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:OBJECT toKeyPath:VIDEO withMapping:[SIAVideo objectMapping]]];
    [videoNotificationMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:OBJECT_USER toKeyPath:kChosenUserKeyStr withMapping:[SIAUser objectMapping]]];
    
    RKObjectMapping *userNotificationMapping = [[RKObjectMapping alloc] initWithClass:[SIANotificationsDataModel class]];
    [userNotificationMapping addAttributeMappingsFromDictionary:baseNotificationMapping];
    [userNotificationMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:OBJECT toKeyPath:kChosenUserKeyStr withMapping:[SIAUser objectMapping]]];

    
    RKObjectMapping *commentNotificationMapping = [[RKObjectMapping alloc] initWithClass:[SIANotificationsDataModel class]];
    
    [commentNotificationMapping addAttributeMappingsFromDictionary:baseNotificationMapping];
    [commentNotificationMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:OBJECT
                                                                                               toKeyPath:@"comment"
                                                                                             withMapping:[SIATextComment objectMapping]]];
    
    
    RKObjectMapping *recordNotificationObjectMapping = [[RKObjectMapping alloc] initWithClass:[SIANotificationsDataModel class]];
    
    [recordNotificationObjectMapping addAttributeMappingsFromDictionary:baseNotificationMapping];
    [recordNotificationObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:OBJECT
                                                                                               toKeyPath:@"musicVideo"
                                                                                             withMapping:[MusicVideo objectMapping]]];
    
    
    RKDynamicMapping *dynamicMapping = [RKDynamicMapping new];
    
    [dynamicMapping setObjectMappingForRepresentationBlock:^RKObjectMapping *(id representation) {
        if ([[representation valueForKey:TYPE] isEqualToString:VIDEO] ||
            [[representation valueForKey:TYPE] containsString:@"cameo"])
            
            return videoNotificationMapping;
        
        else if ([[representation valueForKey:TYPE] isEqualToString:kChosenUserKeyStr])
            
            return userNotificationMapping;
        
        else if ([[representation valueForKey:TYPE] isEqualToString:@"comment"])
            
            return commentNotificationMapping;
        
        else if ([[representation valueForKey:TYPE] isEqualToString:@"sound"])
            
            return recordNotificationObjectMapping;
        
        else return defaultNotificationMapping;
    }];
        
    return dynamicMapping;
    
}

- (void) setNotificationStatus {
    [SIANotificationsDataModel setNotoficationStatus:self.status forNotification:self];
}

/**
 *  Sends a delete request for the notification to the server.
 */
- (void) deleteNotification {    NSString *notificationsPath = [NOTIFICATIONS_PATH stringByAppendingString:@"/"];
    notificationsPath = [notificationsPath stringByAppendingString:self.notification_id.stringValue];
    
    [RequestManager deleteRequest:notificationsPath
                           object:nil
                  queryParameters:nil
                          success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                              NSLog(@"Baleeted");
                          }
                          failure:^(RKObjectRequestOperation *operation, NSError *error) {
                              NSLog(@"NOt baleeted");
                          }
                 showFailureDialog:YES];
}

+ (void) setNotoficationStatus:(NSString *)status forNotification:(SIANotificationsDataModel *)notification {
    notification.status = status;
    
    NSString *markAsReadPath = [NOTIFICATIONS_PATH stringByAppendingString:@"/"];
    markAsReadPath = [markAsReadPath stringByAppendingString:notification.notification_id.stringValue];
    [RequestManager putRequest:markAsReadPath
                        object:notification
               queryParameters:nil
                       success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                           
                       }
                       failure:^(RKObjectRequestOperation *operation, NSError *error) {
                           
                       }
              showFailureDialog:YES];

}

@end
