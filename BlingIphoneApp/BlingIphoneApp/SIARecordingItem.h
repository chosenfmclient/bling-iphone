//
//  SIARecordingItem.h
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 09/09/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "SIAVideoUploadDataModel.h"
#import "SIAUploadVideo.h"
#import "SIAVideo.h"
#import "SIAItunesMusicElement.h"
#import "SIASoundEffectModel.h"

typedef NS_OPTIONS(NSUInteger, SIALockedTagFields) {
    SIALockCategory = (1 << 0),
    SIALockTitle = (1 << 1),
    SIALockArtist = (1 << 2),
    SIALockAll = SIALockCategory | SIALockTitle | SIALockArtist
};

typedef NS_OPTIONS(NSUInteger, SIARecordingMediaType) {
    SIARecordingMediaTypeVideo,
    SIARecordingMediaTypePhoto
};

@interface RecordingOverlay : NSObject

@property (strong, nonatomic) NSString *segue;
@property (nonatomic) BOOL blackBackground;
@property (strong, nonatomic) id additionalInfo;

@end

// defines
#define RECORD_TYPE_RESPONSE               @"response"
#define BIO_TYPE                            @"bio"

//views when recording
FOUNDATION_EXPORT NSString * const kWhenRecordingViewLyrics;
FOUNDATION_EXPORT NSString * const kWhenRecordingViewTimer;
FOUNDATION_EXPORT NSString * const kWhenRecordingViewPitchDetection;

//views post recording

@class MusicVideo;

@interface SIARecordingItem : SIAVideo

@property(weak, nonatomic) SIAUploadVideo *uploader;

@property (nonatomic) SIALockedTagFields lockedFields;

@property (nonatomic) CGFloat recordingTime;

@property (nonatomic, strong) UIColor *themeColor;

@property (nonatomic) BOOL shouldRecordVideo;
@property (nonatomic) BOOL shouldRecordAudio;
@property (nonatomic) BOOL sholudPlayPlayback;
@property (nonatomic, strong, readonly) AVPlayer *playbackPlayer;
@property (nonatomic, strong) NSURL *playbackURL;
@property (nonatomic, strong) NSString *playbackId;
@property (strong, nonatomic) NSURL *soundtrackURL;
@property (strong, nonatomic) NSURL *lrcURL;
@property (nonatomic, strong) NSString *addSong; // this is for uploading from a game!
@property (nonatomic, strong) NSMutableArray <NSString *> *tags;
@property (nonatomic, strong) NSIndexSet *lockedTagIndexes;
@property (nonatomic, strong) SIAVideo *parentVideo;

@property (nonatomic, strong) NSString *bioQuestionId; 

@property (nonatomic) BOOL dontContinueToPreview;

@property (nonatomic, strong, setter=setRecordType:) NSString *recordType;
@property (nonatomic, strong) NSString *recordStatus;
@property (nonatomic, strong) NSString *parentId;
@property (nonatomic, strong) NSURL *recordURL;
@property (nonatomic, strong) NSURL *cameraRollURL;
@property (nonatomic, strong) AVAsset *recordedAsset;
@property (nonatomic, weak) AVAsset *assetForPlayback;
@property (nonatomic) CMTimeRange recordedTimeRange;

@property (strong, nonatomic) NSString *urlForShare;

@property (nonatomic, strong) NSArray *videoFilters;
@property (nonatomic, strong) NSArray *audioFilters;
@property (nonatomic) NSUInteger frameIndex;

@property (nonatomic, strong) NSArray <NSString *> *pickTheHookTimeRange;

@property (nonatomic, strong) NSArray *instructionsToShow;
@property (strong, nonatomic) NSString *instructionText;
@property (nonatomic) RecordingOverlay *overlayToShowWhenRecording;
@property (nonatomic, strong) NSArray *viewsToShowAfterRecording;
@property (nonatomic, strong) MusicVideo *musicVideo;
@property (strong, nonatomic) UIImage *image;

- (void)startUploadVideoWithConvertHandler:(void(^)(BOOL converted))convertHandler
                             uploadHandler:(void (^)(BOOL uploaded))handler;
- (void)uploadThumbnail:(UIImage *)thumbnail;
- (void)updateFilters;
@property (strong, nonatomic) SIAVideoUploadDataModel *uploadData;

// obsereved members
//@property (nonatomic) NSInteger uploadingProgressInPercents;
@property (nonatomic) BOOL finishedUploadingVideo;
@property (nonatomic) BOOL finishedUploadingThumb;
@property (nonatomic) BOOL finishedUpdatingVideo;

@property (nonatomic) BOOL recordCanceled;

@property (nonatomic) CMTime timeOfPlayback;
@property (nonatomic) CMTime playbackStartOffset;
@property (nonatomic) BOOL recordingIsReadyForPreview;

@property (nonatomic) Float64 recordingSpeedScale;


// tag stuff whatever
@property (nonatomic) BOOL removeArtistTag;

- (id) initForRecorder;
- (void)tearDownPlaybackPlayer;
+(NSString *)getFilePathForFileName:(NSString *)fileName;
- (void)setDataFromSoundEffect:(SIASoundEffect *)soundEffect;
-(void) setDataFromMusicElement:(SIAItunesMusicElement *)musicElement;
+ (NSURL *)recordURLForRecord;
- (void)prepareForUploadWithCompletionHandler: (void(^_Nonnull)(BOOL completed))handler;
- (void)generateThumbnails:(void(^ _Nonnull)(UIImage * _Nullable image))handler;

@end
