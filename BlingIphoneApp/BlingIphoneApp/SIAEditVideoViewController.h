//
//  SIAEditVideoViewController.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/23/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIABasePostRecordViewController.h"

@interface SIAEditVideoViewController : SIABasePostRecordViewController

@property (weak, nonatomic) IBOutlet UIView   *buttonsView;
@property (weak, nonatomic) IBOutlet UIButton *audioEffectsButton;
@property (weak, nonatomic) IBOutlet UIButton *videoEffectsButton;
@property (weak, nonatomic) IBOutlet UIButton *framesButton;
@property (weak, nonatomic) IBOutlet UIButton *thumbnailsButton;
@property (weak, nonatomic) IBOutlet UIButton *hookButton;

@property (weak, nonatomic) BIAVideoPlayer *player;
@property (weak, nonatomic) SIARecordingItem *recordingItem;
@end
