//
//  SIAEditVideoViewController.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/23/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAEditVideoViewController.h"
#import "SIAHookPickerView.h"
#import "SIAFramesDataModel.h"
#import "SIAHorizontalSelectionScrollView.h"
#import "SDWebImagePrefetcher.h"

typedef NS_ENUM(NSInteger, SIAAudioEffectButtonType){
    SIAAudioEffectButtonTypeNone = 100,
    SIAAudioEffectButtonTypeReverb,
    SIAAudioEffectButtonTypeEcho
};

typedef NS_ENUM(NSInteger, SIAVideoEffectButtonType){
    SIAVideoEffectButtonTypeNone = 200,
    SIAVideoEffectButtonTypeBW,
    SIAVideoEffectButtonTypeSaturate
};

@interface SIAEditVideoViewController () <SIAHookPickerDelegate>



@property (strong, nonatomic) NSMutableArray <UIButton *> *optionsButtons;

// bottom container views
@property (strong, nonatomic) IBOutlet UIView *optionsView;
@property (strong, nonatomic) IBOutlet UIView *audioEffectsView;
@property (strong, nonatomic) IBOutlet UIView *videoEffectsView;
@property (strong, nonatomic) IBOutlet UIView *thumbnailsView;
@property (strong, nonatomic) IBOutlet UIView *framesView;
@property (strong, nonatomic) SIAHorizontalSelectionScrollView *framesScrollView;
@property (strong, nonatomic) IBOutlet SIAHookPickerView *hookPickerView;


// audio effects
@property (strong, nonatomic) NSArray *audioEffectsButtons;
@property (strong, nonatomic) IBOutlet UIButton *noAudioEffectButton;
@property (strong, nonatomic) IBOutlet UIButton *reverbButton;
@property (strong, nonatomic) IBOutlet UIButton *echoButton;
@property (nonatomic) SIAAudioEffectType currentAudioEffect;

// video effects
@property (strong, nonatomic) NSArray *videoEffectsButtons;
@property (strong, nonatomic) IBOutlet UIButton *noVideoEffectButton;
@property (strong, nonatomic) IBOutlet UIButton *blackAndWhiteButton;
@property (strong, nonatomic) IBOutlet UIButton *saturatedButton;
@property (nonatomic) SIAVideoEffectType currentVideoEffect;

// thumbnails
@property (strong, nonatomic) NSArray *thumbnailButtons;
@property (strong, nonatomic) IBOutlet UIButton *thumbnail1;
@property (strong, nonatomic) IBOutlet UIButton *thumbnail2;
@property (strong, nonatomic) IBOutlet UIButton *thumbnail3;

// frames
//@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *frameButtons;
@property (strong, nonatomic) NSMutableArray <UIButton *> *frameButtons;
@property (strong, nonatomic) IBOutlet UIButton *noFrameButton;
@property (strong, nonatomic) IBOutlet UIButton *frame1;
@property (strong, nonatomic) IBOutlet UIButton *frame2;
@property (strong, nonatomic) IBOutlet UIButton *frame3;
@property (strong, nonatomic) NSMutableArray <NSURL *> *frameURLs;
@property (strong, nonatomic) NSArray <SIAFrame *> *frames;
@property (strong, nonatomic) NSArray <UIImage *> *frameTextures;

// photo stuff
@property (strong, nonatomic) UIImage *originalImage;
@property (strong, nonatomic) UIImage *originalThumb;
@property (strong, nonatomic) UIView *currentOptionsView;
@property (nonatomic) CGRect originalOptionsViewFrame;

@end

@implementation SIAEditVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.audioEffectsButtons = @[_noAudioEffectButton, _reverbButton, _echoButton];
    self.videoEffectsButtons = @[_noVideoEffectButton, _blackAndWhiteButton, _saturatedButton];
    self.optionsButtons = @[_audioEffectsButton, _videoEffectsButton, _framesButton, _thumbnailsButton, _hookButton].mutableCopy;
    self.thumbnailButtons = @[_thumbnail1, _thumbnail2, _thumbnail3];
    self.frameURLs = [[NSMutableArray alloc] init];

    [self selectAudioEffectButton:self.noAudioEffectButton];
    [self selectVideoEffectButton:self.noVideoEffectButton];
    [self generateThumbnails];
    if (self.recordingItem.image) {
        [self createImageThumbnail];
    }

    for (UIButton *thumbButton in _thumbnailButtons) {
        thumbButton.layer.borderWidth = 6.0;
        thumbButton.layer.borderColor = [[UIColor clearColor] CGColor];
    }
    
    self.buttonsView.hidden = [self isReviewVideo];
    
    self.originalImage = self.recordingItem.image.copy;
    
    [self setImageButtonBorder:_thumbnail2];
    
    [SIAFramesDataModel getFrames:^(SIAFramesDataModel *model) {
        [self setUpFramesScrollView:model];
    }];
    
    CGRect hookPickerFrame = self.hookPickerView.frame;
    hookPickerFrame.size.width = self.view.frame.size.width;
    self.hookPickerView.frame = hookPickerFrame;
    
    [self.hookPickerView setupForRecordingItem:self.recordingItem
                                      delegate:self];
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:self.recordingItem.recordURL
                                            options:nil];
    CMTime minimumHookTime = CMTimeMakeWithSeconds(15, 1);
    
    NSMutableArray *buttonsToRemove = [NSMutableArray array];
    
    if (self.recordingItem.image) {
        [self setOptionsButtonsForPhoto];
    }
    else
    {
        if (CMTIME_COMPARE_INLINE(asset.duration, <=, minimumHookTime)) {
            [buttonsToRemove addObject:self.hookButton];
        }
        if (self.recordingItem.soundtrackURL)
        {
            [buttonsToRemove addObject:self.audioEffectsButton];
        }
        
        [self setOptionsButtonsLayoutByRemovingButtons:buttonsToRemove];
   
    }
    
    self.originalOptionsViewFrame = self.optionsView.frame;
    
    if ([self isReviewVideo])
    {
        [self thumbnailsWasTapped:self.thumbnailsButton];
    }
    else if (self.recordingItem.image || self.recordingItem.soundtrackURL) {
        [self videoEffectsWasTapped:self.videoEffectsButton];
    }
    else
    {
        [self audioEffectsWasTapped:self.audioEffectsButton];
    }
    
    [SIAUploadVideo sharedUploadManager].videoFilters = nil;
    [SIAUploadVideo sharedUploadManager].audioFilters = nil;
    
    for (UIButton *button in self.thumbnailButtons)
    {
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        button.imageView.contentMode = UIViewContentModeScaleAspectFill;
    }
}

- (void)viewWillAppear:(BOOL)animated {
//    if ([self isReviewVideo])
//    {
//        [self thumbnailsWasTapped:self.thumbnailsButton];
//    }
//    else if (self.recordingItem.image) {
//        [self videoEffectsWasTapped:self.videoEffectsButton];
//    }
//    else
//    {
//        [self audioEffectsWasTapped:self.audioEffectsButton];
//    }

    [self.navigationController setNavigationBarLeftButtonImage:[UIImage imageNamed:@"back.png"]
                                                        target:self
                                                        action:@selector(back)];
    
    [self.navigationController setNavigationBarRightButtonNextTarget:self
                                                              action:@selector(next)];
    
    if (self.recordingItem.image) {
        [self.navigationItem setTitle:@"Edit Photo"];
    }
    else {
        [self.navigationItem setTitle:@"Edit Video"];
    }
    //[Flurry logEvent:FLURRY_VIEW_POSTRECORD_EDITVIDEO];
}

- (void)back {
    [self.delegate recordingItemWasChanged:self.recordingItem];
    [super back];
   // [Flurry logEvent:FLURRY_TAP_BACK_POSTRECORD_EDITVIDEO];
}

- (void)next {
    SIAUploadVideo *uploader = [SIAUploadVideo sharedUploadManager];
    if (self.currentAudioEffect == SIAAudioEffectTypeDelay) {
        uploader.audioFilters = @[@"echo"];
    }
    else if (self.currentAudioEffect == SIAAudioEffectTypeReverb) {
        uploader.audioFilters = @[@"reverb"];
    }
    if (self.currentVideoEffect == SIAVideoEffectTypeBlackAndWhite) {
        uploader.videoFilters = @[@"black_white"];
    }
    else if (self.currentVideoEffect == SIAVideoEffectTypeSaturation) {
        uploader.videoFilters = @[@"saturation"];
    }
    
    [self.delegate recordingItemWasChanged:self.recordingItem];
    [super next];
  //  [Flurry logEvent:FLURRY_TAP_NEXT_POSTRECORD_EDITVIDEO];
}

#pragma mark Editor Buttons

- (IBAction)audioEffectsWasTapped:(UIButton *)sender {
    if (sender.alpha == 1) return;
  // [Flurry logEvent:FLURRY_TAP_ENHANCEAUDIO_POSTRECORD_EDITVIDEO];
    [self optionsViewTransitionOut];
    CGPoint buttonsViewCenter = self.audioEffectsView.center;
    buttonsViewCenter.x = self.optionsView.center.x;
    self.audioEffectsView.center = buttonsViewCenter;
    [self.optionsView addSubview:self.audioEffectsView];
    self.currentOptionsView = self.audioEffectsView;
    [self dimOptionsButtonsExcept:sender];
}

- (IBAction)videoEffectsWasTapped:(UIButton *)sender {
    if (sender.alpha == 1) return;
  //  [Flurry logEvent:FLURRY_TAP_ENHANCEVIDEO_POSTRECORD_EDITVIDEO];
    [self optionsViewTransitionOut];
    CGPoint buttonsViewCenter = self.videoEffectsView.center;
    buttonsViewCenter.x = self.optionsView.center.x;
    self.videoEffectsView.center = buttonsViewCenter;
    [self.optionsView addSubview:self.videoEffectsView];
    self.currentOptionsView = self.videoEffectsView;
    [self dimOptionsButtonsExcept:sender];
}

- (IBAction)framesWasTapped:(UIButton *)sender {
    if (sender.alpha == 1) return;
   // [Flurry logEvent:FLURRY_TAP_FRAMES_POSTRECORD_EDITVIDEO];
    [self.currentOptionsView removeFromSuperview];
    CGPoint buttonsViewCenter = self.framesScrollView.center;
    buttonsViewCenter.x = self.optionsView.center.x;
    self.framesScrollView.center = buttonsViewCenter;
    if (self.framesScrollView.contentSize.width < SCREEN_WIDTH)
    {
        // center the frames if the screen is wider than the width of the scroll view content
        CGPoint offset = CGPointMake(-(SCREEN_WIDTH - self.framesScrollView.contentSize.width) / 2, 0);
        self.framesScrollView.contentOffset = offset;
    }
    [self addLargeOptionsViewWithAnimation:self.framesScrollView];
    self.currentOptionsView = self.framesScrollView;
    [self dimOptionsButtonsExcept:sender];
}


- (IBAction)thumbnailsWasTapped:(UIButton *)sender {
    if (sender.alpha == 1) return;
  //  [Flurry logEvent:FLURRY_TAP_THUMBNAILS_POSTRECORD_EDITVIDEO];
    [self.currentOptionsView removeFromSuperview];
    CGPoint buttonsViewCenter = self.thumbnailsView.center;
    buttonsViewCenter.x = self.optionsView.center.x;
    self.thumbnailsView.center = buttonsViewCenter;
    [self addLargeOptionsViewWithAnimation:self.thumbnailsView];
    self.currentOptionsView = self.thumbnailsView;
    [self dimOptionsButtonsExcept:sender];
}

- (IBAction)hookWasTapped:(UIButton *)sender {
    if (sender.alpha == 1) return;
//    [Flurry logEvent:FLURRY_TAP_PICKHOOK_POSTRECORD_EDITVIDEO];
    [self optionsViewTransitionOut];
    self.hookPickerView.frame = self.optionsView.bounds;
    [self.optionsView addSubview:self.hookPickerView];
    self.currentOptionsView = self.hookPickerView;
    [self dimOptionsButtonsExcept:sender];
}

- (void)optionsViewTransitionOut {
    if (self.currentOptionsView == self.thumbnailsView)
        [self removeLargeOptionsViewWithAnimation:self.thumbnailsView];
    else if (self.currentOptionsView == self.framesScrollView)
        [self removeLargeOptionsViewWithAnimation:self.framesScrollView];
    else
        [self.currentOptionsView removeFromSuperview];
}

#pragma Audio Effects

- (IBAction)audioEffectButtonWasTapped:(UIButton *)sender {
    if (sender.selected) return;
    [self selectAudioEffectButton:sender];
    [self.player unsetAudioFilter:self.currentAudioEffect];
    switch (sender.tag) {
        case SIAAudioEffectButtonTypeNone:
   //         [Flurry logEvent:FLURRY_TAP_NOAUDIOEFFECT_POSTRECORD_EDITVIDEO];
            self.currentAudioEffect = SIAAudioEffectButtonTypeNone;
            break;
        case SIAAudioEffectButtonTypeReverb:
    //        [Flurry logEvent:FLURRY_TAP_ADDREVERB_POSTRECORD_EDITVIDEO];
            [self.player setAudioFilter:SIAAudioEffectTypeReverb];
            self.currentAudioEffect = SIAAudioEffectTypeReverb;
            break;
        case SIAAudioEffectButtonTypeEcho:
    //        [Flurry logEvent:FLURRY_TAP_ADDECHO_POSTRECORD_EDITVIDEO];
            [self.player setAudioFilter:SIAAudioEffectTypeDelay];
            self.currentAudioEffect = SIAAudioEffectTypeDelay;
        default:
            break;
    }
}

- (void)selectAudioEffectButton:(UIButton *)effectButton {
    effectButton.selected = YES;
    [self setButtonBackgroundSelected:effectButton];
    for (UIButton *effectButtonToDeselect in self.audioEffectsButtons) {
        if (effectButtonToDeselect != effectButton) {
            effectButtonToDeselect.selected = NO;
            [self setButtonBackgroundSelected:effectButtonToDeselect];
        }
    }
}


#pragma Video Effects


- (IBAction)videoEffectButtonwasTapped:(UIButton *)sender {
    if (sender.selected) return;
    [self selectVideoEffectButton:sender];
    switch (sender.tag) {
        case SIAVideoEffectButtonTypeNone:
 //           [Flurry logEvent:FLURRY_TAP_NOVIDEOEFFECT_POSTRECORD_EDITVIDEO];
            [self.player unsetVideoFilter:self.currentVideoEffect];
            self.currentVideoEffect = SIAVideoEffectTypeNone;
            if (self.recordingItem.image) {
                self.recordingItem.image = self.originalImage;
                [self.delegate photoFilterWasApplied];
                [self.delegate thumbnailWasChosen:self.originalThumb];
            }
            break;
        case SIAVideoEffectButtonTypeBW:
    //        [Flurry logEvent:FLURRY_TAP_BLACKANDWHITE_POSTRECORD_EDITVIDEO];
            [self.player setVideoFilter:SIAVideoEffectTypeBlackAndWhite];
            self.currentVideoEffect = SIAVideoEffectTypeBlackAndWhite;
            if (self.recordingItem.image)
            {
                self.recordingItem.image = [self applyBlackAndWhiteToImage:self.originalImage];
                [self.delegate photoFilterWasApplied];
                [self.delegate thumbnailWasChosen:[self applyBlackAndWhiteToImage:self.originalThumb]];
            }
            break;
        case SIAVideoEffectButtonTypeSaturate:
    //        [Flurry logEvent:FLURRY_TAP_SATURATE_POSTRECORD_EDITVIDEO];
            [self.player setVideoFilter:SIAVideoEffectTypeSaturation];
            self.currentVideoEffect = SIAVideoEffectTypeSaturation;
            if (self.recordingItem.image)
            {
                self.recordingItem.image = [self applySaturationToImage:self.originalImage];
                [self.delegate thumbnailWasChosen:[self applySaturationToImage:self.originalThumb]];
                [self.delegate photoFilterWasApplied];
            }
        default:
            break;
    }
}

- (UIImage *)applyBlackAndWhiteToImage:(UIImage *)image {
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *input = [CIImage imageWithCGImage:[image CGImage]];
    CIImage *output = [input imageByApplyingFilter:@"CIColorControls"
                               withInputParameters:@{kCIInputSaturationKey:@0.0f}];
    CGImageRef CGImage = [context createCGImage:output
                                       fromRect:[output extent]];
    image = [UIImage imageWithCGImage:CGImage];
    CGImageRelease(CGImage);
    context = nil;
    return image;
}

- (UIImage *)applySaturationToImage:(UIImage *)image {
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *input = [CIImage imageWithCGImage:[image CGImage]];
    CIImage *output = [input imageByApplyingFilter:@"CIColorControls"
                               withInputParameters:@{kCIInputSaturationKey:@2.0f}];
    CGImageRef CGImage = [context createCGImage:output
                                       fromRect:[output extent]];
    image = [UIImage imageWithCGImage:CGImage];
    CGImageRelease(CGImage);
    context = nil;
    return image;
}

- (void)selectVideoEffectButton:(UIButton *)effectButton {
    effectButton.selected = YES;
    [self setButtonBackgroundSelected:effectButton];
    for (UIButton *effectButtonToDeselect in self.videoEffectsButtons) {
        if (effectButtonToDeselect != effectButton) {
            effectButtonToDeselect.selected = NO;
            [self setButtonBackgroundSelected:effectButtonToDeselect];
        }
    }
}

#pragma Frames

- (IBAction)frameWasTapped:(UIButton *)sender {
    if (sender.selected) return;
    [self selectFrameButton:sender];
    
    NSNumber *frame_id = @(self.frames[sender.tag].frame_id);
    NSString *name = self.frames[sender.tag].name;
    NSDictionary *flurryParams = @{@"frame_id":frame_id,
                                   @"frame_name":name,
                                   @"pos":@(sender.tag)};
//    [Flurry logEvent:FLURRY_TAP_FRAME_POSTRECORD_EDITVIDEO
//      withParameters:flurryParams];
}

- (void)selectFrameButton:(UIButton *)frameButton {
    
    [self.framesScrollView selectButtonAtIndex:frameButton.tag];
    
    if (frameButton.tag < self.frameURLs.count)
        [self.delegate frameWasChosen:self.frameURLs[frameButton.tag]
                           frameIndex:frameButton.tag];
}

- (void)setUpFramesScrollView:(SIAFramesDataModel *)framesModel {
    
    self.frames = framesModel.frames;
    self.frameButtons = [NSMutableArray array];
    NSMutableArray *frameViews = [NSMutableArray array];
    NSMutableArray *imagesForPrefetching = [NSMutableArray array];
    NSUInteger idx = 0;
    NSString *aspectRatioKey = [self aspectRatioKey];
    for (SIAFrame *frame in framesModel.frames) {
        
        SIASelectableView *frameView = [[SIASelectableView alloc] initWithImageURL:frame.texture
                                                                             title:frame.name
                                                                      buttonTarget:self
                                                                    buttonSelector:@selector(frameWasTapped:)
                                                                         buttonTag:[framesModel.frames indexOfObject:frame]
                                                                    placementIndex:idx];
        frameView.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [frameViews addObject:frameView];
        NSURL *frameURL;
        if (frame.frames && [frame.frames.allKeys containsObject:aspectRatioKey])
        {
            frameURL = [frame.frames objectForKey:aspectRatioKey];
            [imagesForPrefetching addObject:frameURL];
            [self.frameURLs addObject:frameURL];
        }
        else [self.frameURLs addObject:[NSURL URLWithString:[NSString string]]];
        
        [self.frameButtons addObject:frameView.button];
        frameView.button.accessibilityLabel = idx ? @"button_frame" : @"button_none";
        idx++;
    }
    
    self.framesScrollView = [[SIAHorizontalSelectionScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 85)
                                                                              views:frameViews
                                                                              style:SIAHorizontalSelectionScrollViewStyleButtonBorderHighlights];
    
    [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:imagesForPrefetching];

    [self.framesScrollView selectButtonAtIndex:self.frameButtons.firstObject.tag];
}

- (NSString *)aspectRatioKey {
    
    CGSize mediaSize;
    
    if (self.recordingItem.image)
    {
        mediaSize = self.recordingItem.image.size;
    }
    else
    {
        mediaSize = [self sizeFromRecordedVideo];
    }
    
    CGFloat aspectRatio = mediaSize.width / mediaSize.height;
    return [SIAFramesDataModel aspectRatioKeyForAspectRatio:aspectRatio];
}

- (BOOL) isLandscape {
    if (self.originalImage)
    {
        if (self.originalImage.size.width > self.originalImage.size.height)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    else
    {
        CGSize transformedSize = [self sizeFromRecordedVideo];
        if (transformedSize.width > transformedSize.height)
            return YES;
        
        return NO;
    }
}

- (CGSize)sizeFromRecordedVideo {
    AVAssetTrack *videoTrack = [[AVAsset assetWithURL:self.recordingItem.recordURL] tracksWithMediaType:AVMediaTypeVideo].lastObject;
    CGSize size = videoTrack.naturalSize;
    CGAffineTransform preferredTransform = videoTrack.preferredTransform;
    CGRect rectForTransform = CGRectZero;
    rectForTransform.size = size;
    rectForTransform = CGRectApplyAffineTransform(rectForTransform, preferredTransform);
    return rectForTransform.size;
}

#pragma Thumbnails

- (IBAction)thumbnailWasTapped:(UIButton *)sender {
    if (sender.selected) return;
    [self selectThumbnailButton:sender];
    if (sender == _thumbnail1) {
//        [Flurry logEvent:FLURRY_TAP_THUMB1_POSTRECORD_EDITVIDEO];
    }
    else if (sender == _thumbnail2) {
//        [Flurry logEvent:FLURRY_TAP_THUMB2_POSTRECORD_EDITVIDEO];
    }
    else if (sender == _thumbnail3) {
  //      [Flurry logEvent:FLURRY_TAP_THUMB3_POSTRECORD_EDITVIDEO];
    }
}

- (void)selectThumbnailButton:(UIButton *)thumbnailButton {
    thumbnailButton.selected = YES;
    [self.delegate thumbnailWasChosen:thumbnailButton.imageView.image];
    [self setImageButtonBorder:thumbnailButton];
    [self deselectButtonsExcept:thumbnailButton
                        inArray:self.thumbnailButtons];
}

-(void)generateThumbnails{
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:self.recordingItem.recordURL
                                            options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    
    CMTime duration = asset.duration;
    CMTime firstTime = CMTimeMakeWithSeconds(0,30);
    CMTime middleTime = CMTimeMakeWithSeconds(CMTimeGetSeconds(duration)/2, 30);
    CMTime lastTime = duration;
//    
//    CGSize maxSize = CGSizeMake(640, 400);
//    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:@[[NSValue valueWithCMTime:firstTime], [NSValue valueWithCMTime:middleTime], [NSValue valueWithCMTime:lastTime]] completionHandler:^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){
        if (result != AVAssetImageGeneratorSucceeded) {
            NSLog(@"couldn't generate thumbnail, error:%@", error);
        }else{
            UIImage *image = [UIImage imageWithCGImage:im];
            [Utils dispatchOnMainThread:^{
                if (!CMTimeCompare(requestedTime, firstTime))
                {
                    [_thumbnail1 setImage:image forState:UIControlStateNormal];
                }
                else if (!CMTimeCompare(requestedTime, middleTime))
                {
                    [_thumbnail2 setImage:image forState:UIControlStateNormal];
                    [self.delegate thumbnailWasChosen:image];
                }
                else if (!CMTimeCompare(requestedTime, lastTime))
                    [_thumbnail3 setImage:image forState:UIControlStateNormal];
            }];
        }
    }];
}

- (void)createImageThumbnail {
//    CGImageRef thumbCG = [self.recordingItem.image CGImage];
//    UIImage *squareThumb = [UIImage imageWithCGImage:[self cropImageToSquare:thumbCG]];
    UIImage *image = self.recordingItem.image;
    CGRect scaledRect = CGRectZero;
    scaledRect.size = image.size;
    CGFloat scale = 1;
    if (scaledRect.size.width > [self thumbnailImageSize])
    {
        scale = [self thumbnailImageSize] / scaledRect.size.width;
    }
    scaledRect = CGRectApplyAffineTransform(scaledRect, CGAffineTransformMakeScale(scale, scale));
    UIGraphicsBeginImageContextWithOptions(scaledRect.size, NO, image.scale);
    [image drawInRect:scaledRect];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.delegate thumbnailWasChosen:image];
    self.originalThumb = image.copy;
}

- (CGImageRef)cropImageToSquare:(CGImageRef)image {
    size_t height = CGImageGetHeight(image);
    size_t width = CGImageGetWidth(image);
    
    if (height == width) return image; // already sqaure
    
    NSUInteger squareSide = MIN(width, height);
    NSUInteger longerSide = MAX(width, height);
    
    CGRect squareRect;
    
    if (longerSide == width) // landscape
    {
        squareRect = CGRectMake(longerSide / 2 - squareSide / 2,
                                0,
                                squareSide,
                                squareSide);
    }
    else
    {
        squareRect = CGRectMake(0,
                                longerSide / 2 - squareSide / 2,
                                squareSide,
                                squareSide);
    }
    
    return CGImageCreateWithImageInRect(image, squareRect);
}

- (NSUInteger)thumbnailImageSize {
    return 192;
}

#pragma mark Hook Picker

- (void)hookTimeChangedWithStart:(CGFloat)start end:(CGFloat)end {
    self.recordingItem.pickTheHookTimeRange = @[[NSString stringWithFormat:@"%d", (int)start],
                                                [NSString stringWithFormat:@"%d", (int)end]];
    [SIAUploadVideo sharedUploadManager].pickTheHookTimeRange = self.recordingItem.pickTheHookTimeRange;
    [self.delegate repeatPlaybackFrom:start
                                   to:end];
}

#pragma Button Selection Animations

- (void)setButtonBackgroundSelected:(UIButton *)button {
    [UIView animateWithDuration:0.2
     animations:^{
         button.backgroundColor = button.selected ? [[SIAStyling defaultStyle] recordingOptionsButtonsRed] : [UIColor clearColor];
     }];
}

- (void)setImageButtonBorder:(UIButton *)button {
    
    CABasicAnimation *borderAnimation = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    borderAnimation.duration = 0.1;
    borderAnimation.fillMode = kCAFillModeForwards;
    borderAnimation.removedOnCompletion = NO;
    
    CGColorRef borderColor = button.selected ? [[[SIAStyling defaultStyle] recordingOptionsButtonsRed] CGColor] : [[UIColor clearColor]CGColor];
    borderAnimation.toValue = (__bridge id)borderColor;
    
    [CATransaction begin]; {
        [CATransaction setCompletionBlock:^{
           button.layer.borderColor = borderColor;
            [button.layer removeAllAnimations];
        }];
        [button.layer addAnimation:borderAnimation
                                  forKey:@"borderAnimation"];
    }
    [CATransaction commit];
}

#pragma mark Thumbnails View In/Out Animations

- (void)addLargeOptionsViewWithAnimation:(UIView *)largeView {
    CGRect largeViewBounds = largeView.bounds;
    CGRect optionsViewFrame = self.optionsView.frame;
    optionsViewFrame.size.height = largeViewBounds.size.height;
    CGFloat sizeChange = optionsViewFrame.size.height - self.optionsView.bounds.size.height;
    optionsViewFrame.origin.y -= sizeChange;
    CGAffineTransform buttonsViewTransform = CGAffineTransformTranslate(self.buttonsView.transform,
                                                                        0,
                                                                        -sizeChange);
    [self.buttonsView setUserInteractionEnabled:NO];
    [self.optionsView addSubview:largeView];
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.optionsView.frame = optionsViewFrame;
                         self.buttonsView.transform = buttonsViewTransform;
                         largeView.alpha = 1;
                     } completion:^(BOOL finished) {
                         [self.buttonsView setUserInteractionEnabled:YES];
                     }];
}

- (void)removeLargeOptionsViewWithAnimation:(UIView *)largeView {
    [self.buttonsView setUserInteractionEnabled:NO];
    
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.optionsView.frame = self.originalOptionsViewFrame;
                         self.buttonsView.transform = CGAffineTransformIdentity;
                         largeView.alpha = 0;
                     } completion:^(BOOL finished) {
                         [largeView removeFromSuperview];
                         [self.buttonsView setUserInteractionEnabled:YES];
                     }];
}

- (void)deselectButtonsExcept:(UIButton *)button
                      inArray:(NSArray *)buttonsArray {
    for (UIButton *buttonToDeselect in buttonsArray) {
        if (buttonToDeselect != button) {
            buttonToDeselect.selected = NO;
            [self setImageButtonBorder:buttonToDeselect];
        }
    }
}

- (void)dimOptionsButtonsExcept:(UIButton *)activeButton {
    [UIView animateWithDuration:0.1
                     animations:^{
                         activeButton.alpha = 1.0;
                         for (UIButton *button in self.optionsButtons)
                         {
                            if (!(button == activeButton))
                            {
                                button.alpha = 0.33;
                            }
                         }
                     }];
}

- (void)setOptionsButtonsForPhoto {
    [self setOptionsButtonsLayoutByRemovingButtons:@[self.audioEffectsButton,
                                                     self.thumbnailsButton,
                                                     self.hookButton]];
}

- (void)setOptionsButtonsForShortVideo {
    [self setOptionsButtonsLayoutByRemovingButtons:@[self.hookButton]];
}

- (void)setOptionsButtonsLayoutByRemovingButtons:(NSArray *)removedButtons {
    if (removedButtons)
    {
        for (UIButton *button in self.optionsButtons) {
            if ([removedButtons containsObject:button])
                button.hidden = YES;
        }
        [self.optionsButtons removeObjectsInArray:removedButtons];
    }
    int idx = 1;
    for (UIButton *button in self.optionsButtons)
    {
        CGRect buttonFrame = button.frame;
        buttonFrame.origin.x = SCREEN_WIDTH * idx / (self.optionsButtons.count + 1) - buttonFrame.size.width / 2;
        button.frame = buttonFrame;
        idx++;
    }
}

- (BOOL)isReviewVideo {
    return [self.recordingItem.recordType isEqualToString:kRecordTypeReview] && self.recordingItem.mediaType != SIARecordingMediaTypePhoto;
}

@end
