//
//  BIAScreenCheckerWrapper.m
//  BlingIphoneApp
//
//  Created by Joseph Nahmias on 21/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import "BIAScreenCheckerWrapper.h"
#import "BIAScreenChecker.h"

@implementation BIAScreenCheckerWrapper

+(BOOL)checkScreen:(CVPixelBufferRef)screen forAccuracyRate:(CGFloat)accuracy resizeFactor:(CGFloat)resizeFactor {
    
    return [BIAScreenChecker checkScreen:screen forAccuracyRate:accuracy resizeFactor:resizeFactor];
}


@end
