//
//  RemoteDataHandler.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 10/17/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class RemoteDataHandler: NSObject {
    
    /// Handles push notifications from the userInfo dictionary passed to the app delegate
    // @param pushNotification dictionary of push notification data
    // @param completionHandle call this with a UIBackgroundFetchResult after the data has been handled
    static func parseUserInfo(_ userInfo: [AnyHashable : Any],
                              _ completionHandler: ((UIBackgroundFetchResult) -> Void)?) {
        guard let type = userInfo["type"] as? String else {
            completionHandler?(.noData)
            return
        }
        guard let data = userInfo["data"] as? [String : Any] else {
            completionHandler?(.noData)
            return
        }
        
        var stupidType: String?
        if let noteType = data["notification_type"] as? String, noteType == "viral_cameo_request" {
            stupidType = "cameo_invite"
        }
        
        RemoteDataHandler.parseData(data, stupidType ?? type)
        RemoteDataHandler.sendAmplitudeEvent(forUserInfo: userInfo)
        completionHandler?(.noData)
    }
    
    static func sendPushMetrics(forReceiveType type: String, alert: String? = nil, data: [String: Any]) {
        
        var pushData = data
        pushData["type"] = type
        if let _ = alert {
            pushData["alert"] = alert
        }
        let pushModel = PushNotificationModel(withData: pushData)
        RequestManager.postRequest("api/v1/push-metric",
                                   object: pushModel,
                                   queryParameters: nil,
                                   success: { (req, res) in
              print("push metric send success")
        }, failure: { (req, err) in
            print("push metric send failure")
        }, showFailureDialog: false)
        
    }
    /** Handles push notifications from the UNUserNotificationCenter in iOS 10
 */
    @available(iOS 10.0, *)
    static func parseResponse(_ response: UNNotificationResponse?,
                              _ completionHandler: @escaping () -> Void) {
        guard let userInfo = response?.notification.request.content.userInfo else {
            completionHandler()
            return
        }
        (UIApplication.shared.delegate as! AppDelegate).remoteDataCompletion = completionHandler
        RemoteDataHandler.parseUserInfo(userInfo, nil)
    }
    
    fileprivate static func sendAmplitudeEvent (forUserInfo userInfo: [AnyHashable : Any]) {
        guard var data = userInfo["data"] as? [String : Any] else { return }
        guard let aps = userInfo["aps"] as? [String : Any] else { return }
        data["notification_text"] = (aps["alert"] as? String) ?? "NULL"
        AmplitudeEvents.log(event: "pushnotification:active", with: data)
    }
    
    fileprivate static func parseData(_ data: [String : Any], _ type: String) {
        
        switch type {
        case "invite":
                (UIApplication.shared.delegate as! AppDelegate).remoteDataHandlerDidReceiveRemoteAction(actionType: type)
        case "discover":
            let videoId = data["object_id"] as? String
            (UIApplication.shared.delegate as! AppDelegate).remoteDataHandlerDidReceiveRemoteAction(actionType: type,
                                                                                                    withObjectId: videoId)
        case "video", "cameo_invite":
            // video
            guard let videoId = data["object_id"] as? String else { return }
            (UIApplication.shared.delegate as! AppDelegate).remoteDataHandlerDidReceiveRemoteAction(actionType: type,
                                                                                                    withObjectId: videoId)
        case "comment":
            // comment
            guard let videoId = data["video_id"] as? String else { return }
            (UIApplication.shared.delegate as! AppDelegate).remoteDataHandlerDidReceiveRemoteAction(actionType: type,
                                                                                                    withObjectId: videoId)
        case "user":
            //user
            guard let userId = data["object_id"] as? String else { return }
            (UIApplication.shared.delegate as! AppDelegate).remoteDataHandlerDidReceiveRemoteAction(actionType: type,
                                                                                                    withObjectId: userId)
        case "record":
            let searchTerm = data["object_id"] as? String
            (UIApplication.shared.delegate as! AppDelegate).remoteDataHandlerDidReceiveRemoteAction(actionType: type,
                                                                                                    withObjectId: searchTerm)
        case "leaderboard":
            (UIApplication.shared.delegate as! AppDelegate).remoteDataHandlerDidReceiveRemoteAction(actionType: type)
            
        default:
            // something else??
            (UIApplication.shared.delegate as! AppDelegate).remoteDataHandlerDidFailToParseData(data)
        }
    }
    
}
