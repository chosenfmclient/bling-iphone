//
//  SIAProfileNewViewCell.m
//  ChosenIphoneApp
//
//  Created by Roni Shoham on 24/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAVideoTableViewCell.h"

#define IMAGE_PADDING_FROM_BORDER (0.)
#define IMAGE_SIZE_WIDTH (53.)
#define IMAGE_SIZE_HEIGHT (53.)
//old defines - image padding from border = (15.), image size width = (104.), image size height = (59.)
#define TEXT_LABEL_RIGHT_PADDING (30.)
#define TEXT_LABEL_LEFT_PADDING (10.)
#define TEXT_LABEL_LEFT_FROM_CONTAINER (IMAGE_SIZE_WIDTH + TEXT_LABEL_LEFT_PADDING)
#define THUMBNAIL_TOP_IMAGE_NAME @"play-alpha-icon.png"

@interface SIAVideoTableViewCell ()

@property (nonatomic, strong) UIImageView *playImageView;

@end

@implementation SIAVideoTableViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(IMAGE_PADDING_FROM_BORDER, IMAGE_PADDING_FROM_BORDER, IMAGE_SIZE_WIDTH, IMAGE_SIZE_HEIGHT);
    
    
    if (_parentVideoCell) {
        self.textLabel.numberOfLines = 2;
        self.detailTextLabel.numberOfLines = 1;
        self.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        self.detailTextLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    
    else {
        self.textLabel.numberOfLines = 3;
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.detailTextLabel.numberOfLines = 1;
    }
    
    [self.detailTextLabel sizeToFit];
    CGFloat textLabelWidth = self.frame.size.width - TEXT_LABEL_LEFT_FROM_CONTAINER - TEXT_LABEL_RIGHT_PADDING;
    CGRect textFrame = CGRectNull;
    if (self.textLabel.attributedText) {
        textFrame = [self.textLabel.attributedText boundingRectWithSize:CGSizeMake(textLabelWidth, self.contentView.bounds.size.height - self.detailTextLabel.bounds.size.height)
                                                                        options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading | NSStringDrawingTruncatesLastVisibleLine
                                                                context:nil];
        [self.textLabel.attributedText drawWithRect:textFrame
                                                  options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine
                                                  context:nil];
    }
    else {
        [self.textLabel sizeToFit];
    }
    
    CGFloat totalLabelHeight;
    if (CGRectIsNull(textFrame)) {
        totalLabelHeight = self.textLabel.frame.size.height + self.detailTextLabel.frame.size.height;
    }
    else {
        totalLabelHeight = self.detailTextLabel.frame.size.height + textFrame.size.height;
    }
    CGFloat textLabelOriginY = (self.frame.size.height - totalLabelHeight) / 2;
 
    if(CGRectIsNull(textFrame)) {
        self.textLabel.frame = CGRectMake(TEXT_LABEL_LEFT_FROM_CONTAINER, textLabelOriginY, textLabelWidth, self.textLabel.frame.size.height);
    }
    else {
        self.textLabel.frame = CGRectMake(TEXT_LABEL_LEFT_FROM_CONTAINER, textLabelOriginY, textFrame.size.width, textFrame.size.height);
    }
    CGFloat detailedTextLabelY = CGRectGetMaxY(self.textLabel.frame);
    self.detailTextLabel.frame = CGRectMake(TEXT_LABEL_LEFT_FROM_CONTAINER, detailedTextLabelY, textLabelWidth, self.detailTextLabel.frame.size.height);
    
    /*
    self.textLabel.frame = CGRectMake(90, textLabelOriginY, textLabelWidth, self.textLabel.frame.size.height);
    CGFloat detailedTextLabelY = CGRectGetMaxY(self.textLabel.frame);
    if (CGRectIsNull(detailFrame)) {
        self.detailTextLabel.frame = CGRectMake(90, detailedTextLabelY, textLabelWidth, self.detailTextLabel.frame.size.height);
    }
    else {
        self.detailTextLabel.frame = CGRectMake(90, detailedTextLabelY, detailFrame.size.width, detailFrame.size.height);
    }
     */
   
    self.textLabel.backgroundColor = [UIColor clearColor];
    
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.clipsToBounds = YES;
    self.imageView.backgroundColor = [UIColor blackColor];
    
    if (![self.contentView viewWithTag:100]) {
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.contentView.frame.size.height, self.contentView.frame.size.width, 1)];
        lineView.tag = 100;
        lineView.backgroundColor = SingrDirtyWhite;
        [self.contentView addSubview:lineView];
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    UIFont *font = kAppFontItalic(10.0);
//    self.videoTimeStamp.font = font;

}

@end
