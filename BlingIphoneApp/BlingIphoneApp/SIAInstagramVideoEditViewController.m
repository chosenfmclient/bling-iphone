//
//  SIAInstagramVideoEditViewController.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 4/2/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAInstagramVideoEditViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SDWebImageManager.h"
#import <Restkit/Restkit.h>
#import "UIImage+SIAAdditions.h"

@import AssetsLibrary;
@import Photos;



@interface SIAInstagramVideoEditViewController () <VideoPlayerManagerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *cropVideoScrollView;
@property (strong, nonatomic) UIView *playerView;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *resizeButton;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIView *progressContainerView;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImageView *frameImageView;
@property (strong, nonatomic) UIImage *squareFrameImage;
@property (strong, nonatomic) UIImage *videoAspectFrameImage;
@property (strong, nonatomic) UIView *originalPlayerSuperview;
@property (nonatomic) CGRect originalPlayerFrame;
@property (nonatomic) CGRect zoomedFrame;
@property (nonatomic, strong) AVAssetExportSession *exporter;
@property (strong, nonatomic) id playerItemFinishObserver;
@property (nonatomic) BOOL mediaAssetIsSquare;
@property (nonatomic) BOOL assetWasDownloaded;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation SIAInstagramExportAsset

@end

@implementation SIAInstagramVideoEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect scrollViewFrame = self.cropVideoScrollView.frame;
    scrollViewFrame.size = CGSizeMake(self.view.frame.size.width, self.view.frame.size.width);
    self.cropVideoScrollView.frame = scrollViewFrame;
    
    [self setUpMediaScrollView];
    
    CGRect resizeButtonFrame = self.resizeButton.frame;
    resizeButtonFrame.origin = CGPointMake(10, CGRectGetMaxY(scrollViewFrame) - resizeButtonFrame.size.height - 10);
    self.resizeButton.frame = resizeButtonFrame;
    CGRect postButtonFrame = self.postButton.frame;
    postButtonFrame.origin.y = (self.view.frame.size.height + CGRectGetMaxY(self.cropVideoScrollView.frame))/2 - postButtonFrame.size.height/2; // center it between bottom of scrollview and bottom of screen
    self.postButton.frame = postButtonFrame;
    CGRect progressContainerFrame = self.progressContainerView.frame;
    progressContainerFrame.origin.y = postButtonFrame.origin.y - progressContainerFrame.size.height - 15;
    self.progressContainerView.frame = progressContainerFrame;
    
//    if (CMTIME_COMPARE_INLINE(self.exportAsset.clipTimeRange.start, !=, self.exportAsset.clipTimeRange.duration))
//    {
//        [self repeatPlaybackWithRange:self.exportAsset.clipTimeRange];
//    }
//    else
//    {
//        [self.effectPlayer repeatPlayback];
//    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.postButton.layer.cornerRadius = self.postButton.bounds.size.height / 2;
    self.postButton.clipsToBounds = YES;
    self.postButton.backgroundColor = [UIColor colorWithRed:(39/255.0) green:(167/255.0) blue:(222/255.0) alpha:1];
}

//- (void)viewWillAppear:(BOOL)animated {
//    
//    CGRect scrollViewFrame = self.cropVideoScrollView.frame;
//    scrollViewFrame.size = CGSizeMake(self.view.frame.size.width, self.view.frame.size.width);
//    self.cropVideoScrollView.frame = scrollViewFrame;
//    
//    [self setUpMediaScrollView];
//    
//    [self.navigationController setNavigationBarHidden:NO
//                                             animated:NO];
//    [self.navigationController setNavigationBarLeftButtonBack];
//    self.navigationItem.title = @"Share to Instagram";
//    
//    self.postButton.layer.cornerRadius = self.postButton.bounds.size.height / 2;
//    self.postButton.clipsToBounds = YES;
//    
//
//    CGRect resizeButtonFrame = self.resizeButton.frame;
//    resizeButtonFrame.origin = CGPointMake(10, CGRectGetMaxY(scrollViewFrame) - resizeButtonFrame.size.height - 10);
//    self.resizeButton.frame = resizeButtonFrame;
//    CGRect postButtonFrame = self.postButton.frame;
//    postButtonFrame.origin.y = (self.view.frame.size.height + CGRectGetMaxY(self.cropVideoScrollView.frame))/2 - postButtonFrame.size.height/2; // center it between bottom of scrollview and bottom of screen
//    self.postButton.frame = postButtonFrame;
//    CGRect progressContainerFrame = self.progressContainerView.frame;
//    progressContainerFrame.origin.y = postButtonFrame.origin.y - progressContainerFrame.size.height - 15;
//    self.progressContainerView.frame = progressContainerFrame;
//    
//    if (CMTIME_COMPARE_INLINE(self.exportAsset.clipTimeRange.start, !=, self.exportAsset.clipTimeRange.duration))
//    {
//        [self repeatPlaybackWithRange:self.exportAsset.clipTimeRange];
//    }
//    else
//    {
//        [self.effectPlayer repeatPlayback];
//    }
//}

- (void)repeatPlaybackWithRange:(CMTimeRange)range{
    [self.effectPlayer repeatPlaybackFrom:CMTimeGetSeconds(range.start)
                                       to:CMTimeGetSeconds(range.duration)];
}

- (IBAction)back:(UIButton *)sender {
    
    
    if (self.exporter.status == AVAssetExportSessionStatusExporting)
    {
        [self.exporter cancelExport];
    }
    [self.effectPlayer stopRepeatingPlayback];
    [self.effectPlayer pause];
    [VideoPlayerManager shared].playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.presentingViewController dismissViewControllerAnimated:YES
                                                      completion:^{
                                                          [Utils dispatchOnMainThread:^{
                                                              self.playerView.frame = self.originalPlayerFrame;
                                                              [self.playerView setNeedsLayout];
                                                              [self.originalPlayerSuperview addSubview:self.playerView];
                                                              self.originalPlayerSuperview = nil;
                                                              [self.effectPlayer pause];
                                                              self.effectPlayer = nil;
                                                          }];
                                                      }];
}

- (void)viewDidLayoutSubviews {
    [self.postButton setTitle:@"POST TO INSTAGRAM"
                     forState:UIControlStateNormal];
    [self.postButton setTitleColor:[UIColor whiteColor]
                          forState:UIControlStateNormal];
    self.titleLabel.text = @"Post To Instagram";
}


- (void)setEffectPlayer:(BIAVideoPlayer *)effectPlayer {
    _effectPlayer = effectPlayer;
    
//    self.originalPlayerFrame = playerView.frame;
//    self.originalPlayerSuperview = playerView.superview;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setBlurredBackgroundImage:(UIImage *)imageToBlur{
    
    UIImage *blurredImage = [[SIAStyling defaultStyle] blurredImageFromImage:imageToBlur];
    UIImageView *blurredImageView = [[UIImageView alloc] initWithImage:blurredImage];
    blurredImageView.frame = self.view.bounds;
    blurredImageView.contentMode = UIViewContentModeScaleAspectFill;
    [UIView transitionWithView:self.view
                      duration:0.15
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                            [self.view insertSubview:blurredImageView
                                             atIndex:0];
                    } completion:nil];
}

- (void)setBlurredBackgroundImageFromVideo:(AVURLAsset *)asset {
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    [generator generateCGImagesAsynchronouslyForTimes:@[[NSValue valueWithCMTime:kCMTimeZero]]
                                    completionHandler:^(CMTime requestedTime, CGImageRef  _Nullable image, CMTime actualTime, AVAssetImageGeneratorResult result, NSError * _Nullable error) {
                                        UIImage *imageToBlur = [UIImage imageWithCGImage:image];
                                        [Utils dispatchOnMainThread:^{
                                            [self setBlurredBackgroundImage:imageToBlur];
                                        }];
                                    }];
}

- (void)setUpMediaScrollView {
    
    CGSize mediaSize;
    BOOL isLandscape;
    
    if (self.image)
    {
        mediaSize = self.image.size;
        CGFloat maxWidth = [self instagramMaxImageWidth];
        if (mediaSize.width > maxWidth)
        {
            CGFloat scale = maxWidth / mediaSize.width ;
            self.image = [UIImage scaleImage:self.image
                                    isOpaque:YES
                                       scale:scale];
            mediaSize = self.image.size;   
        }
        
        self.imageView = [[UIImageView alloc] initWithImage:self.image];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self setBlurredBackgroundImage:self.image];
        isLandscape = mediaSize.width > mediaSize.height;
    }
    else
    {
        AVURLAsset *asset = [AVURLAsset URLAssetWithURL:self.exportAsset.assetURL
                                                options:nil];
        AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject];
        mediaSize = [self transformedSizeForVideoTrack:videoTrack];
        isLandscape = [self videoIsLandscape:videoTrack];
        [self setBlurredBackgroundImageFromVideo:asset];
    }
    
    if (mediaSize.width == 0 || mediaSize.height == 0) return;
    
    self.mediaAssetIsSquare = mediaSize.width == mediaSize.height;
    
    CGRect filterViewFrame;
    
    if (isLandscape)
    {
        CGFloat filterViewHeight = self.cropVideoScrollView.bounds.size.height;
        filterViewFrame  = CGRectMake(0,
                                      0,
                                      floorf(mediaSize.width * (filterViewHeight / mediaSize.height)),
                                      filterViewHeight);
        
        self.playerView = [[UIView alloc] initWithFrame:filterViewFrame];
        self.imageView.frame = filterViewFrame;
        self.cropVideoScrollView.contentOffset = CGPointMake(filterViewFrame.size.width / 4, 0);
    }
    else
    {
        CGFloat filterViewWidth = self.view.bounds.size.width;
        filterViewFrame = CGRectMake(0,
                                     0,
                                     filterViewWidth,
                                     floorf(mediaSize.height * (filterViewWidth / mediaSize.width)));
        
        self.playerView = [[UIView alloc] initWithFrame:filterViewFrame];
        self.imageView.frame = filterViewFrame;
        self.cropVideoScrollView.contentOffset = CGPointMake(0, filterViewFrame.size.height / 4);
    }
    self.playerView.autoresizingMask = UIViewAutoresizingNone;
    self.playerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cropVideoScrollView.contentSize = filterViewFrame.size;
    self.effectPlayer = [VideoPlayerManager shared].player;
    [VideoPlayerManager shared].playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    [self.cropVideoScrollView addSubview:self.playerView];
    [self.playerView.widthAnchor constraintEqualToConstant:filterViewFrame.size.width].active = YES;
    [self.playerView.heightAnchor constraintEqualToConstant:filterViewFrame.size.height].active = YES;
    [self.playerView.leadingAnchor constraintEqualToAnchor:self.cropVideoScrollView.leadingAnchor].active = YES;
    //[self.playerView.centerXAnchor constraintEqualToAnchor:_cropVideoScrollView.centerXAnchor].active = YES;
    [self.playerView.centerYAnchor constraintEqualToAnchor:_cropVideoScrollView.centerYAnchor].active = YES;
    [self.playerView setNeedsLayout];
    [[VideoPlayerManager shared] setShouldPlay:YES];
    [Utils dispatchOnMainThread:^{
        if (self.exportAsset.asset)
        {
            [[VideoPlayerManager shared] setNewPlayerViewWithView:self.playerView
                                                            asset:self.exportAsset.asset
                                                     withDelegate:self
                                                  shouldLimitTime:YES
                                                    withSpinner:NO];
        }
        else {
            [[VideoPlayerManager shared] setNewPlayerViewWithView:self.playerView
                                                         videoURL:self.exportAsset.assetURL
                                                     withDelegate:self
                                                  shouldLimitTime:YES
                                                      usingOption:VideoPlayerAssetManagerOptionsStream
                                                      withSpinner:NO];
            
        }
        [[VideoPlayerManager shared] layoutVideo];
    }];
}

- (void)playerDidReachEnd {
    
}


- (IBAction)postToInstagramButtonWasTapped:(UIButton *)sender {
    
    self.cropVideoScrollView.scrollEnabled = NO;
    sender.enabled = NO;
    
    /*[Flurry logEvent:FLURRY_TAP_POST_INSTAGRAM];*/
    
    if (self.image)
    {
        [self postImage];
    }
    else
    {
        [self postVideo];
    }
}

- (void)postVideo {
    __weak SIAInstagramVideoEditViewController *wSelf = self;
    [self downloadVideoFileWithCompletion:^(BOOL completed) {
        if (!completed) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"An error occurred. Please try again later" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        [wSelf saveCroppedVideoWithCompletion:^(BOOL completed, NSURL *outputURL) {
            if (completed && outputURL)
                [wSelf saveVideoToCameraRoll:outputURL
                             completed:^(NSURL *assetURL) {
                                 wSelf.cropVideoScrollView.scrollEnabled = YES;
                                 [wSelf.effectPlayer stopRepeatingPlayback];
                                 [Utils dispatchOnMainThread:^{
                                    [wSelf postToInstagram:assetURL];
                                 }];
                             }];
            else
            {
                wSelf.cropVideoScrollView.scrollEnabled = YES;
                // whatever happens when it fails
            }
        }];
    }];
}

- (void)postImage {
    
    self.image = [self croppedPhotoWithWatermark];
    [self savePhotoToCameraRollWithCompletion:^(NSURL *assetURL) {
        [self postToInstagram:assetURL];
    }];
    
}

- (IBAction)resizeButtonWasTapped:(UIButton *)sender {
    
    if (self.mediaAssetIsSquare) return; // already square, no zooming to do!
    
    [self toggleZoom];
}

- (void)toggleZoom {
    
    CGRect newFrame;
    CGRect currentFrame;
    UIImage *frameImage;
    BOOL scrollEnabled;
    
    if (self.image)
    {
        currentFrame = self.imageView.frame;
    }
    else
    {
        currentFrame = self.playerView.frame;
    }

    if (currentFrame.size.width == currentFrame.size.height)
    {
        newFrame = self.zoomedFrame;
        scrollEnabled = YES;
        frameImage = self.squareFrameImage;
    }
    else
    {
        self.zoomedFrame = currentFrame;
        newFrame = self.cropVideoScrollView.bounds;
        scrollEnabled = NO;
        frameImage = self.videoAspectFrameImage;
    }
    self.cropVideoScrollView.scrollEnabled = scrollEnabled;
    self.cropVideoScrollView.contentSize = newFrame.size;
    //self.playerView.frame = newFrame;
    [NSLayoutConstraint deactivateConstraints:self.playerView.constraints];
    [self.playerView setNeedsUpdateConstraints];
    [self.playerView updateConstraints];
    //[self.playerView removeConstraints:self.playerView.constraints];
    [self.playerView.widthAnchor constraintEqualToConstant:newFrame.size.width].active = YES;
    [self.playerView.heightAnchor constraintEqualToConstant:newFrame.size.height].active = YES;
    [self.playerView.leadingAnchor constraintEqualToAnchor:self.cropVideoScrollView.leadingAnchor].active = YES;
    //[self.playerView.centerXAnchor constraintEqualToAnchor:_cropVideoScrollView.centerXAnchor].active = YES;
    [self.playerView.centerYAnchor constraintEqualToAnchor:_cropVideoScrollView.centerYAnchor].active = YES;
    [self.playerView setNeedsUpdateConstraints];
    [self.playerView updateConstraints];
    [self.playerView setNeedsLayout];
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.imageView.frame = newFrame;
                         self.frameImageView.image = frameImage;
                         [self.playerView layoutIfNeeded];
                         [[VideoPlayerManager shared] layoutVideo];
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

- (void)savePhotoToCameraRollWithCompletion:(void(^)(NSURL *assetURL))handler {
    
    ALAssetsLibrary *assetsLib = [[ALAssetsLibrary alloc] init];
    
    [assetsLib writeImageToSavedPhotosAlbum:self.image.CGImage
                                orientation:ALAssetOrientationUp
                            completionBlock:^(NSURL *assetURL, NSError *error) {
                                if (!error)
                                    handler(assetURL);
                                else if (error.code == ALAssetsLibraryAccessUserDeniedError || error.code == ALAssetsLibraryAccessGloballyDeniedError)
                                {
                                    SIAAlertController *alertController = [SIAAlertController alertControllerWithTitle:@"Camera Roll Permission"
                                                                                                               message:@"Please allow Blingy access to your camera roll in your phone's privacy settings in order to post to Instagram."];
                                    SIAAlertAction *alertAction  = [SIAAlertAction actionWithTitle:@"OK"
                                                                                             style:SIAAlertActionStyleCancel
                                                                                           handler:nil];
                                    [alertController addAction:alertAction];
                                    [self presentViewController:alertController
                                                       animated:YES
                                                     completion:nil];
                                }
                                
                            }];
}

- (void)saveVideoToCameraRoll:(NSURL *)outputURL completed:(void(^)(NSURL *assetURL))handler {
    __weak SIAInstagramVideoEditViewController *wSelf = self;
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if (status == PHAuthorizationStatusAuthorized) {
            ALAssetsLibrary *assetLib = [[ALAssetsLibrary alloc] init];
            [assetLib writeVideoAtPathToSavedPhotosAlbum:outputURL
                                         completionBlock:^(NSURL *assetURL, NSError *error) {
                                             handler(assetURL);
                                         }];
        }
        else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Camera Roll Permission"
                                                                           message:@"Please allow Blin.gy to access your camera roll in your privacy settings in order to post to Instagram"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                      style:UIAlertActionStyleCancel
                                                    handler:nil]];
            [wSelf presentViewController:alert
                                animated:YES
                              completion:nil];
        }
    }];
}

- (UIImage *)croppedPhotoWithWatermark {
    
    if (CGRectEqualToRect(self.cropVideoScrollView.bounds, self.imageView.frame))
    {
        //unzoomed, full image
        self.image = [self drawFullImageInSquare];
    }
    else
    {
        // zoomed, square image
        self.image = [self cropImageToSquareWithWatermark:self.image.CGImage];
        
    }
    
    return self.image;
}

- (UIImage *)drawFullImageInSquare {
    CGFloat height = self.image.size.height;
    CGFloat width = self.image.size.width;
    
    CGFloat squareSide = MAX(width, height);
    CGFloat shorterSide = MIN(width, height);
    
    CGRect imageRect;
    
    CGFloat watermarkWidth = shorterSide * 0.42;
    
    if (shorterSide == height) // landscape
    {
        imageRect = CGRectMake(0,
                               squareSide / 2 - shorterSide / 2,
                               squareSide,
                               shorterSide);
        
    }
    else
    {
        imageRect = CGRectMake(squareSide / 2 - shorterSide / 2,
                               0,
                               shorterSide,
                               squareSide);
    }
    
    if (self.frameId)
    {
        return [self drawFrameOnFullImage:self.image
                               squareSide:squareSide
                                imageRect:imageRect];
    }
    else
    {
        return [self drawWatermarkOnFullImage:self.image
                                   squareSide:squareSide
                                    imageRect:imageRect
                               watermarkWidth:watermarkWidth];
    }
    
}

- (UIImage *)drawWatermarkOnFullImage:(UIImage *)image
                           squareSide:(CGFloat)squareSide
                            imageRect:(CGRect)imageRect
                       watermarkWidth:(CGFloat)watermarkWidth {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(squareSide, squareSide), YES, 0.0);
    
    CALayer *blackLayer = [CALayer layer];
    blackLayer.frame = CGRectMake(0, 0, squareSide, squareSide);
    blackLayer.backgroundColor = [UIColor blackColor].CGColor;
    [blackLayer renderInContext:UIGraphicsGetCurrentContext()];
    [image drawInRect:imageRect];
    UIImage *watermark = [UIImage imageNamed:@"watermark5x2.png"];
    CGFloat watermarkAspectRatio = watermark.size.width / watermark.size.height;
    CGFloat watermarkHeight = watermarkWidth / watermarkAspectRatio;
    [watermark drawInRect:CGRectMake(CGRectGetMaxX(imageRect) - watermarkWidth,
                                     CGRectGetMaxY(imageRect) - watermarkHeight,
                                     watermarkWidth,
                                     watermarkHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)drawFrameOnFullImage:(UIImage *)image
                       squareSide:(CGFloat)squareSide
                        imageRect:(CGRect)imageRect {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(squareSide, squareSide), YES, 0.0);
    CALayer *blackLayer = [CALayer layer];
    blackLayer.frame = CGRectMake(0, 0, squareSide, squareSide);
    blackLayer.backgroundColor = [UIColor blackColor].CGColor;
    [blackLayer renderInContext:UIGraphicsGetCurrentContext()];
    [image drawInRect:imageRect];
    UIImage *frame = self.videoAspectFrameImage;
    [frame drawInRect:imageRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (UIImage *)cropImageToSquareWithWatermark:(CGImageRef)image {
    size_t height = CGImageGetHeight(image);
    size_t width = CGImageGetWidth(image);
    
    NSUInteger squareSide = MIN(width, height);
    NSUInteger longerSide = MAX(width, height);
    
    CGRect squareRect;
    
    if (longerSide == width) // landscape
    {
        squareRect = CGRectMake(self.cropVideoScrollView.contentOffset.x * (squareSide / self.cropVideoScrollView.bounds.size.width),
                                0,
                                squareSide,
                                squareSide);
    }
    else
    {
        squareRect = CGRectMake(0,
                                self.cropVideoScrollView.contentOffset.y * (squareSide / self.cropVideoScrollView.bounds.size.height),
                                squareSide,
                                squareSide);
    }
    
    UIImage *croppedImage = [UIImage imageWithCGImage:CGImageCreateWithImageInRect(image, squareRect)];

    CGRect squareImageRect = CGRectMake(0, 0, squareSide, squareSide);
    
    UIImage *processedImage;
    
    if (self.frameId)
    {
        processedImage = [self drawFrameOnSquareImage:croppedImage
                                               inRect:squareImageRect];
    }
    else
    {
        processedImage = [self drawWatermarkOnSquareImage:croppedImage
                                                   inRect:squareImageRect];
    }
    
    return processedImage;
}

- (UIImage *)drawWatermarkOnSquareImage:(UIImage *)image
                                 inRect:(CGRect)squareImageRect {
    
    CGFloat squareSide = squareImageRect.size.width;
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0);
    [image drawInRect:squareImageRect];
    UIImage *watermark = [UIImage imageNamed:@"watermark5x2.png"];
    CGFloat watermarkAspectRatio = watermark.size.width / watermark.size.height;
    CGFloat watermarkWidth = squareSide * 0.42;
    CGFloat watermarkHeight = watermarkWidth / watermarkAspectRatio;
    [watermark drawInRect:CGRectMake(squareSide - watermarkWidth,
                                     squareSide - watermarkHeight,
                                     watermarkWidth,
                                     watermarkHeight)];
    UIImage *croppedImageWithWatermark = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return croppedImageWithWatermark;
}

- (UIImage *)drawFrameOnSquareImage:(UIImage *)image
                             inRect:(CGRect)squareImageRect {
    
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0);
    [image drawInRect:squareImageRect];
    [self.squareFrameImage drawInRect:squareImageRect];
    UIImage *croppedImageWithFrame = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return croppedImageWithFrame;
}

- (void)saveCroppedVideoWithCompletion:(void(^)(BOOL completed, NSURL *outputURL))completed {
    
    AVAsset *asset = _assetWasDownloaded ? [AVURLAsset assetWithURL:self.exportAsset.assetURL] : self.effectPlayer.currentItem.asset;
    
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject];

    AVMutableVideoComposition *videoComp = [AVMutableVideoComposition videoCompositionWithPropertiesOfAsset:asset];
    
    CGFloat squareSide = [self squareSide];
    
    videoComp.renderSize = CGSizeMake(squareSide, squareSide);
    
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    CMTime beginTime = kCMTimeZero;
    CMTime endTime;
    if (CMTIME_COMPARE_INLINE(asset.duration, >, CMTimeMake(15, 1)))
         endTime = CMTimeMake(15, 1);
    else endTime = asset.duration;
    
    CMTimeRange timeRange;
    if (CMTIME_COMPARE_INLINE(self.exportAsset.exportTimeRange.duration, !=, self.exportAsset.exportTimeRange.start))
    {
        timeRange = self.exportAsset.exportTimeRange;
    }
    else
    {
        timeRange = CMTimeRangeMake(beginTime, CMTimeSubtract(endTime, beginTime));
    }
    instruction.timeRange = timeRange;
    
    AVMutableVideoCompositionLayerInstruction *transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    
    CGAffineTransform cropTransform;
    
    if ([self videoIsZoomed])
    {
        if ([self videoIsLandscape:videoTrack])
            cropTransform = [self cropTransformForZoomedLandscapeVideo:videoTrack];
        else
            cropTransform = [self cropTransformForZoomedVerticalVideo:videoTrack];
    }
    else
    {
        if ([self videoIsLandscape:videoTrack])
            cropTransform = [self cropTransformForUnzoomedLandscapeVideo:videoTrack];
        else
            cropTransform = [self cropTransformForUnzoomedVerticalVideo:videoTrack];
    }
    
    [transformer setTransform:cropTransform
                       atTime:kCMTimeZero];
    
    instruction.layerInstructions = @[transformer];
    videoComp.instructions = @[instruction];
    AVVideoCompositionCoreAnimationTool *animationTool = [self waterMarkAnimationToolForVideoTrack:videoTrack
                                                                                 withCropTransform:cropTransform];
    
    if (!animationTool) {
        completed(NO, nil);
        return;
    }
    
    videoComp.animationTool = animationTool;
//    if (!_assetWasDownloaded && self.exportAsset.hasNoRecordedAudio)
//    {
//        AVMutableComposition *composition = [AVMutableComposition composition];
//        
//        NSError* error = NULL;
//        
//        CMTimeRange range = CMTimeRangeMake(kCMTimeZero,asset.duration);
//        
//        [composition insertTimeRange:range ofAsset:asset atTime:kCMTimeZero error:&error];
//        
//        AVMutableCompositionTrack *compositionPlaybackAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
//        
//        [compositionPlaybackAudioTrack insertTimeRange:range
//                                               ofTrack:[[self.effectPlayer.currentItem.asset tracksWithMediaType:AVMediaTypeAudio]objectAtIndex:0]
//                                                atTime:kCMTimeZero
//                                                 error:&error];
//        
//        _exporter = [[AVAssetExportSession alloc] initWithAsset:composition
//                                                     presetName:AVAssetExportPresetHighestQuality];
//    }
//    
//    else
//    {
        _exporter = [[AVAssetExportSession alloc] initWithAsset:asset
                                                     presetName:AVAssetExportPresetHighestQuality];

//    }

    _exporter.videoComposition = videoComp;
    _exporter.timeRange = timeRange;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"croppedVideo.mp4"];
    NSURL *outputURL = [NSURL fileURLWithPath:filePath];
    [[NSFileManager defaultManager] removeItemAtURL:outputURL
                                              error:nil];
    
    _exporter.outputURL = outputURL;
    _exporter.outputFileType = AVFileTypeMPEG4;
    
    [Utils dispatchOnMainThread:^{
        NSTimer *progressUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.2
                                                                        target:self
                                                                      selector:@selector(updateExportProgress:)
                                                                      userInfo:nil
                                                                       repeats:YES];
        
        
        self.progressLabel.text = @"Exporting...";
        [self.progressView setProgress:0
                              animated:NO];
        [_exporter exportAsynchronouslyWithCompletionHandler:^{
            
            [progressUpdateTimer invalidate];
            
            switch ([_exporter status]) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"EXPORT FAIL WAAAHHHH");
                    if (completed)
                        completed(NO, nil);
                    return;
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"EXPORT CANCELLED WHY");
                    if (completed)
                        completed(NO, nil);
                    return;
                default:
                    break;
            }
            if (completed)
                completed(YES, _exporter.outputURL);
            self.progressLabel.text = @"Done!";
        }];
    }];
}

- (void)updateExportProgress:(NSTimer *)timer
{
    CGFloat progress = _exporter.progress * 100;
    [self.progressView setProgress:_exporter.progress
                          animated:YES];
    NSLog(@"Export Progress: %i", (int)progress);
    
}
- (void)downloadVideoFileWithCompletion:(void(^)(BOOL completed))completed {
    
    if ([self mediaIsLocalAsset]) {
        completed(YES);
        return;
    }
    _assetWasDownloaded = YES;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pathExtension = self.exportAsset.assetURL.pathExtension;
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, [NSString stringWithFormat:@"downloadedVideo.%@", pathExtension]];
    NSURL *outputURL = [NSURL fileURLWithPath:filePath];
    [[NSFileManager defaultManager] removeItemAtURL:outputURL
                                              error:nil];
    
    NSURLRequest *videoRequest = [NSURLRequest requestWithURL:self.exportAsset.assetURL];
    AFRKHTTPRequestOperation *operation =   [[AFRKHTTPRequestOperation alloc] initWithRequest:videoRequest];

    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    
    self.progressLabel.text = @"Downloading...";
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float progress = (float)totalBytesRead / (float)totalBytesExpectedToRead;
        [self.progressView setProgress:progress
                              animated:YES];
    }];
    
    [operation setCompletionBlock:^{
        self.exportAsset.assetURL = outputURL;
        if (operation.error) {
            completed(NO);
        } else {
            completed(YES);

        }
    }];
    [operation start];
}

- (CGAffineTransform)cropTransformForZoomedVerticalVideo:(AVAssetTrack *)videoTrack {
    CGFloat squareSide = [self squareSide];
    CGSize transformedSize = [self transformedSizeForVideoTrack:videoTrack];
    CGFloat scale = squareSide/transformedSize.width;
    CGPoint translation = CGPointMake(0, -(self.cropVideoScrollView.contentOffset.y * (squareSide / self.cropVideoScrollView.bounds.size.width)));
    
    return [self applyScale:scale
                translation:translation
           toVideoTransform:[videoTrack preferredTransform]];
}

- (CGAffineTransform)cropTransformForUnzoomedVerticalVideo:(AVAssetTrack *)videoTrack {
    CGFloat squareSide = [self squareSide];
    CGSize transformedSize = [self transformedSizeForVideoTrack:videoTrack];
    CGFloat scale = squareSide / transformedSize.height;
    CGPoint translation = CGPointMake(squareSide / 2 - transformedSize.width * scale / 2, 0);
    
    return [self applyScale:scale
                translation:translation
           toVideoTransform:[videoTrack preferredTransform]];
}

- (CGAffineTransform)cropTransformForZoomedLandscapeVideo:(AVAssetTrack *)videoTrack {
    CGFloat squareSide = [self squareSide];
    CGSize transformedSize = [self transformedSizeForVideoTrack:videoTrack];
    CGFloat scale = squareSide/transformedSize.height;
    CGPoint translation = CGPointMake(-(self.cropVideoScrollView.contentOffset.x * (squareSide / self.cropVideoScrollView.bounds.size.height)), 0);
    
    return [self applyScale:scale
                translation:translation
           toVideoTransform:[videoTrack preferredTransform]];
}

- (CGAffineTransform)cropTransformForUnzoomedLandscapeVideo:(AVAssetTrack *)videoTrack {
    CGFloat squareSide = [self squareSide];
    CGSize transformedSize = [self transformedSizeForVideoTrack:videoTrack];
    CGFloat scale = squareSide / transformedSize.width;
    CGPoint translation = CGPointMake(0, squareSide / 2 - transformedSize.height * scale / 2);
    
    return [self applyScale:scale
                translation:translation
           toVideoTransform:[videoTrack preferredTransform]];
}
//
//- (CGAffineTransform)squareTransformForAssetTrack:(AVAssetTrack *)assetTrack {
//    CGSize videoSize = assetTrack.naturalSize;
//    
//}

- (CGAffineTransform)applyScale:(CGFloat)scale
                    translation:(CGPoint)translation
               toVideoTransform:(CGAffineTransform)preferredTransform {
    
    CGAffineTransform scaleTransform = CGAffineTransformMakeScale(scale, scale);
    CGAffineTransform translationTransform = CGAffineTransformMakeTranslation(translation.x, translation.y);
    CGAffineTransform cropTransform = CGAffineTransformConcat(preferredTransform, scaleTransform);
    cropTransform = CGAffineTransformConcat(cropTransform, translationTransform);

    return cropTransform;
}

- (void)postToInstagram:(NSURL *)assetURL {
    NSString *caption = @"HAHA";
    
    
    NSString *escapedString = [assetURL.absoluteString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    escapedString = [escapedString stringByReplacingOccurrencesOfString:@"="
                                                             withString:@"%3D"];
    escapedString = [escapedString stringByReplacingOccurrencesOfString:@"&"
                                                             withString:@"%26"];
    escapedString = [escapedString stringByAppendingString:@"&ext=mp4"];
    NSString *escapedCaption  = [caption stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *urlString = [NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@", escapedString, escapedCaption];
    NSURL *instagramURL = [NSURL URLWithString:urlString];
    
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        [[UIApplication sharedApplication] openURL:instagramURL];
        [self back:nil];
    }
    else
    {
        SIAAlertController *alertController = [SIAAlertController alertControllerWithTitle:@"Instagram"
                                                                                   message:@"You have not installed Instagram on this device."];
        SIAAlertAction *alertAction  = [SIAAlertAction actionWithTitle:@"OK"
                                                                 style:SIAAlertActionStyleCancel
                                                               handler:nil];
        [alertController addAction:alertAction];
        [self presentViewController:alertController
                           animated:YES
                         completion:nil];
    }
}

- (BOOL)videoIsLandscape:(AVAssetTrack *)videoTrack {
    CGSize transformedSize = [self transformedSizeForVideoTrack:videoTrack];
    return transformedSize.width > transformedSize.height;
}

// this gives the true size whether video is vertical or landscape by rotating if needed
- (CGSize)transformedSizeForVideoTrack:(AVAssetTrack *)videoTrack {
    CGRect rectForTransform = CGRectZero;
    rectForTransform.size = videoTrack.naturalSize;
    
    rectForTransform = CGRectApplyAffineTransform(rectForTransform, [videoTrack preferredTransform]);
    
    return rectForTransform.size;
}

- (CGFloat)squareSide {
    return 640.0;
}

- (CGFloat)instagramMaxImageWidth {
    return 1080.0;
}

- (AVVideoCompositionCoreAnimationTool *)waterMarkAnimationToolForVideoTrack:(AVAssetTrack *)videoTrack
                                                           withCropTransform:(CGAffineTransform)cropTransform {
    
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    
    CGRect videoLayerFrame = CGRectMake(0, 0, [self squareSide], [self squareSide]);
    
    
    parentLayer.frame = videoLayerFrame;
    videoLayer.frame = videoLayerFrame;
    [parentLayer addSublayer:videoLayer];
    
    CGRect transformedVideoRect = CGRectZero;
    transformedVideoRect.size = videoTrack.naturalSize;
    transformedVideoRect = CGRectApplyAffineTransform(transformedVideoRect, cropTransform);

    if (CGRectIsEmpty(transformedVideoRect) || CGRectIsNull(transformedVideoRect)){
        return nil;
    }
    
    if (self.frameId)
    {
        [parentLayer addSublayer:[self frameLayerForVideoRect:transformedVideoRect]];
    }
    else
    {
        [parentLayer addSublayer:[self waterMarkLayerForVideoRect:transformedVideoRect]];
    }
    
    return [AVVideoCompositionCoreAnimationTool
            videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer
            inLayer:parentLayer];
}

- (CALayer *)frameLayerForVideoRect:(CGRect)transformedVideoRect {
    UIImage *myImage;
    if ([self videoIsZoomed])
    {
        myImage = self.squareFrameImage;
    }
    else
    {
        myImage = self.videoAspectFrameImage;
    }
    CALayer *frameLayer = [CALayer layer];
    frameLayer.contents = (id)myImage.CGImage;

    CGFloat frameWidth = MIN(transformedVideoRect.size.width, [self squareSide]);
    CGFloat frameHeight = MIN(transformedVideoRect.size.height, [self squareSide]);
    
    CGFloat horizontalAdjustment = frameWidth < [self squareSide] ? [self squareSide] / 2 - frameWidth / 2 : 0;
    CGFloat verticalAdjustment = frameHeight < [self squareSide] ? [self squareSide] / 2 - frameHeight / 2 : 0;
    
    frameLayer.frame = CGRectMake(horizontalAdjustment,
                                  verticalAdjustment,
                                  frameWidth,
                                  frameHeight);
    return frameLayer;
}

- (CALayer *)waterMarkLayerForVideoRect:(CGRect)transformedVideoRect {
    
    UIImage *myImage = [UIImage imageNamed:self.isCameoShare ? @"watermark_duet" : @"watermark7"];
    CALayer *logoLayer = [CALayer layer];
    logoLayer.frame = CGRectMake(0, 0, 1, 1);
    logoLayer.contents = (id)myImage.CGImage; // lets do this stuff after we have a watermark
    logoLayer.contentsScale = 2.0;
    logoLayer.rasterizationScale = 2.0;
    logoLayer.shouldRasterize = YES;
    CGFloat imageRatio = myImage.size.height / myImage.size.width;
    // set the logo size
    // .width is 9% from the video width for 16:9 videos which are most of our videos.
    CGFloat logoWidth = [self squareSide] * 0.25;
    CGFloat logoHeight = logoWidth * imageRatio;
    logoWidth = roundf(logoWidth);
    logoHeight = roundf(logoHeight);
    
    // if the video isn't zoomed to fill the square, adjust the image position to be inside the rendered video
    CGFloat horizontalAdjustment = transformedVideoRect.size.width < [self squareSide] ? [self squareSide] / 2 - transformedVideoRect.size.width / 2 : 0;
    CGFloat verticalAdjustment = transformedVideoRect.size.height < [self squareSide] ? [self squareSide] / 2 - transformedVideoRect.size.height / 2 : 0;
    
    CGRect logoFrame = CGRectMake(horizontalAdjustment,
                                  verticalAdjustment,
                                  logoWidth,
                                  logoHeight);
    
    
    logoLayer.frame = logoFrame;
    return logoLayer;
}

/*- (void)loadFrameImageForAspectRatio:(CGFloat)aspectRatio {
    [SIAFramesDataModel getFrames:^(SIAFramesDataModel *model) {
        
        NSURL *squareFrameURL;
        NSURL *videoAspectFrameURL;
        
        for (SIAFrame *frame in model.frames)
        {
            if (frame.frame_id == self.frameId)
            {
                squareFrameURL =  [frame.frames objectForKey:@"1x1"];
                videoAspectFrameURL = [frame.frames objectForKey:[SIAFramesDataModel aspectRatioKeyForAspectRatio:aspectRatio]];
            }
        }
        
        self.frameImageView = [[UIImageView alloc] initWithFrame:self.cropVideoScrollView.frame];
        self.frameImageView.backgroundColor = [UIColor clearColor];
        self.frameImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.view insertSubview:self.frameImageView
                    belowSubview:self.resizeButton];
        
         [[SDWebImageManager sharedManager] downloadImageWithURL:videoAspectFrameURL
                                                         options:SDWebImageRefreshCached
                                                        progress:nil
                                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                           self.videoAspectFrameImage = image;
                                                       }];
        
        [self.frameImageView sd_setImageWithURL:squareFrameURL
                               placeholderImage:nil
                                        options:SDWebImageRefreshCached
                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                          self.squareFrameImage = image;
                                          [UIView transitionWithView:self.frameImageView
                                                            duration:0.15
                                                             options:UIViewAnimationOptionTransitionCrossDissolve
                                                          animations:^{
                                                              [self.frameImageView setImage:image];
                                                          } completion:nil];
                                      }];
    }];
}*/

- (BOOL)mediaIsLocalAsset {
    return [self.exportAsset.assetURL.scheme isEqualToString:@"file"] || [self.exportAsset.assetURL.scheme isEqualToString:@"assets-library"];
}

- (BOOL)videoIsZoomed {
    return !CGRectEqualToRect(self.cropVideoScrollView.bounds, self.playerView.frame);
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    NSLog(@"Totally deallocked instagram vc yo");
}


@end
