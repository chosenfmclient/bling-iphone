//
//  SIAHorizontalSelectionScrollView.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 5/4/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIASelectableView.h"

typedef enum : NSUInteger {
    SIAHorizontalSelectionScrollViewStyleNone = 0,
    SIAHorizontalSelectionScrollViewStyleButtonBorderHighlights
} SIAHorizontalSelectionScrollViewStyle;

@interface SIAHorizontalSelectionScrollView : UIScrollView

@property (strong, nonatomic) NSArray <SIASelectableView *> *views;

- (id)initWithFrame:(CGRect)frame
              views:(NSArray <SIASelectableView *> *)views
              style:(SIAHorizontalSelectionScrollViewStyle)style;

- (void)selectButtonAtIndex:(NSUInteger)index;

@end
