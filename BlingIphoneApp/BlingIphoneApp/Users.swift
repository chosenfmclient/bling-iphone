//
//  Users.swift
//  BlingIphoneApp
//
//  Created by Zach on 14/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class Users: NSObject {
    var data = [SIAUser]()
    static func objectMapping() -> RKObjectMapping {
        let mapping = RKObjectMapping(for: Users.self)
        
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "data", toKeyPath: "data", with: SIAUser.objectMapping()))
        
        return mapping!
    }
}
