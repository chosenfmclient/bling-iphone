//
//  SIAVideoUploadDataModel.h
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 8/17/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface SIAVideoUploadDataModel : NSObject

@property (nonatomic, strong) NSString *video_signed_url;
@property (nonatomic, strong) NSString *thumbnail_signed_url;
@property (nonatomic, strong) NSString *video_method;
@property (nonatomic, strong) NSString *thumbnail_method;
@property (nonatomic, strong) NSString *video_content_type;
@property (nonatomic, strong) NSString *thumbnail_content_type;
@property (nonatomic, strong) NSString *video_id;
@property (nonatomic, strong) NSURL *soundtrack_url;

+(RKObjectMapping *)objectMapping;


@end
