//
//  SIAProfileNewViewCell.h
//  ChosenIphoneApp
//
//  abstract : for customise cell
//
//  Created by Roni Shoham on 24/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIAVideoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *videoThumb;
@property (weak, nonatomic) IBOutlet UILabel *videoText;
@property (weak, nonatomic) IBOutlet UILabel *videoTimeStamp;
@property (nonatomic) BOOL isVideo;
@property (nonatomic) BOOL parentVideoCell;

@end
