//
//  BottomBarSegue.swift
//  BlingIphoneApp
//
//  Created by Zach on 25/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class BottomBarSegue: UIStoryboardSegue {

    override func perform() {
        let navController: BIANavigationController
        if source is BIANavigationController {
            navController = source as! BIANavigationController
        } else {
            navController = source.navigationController as! BIANavigationController
        }
        
        var dest = destination
        
        if (navController.tabViewControllers.count < 1) {
            navController.tabViewControllers.add(navController.viewControllers[0])
        }
        
        if (dest is SIANotificationsViewController) { //notifications should not be reused
            transition(to: dest, using: navController)
            return
        }
        var viewControllerFound = false
        
        let vc = navController.viewControllers[0]
        if (object_getClassName(destination) == object_getClassName(vc)) {
            dest = vc
            viewControllerFound = true
        }
    
        if !viewControllerFound, let viewControllers = navController.tabViewControllers {
            for vc in viewControllers {
                if (object_getClassName(destination) == object_getClassName(vc)) {
                    dest = vc as! UIViewController
                    break
                }
            }
        }
        
        if (!navController.tabViewControllers.contains(dest)) {
            navController.tabViewControllers.add(dest)
        }
        
        transition(to: dest, using: navController)
    }
    
    func transition(to destination: UIViewController, using navigationController: UINavigationController) {
        let transition = CATransition()
        transition.duration = 0.1
        transition.type = kCATransitionFade
        navigationController.view.layer.add(transition, forKey: "transition")
        navigationController.setViewControllers([destination], animated: false)
    }
}
