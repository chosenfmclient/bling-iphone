//
//  BIAVideoToMatExploder.mm
//  
//
//  Created by Michael Biehl on 11/13/16.
//
//

#import "BIAVideoToMatExploder.h"



@interface BIAVideoToMatExploder ()
{
    std::map<Float64, cv::Mat> videoMap;
}

@property (strong, nonatomic) AVAssetReader *reader;
@property (strong, nonatomic) AVAssetReaderTrackOutput *readerOutput;
@property (strong, nonatomic) NSMutableDictionary *framesDict;

@end

@implementation BIAVideoToMatExploder

- (id)initWithAsset:(AVAsset *)asset {
    if (self = [super init]) {
        [self initializeReaderWithAsset:asset];
    }
    return self;
}

- (void)initializeReaderWithAsset:(AVAsset *)asset {
    [self setUpReaderWithAsset:asset
                  withReadTime:kCMTimeZero];
}

- (void)setUpReaderWithAsset:(AVAsset *)asset
                withReadTime:(CMTime)readTime {
    
    [self finishReading]; // in case we were reading??
    NSError *error;
    self.reader = [AVAssetReader assetReaderWithAsset:asset
                                                error:&error];
    
    [self.reader setTimeRange:CMTimeRangeMake(readTime, kCMTimePositiveInfinity)]; // the length of the range isn't important, go to INFINITY!
    
    self.readerOutput = [[AVAssetReaderTrackOutput alloc] initWithTrack:[asset tracksWithMediaType:AVMediaTypeVideo].firstObject
                                                         outputSettings:@{(id)kCVPixelBufferPixelFormatTypeKey : [NSNumber numberWithInt:kCVPixelFormatType_32BGRA]}];
    self.videoSize = CGSizeZero;
    [self.reader addOutput:self.readerOutput];
    [self.reader startReading];
    self.error = error != nil ? error : self.reader.error;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)applicationDidBecomeActive:(NSNotification *)note {
    if (self.reader.status == AVAssetReaderStatusFailed) {
        [self initializeReaderWithAsset:self.reader.asset];
        NSLog(@"AVAssetReader failed in the background - REINITIALIZING!");
    }
}

- (cv::Mat)getFrameAtTime:(CMTime)time {
    
    CMTime pst = kCMTimeInvalid;
    CMSampleBufferRef sampleBuffer = NULL;
    BOOL timeMatched = NO;
    while (!timeMatched) {
        sampleBuffer = [self.readerOutput copyNextSampleBuffer];
        if (sampleBuffer == NULL) {
            if (self.reader.status == AVAssetReaderStatusFailed) {
                NSLog(@"AVAssetReader returned NULL, REINITIALIZING!");
                [self setUpReaderWithAsset:self.reader.asset
                              withReadTime:time];
                if (self.error == nil) {
                    return [self getFrameAtTime:time]; // watch out for RECURSION!!!11!
                }
                else return cv::Mat();
            }
            else {
                return cv::Mat();
            }
        }
        pst = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
        timeMatched = CMTIME_COMPARE_INLINE(pst, >=, time);
        if (!timeMatched) {
            CFRelease(sampleBuffer);
        }
    }
    
    self.currentFrameTime = pst;
    CVImageBufferRef imageBuffer = NULL;
    imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
//    CIImage *ciImage = [CIImage imageWithCVImageBuffer:imageBuffer];
    
    
    if (imageBuffer == NULL) return cv::Mat();
    
    if (CGSizeEqualToSize(self.videoSize, CGSizeZero)) {
        self.videoSize = CGSizeMake(CVPixelBufferGetWidth(imageBuffer),
                                    CVPixelBufferGetHeight(imageBuffer));
    }
    
    cv::Mat image = [self cvMatFromImageBuffer:imageBuffer].clone();
    //UIImage *uiIMage = [self UIImageFromCVMat:image];
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
    CFRelease(sampleBuffer);
    //CFRelease(imageBuffer);
    return image;
}

//- (void)explodeVideoWithCompletion:(void (^)(BOOL completed))handler {
//    
//    if ([self.reader startReading]) {
//        
//        BOOL continueReading = YES;
//        while (continueReading) {
//            //std::pair<Float64, cv::Mat> frame = [self readNextFrame];
//            if (frame.first == -1) {
//                continueReading = NO;
//            }
//            else {
//               // videoMap.insert(frame);
//            }
//        }
//        
//        handler(self.reader.status == AVAssetReaderStatusCompleted);
//        
//    }
//    else {
//        NSLog(@"VideoExploder: Start Reading Failed!!!");
//    }
//}



- (void)finishReading {
    if (self.reader.status == AVAssetReaderStatusReading) {
        [self.reader cancelReading];
    }
}

- (cv::Mat)cvMatFromImageBuffer:(CVImageBufferRef)imageBuffer {
    
    void* bufferAddress;
    size_t width;
    size_t height;
    size_t bytesPerRow;
    
    int format_opencv;
    
    OSType format = CVPixelBufferGetPixelFormatType(imageBuffer);
    if (format == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) {
        
        format_opencv = CV_8UC1;
        
        bufferAddress = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
        width = CVPixelBufferGetWidthOfPlane(imageBuffer, 0);
        height = CVPixelBufferGetHeightOfPlane(imageBuffer, 0);
        bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0);
        
    } else { // expect kCVPixelFormatType_32BGRA
        
        format_opencv = CV_8UC4;
        
        bufferAddress = CVPixelBufferGetBaseAddress(imageBuffer);
        width = CVPixelBufferGetWidth(imageBuffer);
        height = CVPixelBufferGetHeight(imageBuffer);
        bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    }
    
    cv::Mat image((int)height, (int)width, format_opencv, bufferAddress, bytesPerRow);
    
    cvtColor(image, image, CV_BGRA2RGBA);
//    UIImage *uiImage = [self UIImageFromCVMat:image];
//    NSLog(@"IMAGE TIME");
//    uiImage = nil;
    
    return image;
}

-(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat {
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    
    CGColorSpaceRef colorSpace;
    CGBitmapInfo bitmapInfo;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
        bitmapInfo = kCGImageAlphaNone | kCGBitmapByteOrderDefault;
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
        bitmapInfo = kCGBitmapByteOrder32Little | (
                                                   cvMat.elemSize() == 3? kCGImageAlphaNone : kCGImageAlphaNoneSkipLast
                                                   );
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(
                                        cvMat.cols,                 //width
                                        cvMat.rows,                 //height
                                        8,                          //bits per component
                                        8 * cvMat.elemSize(),       //bits per pixel
                                        cvMat.step[0],              //bytesPerRow
                                        colorSpace,                 //colorspace
                                        bitmapInfo,                 // bitmap info
                                        provider,                   //CGDataProviderRef
                                        NULL,                       //decode
                                        false,                      //should interpolate
                                        kCGRenderingIntentDefault   //intent
                                        );
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return finalImage;
}

@end
