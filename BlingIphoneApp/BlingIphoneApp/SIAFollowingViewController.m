//
//  SIAFollowingViewController.m
//  ChosenIphoneApp
//
//  Created by Roni Shoham on 09/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAFollowingViewController.h"
#import "SIAFollowersDataModel.h"
#import "SIAUser.h"

#define TABLE_SECTION_HEADER_HEIGHT (30.)
#define CELL_HEIGHT (54.)
#define FOLLOWING_TABLE_CELL_ID @"setCell"
#define NUMBER_OF_FOLLOWERS_KEY_STR @"followers"
#define FOLLOWING_HEADER_YOU_TEXT @"You are following %d people"
#define FOLLOWING_HEADER_YOU_TEXT_SINGULAR @"You are following 1 person"
#define FOLLOWING_HEADER_OTHER_TEXT @"Following %d people"
#define FOLLOWING_HEADER_OTHER_TEXT_SINGULAR @"Following 1 person"
#define FOLLOWERS_HEADER_YOU_TEXT @"You are followed by %d people"
#define FOLLOWERS_HEADER_OTHER_TEXT @"Followed by %d people"
#define FOLLOWERS_HEADER_YOU_TEXT_SINGULAR @"You are followed by 1 person"
#define FOLLOWERS_HEADER_OTHER_TEXT_SINGULAR @"Followed by 1 person"
#define FOLLOWING_IMAGE_NAME @"following_orange.png"
#define NOT_FOLLOWING_IMAGE_NAME @"follow_blue.png"
#define FOLLOWERS_TITLE @"Followers"
#define FOLLOWING_TITLE @"Following"
#define USER_PLACEHOLDER_IMAGE @"user-pic-placeholder.png"
#define NO_FOLLOWERS_IMAGE @"no-friends-icon.png"
#define NO_FOLLOWERS_LABEL_HEIGHT (18)
#define NO_FOLLOWERS_MESSAGE @"No followers yet"
#define NO_FOLLOWINGS_MESSAGE @"Nothing here yet"
#define FIND_FRIENDS_BUTTON_LABEL @"Find Friends"

#define TEXT_COLOR [UIColor whiteColor]

#define ME nil

@interface SIAFollowingViewController ()

@property (nonatomic, strong) UIImageView *noFollowersImage;
@property (nonatomic, strong) UILabel *noFollowersLabel;
@property (nonatomic, strong) UIButton *findFriendsButton;
@property (nonatomic, strong) NSIndexPath *buttonIndexPath;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation SIAFollowingViewController


- (void) viewDidLoad {
    [super viewDidLoad];
    
    Class modelClass;
    if (self.isFollowing) {
        modelClass = [SIAFollowingDataModel class];
        self.followTitle = FOLLOWING_TITLE;
    }
    else {
        modelClass = [SIAFollowersDataModel class];
        self.followTitle = FOLLOWERS_TITLE;
    }
    
    self.backgroundImageView.image = self.backgroundImage;
    
    __weak SIAFollowingViewController* wSelf = self;
    [Utils dispatchInBackground:^{
        wSelf.paginator = [[SIAPaginator alloc] initForModelClass:modelClass delegate:wSelf];
        ((SIAFollowListModel *)wSelf.paginator.paginatorModel).userId = wSelf.userId;
        [wSelf.paginator fetchFirstPage];
    }];
    
    [self noFollowers];
    self.noFollowersImage.hidden = YES;
    self.noFollowersLabel.hidden = YES;
    self.findFriendsButton.hidden = YES;
    self.buttonIndexPath = nil;

    //
    //    if (self.model.usersList.count == 0) {
    //
    //        self.noFollowersLabel.hidden = NO;
    //        self.noFollowersImage.hidden = NO;
    
    //    }
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (self.needToRefresh) {
        
        [self startLoadingAnimation];
        self.needToRefresh = NO;
        [self.paginator fetchFirstPage];
        self.tableView.userInteractionEnabled = NO;
    }
}

- (void) viewDidAppear:(BOOL)animated {
    if ([self.followTitle isEqualToString:FOLLOWERS_TITLE]) {
//        [Flurry logEvent:FLURRY_VIEW_FOLLOWERS_PROFILE];
        self.title = FOLLOWERS_TITLE;
    }
    else {
//        [Flurry logEvent:FLURRY_VIEW_FOLLOWING_PROFILE];
        self.title = FOLLOWING_TITLE;
    }
}

-(void)finishLoadingWithAnimation{

    [super finishLoadingWithAnimation];
    self.tableView.userInteractionEnabled = YES;
}

- (NSString *)navigationTitleString{
    return self.followTitle;
}


//- (void) showData  {
//    [super showData];
//    if (self.model == nil){
//        if ([self.followTitle isEqualToString:FOLLOWERS_TITLE]) {
//            [SIAFollowersDataModel getDataForUserId:self.userId
//                              WithCompletionHandler:^(SIAFollowListModel *model) {
//                                  self.model = (SIAFollowersDataModel *)model;
//                                  self.listFromModel = self.model.usersList;
//                                  [self refreshView];
//                              }];
//        } else if ([self.followTitle isEqualToString:FOLLOWING_TITLE]) {
//            [SIAFollowingDataModel getDataForUserId:self.userId
//                              WithCompletionHandler:^(SIAFollowListModel *model) {
//                                  self.model = (SIAFollowingDataModel *)model;
//                                  self.listFromModel = self.model.usersList;
//                                  [self refreshView];
//                              }];
//        }
//    } else {
//        [self refreshView];
//    }
//
//}

- (CGFloat)tableHeaderSize{
    return self.totalPeople > 0 ? TABLE_SECTION_HEADER_HEIGHT : 0.0;
}

- (NSString *)tableHeaderString{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([self.userId isEqualToString:[defaults objectForKey:kChosenUserIdKeyStr]]) {
        
        if ([self.followTitle isEqualToString:FOLLOWERS_TITLE]) {
            
            if (self.totalPeople == 1) {
                
                return FOLLOWERS_HEADER_YOU_TEXT_SINGULAR;
            }
            
            else
                return [NSString stringWithFormat:FOLLOWERS_HEADER_YOU_TEXT, self.totalPeople];
            
        }
        else {
            
            if (self.totalPeople == 1)
                return FOLLOWING_HEADER_YOU_TEXT_SINGULAR;
            
            else
                return [NSString stringWithFormat:FOLLOWING_HEADER_YOU_TEXT, self.totalPeople];
        }
    }
    
    else {
        if ([self.followTitle isEqualToString:FOLLOWERS_TITLE]) {
            
            if (self.totalPeople == 1) {
                
                return FOLLOWERS_HEADER_OTHER_TEXT_SINGULAR;
            }
            
            else
                return [NSString stringWithFormat:FOLLOWERS_HEADER_OTHER_TEXT, self.totalPeople];
            
        }
        else {
            
            if (self.totalPeople == 1)
                return FOLLOWING_HEADER_OTHER_TEXT_SINGULAR;
            
            else
                return [NSString stringWithFormat:FOLLOWING_HEADER_OTHER_TEXT, self.totalPeople];
            
        }
        
    }
    
}

- (NSString *)cellIdentifierForRow:(NSInteger)row{
    @try {
        if ([self.paginator.results[row] isKindOfClass:[[NSNull null] class]]) {
            return LOADING_CELL_ID;
        }
        return FOLLOWING_TABLE_CELL_ID;
    }
    @catch (NSException *exception) {
        return LOADING_CELL_ID;
    }

}

- (UIImage *)placeholderForRow:(NSUInteger)row{
    @try {
        if (self.paginator.results[row]){
            return [UIImage imageNamed:USER_PLACEHOLDER_IMAGE];
        }
    }
    @catch (NSException *exception) {
        ;
    }
}

- (NSURL *)imageURLForRow:(NSInteger)row {
    SIAUser *user = self.paginator.results[row];
    return user.imageURL;
}



- (void)setImageForRow:(NSUInteger)row withHandler:(void (^)(UIImage* image))handler{
    SIAUser *user = self.paginator.results[row];
    if (user.image) {
        handler(user.image);
    } else {
        [user imageWithCompletionHandler:^(UIImage *image) {
            handler (image);
        }];
    }
}

- (NSAttributedString *)textLabelStringForRow:(NSInteger)row{
    SIAUser *user = self.paginator.results[row];
    return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", user.first_name, user.last_name]];
}

- (NSAttributedString *)detailedTextStringForRow:(NSInteger)row{
    SIAUser *user = self.paginator.results[row];
    return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ Followers", user.total_followers]];
}

- (UIView *)viewForAccessoryViewForRow:(NSUInteger)row {
    SIAUser *user = self.paginator.results[row];
    UIButton *followButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CELL_HEIGHT, CELL_HEIGHT)];
    [followButton setImage:[UIImage imageNamed:NOT_FOLLOWING_IMAGE_NAME] forState:UIControlStateNormal];
    [followButton setImage:[UIImage imageNamed:FOLLOWING_IMAGE_NAME]     forState:UIControlStateSelected];
    [followButton addTarget:self action:@selector(followUnfollow:) forControlEvents:UIControlEventTouchUpInside];
    followButton.imageEdgeInsets = UIEdgeInsetsMake(15, 0, 0, 0);
    
#pragma warning - can be other's followers list, so it's not automatically yes
    
    followButton.isAccessibilityElement = YES;
    followButton.accessibilityIdentifier = @"following_button_follow";
    followButton.accessibilityValue = [NSString stringWithFormat:@"%i", row];
    
    if ([user.user_id isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:kChosenUserIdKeyStr]]) {    // user is me - hide follow button
        
        followButton.hidden = YES;
    }
    else if (user.followedByMe) {
        followButton.selected = YES;
        followButton.accessibilityLabel = @"Following";
    }
    else {
        followButton.selected = NO;
        followButton.accessibilityLabel = @"Follow";
    }
    return followButton;
}


/** \refresh the view with new data
 *
 * \param none
 *
 * \return none
 *
 */
-(void)refreshView{
    [super refreshView];
    
    
    if (self.paginator.results.count == 0) {
        
        self.noFollowersLabel.hidden = NO;
        self.noFollowersImage.hidden = NO;
        
        if ([self.followTitle isEqualToString:FOLLOWING_TITLE] && _isMyProfile)
            self.findFriendsButton.hidden = NO;
    }
}


/** \brief creates the follow or unfollow action - depends on current state
 *
 * \param sender
 *
 * \return nil
 *
 *
 *
 */
- (IBAction)followUnfollow:(id)sender {
    
    UIButton *button = sender;
    CGPoint buttonOriginInTableView = [sender convertPoint:CGPointZero toView:self.tableView];
    _buttonIndexPath = [self.tableView indexPathForRowAtPoint:buttonOriginInTableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonOriginInTableView];
    SIAUser *user = self.paginator.results[indexPath.row];
    if (button.selected){
        button.selected = NO;
        
        if ([self.followTitle isEqualToString:FOLLOWERS_TITLE]) {
//            [Flurry logEvent:FLURRY_TAP_UNFOLLOW_FOLLOWERS_PROFILE];
        }
        else {
//            [Flurry logEvent:FLURRY_TAP_UNFOLLOW_FOLLOWING_PROFILE];
        }
        [user unfollowWithHandler:^(BOOL success) {
            if (!success) {
                button.selected = YES;
            }
        } withAmplitudePageName: AmplitudeEvents.kFollowingPageName
                 withListPosition:indexPath.row];
    }
    
    else {
        button.selected = YES;
        if ([self.followTitle isEqualToString:FOLLOWERS_TITLE]) {
//            [Flurry logEvent:FLURRY_TAP_FOLLOW_FOLLOWERS_PROFILE
//              withParameters:@{@"pos":@(_buttonIndexPath.row),
//                               kUserIdKeyStr:user.user_id}];
        }
        else {
//            [Flurry logEvent:FLURRY_TAP_FOLLOW_FOLLOWING_PROFILE
//              withParameters:@{@"pos":@(_buttonIndexPath.row),
//                               kUserIdKeyStr:user.user_id}];
        }
        [user followWithHandler:^(BOOL success) {
            if (!success) {
                button.selected = NO;
            }
        } withAmplitudePageName:AmplitudeEvents.kFollowingPageName
               withListPosition:indexPath.row];
    }
    
}

- (void) signInDismissed {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSignInViewDismissed object:nil];
    
    
//        SIAAppDelegate *appDelegate = (SIAAppDelegate *)[[UIApplication sharedApplication] delegate];
//        appDelegate.refreshLikes = YES;
        
    [self followUnfollow:nil];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

    if (self.paginator.results && indexPath.row < self.paginator.results.count) {
        if ([self.paginator.results[indexPath.row] isKindOfClass:[SIAUser class]]) {
            SIAUser *user = self.paginator.results[ indexPath.row ];
            UIStoryboard *profileSB = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ProfileViewController *profileController = [profileSB instantiateViewControllerWithIdentifier:@"profileViewController"];
            [self.navigationController pushViewController:profileController animated:YES];
            profileController.userId = user.user_id;
            
//            NSDictionary *flurryParams = @{@"pos":[NSNumber numberWithInteger:indexPath.row],
//                                           kUserIdKeyStr:user.user_id};
            
            if ([self.followTitle isEqualToString:FOLLOWERS_TITLE]) {
//                [Flurry logEvent:FLURRY_TAP_USER_FOLLOWERS_PROFILE withParameters:flurryParams];
            }
            else {
//                [Flurry logEvent:FLURRY_TAP_USER_FOLLOWING_PROFILE withParameters:flurryParams];
            }
        }
    }
}

- (void) noFollowers {
    
    self.noFollowersImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:NO_FOLLOWERS_IMAGE]];
    self.noFollowersImage.center = CGPointMake(SCREEN_WIDTH     / 2, SCREEN_HEIGHT / 2);
    [self.view addSubview:self.noFollowersImage];
    
    
    self.noFollowersLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, (SCREEN_HEIGHT / 2)
                                                                      + self.noFollowersImage.image.size.height, SCREEN_WIDTH, NO_FOLLOWERS_LABEL_HEIGHT)];
    if ([self.followTitle isEqualToString:FOLLOWERS_TITLE]) {
        self.noFollowersLabel.text = NO_FOLLOWERS_MESSAGE;
    }
    
    else if ([self.followTitle isEqualToString:FOLLOWING_TITLE]) {
        self.noFollowersLabel.text = NO_FOLLOWINGS_MESSAGE;
    }
    
    self.noFollowersLabel.textAlignment = NSTextAlignmentCenter;
    self.noFollowersLabel.font = kAppFontItalic(12.0);
    self.noFollowersLabel.textColor = TEXT_COLOR;
    
    self.findFriendsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    [self.findFriendsButton setTitle:FIND_FRIENDS_BUTTON_LABEL forState:UIControlStateNormal];
    self.findFriendsButton.titleLabel.font = kAppFontItalic(12);
    //self.findFriendsButton.titleLabel.textColor = [UIColor whiteColor];
    self.findFriendsButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.findFriendsButton.contentEdgeInsets = UIEdgeInsetsMake(6, 15, 6, 15);
    self.findFriendsButton.layer.cornerRadius = 15;
    [self.findFriendsButton sizeToFit];
    self.findFriendsButton.center = CGPointMake(SCREEN_WIDTH / 2, self.noFollowersLabel.frame.origin.y + NO_FOLLOWERS_LABEL_HEIGHT + self.findFriendsButton.frame.size.height);
    self.findFriendsButton.backgroundColor = SingrDarkGray;
    [self.findFriendsButton addTarget:self action:@selector(goToFindFriends) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.findFriendsButton];
    [self.view addSubview:self.noFollowersLabel];
    
}

- (void) noData {
    self.noFollowersImage.hidden = NO;
    self.noFollowersLabel.hidden = NO;
    
}

- (void) goToFindFriends {
    
    UIViewController *newViewController = [[UIStoryboard storyboardWithName:@"ProfileStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"FindFriendsMain"];
    
    [self.navigationController pushViewController:newViewController animated:YES];
    
}



@end
