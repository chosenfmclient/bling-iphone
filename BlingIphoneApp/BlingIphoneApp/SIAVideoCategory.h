//
//  SIAVideoCategory.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/10/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SIAVideoCategory : NSObject

@property (strong, nonatomic) NSString *label;
@property (strong, nonatomic) NSArray <NSString *> *types;


+ (RKObjectMapping *)objectMapping;

@end
