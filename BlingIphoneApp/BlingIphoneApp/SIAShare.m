//
//  SIAShare.m
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 13/07/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAShare.h"
#include "REComposeViewController.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "PHAsset+Utilities.h"
@import Photos;

typedef NS_ENUM(NSUInteger, SIAShareOperation) {
    SIAShareOperationGetShareId,
    SIAShareOperationShareSuccessful
};

#define ACCESS_TOKEN @"access_token"
#define USER_WITHOUT_KEY @"api/v1/users/me"
#define BASE_VIDEO_URL @"api/v1/videos/%@/share"
#define BASE_PROFILE_URL @"api/v1/users/%@/share"
#define BASE_BADGE_URL @"api/v1/badges/%@/share"
#define BASE_GAME_URL @"api/v1/share-games/%@"
#define SHARE_VIDEO_URL @"v/%@_%@"
#define SHARE_PHOTO_URL @"photo/%@?share_id=%@"
#define SHARE_PROFILE_URL @"u/%@_%@"
#define SHARE_GAME_URL @"g/%@/_%@"
#define SHARE_BADGE_URL @"badge/%@_%@"
#define NATIVE_VIDEO_API @"api/v1/native-video"
#define FB_APP_NAMESPACE @"chosenfm:"
#define FB_VIDEO_OG_TYPE @"video.other"

#define REVIEW_VIDEO_TITLE @"%@ reviewing %@'s performance of %@"
#define MY_VIDEO_SHARE_TEXT @"Check out this video I made with Blingy! %@"
#define OTHERS_VIDEO_SHARE_TEXT @"This Blingy video by %@ is a must see! Watch it now! %@"
#define MY_PHOTO_SHARE_TEXT @"Check out this photo I made with Blingy! %@"
#define OTHERS_PHOTO_SHARE_TEXT @"This Blingy photo by %@is a must see! Check it out! %@"
#define PROFILE_SHARE_TEXT @"Don't miss %@! Check them out on Blingy now! %@"
#define GAME_SHARE_TEXT @"%@ on Blingy is definitely worth playing. See for yourself now! %@"
#define MY_VIDEO_SHARE_TEXT_FBTW @"Check out this video I made with @Blingy! %@"
#define OTHERS_VIDEO_SHARE_TEXT_FBTW @"This @Blingy video by %@ is a must see! Watch it now! %@"
#define MY_PHOTO_SHARE_TEXT_FBTW @"Check out this photo I made with @Blingy! %@"
#define OTHERS_PHOTO_SHARE_TEXT_FBTW @"This @Blingy photo by %@ is a must see! Check it out! %@"
#define PROFILE_SHARE_TEXT_FBTW @"Don't miss %@! Check them out on @Blingy now! %@"
#define GAME_SHARE_TEXT_FBTW @"%@ on @Blingy is definitely worth playing. See for yourself now! %@"
#define SHARE_TEXT_TW_BADGE @"I unlocked the %@ badge on @chosen ! %@ #chosenapp"

@implementation SIANativeVideo

@end

@interface SIAShare() <FBSDKSharingDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, FBSDKGraphRequestConnectionDelegate>

@property (strong, nonatomic) NSString *facebookShareText;
@property (strong, nonatomic) NSString *shareDescription;
@property (strong, nonatomic) NSString *objectImage;
@property (strong, nonatomic) UIAlertView *twitterAlert;
@property (strong, nonatomic) UIView *baseView;
@property (nonatomic, copy) void (^shareCompletion)(BOOL);
@property (nonatomic) BOOL uploadToFacebookAfterProcessingComplete;
@property (nonatomic) BOOL didSaveToCameraRoll;
@end

@implementation SIAShare

-(void)shareOnService:(NSString *)serviceType withCompletionBlock:(myCompletion)completed {
    
   //facebook
    NSString *service;
    if ([serviceType isEqualToString:SLServiceTypeFacebook]){
        service = @"facebook";
    }
    
    //twitter
    else if ([serviceType isEqualToString:SLServiceTypeTwitter]){
        service = @"twitter";
    }
    
    _platform = service;
    
    if ([_shareType isEqualToString:kChosenVideoKeyStr] || [_shareType isEqualToString:@"photo"]) {
        
       // _imageURL = [NSURL URLWithString:@"chosen.fm/images/logo.png"];
        
        if (_video.thumbnailUrl) {
            _imageURL = _video.thumbnailUrl;
        }
        
        if ([service isEqualToString:@"facebook"]) {
                [self videoShare:^(BOOL videoShareComplete){
                    completed(videoShareComplete);
                }];
        }
        
        else if ([service isEqualToString:@"twitter"]) {
            [self getShareId:^(NSString *shareId) {
                _shareURL = [self getShareURLforShareId:shareId];
                [self tweet:^(BOOL tweetCompleted) {
                    completed(tweetCompleted);
                    }];
                }];
            }
    }
    else {
        
        [self getShareId:^(NSString *shareId) {
            
            _shareURL = [self getShareURLforShareId:shareId];
            
            if ([service isEqualToString:@"facebook"]) {
                
                //_shareURL = SHARE_BASE_URL;
                if ([_shareType isEqualToString:kChosenProfileKeyStr] || [_shareType isEqualToString:kChosenGameKeyStr])
                    [self displayFacebookShareDialog:^(BOOL facebookDisplayComplete) {
                        completed(facebookDisplayComplete);
                    }];
                else if ([_shareType isEqualToString:@"badge"])
                    [self displayFacebookBadgeShareDialog];
            }
            
            else if ([service isEqualToString:@"twitter"]) {
                
                [self tweet:^(BOOL tweetComplete) {
                    completed(tweetComplete);
                }];
                
                
            }
        }];
    }
}

#pragma mark - FACEBOOK

-(void)shareOnFBWithCompletionBlock:(myCompletion)completed {
    [self shareOnService:SLServiceTypeFacebook withCompletionBlock:^(BOOL finished) {
        completed(finished);
    }];
}

- (void) videoShare:(myCompletion)completed {
    
    [self getShareId:^(NSString *shareId) {
        _shareURL = [self getShareURLforShareId:shareId];
        [self displayFacebookShareDialog:^(BOOL facebookDisplayComplete) {
            completed(facebookDisplayComplete);
        }];
    }];
    
}

- (void) askToPublish:(myCompletion)completed {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Upload to Facebook?"
                                                                   message:@"Would you like to upload this content to Facebook? We will let you know when it's ready."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                         [self composePost];
                                                     }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    
    [_viewController presentViewController:alert
                                  animated:YES
                                completion:^{
                                    completed(YES);
                                }];
    
}

- (void) checkIfVideoIsReadyToShare {
    [FacebookManager requestPublishPermissions:^(BOOL permissionGranted) {
        
        [SIAVideo getDataForVideoId:_video.videoId withCompletionHandler:^(SIAVideo *video) {
//            if ([video.type isEqualToString:@"karaoke"] && video.karaoke_url) {
//                _video.karaoke_url = video.karaoke_url;
//                [self publishVideoToFacebook];
//            }
//            
//            else if ([video.type isEqualToString:@"response"] && video.review_url) {
//                _video.review_url = video.review_url;
//                [self publishVideoToFacebook];
//            }
//            
//            else if (video.urlUrl && !([video.type isEqualToString:@"response"] || [video.type isEqualToString:@"karaoke"])) {
//                _video.urlUrl = video.urlUrl;
                [self publishVideoToFacebook];
//            }
            
//            else {    // video is not ready. check again after 10 seconds.
//                
//                [self performSelector:@selector(checkIfVideoIsReadyToShare) withObject:nil afterDelay:10];
//            }
        }];
    }];
}

- (void) composePost {
    
    _lockOrientation = YES;
    
    if (_recordingPreviewHeaderView) {
        _recordingPreviewHeaderView.hidden = YES;
    }
    
    if (_videoHomeScrubberView) {
        _videoHomeScrubberView.hidden = YES;
    }
    
    REComposeViewController *composeViewController = [[REComposeViewController alloc] init];
    
    // set navigation bar
    
    [composeViewController.navigationBar setBackgroundImage:[UIImage imageNamed:@"white_stripe.png"] forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor blackColor], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"ArialMT-Bold" size:16.0], NSFontAttributeName,nil]];
    
    composeViewController.navigationBar.topItem.title = @"Facebook";
    composeViewController.hasAttachment = YES;
    composeViewController.attachmentImage = _shareImage;
    // composeViewController.text = [self shareTextForFacebook];
    composeViewController.view.frame = _viewController.view.frame;
    
    UIViewController *presentingVC = _viewController ? _viewController : self.viewController ? self.viewController : nil;
    composeViewController.view.frame = presentingVC.view.bounds;
    [composeViewController presentFromViewController:presentingVC];
    
    composeViewController.completionHandler = ^(REComposeViewController *composeViewController, REComposeResult result) {
        [composeViewController dismissViewControllerAnimated:NO completion:nil];
        
        _lockOrientation = NO;
        
        if (_recordingPreviewHeaderView) {
            _recordingPreviewHeaderView.hidden = NO;
        }
        
        if (_videoHomeScrubberView) {
            _videoHomeScrubberView.hidden = NO;
        }
        
        if (result == REComposeResultCancelled) {
            NSLog(@"Cancelled");
        }
        
        if (result == REComposeResultPosted) {
            NSLog(@"Text: %@", composeViewController.text);
            _shareText = composeViewController.text;// stringByAppendingString:[NSString stringWithFormat:@" %@", _shareURL]];
            [self publishVideoToFacebook];
        }
    };
    
    
}

- (void) publishVideoToFacebook {
    if (![SIANetworking checkConnection]) return;
    
    [FacebookManager requestPublishPermissions:^(BOOL permissionGranted) {
        if (permissionGranted) {
            
            if (self.isRecordingShare)
            {
                if ([self.video.mediaType isEqualToString:kChosenVideoKeyStr])
                {
                    if (self.compositedAssetURL)
                        [self uploadVideoToFacebook:self.compositedAssetURL];
                    else
                    {
                        self.uploadToFacebookAfterProcessingComplete = YES;
                    }
                }
                else
                {
                    if (self.photoData)
                        [self uploadPhotoToFacebook];
                }
            }
            else
            {
                
                NSDictionary *params = @{@"video_id":_video.videoId,
                                         @"facebook_token":[FBSDKAccessToken currentAccessToken].tokenString,
                                         @"text":_shareText,
                                         @"caption":_shareText};
                
                [RequestManager postRequest:NATIVE_VIDEO_API
                                     object:nil
                            queryParameters:params
                                    success:^(RKObjectRequestOperation * _Nullable operation, RKMappingResult * _Nullable result) {
                                        NSString *videoTitle = _video.song_name.length > 1 ? [NSString stringWithFormat:@"\"%@\"", _video.song_name] :
                                        [NSString stringWithFormat:@"Your %@", _video.mediaType];
                                        NSString *alertMessage = [NSString stringWithFormat:@"%@ will be uploaded to Facebook.", videoTitle];
                                        
                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!"
                                                                                        message:alertMessage
                                                                                       delegate:nil
                                                                              cancelButtonTitle:@"OK"
                                                                              otherButtonTitles:nil, nil];
                                        [alert show];

                                    } failure:^(RKObjectRequestOperation * _Nullable operation, NSError * _Nullable error) {
                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                        message:@"Something went wrong while uploading to Facebook, please try again."
                                                                                       delegate:nil
                                                                              cancelButtonTitle:@"OK"
                                                                              otherButtonTitles:nil, nil];
                                        [alert show];
                                        
                                        
                                    } showFailureDialog:NO];

                
            }
        }
    }];
}

- (void)uploadVideoToFacebookAfterProcessingFinished:(NSURL*)localURL
{
    if (self.uploadToFacebookAfterProcessingComplete)
    {
        [self uploadVideoToFacebook:localURL];
    }
}

- (void)uploadVideoToFacebook:(NSURL*)localURL {
    
    
    NSData *videoData = [NSData dataWithContentsOfURL:localURL];
    NSMutableDictionary *params;
    if (videoData)
    {
        params  = @{localURL.absoluteString:videoData,
                    @"description":_shareText}.mutableCopy;
        if (self.video.song_name.length > 0)
        {
            [params setObject:_video.song_name
                       forKey:@"title"];
        }
    }
    else return;
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me/videos"
                                                                   parameters:params
                                                                   HTTPMethod:@"POST"];
    
    FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
    
    [connection addRequest:request
         completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (error){
                 NSLog(@"%@", error.localizedDescription);
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                 message:@"Something went wrong while posting to Facebook, please try again."
                                                                delegate:_viewController
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 [alert show];
             }
             else if ([result objectForKey:@"id"])
             {
                 [self sharePostSuccessful];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                                 message:@"Your video was uploaded to Facebook."
                                                                delegate:_viewController
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 [alert show];
             }
         }];
    [connection setDelegate:self];
    [connection start];
    
}

- (void)uploadPhotoToFacebook {
    
    NSMutableDictionary *params;
    
    if (self.photoData)
    {
        params  = @{@"source":self.photoData,
                    @"caption":_shareText}.mutableCopy;
        if (self.video.song_name.length > 0)
        {
            [params setObject:_video.song_name
                       forKey:@"title"];
        };
    }
    else return;
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me/photos"
                                                                   parameters:params
                                                                   HTTPMethod:@"POST"];
    
    FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
    
    [connection addRequest:request
         completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (error){
                 NSLog(@"%@", error.localizedDescription);
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                 message:@"Something went wrong while posting to Facebook, please try again."
                                                                delegate:_viewController
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 [alert show];
             }
             else if ([result objectForKey:@"id"])
             {
                 [self sharePostSuccessful];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                                 message:@"Your photo was uploaded to Facebook."
                                                                delegate:_viewController
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 [alert show];
             }
         }];
    [connection setDelegate:self];
    [connection start];
    
}

- (FBSDKShareDialog *)getShareDialogWithContentURL:(NSURL *)objectURL
{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = objectURL;
    if (_imageURL){
        content.imageURL = _imageURL;
    }
    FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
    shareDialog.shareContent = content;
    return shareDialog;
}

-(void) displayFacebookShareDialog:(myCompletion)completed {
    
    self.shareCompletion = completed;
    FBSDKShareDialog *shareDialog = [self getShareDialogWithContentURL:[NSURL URLWithString:_shareURL]];
    shareDialog.delegate = self;
    if ([shareDialog canShow]) {
        [shareDialog show];
    } 
}

-(void) displayFacebookBadgeShareDialog {
    
    self.shareCompletion = nil;
    FBSDKShareDialog *shareDialog = [self getShareDialogWithContentURL:[NSURL URLWithString:_shareURL]];
    shareDialog.delegate = self;
    ((FBSDKShareLinkContent *)shareDialog.shareContent).imageURL = _imageURL;
    //    ((FBSDKShareLinkContent *)shareDialog.shareContent).caption = [self shareDescriptionForBadge];
    
    if ([shareDialog canShow]) {
        [shareDialog show];
    } else {
       /* [[PopUpView popUpView] show:@"Can't show facebook share dialog\nPlease try again later.\nIf this problem persists, please visit our support page from the main menu" withTitle:@"Error" optionalButton:nil];*/
    }
}

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"completed share:%@", results);
    [self sharePostSuccessful];
    if (self.shareCompletion) {
        self.shareCompletion(YES);
        self.shareCompletion = nil;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                    message:@"Post successful"
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK" , nil)
                                          otherButtonTitles:nil, nil];
    [alert show];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"sharing error:%@", error);
    if (self.shareCompletion) {
        self.shareCompletion(NO);
        self.shareCompletion = nil;
    }
    NSString *message = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?:
    @"Something went wrong while \n posting to Facebook.\n\n Please try again";
    NSString *title = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Error";

    /*[[PopUpView popUpView] show:message withTitle:title optionalButton:nil];*/
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    NSLog(@"share cancelled");
    if (self.shareCompletion) {
        self.shareCompletion(NO);
        self.shareCompletion = nil;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cancelled"
                                                    message:@"Post cancelled"
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK" , nil)
                                          otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - TWEETER
-(void)shareOnTwitterWithCompletionBlock:(myCompletion)completed {
    
    [self checkTwitterAccount:^(BOOL hasAccount){
        if (hasAccount) {
            [self shareOnService:SLServiceTypeTwitter withCompletionBlock:^(BOOL finished) {
                completed(finished);
            }];
        } else {
            return completed(NO);
        }
    }];
}

- (void) tweet:(myCompletion)completed {
    NSString *shareText = [self shareTextForTwitter];
    
    SLComposeViewController *composer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
//    composer.supportedInterfaceOrientation = self.interfaceOrientationForTwitterComposer;
    if (!_viewController) {
        AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        _viewController = delegate.window.rootViewController;
    }
    
    shareText = [NSString stringWithFormat:shareText, _shareURL];
    [composer setInitialText:shareText];
    
    [composer setCompletionHandler:^(SLComposeViewControllerResult result) {
        if (result == SLComposeViewControllerResultDone) {
            [self sharePostSuccessful];
            completed(YES);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                            message:@"Post successful"
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK" , nil)
                                                  otherButtonTitles:nil, nil];
            [alert show];
        }
        else {
            completed(NO);
        }
    }];
    if (_shareImage || [self.shareType isEqualToString:kChosenVideoKeyStr] || [self.shareType isEqualToString:@"photo"] || [self.shareType isEqualToString:kChosenGameKeyStr]) { // not sharing image to twitter  sia-6340 on video
        if (![self.shareType isEqualToString:kChosenVideoKeyStr]){
            [composer addImage:_shareImage];
        }
        [_viewController presentViewController:composer
                                      animated:YES
                                    completion:^{
        }];
        
    }
    else {
        __block void(^blockCompleted)(BOOL) = completed;
        
        /*[[[SIAConnection alloc] init] getPictureFromServerWithURL:_imageURL completionHandler:^(NSData *receivedData) {
            
            [composer addImage:[UIImage imageWithData:receivedData]];
            // notification center to catch when the keyboard appears for the twitter composer
            NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
            NSObject *observer = [center addObserverForName:nil
                                                     object:nil
                                                      queue:nil
                                                 usingBlock:^(NSNotification *note) {
                                                     if ([note.name isEqualToString:@"UIKeyboardPromoteWindowLevelNotification"]) {
                                                         if (completed)
                                                             completed(YES);
                                                         blockCompleted(YES);
                                                     }
                                                 }];
            
            
            [_viewController presentViewController:composer
                                          animated:YES
                                        completion:^{
                                            completed(YES);
                                        }];
        }];*/
    }
}

#pragma mark SNAPCHAT
- (void)shareOnPlatformWithoutCameraRoll:(NSString *)platform
                          withCompletion:(myCompletion)completion {
    
    __weak SIAShare *wSelf = self;
    [wSelf checkPlatformIsInstalledShowingDialog:platform
                                  withCompletion:^(BOOL success) {
        if (!success) {
            [self showNoPhotosPermissionForPlatform:platform];
            completion(NO);
            return;
        }
        
        [wSelf openSharePlatform:platform
                  withCompletion:^(BOOL success) {
            if (success) {
                wSelf.platform = platform;
                [wSelf getShareId:^(NSString *s_id) {/*dont care, just for analytics*/}];
            }
            completion(success);
        }];
    }];
}

- (void)shareOnPlatform:(NSString *)platform withAsset: (AVAsset *)asset withCompletion:(myCompletion)completion {
    
    __weak SIAShare *wSelf = self;
    [wSelf checkPlatformIsInstalledShowingDialog:platform
                                  withCompletion:^(BOOL success) {
        if (!success) {
            completion(NO);
            return;
        }
        
        
        [wSelf getCameraRollPermissionsWithCompletion:^(BOOL success){
            if (!success) {
                [self showNoPhotosPermissionForPlatform:platform];
                completion(NO);
                return;
            }
            
            
            [wSelf addWatermarkToVideo:asset withCompletion:^(NSURL *watermarkedVideoURL) {
                if (!watermarkedVideoURL) {
                    completion(NO);
                    return;
                }
                
                [wSelf saveVideoToCameraRoll:watermarkedVideoURL withCompletion:^(BOOL success) {
                    if (!success) {
                        completion(NO);
                        return;
                    }
                    
                    [wSelf openSharePlatform:platform
                              withCompletion:^(BOOL success) {
                        if (success) {
                            wSelf.platform = platform;
                            [wSelf getShareId:^(NSString *s_id) {/*dont care, just for analytics*/}];
                        }
                        completion(success);
                    }];
                }];
            }];
            
        }];
    }];
}

- (void)shareonPlatform:(NSString *)platform
         withCompletion:(myCompletion)completion {
    
    __weak SIAShare *wSelf = self;
    [wSelf checkPlatformIsInstalledShowingDialog:platform
                                  withCompletion:^(BOOL success) {
        //check for snapchat
        if (!success) {
            completion(NO);
            return;
        }
        [wSelf getCameraRollPermissionsWithCompletion:^(BOOL success){
            if (!success) {
                [self showNoPhotosPermissionForPlatform:platform];
                completion(NO);
                return;
            }
            
            [wSelf downloadVideoFileWithCompletion:^(NSURL *assetURL) {
                //download asset
                if (!assetURL) {
                    completion(NO);
                    return;
                }
                
                [wSelf addWatermarkToVideo:[AVURLAsset assetWithURL:assetURL]
                            withCompletion:^(NSURL *watermarkedVideoURL) {
                                //add watermark
                                if (!watermarkedVideoURL) {
                                    completion(NO);
                                    return;
                                }
                                
                                [wSelf saveVideoToCameraRoll:watermarkedVideoURL withCompletion:^(BOOL success) {
                                    //save to camera roll
                                    if (!success) {
                                        completion(NO);
                                        return;
                                    }
                                    
                                    [wSelf openSharePlatform:platform
                                              withCompletion:^(BOOL success) {
                                        //share to platform
                                        if (success) {
                                            wSelf.platform = platform;
                                            [wSelf getShareId:^(NSString *s_id) {/*dont care, just for analytics*/}];
                                        }
                                        completion(success);
                                    }];
                                }];
                            }];
            }];
        }];
    }];
    
}

- (void)showNoPhotosPermissionForPlatform:(NSString *)platform {
    
    NSString *message = @"Allow Photos Access in Settings -> Blin.gy%@";
    NSString *text;
    if ([platform isEqualToString:@"save"]) {
        text = @".";
    }
    else {
        text = [@" to enable " stringByAppendingString:[[self appNameForPlatform:platform] stringByAppendingString:@" sharing."]];
    }
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cannot save"
                                                                   message:[NSString stringWithFormat:message, text]
                                                            preferredStyle: UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                              style:UIAlertActionStyleCancel
                                            handler:nil]];
    [_viewController presentViewController:alert
                                  animated:YES
                                completion:nil];
}

-(void)getCameraRollPermissionsWithCompletion:(myCompletion)completion {
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        [Utils dispatchOnMainThread:^{
            completion(status == PHAuthorizationStatusAuthorized);
        }];
    }];
}

-(void)checkPlatformIsInstalledShowingDialog:(NSString *)platform
                              withCompletion:(myCompletion)completion {
    
    if ([[UIApplication sharedApplication] canOpenURL:[[NSURL alloc] initWithString:[platform stringByAppendingString:@"://"]]] ||
        [platform isEqualToString:@"save"]) {
        completion(YES);
    } else {
        NSString *message = [self noAppMessageForPlatform:platform];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Uh oh!"
                                                                                 message:message
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        
        [_viewController presentViewController:alertController
                                      animated:YES
                                    completion:^{
                                        completion(NO);
                                    }];
    }

}

- (NSString *)noAppMessageForPlatform:(NSString *)platform {
    
    NSString *message = @"Install %@ to enable %@. Visit the App Store to download.";
    NSString *appName = [NSString string];
    NSString *sharingText = [NSString string];
    if ([platform isEqualToString:@"musically"]) {
        appName = @"Musical.ly";
        sharingText = @"Musical.ly sharing";
    }
    else if ([platform isEqualToString: @"youtube"]) {
        appName = @"the YouTube app";
        sharingText = @"uploads";
    }
    else if ([platform isEqualToString: @"snapchat"]) {
        appName = @"Snapchat";
        sharingText = @"sharing to Snapchat";
    }
    
    return [NSString stringWithFormat:message, appName, sharingText];
}

- (NSString *)appNameForPlatform:(NSString *)platform {
    if ([platform isEqualToString:@"musically"]) {
        return @"Musical.ly";
    }
    else if ([platform isEqualToString:@"youtube"]) {
        return @"YouTube";
    }
    return @"Snapchat";
}

- (void)openSharePlatform:(NSString *)platform
           withCompletion:(myCompletion) completion {
//    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"shared_to_schat"]) {
//        [[UIApplication sharedApplication] openURL:[[NSURL alloc] initWithString:@"snapchat://"]];
//        completion(YES);
//    } else {
    if ([platform isEqualToString:@"save"]) {
        completion(YES);
        return;
    }
    NSString *message;
    if ([platform isEqualToString:@"snapchat"]) {
        message = @"You can share your Blingy video on Snapchat by using memories!";
    }
    else if ([platform isEqualToString:@"musically"]) {
        message = @"Select \'upload video\' in Musical.ly to share your Blin.gy video!";
    }
    else {
        message = @"Select \'upload video\' in YouTube to share your Blin.gy video!";
    }
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[self appNameForPlatform:platform]
                                                                                 message:message
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
    [alertController addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"Go To %@!", [self appNameForPlatform:platform]]
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [[UIApplication sharedApplication] openURL:[[NSURL alloc] initWithString:[platform stringByAppendingString:@"://"]]];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shared_to_schat"];
            completion(YES);
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Maybe Later" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            completion(NO);
        }]];
        
        
        [_viewController presentViewController:alertController
                                      animated:YES
                                    completion:^{
                                    }];
 //   }
}

-(void) saveVideoToCameraRoll:(NSURL*) assetURL withCompletion:(myCompletion) completion{
    
    if (_didSaveToCameraRoll) {
        completion(YES);
        return;
    }
    
    __block UIAlertView *statusAlert = [UIAlertView alloc];
    
    [PHAsset saveVideoAtURL:assetURL location:nil completionBlock:^(PHAsset *asset, BOOL success) {
        if (success) {
            _didSaveToCameraRoll = YES;
            [asset saveToAlbum:@"Blingy"
               completionBlock:^(BOOL success) {
                   // we got the video on our custom album!
                   NSString *messageStr;
                   NSString *mediaNameStr;
                   if (self.video.song_name.length > 0)
                   {
                       mediaNameStr = [NSString stringWithFormat:@"\"%@\"", self.video.song_name];
                   }
                   else
                   {
                       mediaNameStr = @"Your Video";
                   }
                   messageStr = [NSString stringWithFormat:@"%@ has been saved to your Camera Roll.",
                                 mediaNameStr];
                   
                   UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:messageStr preferredStyle:UIAlertControllerStyleAlert];
                   [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction *action) {
                                                                         completion(YES);
                                                                     }
                                               ]];
                   
                   [_viewController presentViewController:alertController
                                                 animated:YES
                                               completion:^{
                                               }];
               }];
            
        } else {
            // ERROR
            if ([PHPhotoLibrary authorizationStatus] != PHAuthorizationStatusAuthorized) {
                statusAlert = [[UIAlertView alloc] initWithTitle:@"Can't save to camera roll"
                                                         message:@"Please enable Photos permissions: Settings > Blingy > Photos"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
                [statusAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
                
            } else {
                
                statusAlert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Can't save to camera roll."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
                [statusAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
            }
        }
    }];
    
}

-(void) addWatermarkToVideo:(AVAsset*) videoAsset withCompletion:(void(^)(NSURL *assetURL))completion {
    // create the layer with the watermark image
    NSLog(@"%@", [videoAsset tracksWithMediaType:AVMediaTypeVideo]);
    
    if (!videoAsset || ![[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject]) {
        completion(nil);
    }
    
    CGSize videoSize = [[[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject] naturalSize];
    CGAffineTransform transform = [[[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject] preferredTransform];
    CGSize transformedVideoSize = CGSizeApplyAffineTransform(videoSize, transform);
    transformedVideoSize = CGSizeMake (fabs(transformedVideoSize.width), fabs(transformedVideoSize.height));
    
    // sorts the layer in proper order
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, transformedVideoSize.width, transformedVideoSize.height);
    videoLayer.frame = CGRectMake(0, 0, transformedVideoSize.width, transformedVideoSize.height);
    [parentLayer addSublayer:videoLayer];
    
    UIImage *myImage = [UIImage imageNamed:self.video.isCameo ? @"watermark_duet" : @"watermark7"];
    CALayer *logoLayer = [CALayer layer];
    logoLayer.contents = (id)myImage.CGImage;
    logoLayer.contentsScale = 2.0;
    logoLayer.rasterizationScale = 2.0;
    logoLayer.shouldRasterize = YES;
    CGFloat imageRatio = myImage.size.height / myImage.size.width;
    // set the logo size
    // .width is 9% from the video width for 16:9 videos which are most of our videos.
    CGFloat logoWidth = transformedVideoSize.width > transformedVideoSize.height ? transformedVideoSize.width * 0.25 : transformedVideoSize.height * 0.25;
    CGFloat logoHeight = logoWidth * imageRatio;
   // logoLayer.contentsScale = logoWidth / myImage.size.width;
    logoWidth = roundf(logoWidth);
    logoHeight = roundf(logoHeight);
    logoLayer.frame = CGRectMake(0,
                                 0, //dont know why but for some reason it takes it from the bottom (sephi)
                                 logoWidth,
                                 logoHeight);
    [parentLayer addSublayer:logoLayer];
    
    
    // create the composition and add the instructions to insert the layer
    AVMutableVideoComposition* videoComp = [AVMutableVideoComposition videoComposition];
    videoComp.renderSize =  transformedVideoSize;
    videoComp.frameDuration = CMTimeMake(1, 30);
    videoComp.animationTool = [AVVideoCompositionCoreAnimationTool
                               videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer
                               inLayer:parentLayer];
    videoComp.renderScale = 1.0;
    
    // instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [videoAsset duration]);
    AVAssetTrack *videoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    AVMutableVideoCompositionLayerInstruction* layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    
    AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    [layerInstruction setTransform:transform atTime:kCMTimeZero];
    instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
    videoComp.instructions = [NSArray arrayWithObject: instruction];
    
    
    AVAssetExportSession *watermarkAssetExport;

    watermarkAssetExport = [[AVAssetExportSession alloc]
                                      initWithAsset:videoAsset
                                      presetName:AVAssetExportPresetHighestQuality];
    if (nil == watermarkAssetExport) {
        completion(nil);
        return;
    }
    
    if (videoComp.renderSize.height < 0 || videoComp.renderSize.width < 0) // somehow had a negative renderSize on the iPod Touch 6G
    {
        completion(nil);
        return;
    }
    watermarkAssetExport.videoComposition = videoComp;
    
    NSString *exportPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"watermarkVideo.mov"];
    NSURL    *exportUrl = [NSURL fileURLWithPath:exportPath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
    }
    
    watermarkAssetExport.outputFileType = AVFileTypeQuickTimeMovie;
    watermarkAssetExport.outputURL = exportUrl;
    watermarkAssetExport.shouldOptimizeForNetworkUse = YES;
    
    [watermarkAssetExport exportAsynchronouslyWithCompletionHandler:^{
        completion(watermarkAssetExport.outputURL);
    }];
}


- (void)downloadVideoFileWithCompletion:(void(^)(NSURL *assetURL))completed {
    
    /*if () {
        completed(YES);
        return;
    }*/
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pathExtension = self.video.videoUrl.pathExtension;
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, [NSString stringWithFormat:@"downloadedVideo.%@", pathExtension]];
    NSURL *outputURL = [NSURL fileURLWithPath:filePath];
    [[NSFileManager defaultManager] removeItemAtURL:outputURL
                                              error:nil];
    
    NSURLRequest *videoRequest = [NSURLRequest requestWithURL:self.video.videoUrl];
    AFRKHTTPRequestOperation *operation =   [[AFRKHTTPRequestOperation alloc] initWithRequest:videoRequest];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    
    __weak AFRKHTTPRequestOperation *wOp = operation;
    
    [operation setCompletionBlock:^{
        if (wOp.error) {
            completed(nil);
        } else {
            completed(outputURL);
            
        }
    }];
    [operation start];
}

#pragma mark - INSTAGRAM

-(void)shareOnInstagramWithCompletionBlock:(myCompletion)completed {
    
    SIAInstagramVideoEditViewController *instagramVC = [[UIStoryboard storyboardWithName:@"SIAShare"
                                                                                  bundle:nil] instantiateViewControllerWithIdentifier:@"cropVideoVC"];
    
    SIAInstagramExportAsset *exportAsset = [[SIAInstagramExportAsset alloc] init];
    instagramVC.isCameoShare = self.video.isCameo;
    
    if (self.video.isVideo && ![self.video isKindOfClass:[SIARecordingItem class]] && (self.video.videoUrl)) {
        SIAVideo *videoForInstagram = [self.video copy];
        videoForInstagram.videoUrl = self.video.videoUrl;
        //instagramVC.effectPlayer = [[BIAVideoPlayer alloc] initWithVideo:videoForInstagram];
//        if (self.video.clipUrl && [self.video.clipUrl.absoluteString containsString:@"Clip"])
//            exportAsset.assetURL = self.video.clipUrl;
//        else
//        {
            exportAsset.assetURL = self.video.videoUrl;
//            CGFloat startFloat = self.video.clip.firstObject.floatValue;
//            CGFloat endFloat = self.video.clip.lastObject.floatValue;
//            CMTime startTime = CMTimeMake(startFloat * 1000, 1000);
//            CMTime endTime = CMTimeMake(endFloat * 1000, 1000);
//            exportAsset.clipTimeRange = CMTimeRangeMake(startTime, endTime);
            exportAsset.exportTimeRange = exportAsset.clipTimeRange;
//        }
    }
    else
    {
        exportAsset.assetURL = _video.videoUrl ?: _assetURL;
        exportAsset.exportTimeRange = self.exportTimeRange;
        exportAsset.hasNoRecordedAudio = _assetHasNoRecordedAudio;
        exportAsset.asset = self.asset;
        instagramVC.effectPlayer = self.effectPlayer;
        
    }
    
    [[SDWebImageManager sharedManager] loadImageWithURL:_video.videoUrl ?: _assetURL options:0 progress:nil completed:^(UIImage *image, NSData *data, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        _image = image;
    }];
    instagramVC.image = self.image;
//    instagramVC.frameId = self.video.frame;
    instagramVC.exportAsset = exportAsset;
    instagramVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    instagramVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"instagram://"]])
    {
        __weak SIAShare *wSelf = self;
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                [wSelf.viewController presentViewController:instagramVC
                                                  animated:YES
                                                completion:^{
                                                    completed(YES);
                                                }];
            }
            else {
                [wSelf showInstagramCameraPermissionMessage];
                completed(NO);
            }
        }];
        
    }
    else {
        SIAAlertController *alertController = [SIAAlertController alertControllerWithTitle:@"Instagram"
                                                                                   message:@"You have not installed Instagram on this device."];
        SIAAlertAction *alertAction  = [SIAAlertAction actionWithTitle:@"OK"
                                                                 style:SIAAlertActionStyleCancel
                                                               handler:nil];
        [alertController addAction:alertAction];
        [_viewController presentViewController:alertController
                                      animated:YES
                                    completion:^{
                                        completed(YES);
                                    }];
    }

}

- (void)showInstagramCameraPermissionMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Camera Roll Permission"
                                                                   message:@"Please allow Blin.gy to access your camera roll in your privacy settings in order to post to Instagram"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                              style:UIAlertActionStyleCancel
                                            handler:nil]];
    [self.viewController presentViewController:alert
                                      animated:YES
                                    completion:nil];
}

#pragma mark - SMS

-(void)shareBySMSWithCompletionBlock:(myCompletion)completed{
    
    self.platform = @"sms";
    self.shareCompletion = completed;
    [self getShareId:^(NSString *shareId) {
        
        _shareURL = [self getShareURLforShareId:shareId];
        [self showSMS:self.shareURL];
        
    }];
    
}

- (void)showSMS:(NSString *)shareURL {
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        self.shareCompletion(NO);
        return;
    }
    
    NSString *message = [self shareTextForSMS];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:message];

    [self.viewController presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            if (self.shareCompletion) {
                self.shareCompletion(NO);
                self.shareCompletion = nil;
            }
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            if (self.shareCompletion) {
                self.shareCompletion(NO);
                self.shareCompletion = nil;
            }
            break;
        }
            
        case MessageComposeResultSent:
           // [self sharePostSuccessful];
            if (self.shareCompletion) {
                self.shareCompletion(YES);
                self.shareCompletion = nil;
            }
            break;
            
        default:
            break;
    }
    
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MAIL

-(void)shareByMailWithCompletionBlock:(myCompletion)completed{
    
    self.platform = @"email";
    self.shareCompletion = completed;
    [self getShareId:^(NSString *shareId) {
        
         //shareId = @12345;
        
         NSLog(@"shareId = %@", shareId);
        
        _shareURL = [self getShareURLforShareId:shareId];
        [self showEmail:self.shareURL withCompletion:completed];
        
    }];
    
    
    
}

-(NSString *)mailTitle{
    
    return self.mailSubject;
    NSString *base = @"Check out %@ on Blingy!";
    
    if ([self.shareType isEqualToString:kChosenVideoKeyStr] || [self.shareType isEqualToString:@"photo"]) {
        if ([self.video.type isEqualToString:RESPONSE_TYPE])
        {
            return [NSString stringWithFormat:base, [NSString stringWithFormat:@"%@ from %@",@"this review", self.video.user.first_name]];
        }
        return [NSString stringWithFormat:base, [NSString stringWithFormat:@"%@ from %@",self.video.song_name, self.video.user.first_name]];
    } else if ([self.shareType isEqualToString:kChosenGameKeyStr]){
        return [NSString stringWithFormat:base, self.gameName];
    } else if ([self.shareType isEqualToString:kChosenProfileKeyStr]){
        return [NSString stringWithFormat:base, self.user.fullName];
    }
    
    return @"Blingy";
}

- (void)showEmail:(NSString *)shareURL withCompletion:(myCompletion)completion{
    
    // Email Subject
    NSString *emailTitle = [self mailTitle];
    // Email Content
    NSString *messageBody =  [self shareTextForMail];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    
    if (!mc) {
        completion(NO);
        return;
    }
    // Present mail view controller on screen
    [self.viewController presentViewController:mc animated:YES completion:^{
        completion(YES);
    }];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            if (self.shareCompletion) {
                self.shareCompletion(NO);
                self.shareCompletion = nil;
            }
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            if (self.shareCompletion) {
                self.shareCompletion(NO);
                self.shareCompletion = nil;
            }
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
           // [self sharePostSuccessful];
            if (self.shareCompletion) {
                self.shareCompletion(YES);
                self.shareCompletion = nil;
            }
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            if (self.shareCompletion) {
                self.shareCompletion(NO);
                self.shareCompletion = nil;
            }
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - OS SHARING
-(void) shareByOther: (UICollectionViewCell*) cell{
    self.platform = @"unknown";
    [self getShareId:^(NSString *shareId) {
        
        _shareURL = [self getShareURLforShareId:shareId];
        NSString *textToShare = @"";//[[NSString stringWithFormat:[self baseTextForSharing], @""] stringByAppendingString:self.shareTags ?: @""];
        NSURL *url = [NSURL URLWithString:_shareURL];
        
        NSArray *objectsToShare = @[textToShare, url];
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList];
        
        activityVC.excludedActivityTypes = excludeActivities;
        activityVC.popoverPresentationController.sourceView = cell;
        [self.viewController.navigationController presentViewController:activityVC animated:YES completion:nil];
        [self sharePostSuccessful];
    }];

}
    
-(void)shareByCopyLinkWithCompletionBlock:(myCompletion)completed{
        
    self.platform = @"copy_link";
    self.shareCompletion = completed;
    [self getShareId:^(NSString *shareId) {
        
         NSLog(@"shareId = %@", shareId);
        
        _shareURL = [self getShareURLforShareId:shareId];

        [UIPasteboard generalPasteboard].string = _shareURL;
        completed(YES);
        
    }];
        
}


#pragma mark - SHARE TEXTS
-(NSString *)shareTextForServiceType:(NSString *)serviceType {
    
    NSString *selectedShareText = [[NSString alloc] init];
    
    if ([serviceType isEqualToString:SLServiceTypeFacebook])
        selectedShareText = [self shareTextForFacebook];
    else if ([serviceType isEqualToString:SLServiceTypeTwitter])
        selectedShareText = [self shareTextForTwitter];
    
    return selectedShareText;
}

- (NSString *)baseTextForSharing{
    if ([_video.user.user_id isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:kUserIdKeyStr]]) {
        _isMyVideo = YES;
    }
    
    if ([_shareType isEqualToString:kChosenVideoKeyStr])
    {
        if (self.isMyVideo) {
            return MY_VIDEO_SHARE_TEXT;
        } else {
            return [NSString stringWithFormat:OTHERS_VIDEO_SHARE_TEXT, self.video.user.fullName, @"%@"];
        }
    }
    else if ([_shareType isEqualToString:@"photo"]) {
        if (self.isMyVideo) {
            return MY_PHOTO_SHARE_TEXT;
        } else {
            return [NSString stringWithFormat:OTHERS_PHOTO_SHARE_TEXT, self.video.user.fullName, @"%@"];
        }
    }
    else if ([_shareType isEqualToString:kChosenProfileKeyStr]) {
        return [NSString stringWithFormat:PROFILE_SHARE_TEXT, self.user.fullName, @"%@"] ;
    }
    else if ([ _shareType isEqualToString:@"badge"]) {
        return [NSString stringWithFormat:SHARE_TEXT_TW_BADGE, _badgeName, @"%@"];
    }
    else if ([self.shareType isEqualToString:@"game"]){
        return [NSString stringWithFormat:GAME_SHARE_TEXT, self.gameName, @"%@" ];
    }
    
    return @"";
}

- (NSString *)baseTextForSharingFBTW{
    if ([_video.user.user_id isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:kUserIdKeyStr]]) {
        _isMyVideo = YES;
    }
    
    if ([_shareType isEqualToString:kChosenVideoKeyStr])
    {
        if (self.isMyVideo) {
            return MY_VIDEO_SHARE_TEXT_FBTW;
        } else {
            return [NSString stringWithFormat:OTHERS_VIDEO_SHARE_TEXT_FBTW, self.video.user.fullName, @"%@"];
        }
    }
    else if ([_shareType isEqualToString:@"photo"]) {
        if (self.isMyVideo) {
            return MY_PHOTO_SHARE_TEXT_FBTW;
        } else {
            return [NSString stringWithFormat:OTHERS_PHOTO_SHARE_TEXT_FBTW, self.video.user.fullName, @"%@"];
        }
    }
    else if ([_shareType isEqualToString:kChosenProfileKeyStr]) {
        return [NSString stringWithFormat:PROFILE_SHARE_TEXT_FBTW, self.user.fullName, @"%@"] ;
    }
    else if ([ _shareType isEqualToString:@"badge"]) {
        return [NSString stringWithFormat:SHARE_TEXT_TW_BADGE, _badgeName, @"%@"];
    }
    else if ([self.shareType isEqualToString:@"game"]){
        return [NSString stringWithFormat:GAME_SHARE_TEXT_FBTW, self.gameName, @"%@" ];
    }
    
    return @"";
}

-(NSString *)shareTextForFacebook {
    return self.starterShareText;
    if (self.shareTags) {
        return [[[NSString stringWithFormat:[self baseTextForSharingFBTW], @""] stringByAppendingString:@"\n#ChosenApp "] stringByAppendingString:self.shareTags];
    }
    
    return [[NSString stringWithFormat:[self baseTextForSharingFBTW], @""] stringByAppendingString:@"\n#ChosenApp"];
}

-(NSString *)shareTextForTwitter {
    return self.starterShareText;
    
    if (self.shareTags) {
        return [[[self baseTextForSharingFBTW] stringByAppendingString:@" #ChosenApp "] stringByAppendingString:self.shareTags];
    }
    
    return [[self baseTextForSharingFBTW] stringByAppendingString:@" #ChosenApp"];
    
}

-(NSString *)shareTextForSMS {
    
    return self.starterShareText;
    if (self.shareTags) {
        return [[NSString stringWithFormat:[self baseTextForSharing], @""] stringByAppendingString:self.shareTags];
    }
    
    return [NSString stringWithFormat:[self baseTextForSharing], @""];
}

-(NSString *)shareTextForMail {
    return self.starterShareText;
    if (self.shareTags) {
        return [[NSString stringWithFormat:[self baseTextForSharing], @""] stringByAppendingString:self.shareTags];
    }
    
    return [NSString stringWithFormat:[self baseTextForSharing], @""];
}

#pragma mark - RESTKITSHIT
-(NSString *)getShareURLforShareId:(NSString *)shareId{
    NSMutableString *shareURL = SHARE_BASE_URL.mutableCopy;
    
    if ([self.shareType isEqualToString:kChosenVideoKeyStr]) {
        if (self.video.videoId) {
            [shareURL appendFormat:SHARE_VIDEO_URL, self.video.videoId, shareId];
        }
    } else if ([self.shareType isEqualToString:@"photo"]) {
        if (self.video.videoId) {
            [shareURL appendFormat:SHARE_PHOTO_URL, self.video.videoId, shareId];
        }
    } else if ([self.shareType isEqualToString:kChosenProfileKeyStr]){
        if (self.user.user_id) {
            [shareURL appendFormat:SHARE_PROFILE_URL, self.user.user_id, shareId];
        }
    } else if ([self.shareType isEqualToString:kChosenBadgeKeyStr]){
        if (self.userId){
            [shareURL appendFormat:SHARE_PROFILE_URL, self.userId, shareId];
        }
    } else if ([self.shareType isEqualToString:kChosenGameKeyStr]){
        if (self.gameId){
            [shareURL appendFormat:SHARE_GAME_URL, self.object_id, shareId];
        }
        
    }
    
    return shareURL;
}


+ (RKObjectMapping *) shareMapping {
    
    RKObjectMapping *shareMapping = [RKObjectMapping mappingForClass:[SIAShare class]];
    
    [shareMapping addAttributeMappingsFromDictionary:@{@"share_id":@"share_id", @"url":@"url"}];
    
    return shareMapping;
}

- (NSString *)sharePath {
    NSString *sharePath;
    if ([_shareType isEqualToString:kChosenVideoKeyStr] || [_shareType isEqualToString:@"photo"]) {
        sharePath = [NSString stringWithFormat:BASE_VIDEO_URL, _video.videoId != nil ? _video.videoId : _object_id];
    }
    else if ([_shareType isEqualToString:kChosenProfileKeyStr]) {
        sharePath = [NSString stringWithFormat:BASE_PROFILE_URL, _user.user_id != nil ? _user.user_id : _object_id];
    }
    else if ([_shareType isEqualToString:@"badge"]) {
        sharePath = [NSString stringWithFormat:BASE_BADGE_URL, _badgeId != nil ? _badgeId : _object_id];
    }
    else if ([_shareType isEqualToString:kChosenGameKeyStr]) {
        sharePath = [NSString stringWithFormat:BASE_GAME_URL, _gameId != nil ? _gameId : _object_id];
    }
    
    return sharePath;
}

- (void) getShareId:(void (^)(NSString *))shareId  {
    
    SIAShareMapping *mapping = [[SIAShareMapping alloc] init];
    mapping.platform = self.platform;
    
    __weak SIAShare* wSelf = self;
    [RequestManager
     postRequest:[self sharePath]
     object:mapping
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) { 
         //SIAShareMappingWrapper *share = mappingResult.firstObject;
         
                   // NSLog(@"share object = %@", share);
         SIAShareMapping *share = mappingResult.firstObject;
         wSelf.object_id = share.object_id;
         wSelf.starterShareText = share.text;
         wSelf.mailSubject = share.mailSubject;
         shareId(share.share_id);
         wSelf.share_id = share.share_id;
         
     } failure:^(RKObjectRequestOperation *operation, NSError *error) {
         NSLog(@"Fail!");
     }
     showFailureDialog:YES];
}

-(void)sharePostSuccessful {
    [RequestManager
     putRequest:[NSString stringWithFormat:@"%@", [self sharePath]]
     object:self
     queryParameters:nil
     success:nil
     failure:nil
     showFailureDialog:YES];
}

- (void)nativeShare:(void (^)(BOOL))shouldShareNative {
    [RequestManager
     getRequest:NATIVE_VIDEO_API
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         SIAShare *share = mappingResult.firstObject;
         shouldShareNative(share.native_video);
     }
     failure:^(RKObjectRequestOperation *operation, NSError *error) {
         shouldShareNative(NO);
     }
     showFailureDialog:YES];
}

- (void)checkTwitterAccount:(void (^)(BOOL))handler {
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error){
        if (granted) {
            
            NSArray *accounts = [accountStore accountsWithAccountType:accountType];
            
            if (accounts.count > 0) { // Check if the users has setup at least one Twitter account
                handler(YES);
                ACAccount *twitterAccount = [accounts firstObject];
                NSString *twitterID = [((NSDictionary *)[twitterAccount valueForKey:@"properties"]) valueForKey:@"user_id"];
                //[self updateUserTwitterId:twitterID];
                
            } else {
                handler(NO);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TWITTER_NO_ACCOUNT_TITLE
                                                                message:TWITTER_NO_ACCOUNT_MESSAGE
                                                               delegate:nil
                                                      cancelButtonTitle:NSLocalizedString(@"OK" , nil)
                                                      otherButtonTitles:nil, nil];
                [alert show];

            }
        }
        
        else {
//            _hasTwitterAccount = TWITTER_NO_PERMISSION;
            _twitterAlert = [[UIAlertView alloc] initWithTitle:TWITTER_NO_ACCESS_TITLE
                                                       message:TWITTER_NO_ACCESS_MESSAGE
                                                      delegate:nil
                                             cancelButtonTitle:NSLocalizedString(@"OK" , nil)
                                             otherButtonTitles:nil, nil];
            [_twitterAlert show];
            handler(NO);
        }
    }];
}
@end
