//
// Created by Ben on 27/11/2016.
//

#include "chromaKey.h"
#include <time.h>
#include <sys/time.h>
#include "globals.h"


chromaKey::chromaKey()
{
    BRIGHTNESS = 0;
    CONTRAST = 1.0f;

    isSet = false;
    count4Loop = 0;
}

chromaKey::~chromaKey()
{ }

/**
 * pass corrected positions
 */
DetectionResponse chromaKey::findChromaKeys(Mat mDss, int faceLeft, int faceTop, int faceRight, int faceBottom, bool frontCamera)
{
    Mat mask(mDss.size(), 0);
    cvtColor(mDss, mask, COLOR_RGBA2GRAY);

    Mat bMask;

    blur(mask, bMask, Size(3, 3));
    mask.release();

    blur(bMask, mask, Size(3, 3));
    bMask.release();

    Canny(mask, bMask, 20, 60, 3);
    mask.release();

    mask = bMask;


    //int WIDTH = mDss.cols, HEIGHT = mDss.rows, WIDTH_X_4 = WIDTH * mDss.channels();

    int padding = (int) (((float) (faceBottom - faceTop)) * PADDING_MULTIPLIER);
    int paddingHalf = (int) (((float) (faceBottom - faceTop)) * PADDING_MULTIPLIER_HALF);
    int paddingWidth = (int) (((float) (faceRight - faceLeft)) * PADDING_MULTIPLIER_HALF);

    int bottomTopArea = frontCamera ? faceBottom + padding : WIDTH - 80;
    
    if(bottomTopArea > WIDTH - 1)
        bottomTopArea = WIDTH - 1;
    
    int bottomBottomArea = frontCamera ? faceTop + paddingHalf : 1;
    int rightLeftArea = frontCamera ? faceLeft - paddingWidth : HEIGHT/5;
    int leftRightArea = frontCamera ? faceRight + paddingWidth : HEIGHT - HEIGHT/5;

    if(bottomBottomArea < 1)
        bottomBottomArea = 1;

    Mat bMdss;
    blur(mDss, bMdss, Kernel_5X5);
    uchar* dss = bMdss.data;
    ////////////////// Init chroma key

    float rt, gt, bt, min, max, delta;
    float saturation = 0, luminance = 0, hue = 0;

    if (mHuesBot != NULL)
    {
        free(mHuesBot);
        free(mSaturationBot);
        free(mSaturationTop);
        free(mHuesTop);
        free(mLuminanceBot);
        free(mLuminanceTop);
    }
    


    mHuesBot = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));
    mHuesTop = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));

    mSaturationBot = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));
    mSaturationTop = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));

    mLuminanceBot = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));
    mLuminanceTop = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));

    int inner;
    chromaKeysArrayLength = 0;
    baseChromaKeysArrayLength = chromaKeysArrayLength;

    ///////////////////////////DETECTION////////////////////////////////////
    
    int totalLinesToDetect = 0, foundedEdges = 0;
    

    if (faceLeft != 0)
    {
        int distanceFromEdge = 5;
        bool foundEdge = false;
        
        totalLinesToDetect += (WIDTH - bottomTopArea) > 0 ? (WIDTH - bottomTopArea) : 0;

        ///////////Top part/////////////
        for (int c = WIDTH - distanceFromEdge, r, index, index2, channels = mDss.channels(), incSrc = WIDTH * channels, incSrc2 = incSrc * 2; c > bottomTopArea + distanceFromEdge; c--)
        {
            index = c * channels + incSrc2;
            index2 = c + WIDTH + (distanceFromEdge * WIDTH);
            foundEdge = false;
//            index3 = c + WIDTH + (distanceFromEdge * WIDTH) - 50;

            for (r = 2; r < HEIGHT - distanceFromEdge; r++, index += incSrc, index2 += WIDTH)
            {
                for(int mi = -distanceFromEdge; mi < distanceFromEdge; mi++)
                {
                    if(mask.data[index2 + mi] != 0)
                    {
                        foundEdge = true;
                        break;
                    }
                }
                
                if(foundEdge) {
                    foundEdge = false;
                    //printf("\n*found edge");
                    foundedEdges++;
                    break;
                }
                

//                if(mask.data[index2] != 0)
//                    break;
                ////////////////Find HSL/////////////////
                rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

//                rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
//                if(dss[index + 2] < dss[index + 1])
//                {
//                    if(dss[index + 2] < dss[index])
//                    {
//                        min = rt;
//                        if(dss[index + 1] < dss[index]) max = bt;
//                        else max = gt;
//                    }
//                    else { min = bt;   max = gt; }
//                }
//                else if(dss[index] < dss[index + 2])
//                {
//                    max = rt;
//                    if(dss[index + 1] < dss[index])
//                        min = gt;
//                    else min = bt;
//                }
//                else { max = bt;   min = gt; }
//
//                delta = max - min;
//                if(delta == 0)
//                    hue = 0;
//                else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
//                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
//                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );
//
//                luminance = (max + min) / 2;
//                if(delta == 0)
//                    saturation = 0;
//                else
//                    saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
//                if (hue < 0) hue = hue + 360;
//                ////////////// End HSL //////////////

                //////////////Hues ...
                for (inner = 0; inner < chromaKeysArrayLength; inner++)
                {
                    if (mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                        && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                        && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                       )
                        break;
                }

                if (inner == chromaKeysArrayLength)
                {
                    mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                    mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                    mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                    mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                    mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                    mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                    chromaKeysArrayLength++;

                    if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                        break;
                }

                if(drawDetectionGuilds)
                {
                    mDss.data[index + 2] = 125;
                    mDss.data[index + 1] = 125;
                    mDss.data[index] = 125;
                }
            }
            if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                break;

            r += distanceFromEdge;
            if(r < HEIGHT)
            {
                index = (HEIGHT * WIDTH_X_4) + (c * channels) - WIDTH_X_4;
                index2 = (HEIGHT * WIDTH) + c - WIDTH - (distanceFromEdge * WIDTH);

                foundEdge = false;

                for (r = HEIGHT - distanceFromEdge; r > distanceFromEdge; r--, index -= incSrc, index2 -= WIDTH)
                {
                    for(int mi = -distanceFromEdge; mi < distanceFromEdge; mi++)
                    {
                        if(mask.data[index2 + mi] != 0)
                        {
                            foundEdge = true;
                            break;
                        }
                    }
                    if(foundEdge) {
                        foundEdge = false;
                        //printf("\n**found edge");
                        foundedEdges++;
                        break;

                    }
                    if(mask.data[index2] != 0)  break;
                    ////////////////Find HSL/////////////////
                    rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
                    ////////////// End HSL //////////////

                    //////////////Hues ...
                    for (inner = 0; inner < chromaKeysArrayLength; inner++)
                    {
                        if (mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                            && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                            && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                                )
                            break;
                    }

                    if (inner == chromaKeysArrayLength)
                    {
                        mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                        mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                        mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                        mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                        mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                        mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                        chromaKeysArrayLength++;

                        if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                            break;
                    }

                    if(drawDetectionGuilds)
                    {
                        mDss.data[index + 2] = 125;
                        mDss.data[index + 1] = 125;
                        mDss.data[index] = 125;
                    }
                }
            }
        }
        /////////////////////////////////

        totalLinesToDetect += rightLeftArea > 0 ? rightLeftArea : 0;

        ///////////Left side/////////////
        for (int r = distanceFromEdge, index, index2, channels = mDss.channels(), incSrc = WIDTH * channels; r < rightLeftArea - distanceFromEdge; r++)//, indexMask += widthMask) && rDst < dst.rows
//        for (int c = bottomTopArea, index, index2, channels = mDss.channels(), incSrc = WIDTH * channels; c >= 0 && !foundEdge; c--)// && cDst > 0
        {
            index = (r * WIDTH_X_4) + (bottomTopArea * channels);
            index2 = (r * WIDTH) + bottomTopArea - distanceFromEdge;
            foundEdge = false;
//            index = c * channels + incSrc + incSrc;
//            index2 = c + WIDTH + WIDTH;

            for (int c = bottomTopArea; c > distanceFromEdge; c--, index -= channels, index2--)// && cDst > 0

//            for (int r = 2; r <= rightLeftArea; r++, index += incSrc, index2 += WIDTH)//, indexMask += widthMask) && rDst < dst.rows
            {
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);

                for(int mi = -distanceFromEdge; mi < distanceFromEdge; mi++)
                {
                    if(mask.data[index2 + (mi * WIDTH)] != 0)
                    {
                        foundEdge = true;
                        break;
                    }
                }
                if(foundEdge) {
                    //printf("\n***found edge");
                    foundEdge = false;
                    if (c > faceBottom ) {
                        foundedEdges++;
                    }
                    break;
                }

//                if(mask.data[index2] != 0)
//                    break;
//                if(mask.data[index2] != 0)
//                {
//                    foundEdge = true;
//                    break;
//                }

                ////////////////Find HSL/////////////////
                rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
                if(dss[index + 2] < dss[index + 1])
                {
                    if(dss[index + 2] < dss[index])
                    {
                        min = rt;
                        if(dss[index + 1] < dss[index]) max = bt;
                        else max = gt;
                    }
                    else { min = bt;   max = gt; }
                }
                else if(dss[index] < dss[index + 2])
                {
                    max = rt;
                    if(dss[index + 1] < dss[index])
                        min = gt;
                    else min = bt;
                }
                else { max = bt;   min = gt; }

                delta = max - min;
                if(delta == 0)
                    hue = 0;
                else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

                luminance = (max + min) / 2;
                if(delta == 0)
                    saturation = 0;
                else
                    saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                if (hue < 0) hue = hue + 360;
                ////////////// End HSL //////////////


                //////////////Hues ...
                for (inner = 0; inner < chromaKeysArrayLength; inner++)
                {
                    if( mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                        && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                        && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                      )
                        break;
                }

                if (inner == chromaKeysArrayLength)
                {
                    mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                    mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                    mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                    mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                    mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                    mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                    chromaKeysArrayLength++;

                    if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                        break;
                }

                if(drawDetectionGuilds)
                {
                    mDss.data[index + 2] = 75;
                    mDss.data[index + 1] = 170;
                    mDss.data[index] = 25;
                }
            }
            if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                break;
        }
        /////////////////////////////////

        totalLinesToDetect += (HEIGHT - leftRightArea) > 0 ? (HEIGHT - leftRightArea) : 0;

        ///////////right side////////////
        for (int r = leftRightArea + distanceFromEdge, index, index2, channels = mDss.channels(), incSrc = WIDTH * channels; r < HEIGHT - distanceFromEdge; r++)
//        for(int c = bottomTopArea, index, index2, channels = mDss.channels(), incSrc = WIDTH * channels; c >= bottomBottomArea && !foundEdge; c--)
        {
            index = (r * WIDTH * channels) + (bottomTopArea * channels);
            index2 = (r * WIDTH) + bottomTopArea - distanceFromEdge;
            foundEdge = false;

            for(int c = bottomTopArea; c > distanceFromEdge; c--, index -= channels, index2--)
//            for (int r = leftRightArea; r <= HEIGHT; r++, index += incSrc, index2+= WIDTH)
            {
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);

                for(int mi = -distanceFromEdge; mi < distanceFromEdge; mi++)
                {
                    if(mask.data[index2 + (mi * WIDTH)] != 0)
                    {
                        foundEdge = true;
                        break;
                    }
                }
                if(foundEdge) {
                    //printf("\n****found edge");
                    foundEdge = false;
                    if (c > faceBottom ) {
                        foundedEdges++;
                    }
                    break;

                }
                if(mask.data[index2] != 0)
                    break;
//                if(mask.data[index2] != 0)
//                {
//                    foundEdge = true;
//                    break;
//                }
                ////////////////Find HSL/////////////////
                rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
                if(dss[index + 2] < dss[index + 1])
                {
                    if(dss[index + 2] < dss[index])
                    {
                        min = rt;
                        if(dss[index + 1] < dss[index]) max = bt;
                        else max = gt;
                    }
                    else { min = bt;   max = gt; }
                }
                else if(dss[index] < dss[index + 2])
                {
                    max = rt;
                    if(dss[index + 1] < dss[index])
                        min = gt;
                    else min = bt;
                }
                else { max = bt;   min = gt; }

                delta = max - min;
                if(delta == 0)
                    hue = 0;
                else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

                luminance = (max + min) / 2;
                if(delta == 0)
                    saturation = 0;
                else
                    saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                if (hue < 0) hue = hue + 360;
                ////////////// End HSL //////////////


                //////////////Hues ...
                for (inner = 0; inner < chromaKeysArrayLength; inner++)
                {
                    if( mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                        && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                        && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                      )
                        break;
                }

                if (inner == chromaKeysArrayLength)
                {
                    mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                    mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                    mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                    mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                    mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                    mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                    chromaKeysArrayLength++;

                    if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                        break;
                }

                if(drawDetectionGuilds)
                {
                    mDss.data[index + 2] = 255;
                    mDss.data[index + 1] = 0;
                    mDss.data[index] = 0;
                }
            }
            if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                break;
        }
    }
    else return DetectionResponseNoFaceDetection;

    printf("\nfounded edges: %d, total lines to detect: %d, ratio: %f", foundedEdges, totalLinesToDetect, (float)foundedEdges/totalLinesToDetect);
    ////////////////////////////////////////////////////////
    if (chromaKeysArrayLength < CHROMA_KEY_ARRAY_LENGTH && chromaKeysArrayLength > 2 && (float)foundedEdges/totalLinesToDetect < EDGES_RATIO)
    {
        baseChromaKeysArrayLength = chromaKeysArrayLength;
        return DetectionResponseSuccess;
    }
    else
    {
        if (!(chromaKeysArrayLength < CHROMA_KEY_ARRAY_LENGTH)) {

            printf ("DetectionResponseTooManyColors");
            return DetectionResponseTooManyColors;
        }

        if (!((float)foundedEdges/totalLinesToDetect < EDGES_RATIO)) {

            printf ("DetectionResponseTooManyEdges");
            return DetectionResponseTooManyEdges;
        }
        if (!(chromaKeysArrayLength > 2)) {
            
            printf ("DetectionResponseNoChromaColors");
            printf("\nface: top:%d, bottom:%d, left:%d, right:%d\nright of left: %d, left of right: %d, top of bottom: %d, bottom of top: %d" ,faceTop, faceBottom, faceLeft, faceRight, rightLeftArea, leftRightArea, bottomTopArea, bottomBottomArea);
            return DetectionResponseNoChromaColors;
        }
        return DetectionResponseFailure;
    }
}

DetectionResponse chromaKey::addChromaKeys(Mat mDss, int faceLeft, int faceTop, int faceRight, int faceBottom, bool frontCamera)
{
//    Mat mask(mDss.size(), 0);
//    cvtColor(mDss, mask, COLOR_RGBA2GRAY);
//    blur(mask, mask, Size(3, 3));
//    blur(mask, mask, Size(3, 3));
//    Canny(mask, mask, 20, 60, 3);
//

    Mat mask(mDss.size(), 0);
    cvtColor(mDss, mask, COLOR_RGBA2GRAY);

    Mat bMask;

    blur(mask, bMask, Size(3, 3));
    mask.release();

    blur(bMask, mask, Size(3, 3));
    bMask.release();

    Canny(mask, bMask, 20, 60, 3);
    mask.release();

    mask = bMask;


    //static int WIDTH = mDss.cols, HEIGHT = mDss.rows, WIDTH_X_4 = WIDTH * 4;

    int padding = (int) (((float) (faceBottom - faceTop)) * PADDING_MULTIPLIER);
    int paddingHalf = (int) (((float) (faceBottom - faceTop)) * PADDING_MULTIPLIER_HALF);
    int paddingWidth = (int) (((float) (faceRight - faceLeft)) * PADDING_MULTIPLIER_HALF);// / 3;

    int bottomTopArea = frontCamera ? faceBottom + padding : WIDTH - 80;
    
    if(bottomTopArea > WIDTH - 1)
        bottomTopArea = WIDTH - 1;
    
    int topBottomArea = frontCamera ? faceTop + paddingHalf : 1;
    int rightLeftArea = frontCamera ? faceLeft - paddingWidth : HEIGHT/5;
    int leftRightArea = frontCamera ? faceRight + paddingWidth : HEIGHT - HEIGHT/5;

    if(bottomTopArea > WIDTH)
        bottomTopArea = WIDTH;
    else if(topBottomArea < 1)
        topBottomArea = 1;

    Mat bDss(mDss.size(), mDss.type());

    blur(mDss, bDss, Kernel_5X5);

    uchar *dss = bDss.data;
    ////////////////// Init chroma key

    float rt, gt, bt, min, max, delta;
    float saturation = 0, luminance = 0, hue = 0;

    int inner;

    ///////////////////////////DETECTION////////////////////////////////////

    //if (faceLeft != 0)
    //{
        int distanceFromEdge = 5;
        bool foundEdge = false;
        ///////////Top part/////////////
        for (int c = WIDTH - 2, r, index, index2, channels = mDss.channels(), incSrc = WIDTH * channels, incSrc2 = incSrc * 2; c > bottomTopArea; c--)
        {
            index = c * channels + incSrc2;
            index2 = c + WIDTH + (distanceFromEdge * WIDTH);
            foundEdge = false;

            for (r = 2; r < HEIGHT - distanceFromEdge; r++, index += incSrc, index2 += WIDTH)
            {
                for(int mi = -distanceFromEdge; mi < distanceFromEdge; mi++)
                {
                    if(mask.data[index2 + mi] != 0)
                    {
                        foundEdge = true;
                        break;
                    }
                }
                if(foundEdge)
                    break;

//                if(mask.data[index2] != 0)  break;
//
//        for (int c = WIDTH - 2, r, index, index2, channels = mDss.channels(), incSrc = WIDTH * channels, incSrc2 = incSrc * 2; c >= bottomTopArea; c--)
//        {
//            index = c * channels + incSrc2;
//            index2 = c + WIDTH;
//            for (r = 2; r <= HEIGHT; r++, index += incSrc, index2 += WIDTH)
//            {
//                if(mask.data[index2] != 0)  break;
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);

                ////////////////Find HSL/////////////////
                rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
                if(dss[index + 2] < dss[index + 1])
                {
                    if(dss[index + 2] < dss[index])
                    {
                        min = rt;
                        if(dss[index + 1] < dss[index]) max = bt;
                        else max = gt;
                    }
                    else { min = bt;   max = gt; }
                }
                else if(dss[index] < dss[index + 2])
                {
                    max = rt;
                    if(dss[index + 1] < dss[index])
                        min = gt;
                    else min = bt;
                }
                else { max = bt;   min = gt; }

                delta = max - min;
                if(delta == 0)
                    hue = 0;
                else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

                luminance = (max + min) / 2;
                if(delta == 0)
                    saturation = 0;
                else
                    saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                if (hue < 0) hue = hue + 360;
                ////////////// End HSL //////////////


                //////////////Hues ...
                for (inner = 0; inner < chromaKeysArrayLength; inner++)
                {
                    if (mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                        && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                        && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                       )
                        break;
                }

                if (inner == chromaKeysArrayLength)
                {
                    mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                    mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                    mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                    mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                    mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                    mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                    chromaKeysArrayLength++;

                    if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                        break;
                }

                if(drawDetectionGuilds)
                {
                    mDss.data[index + 2] = 125;
                    mDss.data[index + 1] = 125;
                    mDss.data[index] = 125;
                }
            }
            if (chromaKeysArrayLength > CHROMA_KEY_ARRAY_LENGTH)
                break;

            r += distanceFromEdge;
            if(r < HEIGHT)
            {
                index = (HEIGHT * WIDTH_X_4) + (c * channels) - WIDTH_X_4;
                index2 = (HEIGHT * WIDTH) + c - WIDTH - (distanceFromEdge * WIDTH);
                foundEdge = false;

                for (r = HEIGHT; r > distanceFromEdge; r--, index -= incSrc, index2 -= WIDTH)
                {
                    for(int mi = -distanceFromEdge; mi < distanceFromEdge; mi++)
                    {
                        if(mask.data[index2 + mi] != 0)
                        {
                            foundEdge = true;
                            break;
                        }
                    }
                    if(foundEdge)
                        break;
//                    if(mask.data[index2] != 0)  break;
//            if(r < HEIGHT)
//            {
//                index = (HEIGHT * WIDTH_X_4) + (c * channels) - WIDTH_X_4;
//                index2 = (HEIGHT * WIDTH) + c - WIDTH;
//                for (r = HEIGHT; r != 0; r--, index -= incSrc, index2 -= WIDTH)
//                {
//                    if(mask.data[index2] != 0)  break;
                    ////////////////Find HSL/////////////////
                    rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
                    ////////////// End HSL //////////////

                    //////////////Hues ...
                    for (inner = 0; inner < chromaKeysArrayLength; inner++)
                    {
                        if (mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                            && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                            && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                                )
                            break;
                    }

                    if (inner == chromaKeysArrayLength)
                    {
                        mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                        mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                        mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                        mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                        mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                        mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                        chromaKeysArrayLength++;

                        if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                            break;
                    }

                    if(drawDetectionGuilds)
                    {
                        mDss.data[index + 2] = 125;
                        mDss.data[index + 1] = 125;
                        mDss.data[index] = 125;
                    }
                }
            }

        }

        /////////////Left side/////////////


        ///////////Left side/////////////
        for (int r = distanceFromEdge, index, index2, channels = mDss.channels(), incSrc = WIDTH * channels; r < rightLeftArea - distanceFromEdge; r++)//, indexMask += widthMask) && rDst < dst.rows
        {
            index = (r * WIDTH_X_4) + (bottomTopArea * channels);
            index2 = (r * WIDTH) + bottomTopArea - distanceFromEdge;
            foundEdge = false;

            for (int c = bottomTopArea; c > distanceFromEdge; c--, index -= channels, index2--)// && cDst > 0
            {
                for(int mi = -distanceFromEdge; mi < distanceFromEdge; mi++)
                {
                    if(mask.data[index2 + (mi * WIDTH)] != 0)
                    {
                        foundEdge = true;
                        break;
                    }
                }
                if(foundEdge)
                    break;

//                if(mask.data[index2] != 0)
//                    break;
//        for (int c = bottomTopArea, index, channels = mDss.channels(), incSrc = WIDTH * channels, incSrc2 = incSrc * 2; c >= topBottomArea; c--)// && cDst > 0
//        {
//            index = c * channels + incSrc2;
//
//            for (int r = 2; r <= rightLeftArea; r++, index += incSrc)//, indexMask += widthMask) && rDst < dst.rows
//            {
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);

                ////////////////Find HSL/////////////////
                rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
                if(dss[index + 2] < dss[index + 1])
                {
                    if(dss[index + 2] < dss[index])
                    {
                        min = rt;
                        if(dss[index + 1] < dss[index]) max = bt;
                        else max = gt;
                    }
                    else { min = bt;   max = gt; }
                }
                else if(dss[index] < dss[index + 2])
                {
                    max = rt;
                    if(dss[index + 1] < dss[index])
                        min = gt;
                    else min = bt;
                }
                else { max = bt;   min = gt; }

                delta = max - min;
                if(delta == 0)
                    hue = 0;
                else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

                luminance = (max + min) / 2;
                if(delta == 0)
                    saturation = 0;
                else
                    saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                if (hue < 0) hue = hue + 360;
                ////////////// End HSL //////////////


                //////////////Hues ...
                for (inner = 0; inner < chromaKeysArrayLength; inner++)
                {
                    if (mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                        && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                        && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                       )
                        break;
                }

                if (inner == chromaKeysArrayLength)
                {
                    mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                    mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                    mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                    mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                    mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                    mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                    chromaKeysArrayLength++;

                    if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                        break;
                }

                if(drawDetectionGuilds)
                {
                    mDss.data[index + 2] = 225;
                    mDss.data[index + 1] = 225;
                    mDss.data[index] = 225;
                }
            }
            if (chromaKeysArrayLength > CHROMA_KEY_ARRAY_LENGTH)
                break;
        }


        /////////////Right Side //////////////
        for (int r = leftRightArea + distanceFromEdge, index, index2, channels = mDss.channels(), incSrc = WIDTH * channels; r < HEIGHT - distanceFromEdge; r++)
        {
            index = (r * WIDTH * channels) + (bottomTopArea * channels);
            index2 = (r * WIDTH) + bottomTopArea - distanceFromEdge;
            foundEdge = false;

            for(int c = bottomTopArea; c > distanceFromEdge; c--, index -= channels, index2--)
            {
                for(int mi = -distanceFromEdge; mi < distanceFromEdge; mi++)
                {
                    if(mask.data[index2 + (mi * WIDTH)] != 0)
                    {
                        foundEdge = true;
                        break;
                    }
                }
                if(foundEdge)
                    break;

//                if(mask.data[index2] != 0)
//                    break;
//
//        for(int c = bottomTopArea, index, channels = mDss.channels(), incSrc = WIDTH * channels; c >= topBottomArea; c--)// && cDst > 0
//        {
//            index = (leftRightArea * incSrc) + c * channels;
//
//            for (int r = leftRightArea; r <= HEIGHT; r++, index += incSrc)
//            {
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);

                ////////////////Find HSL/////////////////
                rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
                if(dss[index + 2] < dss[index + 1])
                {
                    if(dss[index + 2] < dss[index])
                    {
                        min = rt;
                        if(dss[index + 1] < dss[index]) max = bt;
                        else max = gt;
                    }
                    else { min = bt;   max = gt; }
                }
                else if(dss[index] < dss[index + 2])
                {
                    max = rt;
                    if(dss[index + 1] < dss[index])
                        min = gt;
                    else min = bt;
                }
                else { max = bt;   min = gt; }

                delta = max - min;
                if(delta == 0)
                    hue = 0;
                else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

                luminance = (max + min) / 2;
                if(delta == 0)
                    saturation = 0;
                else
                    saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                if (hue < 0) hue = hue + 360;
                ////////////// End HSL //////////////


                //////////////Hues ...
                for (inner = 0; inner < chromaKeysArrayLength; inner++)
                {
                    if( mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                        && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                        && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                      )
                        break;
                }

                if (inner == chromaKeysArrayLength)
                {
                    mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                    mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                    mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                    mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                    mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                    mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                    chromaKeysArrayLength++;

                    if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                        break;
                }

                if(drawDetectionGuilds)
                {
                    mDss.data[index + 2] = 255;
                    mDss.data[index + 1] = 0;
                    mDss.data[index] = 0;
                }
            }
            if (chromaKeysArrayLength > CHROMA_KEY_ARRAY_LENGTH)
                break;
        }
    //}

    ////////////////////////////////////////////////////////
    if (chromaKeysArrayLength < CHROMA_KEY_ARRAY_LENGTH)
    {
        baseChromaKeysArrayLength = chromaKeysArrayLength;
        return DetectionResponseSuccess;
    }
    else
    {
        //chromaKeysArrayLength = 0;
        //baseChromaKeysArrayLength = chromaKeysArrayLength;
        return DetectionResponseFailure;
    }
}


void chromaKey::addChromaKey(uchar* dss, int index)
{
    float rt = (dss[index + 2] & 0xFF) / 255.0f;
    float gt = (dss[index + 1] & 0xFF) / 255.0;
    float bt = (dss[index] & 0xFF) / 255.0f;

    float max, min;

    if(dss[index + 2] < dss[index + 1])
    {
        if(dss[index + 2] < dss[index])
        {
            min = rt;
            if(dss[index + 1] < dss[index]) max = bt;
            else max = gt;
        }
        else { min = bt;   max = gt; }
    }
    else if(dss[index] < dss[index + 2])
    {
        max = rt;
        if(dss[index + 1] < dss[index])
            min = gt;
        else min = bt;
    }
    else { max = bt;   min = gt; }

    float delta = max - min;
    float luminance = (max + min) / 2, hue, saturation;
    if(delta == 0)
    {
        hue = 0;
        saturation = 0;
    }
    else
    {
        saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
        if (max == rt)      hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
        else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
        else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );
        if (hue < 0) hue = hue + 360;
    }

    ////////////// End HSL //////////////

    int inner;


    for(inner = 0; inner < chromaKeysArrayLength; inner++)
    {
        if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
           && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
           && mLuminanceBot[inner] - DIF_LUMINANCE_THRESHOLD < luminance && luminance < mLuminanceTop[inner]
                )
        {
            break;
        }
    }

    if(inner < chromaKeysArrayLength)
    {
        mLuminanceBot[inner] -= DIF_LUMINANCE_THRESHOLD;
    }
    else
    {
        for(inner = 0; inner < chromaKeysArrayLength; inner++)
        {
            if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
               && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
               && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner] + DIF_LUMINANCE_THRESHOLD
                    )
            {
                break;
            }
        }

        if(inner < chromaKeysArrayLength)
        {
            mLuminanceTop[inner] += DIF_LUMINANCE_THRESHOLD;
        }
        else
        {
            for(inner = 0; inner < chromaKeysArrayLength; inner++)
            {
                if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
                   && mSaturationBot[inner] - DIF_SATURATION_THRESHOLD < saturation && saturation < mSaturationTop[inner]
                   && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                        )
                {
                    break;
                }
            }
        }

        if(inner < chromaKeysArrayLength)
        {
            mSaturationBot[inner] -= DIF_SATURATION_THRESHOLD;
        }
        else
        {
            for(inner = 0; inner < chromaKeysArrayLength; inner++)
            {
                if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
                   && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner] + DIF_SATURATION_THRESHOLD
                                                            && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                        )
                {
                    break;
                }
            }
            if(inner < chromaKeysArrayLength)
            {
                mSaturationTop[inner] += DIF_SATURATION_THRESHOLD;
            }
            else
            {
                if(inner < chromaKeysArrayLength)
                {
                    mSaturationBot[inner] -= DIF_SATURATION_THRESHOLD;
                }
                else
                {
                    for(inner = 0; inner < chromaKeysArrayLength; inner++)
                    {
                        if(mHuesBot[inner] - DIF_HUE_THRESHOLD < hue && hue < mHuesTop[inner]
                           && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner] + DIF_SATURATION_THRESHOLD
                           && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                                )
                        {
                            break;
                        }
                    }
                    if(inner < chromaKeysArrayLength)
                    {
                        mHuesBot[inner] -= DIF_HUE_THRESHOLD;
                    }
                    else
                    {
                        for(inner = 0; inner < chromaKeysArrayLength; inner++)
                        {
                            if(mHuesBot[inner] < hue && hue < mHuesTop[inner] + DIF_HUE_THRESHOLD
                               && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner] + DIF_SATURATION_THRESHOLD
                               && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                                    )
                            {
                                break;
                            }
                        }
                        if(inner < chromaKeysArrayLength)
                        {
                            mHuesTop[inner] += DIF_HUE_THRESHOLD;
                        }

                    }

                }
            }
//            else
//                __android_log_print(ANDROID_LOG_ERROR, "tag43", "checkAndDeductChromaKeysAgainstSkinColor>chromaKeysArrayLength be4>  %d    %d", baseChromaKeysArrayLength, chromaKeysArrayLength);
//            else
//                __android_log_print(ANDROID_LOG_ERROR, "tag43", "checkAndDeductChromaKeysAgainstSkinColor>chromaKeysArrayLength be4>  %d    %d", baseChromaKeysArrayLength, chromaKeysArrayLength);

        }

    }



}

void chromaKey::addChromaKeysSectioned(Mat &mDss, int left, int top, int right, int bottom)
{
    if (chromaKeysArrayLength > CHROMA_KEY_ARRAY_LENGTH)
        return;

    uchar* dss = mDss.data;

    if(left < 0)
        left = 0;

    if(right > HEIGHT)
        right = HEIGHT;

    if(top < 0)
        top = 0;

    if(bottom > WIDTH)
        bottom = WIDTH;

    int baseIndex = left * WIDTH_X_4;


    float hue, luminance, saturation, max, min, rt, gt, bt, delta;
    int inner;
    for (int c = bottom, index; c > top; c--)
    {
        index = baseIndex + (c * 4);
        for(int r = left; r < right; r++, index += WIDTH_X_4)
        {
            ////////////////Find HSL/////////////////
            rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
            if(dss[index + 2] < dss[index + 1])
            {
                if(dss[index + 2] < dss[index])
                {
                    min = rt;
                    if(dss[index + 1] < dss[index]) max = bt;
                    else max = gt;
                }
                else { min = bt;   max = gt; }
            }
            else if(dss[index] < dss[index + 2])
            {
                max = rt;
                if(dss[index + 1] < dss[index])
                    min = gt;
                else min = bt;
            }
            else { max = bt;   min = gt; }

            delta = max - min;
            if(delta == 0)
                hue = 0;
            else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
            else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
            else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

            luminance = (max + min) / 2;
            if(delta == 0)
                saturation = 0;
            else
                saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
            if (hue < 0) hue = hue + 360;
            ////////////// End HSL //////////////


            //////////////Hues ...
            for (inner = 0; inner < chromaKeysArrayLength; inner++)
            {
                if (mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                    && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                    && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                        )
                    break;
            }

            if (inner == chromaKeysArrayLength)
            {
                mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                chromaKeysArrayLength++;

                if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                    return;
            }

            if(drawDetectionGuilds)
            {
                mDss.data[index + 2] = 125;
                mDss.data[index + 1] = 125;
                mDss.data[index] = 125;
            }
        }

    }

}

Mat chromaKey::checkAndAddChromaKeysTopArea(Mat mDss)
{
    int leftSize = CHROMA_KEY_ARRAY_LENGTH - chromaKeysArrayLength;

    if(leftSize <= 0)   return mDss;

    Mat topArea;
    iUtil.cropImage(mDss, topArea, 0, 0, mDss.rows, TOP_AREA_WIDTH);

    if(isKeysEmpty())
        return topArea;

    int HEIGHT = topArea.rows;
    int channels = topArea.channels();
    int incDst = TOP_AREA_WIDTH * channels;

    blur(topArea, topArea, Kernel_5X5);
    uchar* dss = topArea.data;

    float saturation = 0, luminance = 0, hue = 0;

    int  inner;

    float lastHue, lastSaturation, lastLuminance;
    bool foundEdge;

    float rt, gt, bt, min, max, delta;

    for (int c = TOP_AREA_WIDTH, index, r; c >= 0; c--)
    {
        foundEdge = false;
        index = c * channels;
//        rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

        ////////////////Find HSL/////////////////
        rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
        if(dss[index + 2] < dss[index + 1])
        {
            if(dss[index + 2] < dss[index])
            {
                min = rt;
                if(dss[index + 1] < dss[index]) max = bt;
                else max = gt;
            }
            else { min = bt;   max = gt; }
        }
        else if(dss[index] < dss[index + 2])
        {
            max = rt;
            if(dss[index + 1] < dss[index])
                min = gt;
            else min = bt;
        }
        else { max = bt;   min = gt; }

        delta = max - min;
        if(delta == 0)
            hue = 0;
        else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
        else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
        else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

        luminance = (max + min) / 2;
        if(delta == 0)
            saturation = 0;
        else
            saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
        if (hue < 0) hue = hue + 360;
        ////////////// End HSL //////////////

        lastHue = hue;  lastSaturation = saturation;    lastLuminance = luminance;

        for(r = 1; r < HEIGHT; r++, index += incDst)
        {
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
            ////////////////Find HSL/////////////////
            rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
            if(dss[index + 2] < dss[index + 1])
            {
                if(dss[index + 2] < dss[index])
                {
                    min = rt;
                    if(dss[index + 1] < dss[index]) max = bt;
                    else max = gt;
                }
                else { min = bt;   max = gt; }
            }
            else if(dss[index] < dss[index + 2])
            {
                max = rt;
                if(dss[index + 1] < dss[index])
                    min = gt;
                else min = bt;
            }
            else { max = bt;   min = gt; }

            delta = max - min;
            if(delta == 0)
                hue = 0;
            else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
            else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
            else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

            luminance = (max + min) / 2;
            if(delta == 0)
                saturation = 0;
            else
                saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
            if (hue < 0) hue = hue + 360;
            ////////////// End HSL //////////////


            if(lastHue - DIF_HUE_THRESHOLD > hue || hue > lastHue + DIF_HUE_THRESHOLD
                || lastSaturation - DIF_SATURATION_THRESHOLD > saturation || saturation > lastSaturation + DIF_SATURATION_THRESHOLD
                || lastLuminance - DIF_LUMINANCE_THRESHOLD > luminance || luminance > lastLuminance + DIF_LUMINANCE_THRESHOLD
                    )
            {
                foundEdge = true;
                break;
            }
            lastHue = hue;  lastSaturation = saturation;    lastLuminance = luminance;
        }

        if(foundEdge)   continue;

        index = c * channels;
        for(r = 0; r < HEIGHT; r++, index += incDst)
        {
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

            ////////////////Find HSL/////////////////
            rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
            if(dss[index + 2] < dss[index + 1])
            {
                if(dss[index + 2] < dss[index])
                {
                    min = rt;
                    if(dss[index + 1] < dss[index]) max = bt;
                    else max = gt;
                }
                else { min = bt;   max = gt; }
            }
            else if(dss[index] < dss[index + 2])
            {
                max = rt;
                if(dss[index + 1] < dss[index])
                    min = gt;
                else min = bt;
            }
            else { max = bt;   min = gt; }

            delta = max - min;
            if(delta == 0)
                hue = 0;
            else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
            else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
            else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

            luminance = (max + min) / 2;
            if(delta == 0)
                saturation = 0;
            else
                saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
            if (hue < 0) hue = hue + 360;
            ////////////// End HSL //////////////

            //////////////
            for (inner = 0; inner < chromaKeysArrayLength; inner++)
            {
                if( mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                    && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                    && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                  )
                    break;
            }

            if (inner == chromaKeysArrayLength)
            {
                mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                chromaKeysArrayLength++;

                if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                    break;
            }

            if(drawDetectionGuilds)
            {
                mDss.data[index + 2] = 125;
                mDss.data[index + 1] = 125;
                mDss.data[index] = 125;
            }
        }
    }

    return topArea;
}

Mat chromaKey::checkAndAddChromaKeysLeftArea(Mat mDss)
{
    uchar* dataToPaintOn = mDss.data;

    int leftSize = CHROMA_KEY_ARRAY_LENGTH - chromaKeysArrayLength;
    if(leftSize <= 0)
        return mDss;

    Mat leftArea;
    iUtil.cropImage(mDss, leftArea, 0, TOP_AREA_WIDTH, LEFT_AREA_HEIGHT, mDss.cols);//(Size(topAreaWidth, mDss.rows), mDss.type());

    if(isKeysEmpty())   return leftArea;

    int WIDTH = leftArea.cols, HEIGHT = leftArea.rows, WIDTH_CH = WIDTH * leftArea.channels();
    int channels = leftArea.channels();

    blur(leftArea, leftArea, Kernel_5X5);
    uchar* dss = leftArea.data;

    float saturation = 0, luminance = 0, hue = 0;

    int  inner;

    float rt, gt, bt, min, max, delta;

    float lastHue, lastSaturation, lastLuminance;
    bool foundEdge;
    for(int r = 0, c, index; r < LEFT_AREA_HEIGHT; r++)
    {
        foundEdge = false;
        index = (r * WIDTH_CH) - channels;
//        rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

        ////////////////Find HSL/////////////////
        rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
        if(dss[index + 2] < dss[index + 1])
        {
            if(dss[index + 2] < dss[index])
            {
                min = rt;
                if(dss[index + 1] < dss[index]) max = bt;
                else max = gt;
            }
            else { min = bt;   max = gt; }
        }
        else if(dss[index] < dss[index + 2])
        {
            max = rt;
            if(dss[index + 1] < dss[index])
                min = gt;
            else min = bt;
        }
        else { max = bt;   min = gt; }

        delta = max - min;
        if(delta == 0)
            hue = 0;
        else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
        else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
        else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

        luminance = (max + min) / 2;
        if(delta == 0)
            saturation = 0;
        else
            saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
        if (hue < 0) hue = hue + 360;
        ////////////// End HSL //////////////

        lastHue = hue;  lastSaturation = saturation;    lastLuminance = luminance;

        for (c = WIDTH - 1, index -= channels; c > 0; c--, index -= channels)
        {
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

            ////////////////Find HSL/////////////////
            rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
            if(dss[index + 2] < dss[index + 1])
            {
                if(dss[index + 2] < dss[index])
                {
                    min = rt;
                    if(dss[index + 1] < dss[index]) max = bt;
                    else max = gt;
                }
                else { min = bt;   max = gt; }
            }
            else if(dss[index] < dss[index + 2])
            {
                max = rt;
                if(dss[index + 1] < dss[index])
                    min = gt;
                else min = bt;
            }
            else { max = bt;   min = gt; }

            delta = max - min;
            if(delta == 0)
                hue = 0;
            else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
            else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
            else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

            luminance = (max + min) / 2;
            if(delta == 0)
                saturation = 0;
            else
                saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
            if (hue < 0) hue = hue + 360;
            ////////////// End HSL //////////////

            if(lastHue - DIF_HUE_THRESHOLD > hue || hue > lastHue + DIF_HUE_THRESHOLD
               || lastSaturation - DIF_SATURATION_THRESHOLD > saturation || saturation > lastSaturation + DIF_SATURATION_THRESHOLD
               || lastLuminance - DIF_LUMINANCE_THRESHOLD > luminance || luminance > lastLuminance + DIF_LUMINANCE_THRESHOLD
              )
            {
                foundEdge = true;
                break;
            }
            lastHue = hue;  lastSaturation = saturation;    lastLuminance = luminance;
        }

        if(foundEdge)   continue;

        index = (r * WIDTH_CH) - channels;
        for (c = WIDTH - 1; c >= 0; c--, index -= channels)
        {
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

            ////////////////Find HSL/////////////////
            rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
            if(dss[index + 2] < dss[index + 1])
            {
                if(dss[index + 2] < dss[index])
                {
                    min = rt;
                    if(dss[index + 1] < dss[index]) max = bt;
                    else max = gt;
                }
                else { min = bt;   max = gt; }
            }
            else if(dss[index] < dss[index + 2])
            {
                max = rt;
                if(dss[index + 1] < dss[index])
                    min = gt;
                else min = bt;
            }
            else { max = bt;   min = gt; }

            delta = max - min;
            if(delta == 0)
                hue = 0;
            else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
            else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
            else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

            luminance = (max + min) / 2;
            if(delta == 0)
                saturation = 0;
            else
                saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
            if (hue < 0) hue = hue + 360;
            ////////////// End HSL //////////////

            //////////////Hues ...
            for (inner = 0; inner < chromaKeysArrayLength; inner++)
            {
                if( mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                    && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                    && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                  )
                    break;
            }

            if (inner == chromaKeysArrayLength)
            {
                mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                chromaKeysArrayLength++;

                if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                    break;
            }
            if(drawDetectionGuilds)
            {
                dataToPaintOn[index + 2] = 255;
                dataToPaintOn[index + 1] = 0;
                dataToPaintOn[index] = 0;
            }
        }
    }
    return leftArea;
}

Mat chromaKey::checkAndAddChromaKeysRightArea(Mat mDss)
{
    uchar* dataToPaintOn = mDss.data;

    int leftSize = CHROMA_KEY_ARRAY_LENGTH - chromaKeysArrayLength;
    if(leftSize <= 0)   return mDss;

    Mat leftArea;
    iUtil.cropImage(mDss, leftArea, mDss.rows - RIGHT_AREA_HEIGHT, TOP_AREA_WIDTH, mDss.rows, mDss.cols);//(Size(topAreaWidth, mDss.rows), mDss.type());
    if(isKeysEmpty())   return leftArea;

    int WIDTH = leftArea.cols, HEIGHT = leftArea.rows, WIDTH_CH = WIDTH * leftArea.channels();
    int channels = leftArea.channels();

    blur(leftArea, leftArea, Kernel_5X5);
    uchar* dss = leftArea.data;

    float saturation = 0, luminance = 0, hue = 0;

    int  inner;

    float rt, gt, bt, min, max, delta;
    float lastHue, lastSaturation, lastLuminance;
    bool foundEdge;
    for(int r = 0, c, index; r < RIGHT_AREA_HEIGHT; r++)
    {
        foundEdge = false;
        index = (r * WIDTH_CH) - channels;
//        rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

        ////////////////Find HSL/////////////////
        rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
        if(dss[index + 2] < dss[index + 1])
        {
            if(dss[index + 2] < dss[index])
            {
                min = rt;
                if(dss[index + 1] < dss[index]) max = bt;
                else max = gt;
            }
            else { min = bt;   max = gt; }
        }
        else if(dss[index] < dss[index + 2])
        {
            max = rt;
            if(dss[index + 1] < dss[index])
                min = gt;
            else min = bt;
        }
        else { max = bt;   min = gt; }

        delta = max - min;
        if(delta == 0)
            hue = 0;
        else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
        else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
        else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

        luminance = (max + min) / 2;
        if(delta == 0)
            saturation = 0;
        else
            saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
        if (hue < 0) hue = hue + 360;
        ////////////// End HSL //////////////


        lastHue = hue;    lastSaturation = saturation;    lastLuminance = luminance;

        for (c = WIDTH - 1, index -= channels; c > 0; c--, index -= channels)
        {
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

            ////////////////Find HSL/////////////////
            rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
            if(dss[index + 2] < dss[index + 1])
            {
                if(dss[index + 2] < dss[index])
                {
                    min = rt;
                    if(dss[index + 1] < dss[index]) max = bt;
                    else max = gt;
                }
                else { min = bt;   max = gt; }
            }
            else if(dss[index] < dss[index + 2])
            {
                max = rt;
                if(dss[index + 1] < dss[index])
                    min = gt;
                else min = bt;
            }
            else { max = bt;   min = gt; }

            delta = max - min;
            if(delta == 0)
                hue = 0;
            else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
            else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
            else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

            luminance = (max + min) / 2;
            if(delta == 0)
                saturation = 0;
            else
                saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
            if (hue < 0) hue = hue + 360;
            ////////////// End HSL //////////////

            if(lastHue - DIF_HUE_THRESHOLD > hue || hue > lastHue + DIF_HUE_THRESHOLD
               || lastSaturation - DIF_SATURATION_THRESHOLD > saturation || saturation > lastSaturation + DIF_SATURATION_THRESHOLD
               || lastLuminance - DIF_LUMINANCE_THRESHOLD > luminance || luminance > lastLuminance + DIF_LUMINANCE_THRESHOLD
                    )
            {
                foundEdge = true;
                break;
            }
            lastHue = hue;    lastSaturation = saturation;    lastLuminance = luminance;
        }

        if(foundEdge)   continue;

        index = (r * WIDTH_CH) - channels;
        for (c = WIDTH - 1; c >= 0; c--, index -= channels)
        {
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

            ////////////////Find HSL/////////////////
            rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
            if(dss[index + 2] < dss[index + 1])
            {
                if(dss[index + 2] < dss[index])
                {
                    min = rt;
                    if(dss[index + 1] < dss[index]) max = bt;
                    else max = gt;
                }
                else { min = bt;   max = gt; }
            }
            else if(dss[index] < dss[index + 2])
            {
                max = rt;
                if(dss[index + 1] < dss[index])
                    min = gt;
                else min = bt;
            }
            else { max = bt;   min = gt; }

            delta = max - min;
            if(delta == 0)
                hue = 0;
            else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
            else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
            else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

            luminance = (max + min) / 2;
            if(delta == 0)
                saturation = 0;
            else
                saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
            if (hue < 0) hue = hue + 360;
            ////////////// End HSL //////////////

            //////////////Hues ...
            for (inner = 0; inner < chromaKeysArrayLength; inner++)
            {
                if( mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
                    && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
                    && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
                  )
                    break;
            }

            if (inner == chromaKeysArrayLength)
            {
                mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
                mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;

                mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
                mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;

                mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
                mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;

                chromaKeysArrayLength++;

                if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
                    break;
            }
            if(drawDetectionGuilds)
            {
                int indexDataToPaintOn = ((mDss.rows - RIGHT_AREA_HEIGHT + r) * mDss.channels() * mDss.cols) + (c * mDss.channels());// + (TOP_AREA_WIDTH * mDss.channels());
                dataToPaintOn[indexDataToPaintOn + 2] = 255;
                dataToPaintOn[indexDataToPaintOn + 1] = 0;
                dataToPaintOn[indexDataToPaintOn] = 0;
            }
        }
    }
    return leftArea;
}

bool chromaKey::checkChromaKeysAgainstSkinColor()
{
    for (int i = 0; i < chromaKeysArrayLength; i++)
    {
        for (int j = 0; j < skinArrayLength; j++)
        {
            if(
                //HUES
                ( (mHuesBot[i] < skinHuesBot[j] && skinHuesBot[j] < mHuesTop[i])
                || (mHuesBot[i] < skinHuesTop[j] && skinHuesTop[j] < mHuesTop[i])
                )
                // SATURATION
              &&
                ( (mSaturationBot[i] < skinSaturationBot[j] && skinSaturationBot[j] < mSaturationTop[i])
                  || (mSaturationBot[i] < skinSaturationTop[j] && skinSaturationTop[j] < mSaturationTop[i])
                )
                // LUMINANCE
              &&
                ( (mLuminanceBot[i] < skinLuminanceBot[j] && skinLuminanceBot[j] < mLuminanceTop[i])
                  || (mLuminanceBot[i] < skinLuminanceTop[j] && skinLuminanceTop[j] < mLuminanceTop[i])
                )
            )
                return false;
        }
    }
    return true;
}

bool chromaKey::checkAndDeductChromaKeysAgainstSkinColor()
{
//    __android_log_print(ANDROID_LOG_ERROR, "tag43", "checkAndDeductChromaKeysAgainstSkinColor>chromaKeysArrayLength be4>  %d    %d", baseChromaKeysArrayLength, chromaKeysArrayLength);
    if(skinArrayLength == 0 || baseChromaKeysArrayLength == chromaKeysArrayLength)
        return false;
    bool chromaKeyHasBeenDeducted = false;
//    for (int i = baseChromaKeysArrayLength; i <= chromaKeysArrayLength; i++)
    for (int i = baseChromaKeysArrayLength; i < chromaKeysArrayLength; i++)
    {
        for (int j = 0; j < skinArrayLength; j++)
        {
            if(
                    //HUES
                    ( (mHuesBot[i] < skinHuesBot[j] && skinHuesBot[j] < mHuesTop[i])
                      || (mHuesBot[i] < skinHuesTop[j] && skinHuesTop[j] < mHuesTop[i])
                    )
                    // SATURATION
                    &&
                    ( (mSaturationBot[i] < skinSaturationBot[j] && skinSaturationBot[j] < mSaturationTop[i])
                      || (mSaturationBot[i] < skinSaturationTop[j] && skinSaturationTop[j] < mSaturationTop[i])
                    )
                    // LUMINANCE
                    &&
                    ( (mLuminanceBot[i] < skinLuminanceBot[j] && skinLuminanceBot[j] < mLuminanceTop[i])
                      || (mLuminanceBot[i] < skinLuminanceTop[j] && skinLuminanceTop[j] < mLuminanceTop[i])
                    )
              )
            {
                if(i == chromaKeysArrayLength) // last cell in the array
                    chromaKeysArrayLength --;
                else
                {
                    mHuesTop[i] = mHuesTop[chromaKeysArrayLength];
                    mHuesBot[i] = mHuesBot[chromaKeysArrayLength];

                    mSaturationTop[i] = mSaturationTop[chromaKeysArrayLength];
                    mSaturationBot[i] = mSaturationBot[chromaKeysArrayLength];

                    mLuminanceTop[i] = mLuminanceTop[chromaKeysArrayLength];
                    mLuminanceBot[i] = mLuminanceBot[chromaKeysArrayLength];

                    chromaKeysArrayLength --;
                    i--;
                }
                chromaKeyHasBeenDeducted = true;
            }
        }
    }
    baseChromaKeysArrayLength = chromaKeysArrayLength;

    return chromaKeyHasBeenDeducted;
}

void chromaKey::getChromaKeysBuffer(Mat& mDss, Mat& chromaKeyBuffer, int faceLeft, int faceTop, int faceRight, int faceBottom)
{
    timeval tv;
    gettimeofday(&tv, 0);
    long time = ((tv.tv_sec * 1000) + (tv.tv_usec / 1000));
    
    clock_t begin = clock();
    Mat mDsd(mDss.size(), mDss.type());
    Mat bDss;
    blur(mDss, bDss, Kernel_5X5);
    uchar* dss = bDss.data;

    //////////////////// Local field declaration
    float rt, gt, bt, min, max, delta;
    float luminance, saturation, hue;
    bool isObject(false);
    bool isFirsto = true;
    ////////////////// image manipulation

    int widthHeightX4 = (WIDTH_X_4 * HEIGHT) - 4;
    int widthHeight = (WIDTH * HEIGHT) - 1;
    int widthX4XIncrease = WIDTH_X_4 * increase;
    int widthXIncrease = WIDTH * increase;
    bool run = true;

    for(int column = WIDTH, row, inner, index, indexChromaBuffer; column > 0; column--) // index = ((row * WIDTH) + column) * 4
    {
        index = column * 4;
        indexChromaBuffer = column;

        for (row = 0; row < HEIGHT; row += increase, index += widthX4XIncrease, indexChromaBuffer += widthXIncrease )
        {
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

            ////////////////Find HSL/////////////////
            rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
            if(dss[index + 2] < dss[index + 1])
            {
                if(dss[index + 2] < dss[index])
                {
                    min = rt;
                    if(dss[index + 1] < dss[index]) max = bt;
                    else max = gt;
                }
                else { min = bt;   max = gt; }
            }
            else if(dss[index] < dss[index + 2])
            {
                max = rt;
                if(dss[index + 1] < dss[index])
                    min = gt;
                else min = bt;
            }
            else { max = bt;   min = gt; }

            delta = max - min;
            luminance = (max + min) / 2;
            if(delta == 0)
            {
                hue = 0;
                saturation = 0;
            }
            else
            {
                saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                if (max == rt)      hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );
                if (hue < 0) hue = hue + 360;
            }

            ////////////// End HSL //////////////


            for(inner = 0; inner < chromaKeysArrayLength; inner++)
            {
                if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
                   && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
                   && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                        )
                {
                    break;
                }
            }

//            if(! isInChromaKeys(hue, luminance, saturation))
            if(inner == chromaKeysArrayLength)
            {
                if(row > 0)
                {
                    row -= increase;
                    index -= widthX4XIncrease;
                    indexChromaBuffer -= widthXIncrease;
                    run = true;

                    for (; run && row < HEIGHT; row++, index += WIDTH_X_4, indexChromaBuffer += WIDTH)
                    {
//                        rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

                        ////////////////Find HSL/////////////////
                        rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
                        if(dss[index + 2] < dss[index + 1])
                        {
                            if(dss[index + 2] < dss[index])
                            {
                                min = rt;
                                if(dss[index + 1] < dss[index]) max = bt;
                                else max = gt;
                            }
                            else { min = bt;   max = gt; }
                        }
                        else if(dss[index] < dss[index + 2])
                        {
                            max = rt;
                            if(dss[index + 1] < dss[index])
                                min = gt;
                            else min = bt;
                        }
                        else { max = bt;   min = gt; }

                        delta = max - min;
                        luminance = (max + min) / 2;
                        if(delta == 0)
                        {
                            hue = 0;
                            saturation = 0;
                        }
                        else
                        {
                            saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                            if (max == rt)      hue = 60.0f * (fmod(((gt - bt) / delta), 6.0f));
                            else if (max == gt) hue = 60.0f * (((bt - rt) / delta) + 2.0f);
                            else                hue = 60.0f * (((rt - gt) / delta) + 4.0f);
                            if (hue < 0) hue = hue + 360;
                        }

                        ////////////// End HSL //////////////

                        for(inner = 0; inner < chromaKeysArrayLength; inner++)
                        {
                            if( !(mHuesBot[inner] < hue && hue < mHuesTop[inner]
                               && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
                               && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                                    ) )
                                run = false;
                        }
//                        if(!run)
//                            break;
                    }
//                    chromaKeyBuffer.data[indexChromaBuffer] = 255;
//                    index += WIDTH_X_4;
//                    indexChromaBuffer += WIDTH;
//                    row++;
                }

                run = true;
                for ( ; row < HEIGHT; row++, index += WIDTH_X_4, indexChromaBuffer += WIDTH )
                {
//                    rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

                    ////////////////Find HSL/////////////////
                    rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
                    if(dss[index + 2] < dss[index + 1])
                    {
                        if(dss[index + 2] < dss[index])
                        {
                            min = rt;
                            if(dss[index + 1] < dss[index]) max = bt;
                            else max = gt;
                        }
                        else { min = bt;   max = gt; }
                    }
                    else if(dss[index] < dss[index + 2])
                    {
                        max = rt;
                        if(dss[index + 1] < dss[index])
                            min = gt;
                        else min = bt;
                    }
                    else { max = bt;   min = gt; }

                    delta = max - min;
                    luminance = (max + min) / 2;
                    if(delta == 0)
                    {
                        hue = 0;
                        saturation = 0;
                    }
                    else
                    {
                        saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                        if (max == rt)      hue = 60.0f * (fmod(((gt - bt) / delta), 6.0f));
                        else if (max == gt) hue = 60.0f * (((bt - rt) / delta) + 2.0f);
                        else                hue = 60.0f * (((rt - gt) / delta) + 4.0f);

                        if (hue < 0) hue = hue + 360;
                    }

                    ////////////// End HSL //////////////


                    for(inner = 0; inner < chromaKeysArrayLength; inner++)
                    {
                        if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
                           && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
                           && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                          )
                            run = false;
                    }
                    if(!run)
                        break;

                    chromaKeyBuffer.data[indexChromaBuffer] = 255;
                }
                index += WIDTH_X_4;
                indexChromaBuffer += WIDTH;
                row++;
            }
        }
    }

    bDss.release();
}

void chromaKey::getChromaKeysSectionBuffer(Mat mDss, Mat chromaKeyBuffer, int leftSide, int rightSide)
{
    Mat bDss;
    blur(mDss, bDss, Kernel_5X5);
    uchar* dss = bDss.data;

    //////////////////// Local field declaration
    float rt, gt, bt, min, max, delta;
    float luminance, saturation, hue;
    bool isObject(false);
    bool isFirsto = true;
    ////////////////// image manipulation

    int widthHeightX4 = (WIDTH_X_4 * HEIGHT) - 4;
    int widthHeight = (WIDTH * HEIGHT) - 1;
    int widthX4XIncrease = WIDTH_X_4 * increase;
    int widthXIncrease = WIDTH * increase;
    bool run = true;

    int indexBase = leftSide * WIDTH;
    int indexBaseX4 = leftSide * WIDTH * 4;


    for(int column = WIDTH, row, inner, index, indexChromaBuffer; column > 0; column--) // index = ((row * WIDTH) + column) * 4
    {
        index = indexBaseX4 + (column * 4);
        indexChromaBuffer = indexBase + column;

        for (row = leftSide; row < rightSide; row += increase, index += widthX4XIncrease, indexChromaBuffer += widthXIncrease )
        {
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

            ////////////////Find HSL/////////////////
            rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
            if(dss[index + 2] < dss[index + 1])
            {
                if(dss[index + 2] < dss[index])
                {
                    min = rt;
                    if(dss[index + 1] < dss[index]) max = bt;
                    else max = gt;
                }
                else { min = bt;   max = gt; }
            }
            else if(dss[index] < dss[index + 2])
            {
                max = rt;
                if(dss[index + 1] < dss[index])
                    min = gt;
                else min = bt;
            }
            else { max = bt;   min = gt; }

            delta = max - min;
            luminance = (max + min) / 2;
            if(delta == 0)
            {
                hue = 0;
                saturation = 0;
            }
            else
            {
                saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                if (max == rt)      hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );
                if (hue < 0) hue = hue + 360;
            }

            ////////////// End HSL //////////////


            for(inner = 0; inner < chromaKeysArrayLength; inner++)
            {
                if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
                   && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
                   && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                        )
                {
                    break;
                }
            }

//            if(! isInChromaKeys(hue, luminance, saturation))
            if(inner == chromaKeysArrayLength)
            {
                if(row > 0)
                {
                    row -= increase;
                    index -= widthX4XIncrease;
                    indexChromaBuffer -= widthXIncrease;
                    run = true;

                    for (; run && row < rightSide; row++, index += WIDTH_X_4, indexChromaBuffer += WIDTH)
                    {
//                        rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

                        ////////////////Find HSL/////////////////
                        rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
                        if(dss[index + 2] < dss[index + 1])
                        {
                            if(dss[index + 2] < dss[index])
                            {
                                min = rt;
                                if(dss[index + 1] < dss[index]) max = bt;
                                else max = gt;
                            }
                            else { min = bt;   max = gt; }
                        }
                        else if(dss[index] < dss[index + 2])
                        {
                            max = rt;
                            if(dss[index + 1] < dss[index])
                                min = gt;
                            else min = bt;
                        }
                        else { max = bt;   min = gt; }

                        delta = max - min;
                        luminance = (max + min) / 2;
                        if(delta == 0)
                        {
                            hue = 0;
                            saturation = 0;
                        }
                        else
                        {
                            saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                            if (max == rt)      hue = 60.0f * (fmod(((gt - bt) / delta), 6.0f));
                            else if (max == gt) hue = 60.0f * (((bt - rt) / delta) + 2.0f);
                            else                hue = 60.0f * (((rt - gt) / delta) + 4.0f);
                            if (hue < 0) hue = hue + 360;
                        }

                        ////////////// End HSL //////////////

                        for(inner = 0; inner < chromaKeysArrayLength; inner++)
                        {
                            if( !(mHuesBot[inner] < hue && hue < mHuesTop[inner]
                                  && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
                                  && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                            ) )
                                run = false;
                        }
//                        if(!run)
//                            break;
                    }
//                    chromaKeyBuffer.data[indexChromaBuffer] = 255;
//                    index += WIDTH_X_4;
//                    indexChromaBuffer += WIDTH;
//                    row++;
                }

                run = true;
                for ( ; row < rightSide; row++, index += WIDTH_X_4, indexChromaBuffer += WIDTH )
                {
//                    rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

                    ////////////////Find HSL/////////////////
                    rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
                    if(dss[index + 2] < dss[index + 1])
                    {
                        if(dss[index + 2] < dss[index])
                        {
                            min = rt;
                            if(dss[index + 1] < dss[index]) max = bt;
                            else max = gt;
                        }
                        else { min = bt;   max = gt; }
                    }
                    else if(dss[index] < dss[index + 2])
                    {
                        max = rt;
                        if(dss[index + 1] < dss[index])
                            min = gt;
                        else min = bt;
                    }
                    else { max = bt;   min = gt; }

                    delta = max - min;
                    luminance = (max + min) / 2;
                    if(delta == 0)
                    {
                        hue = 0;
                        saturation = 0;
                    }
                    else
                    {
                        saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                        if (max == rt)      hue = 60.0f * (fmod(((gt - bt) / delta), 6.0f));
                        else if (max == gt) hue = 60.0f * (((bt - rt) / delta) + 2.0f);
                        else                hue = 60.0f * (((rt - gt) / delta) + 4.0f);

                        if (hue < 0) hue = hue + 360;
                    }

                    ////////////// End HSL //////////////


                    for(inner = 0; inner < chromaKeysArrayLength; inner++)
                    {
                        if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
                           && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
                           && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                                )
                            run = false;
                    }
                    if(!run)
                        break;

                    chromaKeyBuffer.data[indexChromaBuffer] = 255;

//                    mDss.data[index + 2] = 135;
//                    mDss.data[index + 1] = 175;
//                    mDss.data[index] = 65;
                }
                index += WIDTH_X_4;
                indexChromaBuffer += WIDTH;
                row++;
            }
        }
    }

    bDss.release();
}

void chromaKey::getChromaKeysSectionBufferWithBlured(Mat bDss, Mat chromaKeyBuffer, int leftSide, int rightSide)
{
    int widthX4XIncrease = WIDTH_X_4 * increase;
    int widthXIncrease = WIDTH * increase;

//  Blurred
    uchar* dss = bDss.data;

    //////////////////// Local field declaration
    float rt, gt, bt, min, max, delta;
    float luminance, saturation, hue;
    ////////////////// image manipulation

    bool run = true;

    int indexBase = leftSide * WIDTH;
    int indexBaseX4 = leftSide * WIDTH_X_4;


    for(int column = WIDTH, row, inner, index, indexChromaBuffer; column > 0; column--) // index = ((row * WIDTH) + column) * 4
    {
        index = indexBaseX4 + (column * 4);
        indexChromaBuffer = indexBase + column;

        for (row = leftSide; row < rightSide; row += increase, index += widthX4XIncrease, indexChromaBuffer += widthXIncrease )
        {
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

            ////////////////Find HSL/////////////////
            rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
            if(dss[index + 2] < dss[index + 1])
            {
                if(dss[index + 2] < dss[index])
                {
                    min = rt;
                    if(dss[index + 1] < dss[index]) max = bt;
                    else max = gt;
                }
                else { min = bt;   max = gt; }
            }
            else if(dss[index] < dss[index + 2])
            {
                max = rt;
                if(dss[index + 1] < dss[index])
                    min = gt;
                else min = bt;
            }
            else { max = bt;   min = gt; }

            delta = max - min;
            luminance = (max + min) / 2;
            if(delta == 0)
            {
                hue = 0;
                saturation = 0;
            }
            else
            {
                saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                if (max == rt)      hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );
                if (hue < 0) hue = hue + 360;
            }

            ////////////// End HSL //////////////


            for(inner = 0; inner < chromaKeysArrayLength; inner++)
            {
                if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
                   && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
                   && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                        )
                {
                    break;
                }
            }

//            if(! isInChromaKeys(hue, luminance, saturation))
            if(inner == chromaKeysArrayLength)
            {
                if(row > 0)
                {
                    row -= increase;
                    index -= widthX4XIncrease;
                    indexChromaBuffer -= widthXIncrease;
                    run = true;

                    for (; run && row < rightSide; row++, index += WIDTH_X_4, indexChromaBuffer += WIDTH)
                    {
//                        rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

                        //1246720 = index
                        ////////////////Find HSL/////////////////
                        rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
                        if(dss[index + 2] < dss[index + 1])
                        {
                            if(dss[index + 2] < dss[index])
                            {
                                min = rt;
                                if(dss[index + 1] < dss[index]) max = bt;
                                else max = gt;
                            }
                            else { min = bt;   max = gt; }
                        }
                        else if(dss[index] < dss[index + 2])
                        {
                            max = rt;
                            if(dss[index + 1] < dss[index])
                                min = gt;
                            else min = bt;
                        }
                        else { max = bt;   min = gt; }

                        delta = max - min;
                        luminance = (max + min) / 2;
                        if(delta == 0)
                        {
                            hue = 0;
                            saturation = 0;
                        }
                        else
                        {
                            saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                            if (max == rt)      hue = 60.0f * (fmod(((gt - bt) / delta), 6.0f));
                            else if (max == gt) hue = 60.0f * (((bt - rt) / delta) + 2.0f);
                            else                hue = 60.0f * (((rt - gt) / delta) + 4.0f);
                            if (hue < 0) hue = hue + 360;
                        }

                        ////////////// End HSL //////////////

                        for(inner = 0; inner < chromaKeysArrayLength; inner++)
                        {
                            if( !(mHuesBot[inner] < hue && hue < mHuesTop[inner]
                                  && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
                                  && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                            ) )
                                run = false;
                        }
//                        if(!run)
//                            break;
                    }
//                    chromaKeyBuffer.data[indexChromaBuffer] = 255;
//                    index += WIDTH_X_4;
//                    indexChromaBuffer += WIDTH;
//                    row++;
                }

                run = true;
                for ( ; row < rightSide; row++, index += WIDTH_X_4, indexChromaBuffer += WIDTH )
                {
//                    rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);

                    ////////////////Find HSL/////////////////
                    rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
                    if(dss[index + 2] < dss[index + 1])
                    {
                        if(dss[index + 2] < dss[index])
                        {
                            min = rt;
                            if(dss[index + 1] < dss[index]) max = bt;
                            else max = gt;
                        }
                        else { min = bt;   max = gt; }
                    }
                    else if(dss[index] < dss[index + 2])
                    {
                        max = rt;
                        if(dss[index + 1] < dss[index])
                            min = gt;
                        else min = bt;
                    }
                    else { max = bt;   min = gt; }

                    delta = max - min;
                    luminance = (max + min) / 2;
                    if(delta == 0)
                    {
                        hue = 0;
                        saturation = 0;
                    }
                    else
                    {
                        saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
                        if (max == rt)      hue = 60.0f * (fmod(((gt - bt) / delta), 6.0f));
                        else if (max == gt) hue = 60.0f * (((bt - rt) / delta) + 2.0f);
                        else                hue = 60.0f * (((rt - gt) / delta) + 4.0f);

                        if (hue < 0) hue = hue + 360;
                    }

                    ////////////// End HSL //////////////


                    for(inner = 0; inner < chromaKeysArrayLength; inner++)
                    {
                        if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
                           && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
                           && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                                )
                            run = false;
                    }
                    if(!run)
                        break;

                    chromaKeyBuffer.data[indexChromaBuffer] = 255;

//                    mDss.data[index + 2] = 135;
//                    mDss.data[index + 1] = 175;
//                    mDss.data[index] = 65;
                }
                index += WIDTH_X_4;
                indexChromaBuffer += WIDTH;
                row++;
            }
        }
    }

    bDss.release();
}

bool chromaKey::isInChromaKeys(int hue, float luminance, float saturation)
{
    for(int inner = 0; inner < chromaKeysArrayLength; inner++)
    {
        if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
           && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
           && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
                )
        {
            return true;
//            found = true;
//            lastHue = hue;
//            lastLuminance = luminance;
//            lastSaturation = saturation;
//            break;
        }
    }
    return false;
}

bool chromaKey::isSkinColor(int hue, float saturation, float luminance)
{
//    printf("toCheck: (%d, %f, %f)\n", hue, saturation, luminance);

    int i = 0;

    for(; i < skinArrayLength; i++)
    {
        if(hue < skinHuesTop[i] && hue > skinHuesBot[i]
            && saturation < skinSaturationTop[i] && saturation > skinSaturationBot[i]
            && luminance  < skinLuminanceTop[i]  && luminance  > skinLuminanceBot[i])
            return true;
    }

    return false;//i < skinArrayLength;



//    bool ans =  hue > skinHueValue - SKIN_HUE_THRESHOLD &&
//            hue < skinHueValue + SKIN_HUE_THRESHOLD &&
//            saturation > skinSaturationValue - SKIN_SATURATION_THRESHOLD &&
//            saturation < skinSaturationValue + SKIN_SATURATION_THRESHOLD &&
//            luminance > skinLightnessValue - SKIN_LIGHTNESS_THRESHOLD &&
//            luminance < skinLightnessValue + SKIN_LIGHTNESS_THRESHOLD;
//
////    printf("toCheck: %d\n", ans);
//
//    return ans;
//
//
    // return   !(
    //      hsv[hsvCurrentIndex] > skinHueValue + SKIN_HUE_THRESHOLD||
    //      hsv[hsvCurrentIndex] < skinHueValue - SKIN_HUE_THRESHOLD ||
    //      ((skinHueValue - SKIN_HUE_THRESHOLD < 0) &&
    //       (hsv[hsvCurrentIndex] > 180 - SKIN_HUE_THRESHOLD ) &&
    //       (hsv[hsvCurrentIndex] - 180 < skinHueValue - SKIN_HUE_THRESHOLD)
    //       )||
    //      hsv[hsvCurrentIndex + 1] > skinSaturationValue + SKIN_SATURATION_THRESHOLD ||
    //      hsv[hsvCurrentIndex + 1] < skinSaturationValue - SKIN_SATURATION_THRESHOLD ||
    //      hsv[hsvCurrentIndex + 2] > skinValueValue + SKIN_VALUE_THRESHOLD ||
    //      hsv[hsvCurrentIndex + 2] < skinValueValue - SKIN_VALUE_THRESHOLD
    //      )
    //    ;
}

bool chromaKey::isColorInThreshold(int red, float green, float blue)
{
    return red < COLOR_THRESHOLD && green < COLOR_THRESHOLD && blue < COLOR_THRESHOLD;
}

void chromaKey::rgbToHsl(int red, int green, int blue, float *hue, float *saturation, float *luminance)
{
    // find hue
//    min = (min = (red < green ? red : green) ) < blue ? min : blue;
//    max = (max = (red > green ? red : green) ) > blue ? max : blue;
//
//    if(max == min) hue = 0;
//    else if (max == red)    hue = ((green - blue) / (max - min));
//    else if (max == green)  hue = 2.0f + ((blue - red) / (max - min));
//    else                    hue = 4.0f + ((red - green) / (max - min));
//
//    hue = hue * 60.0f;
//    if (hue < 0) hue = hue + 360.0f;
//
//    ////////////// end init hue for pixel
//
//    // find luminance and saturation
//
////                luminance = (min + max ) / 2;
////                if(luminance < 125f)//if(luminance < 0.5f)
////                    saturation = (max - min)*(max + min);
////                else saturation = (max - min)*(510 - max - min);//  2 * 255 //saturation = (max - min)*(2 - max - min);
//
//    luminance = (red + green + blue)/3.0f;
//
//    if(min == max) // c = 0
//        saturation = 0;
//    else if(luminance != 0)
//        saturation = 1 - (min / luminance);



/////////////////////////////////////////////////////////


    float rt = red / 255.0f, gt = green / 255.0, bt = blue / 255.0f;
    float min, max;
    if(red < green)
    {

        if(red < blue)
        {
            min = rt;
            if(green < blue) max = bt;
            else max = gt;
        }
        else
        {
            min = bt;
            max = gt;
        }
    }
    else if(blue < red)
    {
        max = rt;
        if(green < blue) min = gt;
        else min = bt;
    }
    else
    {
        max = bt;   min = gt;
    }
//                min = (min = red < green ? red : green) < blue ? min : blue;
//                max = (max = red > green ? red : green) > blue ? max : blue;

    // find hue
    float delta = max - min;


    if(delta == 0)
        *hue = 0;
    else if (max == rt)
    {
        *hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
    }
    else if (max == gt) *hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
    else                *hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );

    *luminance = (max + min) / 2;

    if(delta == 0)
        *saturation = 0;
    else
        *saturation =  delta / (1 - abs((int) ( (*luminance * 2) - 1)) );
    if (hue < 0) hue = hue + 360;
    ////////////// end init hue for pixel

//                luminance = (min + max ) / 2;
//                if(luminance < 125f)//if(luminance < 0.5f)
//                    saturation = (max - min)*(max + min);
//                else saturation = (max - min)*(510 - max - min);//  2 * 255 //saturation = (max - min)*(2 - max - min);

//    *luminance = (red + green + blue)/3;
//
//    if(min == max) // c = 0
//        *saturation = 0.0f;
//    else if(*luminance != 0)
//        *saturation = 1.0f - (min / *luminance);
}

void chromaKey::rgbToXYZ(float redT, float greenT, float blueT, float *X, float *Y, float *Z)
{
    float Rt = redT, Gt = greenT, Bt = blueT;

    // Observer = 2°, Illuminant = D65
    if (Rt > 0.04045) Rt = (pow((Rt + 0.055) / 1.055, 2.4)) * 100;
    else Rt = (Rt / 12.92) * 100;
    if (Gt > 0.04045) Gt = (pow((Gt + 0.055) / 1.055, 2.4)) * 100;
    else Gt = (Gt / 12.92) * 100;
    if (Bt > 0.04045) Bt = (pow((Bt + 0.055) / 1.055, 2.4)) * 100;
    else Bt = (Bt / 12.92) * 100;

    *X = Rt * 0.4124 + Gt * 0.3576 + Bt * 0.1805;
    *Y = Rt * 0.2126 + Gt * 0.7152 + Bt * 0.0722;
    *Z = Rt * 0.0193 + Gt * 0.1192 + Bt * 0.9505;
}

void chromaKey::XYZToRgb(float X, float Y, float Z, float *R, float *G, float *B)
{
    float Xt = X / 100, Yt = Y / 100, Zt = Z / 100;

    float Rt = Xt *  3.2406 + Yt * -1.5372 + Zt * -0.4986
        , Gt = Xt * -0.9689 + Yt *  1.8758 + Zt *  0.0415
        , Bt = Xt *  0.0557 + Yt * -0.2040 + Zt *  1.0570;

    if (Rt > 0.0031308) Rt = (1.055 * pow(Rt, 1 / 2.4) ) - 0.055;
    else Rt = 12.92 * Rt;
    if (Gt > 0.0031308) Gt = (1.055 * pow(Gt, 1 / 2.4) ) - 0.055;
    else Gt = 12.92 * Gt;
    if (Bt > 0.0031308) Bt = (1.055 * pow(Bt, 1 / 2.4) ) - 0.055;
    else Bt = 12.92 * Bt;

    *R = Rt * 255;
    *G = Gt * 255;
    *B = Bt * 255;
}

void chromaKey::XYZToLab(float X, float Y, float Z, float *L, float *a, float *b)
{
    // Observer= 2°, Illuminant= D65
    float Xt = X / refX, Yt = Y / refY, Zt = Z / refZ;

    if(Xt > e) Xt = cbrt(Xt);
    else Xt = (7.787 * Xt) + (16 / 116);
    if(Yt > e) Yt = cbrt(Yt);
    else Yt = ( 7.787 * Yt) + (16 / 116);
    if(Zt > e) Zt = cbrt(Zt);
    else Zt = (7.787 * Zt) + (16 / 116);

    *L = (116 * Yt) - 16;
    *a = 500 * (Xt - Yt);
    *b = 200 * (Yt - Zt);
}

void chromaKey::LabToXYZ(float L, float a, float b, float *X, float *Y, float *Z)
{
    float Yt = (L + 16 ) / 116, Xt = (a / 500) + Yt, Zt = Yt - (b / 200);

    if (pow(Yt, 3) > 0.008856) Yt = pow(Yt, 3);
    else Yt = (Yt - (16 / 116)) / 7.787;
    if (pow(Xt, 3) > 0.008856) Xt = pow(Xt, 3);
    else Xt = (Xt - (16 / 116)) / 7.787;
    if (pow(Zt, 3) > 0.008856) Zt = pow(Zt, 3);
    else Zt = (Zt - (16 / 116)) / 7.787;

    *X = refX * Xt;
    *Y = refY * Yt;
    *Z = refZ * Zt;
}

bool chromaKey::isInFace(int c, int r, int faceLeft, int faceTop, int faceRight, int faceBottom)
{
//    if (r >= faceLeft && r <= faceRight && c >= faceTop && c <= faceBottom)
//    {
//        return true;
//    }

    if (c >= faceLeft && c <= faceRight && r >= faceTop && r <= faceBottom)
    {
        return true;
    }
//
//    if (c == faceRight && r >= faceTop && r <= faceBottom)
//    {
//        return true;
//    }
//
//    if (r == faceTop && c >= faceLeft && c <= faceRight)
//    {
//        return true;
//    }
//
//    if (c == faceLeft && r >= faceTop && r <= faceBottom)
//    {
//        return true;
//    }
//
//    if (r == faceBottom && c >= faceLeft && c <= faceRight)
//    {
//        return true;
//    }


    return false;
}

bool chromaKey::saveSkinColorHsl(uchar* dss, unsigned int faceLeft, unsigned int faceTop, unsigned int faceWidth, unsigned int faceHeight)
{
    int bottomPart = faceTop - (faceHeight * 0.22f);  //column
    if(bottomPart > WIDTH)
        bottomPart = WIDTH;
    else if(bottomPart < 0)
        return false;

    int upperPart  = faceTop - (faceHeight * 0.08f);  //column
    if(upperPart < 0)
        upperPart = 0;
    else if(upperPart > WIDTH)
        return false;

    int leftToScan = faceLeft + (0.25f * faceHeight);
    if(leftToScan > HEIGHT)
        return false;
    else if(leftToScan < 0)
        leftToScan = 0;
//    index = (leftToScan * WIDTH * 4)+(c * 4);

    int rightToScan = faceLeft + (faceHeight * 0.8f);
    if(rightToScan > HEIGHT)
        rightToScan = HEIGHT;
    else if(rightToScan < 0)
        return false;

    int baseIndex = leftToScan * WIDTH_X_4;

    if(skinHuesTop != NULL)
    {
        free(skinHuesTop);          free(skinHuesBot);
        free(skinSaturationTop);    free(skinSaturationBot);
        free(skinLuminanceTop);     free(skinLuminanceBot);
    }
    skinHuesTop = (float *) malloc(skinHslArrayLength * sizeof(float));
    skinHuesBot = (float *) malloc(skinHslArrayLength * sizeof(float));
    skinSaturationTop = (float *) malloc(skinHslArrayLength * sizeof(float));
    skinSaturationBot = (float *) malloc(skinHslArrayLength * sizeof(float));
    skinLuminanceTop = (float *) malloc(skinHslArrayLength * sizeof(float));
    skinLuminanceBot = (float *) malloc(skinHslArrayLength * sizeof(float));

    int* counter = (int *) malloc(skinHslArrayLength * sizeof(int));

    skinArrayLength = 0;

    int size = 0;
    float skin_hue, skin_sat, skin_lightness;
    bool foundRange = false;
    int i;

    int red, green, blue;
    float hueVal, saturationVal, luminanceVal;

    int minRangeCount = 0.2f * faceHeight;



    // for head
    for(int c = upperPart, r, index; c > bottomPart; c--)
    {
        index = baseIndex + (c * 4);
        for (r = leftToScan; r < rightToScan; r++, index += WIDTH_X_4)
        {
            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hueVal,
                     &saturationVal, &luminanceVal);

            foundRange = false;

            for (i = 0; i < size; i++)
            {
                if (hueVal > skinHuesBot[i] && hueVal < skinHuesTop[i]
                    && saturationVal > skinSaturationBot[i] && saturationVal < skinSaturationTop[i]
                    && luminanceVal > skinLuminanceBot[i] && luminanceVal < skinLuminanceTop[i])
                {
                    break;
                }
                counter[i]++;
            }

            if (size + 1 >= skinHslArrayLength) // +2 is for this run and for the next run
            {
                break;
            }

            if (i == size)
            {
                size++;

                skinHuesTop[i] = hueVal + SKIN_HUE_THRESHOLD;
                skinHuesBot[i] = hueVal - SKIN_HUE_THRESHOLD;

                skinSaturationTop[i] = saturationVal + SKIN_SATURATION_THRESHOLD;
                skinSaturationBot[i] = saturationVal - SKIN_SATURATION_THRESHOLD;

                skinLuminanceTop[i] = luminanceVal + SKIN_LUMINANCE_THRESHOLD;
                skinLuminanceBot[i] = luminanceVal - SKIN_LUMINANCE_THRESHOLD;

                counter[i] = 1;
            }
            if (drawDetectionGuilds)
            {
                dss[index] = 255;   //b
                dss[index + 1] = 0; //g
                dss[index + 2] = 0;
            }
        }
    }

    upperPart  = bottomPart;  //column
    bottomPart = 1;  //column

    leftToScan = faceLeft + (0.45f * faceHeight);

    rightToScan = faceLeft + (0.55f * faceHeight);

    baseIndex = leftToScan * WIDTH_X_4;

    for(int c = upperPart, r, index; c > bottomPart; c--)
    {
        index = baseIndex + (c * 4);
        for (r = leftToScan; r < rightToScan; r++, index += WIDTH_X_4)
        {
            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hueVal, &saturationVal, &luminanceVal);

            foundRange = false;

            for (i = 0; i < size; i++)
            {
                if (hueVal > skinHuesBot[i] && hueVal < skinHuesTop[i]
                    && saturationVal > skinSaturationBot[i] && saturationVal < skinSaturationTop[i]
                    && luminanceVal > skinLuminanceBot[i] && luminanceVal < skinLuminanceTop[i])
                {
                    break;
                }
                counter[i]++;
            }

            if (size + 1 >= skinHslArrayLength) // +2 is for this run and for the next run
            {
                break;
            }

            if (i == size)
            {
                size++;

                skinHuesTop[i] = hueVal + SKIN_HUE_THRESHOLD;
                skinHuesBot[i] = hueVal - SKIN_HUE_THRESHOLD;

                skinSaturationTop[i] = saturationVal + SKIN_SATURATION_THRESHOLD;
                skinSaturationBot[i] = saturationVal - SKIN_SATURATION_THRESHOLD;

                skinLuminanceTop[i] = luminanceVal + SKIN_LUMINANCE_THRESHOLD;
                skinLuminanceBot[i] = luminanceVal - SKIN_LUMINANCE_THRESHOLD;

                counter[i] = 1;
            }
            if (drawDetectionGuilds)
            {
                dss[index] = 255;   //b
                dss[index + 1] = 0; //g
                dss[index + 2] = 0;
            }
        }
    }

//    __android_log_print(ANDROID_LOG_ERROR, "tag43", "checkAndDeductChromaKeysAgainstSkinColor>chromaKeysArrayLength after>>>>>>>>>>>>>>>>>>>>  %d  ", size);


    int topIndex = 0, topCount = 0;
    for(i = 0; i < size; i++)
    {
        if (counter[i] >= minRangeCount && topCount < counter[i])
        {
            topCount = counter[i];
            topIndex = i;
        }
    }

    free(counter);
    if(topIndex == -1)
    {
        skinArrayLength = 0;
        return false;
    }
    else
    {
        skinArrayLength = size;

        skinHueValue        = skinHuesTop[topIndex]       - SKIN_HUE_THRESHOLD;
        skinSaturationValue = skinSaturationTop[topIndex] - SKIN_SATURATION_THRESHOLD;
        skinLightnessValue  = skinLuminanceTop[topIndex]  - SKIN_LUMINANCE_THRESHOLD;
        foundSkinColor      = true;
        return true;
    }

}

bool chromaKey::addSkinColorHsl(uchar *dss, unsigned int faceRight, unsigned int faceTop, unsigned int faceWidth, unsigned int faceHeight)
{
    return false;
    
}

void chromaKey::getShapeBrightness(uchar* dss, unsigned int channels, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom)
{
    int padding = (int) (((float) (faceBottom - faceTop)) * 0.3f);
    int paddingHalf = (int) (((float) (faceBottom - faceTop)) * 0.2f);
    int paddingWidth = (int) (((float) (faceRight - faceLeft)) * 0.4f);

    int bottomTopArea = faceBottom + padding;

    if(bottomTopArea > WIDTH - 1)
        bottomTopArea = WIDTH - 1;

    int topBottomArea = faceTop + paddingHalf;
    int rightLeftArea = faceLeft - paddingWidth;
    int leftRightArea = faceRight + paddingWidth;

    if(bottomTopArea > WIDTH)
        bottomTopArea = WIDTH;
    else if(topBottomArea < 1)
        topBottomArea = 1;

//    float blue, green, red, min, max, saturation = 0, luminance = 0, hue = 0;
    float averageLuminance = .0f;
    int pixelsCount = 0;


    if (faceLeft != 0)
    {
        for (int c = WIDTH - 2, indexSrc, incSrc = WIDTH * channels; c >= bottomTopArea; c--)
        {
            indexSrc = c * channels + incSrc + incSrc;
            for (int r = 2; r <= HEIGHT; r++, indexSrc += incSrc)
            {
                averageLuminance += ( ( (dss[indexSrc + 2] & 0xFF) + (dss[indexSrc + 1] & 0xFF) + (dss[indexSrc] & 0xFF) ) / 3.0f );
                pixelsCount++;
            }
        }

        for (int c = bottomTopArea, indexSrc, incSrc = WIDTH * channels; c >= topBottomArea; c--)
        {
            indexSrc = c * channels + incSrc + incSrc;
            for (int r = 2; r <= faceLeft; r++, indexSrc += incSrc)
            {
                averageLuminance += ( ( (dss[indexSrc + 2] & 0xFF) + (dss[indexSrc + 1] & 0xFF) + (dss[indexSrc] & 0xFF) ) / 3.0f );
                pixelsCount++;
            }
        }

        for(int c = bottomTopArea, indexSrc, incSrc = WIDTH * channels; c >= topBottomArea; c--)
        {
            indexSrc = (faceRight * WIDTH * channels) + c * channels;
            for (int r = faceRight; r <= HEIGHT; r++, indexSrc += incSrc)
            {
                averageLuminance += ( ( (dss[indexSrc + 2] & 0xFF) + (dss[indexSrc + 1] & 0xFF) + (dss[indexSrc] & 0xFF) ) / 3.0f );
                pixelsCount++;
            }
        }
        averageLuminance /= ((float)pixelsCount);
        BRIGHTNESS = optimumLuminance4Chroma - averageLuminance;
    }
    else BRIGHTNESS = 0;
}

void chromaKey::getShapeContrast(uchar* dss, unsigned int channels, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom)
{
    int padding = (int) (((float) (faceBottom - faceTop)) * 0.3f);
    int paddingHalf = (int) (((float) (faceBottom - faceTop)) * 0.2f);
    int paddingWidth = (int) (((float) (faceRight - faceLeft)) * 0.4f);

    int bottomTopArea = faceBottom + padding;

    if(bottomTopArea > WIDTH - 1)
        bottomTopArea = WIDTH - 1;

    int topBottomArea = faceTop + paddingHalf;
    int rightLeftArea = faceLeft - paddingWidth;
    int leftRightArea = faceRight + paddingWidth;

    if(bottomTopArea > WIDTH)
        bottomTopArea = WIDTH;
    else if(topBottomArea < 1)
        topBottomArea = 1;

//    float blue, green, red, min, max, saturation = 0, luminance = 0, hue = 0;
    float averageContrast = .0f;
    int pixelsCount = 0;


    if (faceLeft != 0)
    {
        for (int c = WIDTH - 2, indexSrc, incSrc = WIDTH * channels; c >= bottomTopArea; c--)
        {
            indexSrc = c * channels + incSrc + incSrc;
            for (int r = 2; r <= HEIGHT; r++, indexSrc += incSrc)
            {
                averageContrast += ( ( ((dss[indexSrc + 2] & 0xFF) * 299) + ( (dss[indexSrc + 1] & 0xFF) * 587) + ( (dss[indexSrc] & 0xFF) * 114) ) / 1000.0f );
                pixelsCount++;
            }
        }

        for (int c = bottomTopArea, indexSrc, incSrc = WIDTH * channels; c >= topBottomArea; c--)
        {
            indexSrc = c * channels + incSrc + incSrc;
            for (int r = 2; r <= faceLeft; r++, indexSrc += incSrc)
            {
                averageContrast += ( ( ((dss[indexSrc + 2] & 0xFF) * 299) + ( (dss[indexSrc + 1] & 0xFF) * 587) + ( (dss[indexSrc] & 0xFF) * 114) ) / 1000.0f );
                pixelsCount++;
            }
        }

        for(int c = bottomTopArea, indexSrc, incSrc = WIDTH * channels; c >= topBottomArea; c--)
        {
            indexSrc = (faceRight * WIDTH * channels) + c * channels;
            for (int r = faceRight; r <= HEIGHT; r++, indexSrc += incSrc)
            {
                averageContrast += ( ( ((dss[indexSrc + 2] & 0xFF) * 299) + ( (dss[indexSrc + 1] & 0xFF) * 587) + ( (dss[indexSrc] & 0xFF) * 114) ) / 1000.0f );
                pixelsCount++;
            }
        }
        averageContrast /= ((float)pixelsCount);
        CONTRAST = ((float)optimumContrast4Chroma) / averageContrast;
    }
    else CONTRAST = 1.0f;
}

void chromaKey::setBrightness(uchar* dss, uchar* dsd)
{
    for(int column = WIDTH, row, inner, index; column>0; column--)
    {
        index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
        for (row = HEIGHT - 1; row > 0; row -= increase, index -= (WIDTH_X_4) * increase)
        {
            float i = (float)dss[index];
            i *= BRIGHTNESS;
            if(i > 255)
                i = 255;
            dsd[index] = (int)i;

            i = (float)dss[index + 1];
            i *= BRIGHTNESS;
            if(i > 255)
                i = 255;
            dsd[index + 1] = (int)i;

            i = (float)dss[index + 2];
            i *= BRIGHTNESS;
            if(i > 255)
                i = 255;
            dsd[index + 2] = (int)i;
        }
    }
}

void chromaKey::setBrightnessAndContrast(uchar *dss, uchar *dsd)
{
    float i;
    for(int column = WIDTH, row, inner, index; column > 0; column--)
    {
        index = (HEIGHT * WIDTH_X_4) - 4 - (column * 4);
        for (row = HEIGHT - 1; row > 0; row -= increase, index -= (WIDTH_X_4) * increase)
        {
            i = (CONTRAST * (float)dss[index]) + BRIGHTNESS;
            if(i > 255)
                i = 255;
            dsd[index] = (int)i;

            i = (CONTRAST * (float)dss[index]) + BRIGHTNESS;
            if(i > 255)
                i = 255;
            dsd[index + 1] = (int)i;

            i = (CONTRAST * (float)dss[index]) + BRIGHTNESS;
            if(i > 255)
                i = 255;
            dsd[index + 2] = (int)i;
        }
    }
}

void chromaKey::resetChromaKeys()
{
    chromaKeysArrayLength = 0;
}

bool chromaKey::isKeysEmpty()
{
    return chromaKeysArrayLength == 0;
}

void* getChromaKeysSectionBufferThreadRunner(void *data)
{
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>>> %d   %d", 5, 25);
    chromaKeyBufferStructure* bufferStructure = ((chromaKeyBufferStructure*)data);

//    cUtil.getChromaKeysSectionBuffer((*bufferStructure).dss, (*bufferStructure).dst, (*bufferStructure).leftSide, (*bufferStructure).rightSide);
    cUtil.getChromaKeysSectionBufferWithBlured((*bufferStructure).dss, (*bufferStructure).dst, (*bufferStructure).leftSide, (*bufferStructure).rightSide);

    pthread_exit(0);
}

pthread_t chromaKey::getChromaKeysSectionBufferThread(Mat& dss, Mat& dst, int leftSide, int rightSide)
{
//    chromaKeyBufferStructure bufferStructure = {dss, dst, leftSide, rightSide};
    pthread_t chromaKeyThread;



//    pthread_create(&chromaKeyThread, NULL, getChromaKeysSectionBufferThreadRunner, (void*) &bufferStructure);
//    pthread_join(chromaKeyThread, 0);
    

    return chromaKeyThread;
}

void chromaKey::getChromaKeysSectionBufferThreaded(Mat &dss, Mat &dst)
{
    Mat bDss;
    blur(dss, bDss, Kernel_5X5);

    chromaKeyBufferStructure bufferStructure = {bDss, dst, 0, dss.rows / 4};
    static pthread_t chromaKeyThread;
    pthread_create(&chromaKeyThread, NULL, getChromaKeysSectionBufferThreadRunner, (void*) &bufferStructure);

    chromaKeyBufferStructure bufferStructure1 = {bDss, dst, dss.rows / 4, dss.rows / 2};
    static pthread_t chromaKeyThread1;
    pthread_create(&chromaKeyThread1, NULL, getChromaKeysSectionBufferThreadRunner, (void*) &bufferStructure1);

    chromaKeyBufferStructure bufferStructure2 = {bDss, dst, dss.rows / 2, dss.rows * 3 / 4};
    static pthread_t chromaKeyThread2;
    pthread_create(&chromaKeyThread2, NULL, getChromaKeysSectionBufferThreadRunner, (void*) &bufferStructure2);

    chromaKeyBufferStructure bufferStructure3 = {bDss, dst, dss.rows * 3 / 4, dss.rows};
    static pthread_t chromaKeyThread3;
    pthread_create(&chromaKeyThread3, NULL, getChromaKeysSectionBufferThreadRunner, (void*) &bufferStructure3);

//    chromaKeyBufferStructure bufferStructure = {bDss, dst, 0, dss.rows / 2};
//    static pthread_t chromaKeyThread;
//    pthread_create(&chromaKeyThread, NULL, getChromaKeysSectionBufferThreadRunner, (void*) &bufferStructure);
//
//    chromaKeyBufferStructure bufferStructure1 = {bDss, dst, dss.rows / 2, dss.rows};
//    static pthread_t chromaKeyThread1;
//    pthread_create(&chromaKeyThread1, NULL, getChromaKeysSectionBufferThreadRunner, (void*) &bufferStructure1);


    pthread_join(chromaKeyThread, 0);
    pthread_join(chromaKeyThread1, 0);
    pthread_join(chromaKeyThread2, 0);
    pthread_join(chromaKeyThread3, 0);
}

int chromaKey::getArrayLength()
{
    return chromaKeysArrayLength;
}

void chromaKey::skinColorClustering(Mat faceImage)
{
    Mat src = faceImage;
    blur(faceImage, faceImage, Kernel_5X5);
    Mat samples(src.rows * src.cols, 3, CV_32F);
    for( int y = 0; y < src.rows; y++ )
        for( int x = 0; x < src.cols; x++ )
            for( int z = 0; z < 3; z++)
                samples.at<float>(y + x*src.rows, z) = src.at<Vec3b>(y,x)[z];
    
    Mat labels;
    int attempts = 5;
    Mat centers;
    if (samples.rows < clusterCount){
        //        return faceImage;
    }
    kmeans(samples, clusterCount, labels, TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 100, 0.01), attempts, KMEANS_PP_CENTERS, centers );
    
    Mat new_image( src.size(), src.type() );
    for( int y = 0; y < src.rows; y++ )
        for( int x = 0; x < src.cols; x++ )
        {
            int cluster_idx = labels.at<int>(y + x*src.rows,0);
            new_image.at<Vec3b>(y,x)[0] = centers.at<float>(cluster_idx, 0);
            new_image.at<Vec3b>(y,x)[1] = centers.at<float>(cluster_idx, 1);
            new_image.at<Vec3b>(y,x)[2] = centers.at<float>(cluster_idx, 2);
        }
    
    for (int i = 0; i < (clusterCount); i++)
    {
        int j = i*3;
        skinColors[j] = centers.at<float>(i, 0);
        skinColors[j + 1] = centers.at<float>(i, 1);
        skinColors[j + 2] = centers.at<float>(i, 2);
        
    }
    
    //    return new_image;
}

void chromaKey::saveSkinColorHsl()
{
    float  saturation = 0, luminance = 0, hue = 0;
    
    for (int i = 0; i < (clusterCount); i++)
    {
        int j = i*3;
        rgbToHsl(skinColors[j + 2], skinColors[j + 1], skinColors[j], &hue, &saturation, &luminance);
        skinColorsHsl[j] = hue;
        skinColorsHsl[j + 1] = saturation;
        skinColorsHsl[j + 2] = luminance;
        
    }
}

void chromaKey::drawSkinColor(Mat bg)
{
    int WIDTH = bg.cols, HEIGHT = bg.rows;
    int RED_THRESHOLD = 10;
    int GREEN_THRESHOLD = 10;
    int BLUE_THRESHOLD = 10;
    
    float  saturation = 0, luminance = 0, hue = 0;
    
    int channels = bg.channels();
    for (int r = 0, index, index2; r < HEIGHT; r++)
    {
        index = WIDTH * r * channels;
        index2 = WIDTH * r;
        for (int c = 0; c < WIDTH; c++, index += channels, index2++)
        {
            
            rgbToHsl(bg.data[index], bg.data[index + 1], bg.data[index + 2], &hue, &saturation, &luminance);
            
            for(int i = 0; i < clusterCount; i++)
            {
                int j = i*3;
                int k, t;
                if (i == 0){
                    k = 9;
                    t = 0;
                } else {
                    t = i*10;
                    k = t + 9;
                }
                
                if (hue > skinColorsHsl[j] - SKIN_FILTER_HUE_THRESHOLD
                    && hue < skinColorsHsl[j] + SKIN_FILTER_HUE_THRESHOLD
                    && saturation > skinColorsHsl[j + 1] - SKIN_FILTER_SATURATION_THRESHOLD
                    && saturation < skinColorsHsl[j + 1] + SKIN_FILTER_SATURATION_THRESHOLD
                    && luminance > skinColorsHsl[j + 2] - SKIN_FILTER_LUMINANCE_THRESHOLD
                    && luminance < skinColorsHsl[j + 2] + SKIN_FILTER_LUMINANCE_THRESHOLD)
                {
                    bg.data[index] = 255;
                    bg.data[index + 1] = 100;
                    bg.data[index + 2] = 255;
                    continue;
                    
                }
                
                
                
                //                    if (bg.data[index] > skinColors[j + 2] - RED_THRESHOLD
                //                        && bg.data[index] < skinColors[j + 2] + RED_THRESHOLD
                //                        && bg.data[index + 1] > skinColors[j + 1] - GREEN_THRESHOLD
                //                        && bg.data[index + 1] < skinColors[j + 1] + GREEN_THRESHOLD
                //                        && bg.data[index + 2] > skinColors[j ] - BLUE_THRESHOLD
                //                        && bg.data[index + 2] < skinColors[j ] + BLUE_THRESHOLD)
                //                    {
                //                        bg.data[index] = 255;
                //                        bg.data[index + 1] = 100;
                //                        bg.data[index + 2] = 255;
                //                        continue;
                //
                //                    }
                
                if (c >= t && c < k){
                    bg.data[index] = skinColors[j + 2];
                    bg.data[index + 1] = skinColors[j + 1];
                    bg.data[index + 2] = skinColors[j];
                }
                
            }
            
        }
    }
}


DetectionResponse chromaKey::checkFaceLightness(Mat image,int faceLeft, int faceTop, int faceRight, int faceBottom)
{
   // uchar * image1 = image.data;
    
    int shift = 0;
    int bottom = faceBottom - shift ;
    int top = faceTop + shift;
    int right = faceRight - shift;
    int left = faceLeft + shift;
    
    if (bottom < 0 || top < 0 || right < 0 || left < 0 || (bottom - top) < 0 || (right - left) < 0 ){
        bottom = faceTop;
        top = faceBottom;
        right = faceRight;
        left = faceLeft;
    }
    
    Mat src = image;
    Mat dst(cv::Size(bottom - top, right - left), src.type());
    
    int incSrc = (src.cols * 1), incDst = (dst.cols * 1);
    int index = (src.cols * src.channels());
    int baseIndex = left * incSrc;
    int rgbIndex = left * index;
    
    uchar* dstData = dst.data;
    uchar* srcData = src.data;
    
    bool skinFlag = false;
    bool chromaKeyFlag = false;
    
    int c_middle =  top + (bottom - top)/2;
    int r_middle = left + (right - left)/2;
    
    float x,y,z, l,a,b, l_max = 0, l_min = 100, l_avg = 0;
    float sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
    int count = 0, sum = 0;
    int count1 = 0, count2 = 0, count3 = 0, count4 = 0;
    float avg1 = 0, avg2 = 0, avg3 = 0, avg4 = 0;
    
    
    for(int c = src.cols - top, cDst = dst.cols, indexSrc, indexRgb, indexDst, index, channels = src.channels(), bottomLimit = src.cols - bottom; c >= bottomLimit; c--, cDst--)// && cDst > 0
    {
        //todo adjust with row * WIDTH * 4 to indexSrc indexDst
        
        indexSrc = baseIndex + (c * 1);
        indexRgb = rgbIndex + (c * channels);
        indexDst = (cDst * channels);
        index = c * 4;
        
        for (int r = left, rDst = 0; r < right; r++, rDst++,indexRgb += index ,indexSrc += incSrc, indexDst += incDst)//, indexMask += widthMask) && rDst < dst.rows
        {
            index = indexSrc*4;
            
            cUtil.rgbToXYZ(image.data[index + 2], image.data[index + 1], image.data[index], &x, &y, &z);
            cUtil.XYZToLab(x, y, z, &l, &a, &b);
            
            l = l/100;
            
            
            if (r > left and r  < r_middle and c < bottom and c > c_middle){
//                timage.data[index] = 255;  //blue
//                timage.data[index + 1] = 0;
//                timage.data[index + 2] = 0;
                
                sum1 += l;
                count1 ++;
                
            }
            
            if (r > left and r  < r_middle and c > top and c < c_middle ){
//                timage.data[index] = 0; //green
//                timage.data[index + 1] = 255;
//                timage.data[index + 2] = 0;
                
                sum2 += l;
                count2 ++;
                
            }
            
            if (r < right and r  > r_middle and c < bottom and c > c_middle){
//                timage.data[index] = 255;  //blue2
//                timage.data[index + 1] = 0;
//                timage.data[index + 2] = 0;
                
                sum3 += l;
                count3 ++;
                
            }
            
            if (r < right and r  > r_middle and c > top and c < c_middle){
//                timage.data[index] = 0;  //red
//                timage.data[index + 1] = 0;
//                timage.data[index + 2] = 255;
                
                sum4 += l;
                count4 ++;
                
            }
            
            
        }
        
    }
    
    float avg_array[4];
    
        avg1 = sum1/count1;
    avg_array[0] = avg1;
    avg2 = sum2/count2;
    avg_array[1] = avg2;
    avg3 = sum3/count3;
    avg_array[2] = avg3;
    avg4 = sum4/count4;
    avg_array[3] = avg4;
    
    printf("bad...avg1 = %f\n" , avg1);
    printf("bad...avg2 = %f\n" , avg2);
    printf("bad...avg3 = %f\n" , avg3);
    printf("bad...avg4 = %f\n" , avg4);
    
    for (int i = 0; i < 4; i++){
        count = 0;
        for (int j = 0; j < 4; j++){
            if (i != j){
                if (avg_array[i] > 1.2*avg_array[j]){
                    count ++;
                }
            }
        }
        if (count >=2){
            printf("bad...return false\n");
            faceLightness = false;
            return DetectionResponseLowLight;
        }
    }
    printf("bad...return true\n");
    faceLightness = true;
    return DetectionResponseSuccess;
    
}





//    int partSize = dss.rows / NUMBER_OF_CHROMA_KEYS_THREADS;
//    int leftOverSize = dss.rows % NUMBER_OF_CHROMA_KEYS_THREADS;
//    pthread_t* threads = (pthread_t*)malloc(NUMBER_OF_CHROMA_KEYS_THREADS * sizeof(pthread_t));
//
////    chromaKeyBufferStructure bufferStructures[NUMBER_OF_CHROMA_KEYS_THREADS]();// = (chromaKeyBufferStructure*)malloc(NUMBER_OF_CHROMA_KEYS_THREADS * sizeof(chromaKeyBufferStructure));
//
////    chromaKeyBufferStructure* bufferStructures = (chromaKeyBufferStructure*)malloc(NUMBER_OF_CHROMA_KEYS_THREADS * sizeof(chromaKeyBufferStructure));
//    chromaKeyBufferStructure** bufferStructures = new chromaKeyBufferStructure *[NUMBER_OF_CHROMA_KEYS_THREADS];//(chromaKeyBufferStructure*)malloc(NUMBER_OF_CHROMA_KEYS_THREADS * sizeof(chromaKeyBufferStructure));
////    std::vector<chromaKeyBufferStructure> bufferStructures();
//
//    for(int i = 0; i < NUMBER_OF_CHROMA_KEYS_THREADS; i++)
//    {
//        int rightSide = ((i + 1) * partSize) + (i == NUMBER_OF_CHROMA_KEYS_THREADS - 1 ? leftOverSize : 0);
////        chromaKeyBufferStructure bufferStructure = {dss, dst, i * partSize, rightSide};
////        bufferStructures[i].dss  = {dss, dst, i * partSize, rightSide};
//
//        bufferStructures[i] = new chromaKeyBufferStructure();
//        //{dss, dst, i * partSize, rightSide};
////        (bufferStructures + i)->dss = dss;
////        (bufferStructures + i)->dst = dst;
////        (bufferStructures + i)->leftSide = i * partSize;
////        (bufferStructures + i)->rightSide = rightSide;
////        , dst, i * partSize, rightSide};
//
////        pthread_create(&threads[i], NULL, getChromaKeysSectionBufferThreadRunner, (void*) &(bufferStructure) );
////
////        pthread_create(&threads[i], NULL, getChromaKeysSectionBufferThreadRunner, (void*) &(chromaKeyBufferStructure{dss, dst, i * partSize, rightSide}) );
//
////        bufferStructures.push_back(bufferStructure);
////        pthread_create(&threads[i], NULL, getChromaKeysSectionBufferThreadRunner, (void*) &(*(bufferStructures + i)) );
//    }

//    for(int i = 0; i < NUMBER_OF_CHROMA_KEYS_THREADS; i++)
//    {
//        pthread_join(threads[i], 0);
//    }


//bool chromaKey::findChromaKeys(Mat mDss, int faceLeft, int faceTop, int faceRight, int faceBottom)
//{
//    int WIDTH = mDss.cols, HEIGHT = mDss.rows, WIDTH_X_4 = WIDTH * 4;
//
//    int padding = (int) (((float) (faceBottom - faceTop)) * PADDING_MULTIPLIER);
//    int paddingHalf = (int) (((float) (faceBottom - faceTop)) * PADDING_MULTIPLIER_HALF);
//    int paddingWidth = (int) (((float) (faceRight - faceLeft)) * PADDING_MULTIPLIER_HALF);
//
//    int bottomTopArea = faceBottom + padding;
//
//    if(bottomTopArea > WIDTH - 1)
//        bottomTopArea = WIDTH - 1;
//
//    int bottomBottomArea = faceTop + paddingHalf;
//    int rightLeftArea = faceLeft - paddingWidth;
//    int leftRightArea = faceRight + paddingWidth;
//
//    if(bottomBottomArea < 1)
//        bottomBottomArea = 1;
//
//    blur(mDss, mDss, Kernel_5X5);
//    uchar* dss = mDss.data;
//    ////////////////// Init chroma key
//
//    float rt, gt, bt, min, max, delta;
//    float saturation = 0, luminance = 0, hue = 0;
//
//    if (mHuesBot != NULL)
//    {
//        free(mHuesBot);
//        free(mSaturationBot);
//        free(mSaturationTop);
//        free(mHuesTop);
//        free(mLuminanceBot);
//        free(mLuminanceTop);
//    }
//
//    mHuesBot = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));
//    mHuesTop = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));
//
//    mSaturationBot = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));
//    mSaturationTop = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));
//
//    mLuminanceBot = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));
//    mLuminanceTop = (float *) malloc(CHROMA_KEY_ARRAY_LENGTH * sizeof(float));
//
//    int inner;
//    chromaKeysArrayLength = 0;
//
//    ///////////////////////////DETECTION////////////////////////////////////
//
//    if (faceLeft != 0)
//    {
//        for (int c = WIDTH - 2, index, channels = mDss.channels(), incSrc = WIDTH * channels, incSrc2 = incSrc * 2; c >= bottomTopArea; c--)
//        {
//            index = c * channels + incSrc2;
//            for (int r = 2; r <= HEIGHT; r++, index += incSrc)
//            {
////                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);
//
//                ////////////////Find HSL/////////////////
//                rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
//                if(dss[index + 2] < dss[index + 1])
//                {
//                    if(dss[index + 2] < dss[index])
//                    {
//                        min = rt;
//                        if(dss[index + 1] < dss[index]) max = bt;
//                        else max = gt;
//                    }
//                    else { min = bt;   max = gt; }
//                }
//                else if(dss[index] < dss[index + 2])
//                {
//                    max = rt;
//                    if(dss[index + 1] < dss[index])
//                        min = gt;
//                    else min = bt;
//                }
//                else { max = bt;   min = gt; }
//
//                delta = max - min;
//                if(delta == 0)
//                    hue = 0;
//                else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
//                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
//                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );
//
//                luminance = (max + min) / 2;
//                if(delta == 0)
//                    saturation = 0;
//                else
//                    saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
//                if (hue < 0) hue = hue + 360;
//                ////////////// End HSL //////////////
//
//
//                //////////////Hues ...
//                for (inner = 0; inner < chromaKeysArrayLength; inner++)
//                {
//                    if (mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
//                        && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
//                        && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
//                            )
//                        break;
//                }
//
//                if (inner == chromaKeysArrayLength)
//                {
//                    mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
//                    mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;
//
//                    mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
//                    mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;
//
//                    mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
//                    mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;
//
//                    chromaKeysArrayLength++;
//
//                    if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
//                        break;
//                }
//
//                if(drawDetectionGuilds)
//                {
//                    mDss.data[index + 2] = 125;
//                    mDss.data[index + 1] = 125;
//                    mDss.data[index] = 125;
//                }
//            }
//            if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
//                break;
//        }
//
//        for (int c = bottomTopArea, index, channels = mDss.channels(), incSrc = WIDTH * channels; c >= bottomBottomArea; c--)// && cDst > 0
//        {
//            index = c * channels + incSrc + incSrc;
//
//            for (int r = 2; r <= rightLeftArea; r++, index += incSrc)//, indexMask += widthMask) && rDst < dst.rows
//            {
////                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);
//
//                ////////////////Find HSL/////////////////
//                rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
//                if(dss[index + 2] < dss[index + 1])
//                {
//                    if(dss[index + 2] < dss[index])
//                    {
//                        min = rt;
//                        if(dss[index + 1] < dss[index]) max = bt;
//                        else max = gt;
//                    }
//                    else { min = bt;   max = gt; }
//                }
//                else if(dss[index] < dss[index + 2])
//                {
//                    max = rt;
//                    if(dss[index + 1] < dss[index])
//                        min = gt;
//                    else min = bt;
//                }
//                else { max = bt;   min = gt; }
//
//                delta = max - min;
//                if(delta == 0)
//                    hue = 0;
//                else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
//                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
//                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );
//
//                luminance = (max + min) / 2;
//                if(delta == 0)
//                    saturation = 0;
//                else
//                    saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
//                if (hue < 0) hue = hue + 360;
//                ////////////// End HSL //////////////
//
//
//                //////////////Hues ...
//                for (inner = 0; inner < chromaKeysArrayLength; inner++)
//                {
//                    if( mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
//                        && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
//                        && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
//                            )
//                        break;
//                }
//
//                if (inner == chromaKeysArrayLength)
//                {
//                    mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
//                    mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;
//
//                    mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
//                    mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;
//
//                    mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
//                    mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;
//
//                    chromaKeysArrayLength++;
//
//                    if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
//                        break;
//                }
//
//                if(drawDetectionGuilds)
//                {
//                    mDss.data[index + 2] = 225;
//                    mDss.data[index + 1] = 225;
//                    mDss.data[index] = 225;
//                }
//            }
//            if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
//                break;
//        }
//
//        for(int c = bottomTopArea, index, channels = mDss.channels(), incSrc = WIDTH * channels; c >= bottomBottomArea; c--)
//        {
//            index = (leftRightArea * WIDTH * channels) + c * channels;
//
//            for (int r = leftRightArea; r <= HEIGHT; r++, index += incSrc)
//            {
////                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);
//
//                ////////////////Find HSL/////////////////
//                rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
//                if(dss[index + 2] < dss[index + 1])
//                {
//                    if(dss[index + 2] < dss[index])
//                    {
//                        min = rt;
//                        if(dss[index + 1] < dss[index]) max = bt;
//                        else max = gt;
//                    }
//                    else { min = bt;   max = gt; }
//                }
//                else if(dss[index] < dss[index + 2])
//                {
//                    max = rt;
//                    if(dss[index + 1] < dss[index])
//                        min = gt;
//                    else min = bt;
//                }
//                else { max = bt;   min = gt; }
//
//                delta = max - min;
//                if(delta == 0)
//                    hue = 0;
//                else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
//                else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
//                else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );
//
//                luminance = (max + min) / 2;
//                if(delta == 0)
//                    saturation = 0;
//                else
//                    saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
//                if (hue < 0) hue = hue + 360;
//                ////////////// End HSL //////////////
//
//
//                //////////////Hues ...
//                for (inner = 0; inner < chromaKeysArrayLength; inner++)
//                {
//                    if( mHuesBot[inner] <= hue && hue <= mHuesTop[inner]
//                        && mSaturationBot[inner] <= saturation && saturation <= mSaturationTop[inner]
//                        && mLuminanceBot[inner] <= luminance && luminance <= mLuminanceTop[inner]
//                            )
//                        break;
//                }
//
//                if (inner == chromaKeysArrayLength)
//                {
//                    mHuesTop[chromaKeysArrayLength] = hue + HUE_THRESHOLD;
//                    mHuesBot[chromaKeysArrayLength] = hue - HUE_THRESHOLD;
//
//                    mSaturationTop[chromaKeysArrayLength] = saturation + SATURATION_THRESHOLD;
//                    mSaturationBot[chromaKeysArrayLength] = saturation - SATURATION_THRESHOLD;
//
//                    mLuminanceTop[chromaKeysArrayLength] = luminance + LUMINANCE_THRESHOLD;
//                    mLuminanceBot[chromaKeysArrayLength] = luminance - LUMINANCE_THRESHOLD;
//
//                    chromaKeysArrayLength++;
//
//                    if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
//                        break;
//                }
//
//                if(drawDetectionGuilds)
//                {
//                    mDss.data[index + 2] = 255;
//                    mDss.data[index + 1] = 0;
//                    mDss.data[index] = 0;
//                }
//            }
//            if (chromaKeysArrayLength >= CHROMA_KEY_ARRAY_LENGTH)
//                break;
//        }
//    }
//    else return false;
//
//    ////////////////////////////////////////////////////////
//    if (chromaKeysArrayLength < CHROMA_KEY_ARRAY_LENGTH && chromaKeysArrayLength > 2)
//    {
//        baseChromaKeysArrayLength = chromaKeysArrayLength;
//        return true;
//    }
//    else
//    {
//        chromaKeysArrayLength = 0;
//        baseChromaKeysArrayLength = chromaKeysArrayLength;
//        return false;
//    }
//}




//////////////////Find HSL/////////////////
//rt = (dss[index + 2] & 0xFF) / 255.0f;  gt = (dss[index + 1] & 0xFF) / 255.0;  bt = (dss[index] & 0xFF) / 255.0f;
//float min, max;
//if(dss[index + 2] < dss[index + 1])
//{
//if(dss[index + 2] < dss[index])
//{
//min = rt;
//if(dss[index + 1] < dss[index]) max = bt;
//else max = gt;
//}
//else { min = bt;   max = gt; }
//}
//else if(dss[index] < dss[index + 2])
//{
//max = rt;
//if(dss[index + 1] < dss[index])
//min = gt;
//else min = bt;
//}
//else { max = bt;   min = gt; }
//
//delta = max - min;
//if(delta == 0)
//hue = 0;
//else if (max == rt) hue = 60.0f * ( fmod( ((gt - bt) / delta), 6.0f) );
//else if (max == gt) hue = 60.0f * ( ((bt - rt) / delta) + 2.0f );
//else                hue = 60.0f * ( ((rt - gt) / delta) + 4.0f );
//
//luminance = (max + min) / 2;
//if(delta == 0)
//saturation = 0;
//else
//saturation =  delta / (1 - abs((int) ( (luminance * 2) - 1)) );
//if (hue < 0) hue = hue + 360;
//////////////// End HSL //////////////







//bool chromaKey::saveSkinColorHsl(uchar* dss, unsigned int faceLeft, unsigned int faceTop, unsigned int faceWidth, unsigned int faceHeight)
//{
//    if(skinHuesTop != NULL)
//    {
//        free(skinHuesTop);          free(skinHuesBot);
//        free(skinSaturationTop);    free(skinSaturationBot);
//        free(skinLuminanceTop);     free(skinLuminanceBot);
//    }
//    skinHuesTop = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinHuesBot = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinSaturationTop = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinSaturationBot = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinLuminanceTop = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinLuminanceBot = (float *) malloc(skinHslArrayLength * sizeof(float));
//
//    int* counter = (int *) malloc(skinHslArrayLength * sizeof(int));
//
//    skinArrayLength = 0;
//    unsigned int index, index2, faceCenter, c, r;
//    int size = 0;
//    float skin_hue, skin_sat, skin_lightness;
//    bool foundRange = false;
//    int i;
//
//    int red, green, blue;
//    float hueVal, saturationVal, luminanceVal;
//
//    int minRangeCount = 0.2f * faceHeight;
//
//    faceCenter = faceTop - (faceHeight * 0.2f);  //column
//    int leftToScan = faceLeft + (0.2f * faceHeight);
//    c = faceCenter;    // = column
//
//    index = (leftToScan * WIDTH * 4)+(c * 4);
//    int rightToScan = faceLeft + (faceHeight * 0.8f);
//
//    for ( r = leftToScan; r < rightToScan; r++, index += WIDTH_X_4)
//    {
//        rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hueVal, &saturationVal, &luminanceVal);
//
//        foundRange = false;
//
//        for(i = 0; i < size; i++)
//        {
//            if( hueVal > skinHuesBot[i] && hueVal < skinHuesTop[i]
//                && saturationVal > skinSaturationBot[i] && saturationVal < skinSaturationTop[i]
//                && luminanceVal  > skinLuminanceBot[i]  && luminanceVal  < skinLuminanceTop[i] )
//            {
//                break;
//            }
//            counter[i]++;
//        }
//
//        if(size + 1 >= skinHslArrayLength) // +2 is for this run and for the next run
//        {
//            // too many ranges found in face -> face does not found correctly
////            free(skinHuesTop);
////            free(skinHuesBot);
////
////            free(skinSaturationTop);
////            free(skinSaturationBot);
////
////            free(skinLuminanceTop);
////            free(skinLuminanceBot);
//
//            free(counter);
//            skinArrayLength = 0;
//
//            return false;
//        }
//
//        if(i == size)
//        {
//            size++;
//
//            skinHuesTop[i] = hueVal + SKIN_HUE_THRESHOLD;
//            skinHuesBot[i] = hueVal - SKIN_HUE_THRESHOLD;
//
//            skinSaturationTop[i] = saturationVal + SKIN_SATURATION_THRESHOLD;
//            skinSaturationBot[i] = saturationVal - SKIN_SATURATION_THRESHOLD;
//
//            skinLuminanceTop[i] = luminanceVal + SKIN_LUMINANCE_THRESHOLD;
//            skinLuminanceBot[i] = luminanceVal - SKIN_LUMINANCE_THRESHOLD;
//
//            counter[i] = 1;
//        }
//        if(drawDetectionGuilds)
//        {
//            dss[index] = 255;   //b
//            dss[index + 1] = 0; //g
//            dss[index + 2] = 0;
//        }
//    }
//
//
//    int topIndex = -1, topCount = 0;
//    for(i = 0; i < size; i++)
//    {
//        if (counter[i] >= minRangeCount && topCount < counter[i])
//        {
//            topCount = counter[i];
//            topIndex = i;
//        }
//    }
//
//    free(counter);
//    if(topIndex == -1)
//    {
////        free(skinHuesTop);
////        free(skinHuesBot);
////
////        free(skinSaturationTop);
////        free(skinSaturationBot);
////
////        free(skinLuminanceTop);
////        free(skinLuminanceBot);
//
//        skinArrayLength = 0;
//        return false;
//    }
//    else
//    {
//        skinArrayLength = size;
//
//        skinHueValue        = skinHuesTop[topIndex]       - SKIN_HUE_THRESHOLD;
//        skinSaturationValue = skinSaturationTop[topIndex] - SKIN_SATURATION_THRESHOLD;
//        skinLightnessValue  = skinLuminanceTop[topIndex]  - SKIN_LUMINANCE_THRESHOLD;
//        foundSkinColor      = true;
//        return true;
//    }
//
//}

//bool chromaKey::findChromaKeys(Mat mDss, int faceLeft, int faceTop, int faceRight, int faceBottom)
//{
//    int WIDTH = mDss.cols, HEIGHT = mDss.rows, WIDTH_X_4 = WIDTH * 4;
//
//    int padding = (int) (((float) (faceBottom - faceTop)) * PADDING_MULTIPLIER);
//    int paddingHalf = (int) (((float) (faceBottom - faceTop)) * PADDING_MULTIPLIER_HALF);
//    int paddingWidth = (int) (((float) (faceRight - faceLeft)) * PADDING_MULTIPLIER_HALF);
//
//    int bottomTopArea = faceBottom + padding;
//
//    if(bottomTopArea > WIDTH - 1)
//        bottomTopArea = WIDTH - 1;
//
//    int bottomBottomArea = faceTop;
//    int rightLeftArea = faceLeft - paddingWidth;
//    int leftRightArea = faceRight + paddingWidth;
//
////    if(bottomTopArea > WIDTH)
////        bottomTopArea = WIDTH;
////    else
//    if(bottomBottomArea < 1)
//        bottomBottomArea = 1;
//
//    Mat bDss(mDss.size(), mDss.type());
//
////    setBrightnessAndContrast(mDss.data, bDss.data);
////    blur(bDss, bDss, Kernel_5X5);
//
////    setBrightnessAndContrast(mDss.data, bDss.data);
//    blur(mDss, bDss, Kernel_5X5);
//
//
////    saveSkinColorHsl(bDss.data, faceLeft, faceBottom, faceRight - faceLeft, faceBottom - faceTop);
//
////    if(! saveSkinColorHsl(bDss.data, faceLeft, faceBottom, faceRight, faceBottom - faceTop))
////    {
////        inInitChromaKey = false;
////        return false;
////    }
//
//    uchar *dss = bDss.data;
//    ////////////////// Init chroma key
//
//    float blue, green, red, min, max, saturation = 0, luminance = 0, hue = 0;
//
//    float *huesTops = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//    float *huesBots = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//
//    float *saturationTops = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//    float *saturationBots = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//
//    float *luminanceTops = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//    float *luminanceBots = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//
//    int *counts = (int *) malloc(ARRAY_LENGTH * sizeof(int));
//
//    int inner;
//    int size = 0;
//
//
//    ///////////////////////////DETECTION////////////////////////////////////
//
//    if (faceLeft != 0)
//    {
//        for (int c = WIDTH - 2, indexSrc, channels = mDss.channels(), incSrc = WIDTH * channels; c >= bottomTopArea; c--)
//        {
//            indexSrc = c * channels + incSrc + incSrc;
//            for (int r = 2; r <= HEIGHT; r++, indexSrc += incSrc)
//            {
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);
//                //////////////Hues ...
//                for (inner = 0; inner < size; inner++)
//                {
//                    if (huesBots[inner] <= hue && hue <= huesTops[inner]
//                        && saturationBots[inner] <= saturation && saturation <= saturationTops[inner]
//                        && luminanceBots[inner] <= luminance && luminance <= luminanceTops[inner]
//                            )
//                    {
//                        counts[inner]++;
//                        break;
//                    }
//                }
//
//                if (inner == size)
//                {
//                    huesTops[size] = hue + HUE_THRESHOLD;
//                    huesBots[size] = hue - HUE_THRESHOLD;
//
//                    saturationTops[size] = saturation + SATURATION_THRESHOLD;
//                    saturationBots[size] = saturation - SATURATION_THRESHOLD;
//
//                    luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
//                    luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
//
//                    counts[size] = 1;
//                    size++;
//
//                    if (size >= ARRAY_LENGTH)
//                    {
//                        break;
//                    }
//                }
//
//                if(drawDetectionGuilds)
//                {
//                    mDss.data[indexSrc + 2] = 125;
//                    mDss.data[indexSrc + 1] = 125;
//                    mDss.data[indexSrc] = 125;
//                }
//            }
//            if (size >= ARRAY_LENGTH)
//            {
//                break;
//            }
//        }
//
//        for (int c = bottomTopArea, indexSrc, channels = mDss.channels(), incSrc = WIDTH * channels; c >= bottomBottomArea; c--)// && cDst > 0
//        {
//            indexSrc = c * channels + incSrc + incSrc;
//
//            for (int r = 2; r <= rightLeftArea; r++, indexSrc += incSrc)//, indexMask += widthMask) && rDst < dst.rows
//            {
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF,
//                         &hue, &saturation, &luminance);
//                //////////////Hues ...
//                for (inner = 0; inner < size; inner++)
//                {
//                    if (huesBots[inner] <= hue && hue <= huesTops[inner]
//                        && saturationBots[inner] <= saturation && saturation <= saturationTops[inner]
//                        && luminanceBots[inner] <= luminance && luminance <= luminanceTops[inner]
//                            )
//                    {
//                        counts[inner]++;
//                        break;
//                    }
//                }
//
//                if (inner == size)
//                {
//                    huesTops[size] = hue + HUE_THRESHOLD;
//                    huesBots[size] = hue - HUE_THRESHOLD;
//
//                    saturationTops[size] = saturation + SATURATION_THRESHOLD;
//                    saturationBots[size] = saturation - SATURATION_THRESHOLD;
//
//                    luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
//                    luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
//
//                    counts[size] = 1;
//                    size++;
//
//                    if (size >= ARRAY_LENGTH)
//                    {
//                        break;
//                    }
//                }
//
//                if(drawDetectionGuilds)
//                {
//                    mDss.data[indexSrc + 2] = 225;
//                    mDss.data[indexSrc + 1] = 225;
//                    mDss.data[indexSrc] = 225;
//                }
//            }
//            if (size >= ARRAY_LENGTH)
//            {
//                break;
//            }
//        }
//
//        for(int c = bottomTopArea, indexSrc, channels = mDss.channels(), incSrc = WIDTH * channels; c >= bottomBottomArea; c--)// && cDst > 0
//        {
//            indexSrc = (leftRightArea * WIDTH * channels) + c * channels;
//
//            for (int r = leftRightArea; r <= HEIGHT; r++, indexSrc += incSrc)//, indexMask += widthMask) && rDst < dst.rows
//            {
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);
//
//                //////////////Hues ...
//                for (inner = 0; inner < size; inner++)
//                {
//                    if( huesBots[inner] <= hue && hue <= huesTops[inner]
//                        && saturationBots[inner] <= saturation && saturation <= saturationTops[inner]
//                        && luminanceBots[inner] <= luminance && luminance <= luminanceTops[inner] )
//                    {
//                        counts[inner]++;
//                        break;
//                    }
//                }
//
//                if (inner == size)
//                {
//                    huesTops[size] = hue + HUE_THRESHOLD;
//                    huesBots[size] = hue - HUE_THRESHOLD;
//
//                    saturationTops[size] = saturation + SATURATION_THRESHOLD;
//                    saturationBots[size] = saturation - SATURATION_THRESHOLD;
//
//                    luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
//                    luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
//
//                    counts[size] = 1;
//                    size++;
//
//                    if (size >= ARRAY_LENGTH)
//                    {
//                        break;
//                    }
//                }
//
//                if(drawDetectionGuilds)
//                {
//                    mDss.data[indexSrc + 2] = 255;
//                    mDss.data[indexSrc + 1] = 0;
//                    mDss.data[indexSrc] = 0;
//                }
//            }
//            if (size >= ARRAY_LENGTH)
//            {
//                break;
//            }
//        }
//    }
//
//    ////////////////////////////////////////////////////////
//    if (size < ARRAY_LENGTH && size > 2)
//    {
//        if (mHuesBot != NULL)
//        {
//            free(mHuesBot);
//            free(mSaturationBot);
//            free(mSaturationTop);
//            free(mHuesTop);
//            free(mLuminanceBot);
//            free(mLuminanceTop);
//        }
//        mHuesBot = (float *) malloc(size * sizeof(float));
//        mHuesTop = (float *) malloc(size * sizeof(float));
//
//
//        mSaturationBot = (float *) malloc(size * sizeof(float));
//        mSaturationTop = (float *) malloc(size * sizeof(float));
//
//        mLuminanceBot = (float *) malloc(size * sizeof(float));
//        mLuminanceTop = (float *) malloc(size * sizeof(float));
//
//        int highestIndex = 0, highestCount = 0;
//
//        for (inner = 0; inner < size; inner++)
//        {
//            mHuesTop[inner] = huesTops[inner];
//            mHuesBot[inner] = huesBots[inner];
//
//            mSaturationTop[inner] = saturationTops[inner];
//            mSaturationBot[inner] = saturationBots[inner];
//
//            mLuminanceTop[inner] = luminanceTops[inner];
//            mLuminanceBot[inner] = luminanceBots[inner];
//
//            if (counts[inner] > highestCount)
//            {
//                highestCount = counts[inner];
//                highestIndex = inner;
//            }
//        }
//
//        hueBot = mHuesBot[highestIndex];
//        hueTop = mHuesTop[highestIndex];
//
//        saturationBottom = mSaturationBot[highestIndex];
//        saturationTop = mSaturationTop[highestIndex];
//
//        luminanceBottom = mLuminanceBot[highestIndex];
//        luminanceTop = mLuminanceTop[highestIndex];
//
//        chromaKeysArrayLength = size;
//
//        free(huesTops);
//        free(huesBots);
//        free(saturationTops);
//        free(saturationBots);
//        free(luminanceTops);
//        free(luminanceBots);
//        free(counts);
//
//        return true;
//    }
//    else
//    {
//        free(huesTops);
//        free(huesBots);
//        free(saturationTops);
//        free(saturationBots);
//        free(luminanceTops);
//        free(luminanceBots);
//        free(counts);
//
//        chromaKeysArrayLength = 0;
//
//        return false;
//    }
//}
//
//bool chromaKey::addChromaKeys(Mat mDss, int faceLeft, int faceTop, int faceRight, int faceBottom)
//{
//    int WIDTH = mDss.cols, HEIGHT = mDss.rows, WIDTH_X_4 = WIDTH * 4;
//
//    int padding = (int) (((float) (faceBottom - faceTop)) * PADDING_MULTIPLIER);
//    int paddingHalf = (int) (((float) (faceBottom - faceTop)) * PADDING_MULTIPLIER_HALF);
//    int paddingWidth = (int) (((float) (faceRight - faceLeft)) * PADDING_MULTIPLIER_HALF);// / 3;
//
//    int bottomTopArea = faceBottom + padding;
//
//    if(bottomTopArea > WIDTH - 1)
//        bottomTopArea = WIDTH - 1;
//
//    int topBottomArea = faceTop + paddingHalf;
//    int rightLeftArea = faceLeft - paddingWidth;
//    int leftRightArea = faceRight + paddingWidth;
//
//    if(bottomTopArea > WIDTH)
//        bottomTopArea = WIDTH;
//    else if(topBottomArea < 1)
//        topBottomArea = 1;
//
//    Mat bDss(mDss.size(), mDss.type());
//
////    setBrightnessAndContrast(mDss.data, bDss.data);
////    blur(bDss, bDss, Kernel_5X5);
//
//    blur(mDss, bDss, Kernel_5X5);
//
////    if(! saveSkinColorHsl(bDss.data, faceLeft, faceBottom, faceRight, faceBottom - faceTop))
////    {
////        return false;
////    }
//
//    uchar *dss = bDss.data;
//    ////////////////// Init chroma key
//
//    float blue, green, red, min, max, saturation = 0, luminance = 0, hue = 0;
//
//    float *huesTops = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//    float *huesBots = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//
//    float *saturationTops = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//    float *saturationBots = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//
//    float *luminanceTops = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//    float *luminanceBots = (float *) malloc(ARRAY_LENGTH * sizeof(float));
//
//    int *counts = (int *) malloc(ARRAY_LENGTH * sizeof(int));
//
//    for(int i = 0; i < chromaKeysArrayLength; i++)
//    {
//        huesTops[i] = mHuesTop[i];
//        huesBots[i] = mHuesBot[i];
//
//        saturationTops[i] = mSaturationTop[i];
//        saturationBots[i] = mSaturationBot[i];
//
//        luminanceTops[i] = mLuminanceTop[i];
//        luminanceBots[i] = mLuminanceBot[i];
//    }
//    int inner;
//    int size = chromaKeysArrayLength;
//
//    ///////////////////////////DETECTION////////////////////////////////////
//
//    if (faceLeft != 0)
//    {
//        for (int c = WIDTH - 2, indexSrc, channels = mDss.channels(), incSrc = WIDTH * channels; c >= bottomTopArea; c--)
//        {
//            indexSrc = c * channels + incSrc + incSrc;
//            for (int r = 2; r <= HEIGHT; r++, indexSrc += incSrc)
//            {
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);
//                //////////////Hues ...
//                for (inner = 0; inner < size; inner++)
//                {
//                    if (huesBots[inner] <= hue && hue <= huesTops[inner]
//                        && saturationBots[inner] <= saturation && saturation <= saturationTops[inner]
//                        && luminanceBots[inner] <= luminance && luminance <= luminanceTops[inner]   )
//                    {
//                        counts[inner]++;
//                        break;
//                    }
//                }
//
//                if (inner == size)
//                {
//                    huesTops[size] = hue + HUE_THRESHOLD;
//                    huesBots[size] = hue - HUE_THRESHOLD;
//
//                    saturationTops[size] = saturation + SATURATION_THRESHOLD;
//                    saturationBots[size] = saturation - SATURATION_THRESHOLD;
//
//                    luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
//                    luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
//
//                    counts[size] = 1;
//                    size++;
//
//                    if (size >= ARRAY_LENGTH)
//                    {
//                        break;
//                    }
//                }
//
//                if(drawDetectionGuilds)
//                {
//                    mDss.data[indexSrc + 2] = 125;
//                    mDss.data[indexSrc + 1] = 125;
//                    mDss.data[indexSrc] = 125;
//                }
//            }
//            if (size >= ARRAY_LENGTH)
//            {
//                break;
//            }
//        }
//
//        for (int c = bottomTopArea, indexSrc, channels = mDss.channels(), incSrc = WIDTH * channels; c >= topBottomArea; c--)// && cDst > 0
//        {
//            indexSrc = c * channels + incSrc + incSrc;
//
//            for (int r = 2; r <= rightLeftArea; r++, indexSrc += incSrc)//, indexMask += widthMask) && rDst < dst.rows
//            {
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF,
//                         &hue, &saturation, &luminance);
//                //////////////Hues ...
//                for (inner = 0; inner < size; inner++)
//                {
//                    if (huesBots[inner] <= hue && hue <= huesTops[inner]
//                        && saturationBots[inner] <= saturation && saturation <= saturationTops[inner]
//                        && luminanceBots[inner] <= luminance && luminance <= luminanceTops[inner]
//                            )
//                    {
//                        counts[inner]++;
//                        break;
//                    }
//                }
//
//                if (inner == size)
//                {
//                    huesTops[size] = hue + HUE_THRESHOLD;
//                    huesBots[size] = hue - HUE_THRESHOLD;
//
//                    saturationTops[size] = saturation + SATURATION_THRESHOLD;
//                    saturationBots[size] = saturation - SATURATION_THRESHOLD;
//
//                    luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
//                    luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
//
//                    counts[size] = 1;
//                    size++;
//
//                    if (size >= ARRAY_LENGTH)
//                    {
//                        break;
//                    }
//                }
//
//                if(drawDetectionGuilds)
//                {
//                    mDss.data[indexSrc + 2] = 225;
//                    mDss.data[indexSrc + 1] = 225;
//                    mDss.data[indexSrc] = 225;
//                }
//            }
//            if (size >= ARRAY_LENGTH)
//            {
//                break;
//            }
//        }
//
//        for(int c = bottomTopArea, indexSrc, channels = mDss.channels(), incSrc = WIDTH * channels; c >= topBottomArea; c--)// && cDst > 0
//        {
//            indexSrc = (leftRightArea * WIDTH * channels) + c * channels;
//
//            for (int r = leftRightArea; r <= HEIGHT; r++, indexSrc += incSrc)
//            {
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);
//                //////////////Hues ...
//                for (inner = 0; inner < size; inner++)
//                {
//                    if( huesBots[inner] <= hue && hue <= huesTops[inner]
//                        && saturationBots[inner] <= saturation && saturation <= saturationTops[inner]
//                        && luminanceBots[inner] <= luminance && luminance <= luminanceTops[inner] )
//                    {
//                        counts[inner]++;
//                        break;
//                    }
//                }
//
//                if (inner == size)
//                {
//                    huesTops[size] = hue + HUE_THRESHOLD;
//                    huesBots[size] = hue - HUE_THRESHOLD;
//
//                    saturationTops[size] = saturation + SATURATION_THRESHOLD;
//                    saturationBots[size] = saturation - SATURATION_THRESHOLD;
//
//                    luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
//                    luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
//
//                    counts[size] = 1;
//                    size++;
//
//                    if (size >= ARRAY_LENGTH)
//                    {
//                        break;
//                    }
//                }
//
//                if(drawDetectionGuilds)
//                {
//                    mDss.data[indexSrc + 2] = 255;
//                    mDss.data[indexSrc + 1] = 0;
//                    mDss.data[indexSrc] = 0;
//                }
//            }
//            if (size >= ARRAY_LENGTH)
//            {
//                break;
//            }
//        }
//    }
//
//    ////////////////////////////////////////////////////////
//    if (size < ARRAY_LENGTH && size > 2)
//    {
//        if (mHuesBot != NULL)
//        {
//            free(mHuesBot);
//            free(mSaturationBot);
//            free(mSaturationTop);
//            free(mHuesTop);
//            free(mLuminanceBot);
//            free(mLuminanceTop);
//        }
//        mHuesBot = (float *) malloc(size * sizeof(float));
//        mHuesTop = (float *) malloc(size * sizeof(float));
//
//
//        mSaturationBot = (float *) malloc(size * sizeof(float));
//        mSaturationTop = (float *) malloc(size * sizeof(float));
//
//        mLuminanceBot = (float *) malloc(size * sizeof(float));
//        mLuminanceTop = (float *) malloc(size * sizeof(float));
//
//        int highestIndex = 0, highestCount = 0;
//
//        for (inner = 0; inner < size; inner++)
//        {
//            mHuesTop[inner] = huesTops[inner];
//            mHuesBot[inner] = huesBots[inner];
//
//            mSaturationTop[inner] = saturationTops[inner];
//            mSaturationBot[inner] = saturationBots[inner];
//
//            mLuminanceTop[inner] = luminanceTops[inner];
//            mLuminanceBot[inner] = luminanceBots[inner];
//
//            if (counts[inner] > highestCount)
//            {
//                highestCount = counts[inner];
//                highestIndex = inner;
//            }
//        }
//
//        hueBot = mHuesBot[highestIndex];
//        hueTop = mHuesTop[highestIndex];
//
//        saturationBottom = mSaturationBot[highestIndex];
//        saturationTop = mSaturationTop[highestIndex];
//
//        luminanceBottom = mLuminanceBot[highestIndex];
//        luminanceTop = mLuminanceTop[highestIndex];
//
//        chromaKeysArrayLength = size;
//
//        free(huesTops);
//        free(huesBots);
//        free(saturationTops);
//        free(saturationBots);
//        free(luminanceTops);
//        free(luminanceBots);
//        free(counts);
//
//        return true;
//    }
//    else
//    {
//        free(huesTops);
//        free(huesBots);
//        free(saturationTops);
//        free(saturationBots);
//        free(luminanceTops);
//        free(luminanceBots);
//        free(counts);
//
//        chromaKeysArrayLength = 0;
//
//        return false;
//    }
//}



//bool chromaKey::saveSkinColorHsl(uchar* dss, unsigned int faceLeft, unsigned int faceTop, unsigned int faceWidth, unsigned int faceHeight)
//{
//    if(skinHuesTop != NULL)
//    {
//        free(skinHuesTop);          free(skinHuesBot);
//        free(skinSaturationTop);    free(skinSaturationBot);
//        free(skinLuminanceTop);     free(skinLuminanceBot);
//    }
//    skinHuesTop = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinHuesBot = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinSaturationTop = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinSaturationBot = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinLuminanceTop = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinLuminanceBot = (float *) malloc(skinHslArrayLength * sizeof(float));
//
//    int* counter = (int *) malloc(skinHslArrayLength * sizeof(int));
//
//    skinArrayLength = 0;
//    unsigned int index, index2, faceCenter, c, r;
//    int size = 0;
//    float skin_hue, skin_sat, skin_lightness;
//    bool foundRange = false;
//    int i;
//
//    int red, green, blue;
//    float hueVal, saturationVal, luminanceVal;
//
//    int minRangeCount = 0.2f * faceHeight;
//
//    faceCenter = faceTop - (faceHeight * 0.2f);  //column
//    int leftToScan = faceLeft + (0.2f * faceHeight);
//    c = faceCenter;    // = column
//
//    index = (leftToScan * WIDTH * 4)+(c * 4);
//    int rightToScan = faceLeft + (faceHeight * 0.8f);
//
//    for ( r = leftToScan; r < rightToScan; r++, index += WIDTH_X_4)
//    {
//        rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hueVal, &saturationVal, &luminanceVal);
//
//        foundRange = false;
//
//        for(i = 0; i < size; i++)
//        {
//            if( hueVal > skinHuesBot[i] && hueVal < skinHuesTop[i]
//                && saturationVal > skinSaturationBot[i] && saturationVal < skinSaturationTop[i]
//                && luminanceVal  > skinLuminanceBot[i]  && luminanceVal  < skinLuminanceTop[i] )
//            {
//                break;
//            }
//            counter[i]++;
//        }
//
//        if(size + 1 >= skinHslArrayLength) // +2 is for this run and for the next run
//        {
//            // too many ranges found in face -> face does not found correctly
////            free(skinHuesTop);
////            free(skinHuesBot);
////
////            free(skinSaturationTop);
////            free(skinSaturationBot);
////
////            free(skinLuminanceTop);
////            free(skinLuminanceBot);
//
//            free(counter);
//            skinArrayLength = 0;
//
//            return false;
//        }
//
//        if(i == size)
//        {
//            size++;
//
//            skinHuesTop[i] = hueVal + SKIN_HUE_THRESHOLD;
//            skinHuesBot[i] = hueVal - SKIN_HUE_THRESHOLD;
//
//            skinSaturationTop[i] = saturationVal + SKIN_SATURATION_THRESHOLD;
//            skinSaturationBot[i] = saturationVal - SKIN_SATURATION_THRESHOLD;
//
//            skinLuminanceTop[i] = luminanceVal + SKIN_LUMINANCE_THRESHOLD;
//            skinLuminanceBot[i] = luminanceVal - SKIN_LUMINANCE_THRESHOLD;
//
//            counter[i] = 1;
//        }
//        if(drawDetectionGuilds)
//        {
//            dss[index] = 255;   //b
//            dss[index + 1] = 0; //g
//            dss[index + 2] = 0;
//        }
//    }
//
//
//    int topIndex = -1, topCount = 0;
//    for(i = 0; i < size; i++)
//    {
//        if (counter[i] >= minRangeCount && topCount < counter[i])
//        {
//            topCount = counter[i];
//            topIndex = i;
//        }
//    }
//
//    free(counter);
//    if(topIndex == -1)
//    {
////        free(skinHuesTop);
////        free(skinHuesBot);
////
////        free(skinSaturationTop);
////        free(skinSaturationBot);
////
////        free(skinLuminanceTop);
////        free(skinLuminanceBot);
//
//        skinArrayLength = 0;
//        return false;
//    }
//    else
//    {
//        skinArrayLength = size;
//
//        skinHueValue        = skinHuesTop[topIndex]       - SKIN_HUE_THRESHOLD;
//        skinSaturationValue = skinSaturationTop[topIndex] - SKIN_SATURATION_THRESHOLD;
//        skinLightnessValue  = skinLuminanceTop[topIndex]  - SKIN_LUMINANCE_THRESHOLD;
//        foundSkinColor      = true;
//        return true;
//    }
//
//}



//void chromaKey::rgbToHsl(int red, int green, int blue, float *hue, float *saturation, float *luminance)
//{
//    // find hue
////    min = (min = (red < green ? red : green) ) < blue ? min : blue;
////    max = (max = (red > green ? red : green) ) > blue ? max : blue;
////
////    if(max == min) hue = 0;
////    else if (max == red)    hue = ((green - blue) / (max - min));
////    else if (max == green)  hue = 2.0f + ((blue - red) / (max - min));
////    else                    hue = 4.0f + ((red - green) / (max - min));
////
////    hue = hue * 60.0f;
////    if (hue < 0) hue = hue + 360.0f;
////
////    ////////////// end init hue for pixel
////
////    // find luminance and saturation
////
//////                luminance = (min + max ) / 2;
//////                if(luminance < 125f)//if(luminance < 0.5f)
//////                    saturation = (max - min)*(max + min);
//////                else saturation = (max - min)*(510 - max - min);//  2 * 255 //saturation = (max - min)*(2 - max - min);
////
////    luminance = (red + green + blue)/3.0f;
////
////    if(min == max) // c = 0
////        saturation = 0;
////    else if(luminance != 0)
////        saturation = 1 - (min / luminance);
//
//
//
///////////////////////////////////////////////////////////
//
//    float min, max;
//    if(red < green)
//    {
//        if(red < blue)
//        {
//            min = red;
//            if(green < blue)
//                max = blue;
//            else
//                max = green;
//        }
//        else
//        {
//            min = blue;
//            max = red;
//        }
//    }
//    else if(blue < red)
//    {
//        max = red;
//        if(green < blue)
//            min = green;
//        else min = blue;
//    }
//    else
//    {
//        max = blue;
//        min = green;
//    }
////                min = (min = red < green ? red : green) < blue ? min : blue;
////                max = (max = red > green ? red : green) > blue ? max : blue;
//
//    // find hue
//
//    if(max == min) *hue = 0;
//    else if (max == red)    *hue = (green - blue) / (max - min);
//    else if (max == green)  *hue = 2 + ((blue - red) / (max - min));
//    else                    *hue = 4 + ((red - green) / (max - min));
//
//    *hue = *hue * 60;
//    if (hue < 0) hue = hue + 360;
//    ////////////// end init hue for pixel
//
////                luminance = (min + max ) / 2;
////                if(luminance < 125f)//if(luminance < 0.5f)
////                    saturation = (max - min)*(max + min);
////                else saturation = (max - min)*(510 - max - min);//  2 * 255 //saturation = (max - min)*(2 - max - min);
//
//    *luminance = (red + green + blue)/3;
//
//    if(min == max) // c = 0
//        *saturation = 0.0f;
//    else if(*luminance != 0)
//        *saturation = 1.0f - (min / *luminance);
//}


//
//void chromaKey::getChromaKeysBuffer(Mat mDss, Mat chromaKeyBuffer, int faceLeft, int faceTop, int faceRight, int faceBottom)
//{
//    Mat mDsd(mDss.size(), mDss.type());
//    Mat bDss;
//
////    setBrightnessAndContrast(mDss.data, mDsd.data);
//
////    blur(mDsd, bDss, Kernel_5X5);
//    blur(mDss, bDss, Kernel_5X5);
//
//    uchar* dss = bDss.data;
//
//    //////////////////// Local field declaration
//    float luminance, saturation, hue;
//    bool isObject(false);
//    bool isFirsto = true;
//    ////////////////// image manipulation
//
//    int widthHeightX4 = (WIDTH_X_4 * HEIGHT) - 4;
//    int widthHeight = (WIDTH * HEIGHT) - 1;
//
//    for(int column = WIDTH, row, inner, index, indexChromaBuffer; column > 1; column--) // index = ((row * WIDTH) + column) * 4
//    {
//        index = widthHeightX4 - (column * 4);
//        indexChromaBuffer = widthHeight - column;
//
//        for (row = HEIGHT - 1; row > increase; row -= increase, index -= (WIDTH_X_4 * increase), indexChromaBuffer -= (WIDTH * increase) )
//        {
////            if(isColorInThreshold(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF))
////            {
////                chromaKeyBuffer.data[indexChromaBuffer] = 255;
////                continue;
////            }
//
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
//
////            isObject = ! isInChromaKeys(hue, luminance, saturation) || isSkinColor(hue, saturation, luminance);
//            isObject = ! isInChromaKeys(hue, luminance, saturation);
//
////            if(isObject)
////            {
////                chromaKeyBuffer.data[indexChromaBuffer] = 255;
////                continue;
////            }
//
//            if(isObject)
//            {
//                row += increase;
//                index += (increase * WIDTH_X_4);
//                indexChromaBuffer += (increase * WIDTH);
//
//                for (; row > 1; row--, index -= WIDTH_X_4, indexChromaBuffer -= WIDTH)
//                {
//                    rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
//
//                    mDss.data[index] = 0;
//                    mDss.data[index + 1] = 255;
//                    mDss.data[index + 2] = 0;
//
//                    if( ! isInChromaKeys(hue, saturation, luminance) )
//                        break;
//                }
//
//                chromaKeyBuffer.data[indexChromaBuffer] = 255;
//                index -= WIDTH_X_4;
//                indexChromaBuffer -= WIDTH;
//                row--;
//
//                for ( ; row > 1; row--, index -= WIDTH_X_4, indexChromaBuffer -= WIDTH )
//                {
//                    rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
//
//                    if( isInChromaKeys(hue, saturation, luminance) )
//                        break;
//
//
//                    chromaKeyBuffer.data[indexChromaBuffer] = 255;
//
//                    mDss.data[index] = 0;
//                    mDss.data[index + 1] = 0;
//                    mDss.data[index + 2] = 255;
//                }
//
//                index -= WIDTH_X_4;
//                indexChromaBuffer -= WIDTH;
//                row--;
//            }
//        }
//    }
//
//    mDsd.release();
//    bDss.release();
//}

//void chromaKey::getChromaKeysBuffer(Mat mDss, Mat chromaKeyBuffer, int faceLeft, int faceTop, int faceRight, int faceBottom)
//{
//    Mat mDsd(mDss.size(), mDss.type());
//    Mat bDss;
//
////    setBrightnessAndContrast(mDss.data, mDsd.data);
//
////    blur(mDsd, bDss, Kernel_5X5);
//    blur(mDss, bDss, Kernel_5X5);
//
//    uchar* dss = bDss.data;
//
//
//    //////////////////// Local field declaration
//    float luminance, saturation, hue;
//    bool isObject(false);
//    bool isFirsto = true;
//    ////////////////// image manipulation
//
//    int widthHeightX4 = (WIDTH_X_4 * HEIGHT) - 4;
//    int widthHeight = (WIDTH * HEIGHT) - 1;
//
//    bool isFirstTest(true);
//    int col = 0;
//    int cols = 0;
//
//    for(int column = WIDTH, row, inner, index, indexChromaBuffer; column > 1; column--) // index = ((row * WIDTH) + column) * 4
//    {
//
//        index = widthHeightX4 - (column * 4);
//        indexChromaBuffer = widthHeight - column;
//        cols++;
//        if(cols>255)
//            cols=255;
//
//        for (row = HEIGHT - 1; row > increase; row -= increase, index -= (WIDTH_X_4 * increase), indexChromaBuffer -= (WIDTH * increase) )
//        {
//
////            if(isColorInThreshold(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF))
////            {
////                chromaKeyBuffer.data[indexChromaBuffer] = 255;
////                continue;
////            }
////            else
////                chromaKeyBuffer.data[indexChromaBuffer] = 0;
//
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
//
////            isObject = ! isInChromaKeys(hue, luminance, saturation) || isSkinColor(hue, saturation, luminance);
//            isObject = ! isInChromaKeys(hue, luminance, saturation);
//
////            if(isObject)
////                chromaKeyBuffer.data[indexChromaBuffer] = 255;
//
//            if(isObject)
//            {
//                row += increase;
//                index += increase * WIDTH_X_4;
//                indexChromaBuffer += increase * WIDTH;
////
//                for (; row > 1; row--, index -= WIDTH_X_4, indexChromaBuffer -= WIDTH)
//                {
//                    rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
//                    if( ! isInChromaKeys(hue, saturation, luminance) )
//                        break;
//                }
////
//                chromaKeyBuffer.data[indexChromaBuffer] = 255;
////
//                index -= WIDTH_X_4;
//                indexChromaBuffer -= WIDTH;
//                row--;
//////
//                col = 0;
//                for ( ; row > 1; row--, index -= WIDTH_X_4, indexChromaBuffer -= WIDTH )
//                {
//                    rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
//
//                    isObject = ! isInChromaKeys(hue, saturation, luminance);
//
//                    if(!isObject)
//                    {
//                        break;
//                    }
//
//                    col++;
//                    if(col>255)
//                        col=255;
//                    mDss.data[index] = 0;
//                    mDss.data[index + 1] = col;
//                    mDss.data[index + 2] = cols; // blue
//                    chromaKeyBuffer.data[indexChromaBuffer] = 255;
//                }
//                isFirstTest = false;
////
////                chromaKeyBuffer.data[indexChromaBuffer] = 255;
//
////                if( row < increase)
////                    break;
//            }
////            else
////            {
//////                mDss.data[index + 2] = 0;
//////                mDss.data[index + 1] = 0;
//////                mDss.data[index] = 255;    // red
//////
//////                chromaKeyBuffer.data[indexChromaBuffer] = 0;
////            }
//        }
//    }
//
//    mDsd.release();
//    bDss.release();
//}




//    inInitChromaKey = false;
//
//    if(true)
//        return;
//
//
//
//    for(int c = WIDTH, index; c > bottomTopArea; c--)
//    {
//        index = c * 4;
//        for (int r = 0; r < HEIGHT; r++, index += WIDTH_X_4)
//        {
//            //////////////// init hsl for pixel
////                      red                     green               blue
////            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
////
////            //////////////Hues ...
////
////            for(inner = 0; inner < size; inner++)
////            {
////                if(huesBots[inner] < hue && hue < huesTops[inner]
////                   && saturationBots[inner] < saturation && saturation < saturationTops[inner]
////                   && luminanceBots[inner] < luminance && luminance < luminanceTops[inner]
////                        )
////                {
////                    counts[inner]++;
////                    break;
////                }
////            }
////
////            if(inner == size)
////            {
////                huesTops[size] = hue + HUE_THRESHOLD;
////                huesBots[size] = hue - HUE_THRESHOLD;
////
////                saturationTops[size] = saturation + SATURATION_THRESHOLD;
////                saturationBots[size] = saturation - SATURATION_THRESHOLD;
////
////                luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
////                luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
////
////                counts[size] = 1;
////                size++;
////
////                if(size >= ARRAY_LENGTH-2)
////                    break;
////            }
//
////            if(size >= ARRAY_LENGTH-2)
////                break;
//
//            mDss.data[index + 2] = 255;
//            mDss.data[index + 1] = 125;
//            mDss.data[index] = 0;
//        }
//    }
//
//    for(int c = bottomTopArea, index; c > bottomBottomAreas; c--)
//    {
//        index = c * 4;
//        for (int r = 0; r < rightBorderLeftArea; r++, index += WIDTH_X_4)
//        {
//            //////////////// init hsl for pixel
////                      red                     green               blue
////            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
////
////            //////////////Hues ...
////
////            for(inner = 0; inner < size; inner++)
////            {
////                if(huesBots[inner] < hue && hue < huesTops[inner]
////                   && saturationBots[inner] < saturation && saturation < saturationTops[inner]
////                   && luminanceBots[inner] < luminance && luminance < luminanceTops[inner]
////                        )
////                {
////                    counts[inner]++;
////                    break;
////                }
////            }
////
////            if(inner == size)
////            {
////                huesTops[size] = hue + HUE_THRESHOLD;
////                huesBots[size] = hue - HUE_THRESHOLD;
////
////                saturationTops[size] = saturation + SATURATION_THRESHOLD;
////                saturationBots[size] = saturation - SATURATION_THRESHOLD;
////
////                luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
////                luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
////
////                counts[size] = 1;
////                size++;
////
////                if(size >= ARRAY_LENGTH-2)
////                    break;
////            }
//
////            if(size >= ARRAY_LENGTH-2)
////                break;
//
//            mDss.data[index + 2] = 255;
//            mDss.data[index + 1] = 125;
//            mDss.data[index] = 0;
//        }
//    }
//
//    for(int c = bottomTopArea, index; c > bottomBottomAreas; c--)
//    {
//        index = c * 4;
//        for (int r = leftBorderRightArea; r < HEIGHT; r++, index += WIDTH_X_4)
//        {
//            //////////////// init hsl for pixel
////                      red                     green               blue
////            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
////
////            //////////////Hues ...
////
////            for(inner = 0; inner < size; inner++)
////            {
////                if(huesBots[inner] < hue && hue < huesTops[inner]
////                   && saturationBots[inner] < saturation && saturation < saturationTops[inner]
////                   && luminanceBots[inner] < luminance && luminance < luminanceTops[inner]
////                        )
////                {
////                    counts[inner]++;
////                    break;
////                }
////            }
////
////            if(inner == size)
////            {
////                huesTops[size] = hue + HUE_THRESHOLD;
////                huesBots[size] = hue - HUE_THRESHOLD;
////
////                saturationTops[size] = saturation + SATURATION_THRESHOLD;
////                saturationBots[size] = saturation - SATURATION_THRESHOLD;
////
////                luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
////                luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
////
////                counts[size] = 1;
////                size++;
////
////                if(size >= ARRAY_LENGTH-2)
////                    break;
////            }
//
////            if(size >= ARRAY_LENGTH-2)
////                break;
//
//            mDss.data[index + 2] = 255;
//            mDss.data[index + 1] = 125;
//            mDss.data[index] = 0;
//        }
//    }
//
//    inInitChromaKey = false;
//
//    if(true)
//        return;
//
//
//
//
//    for(int row = 2, column; row < HEIGHT - 2; row++) // index = ((row * WIDTH) + column) * 4
//    {
//        index = (row * WIDTH_X_4) + 8;
//        for (column = bottomTopArea; column < WIDTH - 2; column ++, index += 4)
//        {
//            //////////////// init hsl for pixel
////                      red                     green               blue
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
//
//            //////////////Hues ...
//
//            for(inner = 0; inner < size; inner++)
//            {
//                if(huesBots[inner] < hue && hue < huesTops[inner]
//                   && saturationBots[inner] < saturation && saturation < saturationTops[inner]
//                   && luminanceBots[inner] < luminance && luminance < luminanceTops[inner]
//                        )
//                {
//                    counts[inner]++;
//                    break;
//                }
//            }
//
//            if(inner == size)
//            {
//                huesTops[size] = hue + HUE_THRESHOLD;
//                huesBots[size] = hue - HUE_THRESHOLD;
//
//                saturationTops[size] = saturation + SATURATION_THRESHOLD;
//                saturationBots[size] = saturation - SATURATION_THRESHOLD;
//
//                luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
//                luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
//
//                counts[size] = 1;
//                size++;
//
//                if(size >= ARRAY_LENGTH-2)
//                    break;
//            }
//
//            mDss.data[index + 2] = 255;
//            mDss.data[index + 1] = 125;
//            mDss.data[index] = 0;
//        }
//        if(size >= ARRAY_LENGTH - 2)
//            break;
//    }
//
//    /////////////////////////////////////////////////////////////////
//
//    for(int row = 2, column; row < HEIGHT - 2; row++) // index = ((row * WIDTH) + column) * 4
//    {
//        index = (row * WIDTH_X_4) + 8;
//        for (column = 2; column < WIDTH - 2; column ++, index += 4)
//        {
//            //////////////// init hsl for pixel
////                      red                     green               blue
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
//
//            //////////////Hues ...
//
//            for(inner = 0; inner < size; inner++)
//            {
//                if(huesBots[inner] < hue && hue < huesTops[inner]
//                   && saturationBots[inner] < saturation && saturation < saturationTops[inner]
//                   && luminanceBots[inner] < luminance && luminance < luminanceTops[inner]
//                        )
//                {
//                    counts[inner]++;
//                    break;
//                }
//            }
//
//            if(inner == size)
//            {
//                huesTops[size] = hue + HUE_THRESHOLD;
//                huesBots[size] = hue - HUE_THRESHOLD;
//
//                saturationTops[size] = saturation + SATURATION_THRESHOLD;
//                saturationBots[size] = saturation - SATURATION_THRESHOLD;
//
//                luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
//                luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
//
//                counts[size] = 1;
//                size++;
//
//                if(size >= ARRAY_LENGTH-2)
//                    break;
//            }
//        }
//        if(size >= ARRAY_LENGTH - 2)
//            break;
//    }



//void chromaKey::clearAndFillChromaKeys(Mat mBg, Mat mDss, Mat chromaKeyBuffer, int faceLeft, int faceTop, int faceRight, int faceBottom)
//{
//    Mat mDsd(mDss.size(), mDss.type());
//
////    setBrightness(mDss.data, mDss.data, userBrightness);
//
////    setBrightness(mDss.data, mDsd .data, BRIGHTNESS);
//
//    uchar* bg = mBg.data;
//
//    Mat bDss;
//
//    blur(mDsd, bDss, Kernel_5X5);
////    blur(bDss, bDss, Size(5, 5));
//
//    uchar* dss = bDss.data;
//
//
////    gaussianBlur2(inBuffer);
//////        gaussianBlur25(inBuffer);
////
////    ////////////////// Local field declaration
////
//    float blue, green, red, min, max, luminance, saturation;
//    float lastBlue, lastGreen, lastRed;
//    float hue;
//    float lastLuminance = 0, lastHue = 0, lastSaturation = 0;
//
//    bool found;
//
//    bool isFirsto = true;
//
//    ////////////////// image manipulation
//
////        for(int row = 0, column, inner, index; row<HEIGHT; row++) // index = ((row * WIDTH) + column) * 4
////        {
////            index = row * WIDTH_X_4;
//////            index = (row * WIDTH_X_4) + 4;
////
////
////            for (column = 0; column < WIDTH; column += increase, index += byteIncrease)
////            {
////
//    for(int column = WIDTH, row, inner, index, indexChromaBuffer; column>0; column--) // index = ((row * WIDTH) + column) * 4
//    {
//        index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
//        indexChromaBuffer = (WIDTH * HEIGHT) - 1 - (column * 1);
////            c++;r=0;
////            index = (row * WIDTH_X_4) + 4;
//
//        for (row = HEIGHT - 1; row > 0; row -= increase, index -= (WIDTH_X_4) * increase, indexChromaBuffer -= WIDTH * increase )
////            for(column = 1; column<WIDTH; column++, index += 4) // index = ((row * WIDTH) + column) * 4
//        {
//
//            /// Pass the face with no checks... canceled for testing the multi buffer
//
////            if(faceLeft != 0 && isInFace(column, row, faceLeft, faceTop, faceRight, faceBottom) )
////            {
////                bg[index] = mDss.data[index];
////                bg[index + 1] = mDss.data[index + 1];
////                bg[index + 2] = mDss.data[index + 2];
////                continue;
////            }
//
//
//
//
//
//            ////////////// init hue for pixel
//
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
//
////            // get rgb
////            blue = dss[index] & 0xFF;
////            green = dss[index + 1] & 0xFF;
////            red = dss[index + 2] & 0xFF;
////
//////            if(red < COLOR_THRESHOLD && green < COLOR_THRESHOLD && blue < COLOR_THRESHOLD)
//////            {
////////                bg[index] = 0;
////////                bg[index + 1] = 0;
////////                bg[index + 2] = 255;
//////
//////                bg[index] = dss[index];
//////                bg[index + 1] = dss[index + 1];
//////                bg[index + 2] = dss[index + 2];
//////
//////                continue;
//////            }
////
////            // find hue
////            min = (min = red < green ? red : green) < blue ? min : blue;
////            max = (max = red > green ? red : green) > blue ? max : blue;
////
////            if(max == min) hue = 0;
////            else if (max == red)    hue = ((green - blue) / (max - min));
////            else if (max == green)  hue = 2.0f + ((blue - red) / (max - min));
////            else                    hue = 4.0f + ((red - green) / (max - min));
////
////            hue = hue * 60.0f;
////            if (hue < 0) hue = hue + 360.0f;
////
////            ////////////// end init hue for pixel
////
////            // find luminance and saturation
////
//////                luminance = (min + max ) / 2;
//////                if(luminance < 125f)//if(luminance < 0.5f)
//////                    saturation = (max - min)*(max + min);
//////                else saturation = (max - min)*(510 - max - min);//  2 * 255 //saturation = (max - min)*(2 - max - min);
////
////            luminance = (red + green + blue)/3.0f;
////
////            if(min == max) // c = 0
////                saturation = 0;
////            else if(luminance != 0)
////                saturation = 1 - (min / luminance);
//
//
//
//
////            if(true)
////            {
////                bg[index] = hue;
////                bg[index + 1] = saturation;
////                bg[index + 2] = luminance;
////                continue;
////            }
//
//            ////////////// end find luminance and saturation
//
//            ////////////// Manipulate the pixel
//
////            found = false;
////            for(inner = 0;inner<length; inner++)
////            {
////                if(mHuesBot[inner] < hue && hue < mHuesTop[inner]
////                   && mSaturationBot[inner] < saturation && saturation < mSaturationTop[inner]
////                   && mLuminanceBot[inner] < luminance && luminance < mLuminanceTop[inner]
////                        )
////                {
////                    found = true;
////                    lastHue = hue;
////                    lastLuminance = luminance;
////                    lastSaturation = saturation;
////                    break;
////                }
////            }
////            if(isInChromaKeys(hue, luminance, saturation))
////            {
////                found = true;
////                lastHue = hue;
////                lastLuminance = luminance;
////                lastSaturation = saturation;
////            }
//
//            found = isInChromaKeys(hue, luminance, saturation);
//
////                if (
//////                            hue == hues[inner]
////                    hue < hueTop && hueBot < hue
////                            && luminance < luminanceTop && luminanceBot < luminance
////                            && saturation < saturationTop && saturationBot < saturation
////                        );
//////                    {
//////                        found = true;// setLittleEndian(buffer, Color.RED, i);
////////                        break;
//////                    }
//////                }
//////                {
//////                    else//
//////
////                else //
//            if(!found)
//            {
////                bg[index] = mDss.data[index];
////                bg[index + 1] = mDss.data[index + 1];
////                bg[index + 2] = mDss.data[index + 2];
//
//                chromaKeyBuffer.data[indexChromaBuffer] = 255;
//
//                if(true)
//                    continue;
//
////                    lastLuminance = luminance;
////                bg[index] = 255;
////                bg[index + 1] = 0;
////                bg[index + 2] = 0;
////                    lastSaturation = saturation;
////                    lastHue = hue;
//
//                bool foundInner = false; // opposite of the above found !
//                for (row += increase, index += increase * WIDTH_X_4; row > 0; row -= 1, index -= (WIDTH_X_4))
//                {
//                    blue = dss[index] & 0xFF;
//                    green = dss[index + 1] & 0xFF;
//                    red = dss[index + 2] & 0xFF;
//
//                    if(red < COLOR_THRESHOLD && green < COLOR_THRESHOLD && blue < COLOR_THRESHOLD)
//                    {
////                        bg[index] = 255;
////                        bg[index + 1] = 255;
////                        bg[index + 2] = 255;
//
//                        bg[index] = dss[index];
//                        bg[index + 1] = dss[index + 1];
//                        bg[index + 2] = dss[index + 2];
//
//                        continue;
//                    }
//
//                    min = (min = red < green ? red : green) < blue ? min : blue;
//                    max = (max = red > green ? red : green) > blue ? max : blue;
//
//                    if (max == min) hue = 0;
//                    else if (max == red)
//                        hue = (green - blue) / (max - min);
//                    else if (max == green)
//                        hue = 2.0f + ( (blue - red) / (max - min));
//                    else hue = 4.0f + ((red - green) / (max - min));
//
//                    hue = hue * 60.0f;
//                    if (hue < 0) hue = hue + 360.0f;
//
//                    ////////////// end init hue for pixel
//
//                    // find luminance and saturation
//
////                        luminance = (min + max ) / 2;
////                        if(luminance < 125f)//if(luminance < 0.5f)
////                            saturation = (max - min)*(max + min);
////                        else saturation = (max - min)*(510 - max - min);//  2 * 255 //saturation = (max - min)*(2 - max - min);
//
//                    luminance = (red + green + blue)/3.0f;
//
//                    if(min == max) // c = 0
//                        saturation = 0;
//                    else if(luminance != 0)
//                        saturation = 1 - (min / luminance);
//
////                    if(!foundInner)
////                    {
////                        foundInner = !isInChromaKeys(hue, luminance, saturation);
////                        if(foundInner)
////                        {
////                            lastHue = hue;
////                            lastLuminance = luminance;
////                            lastSaturation = saturation;
////                        }
////                        else continue;
////                    }
//////                    else if(isInChromaKeys(hue, luminance, saturation))
//////                        break;
////
////                    if(
////                            (lastHue - HUE_DIF_THRESHOLD < hue && hue < lastHue + HUE_DIF_THRESHOLD
////                             && lastSaturation - SATURATION_DIF_THRESHOLD < saturation && saturation < lastSaturation + SATURATION_DIF_THRESHOLD
////                             && lastLuminance - LUMINANCE_DIF_THRESHOLD < luminance && luminance < lastLuminance + LUMINANCE_DIF_THRESHOLD)
////                            )
////                    {
////                        break;
////                    }
//
//                    if(!isInChromaKeys(hue, luminance, saturation))
//                    {
//                        bg[index] = dss[index];
//                        bg[index + 1] = dss[index + 1];
//                        bg[index + 2] = dss[index + 2];
//                    }
//                    lastHue = hue;
//                    lastLuminance = luminance;
//                    lastSaturation = saturation;
//                }
//
//
//                //////////////// end Manipulate the pixel ( main )
//            }
//        }
//    }
//
////    mBg.copyTo(mDss);
//
//}



//void chromaKey::findChromaKeys(Mat mDss)
//{
//    if(inInitChromaKey)
//        return;
//    inInitChromaKey = true;
//
////    setBrightness(mDss.data, mDss.data, userBrightness);
//
//    setBrightness(mDss.data, mDss.data, BRIGHTNESS);
//
//    Mat bDss;
//
//    blur(mDss, bDss, Kernel_5X5);
////    blur(bDss, bDss, Size(5, 5));
//
//    uchar* dss = bDss.data;
//    ////////////////// Init chroma key
//
//    float blue, green, red, min, max, saturation = 0, luminance = 0, hue = 0;
//    int index;
//
//    float* huesTops = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//    float* huesBots = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//
//    float* saturationTops = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//    float* saturationBots = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//
//    float* luminanceTops = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//    float* luminanceBots = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//
//    int* counts = (int*)malloc(ARRAY_LENGTH * sizeof(int));;
//
//    int inner;
//    int size = 0;
//
//    for(int row = 2, column; row < HEIGHT - 2; row++) // index = ((row * WIDTH) + column) * 4
//    {
//        index = (row * WIDTH_X_4) + 8;
//        for (column = 2; column < WIDTH - 2; column ++, index += 4)
//        {
//            //////////////// init hsl for pixel
////                      red                     green               blue
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
//
//            //////////////Hues ...
//
//            for(inner = 0; inner < size; inner++)
//            {
//                if(huesBots[inner] < hue && hue < huesTops[inner]
//                   && saturationBots[inner] < saturation && saturation < saturationTops[inner]
//                   && luminanceBots[inner] < luminance && luminance < luminanceTops[inner]
//                  )
//                {
//                    counts[inner]++;
//                    break;
//                }
//            }
//
//            if(inner == size)
//            {
//                huesTops[size] = hue + HUE_THRESHOLD;
//                huesBots[size] = hue - HUE_THRESHOLD;
//
//                saturationTops[size] = saturation + SATURATION_THRESHOLD;
//                saturationBots[size] = saturation - SATURATION_THRESHOLD;
//
//                luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
//                luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
//
//                counts[size] = 1;
//                size++;
//
//                if(size >= ARRAY_LENGTH-2)
//                    break;
//            }
//        }
//        if(size >= ARRAY_LENGTH - 2)
//            break;
//    }
//
//    mHuesBot = (float*)malloc(size * sizeof(float));
//    mHuesTop = (float*)malloc(size * sizeof(float));
//
//    mSaturationBot = (float*)malloc(size * sizeof(float));
//    mSaturationTop = (float*)malloc(size * sizeof(float));
//
//    mLuminanceBot = (float*)malloc(size * sizeof(float));
//    mLuminanceTop = (float*)malloc(size * sizeof(float));
//
//    int highestIndex = 0, highestCount = 0;
//
//    for(inner = 0; inner < size; inner++)
//    {
//        mHuesTop[inner] = huesTops[inner];
//        mHuesBot[inner] = huesBots[inner];
//
//        mSaturationTop[inner] = saturationTops[inner];
//        mSaturationBot[inner] = saturationBots[inner];
//
//        mLuminanceTop[inner] = luminanceTops[inner];
//        mLuminanceBot[inner] = luminanceBots[inner];
//
//        if(counts[inner] > highestCount)
//        {
//            highestCount = counts[inner];
//            highestIndex = inner;
//        }
//    }
//
//    hueBot = mHuesBot[highestIndex];
//    hueTop = mHuesTop[highestIndex];
//
//    saturationBottom = mSaturationBot[highestIndex];
//    saturationTop =  mSaturationTop[highestIndex];
//
//    luminanceBottom =  mLuminanceBot[highestIndex];
//    luminanceTop =  mLuminanceTop[highestIndex];
//
//    length = size;
//
//    inInitChromaKey = false;
//}



//void chromaKey::getChromaKeysBuffer(Mat mDss, Mat chromaKeyBuffer, int faceLeft, int faceTop, int faceRight, int faceBottom)
//{
//    Mat mDsd(mDss.size(), mDss.type());
//    Mat bDss;
//
////    setBrightnessAndContrast(mDss.data, mDsd.data);
//
////    blur(mDsd, bDss, Kernel_5X5);
//    blur(mDss, bDss, Kernel_5X5);
//
//    uchar* dss = bDss.data;
//
//
//    //////////////////// Local field declaration
//    float luminance, saturation, hue;
//    bool isObject(false);
//    bool isFirsto = true;
//    ////////////////// image manipulation
//
//    for(int column = WIDTH, row, inner, index, indexChromaBuffer; column>0; column--) // index = ((row * WIDTH) + column) * 4
//    {
//        index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
//        indexChromaBuffer = (WIDTH * HEIGHT) - 1 - (column * 1);
//
//
////if(column == faceTop)
////{
////    int indd = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
////    for (row = HEIGHT - 1; row > 0; row -= increase, indd -= (WIDTH_X_4) * increase )
////    {
////        rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
////
////    }
////}
//
//
//        for (row = HEIGHT - 1; row > 0; row -= increase, index -= (WIDTH_X_4) * increase, indexChromaBuffer -= WIDTH * increase )
//        {
//
////            if(isColorInThreshold(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF))
////            {
////                chromaKeyBuffer.data[indexChromaBuffer] = 255;
////                continue;
////            }
////            else
////                chromaKeyBuffer.data[indexChromaBuffer] = 0;
//
//
//            rgbToHsl(dss[index + 2] & 0xFF, dss[index + 1] & 0xFF, dss[index] & 0xFF, &hue, &saturation, &luminance);
//
////            isObject = ! isInChromaKeys(hue, luminance, saturation) || isSkinColor(hue, saturation, luminance);
//            isObject = ! isInChromaKeys(hue, luminance, saturation);
////
////            isObject = isSkinColor(hue, saturation, luminance);
//
////            isObject = isSkinColor(hue, saturation, luminance) || ! isInChromaKeys(hue, luminance, saturation);
////            isObject = isSkinColor(hue, saturation, luminance);// || ! isInChromaKeys(hue, luminance, saturation);
////            isObject = true;//! isInChromaKeys(hue, luminance, saturation);
//
//
//            if(isObject)
//            {
//                mDss.data[index] = 0;
//                mDss.data[index + 1] = 255;
//                mDss.data[index + 2] = 0; // blue
//
//                chromaKeyBuffer.data[indexChromaBuffer] = 255;
//            }
//            else
//            {
//                mDss.data[index + 2] = 0;
//                mDss.data[index + 1] = 0;
//                mDss.data[index] = 255;    // red
//
//                chromaKeyBuffer.data[indexChromaBuffer] = 0;
//            }
//        }
//    }
//    mDsd.release();
//    bDss.release();
//}



//bool chromaKey::saveSkinColorHsl(uchar* dss, unsigned int faceLeft, unsigned int faceTop, unsigned int faceWidth, unsigned int faceHeight)
//{
//    if(skinHuesTop != NULL)
//    {
//        free(skinHuesTop);
//        free(skinHuesBot);
//
//        free(skinSaturationTop);
//        free(skinSaturationBot);
//
//        free(skinLuminanceTop);
//        free(skinLuminanceBot);
//    }
//
////    Mat mDss(iDss.size(), iDss.type());
////    uchar* dss = mDss.data;
////    setBrightness(dss, dss, BRIGHTNESS);
//
//    skinHuesTop = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinHuesBot = (float *) malloc(skinHslArrayLength * sizeof(float));
//
//    skinSaturationTop = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinSaturationBot = (float *) malloc(skinHslArrayLength * sizeof(float));
//
//    skinLuminanceTop = (float *) malloc(skinHslArrayLength * sizeof(float));
//    skinLuminanceBot = (float *) malloc(skinHslArrayLength * sizeof(float));
//
//    int* counter = (int *) malloc(skinHslArrayLength * sizeof(int));
//
//    skinArrayLength = 0;
//    unsigned int faceCenter, index, index2, c, r;
//    int size = 0;
//    float skin_hue, skin_sat, skin_lightness;
//    bool foundRange = false;
//    int i;
//
//    int red, green, blue;
//    float hueVal, saturationVal, luminanceVal;
//
//    int minRangeCount = 0.2f * faceHeight;
//
////    for(int ii = 0; ii < hslArrayLength; ii++)
////    {
////        counter[ii] = 0;
////        hue_values[ii] = 0;
////        saturation_values[ii] = 0;
////        lightness_values[ii] = 0;
////    }
//
//    faceCenter = faceTop -  (faceHeight) * 0.2f;  //column
//    int leftToScan = faceLeft + 0.2*faceHeight;
//    c = faceCenter;    // = column
//
//    index = (leftToScan * WIDTH * 4)+(c * 4);
//    int lengthToScan = faceLeft + (faceHeight * 0.7f);
//
//    for ( r = leftToScan; r < lengthToScan; r++, index += WIDTH_X_4)//, x++ | x axis   //#   && index < MAX_SIZE * 4 - 3
//    {
////        dss[index] = 255;     //b
////        dss[index + 1] = 255; //g
////        dss[index + 2] = 0;   //r
//
//        red = dss[index];
//        green = dss[index + 1];
//        blue = dss[index + 2];
//
//        rgbToHsl(red & 0xFF, green & 0xFF, blue & 0xFF, &hueVal, &saturationVal, &luminanceVal);
//
//        foundRange = false;
//
//        for(i = 0; i < size; i++)
//        {
//            if( hueVal > skinHuesBot[i] && hueVal < skinHuesTop[i]
//             && saturationVal > skinSaturationBot[i] && saturationVal < skinSaturationTop[i]
//             && luminanceVal  > skinLuminanceBot[i]  && luminanceVal  < skinLuminanceTop[i] )
//            {
//                break;
//            }
//            counter[i]++;
//        }
//
//        if(size + 2 >= skinHslArrayLength) // +2 is for this run and for the next run
//        {
//    // too many ranges found in face -> face does not found correctly
////            free(skinHuesTop);
////            free(skinHuesBot);
////
////            free(skinSaturationTop);
////            free(skinSaturationBot);
////
////            free(skinLuminanceTop);
////            free(skinLuminanceBot);
//
//            free(counter);
//            skinArrayLength = 0;
//
//            return false;
//        }
//
//        if(i == size)
//        {
//            size++;
//
//            skinHuesTop[i] = hueVal + SKIN_HUE_THRESHOLD;
//            skinHuesBot[i] = hueVal - SKIN_HUE_THRESHOLD;
//
//            skinSaturationTop[i] = saturationVal + SKIN_SATURATION_THRESHOLD;
//            skinSaturationBot[i] = saturationVal - SKIN_SATURATION_THRESHOLD;
//
//            skinLuminanceTop[i] = luminanceVal + SKIN_LUMINANCE_THRESHOLD;
//            skinLuminanceBot[i] = luminanceVal - SKIN_LUMINANCE_THRESHOLD;
//
//            counter[i] = 1;
//        }
//
////        i = 0;
////        if (count > 0)
////        {
////            while ((!foundRange && i < count) && (i <= hslArrayLength))
////            {
////
////                if (!(hueVal > hue_values[i] + SKIN_HUE_THRESHOLD ||
////                      hueVal < hue_values[i] - SKIN_HUE_THRESHOLD ||
////                      saturationVal > saturation_values[i] + SKIN_SATURATION_THRESHOLD ||
////                      saturationVal < saturation_values[i] - SKIN_SATURATION_THRESHOLD ||
////                      luminanceVal > lightness_values[i] + SKIN_LIGHTNESS_THRESHOLD ||
////                      luminanceVal < lightness_values[i] - SKIN_LIGHTNESS_THRESHOLD))
////                {
////                    counter[i]++;
////                    foundRange = true;
////
////                    if (counter[i] == minRangeCount)
////                    {
////                        skin_hue = hue_values[i];
////                        skin_sat = saturation_values[i];
////                        skin_lightness = lightness_values[i];
////                        //                         return;
////                    }
////                }
////                i++;
////            }
////        }
////
////        if (!foundRange || count == 0){
////            count++;
////            hue_values[i] = hueVal;
////            saturation_values[i] = saturationVal;
////            lightness_values[i] = luminanceVal;
////        }
//
//
//    }
//
//
//    int topIndex = -1, topCount = 0;
//    for(i = 0; i < size; i++)
//    {
//        if (counter[i] >= minRangeCount && topCount < counter[i])
//        {
//            topCount = counter[i];
//            topIndex = i;
//        }
//    }
//
//    free(counter);
//    if(topIndex == -1)
//    {
////        free(skinHuesTop);
////        free(skinHuesBot);
////
////        free(skinSaturationTop);
////        free(skinSaturationBot);
////
////        free(skinLuminanceTop);
////        free(skinLuminanceBot);
//
//        skinArrayLength = 0;
//        return false;
//    }
//    else
//    {
//        skinArrayLength = size;
//
//        skinHueValue = skinHuesTop[topIndex] - SKIN_HUE_THRESHOLD;
//        skinSaturationValue = skinSaturationTop[topIndex] - SKIN_SATURATION_THRESHOLD;
//        skinLightnessValue = skinLuminanceTop[topCount] - SKIN_LUMINANCE_THRESHOLD;
//        foundSkinColor = true;
//        return true;
//    }
//
//}








//bool chromaKey::saveSkinColorHsl(uchar* bg, uchar* dss,  unsigned int faceLeft, unsigned int faceTop, unsigned int faceWidth, unsigned int faceHeight){
//    
//    unsigned int faceCenter, index, index2, c, r;
//    int count = 0;
//    
//    float hue_values[50];
//    float saturation_values[50];
//    float lightness_values[50];
//    int counter[50];
//    
//    float skin_hue, skin_sat, skin_lightness;
//    bool foundRange = false;
//    int i=0;
//    
//    int red, green, blue;
//    float hueVal, saturationVal, luminanceVal;
//    
//    for(int ii = 0; ii < 50; ii++){
//        counter[ii] = 0;
//        hue_values[ii]=0;
//        saturation_values[ii]=0;
//        lightness_values[ii]=0;
//    }
//    
//    
//    faceCenter = faceTop -  (faceHeight)*0.2;  //column
//    c = faceCenter;    // = column
//    index = ((faceLeft + 40) * WIDTH*4)+(c * 4);
//    index2 = (faceLeft * WIDTH) + c;
//    
//    //  for ( r = faceLeft + 40 ; r < faceLeft + (faceHeight*0.7)  && index < MAX_SIZE*4 - 3 ; r++, index2 += WIDTH, index += WIDTH_X_4)//, x++ | x axis
//    //    {
//    //      bg[index] = 255;  //b
//    //      bg[index + 1] = 255; //g
//    //      bg[index + 2] = 0; //r
//    
//    //  }
//    
//    printf("aaabbb////////////////\n");
//    
//    for ( r = faceLeft + 40 ; r < faceLeft + (faceHeight*0.7)  && index < MAX_SIZE*4 - 3 ; r++, index2 += WIDTH, index += WIDTH_X_4)//, x++ | x axis
//    {
//        bg[index] = 255;  //b
//        bg[index + 1] = 255; //g
//        bg[index + 2] = 0; //r
//        
//        red = dss[index];
//        green = dss[index + 1];
//        blue = dss[index + 2];
//        
//        rgbToHsl(red & 0xFF, green & 0xFF, blue & 0xFF, &hueVal, &saturationVal, &luminanceVal);
//        
//        printf("aaa.(r,g,b) = %d, %d, %d\n" , red, green, blue);
//        
//        printf("bbb.(h,s,l) = %f, %f, %f\n" , hueVal, saturationVal*100, (luminanceVal/255)*100);
//        
//        foundRange = false;
//        i = 0;
//        if (count > 0){
//            while ((!foundRange && i < count) && (i <= 50)){
//                if (!(hueVal > hue_values[i] + SKIN_HUE_THRESHOLD ||
//                      hueVal < hue_values[i] - SKIN_HUE_THRESHOLD ||
//                      saturationVal > saturation_values[i] + SKIN_SATURATION_THRESHOLD ||
//                      saturationVal < saturation_values[i] - SKIN_SATURATION_THRESHOLD ||
//                      luminanceVal > lightness_values[i] + SKIN_LIGHTNESS_THRESHOLD ||
//                      luminanceVal < lightness_values[i] - SKIN_LIGHTNESS_THRESHOLD
//                      )){
//                    counter[i]++;
//                    foundRange = true;
//                    
//                    if (counter[i] == 50){
//                        skin_hue = hue_values[i];
//                        skin_sat = saturation_values[i];
//                        skin_lightness = lightness_values[i];
//                        //                         return;
//                    }
//                }
//                i++;
//            }
//        }
//        
//        if (!foundRange || count == 0){
//            count++;
//            hue_values[i] = hueVal;
//            saturation_values[i] = saturationVal;
//            lightness_values[i] = luminanceVal;
//        }
//    }
//    printf("bbbaaa/////////////////\n");
//    
//    // //////////////////
//    //        int ccc = 0;
//    //
//    //
//    //    printf("ccc/////////////////\n");
//    //        //draw line for calculate skin color range
//    //        int WIDTH = 640, HEIGHT = 480;
//    //        for (int r = 0; r < HEIGHT; r++)
//    //        {
//    //            index = WIDTH * r * 4;
//    //            index2 = WIDTH * r;
//    //            for (int c = 0; c < WIDTH; c++, index += 4, index2++)
//    //            {
//    //
//    //                if(faceRight!=0)
//    //                {
//    //                    if (r == faceTop+50 && c >= faceCenter - 200 && c < faceCenter )
//    //                    {
//    //                        ccc++;
//    //                        //green
//    //                        bg[index] = 70;
//    //                        bg[index + 1] = 255;
//    //                        bg[index + 2] = 70;
//    //
//    //                        printf("(%d) : ccc: (%d, %d, %d)\n", ccc, dss[index + 2], dss[index + 1], dss[index]);
//    //                    }
//    //                }
//    //            }
//    //        }
//    //
//    //    printf("ccc/////////////////\n");
//    //    ///////////////
//    
//    
//    for(i = 0; i < 50; i++){
//        if (counter[i] > 49){
//            skin_hue = hue_values[i];
//            skin_sat = saturation_values[i];
//            skin_lightness = lightness_values[i];
//            
//            skinHueValue = skin_hue;
//            skinSaturationValue = skin_sat;
//            skinLightnessValue = skin_lightness;
//            foundSkinColor = true;
//            return true;
//        }
//    }
//    
//    printf("skinColor = (%f , %f , %f)\n", skinHueValue, skinSaturationValue*100, (skinLightnessValue/255)*100);
//    
//    return false;
//    
//}




//////////////////////////////////////////////////////////////////////

//bool chromaKey::findChromaKeys(Mat mDss, int faceLeft, int faceTop, int faceRight, int faceBottom)
//{
//    if(inInitChromaKey)
//        return;
//    inInitChromaKey = true;
//
////    int left, top, right, bottom;
//
////    int padding = (int)(((float)(faceRight - faceLeft)) * 0.4f);
//    int padding = (int)(((float)(faceBottom - faceTop)) * 0.4f);
////    int padding = (int)(((float)(faceTop - faceBottom)) * 0.4f);
////    int paddingHalf = (int)(((float)(faceRight - faceLeft)) * 0.2f);
//    int paddingHalf = (int)(((float)(faceTop - faceBottom)) * 0.2f);
//
////    int bottomTopArea = faceTop + padding;
//    int bottomTopArea = faceBottom + padding;
//
////    int bottomBottomAreas = faceBottom;
//////    int bottomBottomAreas = faceBottom;
////    int rightBorderLeftArea = faceLeft - paddingHalf;
//////    int rightBorderLeftArea = faceLeft - paddingHalf;
////    int leftBorderRightArea = faceRight + paddingHalf;
////    int leftBorderRightArea = faceRight + paddingHalf;
//
//
//
//
//
////    setBrightness(mDss.data, mDss.data, userBrightness);
//
//    setBrightness(mDss.data, mDss.data, BRIGHTNESS);
//
//    Mat bDss;
//
//    blur(mDss, bDss, Kernel_5X5);
////    blur(bDss, bDss, Size(5, 5));
//
//    uchar* dss = bDss.data;
//    ////////////////// Init chroma key
//
//    float blue, green, red, min, max, saturation = 0, luminance = 0, hue = 0;
////    int index;
//
//    float* huesTops = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//    float* huesBots = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//
//    float* saturationTops = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//    float* saturationBots = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//
//    float* luminanceTops = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//    float* luminanceBots = (float*)malloc(ARRAY_LENGTH * sizeof(float));
//
//    int* counts = (int*)malloc(ARRAY_LENGTH * sizeof(int));
//
//    int inner;
//    int size = 0;
//
//
//    ///////////////////////////DETECTION////////////////////////////////////
//
//
//
////    for(int c = src.cols - top, cDst = dst.cols, indexSrc, indexDst, channels = src.channels(); c >= src.cols - bottom; c--, cDst--)// && cDst > 0
////    {
////        indexSrc = c * channels;
////        indexDst = cDst * channels;
////        for (int r = left, rDst = 0; r <= right; r++, rDst++, indexSrc += incSrc, indexDst += incDst)//, indexMask += widthMask) && rDst < dst.rows
////        {
////            dstData[indexDst] = srcData[indexSrc];
////            dstData[indexDst + 1] = srcData[indexSrc + 1];
////            dstData[indexDst + 2] = srcData[indexSrc + 2];
////        }
////    }
//
//    if(faceLeft != 0)
//    {
//        for (int c = WIDTH - 2, indexSrc, channels = mDss.channels(), incSrc = WIDTH * channels; c >= bottomTopArea; c--)
//        {
//            indexSrc = c * channels + incSrc + incSrc;
//            for (int r = 2; r <= HEIGHT; r++, indexSrc += incSrc)
//            {
//                rgbToHsl(dss[indexSrc + 2] & 0xFF, dss[indexSrc + 1] & 0xFF, dss[indexSrc] & 0xFF, &hue, &saturation, &luminance);
//                //////////////Hues ...
//                for (inner = 0; inner < size; inner++)
//                {
//                    if (huesBots[inner] <= hue && hue <= huesTops[inner]
//                        && saturationBots[inner] <= saturation && saturation <= saturationTops[inner]
//                        && luminanceBots[inner] <= luminance && luminance <= luminanceTops[inner]
//                            )
//                    {
//                        counts[inner]++;
//                        break;
//                    }
//                }
//
//                if (inner == size)
//                {
//                    huesTops[size] = hue + HUE_THRESHOLD;
//                    huesBots[size] = hue - HUE_THRESHOLD;
//
//                    saturationTops[size] = saturation + SATURATION_THRESHOLD;
//                    saturationBots[size] = saturation - SATURATION_THRESHOLD;
//
//                    luminanceTops[size] = luminance + LUMINANCE_THRESHOLD;
//                    luminanceBots[size] = luminance - LUMINANCE_THRESHOLD;
//
//                    counts[size] = 1;
//                    size++;
//
//                    if (size >= ARRAY_LENGTH - 2)
//                    {
//                        break;
//                    }
//                }
//
////                mDss.data[indexSrc + 2] = 255;
////                mDss.data[indexSrc + 1] = 125;
////                mDss.data[indexSrc] = 0;
//            }
////
//            if (size >= ARRAY_LENGTH - 2)
//            {
//                break;
//            }
//        }
//
////        if(faceBottom < 0)
////            faceBottom = 0;
//
////        for (int c = bottomTopArea, indexSrc, channels = mDss.channels(), incSrc = WIDTH * channels; c >= faceBottom; c--)// && cDst > 0
////        {
////            indexSrc = c * channels;
////
////            for (int r = 0; r <= faceLeft; r++, indexSrc += incSrc)//, indexMask += widthMask) && rDst < dst.rows
////            {
//////            dstData[indexDst] = srcData[indexSrc];
//////            dstData[indexDst + 1] = srcData[indexSrc + 1];
//////            dstData[indexDst + 2] = srcData[indexSrc + 2];
//////                mDss.data[indexSrc + 2] = 255;
//////                mDss.data[indexSrc + 1] = 255;
//////                mDss.data[indexSrc] = 255;
////            }
////        }
////
////        for(int c = bottomTopArea, indexSrc, channels = mDss.channels(), incSrc = WIDTH * channels; c >= faceBottom; c--)// && cDst > 0
////        {
////            indexSrc = (faceRight * WIDTH * channels) + c * channels;
////
////
////            for (int r = faceRight; r <= HEIGHT; r++, indexSrc += incSrc)//, indexMask += widthMask) && rDst < dst.rows
////            {
////    //            dstData[indexDst] = srcData[indexSrc];
////    //            dstData[indexDst + 1] = srcData[indexSrc + 1];
////    //            dstData[indexDst + 2] = srcData[indexSrc + 2];
//////                mDss.data[indexSrc + 2] = 255;
//////                mDss.data[indexSrc + 1] = 125;
//////                mDss.data[indexSrc] = 0;
////            }
////        }
//
//    }
//
//    ////////////////////////////////////////////////////////
//
//    if(mHuesBot != NULL)
//    {
//        free(mHuesBot);
//        free(mSaturationBot);
//        free(mSaturationTop);
//        free(mHuesTop);
//        free(mLuminanceBot);
//        free(mLuminanceTop);
//    }
//    mHuesBot = (float*)malloc(size * sizeof(float));
//    mHuesTop = (float*)malloc(size * sizeof(float));
//
//
//    mSaturationBot = (float*)malloc(size * sizeof(float));
//    mSaturationTop = (float*)malloc(size * sizeof(float));
//
//    mLuminanceBot = (float*)malloc(size * sizeof(float));
//    mLuminanceTop = (float*)malloc(size * sizeof(float));
//
//    int highestIndex = 0, highestCount = 0;
//
//    for(inner = 0; inner < size; inner++)
//    {
//        mHuesTop[inner] = huesTops[inner];
//        mHuesBot[inner] = huesBots[inner];
//
//        mSaturationTop[inner] = saturationTops[inner];
//        mSaturationBot[inner] = saturationBots[inner];
//
//        mLuminanceTop[inner] = luminanceTops[inner];
//        mLuminanceBot[inner] = luminanceBots[inner];
//
//        if(counts[inner] > highestCount)
//        {
//            highestCount = counts[inner];
//            highestIndex = inner;
//        }
//    }
//
//    hueBot = mHuesBot[highestIndex];
//    hueTop = mHuesTop[highestIndex];
//
//    saturationBottom = mSaturationBot[highestIndex];
//    saturationTop =  mSaturationTop[highestIndex];
//
//    luminanceBottom =  mLuminanceBot[highestIndex];
//    luminanceTop =  mLuminanceTop[highestIndex];
//
//    length = size;
//
//    inInitChromaKey = false;
//}


//////////////////////////////////////////////////////////////////////





////        mHuesBot = new int[foundHues.size()];
////        mLuminanceBot = new int[foundHues.size()];
////        mSaturationBot = new int[foundHues.size()];
////
////        mHuesTop = new int[foundHues.size()];
////        mLuminanceTop = new int[foundHues.size()];
////        mSaturationTop = new int[foundHues.size()];
////
////        index = 0;
////
////        for(int key:foundHues.keySet())
////        {
////            mHuesBot[index] = key - 12;
////            mHuesTop[index] = key + 12;
////
////            saturation = (int) (foundSaturation.get(key) / foundHues.get(key));
////            mSaturationBot[index] = (int) (saturation - 35000);
////            mSaturationTop[index] = (int) (saturation + 45000);
////
////            luminance = foundLuminance.get(key) / foundHues.get(key);
////
////            mLuminanceBot[index] = (int) (luminance - 240);
////            mLuminanceTop[index] = (int) (luminance + 240);
////
////            index++;
////
////            System.out.println(">>>>toc>>>>>initChromaKey2>>>>"+key+">>>"+saturation+">>>>"+luminance);
////
//////            if(foundHues.get(key) > highestHue)
//////            {
//////                highestHue = foundHues.get(key);
//////                saturation = (int) (foundSaturation.get(key) / highestHue);
//////                luminance = (int) (foundLuminance.get(key) / highestHue);
//////                highestKey = key;
//////            }
//////            System.out.println(">>>>tic>>>>toc?>>>>>???>foundHues>>>"+key+">>>"+foundHues.get(key));
////
////        }
////        mHues = hues;
//
////        mHues[0]=highestKey;
////
////        hueTop = highestKey + 12;
////        hueBot = highestKey - 12; // kind of 45 degrees, 22.5 on each side, 23 is set to use integer
////
////        saturationTop = saturation + 4000;
////        saturationBottom = saturation - 4000;
////
////        luminanceTop    = luminance + 20;
////        luminanceBottom = luminance - 20;
//
////    System.out.println(">>>>toc>>>>>initChromaKey24>>>>"+hue+">>>"+saturation+">>>>"+luminance);
//
////        for(int key:foundSaturation.keySet())
////        {
////            if(foundSaturation.get(key) > highestSaturation)
////            {
////                highestSaturation = foundSaturation.get(key);
////                highestKey = key;
////            }
////        }
////        if(saturation >= 0)
//    {
////            activity.saturationTop = saturation + 5000;
////            activity.saturationBottom = saturation - 5000;
//    }
//
////        System.out.println(">>>>toc>>>>>initChromaKey3>>>>"+activity.saturationTop+">>>"+activity.saturationBottom);
//
//
////        else
////        {
////            saturationTop = saturation - 6000;
////            saturationBottom = saturation + 6000;
////        }
//
////        for(int key:foundLuminance.keySet())
////        {
////            if(foundLuminance.get(key) > highestLuminance)
////            {
////                highestLuminance = foundLuminance.get(key);
////                highestKey = key;
////            }
////
////            System.out.println(">>>>tic>>>>toc?>>>>>???>foundLuminance>>>"+key+">>>"+foundLuminance.get(key));
////        }
////        luminanceTop    = luminance + 50;
////        luminanceBottom = luminance - 50;
//
//
////        selectedBlue  = (inBuffer[2]) & 0xFF;
////        selectesGreen = (inBuffer[1] >> 8) & 0xFF;
////        selectedRed   = (inBuffer[0] >> 16) & 0xFF;



//////////////////////////////////////////////////////////////////////////////

//////////////// SKIN COLORS //////////////////
//
//                if(255 - SKIN_COLOR_THRESHOLD <= red && red <= 255
//                        && 224 - SKIN_COLOR_THRESHOLD <= green && green <= 224 + SKIN_COLOR_THRESHOLD
//                        && 189 - SKIN_COLOR_THRESHOLD <= blue && blue == 189 + SKIN_COLOR_THRESHOLD)
//                {
//
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//                if(255 - SKIN_COLOR_THRESHOLD <= red && red <= 255
//                        && 205 - SKIN_COLOR_THRESHOLD <= green && green <= 205 + SKIN_COLOR_THRESHOLD
//                        && 148 - SKIN_COLOR_THRESHOLD <= blue && blue == 148 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//                if(234 - SKIN_COLOR_THRESHOLD <= red && red <= 234
//                        && 192 - SKIN_COLOR_THRESHOLD <= green && green <= 192 + SKIN_COLOR_THRESHOLD
//                        && 134 - SKIN_COLOR_THRESHOLD <= blue && blue == 134 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//
//                if(255 - SKIN_COLOR_THRESHOLD <= red && red <= 255
//                        && 173 - SKIN_COLOR_THRESHOLD <= green && green <= 173 + SKIN_COLOR_THRESHOLD
//                        && 96 - SKIN_COLOR_THRESHOLD <= blue && blue == 96 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//                if(255 - SKIN_COLOR_THRESHOLD <= red && red <= 255
//                        && 227 - SKIN_COLOR_THRESHOLD <= green && green <= 227 + SKIN_COLOR_THRESHOLD
//                        && 159 - SKIN_COLOR_THRESHOLD <= blue && blue == 159 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//
//
//
//                ////From hen
//
//                if(187 - SKIN_COLOR_THRESHOLD <= red && red <= 187
//                        && 135 - SKIN_COLOR_THRESHOLD <= green && green <= 135 + SKIN_COLOR_THRESHOLD
//                        && 111 - SKIN_COLOR_THRESHOLD <= blue && blue == 111 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//
//                if(221 - SKIN_COLOR_THRESHOLD <= red && red <= 221
//                        && 183 - SKIN_COLOR_THRESHOLD <= green && green <= 183 + SKIN_COLOR_THRESHOLD
//                        && 160 - SKIN_COLOR_THRESHOLD <= blue && blue == 160 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//
//                if(242 - SKIN_COLOR_THRESHOLD <= red && red <= 242
//                        && 214 - SKIN_COLOR_THRESHOLD <= green && green <= 214 + SKIN_COLOR_THRESHOLD
//                        && 203 - SKIN_COLOR_THRESHOLD <= blue && blue == 203 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//
//                if(116 - SKIN_COLOR_THRESHOLD <= red && red <= 116
//                        && 69 - SKIN_COLOR_THRESHOLD <= green && green <= 69 + SKIN_COLOR_THRESHOLD
//                        && 61 - SKIN_COLOR_THRESHOLD <= blue && blue == 61 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//
//                if(166 - SKIN_COLOR_THRESHOLD <= red && red <= 166
//                        && 115 - SKIN_COLOR_THRESHOLD <= green && green <= 115 + SKIN_COLOR_THRESHOLD
//                        && 88 - SKIN_COLOR_THRESHOLD <= blue && blue == 88 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//
//                if(206 - SKIN_COLOR_THRESHOLD <= red && red <= 206
//                        && 150 - SKIN_COLOR_THRESHOLD <= green && green <= 150 + SKIN_COLOR_THRESHOLD
//                        && 125 - SKIN_COLOR_THRESHOLD <= blue && blue == 125 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//
//                if(92 - SKIN_COLOR_THRESHOLD <= red && red <= 92
//                        && 57 - SKIN_COLOR_THRESHOLD <= green && green <= 57 + SKIN_COLOR_THRESHOLD
//                        && 55 - SKIN_COLOR_THRESHOLD <= blue && blue == 55 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//
//                if(173 - SKIN_COLOR_THRESHOLD <= red && red <= 173
//                        && 100 - SKIN_COLOR_THRESHOLD <= green && green <= 100 + SKIN_COLOR_THRESHOLD
//                        && 83 - SKIN_COLOR_THRESHOLD <= blue && blue == 83 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//
//                if(170 - SKIN_COLOR_THRESHOLD <= red && red <= 170
//                        && 129 - SKIN_COLOR_THRESHOLD <= green && green <= 129 + SKIN_COLOR_THRESHOLD
//                        && 111 - SKIN_COLOR_THRESHOLD <= blue && blue == 111 + SKIN_COLOR_THRESHOLD)
//                {
//                    outBuffer[index] = inBuffer[index];
//                    outBuffer[index + 1] = inBuffer[index + 1];
//                    outBuffer[index + 2] = inBuffer[index + 2];
//                    continue;
//                    //throw new RuntimeException();
//                }
//                if(red == 255 && green == 205 && blue == 148)
//                {
//                    throw new RuntimeException();
//                }

//                if(red == 234 && green == 192 && blue == 134)
//                {
//                    throw new RuntimeException();
//                }

//                if(red == 255 && green == 173 && blue == 96)
//                {
//                    throw new RuntimeException();
//                }
//
//                if(red == 255 && green == 227 && blue == 159)
//                {
//                    throw new RuntimeException();
//                }
//                else System.out.println("Color>>>>>"+red+">>"+green+">>"+blue);

//                /*
//                (255,224,189)
//#ffcd94	(255,205,148)
//#eac086	(234,192,134)
//#ffad60	(255,173,96)
//#ffe39f	(255,227,159)
//                 */
//
//                /**
//                 *  187,135,111
//                 *  221,183,160
//                 *  242,214,203
//                 *  116,69,61
//                 *  166,115,88
//                 *  206,150,125
//                 *  92,57,55
//                 *  173,100,83
//                 *  170,129,111
//                 *
//                 *
//                 *
//                 */




////////////////////////////////////////////////////////////






//                if(isFirsto)
//                {
//                    lastSaturation = saturation;
//                    lastLuminance = luminance;
//                    lastHue = hue;
//                    isFirsto = false;
//                    continue;
//                }
////
////                else
//                {
//                    //                if (
//////                            hue == hues[inner]
////                    hue < hueTop && hueBot < hue
////                            && luminance < luminanceTop && luminanceBot < luminance
////                            && saturation < saturationTop && saturationBot < saturation
////                        );
//
//                    if(
//                            (lastHue - HUE_DIF_THRESHOLD < hue && hue < lastHue + HUE_DIF_THRESHOLD
//                                    && lastSaturation - SATURATION_DIF_THRESHOLD < saturation && saturation < lastSaturation + SATURATION_DIF_THRESHOLD
//                                    && lastLuminance - LUMINANCE_DIF_THRESHOLD < luminance && luminance < lastLuminance + LUMINANCE_DIF_THRESHOLD)
////                            ||
////                          (hue < hueTop && hueBot < hue
////                            && luminance < luminanceTop && luminanceBot < luminance
////                            && saturation < saturationTop && saturationBot < saturation)
//                            )
//                    {
//                        outBuffer[index] = (byte) 255;
//                        outBuffer[index + 1] = (byte) 255;
//                        outBuffer[index + 2] = (byte) 255;
//                    }
//                    else
//                    {
//                        outBuffer[index] = (byte) 0;
//                        outBuffer[index + 1] = (byte) 0;
//                        outBuffer[index + 2] = (byte) 0;
//
////                        column++; index += 4;
//
//                        if(false)
//                            for (column+=1, index += 4; column < WIDTH; column++, index += 4)
//                            {
//                                ////////////// init hue for pixel
//
//                                blue = inBuffer[index + 2] & 0xFF;
//                                green = inBuffer[index + 1] & 0xFF;
//                                red = inBuffer[index] & 0xFF;
//
//                                min = (min = red < green ? red : green) < blue ? min : blue;
//                                max = (max = red > green ? red : green) > blue ? max : blue;
//
//                                if (max == min) hue = 0;
//                                else if (max == red)
//                                    hue = (green - blue) / (max - min);
//                                else if (max == green)
//                                    hue = 2f + ((blue - red) / (max - min));
//                                else hue = 4f + ((red - green) / (max - min));
//
//                                hue = hue * 60f;
//                                if (hue < 0) hue = hue + 360f;
//
//                                ////////////// end init hue for pixel
//
//                                // find luminance and saturation
//
//                                luminance = (min + max) / 2;
//                                if (luminance < 125f)//if(luminance < 0.5f)
//                                    saturation = (max - min) * (max + min);
//                                else saturation = (max - min) * (510 - max - min);
//
//                                if(lastHue - HUE_2_THRESHOLD < hue && hue < lastHue + HUE_2_THRESHOLD
//                                        && lastSaturation - SATURATION_2_THRESHOLD < saturation && saturation < lastSaturation + SATURATION_2_THRESHOLD
//                                        && lastLuminance - LUMINANCE_2_THRESHOLD < luminance && luminance < lastLuminance + LUMINANCE_2_THRESHOLD
////                                    || (hue < hueTop && hueBot < hue
////                                            && luminance < luminanceTop && luminanceBot < luminance
////                                            && saturation < saturationTop && saturationBot < saturation)
//                                        )
//                                {
////                                column++; index += 4;
//                                    outBuffer[index] = (byte)  255;
//                                    outBuffer[index + 1] = (byte) 255;
//                                    outBuffer[index + 2] = (byte) 255;
//                                    break;
//                                }
//                                else
//                                {
////                                outBuffer[index] = inBuffer[index];
////                                outBuffer[index + 1] = inBuffer[index + 1];
////                                outBuffer[index + 2] = inBuffer[index + 2];
//                                    outBuffer[index] = (byte) 255;
//                                    outBuffer[index + 1] = 0;
//                                    outBuffer[index + 2] = (byte) 0;
//                                }
//                            }
//
//
//
//                    }
//
////                    if(column < WIDTH)
////                    {
////                        column++; index += 4;
////                        outBuffer[index] = (byte) 0;
////                        outBuffer[index + 1] = 0;
////                        outBuffer[index + 2] = (byte) 0;
////                    }
//                    lastSaturation = saturation;
//                    lastLuminance = luminance;
//                    lastHue = hue;
//                    if(true)
//                        continue;
//                }

///////////////////////////////////////////////////////////////





//if(true)
//continue;

//                if (column > 0)
//                {
//                    index -= byteIncrease;
//                    column -= increase;
//                }
//
//                for (; column < WIDTH; column++, index += 4)
//                {
//                    ////////////// init hue for pixel
//
//                    blue = inBuffer[index + 2] & 0xFF;
//                    green = inBuffer[index + 1] & 0xFF;
//                    red = inBuffer[index] & 0xFF;
//
//                    min = (min = red < green ? red : green) < blue ? min : blue;
//                    max = (max = red > green ? red : green) > blue ? max : blue;
//
//                    if (max == min) hue = 0;
//                    else if (max == red)
//                        hue = (green - blue) / (max - min);
//                    else if (max == green)
//                        hue = 2f + ( (blue - red) / (max - min));
//                    else hue = 4f + ((red - green) / (max - min));
//
//                    hue = hue * 60f;
//                    if (hue < 0) hue = hue + 360f;
//
//                    ////////////// end init hue for pixel
//
//                    // find luminance and saturation
//
////                        luminance = (min + max ) / 2;
////                        if(luminance < 125f)//if(luminance < 0.5f)
////                            saturation = (max - min)*(max + min);
////                        else saturation = (max - min)*(510 - max - min);//  2 * 255 //saturation = (max - min)*(2 - max - min);
//
//                    luminance = (red + green + blue)/3f;
//
//                    if(min == max) // c = 0
//                        saturation = 0;
//                    else
//                        saturation = 1 - (min / luminance);
//
//                    ////////////// end find luminance and saturation
//
//
////                        if(red < COLOR_THRESHOLD && green < COLOR_THRESHOLD && blue < COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////
////                        else if(255 - SKIN_COLOR_THRESHOLD <= red && red <= 255
////                                && 224 - SKIN_COLOR_THRESHOLD <= green && green <= 224 + SKIN_COLOR_THRESHOLD
////                                && 189 - SKIN_COLOR_THRESHOLD <= blue && blue == 189 + SKIN_COLOR_THRESHOLD)
////                        {
////
////                            found = true;
////                        }
////                        else if(255 - SKIN_COLOR_THRESHOLD <= red && red <= 255
////                                && 205 - SKIN_COLOR_THRESHOLD <= green && green <= 205 + SKIN_COLOR_THRESHOLD
////                                && 148 - SKIN_COLOR_THRESHOLD <= blue && blue == 148 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////                        else if(234 - SKIN_COLOR_THRESHOLD <= red && red <= 234
////                                && 192 - SKIN_COLOR_THRESHOLD <= green && green <= 192 + SKIN_COLOR_THRESHOLD
////                                && 134 - SKIN_COLOR_THRESHOLD <= blue && blue == 134 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////
////                        else if(255 - SKIN_COLOR_THRESHOLD <= red && red <= 255
////                                && 173 - SKIN_COLOR_THRESHOLD <= green && green <= 173 + SKIN_COLOR_THRESHOLD
////                                && 96 - SKIN_COLOR_THRESHOLD <= blue && blue == 96 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////                        else if(255 - SKIN_COLOR_THRESHOLD <= red && red <= 255
////                                && 227 - SKIN_COLOR_THRESHOLD <= green && green <= 227 + SKIN_COLOR_THRESHOLD
////                                && 159 - SKIN_COLOR_THRESHOLD <= blue && blue == 159 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////
////
////
////                        ////From hen
////
////                        else if(187 - SKIN_COLOR_THRESHOLD <= red && red <= 187
////                                && 135 - SKIN_COLOR_THRESHOLD <= green && green <= 135 + SKIN_COLOR_THRESHOLD
////                                && 111 - SKIN_COLOR_THRESHOLD <= blue && blue == 111 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////
////                        else if(221 - SKIN_COLOR_THRESHOLD <= red && red <= 221
////                                && 183 - SKIN_COLOR_THRESHOLD <= green && green <= 183 + SKIN_COLOR_THRESHOLD
////                                && 160 - SKIN_COLOR_THRESHOLD <= blue && blue == 160 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////
////                        else if(242 - SKIN_COLOR_THRESHOLD <= red && red <= 242
////                                && 214 - SKIN_COLOR_THRESHOLD <= green && green <= 214 + SKIN_COLOR_THRESHOLD
////                                && 203 - SKIN_COLOR_THRESHOLD <= blue && blue == 203 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////
////                        else if(116 - SKIN_COLOR_THRESHOLD <= red && red <= 116
////                                && 69 - SKIN_COLOR_THRESHOLD <= green && green <= 69 + SKIN_COLOR_THRESHOLD
////                                && 61 - SKIN_COLOR_THRESHOLD <= blue && blue == 61 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////
////                        else if(166 - SKIN_COLOR_THRESHOLD <= red && red <= 166
////                                && 115 - SKIN_COLOR_THRESHOLD <= green && green <= 115 + SKIN_COLOR_THRESHOLD
////                                && 88 - SKIN_COLOR_THRESHOLD <= blue && blue == 88 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////
////                        else if(206 - SKIN_COLOR_THRESHOLD <= red && red <= 206
////                                && 150 - SKIN_COLOR_THRESHOLD <= green && green <= 150 + SKIN_COLOR_THRESHOLD
////                                && 125 - SKIN_COLOR_THRESHOLD <= blue && blue == 125 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////
////                        else if(92 - SKIN_COLOR_THRESHOLD <= red && red <= 92
////                                && 57 - SKIN_COLOR_THRESHOLD <= green && green <= 57 + SKIN_COLOR_THRESHOLD
////                                && 55 - SKIN_COLOR_THRESHOLD <= blue && blue == 55 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////
////                        else if(173 - SKIN_COLOR_THRESHOLD <= red && red <= 173
////                                && 100 - SKIN_COLOR_THRESHOLD <= green && green <= 100 + SKIN_COLOR_THRESHOLD
////                                && 83 - SKIN_COLOR_THRESHOLD <= blue && blue == 83 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
////
////                        else if(170 - SKIN_COLOR_THRESHOLD <= red && red <= 170
////                                && 129 - SKIN_COLOR_THRESHOLD <= green && green <= 129 + SKIN_COLOR_THRESHOLD
////                                && 111 - SKIN_COLOR_THRESHOLD <= blue && blue == 111 + SKIN_COLOR_THRESHOLD)
////                        {
////                            found = true;
////                        }
//
//
//                    ////////////// Manipulate the pixel
//
//                    found = false;
//                    for(inner = 0;inner<huesLength; inner++)
//                    {
//                        if(huesBot[inner] < hue && hue < huesTop[inner]
//                           && saturationsBot[inner] < saturation && saturation < saturationsTop[inner]
//                           && luminancesBot[inner] < luminance && luminance < luminancesTop[inner]
//                                )
//                        {
//                            found = true;
//                            break;
//                        }
//                    }
//                    if(!found)
//                        break;
////                        if (hue <= hueTop && hueBot <= hue
////                                && luminance < luminanceTop && luminanceBot < luminance
////                                && saturation < saturationTop && saturationBot < saturation
////                                )
////                            ;//setLittleEndian(buffer, Color.RED, i);
////                        else break;
//                }
//
//                for (; column < WIDTH; column++, index += 4)
//                {
//                    ////////////// init hue for pixel
//
//                    blue = inBuffer[index + 2] & 0xFF;
//                    green = inBuffer[index + 1] & 0xFF;
//                    red = inBuffer[index] & 0xFF;
//
//                    min = (min = red < green ? red : green) < blue ? min : blue;
//                    max = (max = red > green ? red : green) > blue ? max : blue;
//
//                    if (max == min) hue = 0;
//                    else if (max == red)
//                        hue = ( (green - blue) /  (max - min));
//                    else if (max == green)
//                        hue = 2f + ((blue - red) / (max - min));
//                    else hue = 4f + ((red - green) / (max - min));
//
//                    hue = hue * 60f;
//                    if (hue < 0) hue = hue + 360f;
//
//                    ////////////// end init hue for pixel
//
//                    // find luminance and saturation
//
////                        luminance = (min + max ) / 2;
////                        if(luminance < 125f)//if(luminance < 0.5f)
////                            saturation = (max - min)*(max + min);
////                        else saturation = (max - min)*(510 - max - min);//  2 * 255 //saturation = (max - min)*(2 - max - min);
//
//                    luminance = (red + green + blue)/3f;
//
//                    if(min == max) // c = 0
//                        saturation = 0;
//                    else
//                        saturation = 1 - (min / luminance);
//
//                    ////////////// end find luminance and saturation
//
//                    ////////////// Manipulate the pixel
//                    found = false;
//                    for(inner = 0;inner<huesLength; inner++)
//                    {
//                        if(huesBot[inner] < hue && hue < huesTop[inner]
//                           && saturationsBot[inner] < saturation && saturation < saturationsTop[inner]
//                           && luminancesBot[inner] < luminance && luminance < luminancesTop[inner]
//                                )
//                        {
//                            found = true;
//                            break;
//                        }
//                    }
//                    if(!found)
//                    {
//                        outBuffer[index] = inBuffer[index];
//                        outBuffer[index + 1] = inBuffer[index + 1];
//                        outBuffer[index + 2] = inBuffer[index + 2];
////                            outBuffer[index + 3] = inBuffer[index + 3];
//
//
//
////                            lastLuminance = luminance; lastHue = hue; lastSaturation = saturation;
////                            for (; column < WIDTH; column++, index += 4)
////                            {
////                                ////////////// init hue for pixel
////
////                                blue = inBuffer[index + 2] & 0xFF;
////                                green = inBuffer[index + 1] & 0xFF;
////                                red = inBuffer[index] & 0xFF;
////
////                                min = (min = red < green ? red : green) < blue ? min : blue;
////                                max = (max = red > green ? red : green) > blue ? max : blue;
////
////                                if (max == min) hue = 0;
////                                else if (max == red)
////                                    hue = ((green - blue) / (max - min));
////                                else if (max == green)
////                                    hue = 2f + ((blue - red) / (max - min));
////                                else hue = 4f + ((red - green) / (max - min));
////
////                                hue = hue * 60f;
////                                if (hue < 0) hue = hue + 360f;
////
////                                ////////////// end init hue for pixel
////
////                                // find luminance and saturation
////
////                                luminance = (min + max) / 2f;
////                                if (luminance < 0.5f)
////                                    saturation = (max - min) * (max + min);
////                                else saturation = (max - min) * (2f - max - min);
////
////                                if(lastHue - HUE_THRESHOLD < hue && hue < lastHue + HUE_THRESHOLD
////                                        && lastSaturation - SATURATION_THRESHOLD < saturation && saturation < lastSaturation + SATURATION_THRESHOLD
////                                        && lastLuminance - LUMINANCE_THRESHOLD < luminance && luminance < lastLuminance + LUMINANCE_THRESHOLD)
////                                {
////                                    outBuffer[index] = inBuffer[index];
////                                    outBuffer[index + 1] = inBuffer[index + 1];
////                                    outBuffer[index + 2] = inBuffer[index + 2];
//////                                    outBuffer[index + 3] = inBuffer[index + 3];
////                                    lastLuminance = luminance; lastHue = hue; lastSaturation = saturation;
////                                }
////                                else
////                                {
////                                    column--;
////                                    index -= 4;
////                                    break;
////                                }
////                            }
//
//                    }
////                        if (hue <= hueTop && hueBot <= hue
////                                && luminance < luminanceTop && luminanceBot < luminance
////                                && saturation < saturationTop && saturationBot < saturation
////                                )
////                        {
////                            break;
////                        }
////                        else
////                        {
////                            outBuffer[index] = inBuffer[index];
////                            outBuffer[index + 1] = inBuffer[index + 1];
////                            outBuffer[index + 2] = inBuffer[index + 2];
////                            outBuffer[index + 3] = inBuffer[index + 3];
////                        }
//                }
////                        break;
////                    }

/////////////////////////////////////////////////////////////////////////////
