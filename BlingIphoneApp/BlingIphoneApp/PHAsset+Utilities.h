//
//  PHAsset+Utilities.h
//
//  Created by Zakk Hoyt on 9/22/14.
//  Copyright (c) 2014 Zakk Hoyt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

typedef void (^PHAssetBoolBlock)(BOOL success);
typedef void (^PHAssetMetadataBlock)(NSDictionary *metadata);
typedef void (^PHAssetAssetBoolBlock)(PHAsset *asset, BOOL success);

@interface PHAsset (Utilities)


/** \brief Save a copy of a PHAsset to a photo album.
 *
 * \param   title: The title of the album.
 *          completionBlock: This block is passed a BOOL for success.  This parameter may be nil.
 *
 * \return none
 *
 * Save a copy of a PHAsset to a photo album. Will create the album if it doesn't exist.
 *
 */
-(void)saveToAlbum:(NSString*)title completionBlock:(PHAssetBoolBlock)completionBlock;


/** \brief Get asset metadata dictionary
 *
 * \param   completionBlock: This block is passed a dictionary of metadata properties. See ImageIO framework for parsing/reading these. This parameter may be nil.
 *
 * \return none
 *
 * Get metadata dictionary of an asset (the kind with {Exif}, {GPS}, etc....
 *
 */
-(void)requestMetadataWithCompletionBlock:(PHAssetMetadataBlock)completionBlock;


/** \brief Get asset metadata dictionary
 *
 * \param   location: A CLLocation object to be written to the PHAsset. See CoreLocation framework for obtaining locations.
 *          creationDate           An NSDate to be written to the PHAsset.
 *          completionBlock: This block is passed a dictionary of metadata properties. See ImageIO framework for parsing/reading these. This parameter may be nil.
 *
 * \return none
 *
 * Get metadata dictionary of an asset (the kind with {Exif}, {GPS}, etc....
 *
 */
-(void)updateLocation:(CLLocation*)location creationDate:(NSDate*)creationDate completionBlock:(PHAssetBoolBlock)completionBlock;


/** \brief Save a video to camera roll
 *
 * \param   location: A CLLocation object to be written to the PHAsset. See CoreLocation framework for obtaining locations. This parameter may be nil.
 *          creationDate: An NSDate to be written to the PHAsset.
 *          completionBlock: Returns the PHAsset which was written and BOOL for success. This parameter may be nil.
 *
 * \return none
 *
 * Save a video to camera roll with optional completion (returns PHAsset in completion block)
 *
 */
+(void)saveImageToCameraRoll:(UIImage*)image location:(CLLocation*)location completionBlock:(PHAssetAssetBoolBlock)completionBlock;


/** \brief Save a video to camera roll
 *
 * \param   location               A CLLocation object to be written to the PHAsset. See CoreLocation framework for obtaining locations. This parameter may be nil.
 *          creationDate: An NSDate to be written to the PHAsset.
 *          completionBlock: Returns the PHAsset which was written and BOOL for success. This parameter may be nil.
 *
 * \return none
 *
 * Save a video to camera roll with optional completion (returns PHAsset in completion block)
 *
 */
+(void)saveVideoAtURL:(NSURL*)url location:(CLLocation*)location completionBlock:(PHAssetAssetBoolBlock)completionBlock;

@end