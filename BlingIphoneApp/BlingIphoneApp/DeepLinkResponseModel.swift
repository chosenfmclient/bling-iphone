//
//  DeeplinkResponseModel.swift
//  BlingIphoneApp
//
//  Created by Zach on 01/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
import RestKit

@objc class DeepLinkResponseModel: NSObject {
    var type: String?
    var share_id: String?
    var object_id: String?
    
    static func objectMapping() -> RKObjectMapping {
        let mapping = RKObjectMapping(for: DeepLinkResponseModel.self)
        mapping?.addAttributeMappings(from: ["type", "share_id", "object_id"])
        return mapping!
    }
}
