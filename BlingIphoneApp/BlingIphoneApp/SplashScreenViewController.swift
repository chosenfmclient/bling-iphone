//
//  SplashScreenViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 20/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class SplashScreenViewController: SIABaseViewController {

    @IBOutlet weak var logoImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        logoImage.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        logoImage.alpha = 0.25
        navigationController.clearNavigationItemLeftButton()
        
        if BIALoginManager.userIsRegistered() {
            RequestManager.getRequest("api/v1/home", queryParameters: nil, success: { (req, res) in
                guard req?.httpRequestOperation?.response?.statusCode == 200 else { return }
                guard let homeList = res?.firstObject as? HomeResponseModel else { return }
                guard let video = homeList.video_feed.first  else { return }
                VideoPlayerAssetManager.shared.downloadFileAt(video.videoUrl, with: {[weak self] (filePath) in
                    print("too fast mo fo")
                    self?.launchApp()
                })
            }, failure: nil)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews();
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(
            withDuration: 0.34,
            delay: 0.1,
            options: .curveEaseInOut,
            animations: {[weak self] in
                self?.logoImage.transform = CGAffineTransform(scaleX: 1, y: 1)
                self?.logoImage.alpha = 1
            },
            completion: {[weak self] (💩: Bool) in
                Utils.dispatchAfter(BIALoginManager.userIsRegistered() ? 4 : 0.4, closure: {[weak self] in
                    print("too slow mo fo")
                    guard !BIALoginManager.fbAccessTokenDisappeared() else { return }
                    self?.launchApp()
                })
            }
        )

    }
    
    private func launchApp() {
        let vc: UIViewController
        if (BIALoginManager.userIsRegistered()) {
            vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC")
        } else {
            vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationVC")
        }
        let transition = CATransition()
        transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        navigationController?.view.layer.add(transition, forKey:nil)
        navigationController?.setViewControllers([vc], animated: false)

    }
}
