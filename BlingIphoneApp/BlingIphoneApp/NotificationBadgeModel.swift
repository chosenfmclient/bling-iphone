//
//  NotificationBadgeHandler.swift
//  BlingIphoneApp
//
//  Created by Zach on 03/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class NotificationBadgeModel: NSObject {
    
    static func getNotificationBadge(completion: @escaping (_ badgeModel: NotificationBadgeModel?) -> Void) {
        guard BIALoginManager.userIsRegistered() else { return }
        RequestManager.getRequest(
            "api/v1/users/me/notifications",
            queryParameters: nil,
            success: { (req, res) in
                completion(res?.firstObject as? NotificationBadgeModel)
            },
            failure: nil
        )
    }
    
    var unread: String?
    
    static func objectMapping() -> RKObjectMapping {
        let objectMapping = RKObjectMapping(for: NotificationBadgeModel.self)
        objectMapping?.addAttributeMappings(from: ["unread"])
        return objectMapping!
    }
}
