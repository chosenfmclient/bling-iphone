//
//  SIATagTableViewCell.m
//  
//
//  Created by Michael Biehl on 2/10/16.
//
//

#import "SIATagTableViewCell.h"

@implementation SIATagTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void) layoutSubviews {
    
    self.tagField.layer.cornerRadius = self.tagField.bounds.size.height / 2;
    UIColor *placeholderColor = [UIColor colorWithWhite:1.0
                                                  alpha:0.7];
    self.tagField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.tagField.placeholder
                                                                          attributes:@{NSForegroundColorAttributeName:placeholderColor}];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
@end
