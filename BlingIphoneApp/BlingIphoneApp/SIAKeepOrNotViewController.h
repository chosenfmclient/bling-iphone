//
//  SIAKeepOrNotViewController.h
//  
//
//  Created by Michael Biehl on 2/23/16.
//
//

#import "SIABasePostRecordViewController.h"

@interface SIAKeepOrNotViewController : SIABasePostRecordViewController

@property (weak, nonatomic) IBOutlet UIButton *keepItButton;
@property (weak, nonatomic) IBOutlet UIButton *giveItAnotherGoButton;


@end
