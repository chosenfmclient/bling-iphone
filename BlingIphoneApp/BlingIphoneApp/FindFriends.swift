//
//  FindFriends.swift
//  BlingIphoneApp
//
//  Created by Zach on 23/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class FindFriends: NSObject {
    var userData = [SIAUser]()
    var emails_to_remove = [String]()
    
    static func objectMapping() -> (RKObjectMapping) {
        let mapping = RKObjectMapping(for: FindFriends.self)
        mapping!.addAttributeMappings(from: ["emails_to_remove":"emails_to_remove",])
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "data", toKeyPath: "userData", with: SIAUser.objectMapping()))
        return mapping!
    }
}
