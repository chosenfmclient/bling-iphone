//
//  SIACommentTableViewCell.m
//  
//
//  Created by Michael Biehl on 1/18/16.
//
//

#import "SIACommentTableViewCell.h"

@implementation SIACommentTableViewCell
- (void)layoutSubviews {
    self.imageView.layer.masksToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    CGRect nameLabelFrame = self.nameLabel.frame;
    CGRect commentLabelFrame = self.commentLabel.frame;
    
    self.imageView.frame = CGRectMake(15, ((80.0 - 60.0 )/ 2.0), 60, 60);

    // at this point the Name and Comment labels both have their text, let's size to fit text and center vertically
    // constrain comment label size to total height of cell minus name label height minus 30 for 15 top and bottom padding
    CGSize maxCommentLabelSize = CGSizeMake(commentLabelFrame.size.width,
            self.contentView.frame.size.height - nameLabelFrame.size.height);

    CGSize commentLabelSize = [self.commentLabel.attributedText boundingRectWithSize:maxCommentLabelSize
                                                                             options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                                                             context:nil].size;

    commentLabelFrame.size.height = commentLabelSize.height;


    CGFloat totalLabelHeight = nameLabelFrame.size.height + commentLabelFrame.size.height;

    nameLabelFrame.origin.y = self.contentView.frame.size.height / 2 - totalLabelHeight / 2;

    commentLabelFrame.origin.y = CGRectGetMaxY(nameLabelFrame);
    
    if (!_layedOut) {
        commentLabelFrame.origin.x = commentLabelFrame.origin.x + 15.0;
        nameLabelFrame.origin.x = commentLabelFrame.origin.x;
    }
    
    self.nameLabel.frame = nameLabelFrame;
    self.commentLabel.frame = commentLabelFrame;
    
    self.nameLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15.0f];
    
    self.commentLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12.0f];
    _layedOut = true;
}

- (void)setImageViewCornerRadius:(CGFloat)imageViewCornerRadius {
    if (imageViewCornerRadius)
        self.imageView.layer.cornerRadius = imageViewCornerRadius;
}

- (void)setImageViewFrame:(CGRect)imageViewFrame {
    self.imageView.frame = imageViewFrame;
}

- (void)setImageViewContentMode:(UIViewContentMode)imageViewContentMode {
    self.imageView.contentMode = imageViewContentMode;
}


@end
