//
//  SIATagsDataModel.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/10/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SIATagsDataModel : NSObject

@property (strong, nonatomic) NSMutableArray <NSString *> *tagsArray;

@end
