//
//  Invitations.swift
//  BlingIphoneApp
//
//  Created by Zach on 23/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class Invitations: NSObject {
    var emails: [Invitation]?
    var phone: [Invitation]?
    
    init(phoneList: [Invitation]) {
        phone = phoneList
    }
    
    init(emailList: [Invitation]) {
        emails = emailList
    }
    
    static func objectMapping() -> RKObjectMapping {
        let mapping = RKObjectMapping(for: Invitations.self)
        mapping!.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "phone", toKeyPath: "phone", with: Invitation.objectMapping()))
        
        mapping!.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "emails", toKeyPath: "emails", with: Invitation.objectMapping()))
        return mapping!.inverse()
    }
}
