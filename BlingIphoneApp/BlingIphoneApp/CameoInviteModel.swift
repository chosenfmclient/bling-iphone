//
//  CameoInviteModel.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 2/9/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

import UIKit
import RestKit

@objc class CameoInviteModel: NSObject {
    var followings: [CameoInviteUser]?
    var emails_to_remove: [String]?
    var emails: [DeviceContact]?
    var phones: [DeviceContact]?
    
    let contact_emails: [String]
    let contact_phones: [String]
    let video_id: String
    
    required init(contacts: [DeviceContact], followings: [SIAUser], cameoId: String) {
        contact_emails = contacts.filter {
            $0.email != nil
            }.map {
                $0.email!
        }
        contact_phones = contacts.filter {
            $0.phoneNumber != nil
            }.map {
                $0.phoneNumber!
        }

        video_id = cameoId
        super.init()
    }
    
    static func requestMapping() -> RKMapping {
        let mapping = RKObjectMapping.request()
        
        mapping?.addAttributeMappings(from: [
                "contact_emails",
                "contact_phones",
                "video_id"
            ])
        
        return mapping!
    }
    
    static func filteringObjectMapping() -> RKMapping {
        let mapping = RKObjectMapping(for: CameoInviteModel.self)
        
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "followings", toKeyPath: "followings", with: CameoInviteUser.objectMapping()))
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "emails", toKeyPath: "emails", with: DeviceContact.objectMapping()))
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "phones", toKeyPath: "phones", with: DeviceContact.objectMapping()))
        
        mapping?.addAttributeMappings(from: ["emails_to_remove"])
        
        return mapping!
    }
}

class CameoSendInvitesModel: NSObject {
    var invitees: [CameoInvitee]
    var video_id: String
    
    init(invitees: [CameoInvitee], cameoId: String) {
        self.invitees = invitees
        video_id = cameoId
    }
    
    static func inviteRequestMapping() -> RKMapping {
        let mapping = RKObjectMapping.request()
        
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "invitees",
                                                          toKeyPath: "invitees",
                                                          with: CameoInvitee.objectMapping()))
        mapping?.addAttributeMappings(from: ["video_id"])
        
        return mapping!
    }
    
    
}

class CameoInvitee: NSObject {
    var md5Identity: String
    var name: String
    var user_id: String?
    var email: String?
    var phone: String?
    
    init(id: String, name: String, user_id: String? = nil, email: String? = nil, phone: String? = nil) {
        md5Identity = id
        self.name = name
        self.user_id = user_id
        self.email = email
        self.phone = phone
        super.init()
    }
    
    static func objectMapping() -> RKMapping {
        let mapping = RKObjectMapping.request()
        
        mapping?.addAttributeMappings(from: ["email", "name", "user_id", "phone"])
        
        return mapping!
    }
}

@objc class CameoInviteUser: NSObject {
    var user: SIAUser?
    var has_invited: String? {
        didSet {
            wasInvited = has_invited == "true"
        }
    }
    var wasInvited: Bool?
    
    static func objectMapping() -> RKMapping {
        let mapping = RKObjectMapping(for: CameoInviteUser.self)
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "user",
                                                          toKeyPath: "user",
                                                          with: SIAUser.objectMapping()))
        
        mapping?.addAttributeMappings(from: ["has_invited" : "has_invited"])
        
        return mapping!
    }
}
