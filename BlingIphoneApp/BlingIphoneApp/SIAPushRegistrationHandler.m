//
//  SIANotificationsRegistrationHandler.m
//  ChosenIphoneApp
//
//  Created by michael on 8/3/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAPushRegistrationHandler.h"
#import <UserNotifications/UserNotifications.h>
#define TIMER_INTERVAL (60 * 4) // 4 minutes
#import "SIAPushRegistrationInterstitial.h"


@interface SIAPushRegistrationHandler ()

@property (strong, nonatomic) NSMutableDictionary *pushRegistrationDict;
@property (strong, nonatomic) NSDate *loginTime;
@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic, readwrite) BOOL interstitialIsShown;
@property (nonatomic) NSTimeInterval pauseTime;

@end

@implementation SIAPushRegistrationHandler

+ (SIAPushRegistrationHandler *)sharedHandler {
    static SIAPushRegistrationHandler *sharedInstance = nil;
    @synchronized (self) {
        if (!sharedInstance) {
            sharedInstance = [[SIAPushRegistrationHandler alloc] init];
        }
        return sharedInstance;
    }
}

- (id)init {
    if (self = [super init])
    {
        [self setInitialStateIfNeeded];
        self.loginTime = [NSDate date];
        [self startTimerWithTime:TIMER_INTERVAL];
    }
    return self;
}

/**
 *  Checks if we have logged the initial login time and creates the dictionary if needed.
 */
- (void)setInitialStateIfNeeded {
    if (!self.pushRegistrationDict)
    {
        [self setInitialState];
    }
}
/**
 *  Logs the current time for the login time and saves the push registration data dictionary to a file.
 */
- (void)setInitialState {
    
    NSDictionary *pushRegistrationDict = @{kLoginTime : [NSDate date],
                                           kNeedsRegisterForPushNotifications : @(![[UIApplication sharedApplication] isRegisteredForRemoteNotifications]),
                                           kUserChoseNo : @NO
                                           };
    
    [pushRegistrationDict writeToFile:[self pushRegistrationFilePath]
                           atomically:YES];
}

- (void)setUserChoseNo {
    [self.pushRegistrationDict setObject:@YES
                                  forKey:kUserChoseNo];
    [self.pushRegistrationDict setObject:[NSDate date]
                                  forKey:kChoseNoTimestamp];
    [self saveCurrentPushDict];
}

- (void)recordingCompleted {
//    if (([self needsToRegister] && ![self userChoseNo]) || [self threeDaysPassedSinceNo])
//    {
        [self registerNotifications:nil];
//    }
}

- (void)homeScreenDidAppear {
    [self registerIfTwelveHoursSinceInstall];
}

- (void)notificationScreenDidAppear {
    if (![self userChoseNo]) {
        [self registerNotifications:nil];
    }
}

- (void)followedUser {
    [self registerIfThreeDaysSinceNo];
}

- (void)commented {
    [self registerIfThreeDaysSinceNo];
}

- (void)registerIfTwelveHoursSinceInstall {
    if ([self twelveHoursPassedSinceInstall] && ![self userChoseNo]) {
        [self registerNotifications:nil];
    }
}

- (void)registerIfThreeDaysSinceNo {
    if ([self userChoseNo] && [self threeDaysPassedSinceNo])
    {
        [self registerNotifications:nil];
    }
}

- (BOOL)twelveHoursPassedSinceInstall {
    return [[NSDate date] compare:[self twelveHoursSinceInstallDate]] == NSOrderedDescending;
}

- (BOOL)threeDaysPassedSinceNo {
    return [[NSDate date] compare:[self threeDaysSinceNoDate]] == NSOrderedDescending;
}

- (BOOL)sevenDaysPassedSinceNo {
    return [[NSDate date] compare:[self sevenDaysSinceNoDate]] == NSOrderedDescending;
}

- (NSDate *)threeDaysSinceNoDate {
    return [NSDate dateWithTimeInterval:(60 * 60 * 24 * 3)
                              sinceDate:[self.pushRegistrationDict objectForKey:kChoseNoTimestamp]];
}

- (NSDate *)sevenDaysSinceNoDate {
    return [NSDate dateWithTimeInterval:(60 * 60 * 24 * 7)
                              sinceDate:[self.pushRegistrationDict objectForKey:kChoseNoTimestamp]];
}

- (NSDate *)twelveHoursSinceInstallDate {
    return [NSDate dateWithTimeInterval:(60 * 60 * 12)
                              sinceDate:[self.pushRegistrationDict objectForKey:kLoginTime]];
}

- (BOOL)userChoseNo {
    return [[self.pushRegistrationDict objectForKey:kUserChoseNo] boolValue];
}

/**
 *  This initiates the registration for receiving remote notifications. Pass a completion handler to continue normal execution after the user taps 'yes' or 'no'.
 *
 *  @param handler the handler for continuation.
 */
- (void)registerNotifications:(void (^)(void))handler {
    
    if (![self needsToRegister]) return;
    self.interstitialIsShown = YES;
    //self.registrationCompletionHandler = handler;
    
    SIAPushRegistrationInterstitial *interstitial = [[NSBundle mainBundle] loadNibNamed:@"SIAPushRegistrationInterstitial"
                                                                                  owner:self
                                                                                options:nil].firstObject;
    interstitial.completionHandler = ^(BOOL registerForPushNotifications) {
        
        [AmplitudeEvents logWithEvent:@"enablenotifications" with:@{@"enabled" : registerForPushNotifications ? @"yes" : @"no"}];
        [AmplitudeEvents logUserWithProperty:@{@"notification_access":registerForPushNotifications ? @"yes" : @"no"}];
        
        if (registerForPushNotifications)
        {
            if (SYSTEM_VERSION_10_OR_GREATER) {
                [self registerWithUserNotificationCenter];
            }
            else {
                UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
                                                                                                     | UIUserNotificationTypeSound
                                                                                                     | UIUserNotificationTypeAlert)
                                                                                         categories:nil];
                [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
            }
            [self.pushRegistrationDict setObject:@NO
                                          forKey:kNeedsRegisterForPushNotifications];
            [self saveCurrentPushDict];
        }
        else
        {
            [self setUserChoseNo];
            if (self.registrationCompletionHandler)
            {
                self.registrationCompletionHandler();
                self.registrationCompletionHandler = nil;
            }
        }
    };
    
    [interstitial show];
}

- (void)interstitialDidDismiss {
    self.interstitialIsShown = NO;
}

- (void)registerWithUserNotificationCenter {
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [center requestAuthorizationWithOptions:UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                              [self applicationDidRegister];
                              if (!error)
                              {
                                  [[UIApplication sharedApplication] registerForRemoteNotifications];
                              }
                              else
                              {
                                  NSLog(@"remote notifications error: %@", error.localizedDescription);
                              }
                          }];
}

- (void)applicationDidRegister {
    if (self.registrationCompletionHandler) {
        [Utils dispatchOnMainThread:^{
            self.registrationCompletionHandler();
            self.registrationCompletionHandler = nil;
        }];
    }
}

- (BOOL)needsToRegister {
    return [BIALoginManager userIsRegistered] && ((NSNumber*)[self.pushRegistrationDict objectForKey:kNeedsRegisterForPushNotifications]).boolValue;
}

/**
 *  Starts the timer
 *
 *  @param time the time
 */
- (void)startTimerWithTime:(NSTimeInterval)time {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:time
                                                  target:self
                                                selector:@selector(timerFired)
                                                userInfo:nil
                                                 repeats:NO];
}
/**
 *  Pauses the timer. Use this if the app is in the background.
 */
- (void)pauseTimer {
    self.pauseTime = [[NSDate date] timeIntervalSinceDate:self.loginTime];
    NSLog(@"Pause time: %f", self.pauseTime);
    [self.timer invalidate];
}
/**
 *  Resumes the timer.
 */
- (void)resumeTimer {
    if (self.timer.isValid == NO) // prevents timer from being initialized twice :)
    {
        [self startTimerWithTime:TIMER_INTERVAL - self.pauseTime];
    }
}
/**
 *  When the timer fires.
 */
- (void)timerFired {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *topVCClassStr = NSStringFromClass([[appDelegate topViewController] class]);
    BOOL shouldRegisterNow = !([topVCClassStr containsString:@"SIABlingRecordViewController"] ||
                               [topVCClassStr containsString:@"VideoHomeViewController"] ||
                               [topVCClassStr containsString:@"Registration"]);
    if (shouldRegisterNow && ![self userChoseNo])
    {
        [self registerNotifications:nil];
    }
}

/**
 *  The data dictionary for the various criteria to surface the registration flow.
 *
 *  @return NSMutableDictionary the dictionary of criteria.
 */
- (NSMutableDictionary *)pushRegistrationDict {
    if (!_pushRegistrationDict)
    {
        _pushRegistrationDict = [NSMutableDictionary dictionaryWithContentsOfFile:[self pushRegistrationFilePath]];
    }
    return _pushRegistrationDict;
}

- (NSString *)pushRegistrationFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask,
                                                         YES);
    return [paths.firstObject stringByAppendingPathComponent:@"pushRegistrationDict.out"];
}
/**
 *  Writes the pushRegistrationDict to the documents directory.
 */
- (void)saveCurrentPushDict {
    [self.pushRegistrationDict writeToFile:[self pushRegistrationFilePath]
                                atomically:YES];
}

@end
