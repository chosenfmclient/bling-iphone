//
//  SIAShareSheetViewController.swift
//  ChosenIphoneApp
//
//  Created by Zach on 19/08/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

import UIKit
import AppsFlyerLib

enum ShareButtonType : Int {
    case sms = 0
    case instagram
    case mail
    case facebook
    case twitter
    case more
    case snapChat
    case copyLink
    case musically
    case youtube
    case save
    case undefined
}

enum AmplitudeLocation : String {
    case home = "home"
    case videoHome = "videohome"
    case recording = "recording"
    case unknown = "unknown"
}

@objc protocol PostRecordShareDelegate: class {
    func snapchatShareWasInitiated()
}

//MARK: view controller
class SIAShareSheetViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraintOfShareViewForAnimationBecauseImCool: NSLayoutConstraint!
    @IBOutlet var shareViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var linkCopiedSuccessView: UIView!
    
    var platforms: [ShareButtonType] = [
                        .instagram,
                        .snapChat,
                        .musically,
                        .youtube,
                        .sms,
                        .more
                    ]
    
    var videoIsInCameraRoll = false
    var isPostRecording = false
    var asset: AVAsset?
    var objectType: String = "video"
    var objectId: String = ""
    var video: SIAVideo?
    var shareManager: SIAShare = SIAShare.init()
    weak var postRecordDelegate: PostRecordShareDelegate?
    
    var amplitudeLocation  = AmplitudeLocation.unknown
    
    func setPlayerForInstagram(player: BIAVideoPlayer, withBeginning beginTime: CMTime, andEnd endTime:CMTime) {
        shareManager.exportTimeRange = CMTimeRangeMake(beginTime, CMTimeSubtract(endTime, beginTime));
        shareManager.effectPlayer = player;
    }
    
    func setCompositedData(videoURL compositedAssetURL: NSURL?, andPhotoData photoData:NSData?) {
        shareManager.compositedAssetURL = compositedAssetURL as URL!;
        shareManager.photoData = photoData as Data!;
    }
    
    func weAreSharingVideo() -> Bool {
        return (objectType == "video" || (shareManager.shareType != nil && shareManager.shareType == "video"));
    }
    
    //MARK: view controller methods
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        nextButton.layer.cornerRadius = nextButton.bounds.size.height / 2
      bottomConstraintOfShareViewForAnimationBecauseImCool.constant = -UIScreen.main.bounds.height
        nextButton.isHidden = false
        nextButton.setTitle("Close", for: .normal)
        
        weak var weakself = self;
        shareManager.viewController = weakself
        
        if (shareManager.shareType == nil) {
            shareManager.shareType = objectType
            shareManager.object_id = objectId
            if let _ = video {
                shareManager.video = video
            }
        }
        
        var params = ["pagename": amplitudeLocation.rawValue]
        
        if let videoId = video?.videoId {
            params["object_id"] = videoId
        }
        
        AmplitudeEvents.log(event: "share:tap",
                            with: params)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        VideoPlayerManager.shared.shouldPlay = false
        if let _ = video?.videoId {
            showShareSheet()
        }
    }
    
    func showShareSheet() {
        bottomConstraintOfShareViewForAnimationBecauseImCool.constant = 0
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: .curveEaseInOut,
            animations: {[weak self] in
                self?.view.layoutIfNeeded()
            },
            completion: nil)
    }
    
    func dismiss() {
        if let navController = self.navigationController {
            let transition = CATransition.init()
            transition.duration = 0.3;
            transition.timingFunction = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionEaseInEaseOut);
            transition.type = kCATransitionMoveIn;
            transition.subtype = kCATransitionFromBottom;
            navController.view.layer.add(transition,forKey: nil);
            navController.popViewController(animated: false)
        } else {
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        VideoPlayerManager.shared.shouldPlay = true
    }
    
    //MARK: collection view delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return platforms.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Button", for: indexPath as IndexPath) as? SIAShareSheetCollectionViewCell
        weak var weakself = self
        cell!.setSharePlatform(platform: platforms[indexPath.item], with: weakself!)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath as IndexPath) as? SIAShareSheetCollectionViewCell
        cell!.itemTapped()
        guard let _ = video!.videoId else { return }
        AmplitudeEvents.log(event: "share:platform",
                            with: ["pagename": amplitudeLocation.rawValue,
                                   "platform":cell!.amplitudePlatform,
                                   "object_id":video!.videoId]
        )
    }
    
    //MARK: ui outlets
    @IBAction func close(_ sender: UIButton?) {
        /*if (isPostRecording) {
            let nav = self.navigationController as! SIAPostRecordViewController
            nav.close()
        }
        else {
            dismiss()
        }*/
        dismiss()
    }
    @IBAction func nextAction(_ sender: UIButton?) {
        if (isPostRecording) {
            let nav = self.navigationController as! SIAPostRecordViewController
            nav.next()
        }
        else {
             dismiss()
        }
    }
    
    func pauseStupidPlayer() {
        if let postRecordVC = navigationController as? SIAPostRecordViewController {
            postRecordVC.player.stopRepeatingPlayback()
        }
    }
}
import AppsFlyerLib
//MARK: collection view cell
class SIAShareSheetCollectionViewCell: UICollectionViewCell {
    var platform: ShareButtonType = ShareButtonType.undefined
    var itemTapped: () -> Void = {}
    var amplitudePlatform = "unknown"
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var dimmingView: UIView!
    @IBOutlet weak var spinner: SIASpinner!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        dimmingView.bia_rounded()
        if (!spinner.isAnimating) {
            spinner.startAnimation()
        }
    }
    
    func trackCompleteShare() {
        AmplitudeEvents.log(event: "share:complete", with: ["platform":amplitudePlatform])
        
        AppsFlyerTracker.shared().trackEvent("sharevideo", withValues: ["platform":amplitudePlatform])
    }
    
    func setSharePlatform(platform: ShareButtonType, with viewController: SIAShareSheetViewController?) {
        self.platform = platform
        if (viewController?.video is SIARecordingItem) {
            let recordingItem = viewController?.video as! SIARecordingItem
            viewController?.shareManager.image = recordingItem.image
        }
        
        switch platform {
        case .facebook:
            amplitudePlatform = "facebook"
            imageView.image = UIImage.init(named: "large_share_facebook.png")
            label.text = "Facebook"
            itemTapped = {[weak self] in
                self?.dimmingView.isHidden = false
                viewController?.shareManager.shareOnFB(
                    completionBlock: {(success: Bool) -> Void in
                        self?.dimmingView.isHidden = true
                        self?.trackCompleteShare()
                        NSLog("fb successful: %@", success.description)
                    }
                )
            }
            
        case .twitter:
            amplitudePlatform = "twitter"
            imageView.image = UIImage.init(named: "large_share_twitter.png")
            label.text = "Twitter"
            itemTapped = {[weak self] in
                self?.dimmingView.isHidden = false
                viewController?.shareManager.shareOnTwitter(
                    completionBlock: {(success: Bool) -> Void in
                        self?.dimmingView.isHidden = true
                        NSLog("tw successful: %@", success.description)
                        if success {
                            self?.trackCompleteShare()
                        }
                })
            }
            
        case .instagram:
            amplitudePlatform = "instagram"
            imageView.image = UIImage.init(named: "large_share_instagram.png")
            label.text = "Instagram"
            itemTapped = {[weak self] in
                self?.dimmingView.isHidden = false
                viewController?.shareManager.asset = viewController?.asset
                viewController?.pauseStupidPlayer()
                viewController?.shareManager.shareOnInstagram(
                    completionBlock: {(success: Bool) -> Void in
                        self?.dimmingView.isHidden = true
                       // viewController?.resumeStupidPlayer()
                        NSLog("insta successful: %@", success.description)
                        self?.trackCompleteShare()
                    }
                )
            }
            
        case .sms:
            amplitudePlatform = "sms"
            imageView.image = UIImage.init(named: "large_share_sms.png")
            label.text = "Text"
            itemTapped = { [weak self] in
                self?.dimmingView.isHidden = false
                viewController?.shareManager.shareBySMS(
                    completionBlock: {(success: Bool) -> Void in
                        self?.dimmingView.isHidden = true
                        NSLog("sms successful: %@", success.description)
                        self?.trackCompleteShare()
                    }
                )
            }
            
        case .mail:
            amplitudePlatform = "mail"
            imageView.image = UIImage.init(named: "large_share_mail.png")
            label.text = "Mail"
            itemTapped = {[weak self] in
                self?.dimmingView.isHidden = false
                viewController?.shareManager.shareByMail(
                    completionBlock: {(success: Bool) -> Void in
                        self?.dimmingView.isHidden = true
                        NSLog("mail successful: %@", success.description)
                        self?.trackCompleteShare()
                    }
                )
            }
            
        case .more:
            imageView.image = UIImage.init(named: "large_share_more.png")
            label.text = "More"
            itemTapped = {
                Utils.dispatchOnMainThread {
                    viewController?.view.layoutIfNeeded()

                    viewController?.shareViewHeightConstraint.isActive = false
                    UIView.animate(
                        withDuration: 0.3,
                        delay: 0,
                        options: .curveEaseInOut,
                        animations: {
                            viewController?.view.layoutIfNeeded()
                        },
                        completion: { (kill_me) in
                            guard let vc = viewController else { return }
                            vc.platforms = [
                                .instagram,
                                .snapChat,
                                .musically,
                                .youtube,
                                .sms,
                                .mail,
                                .copyLink,
                                .facebook,
                                .twitter
                            ]
                            if let _ = vc.video, vc.video!.isMyVideo() {
                                vc.platforms.append(.save)
                            }
                            UIView.transition(
                                with: vc.collectionView,
                                duration: 0.3,
                                options: .transitionCrossDissolve,
                                animations: {
                                    vc.collectionView.reloadData()
                                },
                                completion: nil
                            )
                        }
                    )
                }
            }
            
        case .copyLink:
            amplitudePlatform = "copylink"
            imageView.image = UIImage.init(named: "large_share_copy_link.png")
            label.text = "Copy Link"
            itemTapped = {[weak self] in
                print ("copylink tapped")
                self?.dimmingView.isHidden = false
                viewController?.shareManager.shareByCopyLink(
                    completionBlock: { (success: Bool) -> Void in
                        self?.dimmingView.isHidden = true
                        UIView.animate(
                            withDuration: 0.3,
                            delay: 0,
                            options: .curveEaseInOut,
                            animations: {
                                viewController?.linkCopiedSuccessView.alpha = 1
                            },
                            completion: { (x) in
                                Utils.dispatchAfter(1.4, closure: {
                                    UIView.animate(
                                        withDuration: 0.3,
                                        delay: 0,
                                        options: .curveEaseInOut,
                                        animations: {
                                            viewController?.linkCopiedSuccessView.alpha = 0
                                        },
                                        completion: nil)
                                })
                        })
                        NSLog("copyLink successful: %@", success.description)
                        self?.trackCompleteShare()
                    }
                )
            }
        case .snapChat:
            amplitudePlatform = "snapchat"
            imageView.image = UIImage(named: "large_share_snapchat")
            label.text = "Snapchat"
            itemTapped = constructItemTappedAction(forPlatform: "snapchat",
                                                   withViewController: viewController)
        case .musically:
            amplitudePlatform = "musically"
            imageView.image = UIImage(named: "large_share_musically.png")
            label.text = "Musical.ly"
            itemTapped = constructItemTappedAction(forPlatform: "musically",
                                                   withViewController: viewController)
        case .youtube:
            amplitudePlatform = "youtube"
            imageView.image = UIImage(named: "large_share_youtube.png")
            label.text = "YouTube"
            itemTapped = constructItemTappedAction(forPlatform: "youtube",
                                                   withViewController: viewController)
        case .save:
            amplitudePlatform = "save-to-camera-roll"
            imageView.image = UIImage(named: "large_share_save.png")
            label.text = "Save"
            itemTapped = constructItemTappedAction(forPlatform: "save",
                                                   withViewController: viewController)
        default:
            imageView.image = nil
            label.text = ""
        }
    }
    
    func constructItemTappedAction(forPlatform platform: String, withViewController viewController: SIAShareSheetViewController?) -> (() -> ()) {
        return {[weak self] in
            guard let _ = self, let vc = viewController else { return }
            self!.dimmingView.isHidden = false
            if vc.isPostRecording
            {
                if vc.videoIsInCameraRoll || vc.asset == nil
                {
                    vc.shareManager.share(onPlatformWithoutCameraRoll: platform,
                                          withCompletion: {(success: Bool) -> Void in
                                            self?.dimmingView.isHidden = true
                                            self?.trackCompleteShare()
                    })
                }
                else
                {
                    vc.postRecordDelegate?.snapchatShareWasInitiated()
                    vc.shareManager.share(onPlatform: platform,
                                          with: vc.asset,
                                          withCompletion: {(success: Bool) -> Void in
                        self?.dimmingView.isHidden = true
                        self?.trackCompleteShare()
                    })
                }
            }
            else
            {
                vc.shareManager.shareonPlatform(platform,
                                                withCompletion: {(success: Bool) -> Void in
                    self?.dimmingView.isHidden = true
                    self?.trackCompleteShare()
                })
            }
        }
    }
}
