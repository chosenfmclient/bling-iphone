//
//  SIAVideoPlayerView.m
//  ChosenIphoneApp
//
//  Created by Joseph Nahmias on 07/01/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAVideoPlayerView.h"

@interface SIAVideoPlayerView ()


@end

@implementation SIAVideoPlayerView

-(instancetype)initWithPlayer:(AVPlayer *)player{
    self = [super init];
    if (self) {
        self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
        self.playerLayer.contentsScale = [UIScreen mainScreen].scale;
        //[self.playerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        [self.layer addSublayer:self.playerLayer];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.playerLayer.frame = self.bounds;
}


@end
