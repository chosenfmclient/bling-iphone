//
//  DiscoverSearchField.swift
//  BlingIphoneApp
//
//  Created by Zach on 26/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class DiscoverSearchField: UITextField {
    
    override func becomeFirstResponder() -> Bool {
        let returnVal = super.becomeFirstResponder()
        if (returnVal) {
            self.leftView?.isHidden = false
        }
        guard (text == nil || (text?.isEmpty)!) else {
            return returnVal
        }
        return returnVal
    }
    
    override func resignFirstResponder() -> Bool {
        let returnVal = super.resignFirstResponder()
        if (returnVal) {
            if (text == nil || (text?.isEmpty)!) {
                self.leftView?.isHidden = true
            }
        }
        guard (text != nil && !(text?.isEmpty)!) else {
            return returnVal
        }
        
        return returnVal
    }
}
