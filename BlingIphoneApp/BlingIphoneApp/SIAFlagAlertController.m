//
//  SIAFlagAlertController.m
//  ChosenIphoneApp
//
//  Created by Zach on 07/09/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAFlagAlertController.h"

#define FLAG2   @"spam"
#define FLAG3   @"sexual"
#define FLAG4   @"violent"
#define FLAG5   @"hateful"
#define FLAG6   @"harmful"
#define FLAG7   @"infringing"


#define SPAM_CONFIRM_TEXT      @"Are you sure this %@ contains SPAM content?"
#define SPAM_CONFIRM_BUTTON    @"FLAG"
#define INAPPROPRIATE_BUTTON   @"INAPPROPRIATE"
#define SPAM_BUTTON            @"SPAM"
#define ACCESS_TOKEN            @"access_token"
#define SELECTED_FLAG_KEY       @"type"
#define VIDEOS_PATH             @"api/v1/videos"
#define FLAG_VIDEOS_PATH        @"/api/v1/videos/%@/flag"


@interface SIAFlagAlertController()
@property (nonatomic) int selectedFlag;
@property (strong, nonatomic) NSString* type;
@property (weak, nonatomic) NSString* videoId;
@end
@implementation SIAFlagAlertController
+ (SIAFlagAlertController *) getFlagAlertControllerForVideo: (SIAVideo*) video withPresentationHandler: (void (^)(SIAAlertController *))presenter {
    SIAFlagAlertController *flagAlert = [SIAFlagAlertController alertControllerWithTitle:@"Choose Report Reason" message:nil];
    flagAlert.videoId = video.videoId;
    
    SIAAlertAction *inappropAction = [SIAAlertAction actionWithTitle:INAPPROPRIATE_BUTTON style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
        [flagAlert dismissViewControllerAnimated:YES completion:^{
            SIAAlertController *inappropAlert = [SIAAlertController alertControllerWithTitle:@"Choose Report Reason" message:nil];
            [inappropAlert addAction:[SIAAlertAction actionWithTitle:@"SEXUAL CONTENT" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                flagAlert.selectedFlag = 3;
                [flagAlert flagConfirmButtonAction];
                [inappropAlert dismissViewControllerAnimated:YES completion:nil];
            }]];
            
            [inappropAlert addAction:[SIAAlertAction actionWithTitle:@"VIOLENT OR REPULSIVE CONTENT" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                flagAlert.selectedFlag = 4;
                [flagAlert flagConfirmButtonAction];
                [inappropAlert dismissViewControllerAnimated:YES completion:nil];
            }]];
            
            [inappropAlert addAction:[SIAAlertAction actionWithTitle:@"HATEFUL OR ABUSIVE CONTENT" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                flagAlert.selectedFlag = 5;
                [flagAlert flagConfirmButtonAction];
                [inappropAlert dismissViewControllerAnimated:YES completion:nil];
            }]];
            
            [inappropAlert addAction:[SIAAlertAction actionWithTitle:@"HARMFUL DANGEROUS ACTS" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                flagAlert.selectedFlag = 6;
                [flagAlert flagConfirmButtonAction];
                [inappropAlert dismissViewControllerAnimated:YES completion:nil];
            }]];
            
            [inappropAlert addAction:[SIAAlertAction actionWithTitle:@"INFRINGES MY RIGHTS" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                flagAlert.selectedFlag = 7;
                [flagAlert flagConfirmButtonAction];
                [inappropAlert dismissViewControllerAnimated:YES completion:nil];
            }]];
            
            SIAAlertAction *cancelAction = [SIAAlertAction actionWithTitle:@"CANCEL" style:(SIAAlertActionStyleCancel) handler:nil];
            [inappropAlert addAction:cancelAction];
            SIAAlertAction *flagAction = [SIAAlertAction actionWithTitle:@"REPORT" style:(SIAAlertActionStyleConfirmation) handler:nil];
            [inappropAlert addAction:flagAction];
            
            [inappropAlert setActionSize: CGSizeMake(320, 48)];
            
            presenter(inappropAlert);
        }];
    }];
    [flagAlert addAction:inappropAction];
    
    SIAAlertAction *spamAction = [SIAAlertAction actionWithTitle:SPAM_BUTTON style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
        [flagAlert dismissViewControllerAnimated:YES completion:^{
            SIAAlertController *spamAlert = [SIAAlertController alertControllerWithTitle:@"Are you sure?"
                                                                                 message:[NSString stringWithFormat:SPAM_CONFIRM_TEXT, video.mediaType]];
            SIAAlertAction *cancelAction = [SIAAlertAction actionWithTitle:@"CANCEL" style:(SIAAlertActionStyleCancel) handler:nil];
            [spamAlert addAction:cancelAction];
            SIAAlertAction *flagAction = [SIAAlertAction actionWithTitle:@"REPORT" style:(SIAAlertActionStyleConfirmation) handler:^(SIAAlertAction *action) {
                flagAlert.selectedFlag = 2;
                [flagAlert flagConfirmButtonAction];
                [spamAlert dismissViewControllerAnimated:YES completion:nil];
            }];
            [spamAlert addAction:flagAction];
            
            presenter(spamAlert);
        }];
    }];
    [flagAlert addAction:spamAction];
    
    SIAAlertAction *cancelAction = [SIAAlertAction actionWithTitle:@"CANCEL" style:(SIAAlertActionStyleCancel) handler:nil];
    [flagAlert addAction:cancelAction];
    
    SIAAlertAction *nextAction = [SIAAlertAction actionWithTitle:@"NEXT" style:(SIAAlertActionStyleConfirmation) handler:nil];
    [flagAlert addAction:nextAction];

    return flagAlert;
}


- (void)flagConfirmButtonAction {
    
    switch (_selectedFlag) {
            
        case 2:
            _type = FLAG2;
            break;
            
        case 3:
            _type = FLAG3;
            break;
            
        case 4:
            _type = FLAG4;
            break;
            
        case 5:
            _type = FLAG5;
            break;
            
        case 6:
            _type = FLAG6;
            break;
            
        case 7:
            _type = FLAG7;
            break;
            
        default:
            break;
    }
    
    if (_selectedFlag > 1) {
        RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
        [requestMapping addAttributeMappingsFromArray:@[SELECTED_FLAG_KEY]];
        
        [RequestManager
         putRequest:[NSString stringWithFormat:FLAG_VIDEOS_PATH, self.videoId]
         object:self
         queryParameters:nil
         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
             SIAAlertController *alert = [SIAAlertController alertControllerWithTitle:@"Blingy" message:@"Thank you for your report"];
             [alert addAction:[SIAAlertAction actionWithTitle:@"OK" style:(SIAAlertActionStyleCancel) handler:nil]];
             //[[sia_AppDelegate getTopViewController] presentViewController:alert animated:YES completion:nil];
             
             NSLog(@"flag put success");
             
         }
         failure:^(RKObjectRequestOperation *operation, NSError *error) {
             
             NSLog(@"flag put error: %@", error);
         }
         showFailureDialog:YES];
    }
}
@end
