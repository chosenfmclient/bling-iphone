//
//  SIALoginManager.h
//  BlingIphoneApp
//
//  Created by Zach on 15/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SIAUser.h"

@interface BIALoginManager : NSObject
@property (nonatomic) BOOL isRefreshingToken;
@property(strong, readwrite, nonatomic) SIAUser *myUser;
+ (instancetype) sharedInstance;
- (void) refreshAccessTokenForCompletion: (void(^)(BOOL success))completion;
+ (BOOL) userIsRegistered;
+ (BOOL) fbAccessTokenDisappeared; // somehow the FBSDKAccessToken can disappear, like if you update the SDK, so we need to check :)

- (void) createEmailUserWithEmail:(NSString*) email
                         password:(NSString*) password
                        firstName:(NSString*) firstName
                         lastName:(NSString*) lastName
                completionHandler:(void (^)(BOOL, NSString*)) completion;
- (void) sendNewPassword:(NSString*) email
       completionHandler:(void (^)(BOOL, NSString*)) completion;
-(void) FBLoginFromVC:(UIViewController *)vc
          withHandler:(void (^)(BOOL)) handler;

- (void) setApnSettingsWithDeviceToken:(NSData *)deviceToken;
+ (NSString*) myUserId;
- (void) fetchLikesFromServer;
- (void) fetchFollowsFromServer;
@end
