//
//  DetectionFuckedUpViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 14/12/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class DetectionFuckedUpViewController: UIViewController {
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var redetectButton: UIButton!
    
    weak var delegate: RedetectDialogDelegate?
    
    override func viewDidLayoutSubviews() {
        redetectButton.bia_rounded()
        dialogView.layer.cornerRadius = 5.0
    }
    
    @IBAction func redetectWasTapped(_ sender: Any) {
        self.delegate?.redetectWasTapped()
        dismiss(animated: true, completion: nil)
    }
    
}

protocol RedetectDialogDelegate: class {
    func redetectWasTapped()
}
