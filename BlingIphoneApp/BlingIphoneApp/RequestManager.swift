//
//  RequestManager.swift
//  BlingIphoneApp
//
//  Created by Zach on 15/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class RequestManager: NSObject {
    static let versionNumber = "1.0"
    
    fileprivate static func makeRequest(_ requestMethod: RKRequestMethod,
                                        path: String!,
                                        object: Any?,
                                        queryParameters: Dictionary<String, String>?,
                                        success: ((_ request: RKObjectRequestOperation?, _ result: RKMappingResult?) -> Void)?,
                                        failure: ((_ request: RKObjectRequestOperation?, _ error: Error?) -> Void)?,
                                        showFailureDialog: Bool = true) {
        //Refresh token if needed before making request
        
        guard SIANetworking.checkConnection() else {
            failure?(nil, nil)
            return
        }
        
        if (!["api/v1/email", "api/v1/signup", "api/v1/password", "api/v1/users/me", "api/v1/token"].contains(path) &&
            !BIALoginManager.userIsRegistered()) {
            assertionFailure("You tried to call an api with an unregistered user...\n****`\\\\|oO|//`****")
            failure!(nil, nil)
            return
        }
        
        refreshTokenBeforeMakingRequest({
            //Add access token and api version number1
            let query = appendTokenAndVersionTo(queryParameters)
            let queryString = createQueryStringFrom(dictionary: query)
            switch(requestMethod) {
            case RKRequestMethod.GET:
                SIAObjectManager.shared().getObjectsAtPath(path,
                                                           parameters: query,
                                                           success: success,
                                                           failure: addStatusCodeHandlerToFailure(failure, showFailureDialog))
                break;
            case RKRequestMethod.PUT:
                SIAObjectManager.shared().put(object,
                                              path: path + queryString,
                                              parameters: query,
                                              success: success,
                                              failure: addStatusCodeHandlerToFailure(failure, showFailureDialog))
                break;
            case RKRequestMethod.POST:
                SIAObjectManager.shared().post(object,
                                               path: path + queryString,
                                               parameters: query,
                                               success: success,
                                               failure: addStatusCodeHandlerToFailure(failure, showFailureDialog))
                break;
            case RKRequestMethod.DELETE:
                SIAObjectManager.shared().delete(object,
                                                 path: path + queryString,
                                                 parameters: query,
                                                 success: success,
                                                 failure: addStatusCodeHandlerToFailure(failure, showFailureDialog))
                break;
            default:
                 print("request method unsupported by RequestManager.swift")
                break;
            }
            
            }, failureClosure: failure, path: path)
        
    }
    
    static func getPrimitiveObjectAt(_ path: String,
                                     queryParameters: Dictionary<String, String>?,
                                     success: ((_ request: AFRKHTTPRequestOperation?, _ object: Any?) -> Void)?,
                                     failure: ((_ request: AFRKHTTPRequestOperation?, _ error: Error?) -> Void)?) {
        
        refreshTokenBeforeMakingRequest({
            
            let request = SIAObjectManager.shared().httpClient.request(withMethod: "GET",
                                                                       path: path,
                                                                       parameters: appendTokenAndVersionTo(queryParameters))
            
            let operation = AFRKJSONRequestOperation(request: request as URLRequest!)
            
            
            operation?.setCompletionBlockWithSuccess(success,
                                                     failure: failure)
            
            SIAObjectManager.shared().httpClient.enqueue(operation)
        },
                                        failureClosure: nil,
                                        path: path)
    }
    
    
    static func getRequest(_ path: String,
                           queryParameters: Dictionary<String, String>?,
                           success: ((_ request: RKObjectRequestOperation?, _ result: RKMappingResult?) -> Void)?,
                           failure: ((_ request: RKObjectRequestOperation?, _ error: Error?) -> Void)?,
                           showFailureDialog: Bool = true) {
        //Refresh token if needed before making request
        RequestManager.makeRequest(.GET,
                                   path: path,
                                   object: nil,
                                   queryParameters: queryParameters,
                                   success: success,
                                   failure: failure,
                                   showFailureDialog: showFailureDialog)
    }
    
    static func putRequest(_ path: String,
                           object: Any?,
                           queryParameters: Dictionary<String, String>?,
                           success: ((_ request: RKObjectRequestOperation?, _ result: RKMappingResult?) -> Void)?,
                           failure: ((_ request: RKObjectRequestOperation?, _ error: Error?) -> Void)?,
                           showFailureDialog: Bool = true) {
        //Refresh token if needed before making request
        RequestManager.makeRequest(.PUT,
                                   path: path,
                                   object: object,
                                   queryParameters: queryParameters,
                                   success: success,
                                   failure: failure,
                                   showFailureDialog: showFailureDialog)
    }
    
    static func postRequest(_ path: String,
                            object: Any?,
                            queryParameters: Dictionary<String, String>?,
                            success: ((_ request: RKObjectRequestOperation?, _ result: RKMappingResult?) -> Void)?,
                            failure: ((_ request: RKObjectRequestOperation?, _ error: Error?) -> Void)?,
                            showFailureDialog: Bool = true) {
        //Refresh token if needed before making request
        RequestManager.makeRequest(.POST,
                                   path: path,
                                   object: object,
                                   queryParameters: queryParameters,
                                   success: success,
                                   failure: failure,
                                   showFailureDialog: showFailureDialog)
    }
    
    static func deleteRequest(_ path: String,
                              object: Any?,
                              queryParameters: Dictionary<String, String>?,
                              success: ((_ request: RKObjectRequestOperation?, _ result: RKMappingResult?) -> Void)?,
                              failure: ((_ request: RKObjectRequestOperation?, _ error: Error?) -> Void)?,
                              showFailureDialog: Bool = true) {
        //Refresh token if needed before making request
        RequestManager.makeRequest(.DELETE,
                                   path: path,
                                   object: object,
                                   queryParameters: queryParameters,
                                   success: success,
                                   failure: failure,
                                   showFailureDialog: showFailureDialog)
    }
    
    
    fileprivate static func appendTokenAndVersionTo(_ queryParams: Dictionary<String, String>?) -> Dictionary<String, String>
    {
        var query = (queryParams == nil ? [String:String]() : queryParams)
        query!["access_token"] = UserDefaults.standard.object(forKey: "access_token") as! String?
        query!["v"] = RequestManager.versionNumber
        return query!
    }
    
    fileprivate static func createQueryStringFrom(dictionary: Dictionary<String, String>?) -> String
    {
        var paramsArray = [String]()
        for (key, value) in dictionary! {
            paramsArray.append(key + "=" + value)
        }
        var queryString = "?"
        for (index, param) in paramsArray.enumerated() {
            if (index == paramsArray.count - 1) {
                queryString += param
            } else {
                queryString += param + "&"
            }
        }
        return queryString
    }
    
    fileprivate static func refreshTokenBeforeMakingRequest(_ request: @escaping () -> Void,
                                                            failureClosure failure: ((_ request: RKObjectRequestOperation?, _ error: Error?) -> Void)?,
                                                            path: String) {
        //Dont refresh calling these apis
        if (path == "api/v1/token" ||
            path == "api/v1/google-token" ||
            path == "api/v1/signup" ||
            path == "api/v1/email" ||
            path == "api/v1/password" ||
            path == PCHStringDefinesWrapper.preloadedVideosPath()) {
            request()
            return
        }
        
        RequestManager.refreshTokenIfExpired(withCompletion: { (tokenValid: Bool) in
            guard (tokenValid) else {
                print("token refresh failed")
                failure?(nil, nil)
                return
            }
            request()
        })
    }
    
    fileprivate static func addStatusCodeHandlerToFailure(_ failure: ((_ request: RKObjectRequestOperation?, _ error: Error?) -> Void)?,
                                                          _ showFailureDialog: Bool = true)
        ->  ((_ request: RKObjectRequestOperation?, _ error: Error?) -> Void) {
            return ({ (_ request: RKObjectRequestOperation?, _ error: Error?) in
                
                //always call the failure closure
                defer {
                    failure?(request, error)
                }
                
                guard let statusCode = request?.httpRequestOperation?.response?.statusCode
                    else {
                        return
                }
                //handle status code before returning
                if (showFailureDialog) {
                    (UIApplication.shared.delegate as! AppDelegate).showNetworkDialog(for: statusCode)
                }
                switch (statusCode) {
                //OK
                case 200:
                    print("Status code 200: OK!");
                    break;
                //Created
                case 201:
                    print("Status code 201: Created something new.");
                    break;
                //No Content
                case 204:
                    print("Status code 204: Ok, no content.");
                    break;
                //Bad Request
                case 400:
                    print("Error code 400: Bad request, validation error.");
                    break;
                //Unauthorized
                case 401:
                    print("Error code 401: Unathorized. Active access token must be used.");
                    BIALoginManager.sharedInstance().refreshAccessToken(forCompletion: { (tokenValid: Bool) in
                        guard (tokenValid) else {
                            print("token refresh failed")
                            return
                        }
                    })
                    //                    [[PopUpView popUpView] show:@"Something is wrong with your user credentials. Please try again" withTitle:"Uh oh!" optionalButton:nil];
                    break;
                //Forbidden
                case 403:
                    print("Error code 403: Forbidden! You can't do that!");
                    break;
                //Method Not Allowed
                case 405:
                    print("Error code 405: Method not allowed.");
                    break;
                //Internal Server Error
                case 500:
                    print("Error code 500: Internal server error.");
                    //                    [[PopUpView popUpView] show:NSLocalizedString(@"We're having trouble connecting to Blingy. Can you try again in a minute?", nil) withTitle:NSLocalizedString(@"Network Error!", nil) optionalButton:nil];
                    
                    break;
                //Service Unavailable
                case 503:
                    print("Error code 503: Service unavailable.");
                    //                    [[PopUpView popUpView] show:NSLocalizedString(@"We're having trouble connecting to Blingy. Can you try again in a minute?", nil) withTitle:NSLocalizedString(@"Network Error!", nil) optionalButton:nil];
                    
                    break;
                    
                default:
                    break;
                }
                
            })
    }
    
    fileprivate static func refreshTokenIfExpired(withCompletion completion: ((_ success: Bool) -> Void)?) {
        if (accessTokenIsExpired()) {
            BIALoginManager.sharedInstance().refreshAccessToken(forCompletion: {(success: Bool) in
                if let completeAction = completion {
                    completeAction(success)
                }
            })
        } else {
            if let completeAction = completion {
                completeAction(true)
            }
        }
    }
    
    static func accessTokenIsExpired() -> Bool {
        var expireDate = UserDefaults.standard.object(forKey: kAccessTokenDate)
        let loginType = UserDefaults.standard.object(forKey: kLoginType)
        if (loginType as? String == kFacebookLoginType) {
            expireDate = FBSDKAccessToken.current()?.expirationDate
        }
        return (expireDate == nil || Date().isGreaterThanDate(dateToCompare:(expireDate as! Date)))
    }
    
}
