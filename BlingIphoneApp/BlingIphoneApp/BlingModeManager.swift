//
//  BlingModeManager.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 11/14/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//
protocol BlingModeManagerDelegate: class {
    func blingPauseTimeReached()
}

class BlingModeManager  {
    
    weak var backgroundVideo: BackgroundVideo?
    weak var player: BIAVideoPlayer?
    weak var delegate: BlingModeManagerDelegate?
    var triggeredTimes: [Float64] = []
    var timeObserver: Any?
    
    required init(_ backgroundVideo: BackgroundVideo,
                  player: BIAVideoPlayer,
                  delegate: BlingModeManagerDelegate) {
        self.backgroundVideo = backgroundVideo
        self.player = player
        self.delegate = delegate
        setupTimeObserver()
    }
    
    fileprivate func setupTimeObserver() {
        
        guard let times = backgroundVideo!.blingTimes, times.count > 0 else {
            return
        }
        print("BLING TIMES: \(times)")
        var boundaryTimes = [NSValue]()
        for time in (backgroundVideo?.blingTimes)! {
            if (time <= 2.5) { continue }
            let cmTime = CMTime(seconds: time,
                                preferredTimescale: 1000)
            boundaryTimes.append(NSValue(time: cmTime))
        }
        let blingClosure = {[weak self] in
            guard let _ = self, let player = self!.player else { return }
            let currentTime = CMTimeGetSeconds(player.currentTime())

            if let lastTime = self!.triggeredTimes.last, lastTime >= currentTime {
                print("same time! \(currentTime)")
                return
            }
            
            self!.triggeredTimes.append(currentTime)
            print("pausing at time: \(currentTime)")
            self!.delegate?.blingPauseTimeReached()
            
        }

        
        timeObserver = player?.addBoundaryTimeObserver(forTimes: boundaryTimes,
                                                       queue: DispatchQueue.main,
                                                       using: blingClosure)
    }
    
    deinit {
        if let _ = timeObserver {
           try? ObjCUtils.catchException {[weak self] in
                if let _ = self {
                    self!.player?.removeTimeObserver(self!.timeObserver!)
                }
            }
        }
    }
}
