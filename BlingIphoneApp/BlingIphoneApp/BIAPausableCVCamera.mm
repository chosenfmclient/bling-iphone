//
//  BIAPausableCVCamera.m
//  BlingIphoneApp
//
//  Created by Michael Biehl on 10/29/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import "BIAPausableCVCamera.h"
#import "SIAVideoEncoder.h"

static void *ExposureTargetOffsetContext = &ExposureTargetOffsetContext;

@interface BIAPausableCVCamera () {
    dispatch_queue_t sampleWritingQueue;
}

@property (strong, nonatomic) SIAVideoEncoder *encoder;
@property (nonatomic) CMTime pauseOffset;
@property (nonatomic) CMTime lastOffsetSampleTime;
@property (nonatomic) CMTime playerLagOffset;
@property (nonatomic) BOOL needsToAdjustOffset;
@property (nonatomic) BOOL shouldRecord;
@property (readwrite, nonatomic) BOOL isPaused;
@property (nonatomic) BOOL isRecording;
@property (nonatomic) BOOL didStart;
@property (nonatomic) AVCaptureDevicePosition currentCameraPosition;
@property (nonatomic, weak) AVCaptureDevice *currentCaptureDevice;
@property (nonatomic, readwrite) float currentFrameBrightness;
@property (nonatomic, copy) void (^exposureCompletionHandler)();

@end

@implementation BIAPausableCVCamera

- (BOOL)isRecording {
    return self.shouldRecord;
}

- (id)initWithParentView:(UIView *)parent {
    if (self = [super initWithParentView:parent]) {
        self.recordingSpeedScale = 1.0;
        self.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
        self.currentCameraPosition = AVCaptureDevicePositionFront;
        //[self adjustLayoutToInterfaceOrientation:UIInterfaceOrientationPortrait];
        //[self layoutPreviewLayer];
    }
    return self;
}

-(void)layoutPreviewLayer {
    self->customPreviewLayer.position = CGPointMake(self.parentView.frame.size.width/2., self.parentView.frame.size.height/2.);
    self->customPreviewLayer.affineTransform = CGAffineTransformIdentity;
    return;
}

-(void)adjustLayoutToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return;
}

- (void)setRecordingSpeedScale:(Float64)recordingSpeedScale {
    _recordingSpeedScale = recordingSpeedScale;
    NSLog(@"Recording speed scale set to: %f", _recordingSpeedScale);
}

- (void) start {
    sampleWritingQueue = dispatch_queue_create("gy.blin.sampleWritingQueue", DISPATCH_QUEUE_SERIAL);
    _shouldRecord = NO; // don't record at first DUMMY
    [super start];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(captureSessionWasInterrupted:)
                                                 name:AVCaptureSessionWasInterruptedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(captureSessionInterruptionEnded:) name:AVCaptureSessionInterruptionEndedNotification
                                               object:nil];

}

- (void)captureSessionWasInterrupted:(NSNotification *)note {
    NSLog(@"CAPTURE SESSION %@\nWAS INTERRUPTED: %@", note.object, [note.userInfo objectForKey:AVCaptureSessionInterruptionReasonKey]);
}

- (void)captureSessionInterruptionEnded:(NSNotification *)note {
    NSLog(@"CAPTURE SESSION INTERRUPTION ENDED");
}

- (void) startRecording {
    @synchronized (self) {
        _shouldRecord = YES;
    }
}

- (void) pauseRecording {
    @synchronized (self) {
        _isPaused = YES;
        _needsToAdjustOffset = YES;
    }
}

- (void) resumeRecording {
    @synchronized (self) {
        _isPaused = NO;
    }
}

- (void) stopRecording {
    BOOL wasRecording = _shouldRecord;
    @synchronized (self) {
        _shouldRecord = NO;
    }
    if (wasRecording || _isPaused)
    {
        __weak BIAPausableCVCamera *wSelf = self;
        dispatch_async(sampleWritingQueue, ^{
            [_encoder finishAtTime:_lastOffsetSampleTime
             withCompletionHandler:^(NSURL *outputURL) {
                 _recordURL = outputURL;
                 [wSelf stop];
                 _encoder = nil;
                 [Utils dispatchOnMainThread:^{
                     [wSelf.recordDelegate recordingDidStop];
                 }];
             }];
        });
    }
    else {
        dispatch_async(sampleWritingQueue, ^{
            [self stop];
        });    }
}

- (void) cancelRecording {
    @synchronized (self) {
        _shouldRecord = NO;
    }
    
    [_encoder cancelWriting];
    _encoder = nil;
}

- (void)reset {
    [self reset:NO];
}

- (void)reset:(BOOL)resetCaptureSession {
    _isPaused = NO;
    _needsToAdjustOffset = NO;
    _didStart = NO;
    if (_shouldRecord)
    {
        _shouldRecord = NO;
        [_encoder cancelWriting];
        _encoder = nil;
    }
    if (resetCaptureSession)
    {
        self.defaultAVCaptureDevicePosition = self.currentCameraPosition;
        NSLog(@"STOPPING CAPTURE SESSION");
        [self stop];
        NSLog(@"CAPTURE SESSION STOPPED");
        [super start];
    }
}



//- (void) captureOutput:(AVCaptureOutput *)captureOutput didDropSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
//    NSLog(@"DID DROP AT TIME: %f", CMTimeGetSeconds(CMSampleBufferGetPresentationTimeStamp(sampleBuffer)));
//}

- (void) captureOutput:(AVCaptureOutput *)captureOutput
 didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
        fromConnection:(AVCaptureConnection *)connection {
    
    NSDictionary *dataDict = (__bridge NSDictionary *)CMGetAttachment(sampleBuffer, kCGImagePropertyExifDictionary, NULL);
    NSString *brightnessStr = dataDict[(__bridge NSString *  _Nonnull __strong)(kCGImagePropertyExifBrightnessValue)];
    self.currentFrameBrightness = brightnessStr.floatValue;
    (void)captureOutput;
    (void)connection;
    // convert from Core Media to Core Video
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    void* bufferAddress;
    size_t width;
    size_t height;
    size_t bytesPerRow;
    
    CGColorSpaceRef colorSpace;
    CGContextRef context;
    
    int format_opencv;
    
    OSType format = CVPixelBufferGetPixelFormatType(imageBuffer);
    if (format == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) {
        
        format_opencv = CV_8UC1;
        
        bufferAddress = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
        width = CVPixelBufferGetWidthOfPlane(imageBuffer, 0);
        height = CVPixelBufferGetHeightOfPlane(imageBuffer, 0);
        bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0);
        
    } else { // expect kCVPixelFormatType_32BGRA
        
        format_opencv = CV_8UC4;
        
        bufferAddress = CVPixelBufferGetBaseAddress(imageBuffer);
        width = CVPixelBufferGetWidth(imageBuffer);
        height = CVPixelBufferGetHeight(imageBuffer);
        bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    }
    
    // delegate image processing to the delegate
    cv::Mat image((int)height, (int)width, format_opencv, bufferAddress, bytesPerRow);
    
    CGImage* dstImage;
    
    if ([self.cameraDelegate respondsToSelector:@selector(processImage:atTime:)]) {
        [self.cameraDelegate processImage:image
                                   atTime:_player.currentTime];
    }
    
    // check if matrix data pointer or dimensions were changed by the delegate
    bool iOSimage = false;
    if (height == (size_t)image.rows && width == (size_t)image.cols && format_opencv == image.type() && bufferAddress == image.data && bytesPerRow == image.step) {
        iOSimage = true;
    }
    
    
    // (create color space, create graphics context, render buffer)
    CGBitmapInfo bitmapInfo;
    OSType pixelFormat; //pixel format the encoder should expect
    // basically we decide if it's a grayscale, rgb or rgba image
    if (image.channels() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
        bitmapInfo = kCGImageAlphaNone;
        pixelFormat = kCVPixelFormatType_420YpCbCr8BiPlanarFullRange;
    } else if (image.channels() == 3) {
        colorSpace = CGColorSpaceCreateDeviceRGB();
        bitmapInfo = kCGImageAlphaNone;
        pixelFormat = kCVPixelFormatType_32RGBA;
        if (iOSimage) {
            bitmapInfo |= kCGBitmapByteOrder32Little;
        } else {
            bitmapInfo |= kCGBitmapByteOrder32Big;
        }
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
        bitmapInfo = kCGImageAlphaNoneSkipFirst;
        pixelFormat = kCVPixelFormatType_32ARGB;
        if (iOSimage) {
            bitmapInfo |= kCGBitmapByteOrder32Little;
        } else {
            bitmapInfo |= kCGBitmapByteOrder32Big;
        }
    }
    

    
    if (iOSimage) {
        context = CGBitmapContextCreate(bufferAddress, width, height, 8, bytesPerRow, colorSpace, bitmapInfo);
        dstImage = CGBitmapContextCreateImage(context);
        CGContextRelease(context);
    } else {
        
        NSData *data = [NSData dataWithBytes:image.data length:image.elemSize()*image.total()];
        CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
        
        // Creating CGImage from cv::Mat
        dstImage = CGImageCreate(image.cols,                                 // width
                                 image.rows,                                 // height
                                 8,                                          // bits per component
                                 8 * image.elemSize(),                       // bits per pixel
                                 image.step,                                 // bytesPerRow
                                 colorSpace,                                 // colorspace
                                 bitmapInfo,                                 // bitmap info
                                 provider,                                   // CGDataProviderRef
                                 NULL,                                       // decode
                                 false,                                      // should interpolate
                                 kCGRenderingIntentDefault                   // intent
                                 );
        
        CGDataProviderRelease(provider);
    }
    
    
    // render buffer
    dispatch_sync(dispatch_get_main_queue(), ^{
        self->customPreviewLayer.contents = (__bridge id)dstImage;
    });
    
    // lock the thread to allow a boolean to control pausing and unpausing without losing samples
    @synchronized (self) {
        
        if (!_shouldRecord || _isPaused) {
            // pause the bling playback here
            [self pausePlaybackAtTime:CMSampleBufferGetPresentationTimeStamp(sampleBuffer)];
            CGImageRelease(dstImage);
            CGColorSpaceRelease(colorSpace);
            CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
            return;
        }
        if (_encoder == nil)
        {
            _encoder = [SIAVideoEncoder encoderForPath:[self videoFileString]
                                                height:(int)CGImageGetHeight(dstImage)
                                                 width:(int)CGImageGetWidth(dstImage)
                                           pixelFormat:pixelFormat];
            //[_encoder setVideoOrientationFromCaptureOrientation:AVCaptureVideoOrientationPortrait];
            [self.recordDelegate recordingDidStart];
        }
        
        if (self.player && self.player.rate == 0 && !_didStart) {
            CMTime firstAudioTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
            self.player.currentItem.audioTimePitchAlgorithm = AVAudioTimePitchAlgorithmSpectral;

            [self.player setRate:_recordingSpeedScale
                            time:kCMTimeInvalid
                      atHostTime:firstAudioTime];
            // let the delegate know that the background video started with the time difference between master clock and sample time
            [self.recordDelegate backgroundPlaybackDidStartWithTimeOffset:CMTimeSubtract(CMClockGetTime(captureSession.masterClock),
                                                                                         firstAudioTime)];
            
        }
        
        if (!_didStart) {
            _didStart = YES;
        }
        
        if (_needsToAdjustOffset) {
            
            _needsToAdjustOffset = NO;
            CMTime timeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
            
            // if we already have an offset value (this is not the first pause) subtract it from the timestamp of the current sample
            if (_pauseOffset.flags & kCMTimeFlags_Valid) {
                timeStamp = CMTimeSubtract(timeStamp, _pauseOffset);
            }
            // calculate difference between the current sample and the last sample before the pause to update the pause offset
            CMTime offset = CMTimeSubtract(timeStamp, _lastOffsetSampleTime);
            
            NSLog(@"Adding %f to %f (pts %f)",
                  ((double)offset.value)/offset.timescale,
                  ((double)_lastOffsetSampleTime.value)/_lastOffsetSampleTime.timescale,
                  ((double)timeStamp.value/timeStamp.timescale));
            
            // If the global offset is zero, this is the first pause
            if (_pauseOffset.value == 0)
            {
                _pauseOffset = offset;
            }
            else // if global offset is not zero, add current offset to the global to accumulate total offset
            {
                _pauseOffset = CMTimeAdd(_pauseOffset, offset);
            }
            // now our global offset is set correctly
        }
        // if we have an offset value, adjust current sample presentation time
        CMTime timeStampForEncoding = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
        if (_pauseOffset.value > 0)
        {
            timeStampForEncoding = CMTimeSubtract(timeStampForEncoding, _pauseOffset);
        }
        
        //resume playback at the correct time
        if (self.player && self.player.rate == 0) {
            CMTime resumeTime = CMTimeAdd(CMSampleBufferGetPresentationTimeStamp(sampleBuffer), CMSampleBufferGetDuration(sampleBuffer));
            dispatch_async(dispatch_get_main_queue(), ^{
                [self resumePlaybackAtTime:resumeTime];
            });
        }
        
        // calculate the next presentation time: the timestamp + duration of the current sample, to use as the timestamp for the next sample that arrives after a pause
        CMTime duration = CMSampleBufferGetDuration(sampleBuffer);
        if (CMTIME_COMPARE_INLINE(_lastOffsetSampleTime, !=, timeStampForEncoding)) {
            _lastOffsetSampleTime = duration.value > 0 ? CMTimeAdd(timeStampForEncoding, duration) : timeStampForEncoding;
            CVPixelBufferRef pixelBuffer = [self pixelBufferFromCGImage:dstImage];
            CGImageRelease(dstImage);
            if (pixelBuffer) {
               // dispatch_async(sampleWritingQueue, ^{
                    [_encoder encodeVideoFrame:pixelBuffer
                              presentationTime:timeStampForEncoding];
                    
                    
               //     CVPixelBufferRelease(pixelBuffer);
               // });
            }
            else {
                NSLog(@"NO PIXEL BUFFER");
            }
        }
        // cleanup
        CGColorSpaceRelease(colorSpace);
        CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
    }
}

- (void)pausePlaybackAtTime:(CMTime)pauseTime {
    if (self.player && self.player.rate > 0)
    {
        if (CMTIME_IS_VALID(_playerLagOffset))
        {
            pauseTime = CMTimeAdd(pauseTime, _playerLagOffset);
            NSLog(@"Adding lag offset: %f", CMTimeGetSeconds(_playerLagOffset));
            // _playerLagOffset = kCMTimeInvalid;
        }
        
        BOOL doThePause = YES;
        if ([self.player respondsToSelector:@selector(automaticallyWaitsToMinimizeStalling)]) {
            doThePause = !self.player.automaticallyWaitsToMinimizeStalling;
        }
        if (doThePause) {
            [self.player setRate:0
                            time:kCMTimeInvalid//CMTimeSubtract(self.player.currentTime, _playerLagOffset)
                      atHostTime:pauseTime];//CMClockGetTime(captureSession.masterClock)];
        }

    }
}

- (void)resumePlaybackAtTime:(CMTime)resumeTime {
    if (self.player)
    {
        __block id obs = [self.player addBoundaryTimeObserverForTimes:@[[NSValue valueWithCMTime:CMTimeAdd(self.player.currentTime, CMTimeMake(1, 10))]]
                                                                     queue:nil
                                                                usingBlock:^{
                                                                    _playerLagOffset = CMTimeSubtract(CMTimeSubtract(CMClockGetTime(captureSession.masterClock), resumeTime) , CMTimeMake(1, 10));
                                                                    [self.player removeTimeObserver:obs];
                                                                }];
        if (CMTIME_IS_VALID(_playerLagOffset))
        {
            resumeTime = CMTimeSubtract(resumeTime, _playerLagOffset);
            NSLog(@"Adding lag offset: %f", CMTimeGetSeconds(_playerLagOffset));
        }
        NSLog(@"Resuming playback at rate: %f", _recordingSpeedScale);
        [self.player setRate:_recordingSpeedScale
                        time:kCMTimeInvalid//CMTimeSubtract(self.player.currentTime, _playerLagOffset)
                  atHostTime:kCMTimeInvalid];//resumeTime];
    }
}

- (CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image{
    
    CVPixelBufferRef pxbuffer = NULL;
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:NO], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:NO], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    
    size_t width =  CGImageGetWidth(image);
    size_t height = CGImageGetHeight(image);
    size_t bytesPerRow = CGImageGetBytesPerRow(image);
    
    // if bytesPerRow == width, the image is 8-bit format, otherwise it will be 32-bit (unless we have something else and then we screwed up)
    OSType pixelFormat = /*bytesPerRow == width ? kCVPixelFormatType_420YpCbCr8BiPlanarFullRange :*/ kCVPixelFormatType_32ARGB;
    
    CFDataRef  dataFromImageDataProvider = CGDataProviderCopyData(CGImageGetDataProvider(image));
    GLubyte  *imageData = (GLubyte *)CFDataGetBytePtr(dataFromImageDataProvider);
    
    
    CVPixelBufferCreateWithBytes(kCFAllocatorDefault,
                                 width,
                                 height,
                                 pixelFormat,
                                 imageData,
                                 bytesPerRow,
                                 NULL,
                                 NULL,
                                 NULL,
                                 &pxbuffer);
    
    CFRelease(dataFromImageDataProvider);
    
    return pxbuffer;
}

- (void)switchCameras
{
    if ([[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count] > 1)		//Only do if device has multiple cameras
    {
        NSLog(@"Toggle camera");
        NSError *error;
        AVCaptureDeviceInput *oldInput;
        for (AVCaptureDeviceInput *input in captureSession.inputs) {
            if ([input.device hasMediaType: AVMediaTypeVideo]){
                oldInput = input;
            }
        }
        AVCaptureDeviceInput *newVideoInput;
        AVCaptureDevicePosition position = [[oldInput device] position];
        if (position == AVCaptureDevicePositionBack)
        {
           // self.changeToBackCamera = NO;
            newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self videoDeviceWithPosition:AVCaptureDevicePositionFront] error:&error];
            self.currentCameraPosition = AVCaptureDevicePositionFront;
        }
        else if (position == AVCaptureDevicePositionFront)
        {
           //self.changeToBackCamera = YES;
            AVCaptureDevice *backCamera = [self videoDeviceWithPosition:AVCaptureDevicePositionBack];
            if ([backCamera isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
                [backCamera lockForConfiguration:nil];
                backCamera.focusMode = AVCaptureFocusModeContinuousAutoFocus;
                [backCamera unlockForConfiguration];
            }
            newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:backCamera error:&error];
            self.currentCameraPosition = AVCaptureDevicePositionBack;
        }
        else if (self.currentCameraPosition == AVCaptureDevicePositionFront) {
           // self.changeToBackCamera = YES;
            self.currentCameraPosition = AVCaptureDevicePositionBack;
        }
        else {
           // self.changeToBackCamera = NO;
        }
        
        if (newVideoInput != nil && oldInput != nil)
        {
            [captureSession beginConfiguration];		//We can now change the inputs and output configuration.  Use commitConfiguration to end
            [captureSession removeInput:oldInput];
            if ([captureSession canAddInput:newVideoInput])
            {
                [captureSession addInput:newVideoInput];
            } else [captureSession addInput:oldInput];
            
            AVCaptureVideoDataOutput *videoOut;
            for (id output in captureSession.outputs)
            {
                videoOut = [output isKindOfClass:[AVCaptureVideoDataOutput class]] ? output : videoOut;
            }
            
            //Set the connection properties again
           AVCaptureConnection *videoConnection = [videoOut connectionWithMediaType:AVMediaTypeVideo];//[movieFileOutput connectionWithMediaType:AVMediaTypeVideo];
            videoConnection.videoOrientation = self.defaultAVCaptureVideoOrientation;
            videoConnection.videoMirrored = self.currentCameraPosition == AVCaptureDevicePositionFront;
            
            [captureSession commitConfiguration];
        }
    }
}

- (void) lockCurrentDeviceAutoAdjustment {
    [self updateDeviceAutoAdjustmentLocked:YES];
//    [self removeObserver:self forKeyPath:@"captureDevice.exposureTargetOffset"];
}

//-(void)dealloc {
//    @try {
//        [self removeObserver:self forKeyPath:@"captureDevice.exposureTargetOffset"];
//    } @catch (NSException *exception) {
//        ;
//    }
//}

- (void) unlockCurrentDeviceAutoAdjustment {
    [self updateDeviceAutoAdjustmentLocked:NO];
//    [self addObserver:self forKeyPath:@"captureDevice.exposureTargetOffset" options:NSKeyValueObservingOptionNew context:ExposureTargetOffsetContext];
//    NSError *error;
//    if ([self.captureDevice lockForConfiguration:&error]) {
//        if ([self.captureDevice isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure])
//        self.captureDevice.exposureMode = AVCaptureExposureModeContinuousAutoExposure;
//        [self.captureDevice unlockForConfiguration];
//    }
//    [self setISO:self.iso ByExposureDuration:(0.1/10)];
}

- (void) setISO:(CGFloat)newISO ByExposureDuration:(CGFloat)duration{
    

    NSError *error;
    if ([self.captureDevice lockForConfiguration:&error]) {

        [self.captureDevice setExposureModeCustomWithDuration:CMTimeMakeWithSeconds(duration, 1000000000) ISO:newISO completionHandler:^(CMTime syncTime) {
            
                CGFloat offset = self.captureDevice.exposureTargetOffset;

                if (offset < -0.2)  {
                    [self setISO:(newISO + 5) ByExposureDuration:duration];
                }
                else if (offset > 0.2) {
                    [self setISO:(newISO - 5) ByExposureDuration:duration];
                }
                else {
                    NSError *error;
                    if ([self.captureDevice lockForConfiguration:&error]) {

                        if ([self.captureDevice isExposureModeSupported:AVCaptureExposureModeLocked])
                            self.captureDevice.exposureMode = AVCaptureExposureModeLocked;
                        [self.captureDevice unlockForConfiguration];
                    }
                }
            
            
            return;
        }];
        [self.captureDevice unlockForConfiguration];
    }
    
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if (context == ExposureTargetOffsetContext){
        float newExposureTargetOffset = [change[NSKeyValueChangeNewKey] floatValue];
        NSLog(@"Offset is : %f",newExposureTargetOffset);
        
        if(!self.captureDevice) return;
        
        CGFloat currentISO = self.iso;
        CGFloat biasISO = 0;
        
        //Assume 0,3 as our limit to correct the ISO
        if(newExposureTargetOffset > 0.3f) //decrease ISO
            biasISO = -50;
        else if(newExposureTargetOffset < -0.3f) //increase ISO
            biasISO = 50;
        
        if(biasISO){
            //Normalize ISO level for the current device
            CGFloat newISO = currentISO+biasISO;
            newISO = newISO > self.captureDevice.activeFormat.maxISO? self.captureDevice.activeFormat.maxISO : newISO;
            newISO = newISO < self.captureDevice.activeFormat.minISO? self.captureDevice.activeFormat.minISO : newISO;
            
            NSError *error = nil;
            if ([self.captureDevice lockForConfiguration:&error]) {
                [self.captureDevice setExposureModeCustomWithDuration:AVCaptureExposureDurationCurrent ISO:newISO completionHandler:^(CMTime syncTime) {}];
                [self.captureDevice unlockForConfiguration];
            }
        }
    }
    else if (object == self.currentCaptureDevice) {
        if (self.currentCaptureDevice.exposureMode == AVCaptureExposureModeLocked) {
            if (self.exposureCompletionHandler != nil) {
                self.exposureCompletionHandler();
                self.exposureCompletionHandler = nil;
            }
            [self.captureDevice removeObserver:self
                                    forKeyPath:@"isAdjustingExposure"];
        }
    }
}

- (void) updateDeviceAutoAdjustmentLocked:(BOOL)locked {
    AVCaptureDevice *device = self.currentCaptureDevice;
    
    AVCaptureExposureMode exposureMode = locked ? AVCaptureExposureModeLocked : AVCaptureExposureModeContinuousAutoExposure;
    AVCaptureFocusMode focusMode = locked ? AVCaptureFocusModeLocked : AVCaptureFocusModeContinuousAutoFocus;
    AVCaptureWhiteBalanceMode balanceMode = locked ? AVCaptureWhiteBalanceModeLocked : AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance;
    
    NSError *error;
    if ([device lockForConfiguration:&error]) {
//        [self.captureDevice setExposureModeCustomWithDuration:CMTimeMakeWithSeconds(0.01, 1000*1000*1000) ISO:200 completionHandler:^(CMTime syncTime) {}];
        if ([device isExposureModeSupported:exposureMode])
            device.exposureMode = exposureMode;
        if ([device isFocusModeSupported:focusMode])
            device.focusMode = focusMode;
        if ([device isWhiteBalanceModeSupported:balanceMode])
            device.whiteBalanceMode = balanceMode;
        [device unlockForConfiguration];
    }
    else {
        NSLog(@"ERROR SETTING CAMERA AUTO ADJUSTMENT MODES: %@", [error localizedDescription]);
    }
}

#pragma mark Copied from openCV so we can fix their crappy bugs

- (AVCaptureDevice *)videoDeviceWithPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position) {
            return device;
        }
    }
    return nil;
}

- (void)setDesiredCameraPosition:(AVCaptureDevicePosition)desiredPosition;
{
    for (AVCaptureDevice *device in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
        [self removeAdjustingExposureObserverFrom:device];
        if ([device position] == desiredPosition) {
            [self.captureSession beginConfiguration];
            
            NSError* error = nil;
            AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
            if (!input) {
                NSLog(@"error creating input %@", [error localizedDescription]);
                if ([self.recordDelegate respondsToSelector:@selector(recorderDidFailToInitialize:)]) {
                    [self.recordDelegate recorderDidFailToInitialize:error];
                }
                return;
            }
            error = nil;
            if ([device lockForConfiguration:&error]) {
                device.subjectAreaChangeMonitoringEnabled = YES;
                // support for autofocus
                if ([device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
                    device.focusMode = AVCaptureFocusModeContinuousAutoFocus;
                }
                [device unlockForConfiguration];
            }
            else {
                NSLog(@"COULDN'T LOCK DEVICE FOR CONFIG: %@", error);
            }

            //[self.captureSession addInput:input];
            
            for (AVCaptureInput *oldInput in self.captureSession.inputs) {
                [self.captureSession removeInput:oldInput];
            }
            [self.captureSession addInput:input];
            self.currentCaptureDevice = device;
            [self.captureSession commitConfiguration];
            
            
            break;
        }
    }
}

- (void)setCurrentCaptureDevice:(AVCaptureDevice *)currentCaptureDevice {
    [self removeAdjustingExposureObserverFrom:_currentCaptureDevice];
    _currentCaptureDevice = currentCaptureDevice;
}

- (void)startCaptureSession
{
    if (!cameraAvailable) {
        return;
    }
    
    if (self.captureSessionLoaded == NO) {
        [self createCaptureSession];
        [self createCaptureDevice];
        if (self.captureSession.inputs.count < 1) return;
        [self createCaptureOutput];
        
        // setup preview layer
        if (self.useAVCaptureVideoPreviewLayer) {
            [self createVideoPreviewLayer];
        } else {
            [self createCustomVideoPreview];
        }
        
        captureSessionLoaded = YES;
    }
    
    [Utils dispatchInDefaultPriority:^{
        [captureSession startRunning];
        if (captureSession.isRunning) {
            NSLog(@"CAPTURE SESSION RESTARTED SUCCESSFULLY");
        }
        else {
            NSLog(@"CAPTURE SESSION FAILED TO RESTART");
        }
    }];
    
}

- (void)createCaptureSession;
{
    // set a av capture session preset
    self.captureSession = [[AVCaptureSession alloc] init];
    if ([self.captureSession canSetSessionPreset:self.defaultAVCaptureSessionPreset]) {
        [self.captureSession setSessionPreset:self.defaultAVCaptureSessionPreset];
    } else if ([self.captureSession canSetSessionPreset:AVCaptureSessionPresetLow]) {
        [self.captureSession setSessionPreset:AVCaptureSessionPresetLow];
    } else {
        NSLog(@"[Camera] Error: could not set session preset");
    }
}

- (void)createCaptureDevice;
{
    // setup the device
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    [self setDesiredCameraPosition:self.defaultAVCaptureDevicePosition];
    NSLog(@"[Camera] device connected? %@", device.connected ? @"YES" : @"NO");
    NSLog(@"[Camera] device position %@", (device.position == AVCaptureDevicePositionBack) ? @"back" : @"front");
}

-(AVCaptureDevice *)captureDevice{
    return self.currentCaptureDevice;
}

- (void)createCustomVideoPreview;
{
    [self.parentView.layer addSublayer:self->customPreviewLayer];
}

- (AVCaptureWhiteBalanceGains) deviceWhiteBalanceGains {
    return self.currentCaptureDevice.deviceWhiteBalanceGains;
}

- (AVCaptureWhiteBalanceTemperatureAndTintValues) temperatureAndTintValues {
    return [self.currentCaptureDevice temperatureAndTintValuesForDeviceWhiteBalanceGains:[self deviceWhiteBalanceGains]];
}

- (AVCaptureWhiteBalanceChromaticityValues) chromaticityValues{
    return [self.currentCaptureDevice chromaticityValuesForDeviceWhiteBalanceGains:[self deviceWhiteBalanceGains]];
}

- (float) exposureTargetBias {
    return self.currentCaptureDevice.exposureTargetBias;
}

- (float) exposureTargetOffset {
    return self.currentCaptureDevice.exposureTargetOffset;
}

- (double) exposureDuration {
    return CMTimeGetSeconds(self.currentCaptureDevice.exposureDuration);
}

- (float) iso {
    return self.currentCaptureDevice.ISO;
}

- (void)adjustExposureWithCompletion:(void (^)(void))completion {
    NSError *error;
    if ([self.currentCaptureDevice lockForConfiguration:&error]) {
        if ([self.currentCaptureDevice isExposureModeSupported:AVCaptureExposureModeAutoExpose]) {
            [self.currentCaptureDevice setExposureMode:AVCaptureExposureModeAutoExpose];
        }
        [self.currentCaptureDevice unlockForConfiguration];
        
        if (self.currentCaptureDevice.exposureMode == AVCaptureExposureModeAutoExpose && self.currentCaptureDevice.isAdjustingExposure) {
            self.exposureCompletionHandler = completion;
            [self.currentCaptureDevice addObserver:self
                                        forKeyPath:@"exposure​Mode"
                                           options:NSKeyValueObservingOptionNew
                                           context:nil];
        }
        else {
            completion();
        }
    }
    else {
        NSLog(@"FAILED TO SET AUTOEXPOSE MODE ON CURRENT DEVICE: %@", self.currentCaptureDevice);
    }
        
}

- (void)removeAdjustingExposureObserverFrom:(AVCaptureDevice*)device {
    @try {
        [device removeObserver:self
                    forKeyPath:@"exposure​Mode"];
    } @catch (NSException *exception) {
        
    }
}

- (void)dealloc {
    [self removeAdjustingExposureObserverFrom:self.currentCaptureDevice];
}



@end
