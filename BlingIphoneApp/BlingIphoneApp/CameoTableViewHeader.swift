//
//  cameoTableViewHeader.swift
//  BlingIphoneApp
//
//  Created by Zach on 07/12/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
class CameoTableViewHeader: UIView {
    
    @IBOutlet weak var selectAllButton: UIButton!
    var view: UIView?
    var selectAll: (() -> Void)?
    @IBOutlet weak var headerLabel: UILabel!
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        view = (Bundle.main.loadNibNamed("CameoTableViewHeader", owner: self, options: nil)![0] as! UIView)
        view?.frame = frame
        self.addSubview(view!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectAllButton.bia_rounded()
    }
    
    func setText(_ text: String, andAction action: @escaping (() -> Void)) {
        selectAll = action
        headerLabel.text = text
    }
    
    @IBAction func selectAllWasTapped(_ sender: Any) {
        selectAll?()
    }
    
}
