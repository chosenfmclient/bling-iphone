//
//  NSString+SIAAdditions.h
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/3/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    SIAPasswordStrengthTypeWeak,
    SIAPasswordStrengthTypeModerate,
    SIAPasswordStrengthTypeStrong
} SIAPasswordStrengthType;

@interface NSString (SIAAdditions)
- (SIAPasswordStrengthType)sia_passwordStrength;
@end

@interface NSAttributedString (SIAAdditions)
+ (NSAttributedString *)sia_attributedStringWithHTML:(NSString *)html;

+ (NSAttributedString *)sia_attributedStringWithHTML: (NSString *)html
                                            andColor: (NSString*) hexColor
                                        andBoldColor: (NSString*) hexBoldColor
                                             andFont: (NSString*) fontName
                                         andFontSize: (NSUInteger) fontSize;
@end