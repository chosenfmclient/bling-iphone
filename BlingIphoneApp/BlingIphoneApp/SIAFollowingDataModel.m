//
//  SIAFollowingDataModel.m
//  ChosenIphoneApp
//
//  Created by Roni Shoham on 09/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAFollowingDataModel.h"

@implementation SIAFollowingDataModel

- (NSString *)pagePath {
    return [NSString stringWithFormat:FOLLOWING_API, self.userId];
}

+ (RKMapping *)pageMapping {
    return [SIAPaginatorDataModel responseMappingForTotalKey:@"total_objects"
                                               objectMapping:[SIAUser objectMapping]
                                                  modelClass:[SIAFollowingDataModel class]];
}
@end
