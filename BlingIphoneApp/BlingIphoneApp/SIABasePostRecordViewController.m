//
//  SIABasePostRecordViewController.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/17/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIABasePostRecordViewController.h"

@interface SIABasePostRecordViewController ()

@end

@implementation SIABasePostRecordViewController

- (void)next {
    if ([self.delegate respondsToSelector:@selector(next)])
        [self.delegate next];
}

- (void) back {
    if ([self.delegate respondsToSelector:@selector(back)])
        [self.delegate back];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}


@end
