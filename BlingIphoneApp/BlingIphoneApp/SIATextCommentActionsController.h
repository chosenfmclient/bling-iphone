//
// Created by Hezi Cohen on 1/19/16.
// Copyright (c) 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SIATextCommentActionsController : NSObject
- (instancetype)initWithUser:(SIAUser*)user;
- (SIAAlertController *)actionsAlertControllerForComment:(SIATextComment*)comment presentingController:(UIViewController*)vc;
@end