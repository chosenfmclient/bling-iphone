//
//  FollowingList.swift
//  BlingIphoneApp
//
//  Created by Zach on 07/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit
@objc class FollowingList: NSObject {
    var followed_by_me: [Int]?
    
    static func objectMapping() -> RKObjectMapping {
        let objectMapping = RKObjectMapping(for: FollowingList.self)
        objectMapping?.addAttributeMappings(from: ["followed_by_me"])
        return objectMapping!
    }
}
