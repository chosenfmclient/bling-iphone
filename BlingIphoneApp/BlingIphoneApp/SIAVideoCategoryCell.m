//
//  SIAVideoCategoryCell.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/10/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAVideoCategoryCell.h"

@implementation SIAVideoCategoryCell

- (void)awakeFromNib {
    // Initialization code
    
}

- (void) layoutSubviews {
    
    self.categoryBackground.layer.cornerRadius = self.categoryBackground.bounds.size.height / 2;
    self.categoryBackground.layer.masksToBounds = YES;

}
@end
