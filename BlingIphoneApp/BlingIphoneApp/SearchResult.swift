//
//  SearchResult.swift
//  BlingIphoneApp
//
//  Created by Zach on 29/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit
@objc class SearchResult: NSObject {
    var users = [SIAUser]()
    var videos = [MusicVideo]()
    
    static func responseMapping() -> RKObjectMapping {
        let objectMapping = RKObjectMapping(for: SearchResult.self)
        objectMapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "users", toKeyPath: "users", with: SIAUser.objectMapping()))
        objectMapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "results", toKeyPath: "videos", with: MusicVideo.objectMapping()))
        return objectMapping!
    }
}
