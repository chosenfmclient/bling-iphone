//
//  SIATextComment.m
//  ChosenIphoneApp
//
//  Created by Joseph Nahmias on 12/01/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIATextComment.h"
#import "SIATextCommentModel.h"

NSString *const SIACommentUpdateNotification = @"SIACommentUpdateNotification";

@implementation SIATextComment

- (instancetype)initWithText:(NSString *)commentText videoId:(NSString *)videoId{
    
    self = [super init];
    if (self) {
        self.text = commentText;
        self.videoId = videoId;
    }
    return self;
}

+ (RKObjectMapping *)objectMapping{
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[SIATextComment class]];
    [objectMapping addAttributeMappingsFromDictionary:@{@"comment_id":@"commentId",
                                                        @"video_id":@"videoId",
                                                        @"text":@"text",
                                                        @"date":@"date",}];
    
    [objectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"user" toKeyPath:@"user" withMapping:[SIAUser objectMapping]]];
    [objectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"video" toKeyPath:@"video" withMapping:[SIAVideo objectMapping]]];
    
    return objectMapping;
}

- (void)postCommentWithCompletionHandler:(void(^)(BOOL success))handler{
    if (self.videoId.length < 1 || self.text.length < 1) {
        handler(NO);
    } else {        
        [SIATextCommentModel postComment:self.text forVideo:self.videoId withCompletionHandler:^(SIATextComment *model) {
            self.commentId = model.commentId;
            handler(YES);
        }];
    }
}

- (void)editComment:(NSString *)newText withCompletionHandler:(void(^)(BOOL success))handler{
    if (self.commentId.length < 1 || newText.length < 1) {
        handler(NO);
    } else {
        NSDictionary *ui = @{@"edited":@(YES)};
        
        [[NSNotificationCenter defaultCenter] postNotificationName:SIACommentUpdateNotification
                                                            object:self
                                                          userInfo:ui];
        self.text = newText;
        
        [SIATextCommentModel editComment:self.commentId
                                 newText:newText
                   withCompletionHandler:handler];
    }
}

- (void)deleteCommentWithCompletionHandler:(void(^)(BOOL success))handler{
    if (self.commentId.length < 1) {
        handler(NO);
    } else {
        [SIATextCommentModel deleteComment:self.commentId withCompletionHandler:handler];
    }
}

- (void)flagCommentAsType:(SIATextCommetFlagType)type withCompletionHandler:(void(^)(BOOL success))handler{
    if (self.commentId.length < 1) {
        handler(NO);
    } else {
        [SIATextCommentModel flagComment:self.commentId asType:type withCompletionHandler:handler];
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"user name %@: text %@ video id(%@) comment id: %@", self.user.fullName, self.text, self.videoId, self.commentId];
}

- (BOOL)isEqual:(id)object
{
    return [[self commentId] isEqualToString:[object commentId]];
}

@end
