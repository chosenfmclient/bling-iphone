//
//  searchManager.swift
//  BlingIphoneApp
//
//  Created by Zach on 29/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit
@objc class SearchManager: NSObject, UITableViewDataSource, PageRequestHandlerDelegate, FollowedDelegate {
    var userSearchList = [SIAUser?]()
    var videoSearchList = [MusicVideo?]()
    var isUserSearch = true
    var currentQuery = ""
    weak var tableView: UITableView?
    weak var noDataView: UIView?
    var pageRequestHandler: PageRequestHandler?
    
    func search(query: String, isUserSearch: Bool) {
        guard (query != currentQuery || isUserSearch != self.isUserSearch) && query != "" else {
            return
        }
        videoSearchList = [nil]
        userSearchList = [nil]
        tableView?.reloadData()
        currentQuery = query
        self.isUserSearch = isUserSearch
        if isUserSearch {
            pageRequestHandler = PageRequestHandler(apiPath: "api/v1/search", delegate: self, requestIdentifier: query + String(self.isUserSearch), queryParameters: ["type":"user", "q": query])
        } else {
            pageRequestHandler = PageRequestHandler(apiPath: "api/v1/search", delegate: self, requestIdentifier: query + String(self.isUserSearch), queryParameters: ["type":"clip", "q": query])
        }
        pageRequestHandler?.fetchData()
        AmplitudeEvents.log(event: isUserSearch ? "search:ugc" : "search:seed" , with: ["search_text":query])
    }
    
    func dataWasLoaded(page: Int, data: Any?, requestIdentifier: Any?) {
        guard self.currentQuery + String(isUserSearch) == requestIdentifier as? String else {
            return
        }
        guard let searchResult = data as? SearchResult else {
            return
        }
        noDataView?.isHidden = true
        userSearchList.removeLast()
        videoSearchList.removeLast()
        for user in searchResult.users {
            userSearchList.append(user)
        }
        for video in searchResult.videos {
            videoSearchList.append(video)
        }
        userSearchList.append(nil)
        videoSearchList.append(nil)
        self.tableView?.reloadData()
        
        if !isUserSearch { // no paging for videos
            endOfContentReached(at: 1, requestIdentifier: requestIdentifier)
        }
    }
    
    func endOfContentReached(at page: Int, requestIdentifier: Any?) {
        guard self.currentQuery + String(isUserSearch) == requestIdentifier as? String else {
            return
        }
        if page == 0 {
            noDataView?.isHidden = false
        }
        userSearchList.removeLast()
        videoSearchList.removeLast()
        tableView?.reloadData()
    }
    
    func contactWasUpdated() {
        tableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ((isUserSearch && userSearchList[indexPath.row] == nil) ||
            (!isUserSearch && videoSearchList[indexPath.row] == nil)) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath)
            return cell
        }
        
        let identifier = isUserSearch ? "userCell" : "videoCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        if (isUserSearch) {
            let userCell = cell as! FollowContactTableViewCell
            userCell.delegate = self
            userCell.user = userSearchList[indexPath.row]
            userCell.userNameLabel.text = userCell.user?.fullName
            userCell.followButton.setImage(UIImage(named: userCell.user!.followedByMe ? "icon_followed_grey" : "icon_follow_green"), for: .normal)
            cell = userCell
        } else {
            let videoCell = cell as! VideoSearchTableViewCell
            videoCell.musicVideo = videoSearchList[indexPath.row]
            videoCell.artistNameLabel.text = videoCell.musicVideo?.artistName
            videoCell.songNameLabel.text = videoCell.musicVideo?.trackName
            cell = videoCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell is SIALoadingTableViewCell {
            pageRequestHandler?.fetchData()
        }
    }    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isUserSearch ? userSearchList.count : videoSearchList.count
    }
}
