//
// Created by Hezi Cohen on 1/19/16.
// Copyright (c) 2016 SingrFM. All rights reserved.
//

#import "SIATextCommentActionsController.h"
#import "SIATextComment.h"

@implementation SIATextCommentActionsController {
    SIAUser *_currentUser;
}

- (instancetype)initWithUser:(SIAUser*)user
{
    self = [super init];
    if (self) {
        _currentUser = user;
    }

    return self;
}


- (SIAAlertController *)actionsAlertControllerForComment:(SIATextComment*)comment presentingController:(UIViewController*)vc
{
    BOOL deleteEnabled = NO;

    SIAAlertController *commentAlert = [SIAAlertController alertControllerWithTitle:@"COMMENT" message:nil];

    if (_currentUser && [[comment.user user_id] isEqualToString:[_currentUser user_id]]) {
        [commentAlert addAction:[SIAAlertAction actionWithTitle:@"EDIT" style:SIAAlertActionStyleDefault handler:^(SIAAlertAction *action) {
            [vc dismissViewControllerAnimated:YES completion:^{
                SIATextCommentView *commentView = [[SIATextCommentView alloc] initWithFrame:CGRectMake(0, vc.view.frame.size.height + 50, vc.view.frame.size.width, 50)];
                commentView.name = [_currentUser fullName];
                commentView.image = [_currentUser image];
                commentView.delegate = vc;
                [commentView addActionWithType:SIATextCommentActionUpdateType];
                [commentView addActionWithType:SIATextCommentActionCancelType];
                [vc.view addSubview:commentView];
                [commentView beginCommentEditingWithComment:comment.text];
            }];
        }]];

        if(deleteEnabled){
            [commentAlert addAction:[SIAAlertAction actionWithTitle:@"DELETE" style:SIAAlertActionStyleDefault handler:^(SIAAlertAction *action) {
                [vc dismissViewControllerAnimated:YES completion:^{
                    SIAAlertController *deleteAlert = [SIAAlertController alertControllerWithTitle:@"Are you sure?" message:@"Are you sure you want to delete this comment?"];
                    [deleteAlert addAction:[SIAAlertAction actionWithTitle:@"CANCEL" style:(SIAAlertActionStyleCancel) handler:nil]];
                    [deleteAlert addAction:[SIAAlertAction actionWithTitle:@"DELETE" style:(SIAAlertActionStyleConfirmation) handler:^(SIAAlertAction *action) {
                        [comment deleteCommentWithCompletionHandler:^(BOOL success) {
                            if (!success)
                                return;

//                            [self.paginator.results removeObject:_selectedComment];
                            [vc dismissViewControllerAnimated:YES completion:nil];
                        }];
                    }]];

                    [vc presentViewController:deleteAlert animated:YES completion:nil];
                }];

            }]];
        }
    }

    if(![[comment.user user_id] isEqualToString:[_currentUser user_id]])
    {
        [commentAlert addAction:[SIAAlertAction actionWithTitle:@"FLAG" style:SIAAlertActionStyleDefault handler:^(SIAAlertAction *action) {
            [vc dismissViewControllerAnimated:YES completion:^ {
                SIAAlertController *flagAlert = [SIAAlertController alertControllerWithTitle:@"Choose flag reason" message:nil];

                SIAAlertAction *inappropAction = [SIAAlertAction actionWithTitle:@"INAPPROPRIATE" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                    [vc dismissViewControllerAnimated:YES completion:^{
                        SIAAlertController *inappropAlert = [SIAAlertController alertControllerWithTitle:@"Choose flag reason" message:nil];
                        [inappropAlert addAction:[SIAAlertAction actionWithTitle:@"SEXUAL CONTENT" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                            [comment flagCommentAsType:SIATextCommetFlagTypeSexual withCompletionHandler:^(BOOL success) {
                                [vc dismissViewControllerAnimated:YES completion:^ {
                                    [self _presentConfirmationAlert:vc];
                                }];
                            }];
                        }]];

                        [inappropAlert addAction:[SIAAlertAction actionWithTitle:@"VIOLENT OR REPULSIVE CONTENT" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                            [comment flagCommentAsType:SIATextCommetFlagTypeViolent withCompletionHandler:^(BOOL success) {
                                [vc dismissViewControllerAnimated:YES completion:^ {
                                    [self _presentConfirmationAlert:vc];
                                }];
                            }];
                        }]];

                        [inappropAlert addAction:[SIAAlertAction actionWithTitle:@"HATEFUL OR ABUSIVE CONTENT" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                            [comment flagCommentAsType:SIATextCommetFlagTypeHateful withCompletionHandler:^(BOOL success) {
                                [vc dismissViewControllerAnimated:YES completion:^ {
                                    [self _presentConfirmationAlert:vc];
                                }];
                            }];
                        }]];

                        [inappropAlert addAction:[SIAAlertAction actionWithTitle:@"HARMFUL DANGEROUS ACTS" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                            [comment flagCommentAsType:SIATextCommetFlagTypeHarmful withCompletionHandler:^(BOOL success) {
                                [vc dismissViewControllerAnimated:YES completion:^ {
                                    [self _presentConfirmationAlert:vc];
                                }];
                            }];
                        }]];

                        [inappropAlert addAction:[SIAAlertAction actionWithTitle:@"INFRINGES MY RIGHTS" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                            [comment flagCommentAsType:SIATextCommetFlagTypeInfringing withCompletionHandler:^(BOOL success) {
                                [vc dismissViewControllerAnimated:YES completion:^ {
                                    [self _presentConfirmationAlert:vc];
                                }];
                            }];
                        }]];

                        SIAAlertAction *cancelAction = [SIAAlertAction actionWithTitle:@"CANCEL" style:(SIAAlertActionStyleCancel) handler:nil];
                        [inappropAlert addAction:cancelAction];
                        SIAAlertAction *flagAction = [SIAAlertAction actionWithTitle:@"FLAG" style:(SIAAlertActionStyleConfirmation) handler:nil];
                        [inappropAlert addAction:flagAction];

                        [inappropAlert setActionSize: CGSizeMake(300, 48)];

                        [vc presentViewController:inappropAlert animated:YES completion:nil];
                    }];
                }];
                [flagAlert addAction:inappropAction];

                SIAAlertAction *spamAction = [SIAAlertAction actionWithTitle:@"SPAM" style:(SIAAlertActionStyleDefault) handler:^(SIAAlertAction *action) {
                    [vc dismissViewControllerAnimated:YES completion:^{
                        SIAAlertController *spamAlert = [SIAAlertController alertControllerWithTitle:@"Are you sure?" message:@"Are you sure this comment contains SPAM content?"];
                        SIAAlertAction *cancelAction = [SIAAlertAction actionWithTitle:@"CANCEL" style:(SIAAlertActionStyleCancel) handler:nil];
                        [spamAlert addAction:cancelAction];
                        SIAAlertAction *flagAction = [SIAAlertAction actionWithTitle:@"FLAG" style:(SIAAlertActionStyleConfirmation) handler:^(SIAAlertAction *action) {
                            [comment flagCommentAsType:SIATextCommetFlagTypeSpam withCompletionHandler:^(BOOL success) {
                                [vc dismissViewControllerAnimated:YES completion:^ {
                                    [self _presentConfirmationAlert:vc];
                                }];
                            }];
                        }];
                        [spamAlert addAction:flagAction];
                        [vc presentViewController:spamAlert animated:YES completion:nil];
                    }];
                }];
                [flagAlert addAction:spamAction];

                SIAAlertAction *cancelAction = [SIAAlertAction actionWithTitle:@"CANCEL" style:(SIAAlertActionStyleCancel) handler:nil];
                [flagAlert addAction:cancelAction];

                SIAAlertAction *nextAction = [SIAAlertAction actionWithTitle:@"NEXT" style:(SIAAlertActionStyleConfirmation) handler:nil];
                [flagAlert addAction:nextAction];

                [vc presentViewController:flagAlert animated:YES completion:nil];
            }];
        }]];
    }

    [commentAlert addAction:[SIAAlertAction actionWithTitle:@"CANCEL" style:SIAAlertActionStyleCancel handler:nil]];
    [commentAlert addAction:[SIAAlertAction actionWithTitle:@"NEXT" style:SIAAlertActionStyleConfirmation handler:nil]];

    return commentAlert;
}

- (void)_presentConfirmationAlert:(UIViewController*)vc
{
    SIAAlertController *alert = [SIAAlertController alertControllerWithTitle:@"Blingy" message:@"Thank you for your report"];
    [alert addAction:[SIAAlertAction actionWithTitle:@"OK" style:(SIAAlertActionStyleCancel) handler:nil]];
    [vc presentViewController:alert animated:YES completion:nil];

}
@end
