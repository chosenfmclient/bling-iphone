//
//  DiscoverSearchBarView.swift
//  BlingIphoneApp
//
//  Created by Zach on 26/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
protocol DiscoverSearchBarViewDelegate: class {
    func cancelWasTapped()
    func searchFieldWasEntered()
    func searchQuery(_ query: String)
    func searchWasCleared()
}

class DiscoverSearchBarView: UIView, UITextFieldDelegate {

    @IBOutlet weak var textField: DiscoverSearchField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var idleSearchView: UIView!
    @IBOutlet weak var idleImage: UIImageView!
    @IBOutlet weak var idleLabel: UILabel!
    @IBOutlet weak var cancelToSearchConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldLeftPaddingConstraint: NSLayoutConstraint!
    @IBOutlet weak var xButton: UIButton!
    
    weak var delegate: DiscoverSearchBarViewDelegate?
    var view: UIView?
    var timer: Timer?
    var currentQuery: String?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        view = (Bundle.main.loadNibNamed("DiscoverSearchFieldView", owner: self, options: nil)?[0] as! UIView)
        self.addSubview(view!)
        view?.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        view?.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        view?.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        view?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true

        textField.delegate = self
        textField.textColor = .white
        textField.layer.borderWidth = 0
        textField.backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 1, alpha: 0.2)
        
        let leftImageView = UIImageView(image: UIImage(named: "icon_search_field"))
        leftImageView.frame = CGRect(x: 0, y: 0, width: leftImageView.image!.size.width + 16.0, height: leftImageView.image!.size.height)
        leftImageView.contentMode = .center
        textField.leftViewMode = .always
        textField.leftView = leftImageView
        textField.layoutMargins = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        textField.leftView?.isHidden = true
        
        textField.accessibilityIdentifier = "search_field"
        textField.isAccessibilityElement = true
        
        cancelToSearchConstraint?.isActive = false
        cancelButton.alpha = 0
        idleSearchView.isUserInteractionEnabled = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        view?.frame = self.bounds
        view?.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        view?.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        textField.bia_rounded()
        textField.isUserInteractionEnabled = true
    }
    
    @IBAction func searchQueryChanged(_ sender: AnyObject) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(
            timeInterval: 0.6,
            target: self,
            selector: #selector(sendQuery),
            userInfo: nil,
            repeats: false)
    }
    
    func sendQuery() {
        guard let text = textField.text else {
            return
        }
        guard text != currentQuery else {
            return
        }
        currentQuery = text
        delegate?.searchQuery(text)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.searchFieldWasEntered()
        Utils.dispatchOnMainThread {[weak self] in
            self?.cancelToSearchConstraint?.isActive = true
            self?.xButton.isHidden = false
            UIView.animate(withDuration: 0.2) {
                self?.layoutIfNeeded()
                self?.cancelButton.alpha = 1
                self?.idleSearchView.alpha = 0
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let _ = resignFirstResponder()
        return false
    }
    
    override func resignFirstResponder() -> Bool {
        if textField.isFirstResponder && textField.resignFirstResponder() {
            let _ = textField?.resignFirstResponder()
            return true
        }
        return false
    }
    
    @IBAction func cancelButtonWasTapped(_ sender: AnyObject) {
        delegate?.cancelWasTapped()
        cancelSearch()
    }
    
    @IBAction func xButtonWasTapped(_ sender: AnyObject) {
        textField.text = ""
        let _ = textField.becomeFirstResponder()
        delegate?.searchWasCleared()
    }
    
    
    func cancelSearch() {
        let _ = resignFirstResponder()
        textField.text = ""
        textField.leftView?.isHidden = true
        Utils.dispatchOnMainThread {[weak self] in
            self?.idleSearchView.isHidden = false
            self?.cancelToSearchConstraint?.isActive = false
            self?.xButton.isHidden = true
            
            UIView.animate(withDuration: 0.2) {
                self?.layoutIfNeeded()
                self?.cancelButton.alpha = 0
                self?.idleSearchView.alpha = 1
            }
        }
    }
    
    func setSearchText(_ search: String) {
        textField.text = search
        currentQuery = search
        cancelButton.alpha = 1
        idleSearchView.alpha = 0
        textField.leftView?.isHidden = false
        cancelToSearchConstraint?.isActive = true
    }
}
