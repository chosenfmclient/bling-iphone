//
//  SIAVideoCategoriesDataModel.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/10/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAVideoCategoriesDataModel.h"
#define TAGS_API @"api/v1/video-types"

@implementation SIAVideoCategoriesDataModel

+ (RKObjectMapping *)objectMapping {
    
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[self class]];
    [objectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data"
                                                                                  toKeyPath:@"categories"
                                                                                withMapping:[SIAVideoCategory objectMapping]]];
    
    return objectMapping;
}

+ (void) loadCategories:(void(^)(SIAVideoCategoriesDataModel *model))handler {
    
    RKObjectManager *objectManager = [SIAObjectManager sharedManager];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[SIAVideoCategoriesDataModel objectMapping]
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:TAGS_API
                                                                                           keyPath:nil
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:responseDescriptor];
    NSDictionary *params = @{@"access_token":[[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"],
                             API_VERSION_KEY_OBJECT};
    
    [objectManager getObjectsAtPath:TAGS_API
                         parameters:params
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                handler(mappingResult.firstObject);
                            } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                handler(nil);
                            }];
}

@end
