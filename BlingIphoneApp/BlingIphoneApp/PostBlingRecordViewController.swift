//
//  PostBlingRecordViewController.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 10/6/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class PostBlingRecordViewController: SIABasePostRecordViewController {
    
    @IBOutlet weak var activityIndicatorView: UIView!
    @IBOutlet weak var spinner: SIASpinner!
    @IBOutlet weak var activityIndicatorLabel: UILabel!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var savePrivateButton: UIButton!
    
    var setDisplayForcameo = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // TODO: this is silly but for now...
        delegate = navigationController as! SIAPostRecordViewController
        AmplitudeEvents.log(event: "record:review")
    }
    
    func backEvent() {
        AmplitudeEvents.log(event: "record:cancel", with: ["pagename": AmplitudeEvents.kPostRecordPageName])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController.setNavigationItemTitle("Review")
        navigationController.setNavigationBarLeftButtonBack()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        postButton.isEnabled = true
        savePrivateButton.isEnabled = true
    }
    
    override func viewDidLayoutSubviews() {
        postButton.bia_rounded()
        savePrivateButton.bia_rounded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    func setForcameo() {
//        postButton.backgroundColor = SIAStyling.defaultStyle().cameoPink()
//        postButton.setTitle("ADD TO cameo", for: .normal)
//    }
    
    func disableButtonsAndAddSpinner() {
        Utils.dispatchOnMainThread {[weak self] in
            guard let _ = self else { return }
            self?.postButton.isEnabled = false
            self?.savePrivateButton.isEnabled = false
            self?.spinner.startAnimation()
        }
    }
    
    @IBAction func postButtonWasTapped(_ sender: AnyObject) {
        
        delegate.next()
        AmplitudeEvents.log(event: "record:submit")
        if setDisplayForcameo {
            AmplitudeEvents.log(event: "duet:completed",
                                with: ["status" : "public"])
        }
        delegate.startUpload(forPrivate: false)
    }
    @IBAction func savePrivateButtonWasTapped(_ sender: AnyObject) {
        delegate.next()
        AmplitudeEvents.log(event: "record:submit")
        if setDisplayForcameo {
            AmplitudeEvents.log(event: "duet:completed",
                                with: ["status" : "private"])
        }
        delegate.startUpload(forPrivate: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
