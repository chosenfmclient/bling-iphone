//
//  RegistrationViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 18/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
import Contacts
import Amplitude_iOS

enum SignupFieldValidationResult {
    case emailInvalid
    case passwordInvalid
    case firstNameInvalid
    case lastNameInvalid
}

@objc class RegistrationViewController: SIABaseViewController, DidLoginDelegate, InvitedDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var signInButtonsView: UIView!
    @IBOutlet weak var termsConditionsButton: UIButton!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    
    @IBOutlet weak var facebookImageView: UIImageView!
    @IBOutlet weak var emailImageView: UIImageView!
    
    @IBOutlet weak var registrationView: UIView!
    @IBOutlet weak var connectView: UIView!
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var inviteView: UIView!
    @IBOutlet weak var letsGoButton: UIButton!
    @IBOutlet weak var inviteContainer: UIView!
    //constraints - for animation
    @IBOutlet weak var registrationViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var registrationViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var connectViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var connectViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var inviteViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var inviteViewTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var registerBackgroundImage: UIImageView!
    var introVideoURL: URL {
        let fileName = UIDevice.current.userInterfaceIdiom == .pad ? "intro_video_3_CLIPCHAMP_keep" :
        "intro_video"
        let path = Bundle.main.path(forResource: fileName,
                                    ofType: "mp4")!
        return URL(fileURLWithPath: path)
    }
    var player: BIAVideoPlayer?
    
    //MARK: Properties
    var loggedIn = false
    var viewVisible = false
    let homeVc = UIStoryboard(
                                name: "Main",
                              bundle: nil
        ).instantiateViewController(
                                withIdentifier: "HomeVC"
        ) as! HomeViewController
    //MARK: View controller methods
    override func viewDidLoad() {
        
        registrationView.alpha = 0
        termsConditionsButton.alpha = 0
        registerBackgroundImage.alpha = 0
        
        (navigationController as BIANavigationController).clearNavigationItemLeftButton()
    }
    
    override func viewDidLayoutSubviews() {
        emailButton?.bia_rounded()
        facebookButton?.bia_rounded()
        facebookImageView?.bia_rounded()
        emailImageView?.bia_rounded()
        connectButton?.bia_rounded()
        letsGoButton?.bia_rounded()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (!viewVisible) {
            viewVisible = true
            player = BIAVideoPlayer(url: introVideoURL)
            let playerLayer = AVPlayerLayer(player:player)
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            playerLayer.frame = registerBackgroundImage.bounds
            registerBackgroundImage.layer.addSublayer(playerLayer)
            
            let darkView = UIView(frame: registerBackgroundImage.bounds)
            darkView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            registerBackgroundImage.addSubview(darkView)
            
            player?.repeatPlayback()
            player?.playAfterAppBecomesActive = true
            Utils.dispatchAfter(0, closure: {[weak self] in
                UIView.animate(
                    withDuration: 1,
                    delay: 0,
                    options: .curveEaseOut,
                    animations: {
                        self?.registrationView.alpha = 1
                        self?.registerBackgroundImage.alpha = 1
                        self?.termsConditionsButton.alpha = 1
                    },
                    completion: nil)
            })
        }
        else {
            player?.repeatPlayback()
        }
    }
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //MARK: IB Actions
    @IBAction func emailSignupWasTapped(_ sender: AnyObject) {
        signInButtonsView.isUserInteractionEnabled = false
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { [weak self] (action: UIAlertAction) in
            alertController.dismiss(animated: true, completion: nil)
            self?.signInButtonsView.isUserInteractionEnabled = true
        }))
        
        alertController.addAction(UIAlertAction(title: "Log In", style: .default, handler: { [weak self] (action: UIAlertAction) in
            guard let sSelf = self else {
                return
            }
            let signUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC")
            (signUpVC as! SignupViewController).loginMode = true
            (signUpVC as! SignupViewController).delegate = sSelf
            sSelf.navigationController?.pushViewController(signUpVC, animated: true)
            sSelf.signInButtonsView.isUserInteractionEnabled = true
        }))
        
        alertController.addAction(UIAlertAction(title: "Registration", style: .default, handler: { [weak self] (action: UIAlertAction) in
            guard let sSelf = self else {
                return
            }
            let signUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC")
            (signUpVC as! SignupViewController).delegate = sSelf
            sSelf.navigationController?.pushViewController(signUpVC, animated: true)
            sSelf.signInButtonsView.isUserInteractionEnabled = true
        }))
        
        alertController.popoverPresentationController?.sourceView = (sender as! UIView)
        alertController.popoverPresentationController?.sourceRect = (sender as! UIView).bounds

        player?.stopRepeatingPlayback()
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func facebookSignupWasTapped(_ sender: AnyObject) {
        AmplitudeEvents.log(event: "facebooklogin:submit")
        showConnectingView()
        BIALoginManager.sharedInstance().fbLogin(fromVC: self, withHandler: {[weak self](success: Bool) in
            guard let sSelf = self else {
                return
            }
            if (success) {
                UserDefaults.standard.setValue(kFacebookLoginType, forKey: kLoginType)
                sSelf.didLogin()
                AmplitudeEvents.logUser(property: ["authentication_type":"facebook"])
            } else {
                AmplitudeEvents.log(event: "facebooklogin:failed")
            }
        })
    }
    
    func showConnectingView() {
        if let _ = view.viewWithTag(12345) {
            return
        }
        let connectView = SignupViewController.createConnectView(width: 220, height: 200, center: view.center)
        connectView.tag = 12345
        view.addSubview(connectView)
    }
    
    func removeConnectingView() {
        view.viewWithTag(12345)?.removeFromSuperview()
    }
    
    @IBAction func connectButtonWasTapped(_ sender: AnyObject) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        let contactStore = CNContactStore()
        switch authorizationStatus {
        case .authorized:  // authorized previously
            goToInviteView()
            break;
        case .notDetermined: // needs to ask for authorization
            contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { [weak self] (accessGranted, error) -> Void in
                // check access granted.
                if (accessGranted) {
                    self?.goToInviteView()
                } else {
                    self?.goToHome()
                }
            })
            break;
        default:  // not authorized.
            goToHome()
            break;
        }
    }
    
    @IBAction func skipButtonWasTapped(_ sender: AnyObject) {
        if letsGoButton.isHidden && !inviteView.isHidden {
            //user didnt invite
            AmplitudeEvents.log(event: "invite:skip")
        }
        goToHome()
    }
    
    //MARK: Navigation
    func goToConnectView() {
        let width = UIScreen.main.bounds.size.width
        connectView.isHidden = false
        connectViewLeadingConstraint.constant = width
        connectViewTrailingConstraint.constant = -width
        view.layoutIfNeeded()
        
        self.connectViewTrailingConstraint?.constant = 0
        self.connectViewLeadingConstraint?.constant = 0
        self.registrationViewLeadingConstraint?.constant = -width
        self.registrationViewTrailingConstraint?.constant = width
        UIView.animate(
            withDuration: 0.3,
            delay: 0,
            options: .curveEaseInOut,
            animations: {[weak self] in
                self?.view.layoutIfNeeded()
            },
            completion: { [weak self](🇬🇧: Bool) in
                self?.registrationView.removeFromSuperview()
                //preload home video
                RequestManager.getRequest("api/v1/home", queryParameters: nil, success: { (req, res) in
                    guard req?.httpRequestOperation?.response?.statusCode == 200 else { return }
                    guard let homeList = res?.firstObject as? HomeResponseModel else { return }
                    guard let video = homeList.video_feed.first  else { return }
                    VideoPlayerAssetManager.shared.downloadFileAt(video.videoUrl, with: {(filePath) in })
                }, failure: nil)
            }
        )
    }
    
    func goToInviteView() {
        let width = UIScreen.main.bounds.size.width
        Utils.dispatchOnMainThread {[weak self] in
            self?.inviteViewLeadingConstraint.constant = width
            self?.inviteViewTrailingConstraint.constant = -width
            self?.view.layoutIfNeeded()
            
            self?.inviteViewTrailingConstraint.constant = 0
            self?.inviteViewLeadingConstraint.constant = 0
            self?.connectViewLeadingConstraint.constant = -width
            self?.connectViewTrailingConstraint.constant = width
            
            self?.termsConditionsButton.isHidden = true
            self?.inviteView.isHidden = false
            UIView.animate(
                withDuration: 0.3,
                delay: 0,
                options: .curveEaseInOut,
                animations: {
                    self?.view.layoutIfNeeded()
                },
                completion: {[weak self] (🇬🇧: Bool) in
                    self?.connectView?.removeFromSuperview()
                }
            )
        }
        let inviteVC = self.childViewControllers[0] as! InviteViewController
        inviteVC.getContactsToInvite()
        inviteVC.invitedDelegate = self
    }
    
    func contactWasInvited() {
        letsGoButton.isHidden = false
    }
    
    func didLogin() {
        let _ = self.navigationController?.popViewController(animated: true)
        showConnectingView()
        loggedIn = true
        homeVc.preloadHomeList()
        let deeplinkClosure = {[weak self] (deeplonk: DeepLinkDataModel?) in
            self?.removeConnectingView()
            if let _ = deeplonk {
                self?.homeVc.deferredDeeplonk = deeplonk
                self?.goToHome()
            }
            else {
                self?.goToConnectView()
            }
        }
        if let deeplonk = (UIApplication.shared.delegate as! AppDelegate).deferredDeeplink {
            deeplinkClosure(deeplonk)
        }
        else {
            DeepLinkDataModel.checkForDeferredDeeplinks(deeplinkClosure)
        }
    }
    
    func goToHome() {
        guard loggedIn else {
            return
        }
        
        player?.stopRepeatingPlayback()
        player?.replaceCurrentItem(with: nil)
        
        Utils.dispatchOnMainThread {[weak self] in
            guard let _ = self else { return }
            self!.connectView?.isHidden = true
            self!.inviteView?.isHidden = true
            let transition = CATransition()
            transition.duration = 0.3
            transition.type = kCATransitionFade
            self!.navigationController?.view.layer.add(transition, forKey: "transition")
            self!.navigationController?.setViewControllers([self!.homeVc], animated: false)
        }
    }
}

//MARK: Sign Up View controller
protocol DidLoginDelegate: class {
    func didLogin()
}

@objc class SignupViewController: SIABaseViewController, UITextFieldDelegate {
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    //MARK: Properties
    var isLoggingIn = false
    var loggedIn = false
    var loginMode = false
    var leftViewsSet = false
    weak var delegate: DidLoginDelegate?
    
    //MARK: View controller methods
    override func viewDidLoad() {
        emailField.rightViewMode = .always
        passwordField.rightViewMode = .always
        firstNameField.rightViewMode = .always
        navigationController.setNavigationItemTitle("Registration")
        
        if (loginMode) {
            setLoginMode()
            navigationController.setNavigationItemTitle("Log In")
            AmplitudeEvents.log(event:"login:pageview")
        } else {
            AmplitudeEvents.log(event:"register:pageview")
        }
        
        loginButton.bia_setEnabled(false)
    }
    
    override func viewDidLayoutSubviews() {
        loginButton.bia_rounded()
        emailField.bia_rounded()
        passwordField.bia_rounded()
        lastNameField?.bia_rounded()
        firstNameField?.bia_rounded()
        if (!leftViewsSet) {
            leftViewsSet = true
            emailField.bia_setLeftImage(named: "icon_register_field_mail.png")
            passwordField.bia_setLeftImage(named: "icon_register_field_password.png")
            firstNameField?.bia_setLeftImage(named: "icon_register_field_user.png")
            lastNameField?.bia_setLeftImage(named: "icon_register_field_user.png")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.destination is ForgotPasswordViewController) {
            if (emailField.text != nil && !emailField.text!.isEmpty) {
                (segue.destination as! ForgotPasswordViewController).email = emailField.text
            }
        }
    }
    
    func backEvent() {
        AmplitudeEvents.log(event: loginMode ? "login:cancel" : "register:cancel")
    }
    
    //MARK: text field delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return !isLoggingIn
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == passwordField) {
            if (errorMessageLabel.isHidden) {
                errorMessageLabel.text = "Passwords must be at least 8 characters long and contain a number"
                errorMessageLabel.isHidden = false
            }
        }
    }
    
    var shouldValidatePassword = false
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string.contains(" ") && textField == emailField) {
            return false
        }
        if textField == passwordField {
            
            if let _ = textField.text, textField.text!.characters.count >= 7 {
                shouldValidatePassword = true
            }
            
            if let oldPasswordStr = textField.text, shouldValidatePassword {
                let newPasswordStr = NSString(string: oldPasswordStr).replacingCharacters(in: range,
                                                                                          with: string)
                let validationResult = validateInput(email: emailField?.text,
                                                     password: newPasswordStr,
                                                     firstName: firstNameField?.text,
                                                     lastName: lastNameField?.text)
                setUIForValidationResult(validationResult)
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let validationResult = validateInput(email: emailField?.text, password: passwordField?.text, firstName: firstNameField?.text, lastName: lastNameField?.text)
        setUIForValidationResult(validationResult)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (firstNameField != nil && textField == firstNameField) {
            lastNameField.becomeFirstResponder()
        } else if(lastNameField != nil && textField == lastNameField) {
            emailField.becomeFirstResponder()
        } else if (textField == emailField) {
            passwordField.becomeFirstResponder()
        } else if (textField == passwordField) {
            passwordField.resignFirstResponder()
        }
        return true
    }
    
    //MARK: Button actions
    @IBAction func loginTapped(_ sender: AnyObject) {
        lastNameField?.resignFirstResponder()
        firstNameField?.resignFirstResponder()
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
        
        let validationResult = validateInput(email: emailField?.text, password: passwordField?.text, firstName: firstNameField?.text, lastName: lastNameField?.text)
        if (validationResult.count < 1) {
            if (isLoggingIn) {
                return
            }
            isLoggingIn = true
            if (loginMode) {
                login(email: emailField.text!, password: passwordField.text!)
            } else {
                signUp(email: emailField.text!, password: passwordField.text!, firstName: firstNameField.text!, lastName: lastNameField.text!)
            }
        } else {
            setUIForValidationResult(validationResult)
        }
    }
    
    //MARK: validation
    func setUIForValidationResult(_ result: [SignupFieldValidationResult]) {
        if (result.count < 1) {
            errorMessageLabel.isHidden = true
            SignupViewController.makeTextFieldAppearValid(firstNameField)
            SignupViewController.makeTextFieldAppearValid(emailField)
            SignupViewController.makeTextFieldAppearValid(passwordField)
            loginButton.bia_setEnabled(true)
        } else {
            loginButton.bia_setEnabled(false)
            
            if (result.contains(.firstNameInvalid)) {
                if (firstNameField?.text != nil && !firstNameField.text!.isEmpty) {
                    SignupViewController.makeTextFieldAppearInvalid(firstNameField)
                    errorMessageLabel.text = "First name is required and must be more than one letter long."
                    errorMessageLabel.isHidden = false
                }
            } else {
                SignupViewController.makeTextFieldAppearValid(firstNameField)
                errorMessageLabel.isHidden = true
            }
            
            if (result.contains(.emailInvalid)) {
                if (emailField.text != nil && !emailField.text!.isEmpty) {
                    SignupViewController.makeTextFieldAppearInvalid(emailField)
                    errorMessageLabel.text = "Email must be valid."
                    errorMessageLabel.isHidden = false
                }
            } else {
                SignupViewController.makeTextFieldAppearValid(emailField)
                errorMessageLabel.isHidden = true
            }
            
            if (result.contains(.passwordInvalid)) {
                if (passwordField.text != nil && !passwordField.text!.isEmpty) {
                    SignupViewController.makeTextFieldAppearInvalid(passwordField)
                    errorMessageLabel.text = "Passwords must be at least 8 characters long and contain a number."
                    errorMessageLabel.isHidden = false
                }
            } else {
                SignupViewController.makeTextFieldAppearValid(passwordField)
                errorMessageLabel.isHidden = true
            }
        }
    }
    
    static func makeTextFieldAppearValid(_ textField: UITextField?) {
        guard let field = textField else {
            return;
        }
        if (field.text == nil || field.text!.isEmpty) {
            field.rightView?.isHidden = true
        } else {
            field.rightView?.isHidden = false
            field.bia_setRightImage(named: "icon_register_field_ok.png")
            field.layer.borderWidth = 0
        }
    }
    
    static func makeTextFieldAppearInvalid(_ textField: UITextField?) {
        guard let field = textField else {
            return;
        }
        if (field.text == nil || field.text!.isEmpty) {
            field.rightView?.isHidden = true
        } else {
            field.rightView?.isHidden = false
            field.bia_setRightImage(named: "icon_register_field_error.png")
            field.layer.borderWidth = 0
        }
    }
    
    func validateInput(email: String?, password: String?, firstName: String?, lastName: String?)
        -> [SignupFieldValidationResult]
    {
        var errors: [SignupFieldValidationResult] = []
        if (!loginMode && (firstName == nil || !SignupViewController.isFirstNameValid(firstName: firstName!)))
        {
            errors.append(.firstNameInvalid)
        }
        if (email == nil || !SignupViewController.isEmailValid(email: email!))
        {
            errors.append(.emailInvalid)
        }
        if (password == nil || !SignupViewController.isPasswordValid(password: password!))
        {
            errors.append(.passwordInvalid)
        }
        
        return errors
    }
    
    
    static func isPasswordValid(password:String) -> Bool {
        return (password.characters.count >= 8 && password.rangeOfCharacter(from: .decimalDigits, options: [], range: nil) != nil)
    }
    
    static func isEmailValid(email: String) -> Bool {
        return (email.characters.count >= 6 && email.contains("@") && email.contains("."))
    }
    
    static func isFirstNameValid(firstName: String) -> Bool {
        return (firstName.characters.count >= 2)
    }
    
    func setLoginMode() {
        firstNameField?.removeFromSuperview()
        lastNameField?.removeFromSuperview()
        loginButton.setTitle("LOG IN", for: .normal)
        forgotPasswordButton.isHidden = false
        loginMode = true
    }
    
    //MARK: Log in
    func signUp(email: String, password: String, firstName: String, lastName: String) {
        AmplitudeEvents.log(event: "register:submit")
        showConnectingView()
        
        BIALoginManager.sharedInstance().createEmailUser(
            withEmail: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            completionHandler: {[weak self] (success: Bool, explanation: String?) in
                if (success) {
                    //logged in
                    self?.setEmailLoginData(email: email,
                                            password: password)
                    self?.login(email: email,
                                password: password)
                    AmplitudeEvents.logUser(property: ["authentication_type":"email"])
                } else {
                    //not logged in
                    AmplitudeEvents.log(event: "register:failed")
                    if let _ = explanation {
                        self?.errorMessageLabel.text = explanation
                        self?.errorMessageLabel.isHidden = false
                        SignupViewController.makeTextFieldAppearInvalid(self?.emailField)
                    }
                }
        })
    }
    
    func login(email: String, password: String) {
        AmplitudeEvents.log(event: "login:submit")
        isLoggingIn = true
        setEmailLoginData(email: email, password: password)
        showConnectingView()
//        let connectView = SignupViewController.createConnectView(width: 250, height: 150, aboveView: loginButton, withPadding: 15)
//        view.addSubview(connectView)
        SIAUser.getWithId(nil, completionHandler: { [weak self](user: SIAUser?) in
            guard let sSelf = self else {
                return
            }
            if (user != nil) {
                BIALoginManager.sharedInstance().myUser = user
                sSelf.loggedIn = true
                UserDefaults.standard.setValue(kEmailLoginType, forKey: kLoginType)
                sSelf.delegate?.didLogin()
                AmplitudeEvents.logUser(property: ["authentication_type":"email"])
            } else {
                AmplitudeEvents.log(event: "login:failed")
                sSelf.errorMessageLabel.isHidden = false
                sSelf.errorMessageLabel.text = "Email or Password invalid"
                SignupViewController.makeTextFieldAppearInvalid(sSelf.emailField)
                SignupViewController.makeTextFieldAppearInvalid(sSelf.passwordField)
                sSelf.setEmailLoginData(email: nil, password: nil)
                sSelf.loginButton.bia_setEnabled(false)
            }
            sSelf.isLoggingIn = false
            sSelf.removeConnectingView()
//            connectView.removeFromSuperview()
        })
    }
    
    func setEmailLoginData(email: String?, password: String?) {
        if (email == nil || password == nil) {
            UserDefaults.standard.setValue(nil, forKey: kLoginType)
        }
        
        let MyKeychainWrapper = KeychainWrapper()
        MyKeychainWrapper.mySetObject(password, forKey:kSecValueData)
        MyKeychainWrapper.mySetObject(email, forKey:kSecAttrAccount)
        MyKeychainWrapper.writeToKeychain()
    }
    
    static func createConnectView(width: CGFloat, height: CGFloat, aboveView uiView: UIView, withPadding padding: CGFloat) -> UIView {
        let center = CGPoint(x: uiView.center.x, y: uiView.center.y - uiView.frame.height - padding - (height / 2))
        return SignupViewController.createConnectView(width: width, height: height, center: center)
    }
    
    static func createConnectView(width: CGFloat, height: CGFloat, center: CGPoint) -> UIView {
        let connectView = Bundle.main.loadNibNamed("ConnectingView", owner: self, options: nil)![0] as! UIView
        connectView.frame = CGRect(x: 0, y: 0, width: width, height: height)
        connectView.center = center
        return connectView
    }
    func showConnectingView() {
        if let _ = view.viewWithTag(12345) {
            return
        }
        let connectView = SignupViewController.createConnectView(width: 220, height: 200, center: view.center)
        connectView.tag = 12345
        view.addSubview(connectView)
    }
    
    func removeConnectingView() {
        view.viewWithTag(12345)?.removeFromSuperview()
    }
}

//MARK: Forgot password View controller
@objc class ForgotPasswordViewController: SIABaseViewController, UITextFieldDelegate {
    //MARK: outlets
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var successImage: UIImageView!
    @IBOutlet weak var successLabel: UILabel!
    
    //MARK: Properties
    var email: String?
    var isSending = false
    var sent = false
    
    //MARK: View controller methods
    override func viewDidLoad() {
        sendButton.bia_setEnabled(false)
        emailField.rightViewMode = .always
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController.setNavigationItemTitle("Password Recovery")
        navigationController.setNavigationBarLeftButtonBack()
        emailField.bia_rounded()
        sendButton.bia_rounded()
        emailField.bia_setLeftImage(named: "icon_register_field_user.png")
        if let _ = email {
            emailField.text = email
            validateEmail()
            email = nil
        }
    }
    
    //MARK: Button action
    @IBAction func sendPasswordWasTapped(_ sender: AnyObject) {
        if (sent) {
            let _ = navigationController?.popViewController(animated: true)
        } else if (!isSending) {
            isSending = true
            let connectView = SignupViewController.createConnectView(width: 250, height: 150, aboveView: sendButton, withPadding: 15)
            view.addSubview(connectView)

            BIALoginManager.sharedInstance().sendNewPassword(emailField.text, completionHandler: {[weak self] (success: Bool, explanation: String?) in
                guard let sSelf = self else {
                    return
                }
                if (success) {
                    sSelf.successImage.isHidden = false
                    sSelf.successLabel.isHidden = false
                    sSelf.emailField.isHidden = true
                    sSelf.errorLabel.isHidden = true
                    sSelf.sendButton.setTitle("LOG IN", for: .normal)
                    sSelf.sent = true
                } else {
                    if let _ = explanation {
                        sSelf.errorLabel.isHidden = false
                        sSelf.errorLabel.text = explanation
                    }
                    SignupViewController.makeTextFieldAppearInvalid(sSelf.emailField)
                }
                sSelf.isSending = false
                connectView.removeFromSuperview()
            })
            //send new password
        }
    }
    
    //MARK: Text Field delegate methods
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return !isSending
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        validateEmail()
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return !string.contains(" ")
    }
    
    func validateEmail() {
        if (emailField.text != nil && SignupViewController.isEmailValid(email: emailField.text!)) {
            SignupViewController.makeTextFieldAppearValid(emailField)
            sendButton.bia_setEnabled(true)
            errorLabel.isHidden = true
        } else {
            SignupViewController.makeTextFieldAppearInvalid(emailField)
            sendButton.bia_setEnabled(false)
            errorLabel.isHidden = false
            errorLabel.text = "This email is invalid"
        }

    }
    
}

