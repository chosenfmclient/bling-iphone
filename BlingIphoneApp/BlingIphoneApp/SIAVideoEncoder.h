//
//  SIAVideoEncoder.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 4/13/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVFoundation/AVAssetWriter.h"
#import "AVFoundation/AVAssetWriterInput.h"
#import "AVFoundation/AVMediaFormat.h"
#import "AVFoundation/AVVideoSettings.h"
#import "AVFoundation/AVAudioSettings.h"

@interface SIAVideoEncoder : NSObject

{
    AVAssetWriter* _writer;
    AVAssetWriterInput* _videoInput;
    AVAssetWriterInputPixelBufferAdaptor* _writerInputPixelBuffer;
    AVAssetWriterInput* _audioInput;
    NSString* _path;
}

@property NSString* path;

+ (SIAVideoEncoder*) encoderForPath:(NSString*) path height:(int)cy width:(int)cx pixelFormat:(OSType)format channels: (int) ch samples:(Float64) rate;
+ (SIAVideoEncoder*) encoderForPath:(NSString *)path height:(int)cy width:(int)cx pixelFormat:(OSType)format;
- (void) finishAtTime:(CMTime)time
withCompletionHandler:(void (^)(NSURL *outputURL))handler;

- (void) initPath:(NSString*)path Height:(int) cy width:(int) cx pixelFormat:(OSType)format channels: (int) ch samples:(Float64) rate;
- (void) finishWithCompletionHandler:(void(^)(NSURL *outputURL))handler;
- (void)cancelWriting;
- (BOOL) encodeVideoFrame:(CVPixelBufferRef)pixelBuffer presentationTime:(CMTime)presetationTime;
- (BOOL) encodeAudioFrame:(CMSampleBufferRef)sampleBuffer;
- (void) setVideoOrientationFromCaptureOrientation:(AVCaptureVideoOrientation)orientation;


@end
