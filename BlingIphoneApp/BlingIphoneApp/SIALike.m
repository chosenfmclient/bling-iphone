//
//  SIALike.m
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 9/17/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import "SIALike.h"

#define LIKE_API @"api/v1/videos/%@/like"

@implementation SIALike

+ (RKObjectMapping *)objectMapping {
    
    RKObjectMapping *objectMapping = [[RKObjectMapping alloc] initWithClass:[self class]];
    
    [objectMapping addRelationshipMappingWithSourceKeyPath:@"user"
                                                   mapping:[SIAUser objectMapping]];
    
    [objectMapping addAttributeMappingsFromArray:@[@"video_id",
                                                   @"like_id",
                                                   @"date"]];
    return objectMapping;
}


+ (void) addLikeToVideo:(NSString *)videoId withCompletion: (void (^)())completion {
    SIALike* like = [[SIALike alloc] init];
    like.type = @"home";
    [RequestManager
     postRequest:[NSString stringWithFormat:LIKE_API, videoId]
     object:like
     queryParameters:nil
     success:^(RKObjectRequestOperation* request, RKMappingResult* result) {
         if (completion) {
             completion();
         }
     } failure:^(RKObjectRequestOperation* request, NSError* error) {
         if (completion) {
             completion();
         }
     }
     showFailureDialog:YES];
}

+ (void) removeLikeFromVideo:(NSString *)videoId withCompletion: (void (^)())completion {
    SIALike* like = [[SIALike alloc] init];
    like.type = @"home";
    [RequestManager
     deleteRequest:[NSString stringWithFormat:LIKE_API, videoId]
     object:like
     queryParameters:nil
     success:^(RKObjectRequestOperation* request, RKMappingResult* result) {
         if (completion) {
             completion();
         }
     } failure:^(RKObjectRequestOperation* request, NSError* error) {
         if (completion) {
             completion();
         }
     }
     showFailureDialog:YES];
}

@end
