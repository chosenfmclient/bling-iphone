//
//  FeaturedBannerView.swift
//  BlingIphoneApp
//
//  Created by Zach on 02/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

enum BannerType {
    case featured
    case dailyPick
}

@objc class FeaturedBannerView: UIView {
    
    @IBOutlet var bannerView: UIView!
    @IBOutlet var bannerViewBackgroundView: UIView!
    @IBOutlet var bannerLabel: UILabel!
    @IBOutlet var bannerWidthConstraint: NSLayoutConstraint!
    @IBOutlet var bannerLeadingConstraint: NSLayoutConstraint!
    
    var view: UIView?
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        view = (Bundle.main.loadNibNamed("FeaturedBanner", owner: self, options: nil)?[0] as! UIView)
        view?.frame = frame
        self.addSubview(view!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        bannerView.bia_rounded()
    }
    
    func setBannerStyle(for type: BannerType) {
        switch type {
        case .dailyPick:
            bannerLabel.text = "DAILY PICK!"
            bannerView.backgroundColor = UIColor(
                colorLiteralRed: 228/255,
                green: 0/255,
                blue: 132/255,
                alpha: 1
            )
            break
        case .featured:
            bannerLabel.text = "FEATURED"
            bannerView.backgroundColor = UIColor(
                colorLiteralRed: 133/255,
                green: 190/255,
                blue: 63/255,
                alpha: 1
            )
            break
        }
        
        bannerLabel.sizeToFit()
        bannerWidthConstraint.constant = bannerLabel.bounds.width + 30
        bannerViewBackgroundView.backgroundColor = bannerView.backgroundColor
    }
    
    func hideBanner(after seconds: Double) {
        Utils.dispatchOnMainThread { [weak self] in
            UIView.animate(
                withDuration: 0.3,
                delay: seconds,
                options: .curveEaseIn,
                animations: {
                    guard let sSelf = self else { return }
                    sSelf.bannerLeadingConstraint.constant = -sSelf.bannerView.bounds.size.width * 2
                    sSelf.view?.layoutIfNeeded()
                },
                completion: nil
            )
        }
    }
    
    static func addBanner(to view: UIView, ofType type: BannerType) -> FeaturedBannerView? {
        guard let superView = view.superview else { return nil }
        
        let banner = FeaturedBannerView(frame: view.frame)
        banner.setBannerStyle(for: type)
        banner.isUserInteractionEnabled = false
        superView.addSubview(banner)
        
        return banner
    }
    
    static func addBanner(to view: UIView, withText text: String, andHexColor hex: String?) -> FeaturedBannerView? {
        guard let superView = view.superview else { return nil }
        
        let banner = FeaturedBannerView(frame: view.frame)
        banner.bannerLabel.text = text
        banner.bannerLabel.sizeToFit()
        banner.bannerWidthConstraint.constant = banner.bannerLabel.bounds.width + 30
        if let _ = hex {
            banner.bannerView.backgroundColor = UIColor(hex: hex!)
        }
        banner.bannerViewBackgroundView.backgroundColor = banner.bannerView.backgroundColor
        banner.isUserInteractionEnabled = false
        superView.insertSubview(banner, aboveSubview: view)
        
        return banner
    }

}
