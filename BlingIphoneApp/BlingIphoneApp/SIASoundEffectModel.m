//
//  SIASoundEffectModel.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 5/5/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIASoundEffectModel.h"
#import "SIAObjectManager.h"
#import "SIANetworking.h"
#define SOUND_EFFECTS_API @"api/v1/sound-effects"

@implementation SIASoundEffect

+ (RKObjectMapping *)objectMapping {
    
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[self class]];
    [objectMapping addAttributeMappingsFromArray:@[@"name", @"url"]];
    return objectMapping;
}

@end


@implementation SIASoundEffectModel

- (id)init {
    self = [super init];
    if (self)
    {
        self.filteredArray = [NSMutableArray array];
    }
    return self;
}

+ (void)getSoundEffectsWithCompletion:(void (^)(SIASoundEffectModel *))handler {
    [RequestManager
     getRequest:SOUND_EFFECTS_API
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         handler(mappingResult.firstObject);
         
     } failure:^(RKObjectRequestOperation *operation, NSError *error) {
         
     }
     showFailureDialog:YES];
}

+ (RKObjectMapping *)objectMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[self class]];
    
    [objectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data"
                                                                                  toKeyPath:@"effectsArray"
                                                                                withMapping:[SIASoundEffect objectMapping]]];
    return objectMapping;
}

@end
