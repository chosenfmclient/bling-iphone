//
//  FacebookManager.m
//  SingrIphoneApp
//
//  Created by Roni Shoham on 23/12/13.
//  Copyright (c) 2013 SingrFM. All rights reserved.
//

#import "FacebookManager.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>



#define AccountFacebookAccountAccessGranted @"FacebookAccountAccessGranted"
#define kNilJSONOptions 0
#define NUMBER_OF_CONNECTION_ATTEMPTS (3)


//const int FB_OK                     = 0;
//const int FBNoAccountInIOSSettings  = 6;
//const int FBAccountIsNotValid       = 7;
//const int FBOAuthError              = 190;

@implementation FacebookManager

static int connectionAttempts  = 0;


+ (instancetype) sharedInstance {
    static FacebookManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[FacebookManager alloc] init];
    });
    return manager;
}

-(BOOL) saveDefaults:(NSString *) fbUserId
            userName:(NSString *) fbUserName
           userEmail:(NSString *) fbUserEmail
       userFirstName:(NSString *) fbUserFirstName
        userBirthday:(NSString *) fbUserBirthday
        userLastName:(NSString *) fbUserLastName
          userGender:(NSString *) fbUserGender
        access_token:(NSString *) fbAccess_token
        userImageUrl:(NSString *) fbUserImageUrl
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:fbUserId    forKey:kFBUserIdKeyStr];
    [defaults setObject:fbUserName  forKey:kFBUserNameKeyStr];
    
    [[NSUserDefaults standardUserDefaults] setObject:fbUserFirstName forKey:kChosenUserFirstNameKeyStr];
    [[NSUserDefaults standardUserDefaults] setObject:fbUserLastName forKey:kChosenUserLastNameKeyStr];
    
    return YES;
}

+(NSString *)userId
{
    FBSDKProfile *currentProfile = [FBSDKProfile currentProfile];
    if (currentProfile) {
        return currentProfile.userID;
    }
    
    return @"0";
}

-(void)accountChanged:(NSNotification *)notif//no user info associated with this notif
{
    //[self attemptRenewCredentials:^(BOOL completed){}];
    [self attemptRenewCredentials:^(NSError* completed){}];
}


-(void)attemptRenewCredentials:(void (^)(NSError* error))completed{
    
    if (connectionAttempts < NUMBER_OF_CONNECTION_ATTEMPTS) {
        connectionAttempts++;
        if (self.facebookAccount) {
            [self.account renewCredentialsForAccount:(ACAccount *)self.facebookAccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
                if(!error)
                {
                    switch (renewResult) {
                        case ACAccountCredentialRenewResultRenewed:
                            NSLog(@"FacebookManager.m -> attemptRenewCredentials: Good to go");
                            
                        case ACAccountCredentialRenewResultRejected:
                            
                            NSLog(@"FacebookManager.m -> attemptRenewCredentials: User declined permission");
                            
                            break;
                            
                        case ACAccountCredentialRenewResultFailed:
                            
                            NSLog(@"FacebookManager.m -> attemptRenewCredentials: non-user-initiated cancel, you may attempt to retry");
                            
                            break;
                            
                        default:
                            break;
                            
                    }
                }
                
                else{
                    
                    // error is handled on calling method
                    NSLog(@"FacebookManager.m -> attemptRenewCredentials: error from renew credentials%@",error);
                    
                }
                
                // pass error to calling method for handling
                completed(error);
                
            }];
        }
    }
    
    else {
        
        self.connectionFailed = YES;
        //TODO: Replace popupview with SIAAlertView
    }
}

-(void) disconnect
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:kFBUserIdKeyStr];
    [defaults setObject:nil forKey:kChosenUserIdKeyStr];
    //    [defaults setObject:nil forKey:kFBUserEmailKeyStr];
}


/** \brief find and return facebook friend ID list
 *
 * \param friendListArr - array to fill with facebook ID of found friends
 ** ...
 *
 */
-(void) findFriends:(void (^)(NSMutableArray*))friendListArr
{
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/me/friends"
                                  parameters:nil
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        NSArray* friends = [result objectForKey:@"data"];
        NSMutableArray* arrayFriendsList = [[NSMutableArray alloc]initWithCapacity:friends.count];
        
        for (NSDictionary *friend in friends) {
            [arrayFriendsList addObject:friend[@"id"]];
            NSLog(@"friend data %@", friend);
        }
        friendListArr(arrayFriendsList);
    }];
}

+ (void) requestPublishPermissions:(void (^)(BOOL))permissionGranted {
    
    FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
    
    NSArray *permissionsToAsk = @[@"publish_actions"];
    
    if ([accessToken.permissions containsObject:permissionsToAsk.firstObject]){
        permissionGranted(YES);
        return;
    }
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.requestingPublishPermissions = YES;
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithPublishPermissions:permissionsToAsk
                               handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                   if ([result.grantedPermissions containsObject:@"publish_actions"]){
                                       permissionGranted(YES);
                                   }else {
                                       permissionGranted(NO);
                                   }
                               }];
    
}


- (void) requestUserFriendsPermission:(void (^)(BOOL))permissionGranted {
    
    NSArray *permissionsToAsk = @[FB_USER_FRIENDS_PERMISSION_STR];
    
    FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
    if ([accessToken.permissions containsObject:permissionsToAsk.firstObject]){
        permissionGranted(YES);
        return;
    }
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: permissionsToAsk
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if ([result.grantedPermissions containsObject:FB_USER_FRIENDS_PERMISSION_STR]){
                                    permissionGranted(YES);
                                }else {
                                    permissionGranted(NO);
                                }
                            }];
    
    
}

@end
