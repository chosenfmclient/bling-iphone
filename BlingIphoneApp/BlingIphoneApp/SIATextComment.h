//
//  SIATextComment.h
//  ChosenIphoneApp
//
//  Created by Joseph Nahmias on 12/01/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXTERN NSString *const SIACommentUpdateNotification;

typedef enum : NSUInteger {
    SIATextCommetFlagTypeSpam = 0,
    SIATextCommetFlagTypeSexual,
    SIATextCommetFlagTypeViolent,
    SIATextCommetFlagTypeHateful,
    SIATextCommetFlagTypeHarmful,
    SIATextCommetFlagTypeInfringing
} SIATextCommetFlagType;

@interface SIATextComment : NSObject

- (instancetype)initWithText:(NSString *)commentText videoId:(NSString *)videoId;

+ (RKObjectMapping *)objectMapping;

@property (strong, nonatomic) NSString *commentId;
@property (strong, nonatomic) SIAUser *user;
@property (strong, nonatomic) SIAVideo *video;
@property (strong, nonatomic) NSString *videoId;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSDate *date;

- (void)postCommentWithCompletionHandler:(void(^)(BOOL success))handler;
- (void)editComment:(NSString *)newText withCompletionHandler:(void(^)(BOOL success))handler;
- (void)deleteCommentWithCompletionHandler:(void(^)(BOOL success))handler;
- (void)flagCommentAsType:(SIATextCommetFlagType)type withCompletionHandler:(void(^)(BOOL success))handler;

@end
