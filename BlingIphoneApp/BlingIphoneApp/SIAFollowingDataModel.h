//
//  SIAFollowingDataModel.h
//  ChosenIphoneApp
//
//  abstract : responsible to bring the exist data from core data and
//  new data from the server
//
//  Created by Roni Shoham on 09/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SIAFollowListModel.h"

#define FOLLOWING_API @"api/v1/users/%@/following"

@interface SIAFollowingDataModel : SIAFollowListModel



@end
