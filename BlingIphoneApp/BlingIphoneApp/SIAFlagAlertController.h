//
//  SIAFlagAlertController.h
//  ChosenIphoneApp
//
//  Created by Zach on 07/09/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAAlertController.h"
#import "SIAVideo.h"

@interface SIAFlagAlertController : SIAAlertController
+ (SIAFlagAlertController *) getFlagAlertControllerForVideo: (SIAVideo*) video withPresentationHandler: (void (^)(SIAAlertController *))presenter;
@end
