//
// Created by Ben on 06/12/2016.
//

#ifndef BLINGY_OCV_NDK_GLOBALS_H
#define BLINGY_OCV_NDK_GLOBALS_H

#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "edgeDetector.h"
#include "chromaKey.h"
#include "opencv2/objdetect/objdetect_c.h"
#include "imageUtil.h"

//#include <android/log.h>

#include <math.h>

//extern edgeDete


extern const int WHITE_PIXEL
, MAX_DISTANCE
, MAX_DISTANCE_X_2
, MAX_DISTANCE_X_3
, CAMERA_WIDTH
, CAMERA_HEIGHT
, FACE_DETECTION_ADDON
, NUMBER_OF_CHROMA_KEYS_THREADS
;
//, FACE_DETECTION_DECON;

extern cv::Mat edgesBufferGlobal;

extern const cv::Size Kernel_5X5, CAMERA_RESIZE_SIZE;


////////////    face detection  /////////////

extern const int HaarOptions;
extern const cv::Size faceSizeKernel;
extern String haarCascadeFilePath;

/////////////////////////////////////////////

extern int
          WIDTH, HEIGHT
        , CAMERA_DIFFERENCE
        , CAMERA_PADDING
        , WIDTH_X_4
        , WIDTH_X_3

        , MAX_SIZE
        , MAX_SIZE_X_4
        , MAX_SIZE_X_3
        ;

extern float scaleFactor;

extern cv::Size defMatSize, detMatSize_1138X640;

//detection Response

//////////// Camera ///////////////

//extern const int *RES_960X720;//, RES_1280X960, RES_1280_720;

extern int cameraRotation, cameraRes;

extern bool isCameraMirrored, isFaceDetectionOn;

extern const int DEFAULT_CAMERA_WIDTH, DEFAULT_CAMERA_HEIGHT;

///////////////////////////////////

extern int hueSkinColorBot, hueSkinColorTop, saturationSkinColorBot, saturationSkinColorTop, luminanceSkinColorBot, luminanceSkinColorTop;

extern const bool drawDetectionGuilds;


////////////Objects//////////////

extern imageUtil iUtil;
extern chromaKey cUtil;

///////////// fade /////////////

extern const int FADE_THRESHOLD, FADE_DECREASER;

////////////////////////////////

extern int strongestRed, strongestGreen, strongestBlue;
extern const int IDEAL_COLOR_INTENSITY;
extern const float BACKGROUND_EDGES_ACCURACY_PERCENTEGE;


////////////////////////////////

class globals
{

};


#endif //BLINGY_OCV_NDK_GLOBALS_H
