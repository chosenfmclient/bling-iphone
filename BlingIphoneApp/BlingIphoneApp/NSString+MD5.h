//
//  NSString+MD5.h
//  BlingIphoneApp
//
//  Created by Zach on 22/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

@interface NSString (MD5)

- (NSString *)MD5String;

@end
