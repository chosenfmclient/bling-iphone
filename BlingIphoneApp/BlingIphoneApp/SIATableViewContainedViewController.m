//
//  SIATableViewContainedViewController.m
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 22/07/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIATableViewContainedViewController.h"
//#import "SIAProfileViewController.h"
#import "SIAVideoTableViewCell.h"


#define HEADER_FONT kAppFontItalic(12.)
#define DEFAULT_HEADER_HEIGHT (30.)

@interface SIATableViewContainedViewController () <UITableViewDataSource, UITableViewDelegate, SIAPaginatorDelegate>

@property (nonatomic) BOOL loadingFlag;
@property (strong, nonatomic) NSLayoutConstraint *tableConstraintToMove;

@end

@implementation SIATableViewContainedViewController


/** \load the view controller.
 *
 * \param none
 *
 * \return none
 *
 *  update the refresh control
 *  call the function show data for loading exist and new data.
 *
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
//    if ([SIANetworking checkConnection]) {
//        [self showData];
//    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showData) name:@"facebookUpdated" object:nil];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];

}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setNavigation];
}

-(void) setNavigation {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [(BIANavigationController *)self.navigationController setNavigationBarLeftButtonBack];
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:0.149 green:0.663 blue:0.878 alpha:1.000]];
    [self.navigationController setNavigationItemTitle:[self navigationTitleString]];
}

- (NSString *)navigationTitleString{
    return nil;
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void) viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView setLayoutMargins:UIEdgeInsetsZero];
}

/** \call the function getData to bring the exist and the new data
 *
 * \param none
 *
 * \return none
 *
 */
- (void)showData  {
}

- (void)pageLoaded {
    
//    [self.paginator.results removeAllObjects];
//    [self.paginator updatePageNumber:0];
    
    if ((self.paginator.page == 0) && (self.paginator.results.count < 1)) {
        [self noData];
    }
    else [self hasData];
    [self refreshView];
}

- (void)cacheLoaded {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0]
                      withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
    });
}

- (void)noMorePages {
    NSIndexPath *indexOfLastRow = [NSIndexPath indexPathForRow:self.paginator.results.count
                                                     inSection:0];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            [self.tableView deleteRowsAtIndexPaths:@[indexOfLastRow] withRowAnimation:UITableViewRowAnimationAutomatic];
            
        }
        @catch (NSException *exception) {
            
        }
    });
    
}

- (void)noData {
    
}

- (void)hasData {
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, [self tableHeaderSize])];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Roboto-Regular" size:12.0f];
    label.textColor = [self headerTextColor];
    [label setText:[self tableHeaderString]];
    [label setBackgroundColor:[self headerBackgroundColor]];
    return label;
}

- (UIColor *)headerBackgroundColor {
    return [UIColor colorWithWhite:1 alpha:0.3];
}

- (UIColor *)headerTextColor {
    return [UIColor whiteColor];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [self tableHeaderSize];
}

- (CGFloat)tableHeaderSize {
    return DEFAULT_HEADER_HEIGHT;
}

- (NSString *)tableHeaderString {
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

/** \returns number of rows in section
 *
 * \param tableView
 * \param section:  number of sections in table
 *
 * \return NSInteger: number of rows in each section
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSLog(@"%lu", (unsigned long)self.paginator.results.count);
    return self.paginator.results.count;
}

/** \show the data of each cell to the user
 *
 * \param tableView: followings table
 * \param indexPath:  index of row
 *
 * \return UITableViewCell: draw each cell in the table
 *
 * check if the page owns to this user or not. if yes the switches will show
 * and he can decide if he want to following or stop following after something
 *  update all the object in cell
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row > self.paginator.results.count) return nil;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[self cellIdentifierForRow:indexPath.row]];
    if ([cell isKindOfClass:[SIALoadingTableViewCell class]]) {
        return cell;
    }
    else if ([cell isKindOfClass:[SIAVideoTableViewCell class]]){
        if ([self.paginator.results[indexPath.row] isKindOfClass:[SIAVideo class]]) {
            [(SIAVideoTableViewCell *)cell setIsVideo:[(SIAVideo *)self.paginator.results[indexPath.row] isVideo]];
        }
        
    }
    
    cell.textLabel.numberOfLines = 0;
    //cell.textLabel.textColor = [UIColor colorWithRed:56./255. green:61./255. blue:80./255. alpha:1.];
    cell.textLabel.textColor = [UIColor colorFromHexString:@"#202020"];
    cell.detailTextLabel.textColor = [UIColor colorFromHexString:@"#939598"];
    //cell.textLabel.font = kAppFontRegular(15.);
    cell.textLabel.font = kAppFontRegular(15.);
    cell.detailTextLabel.font = kAppFontRegular(12.);
   // cell.imageView.image = [self placeholderForRow:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row < self.paginator.results.count){
        
        [cell.imageView sd_setImageWithURL:[self imageURLForRow:indexPath.row]
                          placeholderImage:[self placeholderForRow:indexPath.row]
                                   options:SDWebImageRefreshCached
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     if (image) {
                                       
                                             [UIView transitionWithView:cell.imageView
                                                               duration:0.1
                                                                options:UIViewAnimationOptionTransitionCrossDissolve
                                                             animations:^{
                                                                 cell.imageView.image = image;
                                                             }
                                                             completion:nil];
                                       
                                     }
                                 }];
        
        [cell.textLabel setAttributedText:[self textLabelStringForRow:indexPath.row]];
        //[cell.textLabel setText:[NSString stringWithFormat:@"%i", indexPath.row]];
        [cell.detailTextLabel setAttributedText:[self detailedTextStringForRow:indexPath.row]];
      //  cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
        cell.imageView.backgroundColor = [UIColor blackColor];
        
        cell.accessoryView = [self viewForAccessoryViewForRow:indexPath.row];;
    }
    

    [cell setSeparatorInset:UIEdgeInsetsZero];
    [cell setLayoutMargins:UIEdgeInsetsZero];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (UIImage *)placeholderForRow:(NSUInteger)row{
    return nil;
}

- (void)setImageForRow:(NSUInteger)row withHandler:(void (^)(UIImage* image))handler{
}

- (NSString *)cellIdentifierForRow:(NSInteger)row {
    return nil;
}

- (NSString *)imageURLStringForRow:(NSInteger)row{
    return nil;
}

- (NSURL *)imageURLForRow:(NSInteger)row{
    return nil;
}

- (NSAttributedString *)textLabelStringForRow:(NSInteger)row {
    return nil;
}

- (NSAttributedString *)detailedTextStringForRow:(NSInteger)row {
    return nil;
}

- (UIView *)viewForAccessoryViewForRow:(NSUInteger)row {
    return nil;
}


-(void)refreshView{
    
    if (self.paginator.paginatorModel.currentPage.integerValue == 0) {
      //  dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView beginUpdates];
        [self.tableView reloadData];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
            if (self.paginator.results.count > 0)
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                      atScrollPosition:UITableViewScrollPositionTop
                                              animated:YES];
            // if results are short enough that the loading cell is visible, attempt to get the next page
            //        NSUInteger loadingCellIndex = [self.tableView.visibleCells indexOfObjectPassingTest:^BOOL(UITableViewCell *cell, NSUInteger idx, BOOL *stop)
            //                                       {
            //                                           if ([cell isKindOfClass:[SIALoadingTableViewCell class]])
            //                                           {
            //                                               return YES;
            //                                           }
            //                                           return NO;
            //                                       }];
            //
            //        if (loadingCellIndex != NSNotFound) {
            //            [self.paginator fetchNextPage];
            //        }
            [self finishLoadingWithAnimation];
        //});
    }
    else {
        NSArray *indexPaths = [self.paginator indexPathForPage:self.paginator.paginatorModel.currentPage];;
        NSIndexPath *lastIndex = indexPaths.lastObject;
        NSIndexPath *firstIndex = indexPaths.firstObject;
        if (lastIndex.row < self.paginator.results.count) {
            
          //  dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.tableView beginUpdates];
                @try {
                    [self.tableView deleteRowsAtIndexPaths:@[firstIndex] withRowAnimation:UITableViewRowAnimationTop];
                    
                }
                @catch (NSException *exception) {
                    
                }
                
                [self.tableView insertRowsAtIndexPaths:indexPaths
                                      withRowAnimation:UITableViewRowAnimationTop];
                [self.tableView endUpdates];
          //  });
        }
        
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[SIALoadingTableViewCell class]]) {
        [self.paginator fetchNextPage];
    }
}

- (void)downloadImagesForPage:(NSNumber *)page {
    NSArray *pageArray = [self.paginator.paginatorModel.pageDict objectForKey:page];
    
    NSUInteger lastRowInPage = self.paginator.results.count - 1;
    NSUInteger firstRowInPage = lastRowInPage - pageArray.count;
    
    [self downloadImagesFrom:firstRowInPage to:lastRowInPage];
//    for (int i = firstRowInPage; i < lastRowInPage; i ++) {
//        SIAVideo *video = self.paginator.results[i];
//        [video thumbnailWithComplitionHandler:^(UIImage *image) {
//            [self.tableView reloadData];
//        }];
//    }
}


-(void)startLoadingAnimation{
    if (!_loadingFlag) {
        _loadingFlag = YES;
        for (NSLayoutConstraint *constraint in self.tableView.superview.constraints) {
            if (constraint.firstItem == self.tableView && constraint.secondItem == self.tableView.superview) {
                if (constraint.firstAttribute == NSLayoutAttributeTop){
                    _tableConstraintToMove = constraint;
                    break;
                }
            };
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.1 animations:^{
                _tableConstraintToMove.constant += 40;
                self.loadingspinnigImageView.alpha = 1;
            }];
        });
        [self runSpinAnimationOnView:self.loadingspinnigImageView duration:0.5 rotations:1 repeat:HUGE_VALF];

    }
}

-(void)finishLoadingWithAnimation{
    if (_loadingFlag) {
        _loadingFlag = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.5 animations:^{
                _tableConstraintToMove.constant -= 40;
                self.loadingspinnigImageView.alpha = 0;
            }];
        });
    }
}

- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

///** \when selecting specific row redirect to the user profile
// *
// * \param tableView
// * \param indexPath: index of the selected row
// *
// * \return none
// *
// */
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSString *userid = self.paginator.results[indexPath.row][@"facebook_id"];
//    
//    UIStoryboard *videoPlayersb = [UIStoryboard storyboardWithName:@"ProfileStoryboard" bundle:nil];
//    SIAProfileViewController *profileController = [videoPlayersb instantiateViewControllerWithIdentifier:@"newProfile"]; // corresponding to the storyboard identifier
//    profileController.userid = userid;
//    [self.navigationController pushViewController:profileController animated:YES];
//}

- (void) dealloc {
    self.tableView.dataSource = nil;
    self.tableView.delegate = nil;
    self.tableView = nil;
}

@end


