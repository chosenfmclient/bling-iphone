//
//  SIAVideoPlayerView.h
//  ChosenIphoneApp
//
//  Created by Joseph Nahmias on 07/01/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIAVideoPlayerView : UIView

-(instancetype)initWithPlayer:(AVPlayer *)player;
@property (nonatomic, weak) AVPlayerLayer *playerLayer;

    
@end
