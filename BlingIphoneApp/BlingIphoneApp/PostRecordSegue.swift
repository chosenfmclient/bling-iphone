//
//  PostRecordSegue.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 9/21/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class PostRecordSegue: UIStoryboardSegue {
    
    override func perform() {
        let navController: UINavigationController
        if (self.source.navigationController!.presentingViewController is UINavigationController) {
           navController = self.source.navigationController!.presentingViewController as! UINavigationController
        }
        else {
            navController = self.source.navigationController!
        }
        var viewControllers: [UIViewController] = navController.viewControllers
        var viewControllerToPop: UIViewController?
        for viewController in viewControllers {
            if let _ = viewController as? SIABlingRecordViewController {
                viewControllerToPop = viewController
            }
            else if let _ = viewController as? UploadVideoViewController {
                viewControllerToPop = viewController
            }
        }
        guard let _ = viewControllerToPop else {
            navController.pushViewController(self.destination, animated: true)
            return
        }
        viewControllers[viewControllers.index(of: viewControllerToPop!)!] = self.destination
        if let _ = viewControllerToPop?.presentedViewController {
            viewControllerToPop?.dismiss(animated: false, completion: {
                navController.setViewControllers(viewControllers, animated: true)
            })
        }
        else {
            navController.setViewControllers(viewControllers, animated: true)
        }
    }
    
}
