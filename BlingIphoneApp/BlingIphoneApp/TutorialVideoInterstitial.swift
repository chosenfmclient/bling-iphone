//
//  TutorialVideoInterstitial.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 1/12/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

import UIKit

class TutorialVideoInterstitial: SIAPushRegistrationInterstitial, BIAVideoPlayerDelegate {
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var xButton: UIButton!
    
    let tutorialURL = URL(fileURLWithPath:Bundle.main.path(forResource: "tutorial_video_6_CLIPCHAMP_keep", ofType: "mp4")!)
    var player: BIAVideoPlayer?
    var playerLayer: AVPlayerLayer?
    var hideXButton = true
    var didAddPlayerLayer = false
//    override func awakeFromNib() {
//        super.awakeFromNib()
//    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        player = BIAVideoPlayer(url: tutorialURL)
        player?.delegate = self
    }
    
    override func layoutSubviews() {
        playerLayer?.frame = videoView.bounds
        xButton.bia_rounded()
        xButton.isHidden = hideXButton
        if !didAddPlayerLayer {
            playerLayer = AVPlayerLayer(player: player)
            videoView.layer.addSublayer(playerLayer!)
            playerLayer?.videoGravity = AVLayerVideoGravityResizeAspect
        }
    }
    
    override func show() {
        Utils.dispatchOnMainThread { [weak self] in
            guard let _ = self else { return }
            if self!.player?.currentItem == nil {
                self!.player?.replaceCurrentItem(with: AVPlayerItem(url: self!.tutorialURL))
            }
            self!.player?.playAfterAppBecomesActive = true
            CATransaction.begin()
            CATransaction.setCompletionBlock({ [weak self] in
                guard let _ = self else { return }
                self!.player?.play()
            })
            self!.superShow()
            CATransaction.commit()
        }
    }
    
    private func superShow() {
        super.show()
    }
    
    @IBAction func xButtonWasTapped(_ sender: Any) {
        dismiss()
    }
 
    override func dismiss() {
        player?.pause()
        player?.playAfterAppBecomesActive = false
        player?.replaceCurrentItem(with: nil)
        completionHandler?(false)
        completionHandler = nil
        super.dismiss()
    }
    
    func playerDidFinishPlaying(_ player: AVPlayer!) {
        dismiss()
    }
    
    deinit {
        if let _ = player {
            player!.replaceCurrentItem(with: nil)
        }
    }

}
