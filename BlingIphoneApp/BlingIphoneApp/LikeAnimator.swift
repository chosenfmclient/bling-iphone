//
//  LikeAnimator.swift
//  BlingIphoneApp
//
//  Created by Zach on 26/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation

@objc class LikeAnimator: NSObject {
    
    static let numberOfHearts = 3
    static let totalDuration = TimeInterval(1.8)
    static let heartScaleDuration: Double = 0.666
    static let heartAlphaDuration: Double = 0.8
    static let delay = 0.05
    
    static func likeAnimation(on view: UIView) {
        Utils.dispatchOnMainThread {
            //Create hearts
            var hearts = [UIImageView]()
            for _ in 0..<LikeAnimator.numberOfHearts {
                let heart = UIImageView(image: UIImage(named:"large_liked"))
                heart.contentMode = .center
                view.addSubview(heart)
                heart.frame = CGRect(origin: view.bounds.origin, size: view.bounds.size)
                heart.transform = CGAffineTransform(scaleX: 0, y: 0)
                hearts.append(heart)
            }
            
            //Animate them
            UIView.animateKeyframes(
                withDuration: LikeAnimator.totalDuration,
                delay: 0,
                options: .calculationModeCubic,
                animations: {
                    for (i, heart) in hearts.enumerated() {
                        let relativeDelay = Double(i) * LikeAnimator.delay
                        UIView.addKeyframe(withRelativeStartTime: relativeDelay, relativeDuration: LikeAnimator.heartScaleDuration, animations: {
                            heart.transform = CGAffineTransform(scaleX: 1, y: 1)
                        })
                        UIView.addKeyframe(withRelativeStartTime: relativeDelay, relativeDuration: LikeAnimator.heartAlphaDuration, animations: {
                            heart.alpha = 0
                        })
                    }
                },
                completion: { (💔) in
                    for heart in hearts {
                        heart.removeFromSuperview()
                    }
                }
            )
        }
    }
}
