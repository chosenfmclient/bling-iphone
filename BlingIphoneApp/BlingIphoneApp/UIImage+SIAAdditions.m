//
//  UIImage+SIAAdditions.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 6/8/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "UIImage+SIAAdditions.h"

@implementation UIImage (SIAAdditions)


+ (UIImage *)scaleImage:(UIImage *)image
               isOpaque:(BOOL)isOpaque
                  scale:(CGFloat)scale {
    
    CGSize size = CGSizeApplyAffineTransform(image.size, CGAffineTransformMakeScale(scale, scale));
    
    UIGraphicsBeginImageContextWithOptions(size, isOpaque, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

@end
