//
//  MashUpProgressMap.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 12/19/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

enum MashUpProgressStage: Int {
    case none = 0
    case record
    case invite
    case collect
    case create
    case processing
    case share
    case new
}

protocol MashupProgressMapDelegate: class {
    func mashupMapWasDismissed(_ map: MashUpProgressMap?)
    func mashupAction(stage: MashUpProgressStage)
}

class MashUpProgressMap: UIView {
    
    @IBOutlet weak var shareStateContainerView: UIView!

    @IBOutlet weak var recordItem: MashupProgressItem!
    @IBOutlet weak var inviteItem: MashupProgressItem!
    @IBOutlet weak var collectItem: MashupProgressItem!
    @IBOutlet weak var createItem: MashupProgressItem!
    @IBOutlet var progressMapItems: [MashupProgressItem]!
    
    @IBOutlet weak var shareItem: MashupProgressItem!
    @IBOutlet weak var newMashupItem: MashupProgressItem!
    var view: UIView!
    
    var progressStage: MashUpProgressStage = .none
    
    weak var delegate: MashupProgressMapDelegate?
    //MARK: Constraint madness!
    
    @IBOutlet weak var progressMapTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var shareContainerLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var lineViewShareStateConstraint: NSLayoutConstraint!
    @IBOutlet weak var lineViewProgressStateConstraint: NSLayoutConstraint!
    let shareContainerLeadingShareStateConstant: CGFloat = CGFloat(-15)
    var progressMapTrailingShareStateConstant: CGFloat {
        return shareStateContainerView.bounds.width
    }
    var bottomConstraint: NSLayoutConstraint?
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        view = (Bundle.main.loadNibNamed("MashUpProgressMap", owner: self, options: nil)![0] as! UIView)
        view?.frame = CGRect(origin: CGPoint(), size: frame.size)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(forView view: UIView,
                     height: CGFloat = 145,
                     delegate: MashupProgressMapDelegate?) {
        let frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: height)
        self.init(frame: frame)
        self.delegate = delegate
    }
    
    //MARK: UI
    func show(onView: UIView!, stage: MashUpProgressStage,
                     delegate: MashupProgressMapDelegate? = nil ,
                     height: CGFloat = 145) {
        
        heightAnchor.constraint(equalToConstant: height).isActive = true
        onView.addSubview(self)
        leadingAnchor.constraint(equalTo: onView.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: onView.trailingAnchor).isActive = true
        bottomConstraint = bottomAnchor.constraint(equalTo: onView.bottomAnchor,
                                                                   constant: height)
        bottomConstraint?.isActive = true
        onView.setNeedsLayout()
        onView.layoutIfNeeded()
        bottomConstraint?.constant = 0
        onView.setNeedsLayout()
        alpha = 0
        updateView(stage: stage)
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       options: .curveEaseInOut,
                        animations: {[weak onView, weak self] in
                            onView?.layoutIfNeeded()
                            self?.alpha = 1
            }, completion: {[weak self] (completed) in
        })
    }
    
    func dismiss(animated: Bool = true) {
        bottomConstraint?.constant = bounds.height
        UIView.animate(withDuration: animated ? 0.2 : 0.0,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {[weak self] in
                        self?.superview?.layoutIfNeeded()
            }, completion: {[weak self] (completed) in
                self?.removeFromSuperview()
                self?.delegate?.mashupMapWasDismissed(self)
        })
    }
    
    func performShareStateTransition() {
        progressMapTrailingConstraint.constant = progressMapTrailingShareStateConstant
        shareContainerLeadingConstraint.constant = shareContainerLeadingShareStateConstant
        lineViewProgressStateConstraint.isActive = false
        lineViewShareStateConstraint.isActive = true
        setNeedsLayout()
        UIView.animate(withDuration: 0.35,
                       delay: 0,
                       options: .curveEaseOut,
                       animations: {[weak self] in
                        guard let _ = self else { return }
                        for item in self!.progressMapItems {
                            item.setProgressViewSizeForShareState()
                        }
                        self?.layoutIfNeeded()
        }, completion: nil)
    }
    
    func updateView(stage: MashUpProgressStage) {
        if progressStage == stage { return }
        var activeItem: MashupProgressItem
        switch stage {
        case .record:
            //
            activeItem = recordItem
            titleLabel.text = "Start New Mashup!"
            break
        case .invite:
            activeItem = inviteItem
            titleLabel.text = "Invite Friends to Mashup"
            break
        case .collect:
            activeItem = collectItem
            titleLabel.text = "Add More Videos"
            break
        case .create:
            activeItem = createItem
            titleLabel.text = "Create Mashup Now!"
            break
        case .processing:
            activeItem = recordItem
            titleLabel.text = "Mashup is gone. Start new!"
        case .share:
            activeItem = shareItem
            titleLabel.text = "Share Your Mashup"
            break
        default:
            activeItem = recordItem
            break
        }
        progressStage = stage
        activeItem.isActive = true
        let activeIndex = progressMapItems.index(of: activeItem)
        for (index, item) in progressMapItems.enumerated() {
            guard let _ = activeIndex else {
                item.shouldAnimateStateChange = false
                item.isCompleted = true
                continue
            }
            item.isCompleted = index < activeIndex!
            item.isPending = index > activeIndex!
        }
        if stage == .share {
            performShareStateTransition()
        }
    }
    
    //MARK: Item actions
    @IBAction func recordItemWasTapped(_ sender: Any) {
        delegate?.mashupAction(stage: .record)
    }
    @IBAction func inviteItemWasTapped(_ sender: Any) {
        delegate?.mashupAction(stage: .invite)
    }
    @IBAction func collectItemWasTapped(_ sender: Any) {
        delegate?.mashupAction(stage: .collect)
    }
    @IBAction func createItemWasTapped(_ sender: Any) {
        delegate?.mashupAction(stage: .create)
    }
    @IBAction func shareItemWasTapped(_ sender: Any) {
        delegate?.mashupAction(stage: .share)
    }
    @IBAction func newMashupItemWasTapped(_ sender: Any) {
        delegate?.mashupAction(stage: .new)
    }
    
    @IBAction func closeButtonWasTapped(_ sender: Any) {
        dismiss()
    }
    
    //MARK: State handling
    

}
