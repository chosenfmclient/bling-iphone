//
//  SIAShareMapping.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 12/9/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAShareMapping.h"

@implementation SIAShareMapping

+(RKObjectMapping *)shareObjectMapping {
    
    RKObjectMapping *shareMapping = [RKObjectMapping requestMapping];
    
    [shareMapping addAttributeMappingsFromDictionary:@{@"share_id":@"share_id",@"platform":@"platform", @"shareTags":@"tag"}];
    
    return shareMapping;
    
}

+(RKObjectMapping *)objectMapping{
    
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[SIAShareMapping class]];
    [mapping addAttributeMappingsFromDictionary:@{@"share_id":@"share_id",
                                                  @"user_id":@"user_id",
                                                  @"object_id": @"object_id",
                                                  @"type" :@"type",
                                                  @"platform" : @"platform",
                                                  @"date":@"date",
                                                  @"text":@"text",
                                                  @"email_subject":@"mailSubject"
                                                  }];
    
    return mapping;
}


@end

@implementation SIAShareMappingWrapper

    +(RKObjectMapping *)objectMapping {
        RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[SIAShareMappingWrapper class]];
        
        [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"data" withMapping:[SIAShareMapping objectMapping]]];
//        [mapping addPropertyMapping:[];
        
        return mapping;
    }

@end
