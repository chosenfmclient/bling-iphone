//
//  SIACountdownView.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/4/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIACountdownView.h"

static CGFloat countGrowDuration = 0.5;
static CGFloat countShrinkDuration = 0.2;
static CGFloat countDelay = 0.3;
static CGFloat countFadeDuration = 0.3;

@interface SIACountdownView ()

@property (strong, nonatomic) UILabel *count1Label;
@property (strong, nonatomic) UILabel *count2Label;
@property (strong, nonatomic) UILabel *count3Label;
@property (strong, nonatomic) UILabel *count4Label;
@property (strong, nonatomic) UILabel *count5Label;
@property (strong, nonatomic) NSArray *countLabelsArray;
@property (nonatomic) NSUInteger currentCount;

@property BOOL interruptCountdown;

@end

@implementation SIACountdownView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id) initWithFrame:(CGRect)frame count:(NSUInteger)count {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.layer.cornerRadius = self.frame.size.height / 2;
        self.layer.masksToBounds = YES;
        _count1Label = [[UILabel alloc] init];
        _count2Label = [[UILabel alloc] init];
        _count3Label = [[UILabel alloc] init];
        _count4Label = [[UILabel alloc] init];
        _count5Label = [[UILabel alloc] init];

        _count1Label.text = @"1";
        _count2Label.text = @"2";
        _count3Label.text = @"3";
        _count4Label.text = @"4";
        _count5Label.text = @"5";
        _countLabelsArray = @[_count1Label,
                             _count2Label,
                             _count3Label,
                              _count4Label,
                              _count5Label];
        for (UILabel *countLabel in _countLabelsArray) {
            countLabel.frame = self.bounds;
            countLabel.backgroundColor = [[SIAStyling defaultStyle] blingMainColor];
            countLabel.hidden = YES;
            countLabel.layer.cornerRadius = countLabel.frame.size.height / 2;
            countLabel.layer.masksToBounds = YES;
            countLabel.transform = CGAffineTransformMakeScale(0., 0.);
            countLabel.textAlignment = NSTextAlignmentCenter;
            countLabel.font = kAppFontLight(75.);
            countLabel.textColor = [UIColor whiteColor];
        }
        _currentCount = count;
    }
    return self;
}

- (void) stopCountdown {
    self.interruptCountdown = YES;
}

- (void) countdownWithHandler:(void(^)(NSUInteger currentCount))count {
    
    count(_currentCount);
    if (self.interruptCountdown || _currentCount == 0)
    {
        [self removeFromSuperview];
        return;
    }
    UIView *currentCountLabel = _countLabelsArray[_currentCount-1];
    [self addSubview:currentCountLabel];
    [self growAnimation:currentCountLabel
         withCompletion:^(BOOL finished)
     {
         [self shrinkAnimation:currentCountLabel
                withCompletion:^(BOOL finished)
          {
              [self fadeAnimation:currentCountLabel
                   withCompletion:^(BOOL finished)
               {
                   _currentCount--;
                   [self countdownWithHandler:count];
               }];
          }];
     }];
}

- (void) growAnimation:(UIView *)view withCompletion:(void(^)(BOOL finished))handler {
        view.hidden = NO;
        [UIView animateWithDuration:countGrowDuration
                              delay:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             view.transform = CGAffineTransformMakeScale(1., 1.);
                         } completion:^(BOOL finished) {
                             handler(finished);
                         }];
}

- (void) shrinkAnimation:(UIView *)view withCompletion:(void(^)(BOOL finished))handler {
    
    [UIView animateWithDuration:countShrinkDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         view.transform = CGAffineTransformMakeScale(0.8, 0.8);
                     } completion:handler];
}

- (void) fadeAnimation:(UIView *)view withCompletion:(void(^)(BOOL finished))handler {
    
    [UIView animateWithDuration:countFadeDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         view.alpha = 0;
                     } completion:handler];
}

@end
