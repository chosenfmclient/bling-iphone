//
//  SIAInstagramManager.h
//  SingrIphoneApp
//
//  Created by Roni Shoham on 29/12/13.
//  Copyright (c) 2013 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SIAInstagramManager : NSObject <UIDocumentInteractionControllerDelegate> {
    UIWebView* mywebview;
}


@property(nonatomic,retain)UIDocumentInteractionController *docFile;
-(void)saveToInstagram:(UIViewController *)view;

@end
