//
//  UIImage+SIAAdditions.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 6/8/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (SIAAdditions)

+ (UIImage *)scaleImage:(UIImage *)image
               isOpaque:(BOOL)isOpaque
                  scale:(CGFloat)scale;
@end
