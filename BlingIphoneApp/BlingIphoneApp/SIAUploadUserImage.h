//
//  SIAUploadUserImage.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 10/29/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface SIAUploadUserImage : NSObject

@property (nonatomic, strong) NSString *image_signed_url;
@property (nonatomic, strong) NSString *image_method;
@property (nonatomic, strong) NSString *image_content_type;

+(RKObjectMapping *)objectMapping;

@end
