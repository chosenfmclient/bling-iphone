//
//  ChooseVideoModel.swift
//  
//
//  Created by Michael Biehl on 9/20/16.
//
//

import UIKit
import RestKit

protocol BackgroundVideo: class {
    
    var clip_id: String? { get }
    var songName: String? { get }
    var artistName: String? { get }
    var blingTimes: [Float64]? { get }
    var stupidBlingTimes: [Any]? { get set }
    var downloadedFilePathURL: URL? { get set }
    var operation: VideoDownloadOperation? { get set }
    var downloadableUrl: URL? { get }
    var thumbnailURL: URL? { get set }
    func downloadVideo(progressBlock: @escaping (_ percent: Float) -> (),
                       completion: @escaping (_ url: URL?, _ error: Error?) -> (Void))
    func cancelDownload()
    func deleteDownloadedVideo()
    func getBlingTimes(completion: @escaping (_ times: [Float64]?) -> ())
}

extension BackgroundVideo {
    
    internal var blingTimes: [Float64]? {
        get {
            return (stupidBlingTimes as? [Float64])?.filter({ $0 > 2.5 })
        }
    }
    
    func downloadVideo(progressBlock: @escaping (_ percent: Float) -> (),
                       completion: @escaping (_ url: URL?, _ error: Error?) -> (Void)) {
        
        guard let _ = downloadableUrl else { return }
        operation = VideoDownloadOperation(url: downloadableUrl! as URL!)
        operation?.download(progressBlock: progressBlock,
                            completion: completion)
    }
    
    func cancelDownload() {
        operation?.cancel()
    }
    
    func deleteDownloadedVideo() {
        operation?.deleteDownloadedVideo()
    }
    
    func getBlingTimes(completion: @escaping (_ times: [Float64]?) -> ()) {
        
        if let _ = stupidBlingTimes, stupidBlingTimes!.count > 0 {
            completion(stupidBlingTimes as? [Float64])
            return
        }
        RequestManager.getPrimitiveObjectAt("api/v1/time-segments",
                                            queryParameters:  ["clip_id" : clip_id ?? ""],
                                            success: {[weak self] (operation, object) in
                                                self?.stupidBlingTimes = object as? [Any]
                                                completion(object as? [Float64])
        },
                                            failure: {(operation, error) in
                                                completion(nil)
        })
    }
}

extension SIAVideo: BackgroundVideo {
    
//    internal var blingTimes: [Float64]? {
//        get {
//            return blingTimeObjects.map()
//        }
//    }

    internal var artistName: String? {
        get {
            return artist_name
        }
    }

    internal var songName: String? {
        get {
            return song_name
        }
    }

    internal var downloadableUrl: URL? {
        return videoUrl as URL
    }

//    internal var operation: VideoDownloadOperation?
//
//    internal var downloadedFilePathURL: NSURL!
    
}

@objc class MusicVideo: NSObject, BackgroundVideo {
    
    internal var downloadableUrl: URL? {
        get {
            return previewUrl as URL?
        }
    }
    
    internal var songName: String? {
        get {
            return trackName
        }
    }

    internal var operation: VideoDownloadOperation?

    internal var downloadedFilePathURL: URL?

    var trackId = ""
    var artistName: String?
    var trackName = ""
    var thumbnailURL: URL?
    var clip_id: String?
    var previewUrl: NSURL!
    var correctSizeThumbURL: NSURL?
    var bannerLabel: String?
    var bannerColor: String?
    internal var stupidBlingTimes: [Any]?

    var isPlaying = false
    
    convenience init(video: SIAVideo) {
        self.init()
        self.trackName = video.song_name
        self.artistName = video.artist_name
        self.previewUrl = video.song_clip_url as NSURL!
        self.thumbnailURL = video.song_image_url
        self.clip_id = video.clip_id
    }
    
    static func objectMapping() -> (RKMapping) {
        let mapping = RKObjectMapping(for: MusicVideo.self)
        mapping!.addAttributeMappings(from: ["trackId", "artistName", "trackName", "previewUrl", "clip_id"])
        mapping!.addAttributeMappings(from: ["artworkUrl100":"thumbnailURL", "label":"bannerLabel", "color":"bannerColor", "time_segments":"stupidBlingTimes"])
        return mapping!
    }
    
    static func blingTimesMapping() -> (RKMapping) {
        let mapping = RKObjectMapping(for: MusicVideo.self)
        mapping?.addPropertyMapping(RKAttributeMapping(fromKeyPath: nil,
                                                       toKeyPath: "blingTimes"))
        
        return mapping!
    }
    
//    func getBlingTimes(completion: @escaping (_ times: [Float64]?) -> ()) {
//        
//        if let _ = blingTimes, blingTimes!.count > 0 {
//            completion(blingTimes as! [Float64]?)
//            return
//        }
//
//        RequestManager.getPrimitiveObjectAt("api/v1/time-segments",
//                                            queryParameters:  ["song_clip_url" : previewUrl.absoluteString!],
//                                            success: { (operation, object) in
//                                                completion(object as? [Float64])
//        },
//                                            failure: {(operation, error) in
//                                                completion(nil)
//        })
//    }
    
    // this is some old crap that I just haven't removed yet :) -- MNB
    func correctSizeThumb(for height: CGFloat) -> NSURL? {
        return thumbnailURL as NSURL?
    }
}

@objc class ChooseVideoModel: NSObject {
    
    var videos: [MusicVideo] = []
    
    static func objectMapping() -> (RKObjectMapping) {
        let mapping = RKObjectMapping(for: ChooseVideoModel.self)
        mapping!.addPropertyMapping(RKRelationshipMapping(fromKeyPath:"results", toKeyPath:"videos", with:MusicVideo.objectMapping()))
        return mapping!
    }
    
    static func getBlingVideos(searchTerm: String?, handler: @escaping (_ model: [MusicVideo]?, _ term: String?) -> (Void)) {
        
        if let term = searchTerm {
            let params = ["q" : term, "type" : "clip"]
            RequestManager.getRequest("api/v1/search",
                                      queryParameters: params,
                                      success: { (operation, result) in
                                        let data = (result?.firstObject as? SearchResult)
                                        handler(data?.videos, searchTerm)
            },
                                      failure: { (request, error) in
                                        
                                        handler(nil, searchTerm)
            })
        } else {
            RequestManager.getRequest("api/v1/record",
                                      queryParameters: nil,
                                      success: { (operation, result) in
                                        let data = (result?.firstObject as? SearchResult)
                                        handler(data?.videos, searchTerm)
            },
                                      failure: { (request, error) in
                                        
                                        handler(nil, searchTerm)
            })
        }
        
    }
    
    static func getVideos(searchTerm: String, handler: @escaping (_ model: ChooseVideoModel?) -> (Void)) {
        
        guard let urlEncodedTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {
            return
        }
        
        let path = "https://itunes.apple.com/search?term=\(urlEncodedTerm)&media=musicVideo&entity=musicVideo"
        
        let responseDescriptor = RKResponseDescriptor(mapping: ChooseVideoModel.objectMapping(),
                                                      method:RKRequestMethod.GET,
                                                      pathPattern: nil,
                                                      keyPath: nil,
                                                      statusCodes:IndexSet(integersIn: 200..<205))
        let urlRequest = URLRequest(url: URL(string: path)!)
        let operation = RKObjectRequestOperation(request:urlRequest, responseDescriptors:[responseDescriptor!])!
        operation.httpRequestOperation.acceptableContentTypes = ["text/javascript"]

        RKMIMETypeSerialization.registerClass(RKNSJSONSerialization.self, forMIMEType:"text/javascript")
        
        
        operation.setCompletionBlockWithSuccess({(request: RKObjectRequestOperation?, result: RKMappingResult?) in
                handler((result?.firstObject as! ChooseVideoModel))
            },
            failure:{(request: RKObjectRequestOperation?, error: Error?) in
                handler(nil)
        })
        
        SIAObjectManager.shared().enqueue(operation) // use this instead of start to prevent a CRASH

    }
}
