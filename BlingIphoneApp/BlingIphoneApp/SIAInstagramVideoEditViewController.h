//
//  SIAInstagramVideoEditViewController.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 4/2/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIAVideoPlayer.h"
#import "SIARecordingItem.h"

@interface SIAInstagramExportAsset : NSObject

@property (strong, nonatomic) NSURL *assetURL;
@property (strong, nonatomic) AVAsset *asset;
@property (nonatomic) CMTimeRange exportTimeRange;
@property (nonatomic) CMTimeRange clipTimeRange;
@property (nonatomic) BOOL hasNoRecordedAudio;

@end

@interface SIAInstagramVideoEditViewController : SIABaseViewController

@property (strong, nonatomic) BIAVideoPlayer *effectPlayer;
@property (strong, nonatomic) UIImage *image;
@property (nonatomic) NSInteger frameId;
@property (strong, nonatomic) SIAInstagramExportAsset *exportAsset;
@property (nonatomic) BOOL isCameoShare;

@end
