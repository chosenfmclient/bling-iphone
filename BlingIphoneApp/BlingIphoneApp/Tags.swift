//
//  Tags.swift
//  BlingIphoneApp
//
//  Created by Zach on 18/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
@objc class Tags: NSObject{
    init(tags:String) {
        self.tags = tags
    }
    
    var tags: String?
}
