//
//  SIALike.h
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 9/17/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SIALike : NSObject

@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *video_id;
@property (strong, nonatomic) NSString *like_id;
@property (strong, nonatomic) SIAUser *user;
@property (strong, nonatomic) NSString *date;

+ (void) addLikeToVideo:(NSString *)videoId withCompletion: (void (^)())completion;
+(void) removeLikeFromVideo:(NSString *)videoId withCompletion: (void (^)())completion;
+ (RKObjectMapping *)objectMapping;

@end
