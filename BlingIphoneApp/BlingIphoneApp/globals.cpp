//
// Created by Ben on 06/12/2016.
//

#include "globals.h"

const int WHITE_PIXEL = 255
, MAX_DISTANCE = 5
, MAX_DISTANCE_X_2 = MAX_DISTANCE * 2
, MAX_DISTANCE_X_3 = MAX_DISTANCE * 3
, CAMERA_WIDTH = 640
, CAMERA_HEIGHT = 480
, FACE_DETECTION_ADDON = 50
, NUMBER_OF_CHROMA_KEYS_THREADS = 2
;
//, FACE_DETECTION_DECON = 20;

cv::Mat edgesBufferGlobal;

int
      WIDTH = 480, HEIGHT = 640
    , WIDTH_X_4 = WIDTH * 4
    , WIDTH_X_3 = WIDTH * 3

    , MAX_SIZE = WIDTH * HEIGHT
    , MAX_SIZE_X_4 = WIDTH * HEIGHT * 4
    , MAX_SIZE_X_3 = WIDTH * HEIGHT * 3
    , CAMERA_PADDING = 0
    , CAMERA_DIFFERENCE = 0;
;

cv::Size defMatSize(WIDTH, HEIGHT), detMatSize_1138X640(1138, 640);

float scaleFactor = 1.0f;

int cameraRotation = 90, cameraRes = 3;

const int DEFAULT_CAMERA_WIDTH = 360, DEFAULT_CAMERA_HEIGHT = 480;

//const int *RES_960X720 = 1;//, RES_1280X960 = 2, RES_1280_720 = 3;

bool isCameraMirrored = false, isFaceDetectionOn = false;

////////////    face detection  /////////////

String haarCascadeFilePath;
const cv::Size faceSizeKernel(60, 60);
const int HaarOptions = CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_DO_ROUGH_SEARCH; // 0|CV_HAAR_SCALE_IMAGE;//

/////////////////////////////////////////////

int hueSkinColorBot, hueSkinColorTop, saturationSkinColorBot, saturationSkinColorTop, luminanceSkinColorBot, luminanceSkinColorTop;


const cv::Size  Kernel_5X5(5, 5),
                CAMERA_RESIZE_SIZE(640, 480);


///////////// fade /////////////

const int FADE_THRESHOLD = 10, FADE_DECREASER = 255 / FADE_THRESHOLD;

////////////////////////////////

int strongestRed, strongestGreen, strongestBlue;
const int IDEAL_COLOR_INTENSITY = 200;
const float BACKGROUND_EDGES_ACCURACY_PERCENTEGE = 0.1;

////////////Object//////////////


imageUtil iUtil;
chromaKey cUtil;


////////////////////////////////
const bool drawDetectionGuilds = false;
