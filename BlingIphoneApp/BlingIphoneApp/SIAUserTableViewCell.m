//
//  SIAFollowingTableViewCell.m
//  ChosenIphoneApp
//
//  Created by Roni Shoham on 09/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAUserTableViewCell.h"

#define IMAGE_PADDING_FROM_BORDER (15.)
#define IMAGE_SIZE (38.)


@implementation SIAUserTableViewCell


- (void)layoutSubviews {
    [super layoutSubviews];

    self.imageView.frame = CGRectMake(IMAGE_PADDING_FROM_BORDER, (58 - IMAGE_SIZE) / 2, IMAGE_SIZE, IMAGE_SIZE);
    self.imageView.layer.cornerRadius = IMAGE_SIZE / 2;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.backgroundColor = [UIColor colorWithRed:51./255. green:51./255. blue:51./255 alpha:1.];
    
    self.textLabel.numberOfLines = 1;
    self.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.textLabel.textColor = [UIColor whiteColor];
    self.detailTextLabel.textColor = [UIColor whiteColor];
    self.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.detailTextLabel.numberOfLines = 3;
    
    CGRect textLabelFrame = self.textLabel.frame;
    CGRect detailTextLabelFrame = self.detailTextLabel.frame;
    
    textLabelFrame.origin.x = CGRectGetMaxX(self.imageView.frame) + 15.0;
    detailTextLabelFrame.origin.x = textLabelFrame.origin.x;

    if (self.detailTextLabel.attributedText) {
        detailTextLabelFrame.size = [self.detailTextLabel.attributedText boundingRectWithSize:CGSizeMake(self.contentView.frame.size.width - textLabelFrame.origin.x, self.contentView.frame.size.height - CGRectGetMaxY(textLabelFrame))
                                                                                                 options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
        detailTextLabelFrame.size.width = self.contentView.frame.size.width - detailTextLabelFrame.origin.x - 15;
        [self.detailTextLabel.attributedText drawWithRect:detailTextLabelFrame
                                                  options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine
                                                  context:nil];
    }
    else {
        [self.detailTextLabel sizeToFit];
    }
    
    textLabelFrame.size.height = [self.textLabel.text sizeWithAttributes:@{NSFontAttributeName:self.textLabel.font}].height + 1;
    textLabelFrame.origin.y = self.imageView.center.y - (textLabelFrame.size.height / 2);
    
    
    self.detailTextLabel.backgroundColor = [UIColor clearColor];
    
    self.notificationFirstLine.numberOfLines = 0;
    self.notificationFirstLine.lineBreakMode = NSLineBreakByWordWrapping;
    
    //[self.textLabel sizeToFit];

    if (self.detailTextLabel.text.length > 0)
    {
        CGFloat totalTextLabelHeight = textLabelFrame.size.height + detailTextLabelFrame.size.height;
        textLabelFrame.origin.y = self.imageView.center.y - totalTextLabelHeight / 2;
        detailTextLabelFrame.origin.y = CGRectGetMaxY(textLabelFrame);
    }
    self.textLabel.frame = textLabelFrame;
    self.detailTextLabel.frame = detailTextLabelFrame;
}


@end
