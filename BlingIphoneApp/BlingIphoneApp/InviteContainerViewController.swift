//
//  InviteContainerViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 16/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

protocol CameoInvites: class {
    var selectedCameoInvitees: [CameoInvitee] { get set }
    var cameoId: String? { get set }
    func addUserToInvitees(_ user: SIAUser)
    func addContactToInvitees(_ contact: DeviceContact)
    func inviteSelectedInvitees()
    func inviteAllInList(_ list: [CameoInvitee])
    func flagPreviouslyInvitedUsers(_ users: [SIAUser])
    func flagPreviouslyInvitedContacts(_ contacts: [DeviceContact])
}

extension CameoInvites {
    
    func addUserListToInvitees(_ users: [SIAUser]) {
        for user in users {
            addUserToInvitees(user)
        }
    }
    
    func addUserToInvitees(_ user: SIAUser) {
        user.wasInvited = true
        selectedCameoInvitees.append(
            CameoInvitee(id: user.user_id.md5(),
                         name: user.fullName,
                         user_id: user.user_id
            )
        )
    }
    
    func addContactListToInvitees(_ contacts: [DeviceContact]) {
        for contact in contacts {
            addContactToInvitees(contact)
        }
    }
    
    func addContactToInvitees(_ contact: DeviceContact) {
        contact.wasInvited = true
        selectedCameoInvitees.append(
            CameoInvitee(id: contact.cameoIdentity(),
                         name: contact.name != nil ? contact.name! : "?",
                         email: contact.email,
                         phone: contact.phoneNumber
            )
        )
    }
    
    func inviteSelectedInvitees() {
        inviteAllInList(selectedCameoInvitees)
    }

    func inviteAllInList(_ list: [CameoInvitee]) {
        guard let cameoId = cameoId else {
            print("How you gonna invite without a Cameo Id????")
            return
        }
        guard list.count > 0 else { return }
        let sendInvitesModel = CameoSendInvitesModel(invitees: list,
                                                     cameoId: cameoId)
        let appDel = (UIApplication.shared.delegate as! AppDelegate)
        let idsList = list.map { $0.md5Identity }
        appDel.addUserIds(idsList,
                          toInvitedListFor: cameoId)
        RequestManager.postRequest("api/v1/cameo-invite",
                                   object: sendInvitesModel,
                                   queryParameters: nil,
                                   success: {(req, res) in
                                    print("Invites were sent for duet/cameo/whatever it's called!")
                                   
            },
                                   failure:{ (req, err) in
                                    appDel.removeUserIds(idsList,
                                                         fromInvitedListFor: cameoId)
        })
    }
    
    func flagPreviouslyInvitedUsers(_ users: [SIAUser]) {
        let appDel = (UIApplication.shared.delegate as! AppDelegate)
        guard let invitedUsers = appDel.listOfInvitedUsers(for: cameoId!) else { return }
        for user in users where invitedUsers.contains(user.user_id.md5()) {
                user.wasInvited = true
        }
    }
    
    func flagPreviouslyInvitedContacts(_ contacts: [DeviceContact]) {
        let appDel = (UIApplication.shared.delegate as! AppDelegate)
        guard let invitedUsers = appDel.listOfInvitedUsers(for: cameoId!) else { return }
        for contact in contacts where invitedUsers.contains(contact.cameoIdentity()){
                contact.wasInvited = true
        }
    }
    
}

protocol CameoInviteFlagging: class {
    func userListWasLoaded(_ list: [SIAUser])
}

@objc class InviteContainerViewController: SIABaseViewController, DiscoverSearchBarViewDelegate, PushProfileDelegate, CameoInvites, CameoInviteFlagging {

    
    //MARK: Outlets
    @IBOutlet weak var inviteTabButtobn: UIButton!
    @IBOutlet weak var followTabButton: UIButton!
    @IBOutlet var highlightViewInviteConstraint: NSLayoutConstraint!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var searchBarView: DiscoverSearchBarView!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    
    //MARK: Properties
    let nFollowTab = 0
    let nInviteTab = 1
    var tab = 1
    let contactHandler = ContactsHandler()
    var tabViewControllers = [UIViewController]()
    var currentQuery = ""
    
    var isDuetInvite = false
    var cameoVideo: SIAVideo?
    var cameoId: String?
    var selectedCameoInvitees = [CameoInvitee]()
    //MARK: View controller methods
    override func viewDidLoad() {
        backButton.isHidden = true
        inviteTabButtobn.isEnabled = false
        followTabButton.isEnabled = true
        followTabButton.alpha = 0.4
        
        navigationController?.clearNavigationItemLeftButton()
        
        //Setup the follow and invite view controllers
        weak var wContactHandler = self.contactHandler

        let followVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "followViewController") as! FollowViewController
        followVC.contactHandler = wContactHandler!
        followVC.delegate = self
        followVC.cameoInviteDelegate = self
        followVC.isCameoInvite = isDuetInvite
        followVC.isRegistration = isDuetInvite
        
        let inviteVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "inviteViewController") as! InviteViewController
        inviteVC.isRegistration = false
        inviteVC.contactHandler = wContactHandler!
        inviteVC.isCameoInvite = isDuetInvite
        inviteVC.cameoInviteDelegate = self

        tabViewControllers = [followVC, inviteVC]
        tabViewControllers[nFollowTab] = followVC
        tabViewControllers[nInviteTab] = inviteVC
        
        addChildViewController(inviteVC)
        addChildViewController(followVC)
        
        if isDuetInvite {
            followVC.flaggingDelegate = self
            Utils.dispatchInBackground {[weak self, weak followVC] in
                followVC?.getFollowingForCameoInvite() {
                    guard let _ = self?.cameoId else { return }
                    guard let usersToFlag = followVC?.friendsOnBling.filter({ $0 != nil }) as? [SIAUser], usersToFlag.count > 0 else { return }
                    self?.flagPreviouslyInvitedUsers(usersToFlag)
                }
            }
            
            inviteTabButtobn.alpha = 0.4
            inviteTabButtobn.isEnabled = true
            followTabButton.alpha = 1
            followTabButton.isEnabled = false
            changeToTab(nFollowTab, animated: false)
            //insert invite into contrainer
            insertViewControllerToContainer(followVC)
        }
        else {
            Utils.dispatchInBackground {[weak self, weak inviteVC] in
                inviteVC?.getContactsToInvite() {
                    
                }
            }
            
            //insert invite into contrainer
            insertViewControllerToContainer(inviteVC)
        }
        
        //set seatch bar
        searchBarView.delegate = self
        searchBarView.textFieldLeftPaddingConstraint.constant = 0
        
        if isDuetInvite {
            setForDuetInvite()
            AmplitudeEvents.log(event: "duet_invite:page")
        }
    }
    
    func setForDuetInvite() {
        followTabButton.setTitle("FOLLOWING", for: .normal)
        inviteTabButtobn.setTitle("CONTACTS", for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        if isDuetInvite {
            inviteSelectedInvitees()
            AmplitudeEvents.log(event: "duet_invite:invite",
                                with: ["invites_sent" : selectedCameoInvitees.count])
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        backButton.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        contactHandler.dataWasLoaded = false
    }
    @IBAction func backButtonWasTapped(_ sender: AnyObject) {
        navigationController.back()
    }

    //MARK: Tab button actions
    @IBAction func followTabButtonWasTapped(_ sender: AnyObject) {
        inviteTabButtobn.alpha = 0.4
        inviteTabButtobn.isEnabled = true
        followTabButton.alpha = 1
        followTabButton.isEnabled = false
        let requestClosure = { [weak self] in
            guard let sSelf = self else { return }
            if sSelf.currentQuery.isEmpty {
                (sSelf.tabViewControllers[sSelf.nFollowTab] as! SearchableUserList).searchCanceled()
            } else {
                (sSelf.tabViewControllers[sSelf.nFollowTab] as! SearchableUserList).newSearch(sSelf.currentQuery)
            }
            if let _ = sSelf.cameoId {
                let usersToFlag = (sSelf.tabViewControllers[sSelf.nFollowTab] as! FollowViewController).friendsOnBling.filter {
                    $0 != nil
                } as! [SIAUser]
                if usersToFlag.count > 0 {
                    self?.flagPreviouslyInvitedUsers(usersToFlag)
                }
            }
        }
        if isDuetInvite {
            (tabViewControllers[nFollowTab] as! FollowViewController)
                .getFollowingForCameoInvite(requestClosure)
        }
        else {
            (tabViewControllers[nFollowTab] as! FollowViewController)
                .getContactsToFollow(requestClosure)
        }
        changeToTab(nFollowTab)
    }
    
    @IBAction func inviteTabButtonWasTapped(_ sender: AnyObject) {
        followTabButton.alpha = 0.4
        followTabButton.isEnabled = true
        inviteTabButtobn.alpha = 1
        inviteTabButtobn.isEnabled = false
        (tabViewControllers[nInviteTab] as! InviteViewController).getContactsToInvite { [weak self] in
            guard let sSelf = self else { return }
            
            if sSelf.currentQuery.isEmpty {
                (sSelf.tabViewControllers[sSelf.nInviteTab] as! SearchableUserList).searchCanceled()
            } else {
                (sSelf.tabViewControllers[sSelf.nInviteTab] as! SearchableUserList).newSearch(sSelf.currentQuery)
            }
            if let _ = sSelf.cameoId {
                let contactsToFlag = (sSelf.tabViewControllers[sSelf.nInviteTab] as! InviteViewController).contacts
                if contactsToFlag.count > 0 {
                    self?.flagPreviouslyInvitedContacts(contactsToFlag)
                }
            }
        }
        changeToTab(nInviteTab)
    }
    
    //MARK: Tab set up
    func changeToTab(_ tab: Int, animated: Bool = true) {
        self.tab = tab
        let transition = CATransition()
        transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.containerView.layer.add(transition, forKey: "fade")
        removeViewControllerFromContainer()
        insertViewControllerToContainer(tabViewControllers[tab])
        
        highlightViewInviteConstraint.isActive = tab == nInviteTab
        UIView.animate(
            withDuration: animated ? 0.2 : 0.0,
            delay: 0,
            options: .curveEaseInOut,
            animations: { [weak self] in
                self?.tabView.layoutIfNeeded()
            },
            completion: nil
        )
    }
    
    func insertViewControllerToContainer(_ vc: UIViewController) {
        containerView.addSubview(vc.view)
        vc.view.frame = containerView.bounds
        vc.didMove(toParentViewController: self)
    }
    
    func removeViewControllerFromContainer() {
        for vc in childViewControllers {
            vc.removeFromParentViewController()
        }
        for subView in containerView.subviews {
            subView.removeFromSuperview()
        }
    }
    
    //MARK: Search
    func searchFieldWasEntered() {
        //dont care
    }
    func searchWasCleared() {
    }
    
    func cancelWasTapped() {
        currentQuery = ""
        (tabViewControllers[tab] as! SearchableUserList).searchCanceled()
    }
    
    func searchQuery(_ query: String) {
        guard currentQuery != query else { return }
        currentQuery = query
        guard !query.isEmpty else {
            (tabViewControllers[tab] as! SearchableUserList).searchCanceled()
            return
        }
        (tabViewControllers[tab] as! SearchableUserList).newSearch(query)
    }
    
    func pushProfile(_ userID: String) {
        let profileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "profileViewController") as! ProfileViewController
        profileVC.userId = userID
        navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func userListWasLoaded(_ list: [SIAUser]) {
        flagPreviouslyInvitedUsers(list)
    }
}

protocol SearchableUserList {
    func newSearch(_ search: String)
    func searchCanceled()
}
