//
//  SIAPostRecordViewController.h
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 2/8/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIABasePostRecordViewController.h"
#import "SIAShare.h"

FOUNDATION_EXTERN NSString *const SIAVideoProcessingDidFinish;

@class BIAVideoPlayer;
@class SIABlingRecordViewController;

@interface SIAPostRecordViewController : BIANavigationController <SIABasePostRecordViewControllerDelegate>

@property(weak, nonatomic) SIARecordingItem *recordingItem;
@property(weak, nonatomic) BIAVideoPlayer *player;
@property (nonatomic, strong) SIABlingRecordViewController *blingRecordVC;

@property (strong, nonatomic) NSURL *compositedVideoURL;
@property (strong, nonatomic) NSData *compositedPhotoData;

- (void)stopRepeatingPlayback;

- (void)repeatPlaybackFrom:(CGFloat)startTime to:(CGFloat)endTime;

- (void)xButtonWithCompletionHandler:(void(^)(void))handler;

- (void) startUpload;

- (void)uploadCompletedWithVideo:(SIAVideo*)video;

- (instancetype)initWithRecordingItem:(SIARecordingItem *)recordingItem
                               player:(BIAVideoPlayer *)player;

- (void)readdTopView;

- (void)saveToCameraRoll;

@end
