//
//  SIATumblrManager.m
//  SingrIphoneApp
//
//  Created by Roni Shoham on 30/12/13.
//  Copyright (c) 2013 SingrFM. All rights reserved.
//

#import "SIATumblrManager.h"


@implementation SIATumblrManager
/*
// Authenticate via OAuth
[TMAPIClient sharedInstance].OAuthConsumerKey = @"MAWHgAIRYBBzNAeutKEsKKkhh1grKzULiNneeCpS2wkVSltppF";
[TMAPIClient sharedInstance].OAuthConsumerSecret = @"Rqkc0snDMoTQy0HYznRIl6lE84ykIFib7wAf5TyWlVeIaZS07A";
[TMAPIClient sharedInstance].OAuthToken = @"niIQDxBy7TdgLXufPOM1iGaK54Zj8VStAJikRa5YdZGyxjUQun";
[TMAPIClient sharedInstance].OAuthTokenSecret = @"HVipX92o2hkKi7pcQ2srvnUuX8yN0226x5Zg4JWzOvvcSjLaO1";

// Make the request
[[TMAPIClient sharedInstance] userInfo:^ (id result, NSError *error) {
    // ...
}];
*/

@synthesize responseData = _responseData;
- (BOOL)sendPhotoToTumblr:(NSString *)photo usingEmail:(NSString *)tumblrEmail andPassword:(NSString *)tumblrPassword withCaption:(NSString *)caption
{
    //get image data from file
     NSURL *imageURL     = [NSURL URLWithString:photo]; //@"http://placekitten.com/500/400"
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    //NSData *imageData = [NSData dataWithContentsOfFile:photo];
    //stop on error
    if (!imageData) return NO;
    
    //Create dictionary of post arguments
    NSArray *keys = [NSArray arrayWithObjects:@"email",@"password",@"type",@"caption",nil];
    NSArray *objects = [NSArray arrayWithObjects:
                        tumblrEmail,
                        tumblrPassword,
                        @"photo", caption, nil];
    NSDictionary *keysDict = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    //create tumblr photo post
    NSURLRequest *tumblrPost = [self createTumblrRequest:keysDict withData:imageData];
    //[keysDict release];
    
    //send request, return YES if successful
    NSURLConnection *tumblrConnection = [[NSURLConnection alloc] initWithRequest:tumblrPost delegate:self];
    if (!tumblrConnection) {
        NSLog(@"Failed to submit request");
        return NO;
    } else {
        NSLog(@"Request submitted");
        //NSMutableData *receivedData = [[NSMutableData data] retain];
        //[tumblrConnection release];
        return YES;
    }
}


-(NSURLRequest *)createTumblrRequest:(NSDictionary *)postKeys withData:(NSData *)data
{
    //create the URL POST Request to tumblr
    NSURL *tumblrURL = [NSURL URLWithString:@"http://www.tumblr.com/api/write"];
    NSMutableURLRequest *tumblrPost = [NSMutableURLRequest requestWithURL:tumblrURL];
    [tumblrPost setHTTPMethod:@"POST"];
    
    //Add the header info
    //NSString *stringBoundary = [NSString stringWithString:@"0xKhTmLbOuNdArY"];
    NSString *stringBoundary = @"0xKhTmLbOuNdArY";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",stringBoundary];
    [tumblrPost addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    //create the body
    NSMutableData *postBody = [NSMutableData data];
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //add key values from the NSDictionary object
    NSEnumerator *keys = [postKeys keyEnumerator];
    int i;
    for (i = 0; i < [postKeys count]; i++) {
        NSString *tempKey = [keys nextObject];
        [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",tempKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[[NSString stringWithFormat:@"%@",[postKeys objectForKey:tempKey]] dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    //add data field and file data
    //[postBody appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"data\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"data\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //[postBody appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[NSData dataWithData:data]];
    [postBody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //add the body to the post
    [tumblrPost setHTTPBody:postBody];
    
    return tumblrPost;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    self.responseData = [[NSMutableData alloc] init];
    [self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Connection failed: %@", [error description]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    //Getting your response string
    //NSString *responseString = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    self.responseData = nil;
}


@end
