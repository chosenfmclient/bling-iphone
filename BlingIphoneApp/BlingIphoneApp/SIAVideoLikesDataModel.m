//
//  SIAVideoLikesDataModel.m
//  
//
//  Created by Michael Biehl on 1/16/16.
//
//

#import "SIAVideoLikesDataModel.h"
#import "SIALike.h"

#define LIKES_PATH @"api/v1/videos/%@/likes"

@implementation SIAVideoLikesDataModel

- (instancetype) initWithVideoId:(NSString *)videoId {
    
    self = [super init];
    
    if (self)
    {
        self.videoId = videoId;
    }
    
    return self;
}

- (instancetype) initWithObjectId:(NSString *)objectId {
    
    self = [super init];
    
    if (self)
    {
        self.videoId = objectId;
        self.objectId = objectId;
    }
    
    return self;
}

- (NSString *) pagePath {
    return [NSString stringWithFormat:LIKES_PATH, self.videoId];
}

- (RKMapping *) objectMapping {
    return [SIALike objectMapping];
}

+ (RKMapping *)pageMapping {
    return [SIAPaginatorDataModel responseMappingForTotalKey:@"total_likes"
                                               objectMapping:[SIALike objectMapping]
                                                  modelClass:[SIAVideoLikesDataModel class]];
}



@end
