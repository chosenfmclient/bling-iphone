//
//  SIACommentsTableViewController.m
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/10/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIACommentsTableView.h"
#import "SIAVideoTableViewCell.h"
#import "SIATextCommentActionsController.h"
//#import "SIAProfileViewController.h"
#import "SIAInfoCarryingTapGestureRecognizer.h"

@interface SIACommentsTableViewController ()
@property (weak, nonatomic) IBOutlet SIATextCommentView *commentView;
@end

@implementation SIACommentsTableViewController {
    NSMutableArray<SIATextComment *> *_comments;
    SIAUser *_currentUser;
    SIATextComment *_selectedComment;

    UILongPressGestureRecognizer *_longPressGestureRecognizer;
}

@synthesize comments = _comments;

- (NSString *)navigationTitleString {
    return @"Comments";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _currentUser = [BIALoginManager sharedInstance].myUser;

    self.commentView.commentColor = @"black";
    self.commentView.name = @"You!";
    self.commentView.comment = @"Say something...";
    self.commentView.delegate = self;
    [self.commentView addActionWithType:SIATextCommentActionSendType];
    [self.commentView addActionWithType:SIATextCommentActionCancelType];

    self.paginator = [[SIAPaginator alloc] initForModelClass:[SIATextCommentModel class]
                                                    delegate:self];

    ((SIATextCommentModel *) self.paginator.paginatorModel).videoId = _video.videoId;
    [self.paginator fetchFirstPage];

    _longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(_longPress:)];
    [_longPressGestureRecognizer setMinimumPressDuration:1.0f];
    [self.tableView addGestureRecognizer:_longPressGestureRecognizer];
}

- (void)viewDidAppear:(BOOL)animated {
    /*[Flurry logEvent:FLURRY_VIEW_COMMENTS
      withParameters:@{kVideoIdKeyStr:_video.videoId,
                       @"media_type":_video.mediaType}];*/
}

- (void)_longPress:(UILongPressGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        CGPoint gestureLocation = [gesture locationInView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:gestureLocation];
        [self tableViewDidLongTapCellForRowAtIndex:indexPath];
    }
}

- (NSURL *)imageURLForRow:(NSInteger)row {
    SIATextComment *comment = self.paginator.results[row];
    return comment.user.imageURL;
}

- (CGFloat)tableHeaderSize {
    return 30.0;
}

- (NSString *)tableHeaderString {
    
    if (!self.paginator.results) return [NSString string];
    
    NSString *commentOrComments = @"comments";
    if (self.paginator.paginatorModel.totalObjects.integerValue == 1) {
        commentOrComments = @"comment";
    }
    
    return [NSString stringWithFormat:@"%i %@ on this video", self.paginator.paginatorModel.totalObjects.integerValue, commentOrComments];
}

- (NSString *)cellIdentifierForRow:(NSInteger)row {
    if ([self.paginator.results[row] isKindOfClass:[[NSNull null] class]]) {
        return LOADING_CELL_ID;
    }
    return @"commentCell";
}

- (SIACommentTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SIACommentTableViewCell *cell = (SIACommentTableViewCell *) [super tableView:tableView
                                                           cellForRowAtIndexPath:indexPath];
    if ([self.paginator.results[indexPath.row] isKindOfClass:[[NSNull null] class]]) return cell;

    SIATextComment *comment = self.paginator.results[indexPath.row];
    if ([comment.user.user_id isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:kUserIdKeyStr]])
    {
        cell.nameLabel.text = @"You!";
    }
    else
    {
    cell.nameLabel.text = comment.user.fullName;
    }
    cell.commentLabel.text = comment.text;

    if (cell.imageView.gestureRecognizers.count == 0) {
        SIAInfoCarryingTapGestureRecognizer *tap = [[SIAInfoCarryingTapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapProfileImage:)];
        tap.userInfo = @{@"indexPath" : indexPath};
        [cell.imageView addGestureRecognizer:tap];
        [cell.imageView setUserInteractionEnabled:YES];
    }

    return cell;
}

- (void)tableViewDidLongTapCellForRowAtIndex:(NSIndexPath *)indexPath {
    _selectedComment = self.paginator.results[indexPath.row];
    SIATextCommentActionsController *actions = [[SIATextCommentActionsController alloc] initWithUser:_currentUser];
    [self presentViewController:[actions actionsAlertControllerForComment:_selectedComment presentingController:self]
                       animated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"DID SELECT YO");
}

- (BOOL)textCommentViewShouldBeginEditing:(SIATextCommentView *)commentView {
   /* [Flurry logEvent:FLURRY_TAP_COMPOSECOMMENT_COMMENTS
      withParameters:@{kVideoIdKeyStr:_video.videoId,
                       @"media_type":_video.mediaType}];*/
    return YES;
}


- (void)textCommentView:(SIATextCommentView *)commentView userDidSendComment:(NSString *)comment {
    BOOL editedComment = NO;
    NSString *commentId;
    
    if(!_selectedComment) {
        /*[Flurry logEvent:FLURRY_TAP_SENDCOMMENT_COMMENTS
          withParameters:@{kVideoIdKeyStr:_video.videoId,
                           @"media_type":_video.mediaType}];*/
        [SIATextCommentModel postComment:comment forVideo:self.video.videoId withCompletionHandler:^(SIATextComment *model) {
            if (!model)
                return;

            model.user.first_name = @"You!";
            model.user.last_name = @"";

            NSMutableArray *firstPageArray = [self.paginator.paginatorModel.pageDict objectForKey:@"0"];
            [firstPageArray insertObject:model
                                 atIndex:0];
            if (firstPageArray)
                [self.paginator.paginatorModel.pageDict setObject:firstPageArray
                                                           forKey:@"0"];
            [self.tableView beginUpdates];
            self.paginator.paginatorModel.totalObjects = @(self.paginator.paginatorModel.totalObjects.integerValue + 1);
            [self.tableView reloadData];
            [self.tableView endUpdates];
            [self.tableView beginUpdates];
            [self.paginator.results insertObject:model atIndex:0];
            [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]]
                                  withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView endUpdates];

            commentView.comment = @"Say something...";
        }];
    } else {
        editedComment = YES;
        commentId = _selectedComment.commentId;
        [_selectedComment editComment:comment withCompletionHandler:^(BOOL success) {
            if (!success)
                return;

            _selectedComment.text = comment;

            [commentView removeFromSuperview];
            [self.tableView reloadData];

            _selectedComment = nil;
        }];
    }
}

- (void)textCommentViewDidEndEditing:(SIATextCommentView *)commentView {
    self.commentView.comment = @"Say something...";
}

- (void)didTapProfileImage:(SIAInfoCarryingTapGestureRecognizer *)gestureRecognizer {
    NSIndexPath *indexPath = (NSIndexPath *) gestureRecognizer.userInfo[@"indexPath"];
    SIATextComment *comment = self.paginator.results[indexPath.row];
    
    [_commentView endCommentEditing];
    
    [self goToProfile:comment.user];
}

- (void)goToProfile:(SIAUser *)user {

    /*UIStoryboard *profileSB = [UIStoryboard storyboardWithName:@"ProfileStoryboard" bundle:nil];
    SIAProfileViewController *profileController = [profileSB instantiateViewControllerWithIdentifier:@"newProfile"];
    [self.navigationController pushViewController:profileController animated:YES];
    profileController.profileUserId = user.user_id;*/
}

@end
