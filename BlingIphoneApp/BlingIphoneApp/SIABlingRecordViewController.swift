 
//
//  BlingRecordViewController.swift
//  ChosenIphoneApp
//
//  Created by Hen Levy on 13/06/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

import UIKit
import Appsee

enum SIARecordButtonAction: Int {
    case ActionRegion = 0, StartRecording, PauseRecording, ResumeRecording, StopRecording, TakePhoto
}

let kAnimationDuration = 0.3

@objc class SIABlingRecordViewController: SIABaseViewController, BIAVideoPlayerDelegate, RecorderOpenCVDelegate, BlingModeManagerDelegate, VideoPlayerManagerDelegate, RedetectDialogDelegate, DetectionFailedViewDelegate {
    
    // IBOutlets
    @IBOutlet weak var rightRedetectConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftRedetectConstraint: NSLayoutConstraint!
    @IBOutlet weak var chromaKeyCameraView: UIView!
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var recordButton: SIARecordButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var stopButtonCenterYConstraint: NSLayoutConstraint!
    @IBOutlet weak var finishVideoSpinner: SIASpinner!
    @IBOutlet weak var recordButtonBgView: UIView!
    @IBOutlet weak var speedButtonContainer: UIView!
    @IBOutlet weak var normalSpeedButton: UIButton!
    @IBOutlet weak var fastSpeedButton: UIButton!
    @IBOutlet var blingModeButtonContainer: UIView!
    @IBOutlet var blingModeButton: UIButton!
    @IBOutlet weak var detectButton: UIButton!
    @IBOutlet weak var trackProgressView: UIProgressView!
    @IBOutlet var countdownSelectorView: UIView!
    @IBOutlet weak var countdownSelectorCancelButton: UIButton!
    @IBOutlet weak var smallRedetectButtonContainer: UIView!
    @IBOutlet weak var smallRedetectButton: UIButton!
    @IBOutlet weak var smallDetectButton: UIButton!
    @IBOutlet weak var errorImageView: UIImageView!
    
    var changeCameraBarButton: UIBarButtonItem! = nil
    var countdownTimeBarButton: UIBarButtonItem! = nil
    var speedButtonSelectedColor: UIColor?
    var speedButtonDeselectedColor: UIColor?
    var countdownTime: UInt = 3
    @IBOutlet weak var tutorialImage: UIImageView!
    // instructions detection iboutlets
    @IBOutlet weak var detectionOverlayView: UIView!
    @IBOutlet weak var detectionStatusView: UIView!
    @IBOutlet weak var instructionsLabel: UILabel!
    @IBOutlet weak var instructionsDetailLabel: UILabel!
    @IBOutlet weak var instructionsContainer: UIView!
    @IBOutlet weak var detectionInstructionsLabel: UILabel!
    
    @IBOutlet weak var detectionSuccessImage: UIImageView!
    @IBOutlet weak var detectionFailureImage: UIImageView!

    @IBOutlet weak var detectionSpinner: SIASpinner!
    
    @IBOutlet weak var downloadSpinner: SIASpinner!
    @IBOutlet weak var videoDownloadProgressLabel: UILabel!
    
    var tutorialInterstitial: TutorialVideoInterstitial?
    @IBOutlet weak var tutorialVideoView: UIView!
    @IBOutlet weak var playTutorialButton: UIButton!
    
    var firstPersonImageViewLeftConstant: CGFloat = 0
    var firstPersonViewLeftConstant: CGFloat = 0
    
    // music video
    var musicVideo: MusicVideo?
    var player: BIAVideoPlayer? = nil
    var playerLayer = AVPlayerLayer()
    let chromaKeyLayer = CALayer()
    let playerItemKeyPath = "status"
    
    // bling mode crap
    var blingManager: BlingModeManager?
    
    // cameo!
    var cameoParent: SIAVideo?
    
    
    // background video
    var backgroundVideo: BackgroundVideo? {
        if let _ = musicVideo {
            return musicVideo
        }
        else if let _ = cameoParent {
            return cameoParent
        }
        return nil
    }
    
    var amplitudeProps: [String : AnyHashable] {
        guard let _ = backgroundVideo else { return [String : AnyHashable]() }
        return ["clip_id" : backgroundVideo!.clip_id ?? "null",
                "artist_name" : backgroundVideo!.artistName ?? "null",
                "video_name" : backgroundVideo!.songName ?? "null"]
    }
    
    // chroma key
    var backCamera = false
    var bothMode = true //false
    var didSetup = false
    var isCapturing = false
    var shouldDetectBackground = false
    
    // recording
    var recordingItem = SIARecordingItem(forRecorder: ())
    var encoder: SIAVideoEncoder?
    var cameraPosition: AVCaptureDevicePosition = .front
    var videoOutput: AVCaptureVideoDataOutput!
    var videoOutputSize = CGSize.zero
    var startTimeInterval: TimeInterval = 0
    var didFinish = false
    var recorderOpenCV = RecorderOpenCVWrapper()
    
    //var greenScreenRecorder: GreenScreenRecorder?
    
    var detectionCount = 0
    
    // face recognition
//    let detector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
    
    // preview
    var playbackPlayer: BIAVideoPlayer? = nil
    var postRecordViewController: SIAPostRecordViewController? = nil
    
    // countdown
    var countdownView: SIACountdownView? = nil
    var isCountdown = false
    
    //progress
    var progressObserver: Any?
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recordButton.tag = SIARecordButtonAction.StartRecording.rawValue
//        personView.clipsToBounds = true
        speedButtonSelectedColor = normalSpeedButton.backgroundColor
        speedButtonDeselectedColor = fastSpeedButton.backgroundColor
        recorderOpenCV.setRecordingSpeedScale(1.0)
        recorderOpenCV.delegate = self
        stopButton.isHidden = true
        
        observeApplicationState()
        AmplitudeEvents.log(event: "record:camera",
                            with: amplitudeProps)
        UIApplication.shared.setStatusBarHidden(true, with: .fade)
        tutorialInterstitial = (Bundle.main.loadNibNamed("TutorialVideoInterstitial",
                                                         owner:self,
                                                         options:nil)?[0] as! TutorialVideoInterstitial)
        
        hideStopButton(true, animated: false, delay: 0)
    }
    
    private func observeApplicationState() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidEnterBackground(note:)),
                                               name: .UIApplicationDidEnterBackground,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationWillResignActive(note:)),
                                               name: .UIApplicationWillResignActive,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidBecomeActive(note:)),
                                               name: .UIApplicationDidBecomeActive,
                                               object: nil)
    }
    var resetForReshootWhenBackFromBackground = false
    var resetForRedetectWhenBackFromBackground = false
    var appWillResignActive = false
    @objc private func applicationWillResignActive(note: Notification) {
        appWillResignActive = true
        if recorderOpenCV.isRecording {
            performRecordControlAction(.PauseRecording)
        }
    }
    
    @objc private func applicationDidEnterBackground(note: Notification) {
        appWillResignActive = false
        
        if recorderOpenCV.isRecording {
          //  performRecordControlAction(.PauseRecording)
            //recorderOpenCV.stopRecording()
            recorderOpenCV.cancelRecording()
            VideoPlayerManager.shared.player.pause()
            recordButton.setRedViewForRecord()
            recordButton.tag = SIARecordButtonAction.StartRecording.rawValue
            fadeOutView(smallRedetectButtonContainer)
            resetForReshootWhenBackFromBackground = true
        }
        else if let _ = recorderOpenCV.captureSession,
                        recorderOpenCV.captureSession.isRunning &&
                        recorderOpenCV.didDetect {
            recorderOpenCV.resetForRedetect()
            resetForRedetectWhenBackFromBackground = true
        }
    }
    
    @objc private func applicationDidBecomeActive(note: Notification) {
        appWillResignActive = false
        Utils.dispatchOnMainThread {[weak self] in
            guard let _ = self else { return }
            
            if self!.resetForReshootWhenBackFromBackground {
                self!.resetForReshoot()
                self!.instructionsLabel.text = ""
            }
            else if self!.resetForRedetectWhenBackFromBackground {
                self!.tutorialImage.isHidden = false;
                self!.tutorialImage.alpha = 1
                self!.tutorialImage.superview?.bringSubview(toFront: self!.tutorialImage)
                self!.resetForRedetect()
                self!.fade(in: self!.detectionInstructionsLabel)
                self!.instructionsLabel.text = ""
            }
            self!.resetForReshootWhenBackFromBackground = false
            self!.resetForRedetectWhenBackFromBackground = false
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func backEvent() {
        AmplitudeEvents.log(event: "record:cancel", with: ["pagename": AmplitudeEvents.kRecordPageName])
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        normalSpeedButton.sia_roundCorners([.topLeft, .bottomLeft])
        fastSpeedButton.sia_roundCorners([.topRight, .bottomRight])
        stopButton.bia_rounded()
        blingModeButton.bia_rounded()
        detectButton.bia_rounded()
        smallDetectButton.bia_rounded()
        smallRedetectButton.bia_rounded()
        playTutorialButton.bia_rounded()
        if !didSetup {
            setupRecording()
        }
    }
    
    func setupOpenCv() {
        
       // AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) {[weak self] (granted) in
           // Utils.dispatchOnMainThread {[weak self] in
 //               guard let _ = self else { return }
//                if !granted {
//                    self!.showCameraAccessAlert()
//                    return
//                }
                recorderOpenCV.setupNewRecorder(with: chromaKeyCameraView)
                //completion?()
                if backCamera {
                    recorderOpenCV.switchCamera()
                }
                view.insertSubview(chromaKeyCameraView,
                                         belowSubview: detectionStatusView)
            //}
        //}
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.setStatusBarHidden(false, with: .fade)
        if !didFinish {
            navigationController?.clearNavigationItemRightButtons()
            VideoPlayerManager.shared.player.pause()
            recorderOpenCV.stopRecording()
            //greenScreenRecorder?.stopRecording()
        }
        if let operation = musicVideo?.operation, operation.isExecuting {
            musicVideo?.cancelDownload()
        }
       // recorderOpenCV.reset()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setBottomBarHidden(true, animated: false)
        navigationController?.setNavigationBarLeftButtonXWithAction(#selector(close))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    var waitForTutorial = false
    var prepareFinishedClosure: (() -> ())?
    
    func setupRecording() {
        guard let _ = backgroundVideo else { return }
        didSetup = true
        finishVideoSpinner.stopAnimation()
        layoutProgressBar()
        blurBackground(for: backgroundVideo!)
        downloadSpinner.startAnimation()
        hideStopButton(true, animated: false, delay: 0)
        
        let playTutorial = UserDefaults.standard.object(forKey: PLAY_TUTORIAL) as! Bool
        
        if playTutorial {
            tutorialInterstitial?.hideXButton = UserDefaults.standard.object(forKey: "allow_tutorial_skip") as! Bool
            let accessStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
            if accessStatus == .authorized {
                setupOpenCv()
            }
            // play here
            waitForTutorial = true
            tutorialInterstitial?.completionHandler = {[weak self] complete in
                guard let _ = self else { return }
                if accessStatus != .authorized {
                    self!.setupOpenCv()
                }
                UserDefaults.standard.setValue(false, forKey: "allow_tutorial_skip")
                UserDefaults.standard.setValue(false, forKey: PLAY_TUTORIAL)
                self!.waitForTutorial = false
                self!.prepareFinishedClosure?()
                self!.prepareFinishedClosure = nil
            }
            tutorialInterstitial?.show()
        }
        else {
            setupOpenCv()
        }
        
        prepareFinishedClosure = { [weak self] in
            guard let sSelf = self, let videoURL = self!.backgroundVideo?.downloadedFilePathURL, !sSelf.waitForTutorial else {
                return
            }
            VideoPlayerManager.shared.setNewPlayerView(view: self!.captureView,
                                                       videoURL: videoURL,
                                                       withDelegate: self!,
                                                       shouldLimitTime: false,
                                                       usingOption: .stream,
                                                       withSpinner: false)
            VideoPlayerManager.shared.shouldPlay = false
            if #available(iOS 10.0, *) {
                VideoPlayerManager.shared.player.automaticallyWaitsToMinimizeStalling = false
            } else {
                // Fallback on earlier versions
            }
            sSelf.fadeOutViews([sSelf.downloadSpinner, sSelf.videoDownloadProgressLabel, sSelf.instructionsLabel])
            sSelf.view.bringSubview(toFront: sSelf.chromaKeyCameraView)
            sSelf.view.bringSubview(toFront: sSelf.detectionStatusView)
            sSelf.recordButton.setRedViewForRecord()
            sSelf.fade(in: [sSelf.detectButton, sSelf.tutorialImage, sSelf.detectionInstructionsLabel])
            //sSelf.instructionsLabel.text = "We need to detect your background\nPush DETECT BACKGROUND\nand stand still 1-2 seconds"
            
            sSelf.instructionsDetailLabel.text = ""
            sSelf.setRightBarButtons()
        }
        if let _ = backgroundVideo {
            prepareForRecorder(backgroundVideo: backgroundVideo!, completion: prepareFinishedClosure!)
        }
        else {
            print("No background video for recording!")
        }
        
    }
    
    @IBAction func playTutorialWasTapped(_ sender: Any) {
        tutorialInterstitial?.hideXButton = false
        tutorialInterstitial?.show()
        tutorialInterstitial?.completionHandler = {[weak self] (butts) in
        }
    }
    
    
    
    // MARK: Setup
    func setRightBarButtons() {
        guard let _ = navigationController else {
            return
        }
        
        let switchCameraButton = UIButton(type: .custom)
        let cameraImage = UIImage(named: "icon_record_button_switch_camera");
        switchCameraButton.setImage(cameraImage, for: .normal)
        switchCameraButton.frame = CGRect(x: 0, y: 0, width: cameraImage!.size.width + 10, height: cameraImage!.size.height)
        switchCameraButton.addTarget(self,
                                     action: #selector(changeCameraDidTap(_:)), for: .touchUpInside)
        
        let timerButton = UIButton(type: .custom)
        let timerImage = UIImage(named:"icon_record_button_counter")
        timerButton.frame = CGRect(x: 0, y: 0, width: timerImage!.size.width + 20, height: timerImage!.size.height)
        timerButton.setImage(timerImage, for: .normal)
        timerButton.addTarget(self,
                              action: #selector(countdownTimeButtonWasTapped(_:)), for: .touchUpInside)
        let buttonTitle = NSAttributedString(string: String(countdownTime) + "s",
                                             attributes: [NSFontAttributeName : UIFont(name: "Roboto", size: CGFloat(14))!,
                                                          NSForegroundColorAttributeName : UIColor.white])
        timerButton.setAttributedTitle(buttonTitle, for: .normal)
        
        navigationController?.setNavigationBarRightButtons([switchCameraButton, timerButton])
        
        changeCameraBarButton = navigationItem.rightBarButtonItems?.first
        countdownTimeBarButton = navigationItem.rightBarButtonItems?.last
        
    }
    
    func setCountdownButtonTitle(count: UInt) {
        if let button = countdownTimeBarButton.customView as? UIButton {
            if (count == 0) {
                button.setImage(UIImage(named:"icon_record_button_counter_0"), for: .normal)
                button.setAttributedTitle(nil, for: .normal)
            }
            else {
                button.setImage(UIImage(named:"icon_record_button_counter"), for: .normal)
                button.setAttributedTitle(NSAttributedString(string: String(count) + "s",
                                                             attributes: [NSFontAttributeName : UIFont(name: "Roboto", size: CGFloat(14))!,
                                                                          NSForegroundColorAttributeName : UIColor.white]),
                                          for: .normal)
            }
        }
    }
    
    func countdownTimeButtonWasTapped(_ sender: UIButton) {
        setCountdownSelectorViewHidden(false,
                                       animated: true)
    }
    
    func setCountdownSelectorViewHidden(_ isHidden: Bool, animated: Bool) {
        UIView.transition(with: view,
                          duration: 0.25,
                          options: [.transitionCrossDissolve, .allowAnimatedContent],
                          animations: {[weak self] in
                            self?.navigationController?.setNavigationBarHidden(!isHidden, animated: false)
                            self?.countdownSelectorView.isHidden = isHidden
            }, completion:nil)
    }
    
    func blurBackground(for backgroundVideo: BackgroundVideo) {
        guard let url = backgroundVideo.thumbnailURL else {
            return
        }
        backgroundImageView.sd_setImage(with:url)
    }
    
    @IBAction func detectionButtonWasTapped(_ sender: UIButton?) {
        var viewsToFade: [UIView] = [detectionInstructionsLabel]
        recorderOpenCV.resetForRedetect()
        if sender == smallRedetectButton {
            sender!.layer.removeAllAnimations()
            viewsToFade.append(contentsOf: [smallRedetectButtonContainer, recordButtonBgView, speedButtonContainer])
        }
        else if let _ = sender {
            viewsToFade.append(sender!)
            viewsToFade.append(playTutorialButton)
        }
        if let failView = chromaKeyCameraView.viewWithTag(DetectionFailedView.viewTag()) as? DetectionFailedView {
            failView.dismiss()
        }
        //greenScreenRecorder?.startBackgroundDetection()
        detectionCount += 1
        fadeOutViews(viewsToFade)
        if detectionCount == 7 {
            detectionCount = 0
            showDetectionInstructions()
            return
        }
        recorderOpenCV.startDetection()
        detectionSpinner.startAnimation()
        view.bringSubview(toFront: tutorialImage)
        view.bringSubview(toFront: detectionSpinner)
        instructionsLabel.text = "Detecting..."
        fade(in: [detectionSpinner, tutorialImage, instructionsLabel])
        instructionsDetailLabel.text = ""
        errorImageView.isHidden = true;
        AmplitudeEvents.log(event: "record:detect start", with: amplitudeProps)

    }
    
    func showDetectionInstructions() {
        detectionSpinner.stopAnimation()
        let vc = UIStoryboard(name: "BlingStoryboard", bundle: nil).instantiateViewController(withIdentifier: "detectionFucked") as! DetectionFuckedUpViewController
        vc.delegate = self
        
        present(vc, animated: true, completion: nil)
    }
    
    //MARK: Green Screen Recorder DELEGATE!
    
    func detectionFinished(withResponse response: DetectionResponseS) {
        switch response {
        case .success:
            backgroundWasDetected()
            break
        default:
            backgroundDetectionFailed(response)
        }
    }
    
    func backgroundWasDetected() {
        detectionFinishedAnimation()
        AmplitudeEvents.log(event: "record:detect complete", with: amplitudeProps)
    }
    
    func backgroundDetectionFailed(_ failed: DetectionResponseS) {
        AmplitudeEvents.log(event: "record:detect failed", with: amplitudeProps)
        Utils.dispatchOnMainThread {[weak self] in
            guard let _ = self  else {return}
            self!.instructionsLabel.text = ""
            self!.fadeOutView(self!.tutorialImage)
            
            guard let detectionFailedView = Bundle.main.loadNibNamed("DetectionFailedView",
                                                                     owner: self,
                                                                     options: nil)?.first as? DetectionFailedView else { return }
            detectionFailedView.delegate = self!
            detectionFailedView.show(on: self!.chromaKeyCameraView,
                                     forReason: failed)
            self!.detectionFinishedAnimation(false)
        }
       // resetForRedetect()
    }
    
    func failedViewDidDismiss() {
    }
    
    func redetectWasTapped() {
        detectionButtonWasTapped(detectButton)
    }
    
    func recorderNeedsRedetect() {
        fadeOutViews([instructionsLabel, instructionsDetailLabel])
        fade(in: detectionInstructionsLabel)
        resetForRedetect()
    }
    
    func resetForRedetect() {
        self.fadeOutViews([self.detectionSpinner, self.videoDownloadProgressLabel, recordButtonBgView, speedButtonContainer, smallRedetectButtonContainer])
        //self.view.bringSubview(toFront: self.chromaKeyCameraView)
        self.detectButton.setTitle("REDETECT BACKGROUND", for: UIControlState.normal)
        let detButton = stopButtonCenterYConstraint.constant == 0 ? smallDetectButton! : detectButton!
        fade(in: [detButton, detectionStatusView, playTutorialButton])
        self.instructionsDetailLabel.isHidden = false
        self.setRightBarButtons()
    }
    
    func cameraPermissionWasDenied() {
        fadeOutViews([detectButton, tutorialImage])
        instructionsLabel.text = "Camera permission denied"
        instructionsDetailLabel.text = "Please give Blin.gy permission to use\nthe camera in your device settings"
        navigationController?.clearNavigationItemRightButtons()
    }
    
    func downloadFailed() {
        fade(in: detectionFailureImage)
        videoDownloadProgressLabel.text = nil
        instructionsLabel.text = "Download failed"
        instructionsDetailLabel.text = "Please try again"
        instructionsDetailLabel.isHidden = false
    }
    
    func prepareForRecorder(backgroundVideo: BackgroundVideo!, completion: @escaping (() -> ())) {
        downloadBackgroundVideoIfNeeded(backgroundVideo) { [weak self] (success) in
            guard let _ = self else {
                return
            }
            if !success {
                self!.downloadFailed()
                return
            }
            let asset = AVURLAsset(url: backgroundVideo.downloadedFilePathURL! as URL)
            
            self!.fixVideoSizeIfNeeded(asset: asset) { [weak self] (croppedVideoURL) in
                guard let _ = self else {
                    return
                }
                if let strongURL = croppedVideoURL {
                    backgroundVideo.downloadedFilePathURL = strongURL as URL
                }
                completion()
            }
        }
    }
    // initialize recorder here so it can start with the correct asset
    func newAssetWasCreated(_ asset: AVAsset) {
        guard !waitForTutorial else { return }
        Utils.dispatchOnMainThread {[weak self] in
            guard let _ = self else { return }
            if self!.recorderOpenCV.isSetUp() {
                self!.recorderOpenCV.setPlayer(VideoPlayerManager.shared.player)
            }
            else {
                self!.setupOpenCv()
                self!.recorderOpenCV.setPlayer(VideoPlayerManager.shared.player)
            }
            self!.view.bringSubview(toFront: self!.tutorialImage)
        }
        if let _ = backgroundVideo {
            backgroundVideo!.getBlingTimes(completion: {[weak self] (times) in
                if let _ = times {
                    Utils.dispatchOnMainThread {
                        guard let _ = self else { return }
                        self!.navigationItem.titleView = self!.blingModeButtonContainer
                        if !self!.setupBlingMode() {
                            self!.setupTrackProgressView()
                        }
                    }
                }
            })
        }
        else if let _ = cameoParent {
            Utils.dispatchOnMainThread {[weak self] in
                self?.setupTrackProgressView()
            }
        }
    }
    
    func playerDidReachEnd() {
        if !appWillResignActive {
            stopButtonWasTapped(stopButton)
        }
    }
    
    func setupBlingMode() -> Bool! {
        guard let _ = backgroundVideo else { return false }
        blingManager = BlingModeManager(backgroundVideo!,
                                        player: VideoPlayerManager.shared.player,
                                        delegate: self)
        setupTrackProgressView()
        guard let _ = backgroundVideo?.blingTimes else { return false }
        addBlingModePausesToProgressBar(at: backgroundVideo!.blingTimes!)
        return true
    }
    
    func addBlingModePausesToProgressBar(at times: [Float64]) {
        let tickWidth: CGFloat = 3.0
        
        //get duration
        var totalTime: Float64 = 15.0
        
        if let durationTime = VideoPlayerManager.shared.player.currentItem?.duration, durationTime.isValid, CMTimeGetSeconds(durationTime) > 0, CMTimeGetSeconds(durationTime) < totalTime
        {
            totalTime = CMTimeGetSeconds(durationTime)
        }
        else
        {
            print("WARNING - YOU ARE CALLING addBlingModePausesToProgressBar before the item is initialized." +
                "\nThis means that the default duration will be used. This will likely lead to bugs... please fix it :)")
        }
        
        //get ratios
        var progressPercentages = [Float64]()
        for time in times {
            guard time < totalTime else { continue }
            progressPercentages.append(time / totalTime)
        }
        
        //create views at correct origin
        let barSize = trackProgressView.frame.size
        for ratio in progressPercentages {
            if ratio > 1 {
                break;
            }
            let view = UIView()
            view.backgroundColor = UIColor.white
            trackProgressView.addSubview(view)
            view.frame = CGRect(x: (CGFloat(ratio) * barSize.width), y: 0, width: tickWidth, height: barSize.height)
        }
        trackProgressView.clipsToBounds = true
        recordButton.isEnabled = true
    }
    
    func blingPauseTimeReached() {
        if blingModeButton.isSelected {
            performRecordControlAction(.PauseRecording)
            instructionsLabel.text = "Ready for next scene"
            fade(in: instructionsLabel)
        }
    }
    
    func downloadBackgroundVideoIfNeeded(_ backgroundVideo: BackgroundVideo,
                                    completion: @escaping ((_ completed: Bool) -> ())) {
        if ((backgroundVideo.downloadedFilePathURL) != nil) {
            completion(true)
        }
        else {
            backgroundVideo.downloadVideo(
                progressBlock: { [weak self] (percent) in
                    guard let strongSelf = self else {
                        return
                    }
                    strongSelf.updateVideoDownloadProgress(percent)
                },
                completion: { [weak self] (url, error) in
                    guard let strongSelf = self, strongSelf.navigationController?.topViewController == self else {
                        return
                    }
                    if let _ = error {
                        completion(false)
                        return
                    }
                    backgroundVideo.downloadedFilePathURL = url
                    completion(true)
                })
        }
    }
    
    func updateVideoDownloadProgress(_ percent: Float) {
        if (videoDownloadProgressLabel.isHidden) {
            videoDownloadProgressLabel.isHidden = false
        }
        let adjustedPercent = percent * 0.8
        videoDownloadProgressLabel.text = String(Int(adjustedPercent)) + "%"
    }
    
    func updateVideoExportProgress(_ percent: Float) {
        let adjustedPercent = 80 + percent * 0.2
        videoDownloadProgressLabel.text = String(Int(adjustedPercent)) + "%"
    }
    
    // MARK: Recording
    
    
    // MARK: Actions
    @IBAction func redetectDidTap(sender: UIButton) {
    }
    
    @IBAction func changeCameraDidTap(_ sender: UIBarButtonItem) {
        isCapturing = false
        backCamera = !backCamera
        recorderOpenCV.usingFrontCamera = !backCamera;
        if backCamera {
            tutorialImage.image = UIImage(named:"silhouette_shadow_back")
        }
        else {
            tutorialImage.image = UIImage(named: "silhouette_shadow_front")
        }
        recorderOpenCV.switchCamera()
        view.bringSubview(toFront: tutorialImage)
        fadeOutViews([smallRedetectButtonContainer, smallDetectButton])
        //DispatchQueue.main.async {[weak greenScreenRecorder] in
        //    greenScreenRecorder?.switchCamera()
        //}
    }
    
    @IBAction func countdownSelectorButtonWasTapped(_ sender: UIButton) {
        countdownTime = UInt(sender.tag)
        setCountdownButtonTitle(count: countdownTime)
        setCountdownSelectorViewHidden(true, animated: true)
    }
    
    @IBAction func countdownSelectorCancelButtonWasTapped(_ sender: UIButton) {
        setCountdownSelectorViewHidden(true, animated: true)
    }
    @IBAction func recordButtonWasTapped(_ sender: SIARecordButton) {
        performRecordControlAction(SIARecordButtonAction(rawValue: sender.tag)!)
    }
    
    func performRecordControlAction(_ type: SIARecordButtonAction) {
//        guard let _ = player else {
//            return
//        }
        
        switch type {
        case SIARecordButtonAction.StartRecording:
            recordButton.tag = SIARecordButtonAction.PauseRecording.rawValue
            recordButton.isEnabled = false
            blingModeButton.isEnabled = false
            countdownTimeBarButton.isEnabled = false
            recordButton.alpha = 0.5
            setCountdownSelectorViewHidden(true,
                                           animated: true)
            startCountdown()
            AmplitudeEvents.log(event: "record:start", with: amplitudeProps)
            Appsee.addEvent("Record:start")
            break
        case SIARecordButtonAction.PauseRecording:
            recordButton.tag = SIARecordButtonAction.ResumeRecording.rawValue
            recordButton.setRedViewForRecord()
            countdownTimeBarButton.isEnabled = true
            recorderOpenCV.pauseRecording()
            fade(in: smallRedetectButtonContainer)
            //greenScreenRecorder?.pauseRecording()
            AmplitudeEvents.log(event: "record:pause", with: amplitudeProps)
            Appsee.addEvent("Record:pause")
            break
        case SIARecordButtonAction.ResumeRecording:
            fadeOutView(instructionsLabel)
            recordButton.tag = SIARecordButtonAction.PauseRecording.rawValue
            recordButton.setRedViewForPause()
            fadeOutView(smallRedetectButtonContainer)
            countdownTimeBarButton.isEnabled = false
            setCountdownSelectorViewHidden(true,
                                           animated: true)
            countdownView = nil
            Appsee.addEvent("Record:resume")
            let length: CGFloat = 100.0
            let countdownFrame = CGRect(x: captureView.frame.midX - length/2,
                                        y: captureView.frame.midY - length/2,
                                        width: length,
                                        height: length)
            countdownView = SIACountdownView(frame: countdownFrame,
                                             count: countdownTime)
            view.addSubview(countdownView!)
            recordButton.isEnabled = false
            countdownView?.countdown(handler: {[weak self] (currentCount) in
                if currentCount == 0 {
                    self?.recordButton.isEnabled = true
                    self?.recorderOpenCV.resumeRecording()
                    //self?.greenScreenRecorder?.resumeRecording()
                }
            })
            break
        default:
            break
        }
    }
    
    @IBAction func stopButtonWasTapped(_ sender: UIButton) {
        recorderOpenCV.stopRecording()
        //greenScreenRecorder?.stopRecording()
    }
    
    @IBAction func speedButtonWasTapped(_ sender: UIButton) {
        
        sender.isEnabled = false
        let deselectedButton = sender == normalSpeedButton ? fastSpeedButton! : normalSpeedButton!
        deselectedButton.isEnabled = true
        sender.sia_setBackgroundColorAnimated(speedButtonSelectedColor)
        sender.setTitleColor(UIColor.black, for: .normal)
        deselectedButton.sia_setBackgroundColorAnimated(speedButtonDeselectedColor)
        deselectedButton.setTitleColor(UIColor.white, for: .normal) 
        
        setRecordingSpeed(sender == normalSpeedButton ? 1.0 : 1.0/2.0)
    }
    
    func setRecordingSpeed(_ speedScale: Float64) {
        recordingItem?.recordingSpeedScale = speedScale
    }
    
    @IBAction func blingModeButtonWasTapped(_ sender: UIButton) {
        
        blingModeButton.isSelected = !blingModeButton.isSelected
        
        var color: UIColor
        var title: String
        if (blingModeButton.isSelected) {
            color = SIAStyling.defaultStyle().blingModeColor()
            title = "BLING MODE"
        }
        else {
            color = SIAStyling.defaultStyle().blingMainColor()
            title = "BASIC MODE"
        }
        blingModeButton.sia_setBackgroundColorAnimated(color)
        blingModeButton.setTitle(title,
                                 for: .normal)
        
    }
    
    
    func startCountdown() {
        isCountdown = true
        countdownView = nil
        stopButton.isHidden = false
        let length: CGFloat = 100.0
        let countdownFrame = CGRect(x: captureView.frame.midX - length/2,
                                    y: captureView.frame.midY - length/2,
                                    width: length,
                                    height: length)
        countdownView = SIACountdownView(frame: countdownFrame,
                                         count: countdownTime)
        view.addSubview(countdownView!)
        
        navigationController?.clearNavigationItemRightButton()
        
        //Appsee.pause();
        fade(in: instructionsLabel)
        instructionsLabel.text = "Please hold still"
        fadeOutViews([smallRedetectButtonContainer, speedButtonContainer, playTutorialButton])
        countdownView!.countdown { [weak self] (currentCount) in
            guard let strongSelf = self else {
                return
            }
            if currentCount == 0 {
//                strongSelf.setupEncoder()
                strongSelf.setupBlingMode()
                strongSelf.recordButton.isEnabled = true
                strongSelf.recorderOpenCV.setRecordingSpeedScale((strongSelf.recordingItem?.recordingSpeedScale)!)
                strongSelf.fadeOutView(strongSelf.instructionsLabel)
                strongSelf.recorderOpenCV.startRecording()
                //strongSelf.greenScreenRecorder?.recordingSpeedScale = (strongSelf.recordingItem?.recordingSpeedScale)!
                //strongSelf.greenScreenRecorder?.startRecording()
                strongSelf.recordButton.setRedViewForPause()
                strongSelf.recordButton.alpha = 1.0
                strongSelf.hideStopButton(false, animated: true, delay: 3)
                strongSelf.isCountdown = false
                Utils.dispatchAsync {
                    strongSelf.speedButtonContainer.isHidden = true
                }
            }
        }
    }
    
    func stopPlaying() {
        isCapturing = false
        if isCountdown {
            countdownView?.stopCountdown()
            isCountdown = false
            recordButton.isEnabled = true
            recordButton.alpha = 1.0
            return
        }
        if let _ = progressObserver {
            VideoPlayerManager.shared.player.removeTimeObserver(progressObserver!)
            progressObserver = nil
        }
        
        self.view.bringSubview(toFront: finishVideoSpinner)
        VideoPlayerManager.shared.delegate = nil
        VideoPlayerManager.shared.removeVideo()
//        finishVideoSpinner.image = UIImage(named: "Loading")
        finishVideoSpinner.startAnimation()
        hideStopButton(true, animated: true, delay: 0)
        fadeOutViews([recordButtonBgView, blingModeButtonContainer, countdownTimeBarButton.customView!, smallRedetectButtonContainer, smallDetectButton, detectButton, playTutorialButton])
        self.captureView.isHidden = true
        self.chromaKeyCameraView.isHidden = true
        self.trackProgressView.isHidden = true
        self.trackProgressView.setProgress(0, animated: false)
        recordButton.setRedViewForRecord()
        recordButton.tag = SIARecordButtonAction.StartRecording.rawValue
        setCountdownSelectorViewHidden(true,
                                       animated: false)
        instructionsLabel.text = "Preparing..."
        fade(in: instructionsLabel)
//        Utils.dispatchInDefaultPriority {[weak self] in
//            //guard let _ = self, let greenScreenRecorder = self?.greenScreenRecorder else { return }
//            
//            let merger = GreenScreenVideoMerger(backgroundAsset: VideoPlayerManager.shared.player.currentItem?.asset,
//                                                recordedAsset: AVURLAsset(url: (greenScreenRecorder.outputURL)!),
//                                                recordingSpeedScale: greenScreenRecorder.recordingSpeedScale,
//                                                colorCube: greenScreenRecorder.colorCube,
//                                                isInWhiteRange: greenScreenRecorder.isInWhiteRange,
//                                                minGroupHue: greenScreenRecorder.minGroupHue,
//                                                maxGroupHue: greenScreenRecorder.maxGroupHue)
//            
//            merger.mergeVideos {[weak self] (outputURL) in
//                guard let strongSelf = self else { return }
//                strongSelf.recordingItem?.recordURL = outputURL
                Utils.dispatchAsync { [weak self] in
                    guard let strongSelf = self else { return }
                    
                    strongSelf.recordingItem?.mediaType = "video"
                    strongSelf.recordingItem?.recordURL = strongSelf.recorderOpenCV.recordURL as URL!
                    strongSelf.recordingItem?.artist_name = strongSelf.musicVideo?.artistName
                    strongSelf.recordingItem?.song_name = strongSelf.musicVideo?.trackName
                    strongSelf.recordingItem?.assetForPlayback = VideoPlayerManager.shared.player.currentItem?.asset
                    strongSelf.playbackPlayer = BIAVideoPlayer(recordingItem: strongSelf.recordingItem,
                                                               delegate: strongSelf)
                    strongSelf.startPlaybackIfReady()
                    strongSelf.playbackPlayer?.playAfterAppBecomesActive = true
                    //Appsee.resume()
                    Utils.dispatchAsync {[weak self] in
                        guard let strongSelf = self else { return }
                        strongSelf.didFinish = true
                        strongSelf.finishVideoSpinner.stopAnimation()
                        strongSelf.fadeOutView(strongSelf.instructionsLabel)
                        strongSelf.navigationController?.setNavigationBarHidden(true, animated: false)
                        strongSelf.performSegue(withIdentifier: "postRecordSegue",
                                                sender: nil)
                        //strongSelf.present(strongSelf.postRecordViewController!, animated: true, completion: nil)
                    }
                }
            //}
        //}
    }
    
    func hideStopButton(_ isHidden: Bool,
                        animated: Bool,
                        delay: TimeInterval) {
        
        Utils.dispatchOnMainThread { [weak self] in
            guard let _ = self else { return }
            self!.stopButtonCenterYConstraint.constant = isHidden ? self!.stopButton.frame.midY * 3 : 0
            self!.stopButton.superview?.setNeedsLayout()
            UIView.animate(withDuration: animated ? 0.25 : 0,
                           delay: delay,
                           options: .curveEaseOut,
                           animations: { [weak self] in
                            self?.stopButton?.superview?.layoutIfNeeded()
                },
                           completion: nil)
        }
    }
    
    //MARK: Recorder Delegate
    
    func recordingDidStart() {
        
    }
    
    func recordingDidStop() {
        guard self == navigationController?.topViewController &&
            UIApplication.shared.applicationState == .active else {
                return
        }
        Appsee.addEvent("record:stop")
        AmplitudeEvents.log(event: "record:stop",
                            with: amplitudeProps)
        stopPlaying()
    }
    
    func backgroundPlaybackDidStart(withTimeOffset offset: CMTime) {
        recordingItem?.playbackStartOffset = offset;
    }
    
    func recorderReadyToRecord() {
        fadeOutViews([tutorialImage, instructionsLabel, instructionsDetailLabel])
        fade(in: recordButtonBgView)
       //  Utils.dispatchAfter(0.5) {[weak self] in
         //   guard let _ = self else { return }
            instructionsLabel.text = "Ready to record!"
            fade(in: instructionsLabel)
        //}
    }
    
    func recorderDidFail(toInitialize error: Error?) {
        Appsee.addEvent("record:cameraFail",
                        withProperties: ["error" : error?.localizedDescription ?? "Error description missing"])
        showCameraAccessAlert()
    }
    
    func showCameraAccessAlert() {
        let alert = UIAlertController(title: "Error",
                                      message: "We are unable to access your camera. If you denied permission to use the camera, please re-enable it by going to Settings -> Privacy -> Camera and tapping the switch for Blin.gy, then tap Retry.")
        let retryAction = UIAlertAction(title: "Retry",
                                        style: .default) {[weak self] (action) in
                                            self?.setupOpenCv()
        }
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel) {[weak self] (action) in
                                            self?.popBack()
        }
        alert.addAction(retryAction)
        alert.addAction(cancelAction)
        present(alert,
                animated: true,
                completion: nil)
    }
    
    func recorderDidFail(toReadBackgroundVideo error: Error?) {
        Appsee.addEvent("record:readBackgroundFail",
                        withProperties: ["error" : error?.localizedDescription ?? "Error description missing"])
        let alert = UIAlertController(title: "Error",
                                      message: "Something went wrong with the video clip, please try again")
        let retryAction = UIAlertAction(title: "Retry",
                                        style: .default) {[weak self] (action) in
                self?.popBack()
        }
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        alert.addAction(retryAction)
        alert.addAction(cancelAction)
        present(alert,
                animated: true,
                completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let postRecordVC = (segue.destination as? SIAPostRecordViewController) {
            recordingItem?.musicVideo = musicVideo
            recordingItem?.parentId = cameoParent?.videoId
            if let _ = cameoParent {
                recordingItem?.type = "cameo"
            }
            postRecordVC.recordingItem = recordingItem
            postRecordVC.player = playbackPlayer
            postRecordVC.blingRecordVC = self
        }
    }
    
    // MARK: Track Progress
    func layoutProgressBar() {
        trackProgressView.transform = CGAffineTransform(scaleX: 1.0, y: 3.0)
    }
    
    func setupTrackProgressView() {
        guard let playerDuration = VideoPlayerManager.shared.player.currentItem?.asset.duration,
            playerDuration.isNumeric
            else {
            print("NO ITEM?")
            return
        }
        var interval: Double = 0.5
        let duration = CMTimeGetSeconds(playerDuration);
        if duration.isFinite
        {
            let width = trackProgressView.bounds.width
            interval = 0.5 * duration / Double(width);
        }
        
        progressObserver = VideoPlayerManager.shared.player.addPeriodicTimeObserver(
            forInterval: CMTime(seconds: interval, preferredTimescale: 1000),
            queue: DispatchQueue.main,
            using: { [weak self] (time) in
                let progress = CMTimeGetSeconds(time) / duration
                if progress.isFinite && !progress.isNaN {
                    self?.trackProgressView.setProgress(Float(progress), animated: progress > 0)
                }
            }
        )
    }
    
    // MARK: Playback
    
    func startPlaybackIfReady() {
        playbackPlayer!.observeStatusForReadyToPlay()
    }
    
    func setupPlayer(completion: () -> ()) {
        VideoPlayerManager.shared.shouldPlay = false
        
        playbackPlayer = BIAVideoPlayer(recordingItem: recordingItem, delegate: self)
        
//        addAudioAndCropVideo(videoURL: recorderOpenCV.recordURL as URL,
//                             audioAsset: (player?.currentItem?.asset)!,
//                             callback: {[weak self] (url) in
//                                self?.recordingItem?.recordURL = url;
//                                self?.playbackPlayer = BIAVideoPlayer(recordingItem: self?.recordingItem, delegate: self)
//                                self?.playbackPlayer!.actionAtItemEnd = .none
//                                if #available(iOS 10.0, *) {
//                                    self?.playbackPlayer!.automaticallyWaitsToMinimizeStalling = false
//                                } else {
//                                    // Fallback on earlier versions
//                                }
//
//        })
    }
    
    func playerReadyToPlay() {
        startPlayback()
        playbackPlayer?.repeatPlayback()
    }
    
    func startPlayback() {
            Utils.dispatchAsync({ [weak self] in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.playbackPlayer!.filterView.frame = strongSelf.captureView.frame
                
                strongSelf.view.addSubview(strongSelf.playbackPlayer!.filterView)
                strongSelf.captureView.isHidden = true
                strongSelf.chromaKeyCameraView.isHidden = true
                strongSelf.recordButton.isHidden = true
                strongSelf.changeCameraBarButton.isEnabled = false
                strongSelf.recordButtonBgView.isHidden = true
                strongSelf.playbackPlayer!.play()
                print("Start Playback")
            })
    }
    
    func repeatPlayback() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { [weak self] (note) in
            self?.playbackPlayer?.seek(to: kCMTimeZero)
        }
    }
    
    func reshoot() {
            self.dismiss(animated: true, completion: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.resetForReshoot()
        })
    }
    
    func resetForReshoot() {
        setupTrackProgressView()
        //greenScreenRecorder?.resetForRedetect()
        //didSetup = false
        recorderOpenCV.reset()
        navigationController?.setNavigationBarHidden(false, animated: false)
        playbackPlayer?.filterView?.removeFromSuperview()
        playbackPlayer?.filterView?.playerLayer?.removeFromSuperlayer()
        playbackPlayer?.stopRepeatingPlayback()
        captureView.isHidden = false
        chromaKeyCameraView.isHidden = false
        speedButtonContainer.isHidden = false
        //            strongSelf.instructionsView.isHidden = false
        recordButton.isHidden = false
        changeCameraBarButton.isEnabled = true
        blingModeButton.isEnabled = true
        recordButtonBgView.isHidden = false
        trackProgressView.isHidden = false
        view.insertSubview(chromaKeyCameraView,
                           belowSubview: tutorialImage)
        didFinish = false
        hideStopButton(true, animated: false, delay: 0)
        //if !(greenScreenRecorder?.captureSession.isRunning)! {
        //    greenScreenRecorder?.captureSession.startRunning()
        //}
        //greenScreenRecorder?.didStartRecording = false
//        instructionsLabel.text = "Stand in front of a solid background"
//        instructionsDetailLabel.text = "and hold still for a sec"
        fade(in: [detectionInstructionsLabel, blingModeButtonContainer, countdownTimeBarButton.customView!, tutorialImage])
        VideoPlayerManager.shared.player.seek(to: kCMTimeZero)
        VideoPlayerManager.shared.shouldPlay = false
        VideoPlayerManager.shared.setNewPlayerView(view: captureView,
                                                   videoURL: (backgroundVideo?.downloadedFilePathURL)!,
                                                   withDelegate: self,
                                                   shouldLimitTime: false,
                                                   usingOption: .stream,
                                                   withSpinner: false)
        VideoPlayerManager.shared.shouldPlay = false
        view.sendSubview(toBack: captureView)
//        instructionsLabel.text = "Point the camera at a\nsolid color wall"
//        instructionsDetailLabel.text = "(So we can make it disappear)"
        detectionCount = 0
        resetForRedetect()
    }
    
    func close() {
        if recorderOpenCV.isRecording {
            if VideoPlayerManager.shared.player.rate > 0 {
                performRecordControlAction(.PauseRecording)
            }
            showAreYouSureMessage(closure: {[weak self] (goBack) in
                guard let _ = self else { return }
                if goBack {
                    self!.recordButton.setRedViewForRecord()
                    self!.recordButton.tag = SIARecordButtonAction.StartRecording.rawValue
                    self!.fadeOutViews([self!.smallRedetectButtonContainer, self!.instructionsLabel, self!.instructionsDetailLabel])
                    self?.resetForReshoot()
                }
            })
        }
        else {
            popBack()
        }
    }
    
    func popBack() {
        UIApplication.shared.setStatusBarHidden(false, with: .fade)
        navigationController?.popViewController(animated: true)
        tutorialInterstitial = nil
    }
    
    @objc func showAreYouSureMessage(closure: @escaping ((_ goBack: Bool) -> ())) {
        let conditionalString = didFinish ? "go back" : "close the camera"
        let alert = UIAlertController(title: "Discard Video",
                                      message: "If you \(conditionalString) now, your video will be discarded")
        alert.addAction(UIAlertAction(title: "Keep",
                                      style: .cancel,
                                      handler: {(action) in
                                        closure(false)
        }))
        alert.addAction(UIAlertAction(title: "Delete",
                                      style: .default,
                                      handler: { (action) in
                                        closure(true)
        }))
        let vc = presentedViewController != nil ? presentedViewController! : self
        vc.present(alert,
                   animated: true,
                   completion: nil)
    }
    
    // MARK: Edit Video
    
    func addAudioAndCropVideo(videoURL: URL, audioAsset: AVAsset, callback: @escaping ((URL) -> ())) {
        let videoEditor = LLVideoEditor(videoURL: videoURL as URL!)
        videoEditor?.addAudio(audioAsset)
       // videoEditor?.crop(CGRect(x: 0, y: 0, width: 320.0, height: 320.0 * (4.0/3.0)))
        
        let videoName = "croppedMovie.mov"
        let croppedURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(videoName)
        print(croppedURL?.absoluteString)
        
        if FileManager.default.fileExists(atPath: croppedURL!.path) {
            let _ = try? FileManager.default.removeItem(atPath: croppedURL!.path)
        }
        
        videoEditor?.export(to: croppedURL, presetName: AVAssetExportPresetMediumQuality, optimizeForNetworkUse: true, outputFileType: AVFileTypeQuickTimeMovie) { (session) in
            switch (session.status) {
            case .completed:
                callback(croppedURL!)
            case .failed:
                print("Failed:%@",session.error)
            case .cancelled:
                print("Canceled:%@", session.error)
            default:
                break;
            }
        }
    }
    // MARK: Animate Detection Instructions
    
    func detectionFinishedAnimation(_ succeeded: Bool = true) {
        instructionsLabel.text = succeeded ? "You're in\nthe video!" : "Might need\nto re-detect"
        fadeOutViews([tutorialImage, detectionSpinner])
        var viewsToFadeIn: [UIView] = [recordButtonBgView, speedButtonContainer, smallRedetectButtonContainer]
        if succeeded {
            viewsToFadeIn.append(detectionSuccessImage)
        }
        else {
            smallRedetectButton.bia_startPulsing()
        }
        fade(in: viewsToFadeIn)
        if !recorderOpenCV.isRecording {
            fade(in: playTutorialButton)
        }
        let currentCount = detectionCount
        Utils.dispatchAfter(1.5) { [weak self] in
            guard let _ = self else { return }
            self!.fadeOutViews([self!.detectionSuccessImage])
            guard self!.detectionCount == currentCount else { return }
            guard !self!.recorderOpenCV.isRecording else { return }
            self!.recordButton.setRedViewForRecord()
            Utils.dispatchAfter(2.0, closure: {
                if self?.instructionsLabel.text != "Detecting..." { //quick fix to a bug when redetecting fast @blin-1151
                    self?.instructionsLabel.text = "Ready to Record"
                    self?.smallRedetectButton.bia_stopPulsing()
                }
            })
        }
    }
    
    // MARK: Fix Video Bad Resolution
    
    func fixVideoSizeIfNeeded(asset: AVAsset, callback: @escaping ((NSURL?) -> ())) {
        let fixedWidth = recorderOpenCV.usingOldDevice ? 320 : 640;
        let fixedHeight = recorderOpenCV.usingOldDevice ? 240 : 480;
        if let _ = asset.tracks(withMediaType: AVMediaTypeVideo).first {
            
            //            let videoTrack = asset.tracks(withMediaType: AVMediaTypeVideo).first!
            let videoEditor = LLVideoEditor(videoURL: backgroundVideo!.downloadedFilePathURL as URL!)
            let cropRect = CGRect(x: 0,
                                  y: 0,
                                  width: fixedWidth,
                                  height: fixedHeight)
            videoEditor?.crop(cropRect)
            videoEditor?.trim(toDuration: 15.0)
            
            let hash = backgroundVideo?.downloadableUrl!.absoluteString.md5()
            let videoName = "crop\(hash!).mp4"
            let croppedURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(videoName)
            
            if FileManager.default.fileExists(atPath: croppedURL!.path) {
                let _ = try? FileManager.default.removeItem(atPath: croppedURL!.path)
            }
            videoEditor?.exportProgressBlock = {[weak self] (percent) in
                self?.updateVideoExportProgress(percent)
            }
            videoEditor?.export(to: croppedURL,
                                presetName: AVAssetExportPresetHighestQuality,
                                optimizeForNetworkUse: true,
                                outputFileType: AVFileTypeMPEG4) { (session) in
                                    switch (session.status) {
                                    case .completed:
                                        callback(croppedURL as NSURL?)
                                    case .failed:
                                        print("Failed:%@",session.error)
                                        callback(nil)
                                    case .cancelled:
                                        print("Canceled:%@", session.error)
                                        callback(nil)
                                    default:
                                        break;
                                    }
            }
        } else {
            callback(nil)
        }
    }
    
    // MARK: Deinit
    
    deinit {
        // remove observers
        if let _ = playbackPlayer {
            playbackPlayer?.stopRepeatingPlayback()
            playbackPlayer?.filterView?.removeFromSuperview()
            playbackPlayer?.filterView?.playerLayer?.removeFromSuperlayer()
            playbackPlayer?.replaceCurrentItem(with: nil)
        }
        if let _ = progressObserver {
            VideoPlayerManager.shared.player.removeTimeObserver(progressObserver!)
            progressObserver = nil
        }
        //            strongPlayer.currentItem!.removeObserver(self, forKeyPath: playerItemKeyPath)
        
        VideoPlayerManager.shared.playerLayer.isHidden = false
        if let _ = blingManager?.timeObserver {
            //      strongPlayer.removeTimeObserver(blingManager?.timeObserver)
        }
        //captureSession.stopRunning()
        recorderOpenCV.stopRecording()
        //greenScreenRecorder?.stopRecording()
        
        musicVideo?.deleteDownloadedVideo()
    }
 }
