//
//  SIAItunesMusicElement.h
//  ChosenIphoneApp
//
//  Created by Joseph Nahmias on 10/04/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SIAItunesMusicElement : NSObject

@property (strong, nonatomic) NSString *artistId;
@property (strong, nonatomic) NSString *collectionId;
@property (strong, nonatomic) NSString *trackId;
@property (strong, nonatomic) NSURL *artistViewUrl;
@property (strong, nonatomic) NSURL *collectionViewUrl;
@property (strong, nonatomic) NSURL *trackViewUrl;

@property (strong, nonatomic) NSString *artistName;
@property (strong, nonatomic) NSString *collectionName;
@property (strong, nonatomic) NSString *trackName;
@property (strong, nonatomic) NSURL *previewUrl;
@property (strong, nonatomic) NSURL *thumbnailURL; //artworkUrl100
@property (strong, nonatomic) UIImage *thumbnail;

+(RKObjectMapping *)objectMapping;

@end
