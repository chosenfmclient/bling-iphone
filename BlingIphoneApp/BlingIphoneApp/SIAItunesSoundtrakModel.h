//
//  SIAItunesSoundtrakModel.h
//  ChosenIphoneApp
//
//  Created by Joseph Nahmias on 11/04/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SIAItunesMusicElement.h"

@interface SIAItunesSoundtrakModel : NSObject

@property (strong, nonatomic) NSArray<SIAItunesMusicElement *> *tracksArray;

- (void)searchTerm:(NSString *)term withCompletionHandler:(void(^)(BOOL success))handler;
+ (void)getFirstTracksWithCompletionHandler:(void(^)(SIAItunesSoundtrakModel *model))handler;

+(RKObjectMapping *)objectMapping;

@end
