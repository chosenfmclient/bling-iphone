//
//  PageRequestHandler.swift
//  BlingIphoneApp
//
//  Created by Zach on 12/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation

protocol PageRequestHandlerDelegate: class {
    func dataWasLoaded(page: Int, data: Any?, requestIdentifier: Any?)
    func endOfContentReached(at page: Int, requestIdentifier: Any?)
}

@objc class PageRequestHandler: NSObject {
    var apiPath: String
    var queryParameters = [String: String]()
    var page: Int = 0
    var isMakingRequest = false
    var didReachEndOfContent = false
    weak var delegate: PageRequestHandlerDelegate?
    var requestIdentifier: Any?
    
    init(apiPath: String,
         delegate: PageRequestHandlerDelegate,
         requestIdentifier: Any? = nil,
         queryParameters: [String: String]? = [String: String]()) {
        self.apiPath = apiPath
        self.delegate = delegate
        self.requestIdentifier = requestIdentifier
        if let _ = queryParameters {
            self.queryParameters = queryParameters!
        }
    }
    
    func fetchData() {
        guard !isMakingRequest else {
            print("WARNING: You are calling for more data before the current request is finished: \(apiPath)")
            return
        }
        guard !didReachEndOfContent else {
            return
        }
        isMakingRequest = true
        
        var query = queryParameters
        query["page"] = String(describing: page)
        
        RequestManager.getRequest(
            apiPath,
            queryParameters: query,
            success: {[weak self] (request, result) in
                guard let sSelf = self else { return }
                if (request?.httpRequestOperation?.response?.statusCode == 204 ||
                    result?.firstObject == nil) {
                    sSelf.delegate?.endOfContentReached(at: sSelf.page, requestIdentifier: sSelf.requestIdentifier)
                    sSelf.didReachEndOfContent = true
                } else {
                    sSelf.delegate?.dataWasLoaded(page: sSelf.page, data: result?.firstObject, requestIdentifier: sSelf.requestIdentifier)
                }
                sSelf.page += 1
                sSelf.isMakingRequest = false
            },
            failure: {[weak self] (request, error) in
                guard let sSelf = self else { return }
                sSelf.delegate?.endOfContentReached(at: sSelf.page, requestIdentifier: sSelf.requestIdentifier)
                sSelf.didReachEndOfContent = true
                sSelf.page += 1
                sSelf.isMakingRequest = false
            }
        )
    }
}
