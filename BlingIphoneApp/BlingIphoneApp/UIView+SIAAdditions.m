//
//  UIView+UIView_SIAAdditions.m
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/3/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "UIView+SIAAdditions.h"

@implementation UIView (SIAAdditions)

- (void)sia_setSubviewsExclusiveTouch
{
    if ([self isKindOfClass:[UIControl class]]){
        self.exclusiveTouch = YES;
    }
    
    if (self.subviews.count <= 0) {
        return;
    }
    
    for (UIView *sub in self.subviews) {
        [sub sia_setSubviewsExclusiveTouch];
    }
}
/// Rounds corners with default radius of half the height, for fully round edges
- (void)sia_roundCorners:(UIRectCorner)corners {
    [self sia_roundCorners:corners
                 withRadii:CGSizeMake(self.bounds.size.height / 2, self.bounds.size.height / 2)];
}

- (void)sia_roundCorners:(UIRectCorner)corners withRadii:(CGSize)radii{
    UIBezierPath *roundedPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                      byRoundingCorners:corners
                                                            cornerRadii:radii];
    CAShapeLayer *roundedLayer = [CAShapeLayer layer];
    roundedLayer.path = roundedPath.CGPath;
    self.layer.mask = roundedLayer;
    self.layer.masksToBounds = YES;
}

- (void)sia_setBackgroundColorAnimated:(UIColor *)color {
    [UIView animateWithDuration:0.1
                     animations:^{
                         self.backgroundColor = color;
                     }];
}

- (id) getParentViewController {
    id nextResponder = [self nextResponder];
    if ([nextResponder isKindOfClass:[UIViewController class]]) {
        return nextResponder;
    } else if ([nextResponder isKindOfClass:[UIView class]]) {
        return [nextResponder getParentViewController];
    } else {
        return nil;
    }
}

@end
