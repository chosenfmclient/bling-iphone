//
//  SIAObjectManager.m
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 12/15/15.
//  Copyright © 2015 SingrFM. All rights reserved.
//

#import "SIAObjectManager.h"
#import "SIAItunesSoundtrakModel.h"
#import "SIAEmailRegistrationdataModel.h"
#import "SIAPaginatorDataModel.h"
#import "SIAVideosDataModel.h"
#import "SIANotificationsDataModel.h"
#import "SIAFollowingDataModel.h"
#import "SIAFollowersDataModel.h"
#import "SIAApnSettingsDataModel.h"

#pragma mark - Object Request Operation
@interface SIAObjectRequestOperation : RKObjectRequestOperation
@end

@implementation SIAObjectRequestOperation
- (instancetype)initWithHTTPRequestOperation:(RKHTTPRequestOperation *)requestOperation responseDescriptors:(NSArray *)responseDescriptors
{
    NSMutableURLRequest *request = [requestOperation.request mutableCopy];
    request.cachePolicy = [SIAObjectManager cachePolicyForRequest:request responseDescriptors:responseDescriptors];

    RKHTTPRequestOperation *newRequestOperation = [[[requestOperation class] alloc] initWithRequest:request];

    self = [super initWithHTTPRequestOperation:newRequestOperation responseDescriptors:responseDescriptors];
    if(self)
    {
    }

    return self;
}

+ (BOOL)canProcessRequest:(NSURLRequest *)request
{ 
    return YES;
}
@end

#pragma mark - Object manager
@implementation SIAObjectManager

+ (instancetype) sharedManager {
    static SIAObjectManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        AFRKHTTPClient *client = [[AFRKHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:SERVER_BASE_URL]];
        client.parameterEncoding = AFRKJSONParameterEncoding;
        //set user agent
        [client setDefaultHeader:@"User-Agent"
                           value:[SIAObjectManager getUserAgent]];
        
        manager = [[SIAObjectManager alloc] initWithHTTPClient:client];
        [manager attatchResponseDescriptors];
        [manager attatchRequestDescriptors];
        manager.requestSerializationMIMEType = RKMIMETypeJSON;
//        LOG TIME
//        RKLogConfigureByName("RestKit", RKLogLevelTrace);
//        RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
//        RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    });
    return manager;
}
- (instancetype)initWithHTTPClient:(AFRKHTTPClient *)client
{
    self = [super initWithHTTPClient:client];
    if(self)
    {
        [self registerRequestOperationClass:[SIAObjectRequestOperation class]];
    }

    return self;
}
+ (NSString*)getUserAgent {
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *appName = @"Blingy_iOS";
    NSString *osVersion = [[UIDevice currentDevice] systemVersion];
    NSString *deviceModel = [[UIDevice currentDevice] model];
    float secondsFromGMT = (float)[[NSTimeZone localTimeZone] secondsFromGMT];
    NSNumber *hoursFromGMT = @( secondsFromGMT /  (60. * 60.));
    NSMutableString *timeZone = @"".mutableCopy;
    int absHours = abs(hoursFromGMT.intValue);
    int absMinutes = 60 * fabsf((hoursFromGMT.floatValue - (hoursFromGMT.intValue)));
    [timeZone appendString:(hoursFromGMT.intValue < 0 ? @"-" : @"+") ];
    [timeZone appendString:[NSString stringWithFormat:@"%02d", absHours]];
    [timeZone appendString:[NSString stringWithFormat:@"%02d", absMinutes]];
    
    return [NSString stringWithFormat:@"%@/%@; %@; %@; %@",
            appName, version, osVersion, deviceModel, timeZone];
}

+ (NSURLRequestCachePolicy)cachePolicyForRequest:(NSURLRequest *)request responseDescriptors:(NSArray *)responseDescriptors
{
    Class objectClass = [self _objectClassForMethod:RKRequestMethodFromString(request.HTTPMethod) path:request.URL.path responseDescriptors:responseDescriptors];
    return objectClass ? [self _cachePolicyForObjectClass:objectClass] : NSURLRequestUseProtocolCachePolicy;
}

+ (Class)_objectClassForMethod:(RKRequestMethod)method path:(NSString *)path responseDescriptors:(NSArray *)responseDescriptors
{
    Class objectClass = nil;
    NSString *actualPath = path;

    if([actualPath characterAtIndex:0] == '/')
        actualPath = [path substringFromIndex:1];

    NSIndexSet *indexSet = [responseDescriptors indexesOfObjectsPassingTest:^BOOL(RKResponseDescriptor *responseDescriptor, NSUInteger idx, BOOL *stop) {
        return [responseDescriptor matchesPath:actualPath] && (method & responseDescriptor.method);
    }];

    NSArray *descriptorArray = [responseDescriptors objectsAtIndexes:indexSet];
    if([descriptorArray count] < 1)
        return nil;

    RKResponseDescriptor *currDescriptor = ((RKResponseDescriptor *)descriptorArray[0]);
    RKObjectMapping *mapping = (RKObjectMapping *)currDescriptor.mapping;
    
    if ([mapping respondsToSelector:@selector(objectClass)]) {
        objectClass = mapping.objectClass;
    }

    return objectClass;
}

+ (NSURLRequestCachePolicy)_cachePolicyForObjectClass:(Class)objectClass
{
    NSURLRequestCachePolicy cachePolicy = NSURLRequestUseProtocolCachePolicy;

    if([objectClass conformsToProtocol:@protocol(SIACachableDataModel)])
    {
        if([objectClass respondsToSelector:@selector(cachePolicy)])
        {
            cachePolicy = [((Class<SIACachableDataModel>)objectClass)cachePolicy];
        }
    }

    return cachePolicy;
}

/**
 *  Hello friend... it's nice to see you again
 *  This is where ALL rest kit descriptors should be registered (when the object manager is created)
 *  ":id" is read by Restkit as a placeholder. Multplie placeholders can be used
 *  E.G. api/v1/:type/:id/share - This matches all share APIs (api/v1/users/441/share, api/v1/videos/2991/share...)
 */
#pragma mark - Response Descriptors
- (void) attatchResponseDescriptors {
    // GET descriptors
    [self addResponseDescriptor:[self getVideoResponseDescriptor]];
    [self addResponseDescriptor:[self getUserResponseDescriptor]];
    [self addResponseDescriptor:[self getCommentsResponseDescriptor]];
    [self addResponseDescriptor:[self getLikesResponseDescriptor]];
    [self addResponseDescriptor:[self getSoundEffectsResponseDescriptor]];
    [self addResponseDescriptor:[self getSoundTracksResponseDescriptor]];
    [self addResponseDescriptor:[self getNativeVideoResponseDescriptor]];
    [self addResponseDescriptor:[self getNotificationsResponseDescriptor]];
    [self addResponseDescriptor:[self getDiscoverResponseDescriptor]];
    [self addResponseDescriptor:[self getSongHomeResponseDescriptor]];
    [self addResponseDescriptor:[self getSearchResponseDescriptor]];
    [self addResponseDescriptor:[self getSearchRecordDescriptor]];
    [self addResponseDescriptor:[self getHomeResponseDescriptor]];
    [self addResponseDescriptor:[self getProfileResponseDescriptor]];
    [self addResponseDescriptor:[self getFollowingResponseDescriptor]];
    [self addResponseDescriptor:[self getFollowersResponseDescriptor]];
    [self addResponseDescriptor:[self getDeeplinkResponseDescriptor]];
    [self addResponseDescriptor:[self getNotificationBadgeResponseDescriptor]];
    [self addResponseDescriptor:[self getLikedByMeResponseDescriptor]];
    [self addResponseDescriptor:[self getFollowedByMeResponseDescriptor]];
    [self addResponseDescriptor:[self getBlingTimeSegmentsResponseDescriptor]];
    [self addResponseDescriptor:[self getLeaderboardResponseDescriptor]];
    
    // POST descriptors
    [self addResponseDescriptor:[self postVideosResponseDescriptor]];
    [self addResponseDescriptor:[self postVideosWithIdResponseDescriptor]];
    [self addResponseDescriptor:[self postShareResponseDescriptor]];
    [self addResponseDescriptor:[self postTokenResponseDescriptor]];
    [self addResponseDescriptor:[self postVideoTagResponseDescriptor]];
    [self addResponseDescriptor:[self postEmailResponseDescriptor]];
    [self addResponseDescriptor:[self postSignupResponseDescriptor]];
    [self addResponseDescriptor:[self postFindFriendsResponseDescriptor]];
    [self addResponseDescriptor:[self postCommentResponseDescriptor]];
    [self addResponseDescriptor:[self postProfilePhotoResponseDescriptor]];
    [self addResponseDescriptor:[self postNativeVideoResponseDescriptor]];
    [self addResponseDescriptor:[self postFollowManyResponseDescriptor]];
    
    // PUT descriptors
    [self addResponseDescriptor:[self putVideoResponseDescriptor]];
    [self addResponseDescriptor:[self putAPNSSettingsResponseDescriptor]];
    [self addResponseDescriptor:[self putUserResponseDescriptor]];
    [self addResponseDescriptor:[self putPhotoResponseDescriptor]];
    
}

#pragma mark - GET Response Descriptors
- (RKResponseDescriptor*) getNativeVideoResponseDescriptor {
    RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[SIAShare class]];
    [responseMapping addAttributeMappingsFromArray:@[@"native_video"]];

    return [RKResponseDescriptor
            responseDescriptorWithMapping:responseMapping
            method:RKRequestMethodGET
            pathPattern:@"api/v1/native-video"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getSoundTracksResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAItunesSoundtrakModel objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/soundtracks"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getSoundEffectsResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIASoundEffectModel objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/sound-effects"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}



- (RKResponseDescriptor*) getUserResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAUser objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/users/:id"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getVideoResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAVideo objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/videos/:id"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getVideosListResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAVideosDataModel pageMapping]
            method:RKRequestMethodGET
            pathPattern:[SIAVideosDataModel pathPattern]
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getCommentsResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIATextCommentModel pageMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/videos/:id/comments"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getLikesResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAVideoLikesDataModel pageMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/videos/:id/likes"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getNotificationsResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIANotificationsDataModel pageMapping]
            method:RKRequestMethodGET
            pathPattern:[SIANotificationsDataModel pathPattern]
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getDiscoverResponseDescriptor {
    return [RKResponseDescriptor responseDescriptorWithMapping:[DiscoverResponseModel objectMapping]
                                                        method:RKRequestMethodGET
                                                   pathPattern:@"api/v1/discover"
                                                       keyPath:nil
                                                   statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getSongHomeResponseDescriptor {
    return [RKResponseDescriptor responseDescriptorWithMapping:[DiscoverResponseModel objectMapping]
                                                        method:RKRequestMethodGET
                                                   pathPattern:@"api/v1/song-home/:id"
                                                       keyPath:nil
                                                   statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getHomeResponseDescriptor {
    return [RKResponseDescriptor responseDescriptorWithMapping:[HomeResponseModel objectMapping]
                                                        method:RKRequestMethodGET
                                                   pathPattern:@"api/v1/home"
                                                       keyPath:nil
                                                   statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getSearchResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SearchResult responseMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/search"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getSearchRecordDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SearchResult responseMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/record"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getProfileResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[ProfileResponseModel objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/users/:id/profile"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}


- (RKResponseDescriptor*) getFollowingResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[Users objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/users/:id/following"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getFollowersResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[Users objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/users/:id/followers"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getDeeplinkResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[DeepLinkResponseModel objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/users/:id/deeplink"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getNotificationBadgeResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[NotificationBadgeModel objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/users/:id/notifications"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getLikedByMeResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[LikedByMeList objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/users/:id/likes"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getFollowedByMeResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[FollowingList objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/following"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getBlingTimeSegmentsResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[MusicVideo blingTimesMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/time-segments"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) getLeaderboardResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[LeaderboardDataModel objectMapping]
            method:RKRequestMethodGET
            pathPattern:@"api/v1/leaderboard"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

#pragma mark - POST Response Descriptors
- (RKResponseDescriptor*) postTokenResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAEmailRegistrationdataModel objectMapping]
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/token"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) postEmailResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAEmailRegistrationdataModel objectMapping]
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/email"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) postSignupResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAEmailRegistrationdataModel objectMapping]
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/signup"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) postShareResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAShareMapping objectMapping]
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/videos/:id/share"
            keyPath:@"data"
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) postVideosResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAVideoUploadDataModel objectMapping]
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/videos"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) postVideosWithIdResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAVideo objectMapping]
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/videos/:id"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) postVideoTagResponseDescriptor {
    RKObjectMapping *objectMapping = [RKObjectMapping requestMapping];
    [objectMapping addAttributeMappingsFromDictionary:@{@"tags":@"tags"}];
    return [RKResponseDescriptor
            responseDescriptorWithMapping:objectMapping
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/videos/:id/tags"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) postFindFriendsResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[FindFriends objectMapping]
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/find-friends"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}



- (RKResponseDescriptor*) postCommentResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIATextComment objectMapping]
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/comments"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor *) postNativeVideoResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[RKObjectMapping mappingForClass:[SIANativeVideo class]]
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/native-video"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) postProfilePhotoResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAUploadUserImage objectMapping]
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/users/:id/photo"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}


- (RKResponseDescriptor*) postFollowManyResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[RKObjectMapping requestMapping]
            method:RKRequestMethodPOST
            pathPattern:@"api/v1/follow"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor*) putVideoResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAVideo objectMapping]
            method:RKRequestMethodPUT
            pathPattern:@"api/v1/videos/:id"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor *) putAPNSSettingsResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[SIAApnSettingsDataModel responseMapping]
            method:RKRequestMethodPUT
            pathPattern:@"api/v1/apns-settings"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor *) putUserResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[RKObjectMapping requestMapping]
            method:RKRequestMethodPUT
            pathPattern:@"api/v1/users/:id"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

- (RKResponseDescriptor *) putPhotoResponseDescriptor {
    return [RKResponseDescriptor
            responseDescriptorWithMapping:[RKObjectMapping requestMapping]
            method:RKRequestMethodPUT
            pathPattern:@"api/v1/users/:id/photo"
            keyPath:nil
            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
}

#pragma mark - Request Descriptors
- (void) attatchRequestDescriptors {
    [self addRequestDescriptor:[self postTokenRequestDescriptor]];
    [self addRequestDescriptor:[self postLikeRequestDescriptor]];
    [self addRequestDescriptor:[self postShareRequestDescriptor]];
    [self addRequestDescriptor:[self postVideoRequestDescriptor]];
    [self addRequestDescriptor:[self postTagsRequestDescriptor]];
    [self addRequestDescriptor:[self postFindFriendsEmailsRequestDescriptor]];
    [self addRequestDescriptor:[self postInviteRequestDescriptor]];
    [self addRequestDescriptor:[self postFollowAllRequestDescriptor]];
    [self addRequestDescriptor:[self postCommentRequestDescriptor]];
    [self addRequestDescriptor:[self postUserPhotoRequestDescriptor]];
    [self addRequestDescriptor:[self postSendCameoInvitesRequestDescriptor]];
    [self addRequestDescriptor:[self postPushMetricsRequestDescriptor]];
    
    [self addRequestDescriptor:[self putNotificationStatusRequestDescriptor]];
    [self addRequestDescriptor:[self putVideoFlagRequestDescriptor]];
    [self addRequestDescriptor:[self putShareRequestDescriptor]];
    [self addRequestDescriptor:[self putCommentRequestDescriptor]];
    [self addRequestDescriptor:[self putCommentFlagRequestDescriptor]];
    [self addRequestDescriptor:[self putVideoRequestDescriptor]];
    [self addRequestDescriptor:[self putAPNSSettingsRequestDescriptor]];
    [self addRequestDescriptor:[self putUserRequestDescriptor]];
    [self addRequestDescriptor:[self putVideoStatusRequestDescriptor]];
}

#pragma mark - POST Request Descriptors
- (RKRequestDescriptor*) postFindFriendsEmailsRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[ContactList objectMapping]
            objectClass:[ContactList class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

- (RKRequestDescriptor*) postInviteRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[Invitations objectMapping]
            objectClass:[Invitations class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

- (RKRequestDescriptor*) postTokenRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[SIAEmailRegistrationdataModel objectMapping]
            objectClass:[SIAEmailRegistrationdataModel class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

- (RKRequestDescriptor*) postLikeRequestDescriptor {
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromArray:@[@"type"]];
    return [RKRequestDescriptor
            requestDescriptorWithMapping:requestMapping
            objectClass:[SIALike class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

- (RKRequestDescriptor*) postShareRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[SIAShareMapping shareObjectMapping]
            objectClass:[SIAShareMapping class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

- (RKRequestDescriptor*) postVideoRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[SIAUploadVideo startUploadObjectMapping]
            objectClass:[SIAUploadVideo class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

- (RKRequestDescriptor*) postTagsRequestDescriptor {
    RKObjectMapping *objectMapping = [RKObjectMapping requestMapping];
    [objectMapping addAttributeMappingsFromDictionary:@{@"tags":@"tags"}];

    return [RKRequestDescriptor
            requestDescriptorWithMapping:objectMapping
            objectClass:[Tags class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

- (RKRequestDescriptor*) postFollowAllRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[UserIDList objectMapping]
            objectClass:[UserIDList class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

- (RKRequestDescriptor*) postCommentRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[CommentRequestModel objectMapping]
            objectClass:[CommentRequestModel class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

- (RKRequestDescriptor*) postUserPhotoRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[UserPhotoRequestModel objectMapping]
            objectClass:[UserPhotoRequestModel class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

- (RKRequestDescriptor*) postSendCameoInvitesRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[CameoSendInvitesModel inviteRequestMapping]
            objectClass:[CameoSendInvitesModel class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

- (RKRequestDescriptor *) postPushMetricsRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[PushNotificationModel requestMapping]
            objectClass:[PushNotificationModel class]
            rootKeyPath:nil
            method:RKRequestMethodPOST];
}

#pragma mark - PUT Request Descriptors
- (RKRequestDescriptor*) putNotificationStatusRequestDescriptor {
    RKObjectMapping *statusMapping = [RKObjectMapping requestMapping];
    [statusMapping addAttributeMappingsFromDictionary:@{@"status":@"status"}];
    return [RKRequestDescriptor
            requestDescriptorWithMapping:statusMapping
            objectClass:[SIANotificationsDataModel class]
            rootKeyPath:nil
            method:RKRequestMethodPUT];
}

- (RKRequestDescriptor*) putVideoFlagRequestDescriptor {
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromArray:@[@"type"]];
    return [RKRequestDescriptor
            requestDescriptorWithMapping:requestMapping
            objectClass:[SIAFlagAlertController class]
            rootKeyPath:nil
            method:RKRequestMethodPUT];
}

- (RKRequestDescriptor*) putShareRequestDescriptor {
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromArray:@[@"platform", @"share_id"]];
    [requestMapping addAttributeMappingsFromDictionary:@{@"shareTags":@"tag"}];
    return [RKRequestDescriptor
            requestDescriptorWithMapping:requestMapping
            objectClass:[SIAShare class]
            rootKeyPath:nil
            method:RKRequestMethodPUT];
}

- (RKRequestDescriptor*) putCommentRequestDescriptor {
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{kTextInstruction : kTextInstruction}];
    return [RKRequestDescriptor
            requestDescriptorWithMapping:requestMapping
            objectClass:[SIATextComment class]
            rootKeyPath:nil
            method:RKRequestMethodPUT];
}

- (RKRequestDescriptor*) putCommentFlagRequestDescriptor {
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{kTypeKeyStr : kTypeKeyStr}];
    return [RKRequestDescriptor
            requestDescriptorWithMapping:requestMapping
            objectClass:[Flag class]
            rootKeyPath:nil
            method:RKRequestMethodPUT];
}

- (RKRequestDescriptor*) putVideoRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[SIAVideo requestMapping]
            objectClass:[SIAVideo class]
            rootKeyPath:nil
            method:RKRequestMethodPUT];
    
}

- (RKRequestDescriptor *) putAPNSSettingsRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[SIAApnSettingsDataModel requestMapping]
            objectClass:[SIAApnSettingsDataModel class]
            rootKeyPath:nil
            method:RKRequestMethodPUT];
}

- (RKRequestDescriptor *) putUserRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[UserRequestModel objectMapping]
            objectClass:[UserRequestModel class]
            rootKeyPath:nil
            method:RKRequestMethodPUT];
}

- (RKRequestDescriptor *) putVideoStatusRequestDescriptor {
    return [RKRequestDescriptor
            requestDescriptorWithMapping:[VideoStatus objectMapping]
            objectClass:[VideoStatus class]
            rootKeyPath:nil
            method:RKRequestMethodPUT];
}
@end
