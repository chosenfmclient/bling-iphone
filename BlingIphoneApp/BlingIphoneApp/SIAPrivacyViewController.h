//
//  SIAPrivacyViewController.h
//  ChosenIphoneApp
//
//  abstract: show the user privacy details
//
//  Created by Roni Shoham on 19/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIAPrivacyViewController : SIABaseViewController<UIPrintInteractionControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextView *privacyText;
@end
