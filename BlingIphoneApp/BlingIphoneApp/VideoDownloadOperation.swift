//
//  VideoDownloadOperation.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 3/21/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

import UIKit
import RestKit

@objc class VideoDownloadOperation: AFRKHTTPRequestOperation {
    
    private var filePath: String?
    private var downloadedVideoURL: URL? {
        if let _ = filePath {
            return URL(fileURLWithPath: filePath!)
        }
        return nil
    }

    init(url: URL!) {
        var request = URLRequest(url: url)
        request.timeoutInterval = 15.0 // 15 seconds timeout yo
        //operation = AFRKHTTPRequestOperation(request: request)
        super.init(request: request)
        let hash = url.absoluteString.md5()
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        filePath = paths.first!.appendingFormat("/blingDownloadedVideo\(hash).\(url.pathExtension)")
        outputStream = OutputStream(toFileAtPath: filePath!, append: false)
    }
    
    func download(progressBlock: @escaping (_ percent: Float) -> (),
                  completion: @escaping (_ url: URL?, _ error: Error?) -> (Void)) {
        
        guard SIANetworking.checkConnection() else {
            (UIApplication.shared.delegate as! AppDelegate).showNetworkDialog(for: 0)
            completion(nil, (NSError() as Error))
            return
        }
        
        setDownloadProgressBlock({ (totalBytesSinceLastUpdate, totalBytesRead, totalBytesExpected) in
            progressBlock(Float(totalBytesRead) / Float(totalBytesExpected) * 100)
        })
        
        setCompletionBlockWithSuccess({[weak self] (operation, response)  in
            guard let _ = self, !self!.isCancelled else {
                return
            }
            Utils.dispatchAsync {
                completion(self!.downloadedVideoURL, nil)
            }
            
            }, failure: { (operation, error) in
                // fail'd?
                completion(nil, error)
        })
        start()
    }
    
    func deleteDownloadedVideo() {
        guard let _ = downloadedVideoURL else {
            return
        }
        try? FileManager.default.removeItem(at: downloadedVideoURL!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
