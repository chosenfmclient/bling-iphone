//
//  SIANotificationsViewController.m
//  SingrIphoneApp
//
//  Created by SingrFM on 10/6/13.
//  Copyright (c) 2013 SingrFM. All rights reserved.
//

#import "SIANotificationsViewController.h"
#import "SIANotificationsDataModel.h"
//#import "SIAProfileNewViewCell.h"
#import "SIAUser.h"
#import "SIAVideo.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define NOTIFICATIONS_PATH                 @"api/v1/notifications"
#define MARK_ALL_AS_READ_PATH              @"api/v1/users/me/notifications?access_token=%@"
#define USER_PLACEHOLDER_IMAGE             @"extra_large_userpic_placeholder.png"
#define VIDEO_PLACEHOLDER_IMAGE            @"placeholder_video.png"
#define ACCESS_TOKEN                       @"access_token"
#define NOTIFICATION_CELL_ID               @"notificationCell"
#define VIDEO_CELL_ID                      @"performanceCell"
#define LOADING_CELL_ID                    @"loadingCell"
#define NOTIFIER_IMAGE_KEY_STR             @"image"
#define NOTIFIER_NAME_KEY_STR              @"notifiername"
#define NOTIFICATION_FIRST_LINE_KET_STR    @"message1"
#define NOTIFICATION_SECOND_LINE_KET_STR   @"message2"
#define NOTIFICATION_ACTION_BUTTON_KEY_STR @"actionbutton"
#define NOTIFICATION_NEW_KEY_STR           @"new"
#define NOTIFICATION_DATE                  @"date"
#define LOADING_TEXT                       @"Loading your notifications"
#define USER                               kChosenUserKeyStr
#define BADGE                              kChosenBadgeKeyStr
#define VIDEO                              kChosenVideoKeyStr
#define GAME                               @"game"
#define TYPE                               @"type"
#define DATA                               @"data"
#define DATE                               @"date"
#define NOTIFICATION_ID                    @"notification_id"
#define TITLE                              @"title"
#define TEXT                               @"text"
#define IMAGE                              @"image"
#define STATUS                             @"status"
#define OBJECT                             @"object"
#define OBJECT_USER                        @"object.user"
#define OPENSANS_FONT                      @"openSans"
#define OPENSANS_ITALIC_FONT               @"openSans-Italic"
#define NUMBER_OF_SECTION_IN_NOTIFICATION_TABLEVIEW 1
#define NUM_OF_NOTIFICATIONS 25



@interface SIANotificationsViewController ()

@property (nonatomic, strong) SIANotificationsDataModel *model;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) NSArray *notificationsArray;
@property (nonatomic) BOOL loadingFlag;
@property (strong, nonatomic) NSMutableArray *imagesArray;
@property (nonatomic) int numOfImages;
@property (strong, nonatomic) NSTimer *restkitTimer;
@property (strong, nonatomic) UIImage *talentScoutImage;
@property (nonatomic) BOOL restkitTimerFlag;

@end

@implementation SIANotificationsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.paginator = [[SIAPaginator alloc] initForModelClass:[SIANotificationsDataModel class] delegate:self];
    [self.paginator fetchFirstPage];
    [self startLoadingAnimation];
    [self.navigationController hideNotificationBadge];
    [AmplitudeEvents logWithEvent:@"notification:pageview"];
}

-(void)startLoadingAnimation{
    if (!_loadingFlag) {
        _loadingFlag = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tableTopConstraint.constant += 40;
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.1 animations:^{
                    [self.view layoutIfNeeded];
                    self.loadingSpinningImageView.alpha = 1;
                }];
            });
            [self runSpinAnimationOnView:self.loadingSpinningImageView duration:0.5 rotations:1 repeat:HUGE_VALF];
        });
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.paginator.results.count;
    
}

-(void)finishLoadingWithAnimation{
    if (_loadingFlag) {
        _loadingFlag = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
            self.tableTopConstraint.constant = 0;
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.5 animations:^{
                    [self.view layoutIfNeeded];
                    self.loadingSpinningImageView.alpha = 0;
                }];
            });
        });
    }
}

- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void) showData  {
    
    if (_model == nil){
        _model = [[SIANotificationsDataModel alloc] init];
    }
    [self startLoadingAnimation];
    
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
   // });
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    if (((SIAAppDelegate*)[[UIApplication sharedApplication] delegate]).newNotification && _model != nil) {
//        [self showData];
//    }
    [self.navigationController setNavigationItemTitle:@"Notifications"];
    [self.navigationController setBottomBarHidden:NO animated:NO];
    [self.navigationController clearNavigationItemLeftButton];
    [self.navigationController clearNavigationItemRightButton];
    self.tableView.userInteractionEnabled = YES;
//    [self.navigationController setSelected:YES forBottomButton:BottomBarButtonTypeNotificationsButton];

//    SIAAppDelegate *appDelegate = sia_AppDelegate;
//    appDelegate.displayNotificationPopup = YES;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self.navigationController setBottomBarHidden:false animated: true];
    [self.navigationController setSelected:true forBottomButton: BottomBarButtonTypeNotificationsButton];

    [[SIAPushRegistrationHandler sharedHandler] notificationScreenDidAppear];
//    [Flurry logEvent:FLURRY_VIEW_NOTIFICATIONS];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [_restkitTimer invalidate];
    [super viewWillDisappear:animated];
    
}


- (void) markAllAsRead {
    __weak SIANotificationsViewController* wSelf = self;
    [RequestManager putRequest:@"api/v1/users/me/notifications"
                        object:nil
               queryParameters:nil
                       success:^(RKObjectRequestOperation *request, RKMappingResult *result) {
                           [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                       }
                       failure:nil
              showFailureDialog:YES];
}

#pragma mark - Table view data source

- (CGFloat) tableHeaderSize {
    return 0;
}

- (NSString *)cellIdentifierForRow:(NSInteger)row {
    
    if ([self.paginator.results[row] isKindOfClass:[[NSNull null] class]]) {
        return LOADING_CELL_ID;
    }
    
    SIANotificationsDataModel *notification = self.paginator.results[row];
    
    if ([notification.type isEqualToString:VIDEO] || [notification.image_type isEqualToString:VIDEO]) {
        return VIDEO_CELL_ID;
    }
    return NOTIFICATION_CELL_ID;
}

- (UIImage *) placeholderForRow:(NSUInteger)row {
    SIANotificationsDataModel *notification = self.paginator.results[row];
    if ([notification.type isEqualToString:VIDEO]) {
        return [UIImage imageNamed:VIDEO_PLACEHOLDER_IMAGE];
    }
    
    else if ([notification.type isEqualToString:USER]) {
        return [UIImage imageNamed:USER_PLACEHOLDER_IMAGE];
    }
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(36, 36), NO, 0.0);
    UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return blank;
}

- (CGFloat)getTotalHeightOfLabels: (NSArray<UILabel*>*) labels {
    CGFloat height = 0.0;
    CGSize size = CGSizeMake([UIScreen mainScreen].bounds.size.width - 53. - 20., 53.);
    for (UILabel* label in labels) {
        if (label.attributedText) {
            height += [label.attributedText boundingRectWithSize:size
                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                         context:nil].size.height;
        } else {
            height += [label.text boundingRectWithSize:size
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName: label.font}
                                               context:nil].size.height;
        }
    }
    return height + 2.0;
}

/** \brief update tableview cells
 *
 * \param notifications tableview
 * \param indexpath
 *
 * \return tableview cell
 *
 * ...
 *
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SIANotificationsDataModel *notification = self.paginator.results[indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[self cellIdentifierForRow:indexPath.row]
                                                            forIndexPath:indexPath];
    if ([cell isKindOfClass:[SIALoadingTableViewCellBlack class]]) {
        return cell;
    }
    
    UIImageView* imageView;
    
    // user notification
    if (!([notification.type isEqualToString:VIDEO] ||
          [notification.image_type isEqualToString:VIDEO])) {
        UserNotificationTableViewCell *notificationCell = (UserNotificationTableViewCell *)cell;
        NSString *time = [[Utils getElapsedTimeStringFor:notification.date] stringByAppendingString:@" ago"];
        [notificationCell.timestampLabel setText:time];
        
        // text
        [notificationCell.notificationTextlabel setAttributedText:notification.textFromHTML];
        imageView = notificationCell.userImageView;
        
        notificationCell.labelViewHeightConstraint.constant = [self getTotalHeightOfLabels: @[notificationCell.notificationTextlabel, notificationCell.timestampLabel]];
    }
    // video notification
    else {
        VideoNotificationTableViewCell *videoCell = (VideoNotificationTableViewCell *)cell;
        //  time
        NSString *time = [[Utils getElapsedTimeStringFor:notification.date] stringByAppendingString:@" ago"];
        [videoCell.timestampLabel setText:time];
        videoCell.notificationTextlabel.lineBreakMode = NSLineBreakByWordWrapping;
        [videoCell.notificationTextlabel setAttributedText:notification.textFromHTML];
        
        imageView = videoCell.videoImageView;
        
        if ([notification.type containsString:@"cameo"]) {
            videoCell.contentView.backgroundColor = [[SIAStyling defaultStyle] cameoNotificationPink];
        }
        
        videoCell.labelViewHeightConstraint.constant = [self getTotalHeightOfLabels: @[videoCell.notificationTextlabel, videoCell.timestampLabel]];

    }
    
    [imageView sd_setImageWithURL:[self imageURLForRow:indexPath.row]
                 placeholderImage:[self placeholderForRow:indexPath.row]
                          options:SDWebImageRefreshCached
                        completed:nil];
    
    if (![notification.status isEqualToString:@"viewed"]) {
        cell.backgroundColor = [UIColor colorWithRed:242./255. green:242./255. blue:242./255. alpha:1.0];
    }
    else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

- (void) noData {
    self.noDataView.hidden = NO;
    self.tableView.hidden = YES;
}

- (void) hasData {
    self.noDataView.hidden = YES;
    self.tableView.hidden = NO;
}

-(void)refreshView{
    
    if (self.paginator.paginatorModel.currentPage.integerValue == 0) {
        [self htmlToString];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        if (self.paginator.results.count > 0)
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
        [self markAllAsRead];

        [self finishLoadingWithAnimation];
        
    }
    else {
        [self htmlToString];
        NSArray *indexPaths = [self.paginator indexPathForPage:self.paginator.paginatorModel.currentPage];
        NSIndexPath *lastIndex = indexPaths.lastObject;
        NSIndexPath *firstIndex = indexPaths.firstObject;
        if (lastIndex.row < self.paginator.results.count) {
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:@[firstIndex] withRowAnimation:UITableViewRowAnimationTop];
            [self.tableView insertRowsAtIndexPaths:indexPaths
                                  withRowAnimation:UITableViewRowAnimationTop];
            [self.tableView endUpdates];
        }
    }
}

/** \brief  calculates notification date to show in cell
 *
 * \param notification date
 *
 * \return date string
 *
 * ...
 *
 */
    

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (![self.paginator.results[indexPath.row] isKindOfClass:[SIANotificationsDataModel class]]) return;
    

    tableView.userInteractionEnabled = NO;
    
    SIANotificationsDataModel *notification = self.paginator.results[indexPath.row];
    NSMutableDictionary *ampProps = @{@"pos":@([indexPath row]),
                                      @"notification_type":notification.type,
                                      @"notification_id": notification.notification_id}.mutableCopy;
    
    NSString *objectId = [NSString string];
    
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    if (![notification.status isEqualToString:@"viewed"]) {
        notification.status = @"viewed";
        [notification setNotificationStatus];
    }

    if ([notification.type isEqualToString:USER]) {
        
        
        UIStoryboard *profileSB = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ProfileViewController *profileController = [profileSB instantiateViewControllerWithIdentifier:@"profileViewController"];
        [self.navigationController pushViewController:profileController animated:YES];
        objectId = notification.user.user_id;
        profileController.userId = notification.user.user_id;
        
    }
    
    else if ([notification.type isEqualToString:VIDEO]) {
        
        
        SIAVideo *video = notification.video;
        VideoHomeViewController *videoHome = [[UIStoryboard storyboardWithName:@"VideoHomeStoryboard" bundle:nil] instantiateInitialViewController];
        videoHome.video = video;
        [self.navigationController pushViewController:videoHome animated:YES];
        objectId = video.videoId;
    }
    
    else if ([notification.type isEqualToString:@"comment"]) {
        
//        [self.navigationController switchToCommentsPageForVideoId:notification.comment.videoId
//                                                 onTopOfVideoHome:YES];
        SIAVideo *video = notification.comment.video;

        NSMutableArray *VCs = [self.navigationController viewControllers].mutableCopy;
        VideoHomeViewController *videoHomeVC = [[UIStoryboard storyboardWithName:@"VideoHomeStoryboard" bundle:nil] instantiateInitialViewController];
        VideoHomeListsViewController *commentsVC = [[UIStoryboard storyboardWithName:@"VideoHomeStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"videoLists"];
        videoHomeVC.video = commentsVC.video = video;
        commentsVC.tab = commentsVC.kCommentTab;
        [VCs addObject:videoHomeVC];
        [VCs addObject:commentsVC];
        [self.navigationController setViewControllers:VCs
                        animated:YES];
        
    }
    
    else if ([notification.type isEqualToString:@"sound"])
    {
        if (notification.musicVideo) {
            DiscoverViewController* discoverVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"discoverViewController"];
            discoverVC.featuredClip = notification.musicVideo;
            [self.navigationController pushViewController:discoverVC
                                                 animated:YES];
        }
    }
    
    else if ([notification.type isEqualToString:@"welcome"]) {
        
//        [sia_AppDelegate.pushRegistrationHandler registerNotifications:nil];
        [notification deleteNotification];
        [self removeNoticationFromArray:notification];
    }
    
    else if ([notification.type isEqualToString:@"leaderboard"]) {
        [self.navigationController switchToLeaderboard];
    }
    
    else if ([notification.type isEqualToString:@"invite"]) {
        [self.navigationController switchToInvite];
    }
    
    else if ([notification.type containsString:@"cameo"]) {
        SIAVideo *video = notification.video;
        VideoHomeViewController *videoHome = [[UIStoryboard storyboardWithName:@"VideoHomeStoryboard" bundle:nil] instantiateInitialViewController];
        videoHome.video = video;
        videoHome.setForCameoInvite = [notification.type isEqualToString:@"cameo_invite"];
        [self.navigationController pushViewController:videoHome animated:YES];
        [ampProps addEntriesFromDictionary:@{@"object_id":video.videoId,
                                             @"clip_id" : video.clip_id ?: @"null",
                                             @"artist_name":video.artist_name,
                                             @"video_name":video.song_name}];
    }
    
    [AmplitudeEvents logWithEvent:@"notification:tap" with:ampProps];

    tableView.userInteractionEnabled = YES;
    
}

- (void)removeNoticationFromArray:(SIANotificationsDataModel *)notification {
    
    [self.paginator.results removeObject:notification];
    [self.tableView reloadData];
}

- (void) htmlToString {
    
    NSArray *pageArray = [self.paginator.paginatorModel.pageDict objectForKey:self.paginator.paginatorModel.currentPage.stringValue];
    for (SIANotificationsDataModel *notification in pageArray) {
        
        
        NSString *formattedStr = @"<span style='font-size:14px; font-family:Roboto'><style>b{font-family:'Roboto-Medium';font-size: 14px;}</style> ";
        NSString *str = notification.text;
        formattedStr = [formattedStr stringByAppendingString:str];
        formattedStr = [formattedStr stringByAppendingString:@"</span>"];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithData:[formattedStr dataUsingEncoding:NSUTF8StringEncoding]
                                                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                      NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                 documentAttributes:nil
                                                                              error:nil];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.maximumLineHeight = 15.0;
        [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attrString.length)];
        
        notification.textFromHTML = attrString;
    }
}

- (NSURL *)imageURLForRow:(NSInteger)row {
    SIANotificationsDataModel *notification = self.paginator.results[row];
    if (notification.imageURL) {
        return [NSURL URLWithString:notification.imageURL];
    }
    if ([notification.type isEqualToString:USER]){
        return notification.user.imageURL;
    }
    else if ([notification.type isEqualToString:VIDEO]) {
        return notification.video.thumbnailUrl;
    }
    else if ([notification.type isEqualToString:@"comment"]) {
        return notification.comment.user.imageURL;
    }
    else {
        return [NSURL URLWithString:notification.imageURL];
    }
    return nil;
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/
@end
