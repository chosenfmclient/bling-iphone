//
//  CameoInviteVieweController.swift
//  BlingIphoneApp
//
//  Created by Zach on 07/12/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
import SDWebImage

class CameoInviteViewController: SIABaseViewController, UITableViewDelegate, UITableViewDataSource, DiscoverSearchBarViewDelegate {

    //MARK: Outlets
    @IBOutlet weak var spinner: SIASpinner!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var searchBarView: DiscoverSearchBarView!
    
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var extraSelectedLabel: UILabel!
    @IBOutlet weak var invitedView: UIView!
    @IBOutlet weak var inviteButton: UIButton!
    
    @IBOutlet weak var noFriendsView: UIView!
    @IBOutlet weak var noFriendsButton: UIButton!
    
    //MARK: Properties
    var following = [SIAUser]()
    var selectedFollowing = [SIAUser]()
    var searchFilteredFollowing = [SIAUser]()
    
    var contacts = [DeviceContact]()
    var selectedContacts = [DeviceContact]()
    var searchFilteredContacts = [DeviceContact]()
    
    var inviteModel: CameoInviteModel?
    var selectedUsers = [CameoInvitee]()
    var inviteeViews = [UIView]()
    
    let contactsHandler = ContactsHandler()
    
    var sectionsLoaded = [Int]()
    
    var videoId: String?
    var shouldPopBackwards = false
    
    //MARK: View controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBarView.delegate = self
        Utils.dispatchInBackground {[weak self] in
            self?.fetchData()
        }
        inviteButton.bia_setEnabled(false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.setBottomBarHidden(true, animated: true)
        navigationController?.setNavigationItemTitle("Invite to Duet")
        navigationController?.setNavigationBarLeftButtonXWithAction(#selector(backEvent))
        selectedUsers = []
        selectedContacts = []
        selectedFollowing = []
        tableview.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if following.count < 1 && following.count < 1 {
            spinner?.startAnimation()
        }
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.setNavigationBarLeftButtonXWithAction(#selector(backEvent))
        navigationController?.setNavigationItemTitle("Invite to Duet")
        navigationController?.setBottomBarHidden(true, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        extraSelectedLabel.bia_rounded()
        inviteButton.bia_rounded()
        noFriendsButton.bia_rounded()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    func backEvent() {
        if shouldPopBackwards {
            navigationController.reversePopViewController()
        } else {
            navigationController.popViewController(animated: true)
        }
    }
    
    //MARK: Data loading
    
    func fetchData() {
        RequestManager.getRequest(
            "api/v1/users/me/following",
            queryParameters: nil,
            success: {[weak self] (req, res) in
                if req?.httpRequestOperation.response.statusCode == 200 {
                    self?.following = (res?.firstObject as! Users).data
                }
                self?.dataWasLoaded(forSection: 0)
            },
            failure: {[weak self](req, res) in
                self?.dataWasLoaded(forSection: 0)
            }
        )
        contactsHandler.shouldFilterExistingUsers = false
        contactsHandler.loadContacts {[weak self] (sms, email, haveBlingy) in
            self?.contacts = sms + email
            self?.dataWasLoaded(forSection: 1)
        }
    }
    
    func dataWasLoaded(forSection section: Int) {
        sectionsLoaded.append(section)
        guard sectionsLoaded.contains(0) && sectionsLoaded.contains(1) else { return }
        
        inviteModel = CameoInviteModel(contacts: contacts,
                                       followings: following,
                                       cameoId: videoId!)
        finishLoading()
    }
    
    func finishLoading() {
        spinner.stopAnimation()
        
        guard contacts.count > 0 || following.count > 0 else {
            noFriendsView.isHidden = false
            return
        }
        searchFilteredContacts = []
        searchFilteredContacts.append(contentsOf: contacts)
        
        searchFilteredFollowing = []
        searchFilteredFollowing.append(contentsOf: following)
        
        tableview.reloadData()
        tableview.isHidden = false
        invitedView.isHidden = false
    }
    
    //MARK: Table View stuff
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "invite", for: indexPath) as! CameoUserTableViewCell
        
        //name label
        cell.nameLabel.text = sectionIsFollowing(indexPath.section) ? searchFilteredFollowing[indexPath.row].fullName : searchFilteredContacts[indexPath.row].name
        
        //images
        if sectionIsFollowing(indexPath.section) {
            SDWebImageManager.shared().loadImage(with: searchFilteredFollowing[indexPath.row].imageURL, options: SDWebImageOptions(rawValue: 0), progress: nil, completed: {[weak self] (image, data, error, cachetype, complete, url) in
                cell.userImageView.image = image
                self?.searchFilteredFollowing[indexPath.row].image = image
            })
        } else {
            cell.userImageView.image = searchFilteredContacts[indexPath.row].image != nil ? searchFilteredContacts[indexPath.row].image : UIImage(named: "extra_large_userpic_placeholder.png")
        }
        
        //selected
        setCellSelected(cell: cell, forIndexPath: indexPath)
        
        //hide seperator on last cell
        if (sectionIsFollowing(indexPath.section) && indexPath.row == searchFilteredFollowing.count - 1) ||
            (!sectionIsFollowing(indexPath.section) && searchFilteredContacts.count - 1 == indexPath.row) {
            cell.seperatorView.isHidden = true
        } else {
            cell.seperatorView.isHidden = false
        }
        
        return cell
    }
    
    private func setCellSelected(cell: CameoUserTableViewCell, forIndexPath indexPath: IndexPath) {
        if (sectionIsFollowing(indexPath.section))
        { // follwoing
            cell.selectedForInvite = selectedFollowing.map({
                return $0.user_id ==  searchFilteredFollowing[indexPath.row].user_id
            }).reduce(false, {conatins, user in return user || conatins})
        }
        else
        { //conatcts
            cell.selectedForInvite = selectedContacts.map({
                return $0.cameoIdentity() == searchFilteredContacts[indexPath.row].cameoIdentity()
            }).reduce(false, {conatins, isUser in return isUser || conatins})
        }
        
        cell.selectedIcon.image = UIImage(named: cell.selectedForInvite ?
            "icon_group_invite_checked" : "icon_group_invite_unchecked")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionIsFollowing(section) ? searchFilteredFollowing.count : searchFilteredContacts.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let totalNumber = sectionIsFollowing(section) ? searchFilteredFollowing.count : searchFilteredContacts.count
        return totalNumber > 0 ? 45.0 : 0
    }
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as! CameoUserTableViewCell
//        
//        if sectionIsFollowing(indexPath.section) { //following
//            if cell.selectedForInvite {
//                selectedFollowing = selectedFollowing.filter({[weak self] (user) -> Bool in
//                    guard let _ = self else { return false }
//                    return user.user_id != self!.searchFilteredFollowing[indexPath.row].user_id
//                })
//                selectedUsers = selectedUsers.filter({ (user) -> Bool in
//                    return indexPath.section != user.fromSection ||
//                        user.md5Identity != searchFilteredFollowing[indexPath.row].user_id.md5()
//                })
//            } else {
//                selectedFollowing.append(searchFilteredFollowing[indexPath.row])
//                let inviteeUser = searchFilteredFollowing[indexPath.row]
//                selectedUsers.append(CameoInvitee(section: indexPath.section,
//                                                   id: inviteeUser.user_id.md5(),
//                                                   image: inviteeUser.image,
//                                                   name: inviteeUser.fullName,
//                                                   user_id: inviteeUser.user_id,
//                                                   email: inviteeUser.email
//                ))
//            }
//        } else { // contacts
//            if cell.selectedForInvite {
//                selectedContacts = selectedContacts.filter({[weak self] (contact) -> Bool in
//                    guard let _ = self else { return false }
//                    return contact.cameoIdentity() != self!.searchFilteredContacts[indexPath.row].cameoIdentity()
//                })
//                
//                selectedUsers = selectedUsers.filter({ (user) -> Bool in
//                    return indexPath.section != user.fromSection ||
//                        user.md5Identity != searchFilteredContacts[indexPath.row].cameoIdentity()
//                })
//            } else {
//                selectedContacts.append(searchFilteredContacts[indexPath.row])
//                let inviteeContact = searchFilteredContacts[indexPath.row]
//                selectedUsers.append(CameoInvitee(section: indexPath.section,
//                                                   id: inviteeContact.cameoIdentity(),
//                                                   image: inviteeContact.image,
//                                                   name: inviteeContact.name != nil ? inviteeContact.name! : "?",
//                                                   email: inviteeContact.email,
//                                                   phone: inviteeContact.phoneNumber
//                ))
//            }
//        }
//        
//        cell.selectedForInvite = !cell.selectedForInvite
//        
//        selectionWasUpdated()
//        tableView.reloadData()
//    }
//    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var total = sectionIsFollowing(section) ? "%d Following" : "%d Contacts"
        let totalNumber = sectionIsFollowing(section) ? following.count : contacts.count
        
        total = String(format: total, totalNumber)
        
        guard totalNumber > 0 else {
            return nil
        }
        
        let header = CameoTableViewHeader(frame: CGRect(origin: CGPoint(), size: CGSize(width: tableView.bounds.size.width, height: 45)))
       
        header.setText(total, andAction: { [weak self] in
            self?.selectAllWasTappedOn(section)
        })
        
        return header
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    private func sectionIsFollowing(_ section: Int) -> Bool {
        return section == 0
    }
    
    //MARK: Search
    func searchWasCleared() {
        searchFilteredFollowing = []
        searchFilteredFollowing.append(contentsOf: following)
        
        searchFilteredContacts = []
        searchFilteredContacts.append(contentsOf: contacts)
        
        tableview.reloadData()
    }
    
    func searchQuery(_ query: String) {
        guard !query.isEmpty else { return searchWasCleared() }
        
        searchFilteredContacts = contacts.filter({ (contact) -> Bool in
            guard let _ = contact.name else { return false }
            return contact.name!.lowercased().contains(query.lowercased())
        })
        
        searchFilteredFollowing = following.filter({ (user) -> Bool in
            return user.fullName.lowercased().contains(query.lowercased())
        })
        
        tableview.reloadData()
    }
    
    func searchFieldWasEntered() {
        //so what? dont care
    }
    
    func cancelWasTapped() {
        searchWasCleared()
    }
    
    //MARK: selection view
//    func selectionWasUpdated() {
//        for image in inviteeViews {
//            image.removeFromSuperview()
//        }
//        
//        let userWidth: CGFloat = 35.0 //images are 30x30 + 5 padding
//        let total = selectedUsers.count
//        
//        guard total > 0 else {
//            extraSelectedLabel.isHidden = true
//            return
//        }
//        
//        let availableWidth = selectedView.bounds.width
//        let totalVisibleCircles = Int(availableWidth / userWidth)
//        
//        var leadingImageOffset: CGFloat = 0
//        
//        if total > totalVisibleCircles
//        {
//            extraSelectedLabel.text = String(format: "+%d", total - totalVisibleCircles + 1)
//            extraSelectedLabel.isHidden = false
//            leadingImageOffset = userWidth
//        }
//        else
//        {
//            extraSelectedLabel.isHidden = true
//        }
//        
//        let circles = total > totalVisibleCircles ? totalVisibleCircles : total
//        leadingImageOffset = CGFloat((circles - 1)) * userWidth
//        for i in (0...selectedUsers.count - 1).reversed() {
//            if selectedUsers.count - i > totalVisibleCircles - 1 && total > totalVisibleCircles {
//                break
//            }
//            var inviteeView: UIView?
//            let frame = CGRect(x: Int(leadingImageOffset + 5), y: Int((selectedView.bounds.size.height - 30.0) / 2), width: 30, height: 30)
//            if selectedUsers[i].image != nil {
//                let imageView = UIImageView(frame: frame)
//                imageView.image = selectedUsers[i].image
//                imageView.contentMode = .scaleAspectFill
//                inviteeView = imageView
//            } else {
//                let labelView = UILabel(frame: frame)
//                labelView.backgroundColor = UIColor(hex: "27a7de") //selectedUsers[i].color
//                let name = selectedUsers[i].name.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
//                labelView.text = String(name[name.startIndex])
//                labelView.font = UIFont(name: "Roboto-Light", size: 12.0)
//                labelView.textColor = UIColor.white
//                labelView.clipsToBounds = true
//                labelView.textAlignment = .center
//                inviteeView = labelView
//            }
//            inviteeViews.append(inviteeView!)
//            selectedView.addSubview(inviteeView!)
//            inviteeView!.bia_rounded()
//            leadingImageOffset -= 35
//        }
//        
//        if selectedUsers.count > 0 {
//            inviteButton.bia_setEnabled(true)
//        }
//        
//    }
    
    //MARK: Actions
    func selectAllWasTappedOn(_ section: Int) {
        if sectionIsFollowing(section) {
            //following
            selectedFollowing = []
            selectedFollowing.append(contentsOf: searchFilteredFollowing)
            for followed in searchFilteredFollowing {
                selectedUsers.append(CameoInvitee( id: followed.user_id.md5(),
                                                   name: followed.fullName,
                                                   user_id: followed.user_id,
                                                   email: followed.email
                ))
            }
        } else {
            //contacts
            selectedContacts = []
            selectedContacts.append(contentsOf: searchFilteredContacts)
            for contact in searchFilteredContacts {
                selectedUsers.append(CameoInvitee(
                                                   id: contact.cameoIdentity(),
                                                   name: contact.name != nil ? contact.name! : "?",
                                                   email: contact.email,
                                                   phone: contact.phoneNumber
                ))
            }
        }
        
        //filter selected so that it is unique
        var hashes = [String]()
        selectedUsers = selectedUsers.filter { (invitee) -> Bool in
            defer { hashes.append(invitee.md5Identity) }
            return !hashes.contains(invitee.md5Identity)
        }
        
        tableview.reloadData()
        //selectionWasUpdated()
    }
    
    @IBAction func inviteButtonWasTapped(_ sender: Any) {
        sendInvites()
        navigationController.popViewController(animated: true)
    }
    
    func sendInvites() {
        let sendInvitesModel = CameoSendInvitesModel(invitees: selectedUsers,
                                                      cameoId: videoId!)
        RequestManager.postRequest("api/v1/cameo-invite",
                                   object: sendInvitesModel,
                                   queryParameters: nil,
                                   success: {[weak self] (req, res) in
                                    let alert = UIAlertController(title: "Invitations Sent!",
                                                                  message: "Your friends will add their videos soon!")
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: .cancel,
                                                                  handler: nil))
                                    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(alert,
                                                                                                                        animated: true,
                                                                                                                        completion: nil)
        },
                                   failure:{ (req, err) in
        })
    }
    
    func showInviteSuccessMessage() {
        
    }
}

class CameoUserTableViewCell: UITableViewCell {
    var selectedForInvite: Bool = false
    
    override func layoutSubviews() {
        userImageView.bia_rounded()
    }
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var selectedIcon: UIImageView!
    @IBOutlet weak var seperatorView: UIView!
    
}
