//
//  VideoPlayerManagerPlayerItem.h
//  BlingIphoneApp
//
//  Created by Michael Biehl on 11/1/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface VideoPlayerManagerPlayerItem : AVPlayerItem

@end
