//
//  SIAUploadUserImage.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 10/29/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAUploadUserImage.h"

@implementation SIAUploadUserImage

+(RKObjectMapping *)objectMapping{

    
    NSDictionary *responseMappingDictionary = @{@"photo.method" : @"image_method",
                                                @"photo.content_type" : @"image_content_type",
                                                @"photo.signed_url" : @"image_signed_url"};
    
    
    RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[SIAUploadUserImage class]];
    [responseMapping addAttributeMappingsFromDictionary:responseMappingDictionary];
    return responseMapping;
}

@end
