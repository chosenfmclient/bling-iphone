//
//  DeepLinkHandler.swift
//  BlingIphoneApp
//
//  Created by Zach on 01/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class DeepLinkDataModel: NSObject {
    var destination: UIViewController?
    var shareID: String?
    
    static let shortTypes = ["u" : "user",
                             "v" : "video",
                             "d" : "discover",
                             "l" : "leaderboard",
                             "du" : "duet"]
    
    init(destination: UIViewController?, shareID: String?) {
        self.destination = destination
        self.shareID = shareID
        super.init()
    }
    
    convenience init(shareID: String?) {
        self.init(destination: nil, shareID: shareID)
    }
    
    static func postShareIDForDeeplinkAnalytics(_ shareID: String?) {
        guard let id = shareID else { return }
        let shareWrapper = SIAShareMapping()
        shareWrapper.share_id = id
        
        RequestManager.postRequest(
            "api/v1/users/me/deeplink",
            object: shareWrapper,
            queryParameters: nil,
            success: { (req, res) in
                print("deeplink post successful")
            },
            failure: nil,
            showFailureDialog: false
        )
    }
    
    static func checkForDeferredDeeplinks(_ completion: @escaping (_ deeplink: DeepLinkDataModel?) -> Void) {
        guard BIALoginManager.userIsRegistered() else { return }
        RequestManager.getRequest(
            "api/v1/users/me/deeplink",
            queryParameters: nil,
            success: { (req, res) in
                guard let response = res?.firstObject as? DeepLinkResponseModel,
                    let type = response.type,
                    let objectID = response.object_id else {
                        completion(nil)
                    return
                }
                completion(
                    DeepLinkDataModel(
                        destination: getDestination(fromComponents: ["/", type, objectID]),
                     shareID: response.share_id
                    )
                )
            },
            failure: {(req, error) in
                completion(nil)
            }
        )
    }
    
    static func parse(url: URL, completion: @escaping (_ deeplink: DeepLinkDataModel?) -> Void)
    {
        if url.pathComponents.count > 2 && url.pathComponents[1] == "m" {
            getMarketingLinkData(id: url.pathComponents[2], completion: completion)
            return
        }
        
        var shareID = getShareIdQuery(from: url)
        
        if shareID == nil {
            shareID = getShortLinkShareID(from: url)
        }
        
        if shareID == nil && url.scheme == "blingy" && url.pathComponents.count > 3 {
            shareID = url.pathComponents[3]
        }
        
        let destination = getDestination(from: url)
        
        completion(DeepLinkDataModel(destination: destination, shareID: shareID))
    }
    
    static func getShortLinkShareID(from url: URL) -> String? {
        if url.pathComponents.count >= 3 && shortTypes.keys.contains(url.pathComponents[1]) && url.pathComponents[2].contains("_") {
            let objectShare = url.pathComponents[2].components(separatedBy: "_")
            if objectShare.count >= 2 {
                return objectShare[1]
            }
        }
        
        return nil
    }
    
    static func getMarketingLinkData(id: String, completion: @escaping (_ deeplink: DeepLinkDataModel?) -> Void) {
        RequestManager.getRequest(
            "api/v1/users/me/deeplink",
            queryParameters: ["m_id": id],
            success: { (req, res) in
                guard let response = res?.firstObject as? DeepLinkResponseModel,
                let type = response.type else {
                    completion(nil)
                    return
                }
                
                let objectID = response.object_id
                
                if type == "record" {
                    completion(
                        DeepLinkDataModel(
                            destination: UIStoryboard(name: "BlingStoryboard", bundle: nil).instantiateInitialViewController(),
                            shareID: response.share_id)
                    )
                } else if let _ = objectID {
                    completion(
                        DeepLinkDataModel(
                            destination: getDestination(fromComponents: ["/", type, objectID!]),
                            shareID: response.share_id)
                    )
                } else {
                    completion(nil)
                }
            },
            failure: {(req, error) in
                completion(nil)
            }
        )
    }
    
    static func getShareIdQuery(from url: URL) -> String?
    {
        var shareID: String?
        
        let querys = url.getQueryDictionary()
        if let _ = querys["share_id"] {
            shareID = querys["share_id"]
        }
        
        return shareID
    }
    
    static func getDestination(fromComponents pathComponents: [String]) -> UIViewController? {
        
        var fakeUrl = "http://blin.gy/"
        for component in pathComponents {
            fakeUrl = fakeUrl + component + "/"
        }
        
        return getDestination(from: URL(string: fakeUrl.trimmingCharacters(in: CharacterSet(charactersIn: "/")))!)
    }
    
    static func getDestination(from url: URL) -> UIViewController?
    {
        if shouldOpenRecord(from: url) {
            return UIStoryboard(name: "BlingStoryboard", bundle: nil).instantiateInitialViewController()
        }
        
        guard url.pathComponents.count > 2  else {
            return nil
        }
        
        var destination: UIViewController?

        var type = url.pathComponents[1]
        
        if let t = shortTypes[type] {
            type = t
        }
        
        var objectId = url.pathComponents[2]
        if objectId.contains("_") {
            objectId = objectId.components(separatedBy: "_")[0]
        }
        
        switch type {
        case "video", "duet":
            //get video viewcontroller
            let vc = UIStoryboard(name: "VideoHomeStoryboard", bundle: nil)
                .instantiateInitialViewController() as! VideoHomeViewController
            vc.videoId = objectId
            vc.setForCameoInvite = type == "duet"
            destination = vc
            break
        case "user":
            //get profile viewcontroller
            let vc = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "profileViewController") as! ProfileViewController
            vc.userId = objectId
            destination = vc
            break
        case "discover":
            let vc = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "discoverViewController") as! DiscoverViewController
            vc.clipID = objectId
            destination = vc
            break
        case "leaderboard":
            let vc = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "leaderboard")
            destination = vc
        default:
            break
        }
        
        return destination
    }
    
    fileprivate static func shouldOpenRecord(from url: URL) -> Bool {
        var record = false
        
        let querys = url.getQueryDictionary()
        
        if let _ = querys["record"] {
            record = true
        }
        
        return record
    }
    
    
}

@objc class DeepLinkResponseModel: NSObject {
    var type: String?
    var object_id: String?
    var share_id: String?
    
    static func objectMapping() -> RKObjectMapping {
        let objectMapping = RKObjectMapping(for: DeepLinkResponseModel.self)
        objectMapping?.addAttributeMappings(from: ["type", "object_id", "share_id"])
        return objectMapping!
    }
}
