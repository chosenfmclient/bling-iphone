//
// Created by Ben on 04/12/2016.
//

#include "imageUtil.h"
//#include <android/log.h>
#include "globals.h"

int whitePointAverage = 200;

imageUtil::imageUtil()
{
    defBlurSize = Size(kernel_size, kernel_size);
}

imageUtil::~imageUtil()
{

}

void imageUtil::mergeBuffers(Mat src1, Mat src2, Mat dst)//(uchar* src1, uchar* src2, uchar* dst)
{
    int width1 = src1.cols, height1 = src1.rows, inc1 = src1.channels() * width1;
    int width2 = src2.cols, height2 = src2.rows, inc2 = src2.channels() * width2;
    int widthDst = dst.cols, heightDst = dst.rows, incDst = dst.channels() * widthDst;

    int offsetSrc2 = (heightDst - height2) / 2;
    if(offsetSrc2 < 0)
        offsetSrc2 = 0;

//            __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLevaaa>2>> %d   -   %d ====   %d   -   %d", heightDst, height2, offsetSrc2, dst.channels());
    for(int c = widthDst, index1, index2, indexDst ; c > 0; c--)
    {
        index1 = c * src1.channels();
        index2 = c * src2.channels();
        indexDst = c * dst.channels();
        for (int r = 0; r < heightDst; r++, index1 += inc1, index2 += inc2, indexDst += incDst)
        {
            if (c < width1 && r < height1)
            {
                dst.data[indexDst] = src1.data[index1];
                dst.data[indexDst + 1] = src1.data[index1 + 1];
                dst.data[indexDst + 2] = src1.data[index1 + 2];
            }
//
            if(c < width2 && r > offsetSrc2 && r < height2 + offsetSrc2)
            {
                index2 = ((r - offsetSrc2) * (width2) * src2.channels()) + (c * src2.channels());
                dst.data[indexDst] = src2.data[index2];
                dst.data[indexDst + 1] = src2.data[index2 + 1];
                dst.data[indexDst + 2] = src2.data[index2 + 2];
            }

//            dst.data[indexDst] = 0;
//            dst.data[indexDst + 1] = 255;
//            dst.data[indexDst + 2] = 0;
        }
    }
}

void imageUtil::mergeBuffer(Mat src, Mat dst)
{
    int widthSrc = src.cols, heightSrc = src.rows, incSrc = src.channels() * widthSrc;
    int widthDst = dst.cols, heightDst = dst.rows, incDst = dst.channels() * widthDst;

    int offsetSrc = (heightDst - heightSrc) / 2;
    if(offsetSrc < 0)
        offsetSrc = 0;

//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLevaaa>2>> %d   -   %d ====   %d   -   %d", heightDst, heightSrc, widthDst, offsetSrc);

    for(int c = widthDst, indexSrc, indexDst; c > 0; c--)
    {
        indexSrc = c * src.channels();
        indexDst = c * dst.channels();
        for (int r = 0; r < heightDst; r++, indexSrc += incSrc, indexDst += incDst)
        {
            if(c < widthSrc && r > offsetSrc && r < heightSrc + offsetSrc)
            {
                indexSrc = ((r - offsetSrc) * (widthSrc) * src.channels()) + (c * src.channels());
                dst.data[indexDst] = src.data[indexSrc];
                dst.data[indexDst + 1] = src.data[indexSrc + 1];
                dst.data[indexDst + 2] = src.data[indexSrc + 2];
            }

//            if (c < widthSrc && r < heightSrc)
//            {
//                dst.data[indexDst] = src.data[indexSrc];
//                dst.data[indexDst + 1] = src.data[indexSrc + 1];
//                dst.data[indexDst + 2] = src.data[indexSrc + 2];
//            }
        }
    }
}

void imageUtil::mergeBufferAccordingToBuffers(Mat src, Mat mask1, Mat mask2, Mat dst)
{
    int widthSrc = src.cols, heightSrc = src.rows, srcChannels = src.channels(), widthSrcChannels = srcChannels * widthSrc;
    int widthDst = dst.cols, heightDst = dst.rows, incDst = dst.channels() * widthDst;
    int widthMask = mask1.cols;

    int offsetSrc = ( heightDst - heightSrc ) / 2;
//    int shiftAmount = 0;
    if(offsetSrc < 0)
        offsetSrc = 0;

    heightSrc += offsetSrc;

//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLevaaa>2>> %d   -   %d ====   %d   -   %d", heightDst, heightSrc, widthDst, widthSrc);

    for(int c = widthDst, indexSrc, indexDst, indexMask; c >= 0; c--)
    {
//        indexSrc = c * src.channels();, indexMask += widthMask//, indexSrc += incSrc
        indexDst = c * dst.channels();
        for (int r = 0; r <= heightDst; r++, indexDst += incDst)
        {
            if(c <= widthSrc && r >= offsetSrc && r <= heightSrc)
            {
                indexMask = ((r - offsetSrc) * widthMask) + c;

                if(mask1.data[indexMask] == WHITE_PIXEL || mask2.data[indexMask] == WHITE_PIXEL)
                {
                    indexSrc = ((r - offsetSrc) * widthSrcChannels) + (c * src.channels());


                    dst.data[indexDst] = src.data[indexSrc];
                    dst.data[indexDst + 1] = src.data[indexSrc + 1];
                    dst.data[indexDst + 2] = src.data[indexSrc + 2];
                }
            }

//            if (c < widthSrc && r < heightSrc)
//            {
//                dst.data[indexDst] = src.data[indexSrc];
//                dst.data[indexDst + 1] = src.data[indexSrc + 1];
//                dst.data[indexDst + 2] = src.data[indexSrc + 2];
//            }
        }
    }
}

void imageUtil::mergeBufferAccordingToBuffersII(Mat src, Mat mask1, Mat mask2, Mat dst)
{
    int widthSrc = src.cols, heightSrc = src.rows, srcChannels = src.channels(), widthSrcChannels = srcChannels * widthSrc;
    int widthDst = dst.cols, heightDst = dst.rows, dstChannels = dst.channels(), incDst = dst.channels() * widthDst;
    int widthMask = mask1.cols;
    for(int c = widthDst, indexSrc, indexDst, indexMask; c >= 0; c--)
    {
        indexSrc = c * srcChannels;
        indexDst = c * dstChannels;
        indexMask = c;
        for (int r = 0; r <= heightDst; r++, indexDst += incDst, indexMask += widthMask, indexSrc += widthSrcChannels)
        {
            if(mask1.data[indexMask] == WHITE_PIXEL || mask2.data[indexMask] == WHITE_PIXEL)
            {
                dst.data[indexDst] = src.data[indexSrc];
                dst.data[indexDst + 1] = src.data[indexSrc + 1];
                dst.data[indexDst + 2] = src.data[indexSrc + 2];
            }
        }
    }
}

void imageUtil::mergeBufferAccordingToBufferII(Mat src, Mat mask1, Mat dst)
{
    int widthSrc = src.cols, heightSrc = src.rows, srcChannels = src.channels(), widthSrcChannels = srcChannels * widthSrc;
    int widthDst = dst.cols, heightDst = dst.rows, dstChannels = dst.channels(), incDst = dst.channels() * widthDst;
    int widthMask = mask1.cols;
    uchar* dstData = dst.data;
    uchar* srcData = src.data;
    uchar* maskData = mask1.data;
    for(int c = widthDst, indexSrc, indexDst, indexMask; c >= 0; c--)
    {
        indexSrc = c * srcChannels;
        indexDst = c * dstChannels;
        indexMask = c;
        for (int r = 0; r <= heightDst; r++, indexDst += incDst, indexMask += widthMask, indexSrc += widthSrcChannels)
        {
            if(maskData[indexMask] == WHITE_PIXEL)
            {
                dstData[indexDst] = srcData[indexSrc];
                dstData[indexDst + 1] = srcData[indexSrc + 1];
                dstData[indexDst + 2] = srcData[indexSrc + 2];
            }
        }
    }
}

void imageUtil::mergeBufferAccordingToBufferIIWithAlpha(Mat& src, Mat& mask1, Mat& dst)
{
    static const int widthSrc = src.cols, heightSrc = src.rows, srcChannels = src.channels(), widthSrcChannels = srcChannels * widthSrc;
    static const int widthDst = dst.cols, heightDst = dst.rows, dstChannels = dst.channels(), incDst = dst.channels() * widthDst;
    static const int widthMask = mask1.cols;

    uchar* dstData = dst.data;
    uchar* srcData = src.data;
    uchar* maskData = mask1.data;

    float dstMaskDif, srcMaskDif;
    for(int c = widthDst, indexSrc, indexDst, indexMask; c >= 0; c--)
    {
        indexSrc = c * srcChannels;
        indexDst = c * dstChannels;
        indexMask = c;
        for (int r = 0; r <= heightDst; r++, indexDst += incDst, indexMask += widthMask, indexSrc += widthSrcChannels)
        {
//            if(maskData[indexMask] == WHITE_PIXEL)
//            {
//                dstData[indexDst] = srcData[indexSrc];
//                dstData[indexDst + 1] = srcData[indexSrc + 1];
//                dstData[indexDst + 2] = srcData[indexSrc + 2];
//            }

            if(maskData[indexMask] != 0)
            {
//                dstData[indexDst] = ( (dstData[indexDst] * (255.0f - maskData[indexMask])) / 255.0f) + ((srcData[indexSrc] * maskData[indexMask]) / 255.0f);
//                dstData[indexDst + 1] = ( (dstData[indexDst + 1] * (255.0f - maskData[indexMask])) / 255.0f) + ((srcData[indexSrc + 1] * maskData[indexMask]) / 255.0f);
//                dstData[indexDst + 2] = ( (dstData[indexDst + 2] * (255.0f - maskData[indexMask])) / 255.0f) + ((srcData[indexSrc + 2] * maskData[indexMask]) / 255.0f);

                dstMaskDif = (255.0f - maskData[indexMask]) / 255.0f;
                srcMaskDif = maskData[indexMask] / 255.0f;
                dstData[indexDst] = ( dstData[indexDst] * dstMaskDif) + (srcData[indexSrc] * srcMaskDif);
                dstData[indexDst + 1] = ( dstData[indexDst + 1] * dstMaskDif) + (srcData[indexSrc + 1] * srcMaskDif);
                dstData[indexDst + 2] = ( dstData[indexDst + 2] * dstMaskDif) + (srcData[indexSrc + 2] * srcMaskDif);
            }
        }
    }
}

//Mat imageUtil::loadBg(Mat src)
//{
//    Mat bg;
//    transpose(src, bg);
////    resize(bg, bg, Size(640, 480));
//    flip(bg, src, 1);
//    cvtColor(src, bg, COLOR_BGR2RGBA);
//
//    return bg;
//}

//Mat imageUtil::loadBg(const char* fileName)
//{
//    Mat bg;
//    transpose(imread(fileName, -10), bg);
//    flip(bg, bg, 1);
//
//    cvtColor(bg, bg, COLOR_BGR2RGB);
//
//    Mat crop = cropImage(bg, 60, 0, 540, 360);
//    bg.release();
//
//    return crop;
//}


//Mat imageUtil::loadBg(const char* fileName)
//{
////    if(lastMat == NULL)
////    if(! tempMatInitialize)
////    {
////        tempMatInitialize = true;
////        Mat bg = imread(fileName, -10);
//////        lastMat = bg;
////        lastMat = cropImage(lastMat, 0, 60, 360, 540);
////    }
//    Mat bg = imread(fileName, -10);
//
////    if(isFirstTimeLoadingBg)
////    {
////
////    }
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>333333>> %d   -   %d", bg.cols, bg.rows);
//
//    lastMat = cropImage(bg, 0, 60, 360, 540);
//    bg.release();
//
////    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>333333>> %d   -   %d", bg.cols, bg.rows);
////    Mat crop = cropImage(bg, 0, 60, 360, 540);
////    Mat crop = cropImage(bg, 60, 0, 540, 360);
//
////    transpose(cropImage(bg, 0, 60, 360, 540), bg);
////    flip(bg, bg, 1);
////
////    cvtColor(bg, bg, COLOR_BGR2RGB);
//
//    transpose(lastMat, lastMat);
//    flip(lastMat, lastMat, 1);
//
//    cvtColor(lastMat, lastMat, COLOR_BGR2RGB);
//
////    return bg;
//    return lastMat;
//}

void imageUtil::loadBg(const char* fileName, Mat& bg)
{
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>bg cols rows> bg after crop> %d   -   %d ", bg.cols, bg.rows);

//    if(isLiteVideo)
//    {
//        bg = imread(fileName, -10);
////        480 - 320 =  160 / 2 = 80
//        Mat tempBg;
//        cropImage(bg, tempBg, 0, 0, 270, 320);
//        transpose(tempBg, bg);
//        flip(bg, tempBg, 1);
//        cvtColor(tempBg, bg, COLOR_BGR2RGB);
//        tempBg.release();
//
//
//
//    }
//    else
//    {
//        bg = imread(fileName, -10);
////        640 - 480 = 160 / 2 = 80
//        Mat tempBg;
//        cropImage(bg, tempBg, 0, 60, 360, 540);
////    bg.release();
//
//        transpose(tempBg, bg);
////    tempBg.release();
//
//        flip(bg, tempBg, 1);
////    bg.release();
//
//        cvtColor(tempBg, bg, COLOR_BGR2RGB);
//        tempBg.release();
//    }
}

void* imageUtil::loadBgThreadRunner(void* data)
{
//    loadBgStructure* bgStructure = ((loadBgStructure*)data);
//    bgStructure->bg.release();
//
//    if(isLiteVideo)
//    {
//        //considering that all videos that arrives here are at least 270 * 480
//        Mat bgLoaded = imread(bgStructure->fileName, -10);
//
//        iUtil.cropImage(bgLoaded, bgStructure->bg, 0, 60, 270, 380);
//        bgLoaded.release();
//
//        transpose(bgStructure->bg, bgLoaded);
//        bgStructure->bg.release();
//
//        flip(bgLoaded, bgStructure->bg, 1);
//        bgLoaded.release();
//
//        cvtColor(bgStructure->bg, bgLoaded, COLOR_BGR2RGB);
//        bgStructure->bg.release();
//
//        bgStructure->bg = bgLoaded;
//    }
//    else
//    {
//
//        //considering that all videos that arrives here are at least 360 * 640
//        Mat bgLoaded = imread(bgStructure->fileName, -10);
//
//        iUtil.cropImage(bgLoaded, bgStructure->bg, 0, 60, 360, 540);
//        bgLoaded.release();
//
//        transpose(bgStructure->bg, bgLoaded);
//        bgStructure->bg.release();
//
//        flip(bgLoaded, bgStructure->bg, 1);
//        bgLoaded.release();
//
//        cvtColor(bgStructure->bg, bgLoaded, COLOR_BGR2RGB);
//        bgStructure->bg.release();
//
//        bgStructure->bg = bgLoaded;
//    }
//
//    pthread_exit(0);
    return NULL;
}

void imageUtil::loadBgThreaded(Mat& bg, const char* fileName, pthread_t& dstThread)
{
    loadBgStructure bgStructure = {bg, fileName};
    pthread_create(&dstThread, NULL, loadBgThreadRunner, (void*) &bgStructure);
    pthread_join(dstThread, 0);
}

void imageUtil::resizeAccordingToWidth270(Mat& src, int width, int height)
{
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>333333>> %d   -   %d  >> %d   -   %d ", src.cols, src.rows, width, height);
    if(src.cols != width)
    {
        if(width != WIDTH)
        {
            float scaleFactor = ((float)width) / ((float)src.cols);
//            int newHeight = (int) ( ((float)src.rows) * scaleFactor );// + 40;

            setImageWidthHeight(width, height, scaleFactor);
        }
//        return cropImage(src, 0, CAMERA_PADDING, height, width + CAMERA_PADDING);
        Mat dst;
        cropImage(src, dst, 0, CAMERA_PADDING, height, width + CAMERA_PADDING);
        src.release();
        src = dst;
    }
//    return src;
}

void imageUtil::resizeAccordingToWidth90(Mat& src, int width, int height)
{
    if(src.cols != width)
    {
        if(width != WIDTH)
        {
            float scaleFactor = ((float)width) / ((float)src.cols);
            int newHeight = (int) ( ((float)src.rows) * scaleFactor );// + 40;
            setImageWidthHeight(width, height, scaleFactor);
        }
        Mat res;
        flip(src, res, -1);
//        res = cropImage(res, 0, 0, height, width);
        cropImage(res, src, 0, 0, height, width);
//        return res;
    }
//    return src;
}

Mat imageUtil::resizeAccordingToWidth90DetectionPhaseI(Mat src, int width, int height)
{
    Mat res;
    flip(src, res, -1);
    return res;
}

Mat imageUtil::resizeAccordingToWidth90PhaseII(Mat src, int width, int height)
{
    Mat dst;
    cropImage(src, dst, 0, 0, height, width);
    src.release();
    return dst;
}

void imageUtil::cropImage(Mat &src, Mat &dst, int left, int top, int right, int bottom)
{
//    if(src.cols < top || src.rows < left )
//        return src;

//    if(bottom > src.cols || right > src.rows)

    dst = Mat(Size(bottom - top, right - left), src.type());

    int incSrc = (src.cols * src.channels()), incDst = (dst.cols * dst.channels());
    int baseIndex = left * incSrc;

    int   channels    = src.channels()
        , bottomLimit = src.cols - bottom
        , cDst        = dst.cols
        , indexSrc
        , indexDst;

    uchar* dstData = dst.data;
    uchar* srcData = src.data;

    if(channels == 3)
    {
        for (int c = src.cols - top; c >= bottomLimit; c--, cDst--)
        {
            indexSrc = baseIndex + (c * channels);
            indexDst = (cDst * channels);

            for (int r = left; r < right; r++, indexSrc += incSrc, indexDst += incDst)
            {
//            dst.get(c,r) = src[c][r];
//            for( int ch = 0; ch < 3; ch++)
//                dstData[indexDst + ch] = srcData[indexSrc + ch];
                *(dstData + indexDst) = *(srcData + indexSrc);
                *(dstData + indexDst + 1) = *(srcData + indexSrc + 1);
                *(dstData + indexDst + 2) = *(srcData + indexSrc + 2);
//            dstData[indexDst] = srcData[indexSrc];
//            dstData[indexDst + 1] = srcData[indexSrc + 1];
//            dstData[indexDst + 2] = srcData[indexSrc + 2];
//            array[]
            }
        }
    }
    else if(channels == 4)
    {
        for (int c = src.cols - top; c >= bottomLimit; c--, cDst--)
        {
            indexSrc = baseIndex + (c * channels);
            indexDst = (cDst * channels);

            for (int r = left; r < right; r++, indexSrc += incSrc, indexDst += incDst)
            {
//            dst.get(c,r) = src[c][r];
//            for( int ch = 0; ch < 3; ch++)
//                dstData[indexDst + ch] = srcData[indexSrc + ch];
                *(dstData + indexDst) = *(srcData + indexSrc);
                *(dstData + indexDst + 1) = *(srcData + indexSrc + 1);
                *(dstData + indexDst + 2) = *(srcData + indexSrc + 2);
                *(dstData + indexDst + 3) = *(srcData + indexSrc + 3);
//            dstData[indexDst] = srcData[indexSrc];
//            dstData[indexDst + 1] = srcData[indexSrc + 1];
//            dstData[indexDst + 2] = srcData[indexSrc + 2];
//            dstData[indexDst + 3] = srcData[indexSrc + 3];
//            array[]
            }
        }
    }

//    return dst;
}

//Mat imageUtil::cropImage(Mat src, int left, int top, int right, int bottom)
//{
////    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>333333>> %d   -   %d  >> %d   -   %d  >> %d   -   %d", src.cols, src.rows, left, top, right, bottom);
//
//    if(src.cols == 0 || src.rows == 0)
//        return src;
//
//    Mat dst(Size(bottom - top, right - left), src.type());
//
//    int incSrc = (src.cols * src.channels()), incDst = (dst.cols * dst.channels());
//    int increaser = src.cols * src.channels();
//    int baseIndex = left * incSrc;
//
//    uchar* dstData = dst.data;
//    uchar* srcData = src.data;
//
//
//    for(int c = src.cols - top, cDst = dst.cols, indexSrc, indexDst, channels = src.channels(), bottom = src.cols - bottom; c >= bottom; c--, cDst--)// && cDst > 0
//    {
//        //todo adjust with row * WIDTH * 4 to indexSrc indexDst
//
//        indexSrc = (left * increaser) + (c * channels);
//        indexDst = (left * increaser) + (cDst * channels);
//
//        for (int r = left, rDst = 0; r <= right; r++, rDst++, indexSrc += incSrc, indexDst += incDst)//, indexMask += widthMask) && rDst < dst.rows
//        {
//            dstData[indexDst] = srcData[indexSrc];
//            dstData[indexDst + 1] = srcData[indexSrc + 1];
//            dstData[indexDst + 2] = srcData[indexSrc + 2];
//        }
//    }
//    return dst;
//}

void imageUtil::cropImage(Mat& src, Mat& dst, int x, int y)
{
    int left = x, top = y, right = x + dst.rows, bottom = y + dst.cols;
    int incSrc = (src.cols * src.channels()), incDst = (dst.cols * dst.channels());
    uchar* dstData = dst.data;
    uchar* srcData = src.data;
    for(int c = src.cols - top, cDst = dst.cols, indexSrc, indexDst, channelsSrc = src.channels(), channelsDst = dst.channels();
                    c >= src.cols - bottom; c--, cDst--)// && cDst > 0
    {
        indexSrc = c * channelsSrc;
        indexDst = cDst * channelsDst;
        for (int r = left, rDst = 0; r <= right; r++, rDst++, indexSrc += incSrc, indexDst += incDst)//, indexMask += widthMask) && rDst < dst.rows
        {
            dstData[indexDst] = srcData[indexSrc];
            dstData[indexDst + 1] = srcData[indexSrc + 1];
            dstData[indexDst + 2] = srcData[indexSrc + 2];
        }
    }
}

Mat imageUtil::resizeAccordingToHeight(Mat src, int height)
{
    if(src.cols != height)
    {
        if(height != WIDTH)
        {
            float scaleFactor = ((float)height) / ((float)src.cols);
            int newHeight = (int) ( ((float)src.rows) * scaleFactor );// + 40;
//            setImageWidthHeight(width + 30, newHeight, scaleFactor);
            setImageWidthHeight(height, newHeight, scaleFactor);
        }
        Mat retMat;
        resize(src, retMat, Size(WIDTH, HEIGHT));
//        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>333333>> %d   -   %d", WIDTH, HEIGHT);

//        Mat cropped = retMat()

        return retMat;
    }
    return src;
}

void imageUtil::setImageWidthHeight(int newWidth, int newHeight, float iScaleFactor)
{
    scaleFactor = iScaleFactor;
    CAMERA_PADDING = (CAMERA_HEIGHT - HEIGHT) / 2;

    WIDTH = newWidth; HEIGHT = newHeight;
    WIDTH_X_4 = WIDTH * 4;
    WIDTH_X_3 = WIDTH * 3;

    MAX_SIZE = WIDTH * HEIGHT;
    MAX_SIZE_X_4 = WIDTH * HEIGHT * 4;
    MAX_SIZE_X_3 = WIDTH * HEIGHT * 3;

    defMatSize = Size(WIDTH, HEIGHT);

    CAMERA_DIFFERENCE = (CAMERA_WIDTH - WIDTH);// / 2
}

void imageUtil::noiseCleaning(Mat &mask, int minimumWidth)
{
    Mat labels, stats, centroid;
    std::vector<int> areasToShow;
    int areasNum = connectedComponentsWithStats(mask, labels, stats, centroid, 4);

    for (int i = 1, area; i < areasNum; i++)
    {
        area = stats.at<int>(i, CC_STAT_AREA);
        if(area > minimumWidth)
            areasToShow.push_back(i);
    }

    Mat rangeMat(mask.size(), mask.type());
    Mat tmpRange;
    for (int i = 0; i < areasToShow.size(); i++)
    {
        inRange(labels, Scalar(areasToShow.at(i)), Scalar(areasToShow.at(i)), tmpRange);
        bitwise_or(tmpRange, rangeMat, rangeMat);
    }

    tmpRange.release();
    labels.release();
    stats.release();
    centroid.release();

    mask.release();
    mask = rangeMat;
}

void imageUtil::noiseCleaningII(Mat &mask, int minimumWidth)
{
    {   Mat labels, stats, centroid;
        std::vector<int> areasToShow;
        int areasNum = connectedComponentsWithStats(mask, labels, stats, centroid, 4);

        for (int i = 1, area; i < areasNum; i++)
        {
            area = stats.at<int>(i, CC_STAT_AREA);
            if (area > minimumWidth)
                areasToShow.push_back(i);
        }
        Mat rangeMat(mask.size(), mask.type());
        Mat tmpRange;
        for (int i = 0; i < areasToShow.size(); i++)
        {
            inRange(labels, Scalar(areasToShow.at(i)), Scalar(areasToShow.at(i)), tmpRange);
            bitwise_or(tmpRange, rangeMat, rangeMat);
        }
        tmpRange.release();
        labels.release();
        stats.release();
        centroid.release();

        mask.release();
        mask = rangeMat;
    }
    Mat invertedMask;
    //threshold(mask, invertedMask, 220, WHITE_PIXEL, THRESH_BINARY_INV);
    bitwise_not(mask, invertedMask);
    mask.release();
    mask = invertedMask;
    minimumWidth /= 2;
    {   Mat labels, stats, centroid;
        std::vector<int> areasToShow;
        int areasNum = connectedComponentsWithStats(mask, labels, stats, centroid, 4);

        for (int i = 1, area; i < areasNum; i++)
        {
            area = stats.at<int>(i, CC_STAT_AREA);
            if (area > minimumWidth)
                areasToShow.push_back(i);
        }
        Mat rangeMat(mask.size(), mask.type());
        Mat tmpRange;
        for (int i = 0; i < areasToShow.size(); i++)
        {
            inRange(labels, Scalar(areasToShow.at(i)), Scalar(areasToShow.at(i)), tmpRange);
            bitwise_or(tmpRange, rangeMat, rangeMat);
        }
        tmpRange.release();
        labels.release();
        stats.release();
        centroid.release();

        mask.release();
        mask = rangeMat;
    }
    Mat reInvertedMask;
    //threshold(mask, reInvertedMask, 220, WHITE_PIXEL, THRESH_BINARY_INV);
    bitwise_not(mask, reInvertedMask);
    mask.release();
    mask = reInvertedMask;
}

Mat lDs;
void imageUtil::noiseCleaningIII(Mat &mDss, Mat &mask, int minimumWidth)
{
    lDs = mDss;
    Mat labels, stats, centroid;
    std::vector<int> areasToShow;
    std::vector<int> areasNotToShow;
    int areasNum = connectedComponentsWithStats(mask, labels, stats, centroid, 4);



    Mat canny = getCanny(mDss);
    Mat bDss(mDss.size(), mDss.type());
    blur(mDss, bDss, Kernel_5X5);

    for (int i = 1, area; i < areasNum; i++)
    {
        area = stats.at<int>(i, CC_STAT_AREA);
        if (area > minimumWidth)
        {
            areasToShow.push_back(i);
        }
        else if(area > minimumWidth / 4)
        {
//            areasNotToShow.push_back(§i);

            int padding = 0;
            int left   = stats.at<int>(i, CC_STAT_LEFT) - padding;
            int top    = stats.at<int>(i, CC_STAT_TOP)  - padding;
            int right  = stats.at<int>(i, CC_STAT_WIDTH)  + left + padding;
            int bottom = stats.at<int>(i, CC_STAT_HEIGHT) + top + padding;


//            drawRec(mDss, left, top, right, bottom, i * 12);
//            drawRec(mDss, top, left, bottom, right, i * 12);
//            drawRec(mDss, top, left, bottom, right, i * 12);
//            bool hasEdge = hasEdges(canny, top, left, bottom, right);
            if(!hasEdges(canny, top, left, bottom, right))
//            if(!hasEdge)
            {
//                __android_log_print(ANDROID_LOG_ERROR, "tag", "copyMat111---------areasNum> %d >> %d   %d   %d   %d  >>> hasEdges> %d  ", i, left, top, right, bottom, hasEdge);
//                __android_log_print(ANDROID_LOG_ERROR, "tag", "copyMat111---------areasNum> %d >> %d   %d   %d   %d  >>> hasEdges", i, left, top, right, bottom);
//                __android_log_print(ANDROID_LOG_ERROR, "tag", "copyMat1112---------areasNum> %d >> %d   %d   chromaKeys =  %d", i, mDss.cols, mDss.rows, cUtil.getArrayLength());

                cUtil.addChromaKeysSectioned(bDss, top, left, bottom, right);
            }
        }
    }

    // testing
//    mDss = bDss;
//    return;



    Mat rangeMat(mask.size(), mask.type());
    Mat tmpRange;

    for (int i = 0; i < areasToShow.size(); i++)
    {
        inRange(labels, Scalar(areasToShow.at(i)), Scalar(areasToShow.at(i)), tmpRange);
        bitwise_or(tmpRange, rangeMat, rangeMat);
    }

//    for (int i = 0; i < areasNotToShow.size(); i++)
//    {
//        inRange(labels, Scalar(areasNotToShow.at(i)), Scalar(areasNotToShow.at(i)), tmpRange);
//        bitwise_or(tmpRange, rangeMat, rangeMat);
//    }

//    mDss = rangeMat;
//    return;
//
//
//        for(int c = 0, index; c < rangeMat.cols; c++)
//        {
//            index = c;
//            for(int r = 0; r < rangeMat.rows; r++, index += rangeMat.cols)
//            {
////            if(stats.data[i] != 0)
////                mask.data[i] = WHITE_PIXEL;
////            else mask.data[i] = 0;
////            mask.data[i] = rangeMat.data[i];
//                if(rangeMat.data[index] != 0)
//                    cUtil.addChromaKey(mDss.data, (r * mDss.channels() * mDss.cols) + (c * mDss.channels()));
//            }
//        }

        tmpRange.release();
        labels.release();
        stats.release();
        centroid.release();

//        mask.release();
        mask = rangeMat;

//        return;

    Mat invertedMask;
    threshold(mask, invertedMask, 220, WHITE_PIXEL, THRESH_BINARY_INV);
    mask.release();
    mask = invertedMask;
    minimumWidth /= 2;

    {   Mat labels, stats, centroid;
        std::vector<int> areasToShow;
        int areasNum = connectedComponentsWithStats(mask, labels, stats, centroid, 4);

        for (int i = 1, area; i < areasNum; i++)
        {
            area = stats.at<int>(i, CC_STAT_AREA);
            if (area > minimumWidth)
                areasToShow.push_back(i);
        }
        Mat rangeMat(mask.size(), mask.type());
        Mat tmpRange;
        for (int i = 0; i < areasToShow.size(); i++)
        {
            inRange(labels, Scalar(areasToShow.at(i)), Scalar(areasToShow.at(i)), tmpRange);
            bitwise_or(tmpRange, rangeMat, rangeMat);
        }
        tmpRange.release();
        labels.release();
        stats.release();
        centroid.release();

        mask.release();
        mask = rangeMat;
    }
    Mat reInvertedMask;
    threshold(mask, reInvertedMask, 220, WHITE_PIXEL, THRESH_BINARY_INV);
    mask.release();
    mask = reInvertedMask;

}

void imageUtil::copyMats(Mat &src, Mat &dst)
{
//    dst = src;return;
    Mat mDst = Mat(src.size(), 0);

    uchar* srcData = src.data;
    uchar* dstData = mDst.data;

    int channels = src.channels(), width = src.cols, height = src.rows, index, increaser = width * channels;

    for (int c = width - channels; c != 0; c--)
    {
        index = c * channels;
        for(int r = 0; r < height; r++, index += increaser)
        {
            for(int ch = 0; ch < channels; ch++)
            {
                dstData[index + ch] = srcData[index + ch];
            }
        }
    }

    dst =  mDst;
}

void imageUtil::drawRec(Mat &dss, int left, int top, int right, int bottom, int color)
{
    int channels = dss.channels(), width = dss.cols, height = dss.rows, index, increaser = width * channels;

    uchar* dssData = dss.data;

    if(left < 0)
        left = 0;

    if(right > dss.rows)
        right = dss.rows;

    if(top < 0)
        top = 0;

    if(bottom > dss.cols)
        bottom = dss.cols;

    int baseIndex = left * channels * width;


    for (int c = bottom - 1; c > top; c--)
    {
        index = baseIndex + (c * channels);
        for(int r = left; r < right; r++, index += increaser)
        {
            for(int ch = 0; ch < channels; ch++)
            {
                dssData[index + ch] = color;
            }
        }
    }
}

bool imageUtil::hasEdges(Mat &cannyMask, int left, int top, int right, int bottom)
{
    uchar* cannyMaskData = cannyMask.data;


    if(left < 0)
        left = 0;

    if(right > HEIGHT)
        right = HEIGHT;

    if(top < 0)
        top = 0;

    if(bottom > WIDTH)
        bottom = WIDTH;

    int baseIndex = left * WIDTH;

    for (int c = bottom, index; c > top; c--)
    {
        index = baseIndex + c;
        for(int r = left; r < right; r++, index += WIDTH)
        {
//            if(drawDetectionGuilds)
//            if(cannyMaskData[index] != 0)
//            {
//                lDs.data[(index * 4)] = 0;
//                lDs.data[(index * 4) + 1] = 255;
//                lDs.data[index * 4 + 2] = 0;
//            }
//            else
//            {
//                lDs.data[(index * 4)] = 0;
//                lDs.data[(index * 4) + 1] = 0;
//                lDs.data[index * 4 + 2] = 255;
//            }

            if(cannyMaskData[index] != 0)
                return true;
        }
    }

    return false;
}

Mat imageUtil::getCanny(Mat &dss)
{
    Mat mask(dss.size(), 0);
    cvtColor(dss, mask, COLOR_RGBA2GRAY);

    Mat Blured(dss.size(), 0);

    blur(mask, Blured, defBlurSize);
//    blur(Blured, Blured, defBlurSize);

    Canny(Blured, mask, lowThreshold, max_lowThreshold, kernel_size);

    Blured.release();

    return mask;
}

void imageUtil::drawFace90(Mat bg, Mat mask, int faceLeft, int faceTop, int faceRight, int faceBottom, int red, int green, int blue)
{
        for (int r = 0, index, index2; r < HEIGHT; r++)
    {
        index = WIDTH * r * 4;
        index2 = WIDTH * r;
        for (int c = 0; c < WIDTH; c++, index += 4, index2++)
        {
//            if (mask.data[index2] == 255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//            else
//            {
//                bg.data[index] = 100;
//                bg.data[index + 1] = 100;
//                bg.data[index + 2] = 100;
//            }

            // we get the image rotated, and so the face detection is rotated as well

            //  faceRight -> top
            //  faceTop   -> left
            //  faceLeft  -> bottom
            //  faveBottom-> right

            if(faceLeft!=0)
            {
                if (c == faceRight && r >= faceTop && r <= faceBottom)
                {
                    bg.data[index] = 0;
                    bg.data[index + 1] = 0;
                    bg.data[index + 2] = 255;
                }

                if (r == faceTop && c <= faceLeft && c >= faceRight)
                {
                    bg.data[index] = 0;
                    bg.data[index + 1] = 255;
                    bg.data[index + 2] = 0;
                }

//                if (c == faceLeft && r >= faceTop && r <= faceBottom)
                if (c == faceLeft && r >= faceTop && r <= faceBottom)
                {
                    bg.data[index] = 255;
                    bg.data[index + 1] = 0;
                    bg.data[index + 2] = 0;
                }

//                if (r == faceBottom && c >= faceLeft && c <= faceRight)
                if (r == faceBottom && c <= faceLeft && c >= faceRight)
                {
                    bg.data[index] = 125;
                    bg.data[index + 1] = 125;
                    bg.data[index + 2] = 125;
                }

            }
        }
    }
}

void imageUtil::drawFace270(Mat bg, Mat mask, int faceLeft, int faceTop, int faceRight, int faceBottom, int red, int green, int blue)
{
    int WIDTH = bg.cols, HEIGHT = bg.rows;

    int channels = bg.channels();
    for (int r = 0, index, index2; r < HEIGHT; r++)
    {
        index = WIDTH * r * channels;
        index2 = WIDTH * r;
        for (int c = 0; c < WIDTH; c++, index += channels, index2++)
        {
//            if (mask.data[index2] == 255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//            else
//            {
//                bg.data[index] = 100;
//                bg.data[index + 1] = 100;
//                bg.data[index + 2] = 100;
//            }

//             we get the image rotated, and so the face detection is rotated as well
//
//              faceRight -> top
//              faceTop   -> left
//              faceLeft  -> bottom
//              faveBottom-> right


            if(faceLeft!=0)
            {
                if (r == faceRight && c >= faceTop && c <= faceBottom)
                {
                    bg.data[index] = red;
                    bg.data[index + 1] = green;
                    bg.data[index + 2] = blue;
                }

                if (c == faceTop && r >= faceLeft && r <= faceRight)
                {
                    bg.data[index] = red;
                    bg.data[index + 1] = green;
                    bg.data[index + 2] = blue;
                }

                if (r == faceLeft && c >= faceTop && c <= faceBottom)
                {
                    bg.data[index] = red;
                    bg.data[index + 1] = green;
                    bg.data[index + 2] = blue;
                }

                if (c == faceBottom && r >= faceLeft && r <= faceRight)
                {
                    bg.data[index] = red;
                    bg.data[index + 1] = green;
                    bg.data[index + 2] = blue;
                }

            }



//            if(faceLeft!=0)
//            {
//                if (c == faceRight && r >= faceTop && r <= faceBottom)
//                {
//                    bg.data[index] = red;
//                    bg.data[index + 1] = green;
//                    bg.data[index + 2] = blue;
//                }
//
//                if (r == faceTop && c >= faceLeft && c <= faceRight)
//                {
//                    bg.data[index] = red;
//                    bg.data[index + 1] = green;
//                    bg.data[index + 2] = blue;
//                }
//
//                if (c == faceLeft && r >= faceTop && r <= faceBottom)
//                {
//                    bg.data[index] = red;
//                    bg.data[index + 1] = green;
//                    bg.data[index + 2] = blue;
//                }
//
//                if (r == faceBottom && c >= faceLeft && c <= faceRight)
//                {
//                    bg.data[index] = red;
//                    bg.data[index + 1] = green;
//                    bg.data[index + 2] = blue;
//                }
//
//            }




//            if(faceLeft!=0)
//            {
//                if (r == faceRight && c >= faceTop && c <= faceBottom)
//                {
//                    bg.data[index] = red;
//                    bg.data[index + 1] = green;
//                    bg.data[index + 2] = blue;
//                }
//
//                if (c == faceTop && r >= faceLeft && r <= faceRight)
//                {
//                    bg.data[index] = red;
//                    bg.data[index + 1] = green;
//                    bg.data[index + 2] = blue;
//                }
//
//                if (r == faceLeft && c >= faceTop && c <= faceBottom)
//                {
//                    bg.data[index] = red;
//                    bg.data[index + 1] = green;
//                    bg.data[index + 2] = blue;
//                }
//
//                if (r == faceBottom && c >= faceLeft && c <= faceRight)
//                {
//                    bg.data[index] = red;
//                    bg.data[index + 1] = green;
//                    bg.data[index + 2] = blue;
//                }
//
//            }


        }
    }
}

bool imageUtil::turnOnFaceDetection()
{
    return faceCascade.load(haarCascadeFilePath);
}

Rect imageUtil::getFace(Mat& dss)
{
    faceCascade.detectMultiScale( dss, faces, 1.1, 2, HaarOptions, faceSizeKernel );

//    __android_log_print(ANDROID_LOG_ERROR, "tag", "1SimLe---------v43>2>sssss> %d   --  %d   ",faces.size(), faces.size());

    if(faces.size() > 0)
        return faces[0];
    return Rect();
}

Rect imageUtil::getFace270(Mat& dss)
{
    Mat rotated;
//    resize(dss, rotated, Size(WIDTH / 2, HEIGHT / 2));

    Mat mask(dss.size(), 0);

    cvtColor(dss, mask, COLOR_RGBA2GRAY);
    equalizeHist(mask, mask);

//    transpose(mask, rotated);

    rotateVideo(mask, rotated, 90);

    Rect face = getFace(rotated);
    rotated.release();

//    if(face.x != 0)
//    {
////        int t = face.x;
////        face.x = face.y;
////        face.y = t;
////
////        t = face.height;
////        face.height = face.width;
////        face.height = t;
//    }
    return face;
}

void imageUtil::initDss(Mat& dss)
{
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>dss cols rows> before resize> %d   -   %d ", dss.cols, dss.rows);

    Mat rDss;
    resize(dss, rDss, CAMERA_RESIZE_SIZE, 0, 0, INTER_NEAREST);
    dss.release();
    dss = rDss;

    if(cameraRotation == 90)
    {
        Mat flipped;
        flip(dss, flipped, -1);
        dss.release();
        dss = flipped;
    }
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>dss cols rows> after resize> %d   -   %d ", dss.cols, dss.rows);
    Mat dst;
    cropImage(dss, dst, 0, 0, HEIGHT, WIDTH);
    dss.release();
    dss = dst;
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>dss cols rows> after crop> %d   -   %d ", dss.cols, dss.rows);
}

//       int RES_960X720 = 1, RES_1280X960 = 2, RES_1280_720 = 3, RES_1024X768 = 4, RES_640X480 = 5, RES_CUSTOM = 0;

void imageUtil::setImageWidthHeight(int cWidth, int cHeight)
{
//    int resizedWidth, resizedHeight;
//
//    if(isLiteVideo)
//    {
//        switch (cameraRes)
//        {
//            case 1://RES_960X720
//                CAMERA_HEIGHT = 960;
//                CAMERA_WIDTH = 720;
//                resizedHeight = 426;
//                resizedWidth = 320;
//                CAMERA_RESIZE_SIZE = Size(426, 320);
//                break;
//            case 2://RES_1280X960
//                CAMERA_HEIGHT = 1280;
//                CAMERA_WIDTH = 960;
//                resizedHeight = 426;
//                resizedWidth = 320;
//                CAMERA_RESIZE_SIZE = Size(426, 320);
//                break;
//            case 3://RES_1280_720
//                CAMERA_HEIGHT = 1280;
//                CAMERA_WIDTH = 720;
//                resizedHeight = 568;
//                resizedWidth = 320;
//                CAMERA_RESIZE_SIZE = Size(568, 320);
//                break;
//            case 4://RES_1024X768
//                CAMERA_HEIGHT = 1024;
//                CAMERA_WIDTH = 768;
//                resizedHeight = 426;
//                resizedWidth = 320;
//                CAMERA_RESIZE_SIZE = Size(426, 320);
//                break;
//            case 5:
//                CAMERA_HEIGHT = 640;
//                CAMERA_WIDTH = 480;
//                resizedHeight = 426;
//                resizedWidth = 320;
//                CAMERA_RESIZE_SIZE = Size(426, 320);
//                break;
//            case 0:
//            default:
//                CAMERA_HEIGHT = cHeight;
//                CAMERA_WIDTH = cWidth;
//                resizedHeight = 320.0f * ((float)cWidth / (float)cHeight);
//                resizedWidth = 320;
//                CAMERA_RESIZE_SIZE = Size(resizedHeight, 320);
//                break;
//        }
//
//
//        WIDTH = 270;
//        HEIGHT = 320;
//        CAMERA_PADDING = (resizedHeight - HEIGHT) / 2;
//        WIDTH_X_4 = WIDTH * 4;
//        WIDTH_X_3 = WIDTH * 3;
//
//        MAX_SIZE = WIDTH * HEIGHT;
//        MAX_SIZE_X_4 = WIDTH * HEIGHT * 4;
//        MAX_SIZE_X_3 = WIDTH * HEIGHT * 3;
//
//        defMatSize = Size(WIDTH, HEIGHT);
//
//        CAMERA_DIFFERENCE = (CAMERA_WIDTH - WIDTH);// / 2
//    }
//    else
//    {
//        switch (cameraRes)
//        {
//            case 1://RES_960X720
//                CAMERA_HEIGHT = 960;
//                CAMERA_WIDTH = 720;
//                resizedHeight = 640;
//                resizedWidth = 480;
//                CAMERA_RESIZE_SIZE = Size(640, 480);
//                break;
//            case 2://RES_1280X960
//                CAMERA_HEIGHT = 1280;
//                CAMERA_WIDTH = 960;
//                resizedHeight = 640;
//                resizedWidth = 480;
//                CAMERA_RESIZE_SIZE = Size(640, 480);
//                break;
//            case 3://RES_1280_720
//                CAMERA_HEIGHT = 1280;
//                CAMERA_WIDTH = 720;
//                resizedHeight = 853;
//                resizedWidth = 480;
//                CAMERA_RESIZE_SIZE = Size(853, 480);
//                break;
//            case 4://RES_1024X768
//                CAMERA_HEIGHT = 1024;
//                CAMERA_WIDTH = 768;
//                resizedHeight = 640;
//                resizedWidth = 480;
//                CAMERA_RESIZE_SIZE = Size(640, 480);
//                break;
//            case 5:
//                CAMERA_HEIGHT = 640;
//                CAMERA_WIDTH = 480;
//                resizedHeight = 640;
//                resizedWidth = 480;
//                CAMERA_RESIZE_SIZE = Size(640, 480);
//                break;
//            case 0:
//            default:
//                CAMERA_HEIGHT = cHeight;
//                CAMERA_WIDTH = cWidth;
//                resizedHeight = 480.0f * ((float)cWidth / (float)cHeight);
//                resizedWidth = 480;
//                CAMERA_RESIZE_SIZE = Size(resizedHeight, 480);
//                break;
//        }
//
//
//        WIDTH = 360;
//        HEIGHT = 480;
//        CAMERA_PADDING = (resizedHeight - HEIGHT) / 2;
//        WIDTH_X_4 = WIDTH * 4;
//        WIDTH_X_3 = WIDTH * 3;
//
//        MAX_SIZE = WIDTH * HEIGHT;
//        MAX_SIZE_X_4 = WIDTH * HEIGHT * 4;
//        MAX_SIZE_X_3 = WIDTH * HEIGHT * 3;
//
//        defMatSize = Size(WIDTH, HEIGHT);
//
//        CAMERA_DIFFERENCE = (resizedWidth - WIDTH);// / 2
//    }
}

/**
 * mask - binary array
 */
void imageUtil::fadeEdges(Mat& mask)
{
    int width = mask.cols, height = mask.rows;
    uchar* data = mask.data;

    bool inShape = false;

    for(int c = width, index = c, decInc; c != 0; c--, index = c, inShape = false)
    {
        for (int r = 0; r < height; r++, index += width)
        {
            if(inShape)
            {
//                if(data[index] != WHITE_PIXEL)
                if(data[index] == 0)
                {
                    r -= FADE_THRESHOLD;

                    if(r < 0)
                    {
                        r = 0;
                        index = c;
                    }
                    else index -= (FADE_THRESHOLD * width);

                    inShape = false;
                    decInc = WHITE_PIXEL;
                    if(r < height)
                    {
                        for (int f = FADE_THRESHOLD; r < height && f >= 0; f--)
                        {
                            r++;
                            index += width;
                            decInc -= FADE_DECREASER;
//                                if(decInc < 0)
//                                    decInc = 0;
//                                __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>333333>> %d   -   %d  >> %d
                            data[index] = decInc;
                        }
                    }
                }
            }
            else if(data[index] == WHITE_PIXEL)
            {
                decInc = 0;
                inShape= true;
                data[index] = decInc;
                if(r < height)
                {
                    for (int f = 0; r < height && f <= FADE_THRESHOLD; f++)
                    {
                        r++;
                        index += width;
                        decInc += FADE_DECREASER;
                        data[index] = decInc;
                    }
                }

            }
        }
    }
}

/**
 * mask - binary array
 */
void imageUtil::fadeEdgesVertically(Mat& mask)
{
    int width = mask.cols, height = mask.rows;
    uchar* data = mask.data;

    bool inShape = false;

    for (int r = 0, index = (r * width) + width, decInc; r < height; r++, index = (r * width) + width, inShape = false )
    {//make r = 1 ...binary array!!
        for(int c = width; c != 0; c--, index--)
        {
            if(inShape)
            {
//                if(data[index] != WHITE_PIXEL)
                if(data[index] == 0)
                {
                    r -= FADE_THRESHOLD;

                    if(r < 0)
                    {
                        r = 0;
                        index = c;
                    }
                    else index -= (FADE_THRESHOLD * width);

                    inShape = false;
                    decInc = WHITE_PIXEL;
                    if(r < height)
                    {
                        for (int f = FADE_THRESHOLD; r < height && f >= 0; f--)
                        {
                            r++;
                            index += width;
                            decInc -= FADE_DECREASER;
//                                if(decInc < 0)
//                                    decInc = 0;
//                                __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>333333>> %d   -   %d  >> %d
                            data[index] = decInc;
                        }
                    }
                }
            }
            else if(data[index] == WHITE_PIXEL)
            {
                decInc = 0;
                inShape= true;
                data[index] = decInc;
                if(r < height)
                {
                    for (int f = 0; r < height && f <= FADE_THRESHOLD; f++)
                    {
                        r++;
                        index += width;
                        decInc += FADE_DECREASER;
                        data[index] = decInc;
                    }
                }

            }
        }
    }

}

void imageUtil::saveWhitePoint(Mat& dss)
{
    int sum = 0;
    uchar* data = dss.data;
    for (int c = WIDTH, channels = dss.channels(), index = c * channels, decInc; c != 0; c--, index = c * channels)
    {
        for (int r = 0; r < HEIGHT; r++, index += WIDTH_X_4)
        {
            if((data[index] & 0xFF) + (data[index + 1] & 0xFF) + (data[index + 2] & 0xFF) > sum)
            {
                sum = (data[index] & 0xFF) + (data[index + 1] & 0xFF) + (data[index + 2] & 0xFF);
                strongestRed = data[index + 2] & 0xFF;
                strongestGreen = data[index + 1] & 0xFF;
                strongestBlue = data[index] & 0xFF;
            }
        }
    }

    // max : 255 * 3 = 765
}

void imageUtil::setBrightnessII(Mat &dss, int db)
{
    dss += Scalar(db, db, db, 0);
    return;
    
    int sum = 0;
    uchar *data = dss.data;

    int WIDTH = dss.cols, WIDTH_X_4 = WIDTH * dss.channels(), HEIGHT = dss.rows;

    int red, green, blue;
    for (int c = WIDTH, channels = dss.channels(), index = c * channels, decInc; c != 0; c--, index = c * channels)
    {
        for (int r = 0; r <= HEIGHT; r++, index += WIDTH_X_4)
        {

//            data[index] = (data[index] & 0xFF) + db;
//            if(data[index] < 0)
//                data[index] = 0;
//            else if(data[index] > WHITE_PIXEL)
//                data[index] =  WHITE_PIXEL;


            if(data[index] + db < 0)
                data[index] = 0;
            else if(data[index] + db > WHITE_PIXEL)
                data[index] =  WHITE_PIXEL;
            else data[index] += db;


            if(data[index + 1] + db < 0)
                data[index + 1] = 0;
            else if(data[index + 1] + db > WHITE_PIXEL)
                data[index + 1] =  WHITE_PIXEL;
            else data[index + 1] += db;


            if(data[index + 2] + db < 0)
                data[index + 2] = 0;
            else if(data[index + 2] + db > WHITE_PIXEL)
                data[index + 2] =  WHITE_PIXEL;
            else data[index + 2] += db;


            
//            data[index] = (data[index] & 0xFF) - (data[index] & 0xFF);
//            data[index + 1] = (data[index + 1] & 0xFF) - (data[index + 1] & 0xFF);
//            data[index + 2] = (data[index+ 2] & 0xFF) - (data[index+ 2] & 0xFF);
//
//            data[index] = db;
//            data[index + 1] = (data[index + 1] & 0xFF) - (data[index + 1] & 0xFF);
//            data[index + 2] =

//            data[index + 1] = (data[index + 1] & 0xFF) + db;
//            if(data[index + 1] < 0)
//                data[index + 1] = 0;
//            else if(data[index + 1] > WHITE_PIXEL)
//                data[index + 1] =  WHITE_PIXEL;
//
//            data[index + 2] = (data[index + 2] & 0xFF) + db;
//            if(data[index + 2] < 0)
//                data[index + 2] = 0;
//            else if(data[index + 2] > WHITE_PIXEL)
//                data[index + 2] =  WHITE_PIXEL;


//            if ((data[index] & 0xFF) + (data[index + 1] & 0xFF) + (data[index + 2] & 0xFF) > sum)
//            {
//                sum = (data[index] & 0xFF) + (data[index + 1] & 0xFF) + (data[index + 2] & 0xFF);
//                strongestRed = data[index + 2] & 0xFF;
//                strongestGreen = data[index + 1] & 0xFF;
//                strongestBlue = data[index] & 0xFF;
//            }
        }
    }
    
}



//Mat imageUtil::resizeAccordingToWidth(Mat src, int width, int height)
//{
//    if(src.cols != width)
//    {
//        if(width != WIDTH)
//        {
//            float scaleFactor = ((float)width) / ((float)src.cols);
//            int newHeight = (int) ( ((float)src.rows) * scaleFactor );// + 40;
////            setImageWidthHeight(width + 30, newHeight, scaleFactor);
//
////            setImageWidthHeight(width, newHeight, scaleFactor);
//
//            setImageWidthHeight(width, height, scaleFactor);
//        }
//
////        cv::Rect roi;
////        roi.x = 0;//(src.cols - width) / 2;//(difx - 640)/2;
////        roi.y = 0;
////        roi.width = 360;
////        roi.height = 480;//height;// img.size().height - (offset_y*2);
//
//        /* Crop the original image to the defined ROI */
//
////        Mat retMat;
////        retMat = cropImage()src(roi);
//
//
//
////        resize(src, retMat, Size(WIDTH, HEIGHT));
////        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>333333>> %d   -   %d  >> %d   -   %d  >> %d   -   %d", width, height, src.cols, src.rows, retMat.cols, retMat.rows);
//
////        Mat cropped = retMat()
//
//        return cropImage(src, 0, 0, height, width);
//    }
//    return src;
//}


////////////////////////////////////////////////////////////////////////////////





//void imageUtil::mergeBuffers(Mat src1, Mat src2, Mat dst)//(uchar* src1, uchar* src2, uchar* dst)
//{
//
////    for(int i=0;i<100;i++)
////    {
////        for(int l=0;l<200;l++)
////        {
////            dst.data[(i*600) + (l*3)] = 255;
////            dst.data[(i*600) + (l*3) + 1] = 255;
////        }
////    }
//
//    int width1 = src1.cols, height1 = src1.rows, inc1 = src1.channels() * width1;
//    int width2 = src2.cols, height2 = src2.rows, inc2 = src2.channels() * width2;
//    int widthDst = dst.cols, heightDst = dst.rows, incDst = dst.channels() * widthDst;
//
//    for(int c = widthDst, index1, index2, indexDst ; c > 0; c--)
//    {
//        index1 = c * src1.channels();
//        index2 = c * src2.channels();
//        indexDst = c * dst.channels();
//        for (int r = 0; r < heightDst; r++, index1 += inc1, index2 += inc2, indexDst += incDst)
//        {
//            if (c < width1 && r < height1)
//            {
//                dst.data[indexDst] = src1.data[index1];
//                dst.data[indexDst + 1] = src1.data[index1 + 1];
//                dst.data[indexDst + 2] = src1.data[index1 + 2];
//            }
////
////            __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLevaaa>2>> %d   -   %d ====   %d   -   %d", c, r, incDst, dst.channels());
//            if(c < width2 && r < height2)
//            {
//                dst.data[indexDst] = src2.data[index2];
//                dst.data[indexDst + 1] = src2.data[index2 + 1];
//                dst.data[indexDst + 2] = src2.data[index2 + 2];
//            }
//
////            dst.data[indexDst] = 0;
////            dst.data[indexDst + 1] = 255;
////            dst.data[indexDst + 2] = 0;
//        }
//    }
//}


void imageUtil::noiseCleaninig(Mat& mask, cv::Rect face) {
    
    Mat labels, stats, centroid;
    std::vector<int> areasToShow, noiseArea;
    int faceSize = face.width * face.height;
    int areasNum = connectedComponentsWithStats(mask, labels, stats, centroid, 4);
    for (int i = 1; i < areasNum; i++) {
        int area = stats.at<int>(i, CC_STAT_AREA);
        if (area < faceSize / 4) {
            noiseArea.push_back(i);
        } else {
            areasToShow.push_back(i);
        }
        
    }
    
    //draw the camera on the bg using the masks as references
    
    //    iUtil.mergeBufferAccordingToBuffers(dss, edgesBuffer, chromaKeyBuffer, tbg);
    Mat rangeMat = Mat::zeros(mask.size(), mask.type());
    
    for (int i = 0; i < areasToShow.size(); i++) {
        Mat tmpRange;
        inRange(labels, Scalar(areasToShow.at(i)), Scalar(areasToShow.at(i)), tmpRange);
        bitwise_or(tmpRange, rangeMat, rangeMat);
    }
    
    mask = rangeMat * 255;
    
}

void imageUtil::keepHeadFull(Mat& dss, Mat& mask, cv::Rect face) {
    
    int left = face.x /*- (face.width / 10)*/ > 0 ? face.x /* - (face.width / 10) */: 0;
    int top = WIDTH - (face.y )/*+ (face.height ))*/ > 0 ? WIDTH - (face.y)/* + (face.height )) */: 0;// + face.height / 5;//* 2 / 3));
    int right = face.x + face.width /*+ face.width / 5*/; if (right > HEIGHT) right = HEIGHT;
    int bottom =  top - face.height /*+ face.height / 5*/; if (bottom > WIDTH) bottom = WIDTH;// - face.height / 5;
    
    
    //    for (int i = top; i < bottom; i++) {
    //        bool breakJ = false, breakK = false;
    //        for (int j = left, k = right; j < (left + right) / 2 + 1 ; j++, k--){
    //           // printf ("\ni: %d j: %d, top: %d, bottom: %d, right:%d, left: %d", i,j,top,bottom, right, left);
    //            //if (breakJ)
    //               mask.at<uchar>(j, i) = WHITE_PIXEL;
    //            //else if (mask.at<uchar>(j, i) == WHITE_PIXEL)
    //            //    breakJ = true;
    //
    //            //if (breakK)
    //                mask.at<uchar>(k, i) = WHITE_PIXEL;
    //            //else if (mask.at<uchar>(k, i) == WHITE_PIXEL)
    //                //breakK = true;
    //        }
    //    }
    
    cv::Rect faceTRect = cv::Rect(WIDTH - face.y - face.height, face.x, face.height, face.width);
    //rectangle(mask, cv::Point(faceTRect.x, faceTRect.y), cv::Point(faceTRect.x + faceTRect.width, faceTRect.y + faceTRect.height), Scalar(255), 1);
    
    
    Mat ellipseBoundingMat(cv::Size(face.height + 2*face.height/3,face.width), CV_8U);
    //circle(ellipseBoundingMat, cv::Point(ellipseBoundingMat.rows/2, ellipseBoundingMat.cols/2), faceTRect.width/2, Scalar(255), -1);
    ellipse(ellipseBoundingMat, RotatedRect(cv::Point(0,0), cv::Point(ellipseBoundingMat.cols -1,0), cv::Point(ellipseBoundingMat.cols - 1, ellipseBoundingMat.rows - 1)), Scalar(255), -1);
    //    ellipse(mask, RotatedRect(cv::Point((WIDTH - face.y + face.height/3) < 480 ? WIDTH - face.y + face.height/3  : 480 , face.x),
    //                              cv::Point((WIDTH - face.y + face.height/3) < 480 ? WIDTH - face.y + face.height/3  : 480 , (face.x + face.width) > HEIGHT ? HEIGHT : face.x + face.width),
    //                              cv::Point((WIDTH - face.y - face.height - face.height/3) > 0 ? (WIDTH - face.y - face.height - face.height/3) : 0, (face.x + face.width) > HEIGHT ? HEIGHT : face.x + face.width)),
    //            Scalar(255), -1);
    
    const static int lowThresh = 20, maxThresh = 60;
    const static int kerSize = 3;
    
    cv::Rect faceForEdgeRect = faceTRect;
    faceForEdgeRect.width = faceForEdgeRect.x + 3*faceForEdgeRect.width/2 > WIDTH ? WIDTH - faceForEdgeRect.x : 3*faceForEdgeRect.width/2;
    faceForEdgeRect.y = faceForEdgeRect.y - faceForEdgeRect.height/10 > 0 ? faceForEdgeRect.y - faceForEdgeRect.height/10 : 0;
    faceForEdgeRect.height = faceForEdgeRect.y + faceForEdgeRect.height + 2*faceForEdgeRect.height/10 > HEIGHT ? HEIGHT - faceForEdgeRect.y : faceForEdgeRect.height + 2*faceForEdgeRect.height/10;
    Mat faceOnlyMat = dss(faceForEdgeRect);
    cvtColor(faceOnlyMat, faceOnlyMat, CV_RGBA2GRAY);
    Mat faceBlurredMat, faceEdges;
    GaussianBlur(faceOnlyMat, faceBlurredMat, cv::Size(3,3), 3);
    Canny(faceBlurredMat, faceEdges, lowThresh, maxThresh, kerSize);
    //imageFull = faceEdges; return;

    for (int i = faceForEdgeRect.x, ied = 0; i < faceForEdgeRect.x + faceTRect.width; i++, ied++) {
        bool breakJ = false, breakK = false;
        for (int j = faceForEdgeRect.y, jed = 0, k = faceForEdgeRect.y + faceForEdgeRect.height, ked = faceForEdgeRect.height - 1; j < (faceForEdgeRect.y + faceForEdgeRect.height / 2) + 1 ; j++, jed++, k--, ked--){
            // printf ("\ni: %d j: %d, top: %d, bottom: %d, right:%d, left: %d", i,j,top,bottom, right, left);
            if (breakJ)
                mask.at<uchar>(j, i) = WHITE_PIXEL;
            else if (faceEdges.at<uchar>(jed, ied) == WHITE_PIXEL)
                breakJ = true;
            
            if (breakK)
                mask.at<uchar>(k, i) = WHITE_PIXEL;
            else if (faceEdges.at<uchar>(ked, ied) == WHITE_PIXEL)
                breakK = true;
        }
    }
    
    for (int r = faceForEdgeRect.y, red = 0; r < faceForEdgeRect.y + faceForEdgeRect.height; r++, red++) {
        bool breakC = false;
        for (int c = faceForEdgeRect.x + faceForEdgeRect.width, ced = faceEdges.cols - 1; c > faceForEdgeRect.x + faceTRect.width; c--, ced--) {
            if (breakC)
                mask.at<uchar>(r, c) = WHITE_PIXEL;
            else if (faceEdges.at<uchar>(red, ced) == WHITE_PIXEL)
                breakC = true;
        }
    }

    //faceRectMat.copyTo(faceAreaOnMask);
    
    /*
    //timage.copyTo(croppedBG, mask);
    
    int ebrX = WIDTH - face.y - face.height - face.height/3 < 0 ? 0 : WIDTH - face.y - face.height - face.height/3;
    int ebrXGap = (ebrX - (WIDTH - face.y - face.height - face.height/3)) ;
    int ebrY = face.x;
    int ebrYGap = 0;
    int ebrWidth = ebrX + face.height + 2*face.height/3 > WIDTH ? WIDTH - ebrX - ebrXGap : face.height + 2*face.height/3 - ebrXGap;
    int ebrWidthGap = ebrX + face.height + 2*face.height/3  - WIDTH< 0 ? 0 : ebrX + face.height + 2*face.height/3  - WIDTH;
    int ebrHeight = face.width;
    int ebrHeightGap = 0;
    
    cv::Rect ellipseBoundingRect(ebrX, ebrY, ebrWidth, ebrHeight);
    
    for (int i = ellipseBoundingRect.y, iel = 0; i < ellipseBoundingRect.y + ellipseBoundingRect.height; i++, iel++) {
        for (int j = ellipseBoundingRect.x, jel = ebrXGap; j < ellipseBoundingRect.x + ellipseBoundingRect.width; j++, jel++) {
            if (mask.at<uchar>(i,j) == 0 && ellipseBoundingMat.at<uchar>(iel, jel) == 255) {
                mask.at<uchar>(i,j) = 64;// 0.5 * tbg.at<Vec4b>(i,j)[0] + 0.5 * timage.at<Vec4b>(i,j)[0];
                
            }
        }
        
    }*/
    
}

Mat imageUtil::reduceWhitePoint(Mat& dss, cv::Rect face) {
    
//    int localWhitePoint = findWhitePoint(dss);
//    if (whitePointAverage == 0) {
//        whitePointAverage = localWhitePoint;
//    }
//    int whitePointFix = whitePointAverage - localWhitePoint;
    int whitePointFix = whitePointAverage - findPixelAverage(dss);
    return dss + Scalar(whitePointFix, whitePointFix, whitePointFix, 0);

}

int imageUtil::findPixelAverage(const Mat src) {
    
    Mat fixedSrc;
    src.convertTo(fixedSrc, CV_16UC4);
    std::vector<Mat> splittedMat;
    split(fixedSrc, splittedMat);
    
    Mat totalMat = splittedMat[0] + splittedMat[1] + splittedMat[2];    
    Scalar sc = mean(totalMat);
    float meanPoint = sc[0];
    
    
    return (int)(meanPoint / 3);
    
}

void imageUtil::rotateVideo(Mat &dss, Mat &dst, int angle)
{
    if(angle == 90)
    {
        dst = Mat(Size(dss.rows, dss.cols), dss.type());

        int channels = dss.channels();
        int widthSrc = dss.cols, WidthX4Src = widthSrc * channels;
        int widthDst = dst.cols, WidthX4Dst = widthDst * channels;
        int widthHeightX4Dst = dst.rows * dst.cols * channels;

        int heightSrc = dss.rows;

        uchar *srcData = dss.data;
        uchar *dstData = dst.data;

        for (int c = widthSrc - 1, indexSrc, indexDst; c >= 0; c--)
        {
            indexSrc = c * channels;
            indexDst = widthHeightX4Dst - (c * WidthX4Dst);

            for (int r = 0; r <= heightSrc; r++, indexSrc += WidthX4Src, indexDst -= channels)
            {
                for (int l = 0; l < channels; l++)
                    dstData[indexDst + l] = srcData[indexSrc + l];
            }
        }
    }

    if(angle == 270)
    {
        dst = Mat(Size(dss.rows, dss.cols), dss.type());

        int channels = dss.channels();
        int widthSrc = dss.cols, WidthX4Src = widthSrc * channels;
        int widthDst = dst.cols, WidthX4Dst = widthDst * channels;
        int widthHeightX4Dst = dst.rows * dst.cols * channels;

        int heightSrc = dss.rows;

        uchar *srcData = dss.data;
        uchar *dstData = dst.data;

        for (int c = widthSrc - 1, indexSrc, indexDst; c >= 0; c--)
        {
            indexSrc = c * channels;
            indexDst = c * WidthX4Dst;

            for (int r = 0; r <= heightSrc; r++, indexSrc += WidthX4Src, indexDst += channels)
            {
                for (int l = 0; l < channels; l++)
                    dstData[indexDst + l] = srcData[indexSrc + l];
            }
        }
    }

}


//cameraRes
//    RES_960X720 = 1;//, RES_1280X960 = 2, RES_1280_720 = 3, RES_1024X768 = 4;
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>isCameraSupports1280X960Res>tac> %d  ", cameraRes);
//    switch(cameraRes)
//    {
//        case 1:
//        case 2:
////            resize(dss, dss, CAMERA_RESIZE_SIZE, 0, 0, INTER_LINEAR);
//            resize(dss, dss, CAMERA_RESIZE_SIZE, 0, 0, INTER_NEAREST);
//            break;
////            return dss;
////            resize(dss , dss, CAMERA_RESIZE_SIZE, 0, 0, INTER_NEAREST);
////            return;
////            return dss;
//        case 3:
//            resize(dss, dss, Size(853, 480), 0, 0, INTER_NEAREST);
////            dss =  cropImage(dss, 0, 0, 640, 480);
//            break;
//    }
