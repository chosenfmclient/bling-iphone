//
//  SIALoadingTableViewCell.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 6/16/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIAShadowTableCell.h"

@interface SIALoadingTableViewCell : SIAShadowTableCell

@property (strong, nonatomic) UIImageView *loadingSpinner;
@property (strong, nonatomic) UIColor *spinnerTintColor;
@property (nonatomic) BOOL unroundCorners;
-(void)removeLoadingAnimation;

@end

@interface SIALoadingTableViewCellBlack : SIALoadingTableViewCell
@end
