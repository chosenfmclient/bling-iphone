//
//  FacebookManager.h
//  SingrIphoneApp
//
//  Created by Roni Shoham on 23/12/13.
//  Copyright (c) 2013 SingrFM. All rights reserved.
//

#import <Accounts/Accounts.h>
#import <UIKit/UIKit.h>
#import <Accounts/Accounts.h>

// DEFINES
//permissions
#define FB_PUBLIC_PROFILE_PERMISSION_STR    @"public_profile"
#define FB_EMAIL_PERMISSION_STR             @"email"
#define FB_USER_FRIENDS_PERMISSION_STR      @"user_friends"

#define FULL_PERMISSIONS                    (4)
#define NO_PUBLIC_PERMISSIONS               (3)
#define NO_EMAIL_PERMISSIONS                (2)
#define NO_FRIENDS_PERMISSIONS              (1)
#define NO_PERMISSIONS                      (0)


@interface FacebookManager : NSObject <UIAlertViewDelegate>

@property (nonatomic, strong) NSString *onboardingPhase;
@property (nonatomic, strong) NSString *enableSignIn;
@property (nonatomic, strong) NSString *promoCode;

@property (nonatomic , strong) ACAccountStore *account;
@property (nonatomic , strong) ACAccount *facebookAccount;

@property (nonatomic) BOOL connectionFailed;
+ (instancetype) sharedInstance;
-(void) sendPost:(UIViewController*)view picture:(UIImage *)image;

-(void) clearDefaults;

+(void) FBLogin;
-(void) FBLoginFromVC:(UIViewController *)vc
          withHandler:(void (^)(BOOL)) handler;
-(void) disconnect;
-(void) findFriends:(void (^)(NSMutableArray*)) friendListArr;
-(void) share;

-(void)attemptRenewCredentials:(void (^)(NSError* error))completed;
+(NSString *)userId;
-(void) requestUserFriendsPermission:(void (^)(BOOL)) permissionGranted;
+ (void) requestPublishPermissions:(void (^)(BOOL))permissionGranted;

@end
