//
//  STVGuiParser.h
//  SingrTV
//
//  Created by Lior Lasry on 20/02/13.
//  Copyright (c) 2013 Lev G. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 for Webview users
 */
#define STV_PERSONAL_BOARD_TIME_ALL     @""
#define STV_PERSONAL_BOARD_TIME_DAY     @"day"
#define STV_PERSONAL_BOARD_TIME_WEEK    @"week"
#define STV_PERSONAL_BOARD_TIME_MONTH   @"month"
@interface SIANetworking : NSObject 
{ 
@private

}


+(void) showAlert:(NSString*) inTitle  message:(NSString*) inMessage;
+(BOOL) wifiAvailable;
+(BOOL) internetConnectionAvailable;
+(void) handleJsonError: (NSError*)err withJson:(NSString*)json;
+(id) getJSON:(NSString*)url ;
+(void) getAsset:(NSString*)stringURL assetName:(NSString*)nameStr;
+(NSData *) base64DecodeString: (NSString *) strBase64;
+(NSString *) base64EncodeString: (NSString *) strData;
+ (NSString *) base64EncodeData: (NSData *) objData ;
+(BOOL)checkConnection;

@end



