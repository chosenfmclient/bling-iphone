//
//  SIAFollowingViewController.h
//  ChosenIphoneApp
//
//  abstract : responsible to bring the data of the user followings
//  and to show this data to the user.
//  if the user that entered the page is the user that owns the page
//  he can change following/unfollowing status
//
//  Created by Roni Shoham on 09/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIATableViewContainedViewController.h"
#import "SIAUserTableViewCell.h"
#import "SIAFollowingDataModel.h"

@interface SIAFollowingViewController : SIATableViewContainedViewController

@property (strong, nonatomic) NSString *followTitle;
@property (strong, nonatomic) SIAFollowListModel *model;
@property (strong, nonatomic) NSString *userId;
@property (weak, nonatomic) UIImage* backgroundImage;
@property (nonatomic) NSUInteger totalPeople;
@property (nonatomic) BOOL isMyProfile;
@property (nonatomic) BOOL needToRefresh;
@property (nonatomic) BOOL isFollowing;

@end

