//
//  SIAVideoLikesDataModel.h
//  
//
//  Created by Michael Biehl on 1/16/16.
//
//

#import <Foundation/Foundation.h>

@interface SIAVideoLikesDataModel : SIAPaginatorDataModel

@property (strong, nonatomic) NSString *videoId;
@property (strong, nonatomic) NSString *objectId;
- (instancetype) initWithObjectId:(NSString *)objectId;
@end
