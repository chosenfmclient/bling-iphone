//
//  SIAShadowTableCell.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 10/28/15.
//  Copyright © 2015 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIAShadowTableCell : UITableViewCell

@property (nonatomic) BOOL noTopShadow;
@property (nonatomic) BOOL noBottomShadow;

- (void) setShadow;
- (void) roundCorners:(NSInteger)corners;
- (void) setSeparator;
- (void) removeSeparator;
@end
