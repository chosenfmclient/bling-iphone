//
//  SIATextCommentView.m
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/9/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIATextCommentView.h"
#import "SIAPlaceholderTextView.h"

@interface SIATextCommentView () <UITextViewDelegate>
@property(nonatomic, readwrite) UILabel *label;
@property(nonatomic, readwrite) UIImageView *imageView;
@property(nonatomic, readwrite) SIAPlaceholderTextView *textView;
@property(nonatomic, readwrite) UIView *actionBar;
@property(nonatomic, readwrite) CGRect originalFrame;
@property(nonatomic, readwrite) UITapGestureRecognizer *resignGesture;
@property(nonatomic, readwrite) UITapGestureRecognizer *beginEditingGesture;

@property(nonatomic, readwrite) NSUInteger actionCount;

@end

@implementation SIATextCommentView
@dynamic image;
@synthesize keyboardAppearance = _keyboardAppearance;

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (instancetype)copy {
    NSData *temp = [NSKeyedArchiver archivedDataWithRootObject:self];
    SIATextCommentView *copy = [NSKeyedUnarchiver unarchiveObjectWithData:temp];

    return copy;
}

- (void)setupSubviews {
    self.floatTextInput = YES;
    self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.3];
    self.layer.cornerRadius = 0;
    self.backgroundIdleColor = [UIColor colorWithWhite:1.0 alpha:0.3];
    self.label = [[UILabel alloc] initWithFrame:CGRectZero];
    self.label.numberOfLines = 3;
    self.label.accessibilityLabel = @"comment_label";
    self.label.isAccessibilityElement = YES;
    self.label.textColor = [UIColor blackColor];
    self.label.font = kAppFontRegular(14);
    [self addSubview:self.label];
    
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    
    self.imageView.image = [UIImage imageNamed:@"extra_large_userpic_placeholder"];
    self.imageView.isAccessibilityElement = YES;
    self.imageView.clipsToBounds = YES;
    [self addSubview:self.imageView];
    
    [Utils dispatchOnMainThread:^{
        self.beginEditingGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(beginCommentEditing)];
        [self addGestureRecognizer:self.beginEditingGesture];
    }];
}

#pragma mark - SIATextCommentView

- (void)setName:(NSString *)name {
    _name = name;
    
    self.label.attributedText = [self _attribString];
    [self setNeedsLayout];
}

- (void)setComment:(NSString *)comment {
    _comment = comment;

    self.label.attributedText = [self _attribString];
    [self setNeedsLayout];
}

- (void)setImage:(UIImage *)image {
    self.imageView.image = image;
}

- (void)setCommentColor:(NSString *)commentColor
{
    _commentColor = commentColor;

    UIColor *color = [[UIColor class] performSelector:NSSelectorFromString([NSString stringWithFormat:@"%@Color", commentColor])];

    self.label.attributedText = [self _attribString];
    self.label.textColor = color;
}

- (void)setName:(NSString*)name comment:(NSString*)comment color:(NSString*)commentColor
{
    _name = name;
    _comment = comment;
    _commentColor = commentColor;
    
    UIColor *color = [[UIColor class] performSelector:NSSelectorFromString([NSString stringWithFormat:@"%@Color", commentColor])];
    
    self.label.attributedText = [self _attribString];
    self.label.textColor = color;

    [self setNeedsLayout];
}

- (UIImage *)image {
    return self.imageView.image;
}

- (NSAttributedString *)_attribString {
    NSString *string = @"<span style=\"font-family:'Roboto-Regular'; font-size: 16px; color: %@\">%@</span>";
    
    NSString *finalString = [NSString stringWithFormat:string, self.commentColor?:@"black",[(self.comment?:@"") stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"]];
    
    NSAttributedString *attribString = [NSAttributedString sia_attributedStringWithHTML:finalString];
    
    return attribString;
}

#pragma mark - SIATextCommentView (Rects)

//- (void)setFrame:(CGRect)frame {
//    self.originalFrame = self.frame;
//    [super setFrame:frame];
//}

- (void)layoutSubviews {
    [UIView setAnimationsEnabled:NO];
    self.label.frame = [self labelRectForBounds:self.bounds];
    
    //    if(!self.isEditing)
    //    {
    self.imageView.frame = [self imageRectForBounds:self.bounds];
    self.imageView.layer.cornerRadius = 19;
    //    }
    
    self.textView.frame = [self textFieldRectForBounds:self.bounds];
    self.actionBar.frame = [self actionBarRectForBounds:self.bounds];
    
    [self.actionBar.subviews enumerateObjectsUsingBlock:^(UIView *actionButton, NSUInteger idx, BOOL *stop) {
        if (actionButton.tag == SIATextCommentActionSendType || actionButton.tag == SIATextCommentActionUpdateType) {
            actionButton.center = CGPointMake(CGRectGetWidth(self.actionBar.frame) - 0, 0);
        } else if (actionButton.tag == SIATextCommentActionCancelType) {
            //UIView *view = [self.actionBar viewWithTag:SIATextCommentActionSendType] ?: [self.actionBar viewWithTag:SIATextCommentActionUpdateType];
            //actionButton.center = CGPointMake(CGRectGetMinX(view.frame) - 10, 0);
        }
    }];
    [UIView setAnimationsEnabled:YES];
}

- (NSInteger)_padding {
    return 10;
}

- (CGSize)_imageSize {
    return CGSizeMake(38, 38);
}

- (CGSize)_maxSize {
    return CGSizeMake(280, 58);
}

- (CGRect)imageRectForBounds:(CGRect)bounds {
    CGSize imageSize = [self _imageSize];

    return CGRectMake(10, 10, imageSize.width, imageSize.height);
}

- (CGRect)labelRectForBounds:(CGRect)bounds {
    CGSize sizeConstraint = CGSizeMake(280, 58);
    CGRect requiredRect = [[self _attribString] boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return CGRectIntegral(CGRectMake([self _padding] + 10 + [self _imageSize].width, 10 , requiredRect.size.width, 38));
}

- (CGRect)textFieldRectForBounds:(CGRect)bounds {
    CGRect rect = [self labelRectForBounds:bounds];
    rect.size.width = bounds.size.width - 48 * (self.actionCount+1) - [self _padding];
    return CGRectIntegral(rect);
}

- (CGRect)actionBarRectForBounds:(CGRect)bounds {
    CGRect textFieldRect = [self textFieldRectForBounds:bounds];
    CGRect rect = CGRectMake(CGRectGetMaxX(textFieldRect), CGRectGetMinY([self imageRectForBounds:bounds]), 43 * self.actionCount, 20);
    
    return CGRectIntegral(rect);
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGRect labelRect = [self labelRectForBounds:CGRectMake(0, 0, size.width, size.height)];
    CGSize imageSize = [self _imageSize];
    CGSize maxSize = [self _maxSize];

    CGFloat height = MAX(58, labelRect.size.height + 20);
    CGFloat width = [self _padding] * 2 + imageSize.width + [self _padding] + labelRect.size.width;

    return CGSizeMake(MIN(width, maxSize.width), MIN(height, maxSize.height+20));
}

#pragma mark - SIATextCommentView (Edit)

- (void)addActionWithType:(SIATextCommentAction)action {
    if (!self.actionBar) {
        self.actionBar = [[UIView alloc] initWithFrame:CGRectZero];
        self.actionBar.hidden = YES;
        [self addSubview:self.actionBar];
        
        self.actionCount = 0;
    }

    self.actionCount++;

    UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    actionButton.layer.cornerRadius = 19;
    actionButton.layer.masksToBounds = YES;
    actionButton.layer.anchorPoint = CGPointMake(1, 0);

    NSString *title = @"";
    SEL selector = nil;
    BOOL enabled = YES;
    NSString *label = @"";
    UIImage *image = nil;
    
    switch (action) {
        case SIATextCommentActionSendType: {
            
            selector = @selector(_performSendAction);
            label = @"comment_action_send";
            enabled = NO;
            image = [UIImage imageNamed:@"icon_send.png"];
            break;
        }
        case SIATextCommentActionUpdateType: {
            
            selector = @selector(_performUpdateAction);
            enabled = NO;
            label = @"comment_action_update";
            image = [UIImage imageNamed:@"icon_send.png"];
            break;
        }
        case SIATextCommentActionCancelType: {
            title = @"Cancel";
            selector = @selector(_performCancelAction);
            label = @"comment_action_cancel";
            image = [UIImage imageNamed:@"icon_nav_close.png"];
            break;
        }
    }
    
    [actionButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    [actionButton setImage:image forState:(UIControlStateNormal)];
    [actionButton setTintColor:[UIColor whiteColor]];
    [actionButton setBackgroundImage:[SIAStyling imageWithColor:[UIColor colorWithRed:39. / 255 green:170. / 255 blue:225. / 255 alpha:1.]] forState:UIControlStateNormal];
    [actionButton setBackgroundImage:[SIAStyling imageWithColor:[UIColor colorWithRed:203. / 255 green:203. / 255 blue:203. / 255 alpha:1.]] forState:UIControlStateDisabled];
    [actionButton.titleLabel setFont:kAppFontItalic(12.0)];
    [actionButton setEnabled:enabled];
    [actionButton setTag:action];
    [actionButton setAccessibilityLabel:label];
    //    [actionButton sizeToFit];
    [actionButton setBounds:CGRectMake(0, 0, 38, 38)];
    [actionButton.layer setCornerRadius:19.0];
    
    [self.actionBar addSubview:actionButton];
    
    [self setNeedsLayout];
}

- (void)_performCancelAction {
    [self endCommentEditing];
}

- (void)_performUpdateAction {
    if ([self.delegate respondsToSelector:@selector(textCommentView:userDidSendComment:)])
        [self.delegate textCommentView:self userDidSendComment:self.textView.text];

    [self endCommentEditing];
}

- (void)_performSendAction {
    if ([self.delegate respondsToSelector:@selector(textCommentView:userDidSendComment:)])
        [self.delegate textCommentView:self userDidSendComment:self.textView.text];

    [self endCommentEditing];
}

- (void)beginCommentEditing {
    [self beginCommentEditingWithComment:nil];
}

- (void)beginCommentEditingWithComment:(NSString *)comment {
    BOOL begin = YES;
    if ([self.delegate respondsToSelector:@selector(textCommentViewShouldBeginEditing:)])
        begin = [self.delegate textCommentViewShouldBeginEditing:self];

    if (!begin)
        return;

    if (self.actionBar)
        self.actionBar.hidden = NO;

    [self setBackgroundColor:[UIColor whiteColor]];
    if (!self.textView) {
        self.textView = [[SIAPlaceholderTextView alloc] initWithFrame:CGRectZero];
        self.textView.delegate = self;
        self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textView.keyboardType = UIKeyboardTypeDefault;
        self.textView.keyboardAppearance = self.keyboardAppearance;
        self.textView.textContainer.maximumNumberOfLines = 2;
        self.textView.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;
        self.textView.accessibilityLabel = @"comment_textField";
        self.textView.placeholder = @"Say something...";
        self.textView.font = kAppFontRegular(14.0);
        self.textView.isAccessibilityElement = YES;
    }

    [self.textView setText:comment];
//    [self adjustContentSize:self.textView];

    [self addSubview:self.textView];
    
    [Utils dispatchOnMainThread:^{
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
        [self.textView becomeFirstResponder];
        
        _editing = YES;
        
        if ([self.delegate respondsToSelector:@selector(textCommentViewDidBeginEditing:)])
            [self.delegate textCommentViewDidBeginEditing:self];
    }];
}

- (void)endCommentEditing {
    _editing = NO;

    [self setBackgroundColor: self.backgroundIdleColor];
    [self.textView endEditing:NO];
    [self.textView setText:@""];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    if(CGRectEqualToRect(self.originalFrame, CGRectZero))
        self.originalFrame = self.frame;
    else
        self.frame = self.originalFrame;

    NSDictionary *userInfo = [notification userInfo];

    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];

    UIWindow *appWindow = [[[UIApplication sharedApplication] delegate] window];
    CGRect absoluteFrame = [self.superview convertRect:self.frame toView:appWindow];
    CGFloat totalHeight = 58 + CGRectGetHeight(keyboardFrame);
    
    absoluteFrame.origin.y = CGRectGetHeight(appWindow.frame) - totalHeight;
    [Utils dispatchOnMainThread:^{
        [UIView animateWithDuration:[[userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]
                              delay:0
                            options:[[userInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue]
                         animations:^{
                             CGRect frame = [appWindow convertRect:absoluteFrame toView:self.superview];
                             if (_floatTextInput) {
                                 frame.origin.y -= 0;
                             }
                             //                             frame.size.height += 20 + [self _padding] * 2;
                             self.frame = frame;
                             [self layoutIfNeeded];
                         }
                         completion:nil];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    if ([notification.name isEqualToString:@"UIApplicationWillEnterForegroundNotification"]) {
        [UIView setAnimationsEnabled:NO];
    }
    
    NSDictionary *userInfo = [notification userInfo];
    [Utils dispatchOnMainThread:^{
        [UIView animateWithDuration:[[userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]
                              delay:0
                            options:[[userInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue]
                         animations:^{
                                 self.frame = self.originalFrame;
                         }
                         completion:nil];
    }];
    
    [UIView setAnimationsEnabled:YES];
}

- (void)adjustContentSize:(UITextView *)tv {
    CGFloat deadSpace = ([tv bounds].size.height - [tv contentSize].height);
    CGFloat inset = MAX(0, deadSpace / 2.0);
    tv.contentInset = UIEdgeInsetsMake(inset, tv.contentInset.left, inset, tv.contentInset.right);
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(textCommentViewShouldBeginEditing:)])
        return [self.delegate textCommentViewShouldBeginEditing:self];

    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [Utils dispatchOnMainThread:^{
        self.resignGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_endCommentEditing:)];
        [[[UIApplication sharedApplication] keyWindow] addGestureRecognizer:self.resignGesture];
        
        [self removeGestureRecognizer:self.beginEditingGesture];
        
        UIButton *actionButton = [self.actionBar viewWithTag:SIATextCommentActionSendType] ?: [self.actionBar viewWithTag:SIATextCommentActionUpdateType];
        NSString *text = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [actionButton setEnabled:text.length > 0];
    }];
}

- (void)_endCommentEditing:(UITapGestureRecognizer *)gesture {
    CGPoint gestureLocation = [gesture locationInView:self.superview];
    if (!CGRectContainsPoint(self.frame, gestureLocation))
        [self endCommentEditing];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (self.actionBar)
        self.actionBar.hidden = YES;

    self.comment = textView.text;
    [self.textView removeFromSuperview];

    if ([self.delegate respondsToSelector:@selector(textCommentViewDidEndEditing:)])
        [self.delegate textCommentViewDidEndEditing:self];

    [self endCommentEditing];
    [Utils dispatchOnMainThread:^{
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [[[UIApplication sharedApplication] keyWindow] removeGestureRecognizer:self.resignGesture];
        
        [self addGestureRecognizer:self.beginEditingGesture];
    }];
}

- (void)textViewDidChange:(UITextView *)textView {
    [self adjustContentSize:textView];

    UIButton *actionButton = [self.actionBar viewWithTag:SIATextCommentActionSendType] ?: [self.actionBar viewWithTag:SIATextCommentActionUpdateType];
    NSString *text = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (text.length > 0)
        [actionButton setEnabled:YES];
    else
        [actionButton setEnabled:NO];

    if ([self.delegate respondsToSelector:@selector(textCommentView:userDidChangeComment:)])
        [self.delegate textCommentView:self userDidChangeComment:text];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"] && [textView.text containsString:@"\n"])
        return NO;
    
    return textView.text.length + (text.length - range.length) <= 60;
}

#pragma mark - SIATextCommentView (Misc.)

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor {
    [UIView transitionWithView:self
                      duration:0.03
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [super setBackgroundColor:backgroundColor];
                    } completion:nil];
}


@end
