//
// Created by Ben on 25/09/2016.
//

#include "fm_bling_blingy_ProcessImage.h"
#include <android/log.h>
#include <jni.h>

#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <stdlib.h>
#include <stdio.h>

#include "edgeDetector.h"
#include "chromaKey.h"
#include "imageUtil.h"

#include "globals.h"


using namespace cv;

const int edgeThresh = 1, lowThreshold = 20, max_lowThreshold = 60;
const int ratio = 3, kernel_size = 3;
const Size size(kernel_size, kernel_size);
edgeDetector eUtil;
chromaKey cUtil;
imageUtil iUtil;
static int   lastFaceDetectionLeft  = 0
    , lastFaceDetectionTop   = 0
    , lastFaceDetectionRight = 0
    , lastFaceDetectionBottom = 0;


/*
 *
 *
A declaration such as:

int num[29];
defines a contiguous array of 29 integers.

To access elements of the array use num[i] where i is the index (starting at 0 for the 1st element).

The expression num on its own gives a pointer (memory address and type) of the first element of the array.

The expression ptr + i (where ptr is a pointer and i is an integer) evaluates to a pointer that is i positions (in units of the type of pointer) after ptr.

So num + i gives a pointer to the element with index i.

The expression &a gives a pointer to some object a.

The expression *ptr gives the object that some pointer ptr is pointing at.

So the expressions a and *(&a) are equivalent.

So num[5] is the same as *(num+5)

and num+5 is the same as &num[5]

and num is the same as &num[0]

When you print a pointer with cout it will show its address.

When you print an object it will print the value of the object.


 *
 */

JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_init(JNIEnv *env, jobject obj)
{
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "hello from jni");
//
//    blur( src_gray, detected_edges, Size(3,3) );
}

JNIEXPORT jbyteArray JNICALL Java_fm_bling_blingy_ProcessImage_processImage
        (JNIEnv *env, jobject obj, jbyteArray inBuffer, jbyteArray outBuffer, jint width, jint height, jint type)
{
    int len = width * height * 4;
    char* buffer = new char[len];

    env->GetByteArrayRegion(inBuffer, 0, len, reinterpret_cast<jbyte*>(buffer));

//    dst.create(640, 480, CV_8UC4);
//    src.create(640, 480, CV_8UC4);

//    to rgba
//    Imgproc.cvtColor(mYuvFrameData, mRgba, Imgproc.COLOR_YUV2RGBA_NV21, 4);

//    Utils.bitmapToMat(cacheBitmap, mInputMat);


//    cvtColor(src, dst, COLOR_BGR2GRAY);
//    blur(dst, dst, Size(3, 3));
//    Canny(dst, dst, 25, 40);

//    env->ReleaseByteArrayElements()

    jbyteArray array = env->NewByteArray (len);
    env->SetByteArrayRegion(array, 0, len, reinterpret_cast<jbyte*>(buffer));

    return array;
}

JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_processImageAddr(JNIEnv *env, jobject obj, jlong addr, jstring bgFileName, jboolean hasBg,
    jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
{
    if(!hasBg)
        return;

    Mat &dss = *((Mat *) addr);

    Mat mask(dss.size(), 24);



    //load bg
    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//    Mat bg(dss.size(), dss.type());// = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
    Mat bg(dss.size(), dss.type());
    if(hasBg)
    {
//        bg = imread(nativeString, -10);     //CV_LOAD_IMAGE_COLOR);// >0
        transpose(imread(nativeString, -10), bg);
        resize(bg, bg, Size(640, 480));
        flip(bg, bg, 1);
    }
    cvtColor(bg, bg, COLOR_BGR2RGBA);
//    else bg(dss.size(), dss.type());
    env->ReleaseStringUTFChars(bgFileName, nativeString);



//
//    //load bg
//    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
////    Mat bg(dss.size(), dss.type());// = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
////    Mat bg(dss.size(), dss.type());
//
//
////    if(hasBg)
////    {
//        Mat bg = imread(nativeString, -10);     //CV_LOAD_IMAGE_COLOR);// >0
//        transpose(bg, bg);
//        Mat res;
//        resize(bg, res, Size(640, 480));
//        flip(res, bg, 1);
////    }
////    else bg(dss.size(), dss.type());
//
//    env->ReleaseStringUTFChars(bgFileName, nativeString);
//    cvtColor(bg, bg, COLOR_BGR2RGBA);


    ////test



    ////////////////////



    cvtColor(dss, mask, COLOR_RGBA2GRAY);
//    blur(mask, mask, Size(3, 3));
//    blur(mask, mask, size);//Size(kernel_size, kernel_size));
    blur(mask, mask, size);//Size(kernel_size, kernel_size));
    blur(mask, mask, size);//Size(kernel_size, kernel_size));

//    Mat lap, absLap;//(mask.size(), mask.type());
//    Laplacian(mask, lap, 24, 3, 1, 0, BORDER_DEFAULT);//
//
//    convertScaleAbs( lap, absLap );

//    if(true)
//    {
//        absLap.copyTo(dss);
//        return;
//    }

//    Mat rgbMat(dss.size(), dss.type());


    Mat hsv(dss.size(), dss.type());
    cvtColor(dss, hsv, COLOR_BGR2HSV);

//    if(true)
//    {
//        hsv.copyTo(dss);
//        return;
//    }

    Mat benMask(mask.size(), mask.type());
    eUtil.findBensEdges(hsv.data, benMask.data, hsv.data);


//    Mat lap, absLap;
//    Laplacian(mask, lap, CV_16S, 3, 5);//, 0, BORDER_CONSTANT);//
//
//    convertScaleAbs( lap, absLap );

    Canny(mask, mask, lowThreshold, max_lowThreshold, kernel_size);


//    mask = mask | absLap;



    const unsigned int laplacianMergingThreshHold = 199;

    for (int k = 0, index2; k < 640; k++)
    {
        index2 = 480 * k;
        for( int n=0; n < 480; n++ , index2++)
        {
            if(//0absLap.data[index2] > laplacianMergingThreshHold ||
                    benMask.data[index2] == 255)
                mask.data[index2] = 255; // White pixel
        }
    }

//    bitwise_or(mask, absLap, mask);



//    if(true)
//    {
//        mask.copyTo(dss);
//        return;
//    }






//    for (int k = 0, index, index2; k < 640; k++)
//    {
//        index = 480 * k * 3;
//        index2 = 480 * k;
//        for( int n=0; n < 480; n++ , index+=3, index2++)
//        {
////            hsv.data[index] = 0;
////            hsv.data[index + 1] = 0;
////            hsv.data[index + 2] = 0;
//            if (mask.data[index2] == 255)
//            {
//                hsv.data[index] = 255;
//                hsv.data[index + 1] = 255;
//                hsv.data[index + 2] = 255;
//            }
//        }
//    }
//
//    if(true)
//    {
//        hsv.copyTo(dss);
//        return;
//    }





//    //draw canny on bg for testing
//    int WIDTH = 640, HEIGHT = 480, index, index2;
//    for (int r = 0; r < HEIGHT; r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for (int c = 0; c < WIDTH; c++, index += 4, index2++)
//        {
//            if (mask.data[index2] == 255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//            else
//            {
//                bg.data[index] = 100;
//                bg.data[index + 1] = 100;
//                bg.data[index + 2] = 100;
//            }
//
//            // we get the image rotated, and so the face detection is rotated as well
//
//            //  faceRight -> top
//            //  faceTop   -> left
//            //  faceLeft  -> bottom
//            //  faveBottom-> right
//
//            if(faceLeft!=0)
//            {
//                if (c == faceRight && r >= faceTop && r <= faceBottom)
//                {
//                    bg.data[index] = 0;
//                    bg.data[index + 1] = 0;
//                    bg.data[index + 2] = 255;
//                }
//
//                if (r == faceTop && c >= faceLeft && c <= faceRight)
//                {
//                    bg.data[index] = 0;
//                    bg.data[index + 1] = 0;
//                    bg.data[index + 2] = 255;
//                }
//
//                if (c == faceLeft && r >= faceTop && r <= faceBottom)
//                {
//                    bg.data[index] = 0;
//                    bg.data[index + 1] = 0;
//                    bg.data[index + 2] = 255;
//                }
//
//                if (r == faceBottom && c >= faceLeft && c <= faceRight)
//                {
//                    bg.data[index] = 0;
//                    bg.data[index + 1] = 0;
//                    bg.data[index + 2] = 255;
//                }
//
//            }
//        }
//    }

    int faceWidth = faceBottom - faceTop, faceHeight = faceLeft - faceRight;
//    eUtil.fillEdges(bg.data, dss.data, mask.data, hsv.data, faceTop, faceRight, faceWidth, faceHeight); // top -> left , right -> top

    bg.copyTo(dss);

//    int WIDTH = bg.cols > dss.cols ? bg.cols : dss.cols, HEIGHT = bg.rows;
//    int index, index2;
//    for (int r = 0; r < HEIGHT; r++)
//    {
//        index = WIDTH * r * 4;
////        index2 = WIDTH * r;
//        for (int c = 0; c < WIDTH; c++, index += 4, index2++)
//        {
//            dss.data[index] = bg.data[index];
//            dss.data[index + 1] = bg.data[index + 1];
//            dss.data[index + 2] = bg.data[index + 2];
//        }
//    }


//    hsv.copyTo(dss);
}

//JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_processImageAddr(JNIEnv *env, jobject obj, jlong addr, jstring bgFileName, jboolean hasBg,
//                                                                          jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
//{
//    if(!hasBg)
//        return;
//
//    Mat &dss = *((Mat *) addr);
//
//
//}

JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_blurMat
        (JNIEnv *env, jobject obj, jlong addr, int intensity)
{
    Mat &dss = *((Mat *) addr);
    blur(dss, dss, Size(intensity, intensity));
}

JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_rotateImage
        (JNIEnv *env, jobject obj, jstring fileName, jint angle)
{
    const char *nativeString = env->GetStringUTFChars(fileName, JNI_FALSE);
    Mat src = imread(nativeString, -10);

    if(angle == 270 || angle == -90){
        // Rotate clockwise 270 degrees
        cv::transpose(src, src);
        cv::flip(src, src, 0);
    }else if(angle == 180 || angle == -180){
        // Rotate clockwise 180 degrees
        cv::flip(src, src, -1);
    }else if(angle == 90 || angle == -270){
        // Rotate clockwise 90 degrees
        cv::transpose(src, src);
        cv::flip(src, src, 1);
    }else if(angle == 360 || angle == 0 || angle == -360){
        if(src.data != src.data){
            src.copyTo(src);
        }
    }

//    transpose(src, src, 90);

    imwrite(nativeString, src);
    src.release();
}

JNIEXPORT jboolean JNICALL Java_fm_bling_blingy_ProcessImage_checkScreen
        (JNIEnv *env, jobject obj, jlong addr)
{
    Mat &dss = *((Mat *) addr);
    return eUtil.checkScreen(dss, 8, 1);
}

JNIEXPORT jboolean JNICALL Java_fm_bling_blingy_ProcessImage_findChromaKeys
        (JNIEnv *env, jobject obj, jlong addr, jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
{
    Mat &dss = *((Mat *) addr);
    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d   -   %d", faceLeft, faceTop);
    if(cameraRotation == 270)
    {
        //        dss = iUtil.resizeAccordingToWidth270(dss, WIDTH, HEIGHT);
        dss = iUtil.resizeAccordingToWidth270(dss, 360, 480);
        if(faceLeft != 0)
        {
            lastFaceDetectionLeft = faceTop;
            lastFaceDetectionTop = faceLeft - CAMERA_DIFFERENCE;
            lastFaceDetectionRight = faceBottom;
            lastFaceDetectionBottom = faceRight - CAMERA_DIFFERENCE;
            iUtil.drawFace270(dss, dss, lastFaceDetectionLeft, lastFaceDetectionTop, lastFaceDetectionRight, lastFaceDetectionBottom, 0, 0, 255);
        }
        else if(isFaceDetectionOn)
        {
            Rect face = iUtil.getFace270(dss);
            if (face.x != 0)
            {
                lastFaceDetectionLeft = face.x;
                lastFaceDetectionTop = face.y;
                lastFaceDetectionRight = face.x + face.width;
                lastFaceDetectionBottom = face.y + face.height;
            }
//            iUtil.drawFace270(dss, dss, lastFaceDetectionLeft, lastFaceDetectionTop, lastFaceDetectionRight, lastFaceDetectionBottom, 255, 255, 0);
        }
    }
    else if(cameraRotation == 90)
    {
        dss = iUtil.resizeAccordingToWidth90(dss, WIDTH, HEIGHT);
    }

    return cUtil.findChromaKeys(dss, lastFaceDetectionLeft, lastFaceDetectionTop, lastFaceDetectionRight, lastFaceDetectionBottom);
}

JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_gsImage
        (JNIEnv *env, jobject obj, jlong addr, jstring bgFileName, jboolean hasBg, jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
{
//    if(!hasBg)
//        return;

    Mat &dss = *((Mat *) addr);

    //////////////// background ////////////////

    //load bg
    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//    Mat bg(dss.size(), dss.type());// = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
    Mat bg(dss.size(), dss.type());
    if(hasBg)
    {
        bg = imread(nativeString, -10);     //CV_LOAD_IMAGE_COLOR);// >0

//        if(true)
//        {
//            bg.copyTo(dss);
//            __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d   -   %d", bg.cols, bg.rows);
//            env->ReleaseStringUTFChars(bgFileName, nativeString);
//
//            return;
//        }

//        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d   -   %d", bg.cols, bg.rows);

        transpose(imread(nativeString, -10), bg);
        flip(bg, bg, 1);

//        resize(bg, bg, Size(640, 480));

//        iUtil.mergeBuffers(bg, dss, dst);
    }
    else
    {
        int WIDTH = 640, HEIGHT = 480;
        int index, index2;
        for (int r = 0; r < HEIGHT; r++)
        {
            index = WIDTH * r * 4;
            //        index2 = WIDTH * r;
            for (int c = 0; c < WIDTH; c++, index += 4, index2++)
            {
                bg.data[index] = 0;
                bg.data[index + 1] = 255;
                bg.data[index + 2] = 0;
            }
        }
    }
//    else bg(dss.size(), dss.type());
    env->ReleaseStringUTFChars(bgFileName, nativeString);
    cvtColor(bg, bg, COLOR_BGR2RGBA);

//
////    Mat bg(dss.size(), dss.type());
//
////    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//
//    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
////    Mat bg(dss.size(), dss.type());// = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
////    Mat bg(dss.size(), dss.type());
//
//
////    if(hasBg)
////    {
//    Mat bg = imread(nativeString, -10);     //CV_LOAD_IMAGE_COLOR);// >0
//    transpose(bg, bg);
//    resize(bg, bg, Size(640, 480));
//    flip(bg, bg, 1);
////    }
////    else bg(dss.size(), dss.type());
//
//    env->ReleaseStringUTFChars(bgFileName, nativeString);
//    cvtColor(bg, bg, COLOR_BGR2RGBA);


//    if(hasBg)
//    {
//
//        bg = imread(nativeString, -10);     //CV_LOAD_IMAGE_COLOR);// >0
//        transpose(imread(nativeString, -10), bg);
//        resize(bg, bg, Size(640, 480));
//        flip(bg, bg, 1);
//
//    }
//    else
//    {
//        int WIDTH = 640, HEIGHT = 480;
//        int index, index2;
//        for (int r = 0; r < HEIGHT; r++)
//        {
//            index = WIDTH * r * 4;
//            //        index2 = WIDTH * r;
//            for (int c = 0; c < WIDTH; c++, index += 4, index2++)
//            {
//                bg.data[index] = 0;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 0;
//            }
//        }
//    }
    ////////////////////////////////////////////////

//    chromaKeyUtil.getChromaKeysBuffer(bg, dss, 640 - faceTop, 480 - faceRight, 640 - faceBottom, 480 - faceLeft);
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d  %d  %d  %d", faceLeft, faceTop, faceRight, faceBottom);

    faceLeft = 640 - faceLeft;
    faceRight = 640 - faceRight;

    faceLeft *= 1.5;
    faceRight *= 1.1;
    faceTop *= 1.1;
    faceBottom *= 0.9;

    Mat ChromaKeyEdges(dss.size(), 0);

//    cUtil.getChromaKeysBuffer(bg, dss, ChromaKeyEdges, faceRight, faceTop, faceLeft, faceBottom);
    cUtil.getChromaKeysBuffer(dss, ChromaKeyEdges, faceRight, faceTop, faceLeft, faceBottom);

    ChromaKeyEdges.copyTo(dss);
}

JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_multiDetection
        (JNIEnv *env, jobject obj, jlong addr, jstring bgFileName, jboolean hasBg, jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
{
    Mat &dss = *((Mat *) addr);

    //load bg
    Mat bg;
    if (hasBg)
    {
        const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
        bg = iUtil.loadBg(nativeString);
        env->ReleaseStringUTFChars(bgFileName, nativeString);
    }
    else bg = Mat(Size(360, 480), dss.type());

    if (cameraRotation == 270) // front camera - counts as mirrored - if not fix !
    {
        // resize dss according to the clip
        dss = iUtil.resizeAccordingToWidth270(dss, bg.cols, bg.rows);

        // get face locations
        if(faceLeft != 0)
        {
            lastFaceDetectionLeft = faceTop;
            lastFaceDetectionTop = faceLeft - CAMERA_DIFFERENCE;
            lastFaceDetectionRight = faceBottom;
            lastFaceDetectionBottom = faceRight - CAMERA_DIFFERENCE;
//            iUtil.drawFace270(dss, dss, lastFaceDetectionLeft, lastFaceDetectionTop, lastFaceDetectionRight, lastFaceDetectionBottom, 0, 0, 255);
//            __android_log_print(ANDROID_LOG_ERROR, "tag", "1SimLe---blue------v43>2>> %d   --  %d   %d   --  %d ", lastFaceDetectionLeft, lastFaceDetectionTop, lastFaceDetectionRight, lastFaceDetectionBottom);
        }
        else if(isFaceDetectionOn)
        {
            Rect face = iUtil.getFace270(dss);
            if (face.x != 0)
            {
                lastFaceDetectionLeft = face.x + (face.width / 10);
                lastFaceDetectionTop = face.y;
                lastFaceDetectionRight = face.x + face.width;
                lastFaceDetectionBottom = face.y + (face.height * 2 / 3);
            }
//            iUtil.drawFace270(dss, dss, lastFaceDetectionLeft, lastFaceDetectionTop, lastFaceDetectionRight, lastFaceDetectionBottom, 255, 0, 0);
//            __android_log_print(ANDROID_LOG_ERROR, "tag", "1SimLe---red------v43>2>> %d   --  %d   %d   --  %d ", lastFaceDetectionLeft, lastFaceDetectionTop, lastFaceDetectionRight, lastFaceDetectionBottom);
        }

        //////////////////////////////////////////////////////////

        // get masks buffers
        Mat edgesBuffer(defMatSize, 0);
        Mat ChromaKeyBuffer(defMatSize, 0);

        // get the edge buffer
//        eUtil.getEdgesBuffer(bg, dss, edgesBuffer, faceLeft, faceTop, faceRight, faceBottom);

        eUtil.getEdgesBuffer(bg, dss, edgesBuffer, lastFaceDetectionLeft, lastFaceDetectionBottom);

        // get the chroma key buffer
        cUtil.getChromaKeysBuffer(dss, ChromaKeyBuffer, lastFaceDetectionLeft, lastFaceDetectionBottom, lastFaceDetectionRight, lastFaceDetectionTop);

        // draw the camera on the bg using the masks as references
        iUtil.mergeBufferAccordingToBuffersII(dss, edgesBuffer, ChromaKeyBuffer, bg);

        flip(bg, dss, 0); // this buffer counts as mirrored so we flip it

//        bg.copyTo(dss); // this buffer is not mirrored so we just copy the data into it

        edgesBuffer.release();
        ChromaKeyBuffer.release();
        bg.release();
    }
    else if (cameraRotation == 90) // rear camera
    {
        // resize dss according to the clip
        dss = iUtil.resizeAccordingToWidth90(dss, bg.cols, bg.rows);

        // get face locations
        if(faceLeft != 0)
        {
            int faceWidth = (faceBottom - faceTop), faceHeight = (faceRight - faceLeft);
            lastFaceDetectionLeft = HEIGHT - faceBottom;// + (faceWidth/2);
            lastFaceDetectionTop = WIDTH - faceRight;// + faceWidth - CAMERA_DIFFERENCE;//HEIGHT - (faceRight  );// - faceWidth;
            lastFaceDetectionRight = HEIGHT - faceTop;// + faceHeight;//WIDTH - faceTop + faceWidth;
            lastFaceDetectionBottom = WIDTH - faceLeft;// + faceWidth - CAMERA_DIFFERENCE;//HEIGHT - (faceLeft ) ;//- faceWidth;// - CAMERA_DIFFERENCE;
//            iUtil.drawFace270(dss, dss, lastFaceDetectionLeft, lastFaceDetectionTop, lastFaceDetectionRight, lastFaceDetectionBottom, 0, 0, 255);
//            __android_log_print(ANDROID_LOG_ERROR, "tag", "1SimLe---blue------v43>2>> %d   --  %d   %d   --  %d ", lastFaceDetectionLeft, lastFaceDetectionTop, lastFaceDetectionRight, lastFaceDetectionBottom);
        }
        else if(isFaceDetectionOn)
        {
            Rect face = iUtil.getFace270(dss);
            if (face.x != 0)
            {
                lastFaceDetectionLeft = face.x + (face.width / 10);
                lastFaceDetectionTop = face.y;
                lastFaceDetectionRight = face.x + face.width;
                lastFaceDetectionBottom = face.y + (face.height * 2 / 3);
            }
//            iUtil.drawFace270(dss, dss, lastFaceDetectionLeft, lastFaceDetectionTop, lastFaceDetectionRight, lastFaceDetectionBottom, 255, 0, 0);
//            __android_log_print(ANDROID_LOG_ERROR, "tag", "1SimLe---red------v43>2>> %d   --  %d   %d   --  %d ", lastFaceDetectionLeft, lastFaceDetectionTop, lastFaceDetectionRight, lastFaceDetectionBottom);
        }


        //////////////////////////////////////////////////////////

        // get masks buffers
        Mat edgesBuffer(defMatSize, 0);
        Mat ChromaKeyBuffer(defMatSize, 0);

        // get the edge buffer
        eUtil.getEdgesBuffer(bg, dss, edgesBuffer, lastFaceDetectionLeft, lastFaceDetectionBottom);

        // get the chroma key buffer
        cUtil.getChromaKeysBuffer(dss, ChromaKeyBuffer, lastFaceDetectionLeft, lastFaceDetectionBottom, lastFaceDetectionRight, lastFaceDetectionTop);

        // draw the camera on the bg using the masks as references
        iUtil.mergeBufferAccordingToBuffers(dss, edgesBuffer, ChromaKeyBuffer, bg);

        bg.copyTo(dss); // this buffer is not mirrored so we just copy the data into it

        edgesBuffer.release();
        ChromaKeyBuffer.release();
        bg.release();
    }

}

//JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_multiDetection
//        (JNIEnv *env, jobject obj, jlong addr, jstring bgFileName, jboolean hasBg, jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
//{
//    Mat &dss = *((Mat *) addr);
//
//
////    Mat bgw = iUtil.cropImage(dss, 0, 0, 200, 200);//.copyTo(dss);
////    Mat bgw = iUtil.cropImage(dss, 0, 140, 480, 500);
////    Mat bgw(Size(360, 480), dss.type());// = iUtil.cropImage(dss, 0, 140, 480, 500);
////    iUtil.cropImage(dss, bgw, 0, 140);
////    bgw.copyTo(dss);
////    return;
//
//    //load bg
//    Mat bg;
//    if (hasBg)
//    {
//        const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//        bg = iUtil.loadBg(nativeString);
//        env->ReleaseStringUTFChars(bgFileName, nativeString);
//
////        bg.copyTo(dss);
////        return;
//    }
//    else bg = Mat(Size(360, 480), dss.type());
//
//    if (cameraRotation == 270)
//    {
//        // resize dss according to the clip
//        dss = iUtil.resizeAccordingToWidth270(dss, bg.cols, bg.rows);
//
//        Mat edgesBuffer(defMatSize, 0);
//        Mat ChromaKeyBuffer(defMatSize, 0);
//
//        __android_log_print(ANDROID_LOG_ERROR, "tag", "1SimLe---------v43>2>> %d   --  %d   %d",
//                            faceRight, dss.cols, CAMERA_DIFFERENCE);
//
//
//        faceRight -= CAMERA_DIFFERENCE;
////    faceLeft -= CAMERA_DIFFERENCE;
////    faceTop -= CAMERA_DIFFERENCE;
//        // set the faceLocations according to the dss new size
////    faceLeft *= scaleFactor;
////    faceTop *= scaleFactor;
////    faceRight *= scaleFactor;
////    faceBottom *= scaleFactor;
//
//        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLe---------v43>2>> %d   --  %d   %d",
//                            faceRight, faceLeft, faceTop);
//
//        //////////////////////////////////////////////////////////
//
//        // get the edge buffer
//        eUtil.getEdgesBuffer(bg, dss, edgesBuffer, faceLeft, faceTop, faceRight, faceBottom);
//
//        // get the chroma key buffer
//        cUtil.getChromaKeysBuffer(dss, ChromaKeyBuffer, faceRight, faceTop, faceLeft,
//                                     faceBottom);
//
//
////    for (int k = 0, index2; k < dss.rows; k++)
////    {
////        index2 = dss.cols * k;
////        for( int n=0; n < dss.cols; n++ , index2++)
////        {
////            if(ChromaKeyBuffer.data[index2] == 255 || edgesBuffer.data[index2] == 255)
////            {
////                bg.data[index2 * bg.channels()] = dss.data[index2 * dss.channels()];
////                bg.data[index2 * bg.channels() + 1] = dss.data[index2 * dss.channels() + 1];
////                bg.data[index2 * bg.channels() + 2] = dss.data[index2 * dss.channels() + 2];
////            }
////        }
////    }
//
//        iUtil.mergeBufferAccordingToBuffers(dss, edgesBuffer, ChromaKeyBuffer, bg);
//
////        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLe---------v43>2>> %d  %d", dss.cols, dss.rows);
//
//
////    if(isCameraMirrored)
////        flip(bg, dss, 0);
////    else
//        bg.copyTo(dss);
//
////    bg.copyTo(dss);
//
//        edgesBuffer.release();
//        ChromaKeyBuffer.release();
//        bg.release();
//
//    }
//
////    iUtil.mergeBuffer(dss, bg);
////    bg.copyTo(dss);
////    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLe---------v43>2>> %d  %d", bg.cols, bg.rows);
//
//}

JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_setCameraRotation
        (JNIEnv *env, jobject obj, jint rotation, jboolean isMirrored)
{
    cameraRotation = rotation;
    isCameraMirrored = isMirrored;
}

JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_turnFaceDetectionOn
        (JNIEnv *env, jobject obj, jstring iHaarCascadeFilePath)
{
    isFaceDetectionOn = true;
    const char *nativeString = env->GetStringUTFChars(iHaarCascadeFilePath, JNI_FALSE);
    haarCascadeFilePath = nativeString;
    env->ReleaseStringUTFChars(iHaarCascadeFilePath, nativeString);

    iUtil.turnOnFaceDetection();
}




////            jint t = faceLeft;
////            faceLeft = faceTop;
////            faceTop = t;
////            int t = faceBottom;
////            faceRight = faceBottom;
////            faceRight = t;
//
//faceRight -= CAMERA_DIFFERENCE;
//faceLeft -= CAMERA_DIFFERENCE;
//
//if(faceBottom > HEIGHT)
//faceBottom = HEIGHT;
//
//if(faceTop > HEIGHT)
//faceTop = HEIGHT;
//
//if(faceLeft > WIDTH)
//faceLeft = WIDTH;
//
//if(faceRight > WIDTH)
//faceRight = WIDTH;
//
////            int dFaceWidth = faceRight - faceLeft;
////            faceLeft -= dFaceWidth;
////            faceRight -= dFaceWidth;
//
////            jint t = faceLeft;
////            faceLeft = faceRight;
////            faceRight = t;
//
//iUtil.drawFace270(dss, dss, faceLeft, faceTop, faceRight, faceBottom, 0, 0, 255);
////            iUtil.drawFace270(dss, dss, faceTop, faceRight, faceBottom, faceLeft, 0, 255, 0);
//
//__android_log_print(ANDROID_LOG_ERROR, "tag", "1SimLe---------v43>2>> %d   --  %d   %d   --  %d ", dFaceLeft, dFaceTop, dFaceRight, dFaceBottom);






//JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_multiDetection
//        (JNIEnv *env, jobject obj, jlong addr, jstring bgFileName, jboolean hasBg, jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
//{
//    Mat &dss = *((Mat *) addr);
//
//
////    Mat bgw = iUtil.cropImage(dss, 0, 0, 200, 200);//.copyTo(dss);
////    Mat bgw = iUtil.cropImage(dss, 0, 140, 480, 500);
////    Mat bgw(Size(360, 480), dss.type());// = iUtil.cropImage(dss, 0, 140, 480, 500);
////    iUtil.cropImage(dss, bgw, 0, 140);
////    bgw.copyTo(dss);
////    return;
//
//    //load bg
//    Mat bg;
//    if (hasBg)
//    {
//        const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//        bg = iUtil.loadBg(nativeString);
//        env->ReleaseStringUTFChars(bgFileName, nativeString);
//
////        bg.copyTo(dss);
////        return;
//    }
//    else bg = Mat(Size(360, 480), dss.type());
//
//    if (cameraRotation == 270) // front camera - counts as mirrored - if not fix !
//    {
//        // resize dss according to the clip
//        dss = iUtil.resizeAccordingToWidth270(dss, bg.cols, bg.rows);
//
//        Mat edgesBuffer(defMatSize, 0);
//        Mat ChromaKeyBuffer(defMatSize, 0);
//
//        faceRight -= CAMERA_DIFFERENCE;
//
//        //////////////////////////////////////////////////////////
//
//        // get the edge buffer
//        eUtil.getEdgesBuffer(bg, dss, edgesBuffer, faceLeft, faceTop, faceRight, faceBottom);
//
//        // get the chroma key buffer
//        cUtil.getChromaKeysBuffer(dss, ChromaKeyBuffer, faceRight, faceTop, faceLeft,
//                                     faceBottom);
//
//        // draw the camera on the bg using the masks as references
//        iUtil.mergeBufferAccordingToBuffers(dss, edgesBuffer, ChromaKeyBuffer, bg);
//
//        flip(bg, dss, 0); // this buffer counts as mirrored so we flip it
//
//        edgesBuffer.release();
//        ChromaKeyBuffer.release();
//        bg.release();
//    }
//    else if (cameraRotation == 90) // rear camera
//    {
//// resize dss according to the clip
//        dss = iUtil.resizeAccordingToWidth90(dss, bg.cols, bg.rows);
//
//        Mat edgesBuffer(defMatSize, 0);
//        Mat ChromaKeyBuffer(defMatSize, 0);
//
//        //////////////////////////////////////////////////////////
//
//        //adjust the camera locations
//        int t = faceTop;
//        faceTop = 480 - faceBottom;
//        faceBottom = 480 - t;
//
//        faceLeft = 640 - faceLeft;
//        faceRight = 640 - faceRight;
//
//        // get the edge buffer
////        eUtil.getEdgesBuffer(bg, dss, edgesBuffer, faceLeft, faceTop, faceRight, faceBottom);
//        eUtil.getEdgesBuffer(bg, dss, edgesBuffer, faceRight, faceTop, faceLeft, faceBottom);
//
//        // get the chroma key buffer
//        cUtil.getChromaKeysBuffer(dss, ChromaKeyBuffer, faceRight, faceTop, faceLeft,
//                                     faceBottom);
//
//        // draw the camera on the bg using the masks as references
//        iUtil.mergeBufferAccordingToBuffers(dss, edgesBuffer, ChromaKeyBuffer, bg);
//
////    for (int r = 0, index, index2; r < HEIGHT; r++)
////    {
////        index = WIDTH * r * 4;
////        index2 = WIDTH * r;
////        for (int c = 0; c < WIDTH; c++, index += 4, index2++)
////        {
//////            if (edgesBuffer.data[index2] == 255)
//////            {
//////                bg.data[index] = 255;
//////                bg.data[index + 1] = 255;
//////                bg.data[index + 2] = 255;
//////            }
//////            else
//////            {
//////                bg.data[index] = 100;
//////                bg.data[index + 1] = 100;
//////                bg.data[index + 2] = 100;
//////            }
////
////            // we get the image rotated, and so the face detection is rotated as well
////
////            //  faceRight -> top
////            //  faceTop   -> left
////            //  faceLeft  -> bottom
////            //  faveBottom-> right
////
////            if(faceLeft!=0)
////            {
////                if (c == faceRight && r >= faceTop && r <= faceBottom)
////                {
////                    bg.data[index] = 0;
////                    bg.data[index + 1] = 0;
////                    bg.data[index + 2] = 255;
////                }
////
////                if (r == faceTop && c <= faceLeft && c >= faceRight)
////                {
////                    bg.data[index] = 0;
////                    bg.data[index + 1] = 255;
////                    bg.data[index + 2] = 0;
////                }
////
//////                if (c == faceLeft && r >= faceTop && r <= faceBottom)
////                if (c == faceLeft && r >= faceTop && r <= faceBottom)
////                {
////                    bg.data[index] = 255;
////                    bg.data[index + 1] = 0;
////                    bg.data[index + 2] = 0;
////                }
////
//////                if (r == faceBottom && c >= faceLeft && c <= faceRight)
////                if (r == faceBottom && c <= faceLeft && c >= faceRight)
////                {
////                    bg.data[index] = 125;
////                    bg.data[index + 1] = 125;
////                    bg.data[index + 2] = 125;
////                }
////
////            }
////        }
////    }
//
//
//
//        bg.copyTo(dss); // this buffer is not mirrored so we just copy the data into it
//
//        edgesBuffer.release();
//        ChromaKeyBuffer.release();
//        bg.release();
//    }
//
//}
//

////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////

//JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_multiDetection
//        (JNIEnv *env, jobject obj, jlong addr, jstring bgFileName, jboolean hasBg, jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
//{
//    Mat &dss = *((Mat *) addr);
//
//    //load bg
//    Mat bg;
//    if(hasBg)
//    {
//        const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//        bg = iUtil.loadBg(nativeString);
//        env->ReleaseStringUTFChars(bgFileName, nativeString);
//
////        bg.copyTo(dss);
////        return;
//    }
//    else bg = Mat(dss.size(), dss.type());
//
//
//    dss = iUtil.resizeAccordingToWidth(dss, bg.cols);
//
//
//
//    Mat edgesBuffer(defMatSize, 0);
//    Mat ChromaKeyBuffer(defMatSize, 0);
//
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>1111111>> %d   -   %d", faceLeft, faceTop);
//
//    faceLeft *= scaleFactor;
//    faceTop *= scaleFactor;
//    faceRight *= scaleFactor;
//    faceBottom *= scaleFactor;
//
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2222222>> %d   -   %d", faceLeft, faceTop);
//
////    ChromaKeyBuffer.copyTo(dss);
//
//
//    //////////////////////////////////////////////////////////
////    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
////
////    bg = imread(nativeString, -10);     //CV_LOAD_IMAGE_COLOR);// >0
////
//////        if(true)
//////        {
//////            bg.copyTo(dss);
//////            __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d   -   %d", bg.cols, bg.rows);
//////            env->ReleaseStringUTFChars(bgFileName, nativeString);
//////
//////            return;
//////        }
////
//////        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d   -   %d", bg.cols, bg.rows);
////
////    transpose(imread(nativeString, -10), bg);
////    flip(bg, bg, 1);
////
//////        resize(bg, bg, Size(640, 480));
////    env->ReleaseStringUTFChars(bgFileName, nativeString);
//
//    //////////////////////////////////////////////////////////
//
//
//
//
//////    int faceWidth = faceBottom - faceTop, faceHeight = faceLeft - faceRight;
//    eUtil.getEdgesBuffer(bg, dss, edgesBuffer, faceLeft, faceTop, faceRight, faceBottom);
//
//
////    edgesBuffer.copyTo(dss);
////    iUtil.mergeBuffer(dss, bg);
////    bg.copyTo(dss);
////    if(true)
////        return;
//
//////    edgesBuffer.copyTo(dss);
////
//    cUtil.getChromaKeysBuffer(dss, ChromaKeyBuffer, faceRight, faceTop, faceLeft, faceBottom);
////
//    for (int k = 0, index2; k < dss.rows; k++)
//    {
//        index2 = dss.cols * k;
//        for( int n=0; n < dss.cols; n++ , index2++)
//        {
////            if(//0absLap.data[index2] > laplacianMergingThreshHold ||
////                    ChromaKeyBuffer.data[index2] == 255)
////                edgesBuffer.data[index2] = 255; // White pixel
//
//            if(//0absLap.data[index2] > laplacianMergingThreshHold ||
//                    ChromaKeyBuffer.data[index2] == 255 || edgesBuffer.data[index2] == 255)
//            {
//                bg.data[index2 * bg.channels()] = dss.data[index2 * dss.channels()];
//                bg.data[index2 * bg.channels() + 1] = dss.data[index2 * dss.channels() + 1];
//                bg.data[index2 * bg.channels() + 2] = dss.data[index2 * dss.channels() + 2];
//            }
//        }
//    }
//
////    edgesBuffer.copyTo(dss);
//
////    eUtil.startThread();
//
////    iUtil.mergeBuffer(dss, bg);
//    bg.copyTo(dss);
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLe---------v43>2>> %d  %d", bg.cols, bg.rows);
//
//}

////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////



//JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_processImageAddr(JNIEnv *env, jobject obj, jlong addr, jstring bgFileName, jboolean hasBg,
//                                                                          jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
////    jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
//{
////    __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>>> %d   %d", 5, 5);
//
//    if(!hasBg)
//        return;
//
//    Mat &dss = *((Mat *) addr);
//
//    Mat mask(dss.size(), 24);
//
//
//
//    //load bg
//    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
////    Mat bg(dss.size(), dss.type());// = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
//    Mat bg(dss.size(), dss.type());
//    if(hasBg)
//    {
////        bg = imread(nativeString, -10);     //CV_LOAD_IMAGE_COLOR);// >0
//        transpose(imread(nativeString, -10), bg);
//        resize(bg, bg, Size(640, 480));
//        flip(bg, bg, 1);
//    }
////    else bg(dss.size(), dss.type());
//    env->ReleaseStringUTFChars(bgFileName, nativeString);
//    cvtColor(bg, bg, COLOR_BGR2RGBA);
//
//
//
////
////    //load bg
////    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//////    Mat bg(dss.size(), dss.type());// = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
//////    Mat bg(dss.size(), dss.type());
////
////
//////    if(hasBg)
//////    {
////        Mat bg = imread(nativeString, -10);     //CV_LOAD_IMAGE_COLOR);// >0
////        transpose(bg, bg);
////        Mat res;
////        resize(bg, res, Size(640, 480));
////        flip(res, bg, 1);
//////    }
//////    else bg(dss.size(), dss.type());
////
////    env->ReleaseStringUTFChars(bgFileName, nativeString);
////    cvtColor(bg, bg, COLOR_BGR2RGBA);
//
//
//    ////test
//
//
//
//    ////////////////////
//
//
//
//    cvtColor(dss, mask, COLOR_RGBA2GRAY);
////    blur(mask, mask, Size(3, 3));
////    blur(mask, mask, size);//Size(kernel_size, kernel_size));
//    blur(mask, mask, size);//Size(kernel_size, kernel_size));
//    blur(mask, mask, size);//Size(kernel_size, kernel_size));
//
////    Mat lap, absLap;//(mask.size(), mask.type());
////    Laplacian(mask, lap, 24, 3, 1, 0, BORDER_DEFAULT);//
////
////    convertScaleAbs( lap, absLap );
//
////    if(true)
////    {
////        absLap.copyTo(dss);
////        return;
////    }
//
//    Mat rgbMat(dss.size(), dss.type());
//
//
//    Mat hsv(dss.size(), dss.type());
//    cvtColor(dss, hsv, COLOR_BGR2HSV);
//
////    if(true)
////    {
////        hsv.copyTo(dss);
////        return;
////    }
//
//    Mat benMask(mask.size(), mask.type());
//    util.findEdges(hsv.data, benMask.data, hsv.data);
//
//
////    Mat lap, absLap;
////    Laplacian(mask, lap, CV_16S, 3, 5);//, 0, BORDER_CONSTANT);//
////
////    convertScaleAbs( lap, absLap );
//
//    Canny(mask, mask, lowThreshold, max_lowThreshold, kernel_size);
//
//
////    mask = mask | absLap;
//
//
//
//    const unsigned int laplacianMergingThreshHold = 199;
//
//    for (int k = 0, index2; k < 640; k++)
//    {
//        index2 = 480 * k;
//        for( int n=0; n < 480; n++ , index2++)
//        {
//            if(//0absLap.data[index2] > laplacianMergingThreshHold ||
//                    benMask.data[index2] == 255)
//                mask.data[index2] = 255; // White pixel
//        }
//    }
//
////    bitwise_or(mask, absLap, mask);
//
//
//
////    if(true)
////    {
////        mask.copyTo(dss);
////        return;
////    }
//
//
//
//
//
//
////    for (int k = 0, index, index2; k < 640; k++)
////    {
////        index = 480 * k * 3;
////        index2 = 480 * k;
////        for( int n=0; n < 480; n++ , index+=3, index2++)
////        {
//////            hsv.data[index] = 0;
//////            hsv.data[index + 1] = 0;
//////            hsv.data[index + 2] = 0;
////            if (mask.data[index2] == 255)
////            {
////                hsv.data[index] = 255;
////                hsv.data[index + 1] = 255;
////                hsv.data[index + 2] = 255;
////            }
////        }
////    }
////
////    if(true)
////    {
////        hsv.copyTo(dss);
////        return;
////    }
//
//
//
//
//
////    //draw canny on bg for testing
////    int WIDTH = 640, HEIGHT = 480, index, index2;
////    for (int r = 0; r < HEIGHT; r++)
////    {
////        index = WIDTH * r * 4;
////        index2 = WIDTH * r;
////        for (int c = 0; c < WIDTH; c++, index += 4, index2++)
////        {
////            if (mask.data[index2] == 255)
////            {
////                bg.data[index] = 255;
////                bg.data[index + 1] = 255;
////                bg.data[index + 2] = 255;
////            }
////            else
////            {
////                bg.data[index] = 100;
////                bg.data[index + 1] = 100;
////                bg.data[index + 2] = 100;
////            }
////
////            // we get the image rotated, and so the face detection is rotated as well
////
////            //  faceRight -> top
////            //  faceTop   -> left
////            //  faceLeft  -> bottom
////            //  faveBottom-> right
////
////            if(faceLeft!=0)
////            {
////                if (c == faceRight && r >= faceTop && r <= faceBottom)
////                {
////                    bg.data[index] = 0;
////                    bg.data[index + 1] = 0;
////                    bg.data[index + 2] = 255;
////                }
////
////                if (r == faceTop && c >= faceLeft && c <= faceRight)
////                {
////                    bg.data[index] = 0;
////                    bg.data[index + 1] = 0;
////                    bg.data[index + 2] = 255;
////                }
////
////                if (c == faceLeft && r >= faceTop && r <= faceBottom)
////                {
////                    bg.data[index] = 0;
////                    bg.data[index + 1] = 0;
////                    bg.data[index + 2] = 255;
////                }
////
////                if (r == faceBottom && c >= faceLeft && c <= faceRight)
////                {
////                    bg.data[index] = 0;
////                    bg.data[index + 1] = 0;
////                    bg.data[index + 2] = 255;
////                }
////
////            }
////        }
////    }
//
//    int faceWidth = faceBottom - faceTop, faceHeight = faceLeft - faceRight;
////    util.fillEdges(bg.data, dss.data, mask.data, hsv.data, faceTop, faceRight, faceWidth, faceHeight); // top -> left , right -> top
//
//    bg.copyTo(dss);
//
////    int WIDTH = bg.cols > dss.cols ? bg.cols : dss.cols, HEIGHT = bg.rows;
////    int index, index2;
////    for (int r = 0; r < HEIGHT; r++)
////    {
////        index = WIDTH * r * 4;
//////        index2 = WIDTH * r;
////        for (int c = 0; c < WIDTH; c++, index += 4, index2++)
////        {
////            dss.data[index] = bg.data[index];
////            dss.data[index + 1] = bg.data[index + 1];
////            dss.data[index + 2] = bg.data[index + 2];
////        }
////    }
//
//
////    hsv.copyTo(dss);
//}


/////////////////////////////////////////////////////////////////////////////////////////////////////////


//    int y = 0, x, index, index2 = 1, startedCol, distance;
//    int rightSideRow, leftSideRow;
//    bool foundObject(false), foundEdge;
//
//    //draw canny on bg for testing
//    for(int r=0;r<HEIGHT;r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for(int c=0;c<WIDTH;c++, index+=4, index2++)
//        {
//            if(mask.data[index2]==255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//        }
//    }
//
//    y=0;
//    for(int c=WIDTH-1, r;c > 1; c--, y++)
//    {
////        index = (WIDTH * HEIGHT * 4) - 4 - (c * 4);
//        index = (c * 4) - 4;
//        index2 = c - 1;
//        x = 0;
//        for(r=1;r<HEIGHT;r++, index+=WIDTH_X_4, index2+=WIDTH, x++)
//        {
//            if(foundObject)
//            {
//                if(mask.data[index2] == 255)
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//                else
//                {
//                    rightSideRow = r;
//                    break;
//                }
//            }
//
//            if(mask.data[index2] == 255)
//            {
//                foundObject = true;
//
//                startedCol = c;
//                rightSideRow = leftSideRow = r;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//            bg.data[index] = 255;
//            bg.data[index + 1] = 0;
//            bg.data[index + 2] = 0;
//        }
//
//        if(foundObject)
//        {
//            rightSideRow = r;
//            int indexRight, indexLeft, inner;
//
//            while(c > 1)
//            {
//                c--;
//                indexRight = (rightSideRow * WIDTH) + c;
//                indexLeft = (leftSideRow * WIDTH) + c;
//
//                ///////////////////left side/////////////////////
//
//                inner = 1;
//                while ( indexLeft - (WIDTH * inner) > 0
//                        && mask.data[indexLeft - (WIDTH * inner)] == 255 )
//                {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    distance = 0;
//                    foundEdge = false;
//                    inner = 1;
//                    do{
//                        inner++;
//                        if ( indexLeft - (WIDTH * inner) > 0
//                                &&
//                                    (mask.data[indexLeft - (WIDTH * inner)] == 255
//                                    || mask.data[indexLeft - (WIDTH * inner) - 1] == 255
//                                    || mask.data[indexLeft - (WIDTH * inner) + 1] == 255
//                                    )
//                                )
//                        {
//                            foundEdge = true;
//                            break;
//                        }
//                    }while(distance++ < MAX_DISTANCE);
//
//                    if(!foundEdge)
//                        inner = 0;
//
//
////                    inner = 2;
////                    while (indexLeft - (WIDTH * inner) > 0
////                           &&
////                           mask.data[indexLeft - (WIDTH * inner)] == 255
////                            )
////                    {
////                        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d  %d  %d", indexLeft, (WIDTH * inner), c);
////
////                        inner++;
////                    }
////                    if(inner == 2)
////                        inner = 0;
////                    else inner--;
////                    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>1>> %d  %d  %d  %d  %d", indexLeft, (WIDTH * inner), c, leftSideRow, inner);
//
////                    inner--;
////                    if(inner == 1)
////                        inner == 0;
//                }
//
//
//                if (inner > 0) {
//                    leftSideRow -= inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexLeft + (WIDTH * inner) > 0
//                           && mask.data[indexLeft + (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    leftSideRow += inner;
//                }
//
//                if(leftSideRow < 0)
//                    leftSideRow = 0;
//
//                ///////////////////right side/////////////////////
//                inner = 1;
//                while (indexRight + (WIDTH * inner) > 0
//                       && mask.data[indexRight + (WIDTH * inner)] == 255) {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    distance = 0;
//                    foundEdge = false;
//                    inner = 1;
//                    do{
//                        inner++;
//                        if ( indexRight + (WIDTH * inner) > 0
//                             &&
//                             (mask.data[indexRight + (WIDTH * inner)] == 255
//                              || mask.data[indexRight + (WIDTH * inner) - 1] == 255
//                              || mask.data[indexRight + (WIDTH * inner) + 1] == 255
//                             )
//                                )
//                        {
//                            foundEdge = true;
//                            break;
//                        }
//                    }while(distance++ < MAX_DISTANCE);
//
//                    if(!foundEdge)
//                        inner = 0;
//
//
////                    inner = 2;
////                    while (indexLeft - (WIDTH * inner) > 0
////                           &&
////                           mask.data[indexLeft - (WIDTH * inner)] == 255
////                            )
////                    {
////                        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d  %d  %d", indexLeft, (WIDTH * inner), c);
////
////                        inner++;
////                    }
////                    if(inner == 2)
////                        inner = 0;
////                    else inner--;
////                    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>1>> %d  %d  %d  %d  %d", indexLeft, (WIDTH * inner), c, leftSideRow, inner);
//
////                    inner--;
////                    if(inner == 1)
////                        inner == 0;
//                }
//
////                if(inner == 0)
////                {
////                    inner = 2;
////                    while (indexRight + (WIDTH * inner) > 0
////                           && mask.data[indexRight + (WIDTH * inner)] == 255) {
////                        inner++;
////                    }
////                    if(inner == 2)
////                        inner = 0;
////                    else inner--;
////                }
//
//                if (inner > 0) {
//                    rightSideRow += inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexRight - (WIDTH * inner) < WIDTH * HEIGHT
//                           && mask.data[indexRight - (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    rightSideRow -= inner;
//                }
//
//                if(rightSideRow > HEIGHT)
//                    rightSideRow = HEIGHT;
//
//                ///////////////////process edges/////////////////////
//                index = ((leftSideRow * WIDTH) + c) * 4;
//                for(inner = leftSideRow; inner < rightSideRow; inner++, index+=WIDTH_X_4 )
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//
//            }
//
//            break;
//        }
//    }
//    bg.copyTo(dss);
//}













//JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_processImageAddr(JNIEnv *env, jobject obj, jlong addr, jstring bgFileName)
//{
//    Mat &dss = *((Mat *) addr);
//    Mat mask(dss.size(), 24);
//
//    //load bg
//    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//    Mat bg = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
//    env->ReleaseStringUTFChars(bgFileName, nativeString);
//    cvtColor(bg, bg, COLOR_BGR2RGBA);
//
//    cvtColor(dss, mask, COLOR_RGBA2GRAY);
//    blur(mask, mask, Size(3, 3));
//    Canny(mask, mask, 10, 60);
//
//    int WIDTH = mask.cols, HEIGHT = mask.rows;
//
//    int column = WIDTH, row, y = 0, x, index, index2 = 1;
//    bool found(false);
//    int startCol, startRow, endRow;
//
//    //draw canny on bg for testing
//    for(int r=0;r<HEIGHT;r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for(int c=0;c<WIDTH;c++, index+=4, index2++)
//        {
//            if(mask.data[index2]==255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//        }
//    }
//
//    int rightSideRow, leftSideRow;
//    bool foundObject = false;
//    y=0;
//    for(int c=WIDTH, r;c > 0; c--, y++)
//    {
////        index = (WIDTH * HEIGHT * 4) - 4 - (c * 4);
//        index = (c * 4) - 4;
//        index2 = c - 1;
//        x = 0;
//        for(r=1;r<HEIGHT;r++, index+=(WIDTH*4), index2+=WIDTH, x++)
//        {
////            if(x < 400 && y < 200)
////            {
////                dss.data[index + 2]=255;
////                dss.data[index + 1]=0;
////                dss.data[index]=0;
////                mask.data[index2]=255;
////            }
//
////            if(index < (WIDTH*4) * 10.5f)
////            {
////                dss.data[index + 2]=255;
////                dss.data[index + 1]=0;
////                dss.data[index]=0;
//////                mask.data[index2]=255;
////                continue;
////            }
////            else break;
//            if(foundObject)
//            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 0;//dss.data[index + 1];
////                bg.data[index + 2] = 0;
//
//                if(mask.data[index2] == 255)
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//                else
//                {
//                    rightSideRow = r;
//                    break;
//                }
//            }
//
//            if(mask.data[index2] == 255)
//            {
//                foundObject = true;
//
//                startCol = column;
//                rightSideRow = leftSideRow = r;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//            bg.data[index] = 255;
//            bg.data[index + 1] = 0;
//            bg.data[index + 2] = 0;
//        }
//
//        if(foundObject)
//        {
//            rightSideRow = r;
//            int indexRight, indexLeft, inner;
//
//            while(c > 0)
//            {
//                c--;
//                indexRight = (rightSideRow * WIDTH) + c;
//                indexLeft = (leftSideRow * WIDTH) + c;
//
//                ///////////////////left side/////////////////////
//                inner = 1;
//                while ( indexLeft - (WIDTH * inner) > 0
//                        && mask.data[indexLeft - (WIDTH * inner)] == 255 )
//                {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    inner = 2;
//
//                    while (indexLeft - (WIDTH * inner) > 0
//                           &&
//                           mask.data[indexLeft - (WIDTH * inner)] == 255
//
//                            )
//                    {
//                        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d  %d  %d", indexLeft, (WIDTH * inner), c);
//
//                        inner++;
//                    }
//                    if(inner == 2)
//                        inner = 0;
//                    else inner--;
////                    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>1>> %d  %d  %d  %d  %d", indexLeft, (WIDTH * inner), c, leftSideRow, inner);
//
////                    inner--;
////                    if(inner == 1)
////                        inner == 0;
//                }
//
//
//                if (inner > 0) {
//                    leftSideRow -= inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexLeft + (WIDTH * inner) > 0
//                           && mask.data[indexLeft + (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    leftSideRow += inner;
//                }
//
//                if(leftSideRow < 0)
//                    leftSideRow = 0;
//
//                ///////////////////right side/////////////////////
//                inner = 1;
//                while (indexRight + (WIDTH * inner) > 0
//                       && mask.data[indexRight + (WIDTH * inner)] == 255) {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    inner = 2;
//                    while (indexRight + (WIDTH * inner) > 0
//                           && mask.data[indexRight + (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    if(inner == 2)
//                        inner = 0;
//                    else inner--;
//                }
//
//                if (inner > 0) {
//                    rightSideRow += inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexRight - (WIDTH * inner) < WIDTH * HEIGHT
//                           && mask.data[indexRight - (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    rightSideRow -= inner;
//                }
//
//                if(rightSideRow > HEIGHT)
//                    rightSideRow = HEIGHT;
//
//                ///////////////////process edges/////////////////////
//                index = ((leftSideRow * WIDTH) + c) * 4;
//                for(inner = leftSideRow; inner < rightSideRow; inner++, index+=(WIDTH*4) )
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//
//            }
//
//            break;
//        }
//    }
//    bg.copyTo(dss);
//}



//JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_processImageAddr(JNIEnv *env, jobject obj, jlong addr, jstring bgFileName)
//{
//    Mat &dss = *((Mat *) addr);
//    Mat mask(dss.size(), 24);
//
//
//    //load bg
//    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//    Mat bg = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
//    env->ReleaseStringUTFChars(bgFileName, nativeString);
//    cvtColor(bg, bg, COLOR_BGR2RGBA);
//
//    cvtColor(dss, mask, COLOR_RGBA2GRAY);
//    blur(mask, mask, Size(3, 3));
//    Canny(mask, mask, 10, 60);
//
//    int WIDTH = mask.cols, HEIGHT = mask.rows;
//
//    int column = WIDTH, row, y = 0, x, index, index2 = 1;
//    bool found(false);
//    int startCol, startRow, endRow;
//
//    //draw canny on bg
//    for(int r=0;r<HEIGHT;r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for(int c=0;c<WIDTH;c++, index+=4, index2++)
//        {
//            if(mask.data[index2]==255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//
//
//
//        }
//    }
//
//    int rightSideRow, leftSideRow;
//    bool foundObject = false;
//    y=0;
//    for(int c=WIDTH, r;c > 0; c--, y++)
//    {
////        index = (WIDTH * HEIGHT * 4) - 4 - (c * 4);
//        index = (c * 4) - 4;
//        index2 = c - 1;
//        x = 0;
//        for(r=1;r<HEIGHT;r++, index+=(WIDTH*4), index2+=WIDTH, x++)
//        {
////            if(x < 400 && y < 200)
////            {
////                dss.data[index + 2]=255;
////                dss.data[index + 1]=0;
////                dss.data[index]=0;
////                mask.data[index2]=255;
////            }
//
////            if(index < (WIDTH*4) * 10.5f)
////            {
////                dss.data[index + 2]=255;
////                dss.data[index + 1]=0;
////                dss.data[index]=0;
//////                mask.data[index2]=255;
////                continue;
////            }
////            else break;
//            if(foundObject)
//            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 0;//dss.data[index + 1];
////                bg.data[index + 2] = 0;
//
//                if(mask.data[index2] == 255)
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//                else
//                {
//                    rightSideRow = r;
//                    break;
//                }
//            }
//
//            if(mask.data[index2] == 255)
//            {
//                foundObject = true;
//
//                startCol = column;
//                rightSideRow = leftSideRow = r;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//            bg.data[index] = 255;
//            bg.data[index + 1] = 0;
//            bg.data[index + 2] = 0;
//        }
//
//        if(foundObject)
//        {
//            rightSideRow = r;
////            if(endRow > HEIGHT)
////                endRow = HEIGHT;
//            int indexRight, indexLeft, inner;
//
//
//            __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>>> %d  %d  %d", leftSideRow, rightSideRow, c);
//
//            while(c > 0)// && column < WIDTH - distance - 2)
//            {
//                c--;
//                indexRight = (rightSideRow * WIDTH) + c;
//                indexLeft = (leftSideRow * WIDTH) + c;
//
//                ///////////////////left side/////////////////////
//                inner = 1;
//                while ( indexLeft - (WIDTH * inner) > 0
//                       && mask.data[indexLeft - (WIDTH * inner)] == 255 )
//                {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    inner = 2;
//
//                    while (indexLeft - (WIDTH * inner) > 0
//                           &&
//                            mask.data[indexLeft - (WIDTH * inner)] == 255
//
//                            )
//                    {
//                        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d  %d  %d", indexLeft, (WIDTH * inner), c);
//
//                        inner++;
//                    }
//                    if(inner == 2)
//                        inner = 0;
//                    else inner--;
////                    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>1>> %d  %d  %d  %d  %d", indexLeft, (WIDTH * inner), c, leftSideRow, inner);
//
////                    inner--;
////                    if(inner == 1)
////                        inner == 0;
//                }
//
//
//                if (inner > 0) {
//                    leftSideRow -= inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexLeft + (WIDTH * inner) > 0
//                           && mask.data[indexLeft + (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    leftSideRow += inner;
//                }
//
//                if(leftSideRow < 0)
//                    leftSideRow = 0;
//
//                ///////////////////right side/////////////////////
//                inner = 1;
//                while (indexRight + (WIDTH * inner) > 0
//                       && mask.data[indexRight + (WIDTH * inner)] == 255) {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    inner = 2;
//                    while (indexRight + (WIDTH * inner) > 0
//                           && mask.data[indexRight + (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    if(inner == 2)
//                        inner = 0;
//                    else inner--;
//                }
//
//                if (inner > 0) {
//                    rightSideRow += inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexRight - (WIDTH * inner) < WIDTH * HEIGHT
//                           && mask.data[indexRight - (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    rightSideRow -= inner;
//                }
//
//                if(rightSideRow > HEIGHT)
//                    rightSideRow = HEIGHT;
//
//                index = ((leftSideRow * WIDTH) + c) * 4;
////
//                for(inner = leftSideRow; inner < rightSideRow; inner++, index+=(WIDTH*4) )
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//
//            }
//
//            break;
//        }
//    }
//
//    bg.copyTo(dss);
//
//    if(true)
//        return;
//
////    if(nextLeftRow < 1)
////        nextLeftRow = 1;
////
////    index = ((nextLeftRow * WIDTH) + column) * 4;
////
////    for(inner = nextLeftRow; inner < nextRightRow; inner++, index-=(WIDTH*4) )
////    {
////
//////                index2 = (inner * WIDTH) + column;
////
////        bg.data[index] = dss.data[index];
////        bg.data[index + 1] = dss.data[index + 1];
////        bg.data[index + 2] = dss.data[index + 2];
////
////    }
//
//
//    //    bg.copyTo(dss);
//
//
//
////    for(int i=0; i<)
//
//    for (;//int column = WIDTH, row, y = 0, x, index, index2 = 1;
//            column >= 0; column--, y++) // index = ((row * WIDTH) + column) * 4
//
////    for (column = 0; col//int column = WIDTH, row, y = 0, x, index, index2 = 1;
////            column >= 0; column--, y++) // index = ((row * WIDTH) + column) * 4
//    {
//        index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
//        index2 = (WIDTH * HEIGHT) - 1 - column;
//
//        for (row = HEIGHT - 1, x = 0; row > 0; row -= 1, index -= (WIDTH * 4), x++, index2-= WIDTH)//-= column)
//        {
////            if(true)
////            {
////                bg.data[index] = dss.data[index];
////                bg.data[index + 1] = dss.data[index + 1];
////                bg.data[index + 2] = dss.data[index + 2];
////                continue;
////            }
//            if(x<250 && y < 100)
//            {
//                dss.data[index] = 0;//dss.data[index];
//                dss.data[index + 1] = 0;//dss.data[index + 1];
//                dss.data[index + 2] = 255;
//
//                __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>>> %d  %d  %d", index, row, column);
//
//                continue;
//            }
//            else continue;
////            if(row < 140 && column < 40)
////            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 255;//dss.data[index + 1];
////                bg.data[index + 2] = 255;
////            }
//
//            if(found)
//            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 0;//dss.data[index + 1];
////                bg.data[index + 2] = 0;
//
//                if(mask.data[index2] == 255)
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//                else
//                {
//                    endRow = row;
//                    break;
//                }
//            }
//            if(mask.data[index2] == 255)
//            {
//                helper help;
//
//                try
//                {
//                    help.drawRecAroundPoint(bg.data, index, WIDTH);
//                }
//                catch (...)
//                {
//
//                }
//
//                found = true;
////                break;
////                dss.data[index] = 255;
////                dss.data[index + 1] = 255;
////                dss.data[index + 2] = 255;
//                startCol = column;
//                endRow = startRow = row;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//        }
//        if(found)
//            break;
//    }
//
//
//
//    if(found)
//    {
//        int nextLeftRow = startRow, nextRightRow = endRow;
////        int startRow = row;
//        int indexRight, indexLeft, inner;
//        int i;
//
//        const int distance = 2;
//
//        while(found && column > distance + 2)// && column < WIDTH - distance - 2)
//        {
//            column--;
//
////            index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
////            index2 = (WIDTH * HEIGHT) - 1 - column;
//
//
//
//            indexRight = (nextRightRow * WIDTH) + column;
//            indexLeft = (nextLeftRow * WIDTH) + column;
//
////            if(indexLeft < distance + 2) break;
////            if(indexRight < distance + 2) break;
////            if(indexRight > WIDTH - distance - 2) break;
////            if(indexLeft > WIDTH - distance - 2) break;
//
////            if(indexLeft > ((HEIGHT*WIDTH) + column)- (distance*WIDTH) )
////            {
////                nextLeftRow = HEIGHT;
////            }
////            else {
//
//            bool firstMiss;
//            i = 1;
//            while (indexLeft + (WIDTH * i) < WIDTH * HEIGHT
//                   && mask.data[indexLeft + (WIDTH * i)] == 255) {
//                i++;
//            }
//
////            i++;
////            while (indexLeft + (WIDTH * i) < WIDTH * HEIGHT
////                   && mask.data[indexLeft + (WIDTH * i)] == 255) {
////                i++;
////            }
////            i--;
//            i--;
//            if (i > 0) {
//                nextLeftRow -= i;
//            }
////                else {
////                    i = 1;
////                    while (indexLeft - (WIDTH * i) > 0
////                           && mask.data[indexLeft - (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextLeftRow += i;
////                }
//
//
//            i = 1;
//            while (indexRight - (WIDTH * i) > 0
//                   && mask.data[indexRight - (WIDTH * i)] == 255) {
//                i++;
//            }
////            i++;
////            while (indexRight - (WIDTH * i) > 0
////                   && mask.data[indexRight - (WIDTH * i)] == 255) {
////                i++;
////            }
////            i--;
//            i--;
//
//            if (i > 0) {
//                nextRightRow += i;
//            }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) < WIDTH * HEIGHT
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextRightRow -= i;
////                }
//
//
////            }
////
////            if(indexRight < WIDTH*distance)
////            {
////                nextRightRow = 0;
////            }
////            else
////            {
////
////                i = 1;
////                while (indexRight - (WIDTH * i) > 0
////                       && mask.data[indexLeft - (WIDTH * i)] == 255) {
////                    i++;
////                }
////                i--;
////
////                if (i > 0) {
////                    nextRightRow += i;
////                }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) > 0
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextLeftRow -= i;
////                }
////
////
////                i = 1;
////                while (indexRight - (WIDTH * i) > 0
////                       && mask.data[indexRight - (WIDTH * i)] == 255) {
////                    i++;
////                }
////                i--;
////
////                if (i > 0) {
////                    nextRightRow += i;
////                }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) < WIDTH * HEIGHT
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextRightRow -= i;
////                }
////            }
//
//
//
////                do {
////                    try {
////
////
////                        if (mask.data[indexLeft + (WIDTH * i)] == 255) // 4
////                        {
////                            nextLeftRow -= i;
////                            break;
////                        }
////                    }
////                    catch (...) {
////                        nextLeftRow = HEIGHT;
////                        break;
////                    }
////
////                } while (i-- > 1);
////                if(i<=1)
////                {
////                    i = distance;
////                    do {
////                        try {
////                            if (mask.data[indexLeft - (WIDTH * i)] == 255) // 4
////                            {
////                                nextLeftRow += i;
////                                break;
////                            }
////                        }
////                        catch (...) {
////                            nextLeftRow = 0;
////                            break;
////                        }
////
////                    } while (i-- > 1);
////                }
////            }
////            if(mask.data[indexLeft + WIDTH] == 255) // 4
////            {
////                nextLeftRow--;
////            }
////            else if(mask.data[indexLeft - WIDTH] == 255) // 6
////            {
////                nextLeftRow++;
////            }
//
////            if(indexRight < WIDTH*distance)
////            {
////                nextRightRow = 0;
////            }
////            else
////            {
////                i = distance;
////                do {
////                    try {
////                        if (mask.data[indexRight - (WIDTH * i)] == 255) // 4
////                        {
////                            nextRightRow -= i;
////                            break;
////                        }
////                    } catch (...) {
////                        nextRightRow = 0;
////                        break;
////                    }
////                } while (i-- > 1);
////
////                if(i<=1)
////                {
////                    i = distance;
////                    do {
////                        try {
////                            if (mask.data[indexRight + (WIDTH * i)] == 255) // 4
////                            {
////                                nextRightRow += i;
////                                break;
////                            }
////                        } catch (...) {
////                            nextRightRow = HEIGHT;
////                            break;
////                        }
////                    } while (i-- > 1);
////                }
////            }
//
//
//
////            if(mask.data[indexRight - WIDTH] == 255) // 4
////            {
////                nextRightRow--;
////            }
////            else if(mask.data[indexRight + WIDTH] == 255) // 6
////            {
////                nextRightRow++;
////            }
//
//
////                __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev23>>> %d  %d", nextLeftRow, nextRightRow);//,//dss.cols,
//
//            if(nextLeftRow < 1)
//                nextLeftRow = 1;
//
//            index = ((nextLeftRow * WIDTH) + column) * 4;
//
//            for(inner = nextLeftRow; inner < nextRightRow; inner++, index-=(WIDTH*4) )
//            {
//
////                index2 = (inner * WIDTH) + column;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//
////            bg.data[nextLeftRow] = 255;
////            bg.data[nextLeftRow + 1] = 255;
////            bg.data[nextLeftRow + 2] = 255;
////
////            bg.data[nextRightRow] = 255;
////            bg.data[nextRightRow + 1] = 255;
////            bg.data[nextRightRow + 2] = 255;
//
//        }
//    }
//
////    bg.copyTo(dss);
//
//}






















//JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_processImageAddr
//        (JNIEnv *env, jobject obj, jlong addr, jstring bgFileName) {
//    Mat &dss = *((Mat *) addr);
//    Mat mask(dss.size(), 24);
//
//
//    //load bg
//    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//    Mat bg = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
//    env->ReleaseStringUTFChars(bgFileName, nativeString);
//
//    cvtColor(bg, bg, COLOR_BGR2RGBA);
//
//
//
////    __android_log_print(ANDROID_LOG_ERROR, "tag","SimLev>>> %d  %d  %d", bg.type(), bg.rows, bg.cols);
////    if(true)
////    {
////        bg.copyTo(dss);
////        return;
////    }
//
//    cvtColor(dss, mask, COLOR_RGBA2GRAY);
//    blur(mask, mask, Size(3, 3));
//    Canny(mask, mask, 10, 60);
//
////    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev>>> %d  %d  %d   %d", dss.type(), dss.cols,
////                        mask.type(), mask.cols);
//
//
//    int WIDTH = mask.cols, HEIGHT = mask.rows;
//
//
//    int column = WIDTH, row, y = 0, x, index, index2 = 1;
//    bool found(false);
//    int startCol, startRow, endRow;
//
//    for(int r=0;r<HEIGHT;r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for(int c=0;c<WIDTH;c++, index+=4, index2++)
//        {
//            if(mask.data[index2]==255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//        }
//    }
//
//    for (;//int column = WIDTH, row, y = 0, x, index, index2 = 1;
//         column >= 0; column--, y++) // index = ((row * WIDTH) + column) * 4
//    {
//        index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
//        index2 = (WIDTH * HEIGHT) - 1 - column;
//
//        for (row = HEIGHT - 1, x = 0; row > 0; row -= 1, index -= (WIDTH * 4), x++, index2-= WIDTH)//-= column)
//        {
////            if(true)
////            {
////                bg.data[index] = dss.data[index];
////                bg.data[index + 1] = dss.data[index + 1];
////                bg.data[index + 2] = dss.data[index + 2];
////                continue;
////            }
////            if(x<200 && y < 200)
////            {
////                bg.data[index] = 0;//dss.data[index];
////                bg.data[index + 1] = 0;//dss.data[index + 1];
////                bg.data[index + 2] = 255;
////                continue;
////            }
////            else break;
////            if(row < 140 && column < 40)
////            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 255;//dss.data[index + 1];
////                bg.data[index + 2] = 255;
////            }
//
//            if(found)
//            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 0;//dss.data[index + 1];
////                bg.data[index + 2] = 0;
//
//                if(mask.data[index2] == 255)
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//                else
//                {
//                    endRow = row;
//                    break;
//                }
//            }
//            if(mask.data[index2] == 255)
//            {
//                helper help;
//
//                try
//                {
//                    help.drawRecAroundPoint(bg.data, index, WIDTH);
//                }
//                catch (...)
//                {
//
//                }
//
//                found = true;
////                break;
////                dss.data[index] = 255;
////                dss.data[index + 1] = 255;
////                dss.data[index + 2] = 255;
//                startCol = column;
//                endRow = startRow = row;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//        }
//        if(found)
//            break;
//    }
//
//
//
//    if(found)
//    {
//        int nextLeftRow = startRow, nextRightRow = endRow;
////        int startRow = row;
//        int indexRight, indexLeft, inner;
//        int i;
//
//        const int distance = 2;
//
//        while(found && column > distance + 2)// && column < WIDTH - distance - 2)
//        {
//            column--;
//
////            index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
////            index2 = (WIDTH * HEIGHT) - 1 - column;
//
//
//
//            indexRight = (nextRightRow * WIDTH) + column;
//            indexLeft = (nextLeftRow * WIDTH) + column;
//
////            if(indexLeft < distance + 2) break;
////            if(indexRight < distance + 2) break;
////            if(indexRight > WIDTH - distance - 2) break;
////            if(indexLeft > WIDTH - distance - 2) break;
//
////            if(indexLeft > ((HEIGHT*WIDTH) + column)- (distance*WIDTH) )
////            {
////                nextLeftRow = HEIGHT;
////            }
////            else {
//
//                bool firstMiss;
//                i = 1;
//                while (indexLeft + (WIDTH * i) < WIDTH * HEIGHT
//                       && mask.data[indexLeft + (WIDTH * i)] == 255) {
//                    i++;
//                }
//
////            i++;
////            while (indexLeft + (WIDTH * i) < WIDTH * HEIGHT
////                   && mask.data[indexLeft + (WIDTH * i)] == 255) {
////                i++;
////            }
////            i--;
//            i--;
//                if (i > 0) {
//                    nextLeftRow -= i;
//                }
////                else {
////                    i = 1;
////                    while (indexLeft - (WIDTH * i) > 0
////                           && mask.data[indexLeft - (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextLeftRow += i;
////                }
//
//
//                i = 1;
//                while (indexRight - (WIDTH * i) > 0
//                       && mask.data[indexRight - (WIDTH * i)] == 255) {
//                    i++;
//                }
////            i++;
////            while (indexRight - (WIDTH * i) > 0
////                   && mask.data[indexRight - (WIDTH * i)] == 255) {
////                i++;
////            }
////            i--;
//            i--;
//
//                if (i > 0) {
//                    nextRightRow += i;
//                }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) < WIDTH * HEIGHT
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextRightRow -= i;
////                }
//
//
////            }
////
////            if(indexRight < WIDTH*distance)
////            {
////                nextRightRow = 0;
////            }
////            else
////            {
////
////                i = 1;
////                while (indexRight - (WIDTH * i) > 0
////                       && mask.data[indexLeft - (WIDTH * i)] == 255) {
////                    i++;
////                }
////                i--;
////
////                if (i > 0) {
////                    nextRightRow += i;
////                }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) > 0
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextLeftRow -= i;
////                }
////
////
////                i = 1;
////                while (indexRight - (WIDTH * i) > 0
////                       && mask.data[indexRight - (WIDTH * i)] == 255) {
////                    i++;
////                }
////                i--;
////
////                if (i > 0) {
////                    nextRightRow += i;
////                }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) < WIDTH * HEIGHT
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextRightRow -= i;
////                }
////            }
//
//
//
////                do {
////                    try {
////
////
////                        if (mask.data[indexLeft + (WIDTH * i)] == 255) // 4
////                        {
////                            nextLeftRow -= i;
////                            break;
////                        }
////                    }
////                    catch (...) {
////                        nextLeftRow = HEIGHT;
////                        break;
////                    }
////
////                } while (i-- > 1);
////                if(i<=1)
////                {
////                    i = distance;
////                    do {
////                        try {
////                            if (mask.data[indexLeft - (WIDTH * i)] == 255) // 4
////                            {
////                                nextLeftRow += i;
////                                break;
////                            }
////                        }
////                        catch (...) {
////                            nextLeftRow = 0;
////                            break;
////                        }
////
////                    } while (i-- > 1);
////                }
////            }
////            if(mask.data[indexLeft + WIDTH] == 255) // 4
////            {
////                nextLeftRow--;
////            }
////            else if(mask.data[indexLeft - WIDTH] == 255) // 6
////            {
////                nextLeftRow++;
////            }
//
////            if(indexRight < WIDTH*distance)
////            {
////                nextRightRow = 0;
////            }
////            else
////            {
////                i = distance;
////                do {
////                    try {
////                        if (mask.data[indexRight - (WIDTH * i)] == 255) // 4
////                        {
////                            nextRightRow -= i;
////                            break;
////                        }
////                    } catch (...) {
////                        nextRightRow = 0;
////                        break;
////                    }
////                } while (i-- > 1);
////
////                if(i<=1)
////                {
////                    i = distance;
////                    do {
////                        try {
////                            if (mask.data[indexRight + (WIDTH * i)] == 255) // 4
////                            {
////                                nextRightRow += i;
////                                break;
////                            }
////                        } catch (...) {
////                            nextRightRow = HEIGHT;
////                            break;
////                        }
////                    } while (i-- > 1);
////                }
////            }
//
//
//
////            if(mask.data[indexRight - WIDTH] == 255) // 4
////            {
////                nextRightRow--;
////            }
////            else if(mask.data[indexRight + WIDTH] == 255) // 6
////            {
////                nextRightRow++;
////            }
//
//
////                __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev23>>> %d  %d", nextLeftRow, nextRightRow);//,//dss.cols,
//
//            if(nextLeftRow < 1)
//                nextLeftRow = 1;
//
//            index = ((nextLeftRow * WIDTH) + column) * 4;
//
//            for(inner = nextLeftRow; inner < nextRightRow; inner++, index-=(WIDTH*4) )
//            {
//
////                index2 = (inner * WIDTH) + column;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//
////            bg.data[nextLeftRow] = 255;
////            bg.data[nextLeftRow + 1] = 255;
////            bg.data[nextLeftRow + 2] = 255;
////
////            bg.data[nextRightRow] = 255;
////            bg.data[nextRightRow + 1] = 255;
////            bg.data[nextRightRow + 2] = 255;
//
//        }
//    }
//
//    bg.copyTo(dss);
//
//}





//    for(int row=0, index; row<480;row++)
//    {
//        index = (row * 640) * 4;
//        for(int col = 0; col<640;col++, index+=4)
//        {
//
////            dss.data[index] = 0;
////            dss.data[index + 1] = 0;
////            dss.data[index + 2] = 255;
//
//            if(mask.data[index + 1] == 255)
//            {
//                mask.data[index + 1] = 0;
//                mask.data[index + 2] = 255;
//            }
//
////            __android_log_print(ANDROID_LOG_ERROR, "tag","SimLev>>> %d  %d  %d", mask.data[index], mask.data[index + 1], mask.data[index + 2]);
//
//        }
//    }



//    for (int row = 0, index = 0; row < HEIGHT; row++) {
//        index = row * WIDTH;
//        for (int col = 0; col < WIDTH; col++, index++) {
//            if (col < 240 && row < 100) {
//                mask.data[index] = 255;
//            }
//        }
//    }

//    for (int row = 0, index = 0; row < HEIGHT; row++) {
//        index = row * WIDTH * 4;
//        for (int col = 0; col < WIDTH; col++, index += 4) {
//            if (col > 440 && row > 400) {
//                dss.data[index] = 255;
//                dss.data[index + 1] = 255;
//                dss.data[index + 2] = 255;
//            }
//        }
//    }




//    mask.copyTo(dss);


//    for(int column = WIDTH, row, index; column>0; column--)
//    {
//        index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
//    }

//    for(int column = WIDTH, row, y=0, x, index, index2 = 1; column>=0; column--, y++) // index = ((row * WIDTH) + column) * 4
//    {
//        index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
//        index2 -= 1;//(WIDTH * HEIGHT) - column;
//
//        for (row = HEIGHT - 1, x = 0; row > 0; row -= 1, index -= (WIDTH * 4), x++, index2++)//-= column)
//        {
//
//            if(index < 0 || index + 3 > ( WIDTH * HEIGHT * 4))
//            {
//                __android_log_print(ANDROID_LOG_ERROR, "tag","SimLev>>> %d  %d  %d   %d", index, ( WIDTH * HEIGHT * 4), row, column);
////                __android_log_print(ANDROID_LOG_ERROR, "tag","SimLev>>> %d  %d  %d", mask.data[index], mask.data[index + 1], mask.data[index + 2]);
//                return;
//            }
//            if(column< 20 && row < 20)
//                mask.data[index2]=255;
////            if(mask.data[index2] == 255)
////            {
////                dss.data[index] = 255;
////                dss.data[index + 1] = 255;
////                dss.data[index + 2] = 255;
////            }
//
//
////            if(mask.data[index + 1] == 255)
////            {
////                mask.data[index + 1] = 0;
////                mask.data[index + 2] = 255;
////            }
//        }
//
//    }

//  for(int row=0, index; row<640;row++)
//    {
//        index = (row * 480) * 4;
//        for(int col = 0; col<480;col++, index+=4)
//        {
//            int temp = bg.data[index];
//            bg.data[index] = bg.data[index + 2];
////            bg.data[index + 1] = 0;
//            bg.data[index + 2] = temp;
//
//
//        }
//    }