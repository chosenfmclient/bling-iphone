//
//  SIARecordButton.m
//  
//
//  Created by Michael Biehl on 2/7/16.
//
//

#import "SIARecordButton.h"

static CGFloat animationDuration = 0.0;

@interface SIARecordButton ()

@property (strong, nonatomic) UIView *redView;
@property (strong, nonnull) CAShapeLayer *redViewShapeLayer;
@property (strong, nonatomic) UIImageView *redViewImage;

@end

@implementation SIARecordButton


- (void) drawRect:(CGRect)rect {
    
//    self.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8].CGColor;
//    self.layer.borderWidth = self.bounds.size.width * .08;
    self.layer.cornerRadius = self.bounds.size.height / 2;
}

- (void) layoutSubviews {
    if (!self.redView) {
        self.redView = [[UIView alloc] initWithFrame:[self redViewRecordFrame]];
        self.redView.userInteractionEnabled = NO;
        self.redView.layer.cornerRadius = self.redView.bounds.size.height / 2;
        [self.redView.layer addSublayer:_redViewShapeLayer];
        if (!self.redViewImage)
        {
            self.redViewImage = [[UIImageView alloc] initWithFrame:self.redView.bounds];
            self.redViewImage.contentMode = UIViewContentModeCenter;
            self.redViewImage.backgroundColor = [UIColor clearColor];
            [self.redView addSubview:self.redViewImage];
        }
        [self setRedViewForRecord];
        [self addSubview:self.redView];
    }
}

- (CABasicAnimation *)frameAnimation {
    return [CABasicAnimation animationWithKeyPath:@"bounds"];
}

- (CABasicAnimation *)cornerAnimation {
    return [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
}

- (CABasicAnimation *)pathAnimation {
    return [CABasicAnimation animationWithKeyPath:@"path"];
}

- (CGRect)redViewRecordFrame {
    return CGRectMake(self.bounds.size.width*0.12,
                     self.bounds.size.width*0.12,
                     self.bounds.size.width - (self.bounds.size.width*0.12)*2,
                     self.bounds.size.height - (self.bounds.size.width*0.12)*2);

}

- (CGRect)redViewStopFrame {
    CGFloat stopSquareWidth = [self redViewRecordFrame].size.width*0.58;
    CGFloat selfCenterPoint = [self redViewRecordFrame].size.width / 2;
    CGRect redViewFrame = CGRectMake(selfCenterPoint - stopSquareWidth/2,
                                     selfCenterPoint - stopSquareWidth/2,
                                     stopSquareWidth,
                                     stopSquareWidth);
    return redViewFrame;
}

- (void)animateRedViewCornerRadius:(CGFloat)cornerRadius bounds:(CGRect)bounds{
    
//        CAAnimationGroup *animationGroup = [[CAAnimationGroup alloc] init];
//        CABasicAnimation *cornerAnimation = [self cornerAnimation];
//        cornerAnimation.toValue = [NSNumber numberWithFloat:cornerRadius];
//        
//        CABasicAnimation *frameAnimation = [self frameAnimation];
//        frameAnimation.toValue = [NSValue valueWithCGRect:bounds];
//        
//        animationGroup.animations = @[cornerAnimation, frameAnimation];
//        animationGroup.duration = animationDuration;
//        animationGroup.fillMode = kCAFillModeForwards;
//        animationGroup.removedOnCompletion = NO;
//        
//        [CATransaction begin]; {
//            [CATransaction setCompletionBlock:^{
                _redView.layer.cornerRadius = cornerRadius;
                _redView.layer.bounds = bounds;
//                [_redView.layer removeAllAnimations];
//            }];
//            [self.redView.layer addAnimation:animationGroup
//                                      forKey:@"frameAndCornerAnimation"];
//        }
//        [CATransaction commit];
    
}

//- (void)animateRedViewBackgroundColor:(UIColor *)color {
//    [UIView animateWithDuration:0.2
//                     animations:^{
//                         self.redView.backgroundColor = color;
//                     }];
//}

- (void) setRedViewForRecord {
    
//    [self setRedViewshapeLayerPath:[[self recordViewPathForRect:[self redViewRecordFrame]] CGPath]
//                             color:[[[SIAStyling defaultStyle] recordingRedColor] CGColor]];
    [UIView transitionWithView:self.redView
                      duration:0.15
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.redViewImage setImage:[UIImage imageNamed:@"icon_record_button_start.png"]];
                        self.redView.backgroundColor = [[SIAStyling defaultStyle] recordingRedColor];
                    } completion:nil];

}

// no animation for now :< maybe someday we can make it BEAUTIFUL
- (void) setRedViewshapeLayerPath:(CGPathRef)layerPath color:(CGColorRef)color {
    //CAAnimationGroup *animationGroup = [[CAAnimationGroup alloc] init];
   // CGRect bounds = [self redViewStopFrame];
    
    CGColorRetain(color);
    CGPathRetain(layerPath);
    
//    CABasicAnimation *pathAnimation = [self pathAnimation];
//    pathAnimation.toValue = (__bridge id)layerPath;
//    pathAnimation.duration = animationDuration;
//    pathAnimation.fillMode = kCAFillModeForwards;
//    pathAnimation.removedOnCompletion = NO;
//    
//    CABasicAnimation *colorAnimation = [CABasicAnimation animationWithKeyPath:@"fillColor"];
//    colorAnimation.toValue = (__bridge id)color;
//    colorAnimation.duration = animationDuration;
//    colorAnimation.fillMode = kCAFillModeForwards;
//    colorAnimation.removedOnCompletion = NO;
//    
////    animationGroup.animations = @[pathAnimation, colorAnimation];
////    animationGroup.duration = animationDuration;
////    animationGroup.fillMode = kCAFillModeForwards;
////    animationGroup.removedOnCompletion = NO;
//    
//    [CATransaction begin]; {
//        [CATransaction setCompletionBlock:^{
            [_redViewShapeLayer setPath:layerPath];
            _redViewShapeLayer.fillColor = color;
            CGPathRelease(layerPath);
            CGColorRelease(color);
//            [_redViewShapeLayer removeAllAnimations];
//        }];
//        [_redViewShapeLayer addAnimation:colorAnimation
//                                  forKey:@"colorAnimation"];
//        [_redViewShapeLayer addAnimation:pathAnimation
//                                  forKey:@"pathAnimation"];
//    }
//    [CATransaction commit];
}

- (void) setRedViewForPause {
    [UIView transitionWithView:self
                      duration:0.15
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.redViewImage setImage:[UIImage imageNamed:@"video_record_pause.png"]];
                        self.redView.backgroundColor = [[SIAStyling defaultStyle] recordingRedColor];
                    } completion:nil];
}

- (void)setRedViewForPhoto {
    [UIView transitionWithView:self
                      duration:0.15
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.redViewImage setImage:[UIImage imageNamed:@"record_photo.png"]];
                        self.redView.backgroundColor = [[SIAStyling defaultStyle] blingMainColor];
                    } completion:nil];
}

- (void) setRedViewForStop {
    [UIView transitionWithView:self
                      duration:0.15
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.redViewImage setImage:[UIImage imageNamed:@"icon_record_button_stop"]];
                        self.redView.backgroundColor = [UIColor clearColor];
                    } completion:nil];
}

- (UIBezierPath *)recordViewPathForRect:(CGRect)rect {
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect))];
    
    int numOfSections = 6;
    for (int i = 0; i < numOfSections; i++)
    {
        [path addArcWithCenter:CGPointMake(rect.size.width/2., rect.size.width/2.)
                        radius:rect.size.width/2.
                    startAngle:(i * M_PI / (numOfSections/2.))
                      endAngle:((i + 1) * M_PI / (numOfSections/2.))
                     clockwise:YES];
    }

    return path;
}

// here we draw a pause symbol using a bezier path
- (UIBezierPath *)pauseViewPathForRect:(CGRect)rect withRadius:(CGFloat)radius {
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(CGRectGetMaxX(rect), CGRectGetMidY(rect))];
    [path addLineToPoint:CGPointMake(path.currentPoint.x, CGRectGetMaxY(rect) - radius)];
    [path addArcWithCenter:CGPointMake(path.currentPoint.x - radius, path.currentPoint.y)
                    radius:radius
                startAngle:0
                  endAngle:M_PI
                 clockwise:YES];
    [path addLineToPoint:CGPointMake(path.currentPoint.x, CGRectGetMidY(rect))];
    [path addLineToPoint:CGPointMake(rect.origin.x + radius*2, path.currentPoint.y)];
    
    [path addLineToPoint:CGPointMake(path.currentPoint.x, CGRectGetMaxY(rect) - radius)];
    [path addArcWithCenter:CGPointMake(path.currentPoint.x - radius, path.currentPoint.y)
                    radius:radius
                startAngle:0
                  endAngle:M_PI
                 clockwise:YES];
    [path addLineToPoint:CGPointMake(path.currentPoint.x, rect.origin.y + radius)];
    [path addArcWithCenter:CGPointMake(path.currentPoint.x + radius, path.currentPoint.y)
                    radius:radius
                startAngle:M_PI
                  endAngle:0
                 clockwise:YES];
    [path addLineToPoint:CGPointMake(path.currentPoint.x, CGRectGetMidY(rect))];
    [path addLineToPoint:CGPointMake(CGRectGetMaxX(rect) - radius*2, path.currentPoint.y)];
    [path addLineToPoint:CGPointMake(path.currentPoint.x, rect.origin.y + radius)];
    [path addArcWithCenter:CGPointMake(path.currentPoint.x + radius, path.currentPoint.y)
                    radius:radius
                startAngle:M_PI
                  endAngle:0
                 clockwise:YES];
    [path closePath];
    
    return path;
}


@end
