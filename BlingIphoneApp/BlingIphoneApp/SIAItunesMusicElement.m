//
//  SIAItunesMusicElement.m
//  ChosenIphoneApp
//
//  Created by Joseph Nahmias on 10/04/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAItunesMusicElement.h"

@implementation SIAItunesMusicElement

+(RKObjectMapping *)objectMapping{
    
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[SIAItunesMusicElement class]];
    [objectMapping addAttributeMappingsFromArray:@[@"artistId", @"collectionId", @"trackId", @"artistViewUrl", @"collectionViewUrl", @"trackViewUrl", @"artistName", @"collectionName", @"trackName", @"previewUrl"]];
    [objectMapping addAttributeMappingsFromDictionary:@{@"artworkUrl100":@"thumbnailURL"}];
    
    return objectMapping;
}

@end
