//
//  UIView+UIView_SIAAdditions.h
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/3/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SIAAdditions)
- (void)sia_setSubviewsExclusiveTouch;
- (void)sia_roundCorners:(UIRectCorner)corners;
- (void)sia_roundCorners:(UIRectCorner)corners withRadii:(CGSize)radii;
- (void)sia_setBackgroundColorAnimated:(UIColor *)color;
- (id) getParentViewController;
@end
