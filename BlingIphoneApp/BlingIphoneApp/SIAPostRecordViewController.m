//
//  SIAPostRecordViewController.m
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 2/8/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAPostRecordViewController.h"
#import "SIAKeepOrNotViewController.h"
#import "SIAEditVideoViewController.h"
#import "PHAsset+Utilities.h"

//#import "BIAVideoPlayer.h"
#import "SIATagsViewController.h"
#import "SIARecordingShareViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <UIKit/UIKit.h>
#import "BlingIphoneApp-Swift.h"

#define LOGO_THRESHOLD_SIZE             (20.)
#define LOGO_WATERMARKED_VIDEO_FILE    @"watermarkVideo.mov"
#define CHOSEN_VIDEO_ALBUM_NAME                     @"Blingy"

NSString *const SIAVideoProcessingDidFinish = @"SIAVideoProcessingDidFinish";

@interface SIAPostRecordViewController ()  <PostRecordShareDelegate>

@property (strong, nonatomic) UIImage *thumbnail;
@property (strong, nonatomic) NSArray *postRecordViewControllers;
@property (nonatomic) NSUInteger currentScreenIdx;
@property (strong, nonatomic) UIImageView *frameView;
@property (strong, nonatomic) AVAssetExportSession *videoWithWatermarkAssetExport;
@property (nonatomic) BOOL goToShareAfterVideoDataLoads;

// copied from SIANavigationViewController ok ok ok bye))
@property (strong, nonatomic) UIImageView *navBarImage;
@property (nonatomic) BOOL shouldSaveToCameraRoll;

@property (strong, nonnull) UIAlertController *areYouSureAlert;


@end

@implementation SIAPostRecordViewController

- (instancetype)initWithRecordingItem:(SIARecordingItem *)recordingItem
                               player:(BIAVideoPlayer *)player
{
    self.recordingItem = recordingItem;
    self.player = player;
    self = [super initWithRootViewController:self.postRecordViewControllers.firstObject];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.frameView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        self.frameView.backgroundColor = [UIColor clearColor];
        self.frameView.contentMode = UIViewContentModeScaleAspectFit;
        [self.view insertSubview:self.frameView
                         atIndex:0];
        if (self.recordingItem.recordType == kRecordTypeBio)
        {
            [self startUpload];
        }
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super initWithCoder:aDecoder]) {
        NSArray *vcs = self.postRecordViewControllers;
        NSLog(@"Post Record initted with vcs: %@", vcs);
    }
    
    return self;
}

- (void)viewDidLoad {
    //[super viewDidLoad];
    _shouldSaveToCameraRoll = true;
    [self setNavigationBarHidden:NO animated:NO];
    self.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationBar setBackgroundImage:[[UIImage alloc]init]
                             forBarMetrics:UIBarMetricsDefault];
}

- (void)viewWillAppear:(BOOL)animated {
    if ([self.recordingItem.type isEqualToString:@"cameo"]) {
        PostBlingRecordViewController *postBlingVC = _postRecordViewControllers.firstObject;
        postBlingVC.setDisplayForcameo = YES;
    }
}

- (void)prepareForUpload:(void (^)(void))completion {
    __weak SIAPostRecordViewController *wSelf = self;
    [_recordingItem prepareForUploadWithCompletionHandler:^(BOOL completed) {
        if (completed) {
            SIAShareSheetViewController *shareVC = [wSelf shareVC];
            shareVC.objectType = @"video";
            shareVC.shareManager.object_id = wSelf.recordingItem.videoId;
            _recordingItem.videoUrl = wSelf.recordingItem.recordURL;
            shareVC.video = wSelf.recordingItem;
            shareVC.asset =  wSelf.player.currentItem.asset;
            if (_goToShareAfterVideoDataLoads) {
                [self next];
            }
            completion();
        }
        else {
            [wSelf showVideoCreationError];
        }
    }];
}

- (void)showVideoCreationError {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:@"Oops! Something went wrong on our end, would you like to retry?" preferredStyle:UIAlertControllerStyleAlert];
    __weak SIAPostRecordViewController *wSelf = self;
    [alert addAction:[UIAlertAction actionWithTitle:@"Retry"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * _Nonnull action) {
                                                [wSelf startUploadForPrivate:[wSelf.recordingItem.status isEqualToString:@"private"]];
                                            }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                              style:UIAlertActionStyleCancel
                                            handler:^(UIAlertAction * _Nonnull action) {
                                                
                                            }]];
    [self presentViewController:alert
                       animated:YES
                     completion:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [self.view sendSubviewToBack:self.frameView];
}

- (void)viewWillDisappear:(BOOL)animated {

}

- (void)readdTopView {
}

- (void)backEvent {
    [AmplitudeEvents logWithEvent: @"record:cancel" with: @{@"pagename":AmplitudeEvents.kUploadPageName}];
}

- (void)next {
    
    _currentScreenIdx++;
    
    if (_currentScreenIdx < self.postRecordViewControllers.count)
    {
        if (_currentScreenIdx == 1 && self.recordingItem.videoId.length < 1) {
            _currentScreenIdx--;
            _goToShareAfterVideoDataLoads = YES;
            PostBlingRecordViewController *postVC = (PostBlingRecordViewController *)self.viewControllers.firstObject;
            [postVC disableButtonsAndAddSpinner];
        }
        else {
            if (_currentScreenIdx == 1 && ![_recordingItem.status isEqual: @"secret"]) {
                [self saveToCameraRoll];
            }
            [self pushViewController:self.postRecordViewControllers[_currentScreenIdx]
                            animated:NO];
        }
    }
    else
    {
        [self dismissViewControllerAnimated:NO
                                 completion:^{
                                     [self.player pause];
                                     [VideoPlayerManager shared].shouldPlay = YES;
                                     //[self.recordVC.navigationController popViewControllerAnimated:YES];
                                 }];
    }
//    if (_currentScreenIdx == self.postRecordViewControllers.count - 2) {
//        [self startUpload];
//    }
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    
    if (![self.postRecordViewControllers containsObject:viewController])
    {
        [self.player pause];
    }
    [self addTransitionAnimation];
    [super pushViewController:viewController
                     animated:NO];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated {
    [self addTransitionAnimation];
    return [super popViewControllerAnimated:animated];
}

- (void)addTransitionAnimation {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.15;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.view.layer addAnimation:transition forKey:nil];
}

- (void)setViewControllers:(NSArray<UIViewController *> *)viewControllers animated:(BOOL)animated {
    for (UIViewController *vc in viewControllers)
    {
        if (![self.postRecordViewControllers containsObject:vc])
        {
            [self.player pause];
        }
    }
    [super setViewControllers:viewControllers
                     animated:animated];
}

- (void)back {
    
    __block UIViewController *poppedVC;
    
    if (_currentScreenIdx > 0)
    {
        poppedVC = [self popViewControllerAnimated:NO];
        // if view controllers outside the record flow have been pushed, don't decrement the index
        if ([self.postRecordViewControllers containsObject:poppedVC])
            _currentScreenIdx--;
    }
    else
    {
        [self.blingRecordVC showAreYouSureMessageWithClosure:^(BOOL goBack) {
            if (goBack) {
                poppedVC = [self popViewControllerAnimated:NO];
                //[self.recordVC backFromPost];
                
                if (self.blingRecordVC) {
                    [self.blingRecordVC reshoot];
                }
                if ([self.postRecordViewControllers containsObject:poppedVC])
                    _currentScreenIdx--;
            }
        }];
    }
   // else if ([self.postRecordViewControllers containsObject:self.viewControllers.lastObject])
      //  [self.player play];
}

- (void)close {
    __weak SIAPostRecordViewController *wSelf = self;
    [self showAreYouSureDialog:^(BOOL goBack) {
        
        if (!goBack) return;
        
        [VideoPlayerManager shared].shouldPlay = YES;
        [[SIAPushRegistrationHandler sharedHandler] recordingCompleted];
        
        [wSelf dismissViewControllerAnimated:NO
                                  completion:^{
                                      [wSelf.player pause];
                                      [wSelf.player replaceCurrentItemWithPlayerItem:nil];
                                      // [weakSelf.recordVC.navigationController popViewControllerAnimated:YES];
                                      if (wSelf.blingRecordVC) {
                                          [wSelf.blingRecordVC close];
                                      }
                                  }];
    }];
}

- (void)showAreYouSureDialog:(void (^_Nonnull)(BOOL))completion {
    
    if ([[SIAUploadVideo sharedUploadManager] isVideoReady]) {
        completion(YES);
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are you sure?"
                                                                   message:@"If you go back your video will continue to upload"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Stay here"
                                              style:UIAlertActionStyleCancel
                                            handler:^(UIAlertAction * _Nonnull action) {
                                                completion(NO);
                                            }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Go back"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * _Nonnull action) {
                                                completion(YES);
                                            }]];
    // if another VC is being presented, queue the alert to present after that one
    if (self.presentedViewController) {
        self.areYouSureAlert = alert;
    }
    else {
        [self presentViewController:alert
                           animated:YES
                         completion:nil];
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)thumbnailWasChosen:(UIImage *)thumbnail {
    self.thumbnail = thumbnail;
}

- (void)frameWasChosen:(NSURL *)frame
            frameIndex:(NSUInteger)frameIdx{
    if ([frame isKindOfClass:[NSNull class]]) frame = nil;
    self.recordingItem.frameIndex = frameIdx;
    
    [self.frameView sd_setImageWithURL:frame
                      placeholderImage:nil
                               options:SDWebImageRefreshCached
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                 [UIView transitionWithView:self.frameView
                                                   duration:0.1
                                                    options:UIViewAnimationOptionTransitionCrossDissolve
                                                 animations:^{
                                                     [self.frameView setImage:image];
                                                 } completion:^(BOOL finished){
                                                     [self.view sendSubviewToBack:self.frameView];
                                                 }];
                             }];
}

- (void)photoFilterWasApplied {
    [Utils dispatchOnMainThread:^{
        //[self.recordVC.photoImageView setImage:self.recordingItem.image];
    }];
}

- (void)recordingItemWasChanged:(SIARecordingItem *)recordingItem {
    self.recordingItem = recordingItem;
}

- (void)startUploadForPrivate:(BOOL)isPrivate {
    
    self.recordingItem.recordStatus = isPrivate ? @"private" : @"public";
    __weak SIAPostRecordViewController *wSelf = self;
    [self prepareForUpload:^{
        
        [SIAUploadVideo sharedUploadManager].uploadAsset = self.player.currentItem.asset;
        SIARecordingItem *recordingItem = wSelf.recordingItem;
        [wSelf.recordingItem startUploadVideoWithConvertHandler:nil
                                                  uploadHandler:^(BOOL uploaded) {
                                                      [recordingItem generateThumbnails:^(UIImage * _Nullable image) {
                                                          [[SIAUploadVideo sharedUploadManager] uploadThumbnail:image];
                                                      }];
                                                  }];
    }];
}

- (void)removeFile:(NSURL *)fileURL
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [fileURL path];
    if ([fileManager fileExistsAtPath:filePath]) {
        NSError *error;
        BOOL success = [fileManager removeItemAtPath:filePath error:&error];
        if (!success)
            ;
            //UIAlertController *alertController = [UIAlertController alloc] ini
    }
}

-(void) snapchatShareWasInitiated {
    if (_videoWithWatermarkAssetExport.status == AVAssetExportSessionStatusExporting) {
        [_videoWithWatermarkAssetExport cancelExport];
    }
    _shouldSaveToCameraRoll = NO;
}

-(void) saveToCameraRoll{
    // could not merge logo to video - notify the user that saving the video to camera roll has failed
//    BOOL saveToCameraRoll = NO;//UserDefaults.saveToCameraRoll;
//    if (!saveToCameraRoll) {
//        return;
//    }
    
    NSNumber *shouldSave = [[NSUserDefaults standardUserDefaults]objectForKey:SAVE_TO_CAMERA_ROLL];
    if (shouldSave != nil && !shouldSave.boolValue) {
        return;
    }
    
    BOOL isUplodedFileFromDevice = [self.recordingItem.recordType isEqualToString:ORIGINAL_UPLOAD_TYPE] || [self.recordingItem.recordType isEqualToString:COVER_UPLOAD_TYPE];
    if (isUplodedFileFromDevice) {
        return;
    }
    
    
    if ([self addLogoTovideo] == NO) {
        UIAlertView *statusAlert = [[UIAlertView alloc] initWithTitle:@"Can't save to camera roll"
                                                              message:@"Failed to create video."
                                                             delegate:self
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [statusAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
        return;
    }
    
    // video merged with log, save the new video with watermark to the camera roll
    NSString *exportPath = [NSTemporaryDirectory() stringByAppendingPathComponent:LOGO_WATERMARKED_VIDEO_FILE];
    NSURL    *exportUrl = [NSURL fileURLWithPath:exportPath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
    }
    
    _videoWithWatermarkAssetExport.outputFileType = AVFileTypeQuickTimeMovie;
    _videoWithWatermarkAssetExport.outputURL = exportUrl;
    _videoWithWatermarkAssetExport.shouldOptimizeForNetworkUse = YES;
    
    [self exportVideoWithWatermark];
}

- (void)exportVideoWithWatermark {
    [_videoWithWatermarkAssetExport exportAsynchronouslyWithCompletionHandler:^{
        NSLog(@"export status : %ld", (long)_videoWithWatermarkAssetExport.status);
//        self.compositedVideoURL = _videoWithWatermarkAssetExport.outputURL;
//        [[self shareVC] setCompositedDataWithVideoURL:self.compositedVideoURL
//                                         andPhotoData:nil];
        //            [[NSNotificationCenter defaultCenter] postNotificationName:SIAVideoProcessingDidFinish
        //                                                                object:nil
        //                                                              userInfo:@{@"assetURL":exportUrl}];
        
        if ([self.topViewController isKindOfClass:[SIAShareSheetViewController class]])
        {
            [((SIAShareSheetViewController*)self.topViewController).shareManager uploadVideoToFacebookAfterProcessingFinished:self.compositedVideoURL];
        }
        
        if (AVAssetExportSessionStatusCompleted == _videoWithWatermarkAssetExport.status) {
            
            NSLog(@"done processing video!");
            
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                // ALAsset is deprecatd from ios9.
                [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:SAVE_TO_CAMERA_ROLL];
                [self saveToAlbumiOS_9];
            }];
        }
        else if (AVAssetExportSessionStatusFailed == _videoWithWatermarkAssetExport.status && [[UIApplication sharedApplication] applicationState] != UIApplicationStateActive)
        {
            __block id obs = [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification
                                                                               object:nil
                                                                                queue:[NSOperationQueue mainQueue]
                                                                           usingBlock:^(NSNotification * _Nonnull note) {
                                                                               [self saveToCameraRoll];
                                                                               [[NSNotificationCenter defaultCenter] removeObserver:obs];
                                                                           }];
        }
    }];
}

- (void)saveImageToCameraRoll:(UIImage *)image {
//    [PHAsset saveImageToCameraRoll:image location:nil completionBlock:^(PHAsset *asset, BOOL success) {
//        if (success) {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your photo has been saved to your Camera Roll." preferredStyle:UIAlertControllerStyleAlert];
//            
//            [alert addAction:[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                ;
//            }]];
//            mainQueue(^{
//                [self presentViewController:alert animated:YES completion:nil];
//            });
//        } else {
//            UIAlertView *statusAlert;
//            if ([PHPhotoLibrary authorizationStatus] != PHAuthorizationStatusAuthorized) {
//                statusAlert = [[UIAlertView alloc] initWithTitle:@"Can't save to camera roll"
//                                                         message:@"Please enable Photos permissions: Settings > Blingy > Photos"
//                                                        delegate:self
//                                               cancelButtonTitle:@"OK"
//                                               otherButtonTitles: nil];
//                [statusAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
//                
//            } else {
//                
//                statusAlert = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                         message:@"Can't save to camera roll."
//                                                        delegate:self
//                                               cancelButtonTitle:@"OK"
//                                               otherButtonTitles: nil];
//                [statusAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
//            }
//        }
//        
//        
//    }];
}

- (UIImage *)getImage:(UIImage *)image withAlpha:(CGFloat) alpha {
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0f);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);
    
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    
    CGContextSetAlpha(ctx, alpha);
    
    CGContextDrawImage(ctx, area, image.CGImage);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(BOOL) addLogoTovideo {
    
    AVMutableComposition* mixComposition = self.player.currentItem.asset;
   
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:self.recordingItem.recordURL options:nil];
   
    // create the layer with the watermark image
    NSLog(@"%@", [videoAsset tracksWithMediaType:AVMediaTypeVideo]);
    
    if (!videoAsset || ![[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject]) return NO;
    AVAssetTrack *originalVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    CGSize videoSize = [originalVideoTrack naturalSize];
    CGAffineTransform transform = [[[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject] preferredTransform];
    CGSize transformedVideoSize = CGSizeApplyAffineTransform(videoSize, transform);
    transformedVideoSize = CGSizeMake (fabs(transformedVideoSize.width), fabs(transformedVideoSize.height));

    // sorts the layer in proper order
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, transformedVideoSize.width, transformedVideoSize.height);
    videoLayer.frame = CGRectMake(0, 0, transformedVideoSize.width, transformedVideoSize.height);
    [parentLayer addSublayer:videoLayer];
   
    if (self.frameView.image) {//if frame add frame, if not add logo
        UIImage *myImage = self.frameView.image;
        CALayer *frameLayer = [CALayer layer];
        frameLayer.contents = (id)myImage.CGImage;
        
        CGFloat frameWidth = transformedVideoSize.width;
        CGFloat frameHeight = transformedVideoSize.height;
        frameLayer.frame = CGRectMake(0,0,frameWidth,frameHeight);
        [parentLayer addSublayer:frameLayer];
    } else {
        UIImage *myImage = [UIImage imageNamed:[self.recordingItem.recordType isEqualToString:@"cameo"] ? @"watermark_duet" : @"watermark7"];
        CALayer *logoLayer = [CALayer layer];
        logoLayer.contents = (id)myImage.CGImage;
        logoLayer.contentsScale = 2.0;
        logoLayer.rasterizationScale = 2.0;
        logoLayer.shouldRasterize = YES;
        CGFloat imageRatio = myImage.size.height / myImage.size.width;
        // set the logo size
        // .width is 9% from the video width for 16:9 videos which are most of our videos.
        CGFloat logoWidth = transformedVideoSize.width > transformedVideoSize.height ? transformedVideoSize.width * 0.25 : transformedVideoSize.height * 0.25;
        CGFloat logoHeight = logoWidth * imageRatio;
        logoWidth = roundf(logoWidth);
        logoHeight = roundf(logoHeight);
        logoLayer.frame = CGRectMake(0,
                                     0, //dont know why but for some reason it takes it from the bottom (sephi)
                                     logoWidth,
                                     logoHeight);
        [parentLayer addSublayer:logoLayer];
    }
    
    
    // create the composition and add the instructions to insert the layer
    AVMutableVideoComposition* videoComp = [AVMutableVideoComposition videoComposition];
    videoComp.renderSize =  transformedVideoSize;
    videoComp.frameDuration = originalVideoTrack.minFrameDuration;
    videoComp.animationTool = [AVVideoCompositionCoreAnimationTool
                               videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer
                               inLayer:parentLayer];
    videoComp.renderScale = 1.0;

    // instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mixComposition duration]);
    AVAssetTrack *videoTrack = [[mixComposition tracksWithMediaType:AVMediaTypeVideo] firstObject];
    AVMutableVideoCompositionLayerInstruction* layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    
    AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    [layerInstruction setTransform:transform atTime:kCMTimeZero];
    instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
    videoComp.instructions = [NSArray arrayWithObject: instruction];

    // create asset exporter. this will be used to export the file (to camera roll for now on [self saveToCameraRoll])
    _videoWithWatermarkAssetExport = [[AVAssetExportSession alloc]
                                      initWithAsset:mixComposition
                                      presetName:AVAssetExportPresetHighestQuality];
    if (nil == _videoWithWatermarkAssetExport) return NO;
    
    _videoWithWatermarkAssetExport.videoComposition = videoComp;
    
    return YES;
}

- (AVMutableComposition *)createCompositionFromRecorded:(NSURL *)recordedURL andPlayback:(AVAsset *)playbackAsset videoTimeRange:(CMTimeRange)range {
    
    
    AVMutableComposition* composition = [AVMutableComposition composition];
    
    AVURLAsset* recordedAsset = [[AVURLAsset alloc]initWithURL:recordedURL options:nil];
    
    NSError* error = NULL;
    
//    if (CMTimeGetSeconds(range.duration) < 0.5 || !CMTimeCompare(range.duration, kCMTimeInvalid)) {
//        range = CMTimeRangeMake(kCMTimeZero, recordedAsset.duration);
//    }
    
    range = CMTimeRangeMake(kCMTimeZero,recordedAsset.duration);
    
    [composition insertTimeRange:range ofAsset:recordedAsset atTime:kCMTimeZero error:&error];
    
    if (playbackAsset != nil && [playbackAsset tracksWithMediaType:AVMediaTypeAudio].count > 0){
        AVMutableCompositionTrack *compositionPlaybackAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        CMTime audioStartTime = kCMTimeZero;//self.recordVC.addSevenSeconds ? CMTimeMakeWithSeconds(7, 60) : kCMTimeZero;
        
        [compositionPlaybackAudioTrack insertTimeRange:CMTimeRangeMake(audioStartTime, range.duration)
                                               ofTrack:[[playbackAsset tracksWithMediaType:AVMediaTypeAudio]objectAtIndex:0]
                                                atTime:kCMTimeZero
                                                 error:&error];
        NSLog(@"volume %f",compositionPlaybackAudioTrack.preferredVolume );
        
    }
    return composition;
}

-(BOOL) saveToAlbumiOS_9 {
    if (!_shouldSaveToCameraRoll) {
        return NO;
    }
    
    [PHAsset saveVideoAtURL:_videoWithWatermarkAssetExport.outputURL location:nil completionBlock:^(PHAsset *asset, BOOL success) {
        if (!_shouldSaveToCameraRoll) {
            return;
        }
        __block UIAlertController *statusAlert;

        if (success) {
            [asset saveToAlbum:@"Blingy"
               completionBlock:^(BOOL success) {
                   [self shareVC].videoIsInCameraRoll = YES;
                   if (!_shouldSaveToCameraRoll) {
                       return;
                   }
                   // we got the video on our custom album!
                   NSString *messageStr;
                   NSString *mediaNameStr;
                   if (self.recordingItem.song_name.length > 0)
                   {
                       mediaNameStr = [NSString stringWithFormat:@"\"%@\"", self.recordingItem.song_name];
                   }
                   else
                   {
                       mediaNameStr = [NSString stringWithFormat:@"Your %@", self.recordingItem.mediaType];
                   }
                   messageStr = [NSString stringWithFormat:@"%@ has been saved to your Camera Roll. You can disable this option in Settings.",
                                 mediaNameStr];
                   
                   statusAlert = [UIAlertController alertControllerWithTitle:@"Success"
                                                                     message:messageStr
                                                              preferredStyle:UIAlertControllerStyleAlert];
                   
                   [statusAlert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil]];
                    
                   [Utils dispatchOnMainThread:^{
                       [self presentViewController:statusAlert
                                          animated:YES
                                        completion:nil];
                   }];
               }];
            
        } else {
            // ERROR
            if ([PHPhotoLibrary authorizationStatus] != PHAuthorizationStatusAuthorized) {
                statusAlert = [UIAlertController alertControllerWithTitle:@"Can't save to camera roll"
                                                                  message:@"Please enable Photos permissions: Settings > Blin.gy > Photos"
                                                           preferredStyle:UIAlertControllerStyleAlert];
                [statusAlert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleCancel
                                                              handler:nil]];
                [Utils dispatchOnMainThread:^{
                    [self presentViewController:statusAlert
                                       animated:YES
                                     completion:nil];
                }];
            
            } else {
                
                statusAlert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                  message:@"Can't save to camera roll."
                                                           preferredStyle:UIAlertControllerStyleAlert];
                [statusAlert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleCancel
                                                              handler:nil]];
                [Utils dispatchOnMainThread:^{
                    [self presentViewController:statusAlert
                                       animated:YES
                                     completion:nil];
                }];
                
            }
        }
    }];
    
    return YES;
}

-(void)saveToAlbumiOS_8{
 //  if (SYSTEM_VERSION_GREATER_THAN_IOS8) return;
    
    __block UIAlertView *statusAlert = [UIAlertView alloc];
    
//    ALAssetsLibrary *assetLib = [[ALAssetsLibrary alloc] init];
//    [assetLib writeVideoAtPathToSavedPhotosAlbum:_videoWithWatermarkAssetExport.outputURL
//                                 completionBlock:^(NSURL *assetURL, NSError *error) {
//                                     
//                                     if ([ALAssetsLibrary authorizationStatus] != PHAuthorizationStatusAuthorized) {
//                                         statusAlert = [[UIAlertView alloc] initWithTitle:@"Can't save to camera roll"
//                                                                                  message:@"Please enable Photos permissions: Settings > Blingy > Photos"
//                                                                                 delegate:self
//                                                                        cancelButtonTitle:@"OK"
//                                                                        otherButtonTitles: nil];
//                                         [statusAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
//                                         return;
//                                     }
//                                     
//                                     if (error) {
//                                         
//                                         
//                                         
//                                         statusAlert = [[UIAlertView alloc] initWithTitle:@"Can't save to camera roll."
//                                                                                  message:error.localizedFailureReason
//                                                                                 delegate:self
//                                                                        cancelButtonTitle:@"OK"
//                                                                        otherButtonTitles: nil];
//                                         [statusAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
//                                     }
//                                     
//                                     NSString *assetURLString = assetURL.absoluteString;
//                                     assetURLString = [assetURLString stringByAppendingString:@"&ext=mov"];
//                                     assetURL = [NSURL URLWithString:assetURLString];
//                                     self.recordingItem.cameraRollURL = assetURL;
//                                     // add the asset to our custom 'Blingy' photo album
//                                     [assetLib addAssetURL:assetURL toAlbum:CHOSEN_VIDEO_ALBUM_NAME withCompletionBlock:^(NSError *error) {
//                                         if (error)
//                                         {
//                                             
//                                             statusAlert = [[UIAlertView alloc] initWithTitle:@"Can't save to camera roll."
//                                                                                      message:error.localizedFailureReason
//                                                                                     delegate:self
//                                                                            cancelButtonTitle:@"OK"
//                                                                            otherButtonTitles: nil];
//                                             [statusAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
//                                             
//                                         }
//                                         else
//                                         {
//                                             
//                                             // we got video on our custom album!
//                                             SIAAppDelegate *appDelegate = (SIAAppDelegate *)[[UIApplication sharedApplication] delegate];
//                                             NSString *messageStr;
//                                             
//                                             
//                                             
//                                             if ([self.recordingItem.recordType isEqualToString:@"bio"])
//                                             {
//                                                 messageStr = @"Your bio has been saved to your Camera Roll.";
//                                             }
//                                             else if ([self.recordingItem.recordType isEqualToString:@"response"])
//                                             {
//                                                 messageStr = @"Your review has been saved to your Camera Roll.";
//                                             }
//                                             else
//                                             {
//                                                 messageStr = [NSString stringWithFormat:@"\"%@\" has been saved to your Camera Roll.",
//                                                               self.recordingItem.song_name];
//                                             }
//                                             
//                                             // different message if it's the first time
//                                             if (appDelegate.firstTimeSaved2CameraRoll)
//                                             {
//                                                 appDelegate.firstTimeSaved2CameraRoll = NO;
//                                                 messageStr = [messageStr stringByAppendingString:@" You can disable this option in Settings"];
//                                             }
//                                             
//                                             statusAlert = [[UIAlertView alloc] initWithTitle:@"Success"
//                                                                                      message:messageStr
//                                                                                     delegate:self
//                                                                            cancelButtonTitle:@"OK"
//                                                                            otherButtonTitles: nil];
//                                             [statusAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
//                                         }
//                                         
//                                         
//                                     }];
//                                 }];

}

- (void)stopRepeatingPlayback
{
}

- (void)repeatPlaybackFrom:(CGFloat)startTime to:(CGFloat)endTime
{
}

- (void)uploadCompletedWithVideo:(SIAVideo*)video {
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"playerStoryboard" bundle:nil];
//    SIAVideoPlayerPlaybackViewController *player = [sb instantiateViewControllerWithIdentifier:@"VideoPlayerView"];
//    SIAAppDelegate *appDelegate = sia_AppDelegate;
//    UIViewController *viewController = [appDelegate getTopViewController];
//    video.playlistArray = @[video.urlUrl].mutableCopy;
//    player.playOneVideo = YES;
//    player.video = video;
//    if ([viewController isMemberOfClass:[SIAVideoPlayerPlaybackViewController class]]) {
//        ((SIAVideoPlayerPlaybackViewController *)viewController).playAnotherVideo = YES;
//    }
//    
//    SIANavigationViewController *navController;
//    if (self.blingRecordVC) {
//        navController = self.blingRecordVC.navigationController;
//    } else  {
//        navController = self.recordVC.navigationController;
//    }
//    NSMutableArray *controllers = [navController.rootNavigationControllerViewControllers mutableCopy];
//    UIViewController *myVC = self.recordVC ?: self.blingRecordVC;
//    [controllers replaceObjectAtIndex:[controllers indexOfObject:myVC]
//                           withObject:player];
//    
//    [self dismissViewControllerAnimated:NO completion:^{
//        [navController setRootNavigationControllerViewControllers:controllers
//                                                         animated:YES];
//    }];
}

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    __weak SIAPostRecordViewController *wSelf = self;
    [super dismissViewControllerAnimated:flag
                              completion:^{
                                  if (completion)
                                      completion();
                                  if (wSelf.areYouSureAlert)
                                  {
                                      [wSelf presentViewController:wSelf.areYouSureAlert
                                                         animated:flag
                                                       completion:nil];
                                  }
                              }];
    [self.player play];
    [self.player repeatPlayback];
}

- (void)dealloc {
    self.postRecordViewControllers = nil;
    self.player = nil;
    self.recordingItem.image = nil;
    self.recordingItem = nil;
    self.videoWithWatermarkAssetExport = nil;
    self.frameView = nil;
}

- (NSArray *)postRecordViewControllers {
    
    if (_postRecordViewControllers) return _postRecordViewControllers;
    
    NSMutableArray <SIABasePostRecordViewController *> *postRecordViewControllers = [NSMutableArray arrayWithObjects:self.viewControllers.firstObject ,[self shareVC], [self uploadVC], nil]; // these appear for ALL video types

    _postRecordViewControllers = [NSArray arrayWithArray:postRecordViewControllers];
    
    return _postRecordViewControllers;
}

- (void)generatePhotoThumbnailForPhotoReview {
    CGImageRef thumbCG = [self.recordingItem.image CGImage];
    UIImage *squareThumb = [UIImage imageWithCGImage:[self cropImageToSquare:thumbCG]];
    CGRect scaledRect = CGRectZero;
    scaledRect.size = squareThumb.size;
    CGFloat scale = 1;
    if (scaledRect.size.width > 192)
    {
        scale = 192 / scaledRect.size.width;
    }
    scaledRect = CGRectApplyAffineTransform(scaledRect, CGAffineTransformMakeScale(scale, scale));
    UIGraphicsBeginImageContextWithOptions(scaledRect.size, NO, squareThumb.scale);
    [squareThumb drawInRect:scaledRect];
    squareThumb = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self thumbnailWasChosen:squareThumb];
}

- (CGImageRef)cropImageToSquare:(CGImageRef)image {
    size_t height = CGImageGetHeight(image);
    size_t width = CGImageGetWidth(image);
    
    NSUInteger squareSide = MIN(width, height);
    NSUInteger longerSide = MAX(width, height);
    
    CGRect squareRect = CGRectMake(0,
                                   longerSide / 2 - squareSide / 2,
                                   squareSide,
                                   squareSide);
    
    return CGImageCreateWithImageInRect(image, squareRect);
}

- (UIStoryboard *)recordingStoryboard {
    return [UIStoryboard storyboardWithName:@"RecordingStoryboard" bundle:nil];
}

- (UIStoryboard *)shareStoryboard {
    return [UIStoryboard storyboardWithName:@"SIAShare" bundle:nil];
}

- (UIStoryboard *)recordFlowStoryboard {
    return [UIStoryboard storyboardWithName:@"recordFlowStoryboard" bundle:nil];
}

- (SIAKeepOrNotViewController *)keepOrNotVC {
    SIAKeepOrNotViewController *keepOrNotVC = [[self recordFlowStoryboard] instantiateViewControllerWithIdentifier:@"keepOrNotVC"];
    keepOrNotVC.delegate = self;
    return keepOrNotVC;
}

- (SIAEditVideoViewController *)editVideoVC {
    SIAEditVideoViewController *editVideoVC = [[self recordFlowStoryboard] instantiateViewControllerWithIdentifier:@"editVideoVC"];
    editVideoVC.delegate = self;
    editVideoVC.player = self.player;
    editVideoVC.recordingItem = self.recordingItem;
    return editVideoVC;
}

- (SIATagsViewController *)tagsVC {
    SIATagsViewController *tagsVC = [[self recordFlowStoryboard] instantiateViewControllerWithIdentifier:@"tagsVC"];
    tagsVC.delegate = self;
    tagsVC.previewParent = self;
    tagsVC.recordingItem = self.recordingItem;
    tagsVC.tagsViewType = SIATagsViewTypePostRecord;
    return tagsVC;
}

- (SIAShareSheetViewController*) shareVC {
    for (UIViewController *vc in _postRecordViewControllers) {
        if ([vc isKindOfClass:[SIAShareSheetViewController class]]) {
            return (SIAShareSheetViewController *)vc;
        }
    }
    SIAShareSheetViewController *shareVC = [[self shareStoryboard] instantiateInitialViewController];
    shareVC.objectType = @"video";
    shareVC.isPostRecording = YES;
    shareVC.postRecordDelegate = self;
    return shareVC;
}

- (SIARecordingShareViewController *)uploadVC {
    SIARecordingShareViewController *uploadVC = [[self recordFlowStoryboard] instantiateViewControllerWithIdentifier:@"shareVC"];
    uploadVC.previewParent = self;
    uploadVC.delegate = self;
    return uploadVC;
}

- (NSString *)photoOrVideo {
    return self.recordingItem.mediaType;
}

@end
