//
//  AmplitudeEvents.swift
//  BlingIphoneApp
//
//  Created by Zach on 16/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import Amplitude_iOS


@objc class AmplitudeEvents: NSObject {
    
    static let kHomePageName = "home"
    static let kVideoHomePageName = "videohome"
    static let kDiscoverPageName = "discover"
    static let kFollowersPageName = "followers"
    static let kPostRecordPageName = "review"
    static let kProfilePageName = "profile"
    static let kRecordPageName = "record"
    static let kFollowingPageName = "following"
    static let kUploadPageName = "upload"
    
    var pendingView: (() -> Void)?
    
    static let shared : AmplitudeEvents = {
        let instance = AmplitudeEvents()
        return instance
    }()
    
    static func log(event: String) {
        Amplitude.instance().logEvent(event)
    }
    
    static func log(event: String, with properties: [String:Any]) {
        Amplitude.instance().logEvent(event, withEventProperties: properties)
    }
    
    static func logUser(property: [String:Any]) {
        Amplitude.instance().setUserProperties(property)
    }
    
    
    func logVideoView(videoID: String,
                      pageName: String,
                      position: String? = nil,
                      videoName: String,
                      artistName: String) {
        var amplitudeProps = ["object_id": videoID,
                              "pagename":pageName,
                              "video_name" : videoName,
                              "artist_name" : artistName]
        if let _ = position {
            amplitudeProps["pos"] = position
        }

        pendingView = {
            Amplitude.instance().logEvent("video:view",
                                          withEventProperties: amplitudeProps)
            RateTheAppViewController.viewedVideo()
        }
        
        Utils.dispatchAfter(3) {[weak self] in
            self?.pendingView?()
        }
    }
}
