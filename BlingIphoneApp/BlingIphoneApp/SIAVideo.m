//
//  SIAVideo.m
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 20/07/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAVideo.h"
#import "SIANetworking.h"
#import "SIAObjectManager.h"

#define BASE_VIDEO_API          @"api/v1/videos/%@"
#define VIDEO_PATH_WITH_TOKEN   @"/api/v1/videos/%@?access_token=%@"
#define VIDEO_PLACEHOLDER_IMAGE @"video-thumb-placeholder.png"

@interface SIAVideo ()

// from preset class
@property (nonatomic, strong) void (^handler)(void);
@property(strong, readwrite, nonatomic) UIImage *thumbnailImage;

@end

@implementation SIAVideo

+ (NSDictionary*) getMappingDict {
    return @{@"video_id": @"videoId",
             @"type": @"type",
             @"media_type":@"mediaType",
             @"status": @"status",
             @"date": @"date",
             @"song_name": @"song_name",
             @"total_comments": @"total_comments",
             @"artist_name": @"artist_name",
             @"genre": @"genre",
             @"thumbnail": @"thumbnailUrl",
             @"intro_url": @"animatedURL",
             @"url": @"videoUrl",
             @"total_likes":@"total_likes",
             @"tags":@"tags",
             @"first_frame_url":@"firstFrameUrl",
             @"song_clip_url" : @"song_clip_url",
             @"song_image_url": @"song_image_url",
             @"clip_id" : @"clip_id",
             @"parent_id" : @"parent_id"
             };
}

+(RKDynamicMapping *)objectMapping{
    
    RKDynamicMapping *dynamicMapping = [[RKDynamicMapping alloc] init];
    [dynamicMapping setObjectMappingForRepresentationBlock:^RKObjectMapping *(id representation) {
        RKObjectMapping *videoMapping = [SIAVideo videoMapping];
        if ([[representation valueForKey:@"type"] isEqualToString:@"cameo"]) {
            [videoMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"parent"
                                                                                         toKeyPath:@"parent"
                                                                                       withMapping:[SIAVideo objectMapping]]];
        }
        
        return videoMapping;
    }];

    
    return dynamicMapping;
}

+(RKObjectMapping *)videoMapping {
    RKObjectMapping *videoMapping = [RKObjectMapping mappingForClass:[SIAVideo class]];
    
    // get field names when server is ready
    [videoMapping addAttributeMappingsFromDictionary:[SIAVideo getMappingDict]];
    
    
    // define SIAUser object mapping
    RKObjectMapping *userMapping = [SIAUser objectMapping];
    
    
    // define relationship mapping
    [videoMapping addPropertyMapping:[RKRelationshipMapping
                                      relationshipMappingFromKeyPath:kChosenUserKeyStr
                                      toKeyPath:kChosenUserKeyStr
                                      withMapping:userMapping]];
    return videoMapping;
}

+ (RKObjectMapping *)requestMapping {
    return [[SIAVideo videoMapping] inverseMapping];
}

-(AVPlayerItem *)playerItem {
    if (!_playerItem) {
        _playerItem = [AVPlayerItem playerItemWithURL:self.videoUrl];
    }
    return _playerItem;
}

+ (void) getDataForVideoId:(NSString*)videoId
     withCompletionHandler:(void(^)(SIAVideo *video))handler{
    [RequestManager
     getRequest:[NSString stringWithFormat:BASE_VIDEO_API, videoId]
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         handler(mappingResult.firstObject);
         
     }
     failure:^(RKObjectRequestOperation *operation, NSError *error) {
         NSLog(@"What do you mean by 'there are no videos?': %@", error);
     }
     showFailureDialog:YES];
    
}

- (BOOL)isPhoto
{
    return [self.mediaType isEqualToString:@"photo"];
}

- (BOOL)isVideo
{
    return [self.mediaType isEqualToString:@"video"];
}

- (BOOL)isFeatured
{
    return [self.status isEqualToString:@"featured"];
}

- (BOOL)isDailyPick
{
    return [self.status isEqualToString:@"daily_pick"];
}

- (id)copyWithZone:(NSZone *)zone {
    
    SIAVideo *newVideo = [[[self class] allocWithZone:zone] init];
    
    if (newVideo)
    {
        newVideo.videoId = _videoId;
        newVideo.type = _type;
        newVideo.mediaType = _mediaType;
        newVideo.status = _status;
        newVideo.date = _date;
        newVideo.song_name = _song_name;
        newVideo.artist_name = _artist_name;
        newVideo.genre = _genre;
        newVideo.thumbnailUrl = _thumbnailUrl;
        newVideo.thumbnailImage = _thumbnailImage;
        newVideo.videoUrl = _videoUrl;
        newVideo.firstFrameUrl = _firstFrameUrl;
        newVideo.tags = _tags;
        newVideo.user = _user;
        newVideo.playerItem = _playerItem;
        newVideo.playbackURL = _playbackURL;
        newVideo.total_likes = _total_likes;
        newVideo.total_comments = _total_comments;
        newVideo.animatedURL = _animatedURL;
    }
    return newVideo;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@> video_id = %@ user_name = %@", NSStringFromClass([self class]), self.videoId, self.user.fullName];
}

- (BOOL) isMyVideo {
    return [self.user.user_id isEqualToString:[BIALoginManager sharedInstance].myUser.user_id];
}

- (void) deleteVideoWithCompletionHandler: (void(^_Nullable)(BOOL success))completion {
    __weak SIAVideo* wSelf = self;
    [RequestManager
     deleteRequest:[NSString stringWithFormat:BASE_VIDEO_API, self.videoId]
     object:self
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         // delete video from list when going back to previous screen
         //TODO add notification that this video was deleted
         wSelf.status = @"deleted";
         if (completion) {
             completion(true);
         }
     } failure:^(RKObjectRequestOperation *operation, NSError *error) {
         if (completion) {
             completion(false);
         }
         NSLog(@"delete error: %@", error);
     }
     showFailureDialog:YES];
}

- (void) changeVideoStatusTo: (NSString *) status withCompletionHandler: (void(^_Nullable)(BOOL success))completion {
    self.status = status;
    [RequestManager
     putRequest:[NSString stringWithFormat:BASE_VIDEO_API, self.videoId]
     object:[[VideoStatus alloc] initWithStatus:status]
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         if (completion) {
             completion(true);
         }
     }
     failure:^(RKObjectRequestOperation *operation, NSError *error) {
         if (completion) {
             completion(false);
         }
         NSLog(@"change video status put error: %@", error);}
     showFailureDialog:YES
     ];
}

- (BOOL) isPrivate {
    return ![self.status isEqualToString:@"public"];
}

- (BOOL) isLikedByMe {
    BOOL liked =  [CoreDataManager.shared videoIsLikedByMeWithVideoID: _videoId];
    return liked;
}

- (BOOL)isCameo {
    return [self.type isEqualToString:@"cameo"];
}

- (void)toggleLikeWithCompletion: (void (^)())completion
          shouldChangeLikesValue: (BOOL)shouldChangeValue
           withAmplitudePageName: (NSString*) pageName {
    if (self.videoIsBeingLiked) {
        return;
    }
    self.videoIsBeingLiked = true;
    if (self.isLikedByMe) {
        [SIALike removeLikeFromVideo:self.videoId
                      withCompletion:^{
                          self.videoIsBeingLiked = false;
                          [CoreDataManager.shared deleteLikeFor:_videoId];
                          [AmplitudeEvents logWithEvent:@"like" with:@{@"pagename":pageName, @"object_id":_videoId}];
                          [AmplitudeEvents logUserWithProperty:@{@"likes":@([CoreDataManager.shared countTotalLikes])}];
                          if (shouldChangeValue) {
                              self.total_likes = [NSString stringWithFormat:@"%ld", (self.total_likes.integerValue - 1)];
                          }
                          if (completion) {
                              completion();
                          }
                      }];
    } else {
        [SIALike addLikeToVideo:self.videoId
                 withCompletion:^{
                     self.videoIsBeingLiked = false;
                     [CoreDataManager.shared saveLikeFor:_videoId];
                     [AmplitudeEvents logWithEvent:@"unlike" with:@{@"pagename":pageName, @"object_id":_videoId}];
                     [AmplitudeEvents logUserWithProperty:@{@"likes":@([CoreDataManager.shared countTotalLikes])}];
                     if(shouldChangeValue) {
                         self.total_likes = [NSString stringWithFormat:@"%ld", (self.total_likes.integerValue + 1)];
                     }
                     if (completion) {
                         completion();
                     }
                 }];
    }
}

- (NSURL *)thumbnailURL {
    return _song_image_url;
}

- (void)setTotal_likes:(NSString *)total_likes {
    NSString *oldVal = _total_likes.copy;
    _total_likes = total_likes;
    if (![_total_likes isEqualToString:oldVal]) {
        [self totalsDidChange];
    }
}

- (void)setTotal_comments:(NSString *)total_comments {
    NSString *oldVal = _total_comments.copy;
    _total_comments = total_comments;
    if (![_total_comments isEqualToString:oldVal]) {
        [self totalsDidChange];
    }
}

- (NSString *)totalCommentsStr {
    if (_total_comments) {
        return [NSString shorthandNumberFrom:_total_comments.integerValue];
    }
    return nil;
}

- (NSString *)totalLikesStr {
    if (_total_likes) {
        return [NSString shorthandNumberFrom:_total_likes.integerValue];
    }
    return nil;
}

- (NSURL *)firstFrameUrl {
    return _firstFrameUrl ?: _thumbnailURL;
}

- (void)totalsDidChange {
    if ([self.delegate respondsToSelector:@selector(videoTotalsDidChange:)]) {
        [self.delegate videoTotalsDidChange:self];
    }
}

@end
