//
//  SIATermAndConditionsViewController.h
//  ChosenIphoneApp
//
//  abstract: show the user terms and condition details
//
//  Created by Roni Shoham on 19/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIATermAndConditionsViewController : SIABaseViewController<UIPrintInteractionControllerDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *termsText;

@end
