//
//  VideoPlayerManager.swift
//  BlingIphoneApp
//
//  Created by Zach & Michael on 07/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

/**
 * Hello again...
 * This is the VideoPlayerManager - a singleton that holds a video player
 * This player should be used EVERYWHERE that we are playing a video from a URL
 * The class handles adding a buffering icon when needed, creating the AV Objects, and deallocating them
 *
 * Key methods:
 * setNewPlayerView(view: UIView, videoURL: URL, withDelegate delegate: VideoPlayerManagerDelegate?)
 *      * Use this method to play a video on a view. If the video was only paused it will resume
 *        The delegate is optional. Use it only if you need to know when the video ended or the asset was created
 *
 * removeVideo()
 *      * Use this to stop a video. It will be reset to the beginning and the video will be hidden.
 *        If the same video is then played again it will start from the beginning without needing to buffer
 *        Note: This may be called while the video is buffering to prevent it from playing when ready
 *
 * player.pause()
 *      * If you simply want to pause a video without resetting it then call pause on the player
 *
 */

import UIKit
import AVKit

@objc protocol VideoPlayerManagerDelegate: class {
    func playerDidReachEnd()
    @objc optional func playerDidPlay()
    @objc optional func newAssetWasCreated(_ asset: AVAsset)
}

@objc class VideoPlayerManager: NSObject {
    var currentPlayerView: UIView?
    var currentURL: URL?
    var player: BIAVideoPlayer
    var playerLayer: AVPlayerLayer
    let playerView = UIView()
    var playerStatusObserverContext = 0
    var itemStatusObserverContext = 0
    var playerBoundaryTimeObserver: Any?
    var didPlayObserver: Any?
    var stopWhenTimeObserverFires = true
    weak var delegate: VideoPlayerManagerDelegate?
    var spinner: SIASpinner?
    var failureCount = 0
    var shouldPlay = false
    var isLoading = false
    
    static let shared : VideoPlayerManager = {
        let instance = VideoPlayerManager()
        return instance
    }()
    
    func setNewPlayerView(view: UIView, asset: AVAsset, withDelegate delegate: VideoPlayerManagerDelegate? = nil, shouldLimitTime: Bool = true, withSpinner: Bool = false) {
        setNewPlayerView(view: view,
                         videoURL: nil,
                         asset: asset,
                         withDelegate: delegate,
                         shouldLimitTime: shouldLimitTime,
                         usingOption: .stream,
                         withSpinner: withSpinner)
    }
    
    func setNewPlayerView(view: UIView, videoURL: URL, withDelegate delegate: VideoPlayerManagerDelegate? = nil, shouldLimitTime: Bool = true, usingOption option: VideoPlayerAssetManagerOptions = .stream, withSpinner: Bool = true) {
        AmplitudeEvents.shared.pendingView = nil
        setNewPlayerView(view: view,
                         videoURL: videoURL,
                         asset: nil,
                         withDelegate: delegate,
                         shouldLimitTime: shouldLimitTime,
                         usingOption: option,
                         withSpinner: withSpinner)
    }
    
    fileprivate func setNewPlayerView(view: UIView, videoURL: URL? = nil, asset: AVAsset? = nil, withDelegate delegate: VideoPlayerManagerDelegate? = nil, shouldLimitTime: Bool = true, usingOption option: VideoPlayerAssetManagerOptions = .stream, withSpinner: Bool = true) {
        
        guard viewIsInsideTopController(view) else { return }
        self.delegate = delegate
        self.stopWhenTimeObserverFires = shouldLimitTime
        
        //If this url was already prepared
        if ((currentURL != nil && videoURL == currentURL) || asset == player.currentItem?.asset)
            && (player.status == .readyToPlay && player.currentItem?.status == .readyToPlay) {
                currentPlayerView = view
                Utils.dispatchOnMainThread {[weak self] in
                    guard let sSelf = self else { return }
                    
                    guard sSelf.playerIsInsideTopViewController() else {
                        print("PLAYER IS NOT INSIDE TOP VC")
                        return
                    }
                    //make sure the player is visisble
                    sSelf.attatchPlayer(to: view)
                    
                    //resume playback
                    if sSelf.shouldPlay {
                        sSelf.player.play()
                        sSelf.delegate?.playerDidPlay?()
                    }
                }
                return
        }
        
        currentPlayerView = view
        CATransaction.begin()
        CATransaction.setAnimationDuration(0)
        playerLayer.frame = (currentPlayerView?.layer.bounds)!
        //playerView.frame = (currentPlayerView?.bounds)!
        CATransaction.commit()
        
        shouldPlay = true
        
        //Add spinner
        if (withSpinner) {
            Utils.dispatchOnMainThread { [weak self] in
                self?.addSpinnerToView(view)
            }
        }
        
        Utils.dispatchOnMainThread { [weak self] in
            guard let sSelf = self else { return }
            sSelf.player.pause()
            sSelf.player.replaceCurrentItem(with: nil)
        }
        
        let itemWasCreated: (AVPlayerItem?) -> Void = {(item) in
            Utils.dispatchOnMainThread { [weak self] in
                guard let sSelf = self else { return }
                sSelf.player.replaceCurrentItem(with: item)
                NotificationCenter.default.removeObserver(self!)
                NotificationCenter.default.addObserver(
                    self!,
                    selector: #selector(self!.playerDidReachEnd),
                    name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                    object: self!.player.currentItem
                )
                if item?.status == .readyToPlay && sSelf.playerIsInsideTopViewController() {
                    sSelf.spinner?.stopAnimation()
                    self?.isLoading = false
                    sSelf.player.play()
                    sSelf.delegate?.playerDidPlay?()
                } else {
                    sSelf.player.currentItem?.addObserver(sSelf, forKeyPath: "status", options: .new, context: &sSelf.itemStatusObserverContext)
                }
                sSelf.attatchPlayer(to: view)
            }
        }
        
        if let _ = videoURL {
            currentURL = videoURL
            createPlayerItemFor(view: view, url: videoURL!, withCompletion: itemWasCreated, usingOption: option)
        } else if let _ = asset {
            currentURL = nil
            createPlayerItemFor(view: view,
                                asset: asset!,
                                withCompletion: itemWasCreated)
        }
    }
    
    func layoutVideo() {
        guard let view = currentPlayerView else { return }
        NSLayoutConstraint.deactivate(playerView.constraints)
        CATransaction.begin()
        CATransaction.setAnimationDuration(0)
        playerLayer.frame = view.bounds
//        playerView.frame = view.bounds
        view.addSubview(self.playerView)
        playerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        playerView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        playerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        playerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        view.setNeedsLayout()
        view.layoutIfNeeded()
        CATransaction.commit()
    }
    
    func isPlaying(_ url: URL?, on view: UIView?) -> Bool {
        return view == currentPlayerView && currentURL == url && player.rate > 0
    }
    
    override init() {
        //init properties
        player = BIAVideoPlayer()
        playerLayer = AVPlayerLayer(player: player)
        playerView.translatesAutoresizingMaskIntoConstraints = false
        super.init()
        stopAfterDuration(15)
        stopSpinnerWhenVideoPlays()
        //configure player & playerLayer
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.pause
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerView.layer.addSublayer(playerLayer)

    }
    
    func playerIsInsideTopViewController() -> Bool {
        guard let _ = currentPlayerView else { return false }
        return viewIsInsideTopController(currentPlayerView!)
    }
    
    func viewIsInsideTopController(_ view: UIView) -> Bool {
        if let vc = view.getParentViewController() as? UIViewController {
            return Utils.viewControllerIsTop(vc)
        }
        return false
    }
    
    fileprivate func attatchPlayer(to view: UIView) {
        Utils.dispatchOnMainThread { [weak self] in
            guard let sSelf = self else { return }
            NSLayoutConstraint.deactivate(sSelf.playerView.constraints)
            if let spinner = sSelf.spinner {
                view.insertSubview(sSelf.playerView, belowSubview: spinner)
            } else {
                view.addSubview(sSelf.playerView)
            }
            sSelf.playerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
            sSelf.playerView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
            sSelf.playerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            sSelf.playerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            //sSelf.playerView.frame = view.bounds
            sSelf.playerView.isHidden = false
            sSelf.playerLayer.isHidden = false
        }
    }
    
    fileprivate func createPlayerItemFor(view: UIView, url: URL, withCompletion completion: @escaping (_ item: AVPlayerItem?) -> Void, usingOption option: VideoPlayerAssetManagerOptions = .stream) {
        //create player item
        Utils.dispatchInBackground { [weak self] in
            self?.removeTimeObserver()
            VideoPlayerAssetManager.shared.prepareVideoAt(url, with: { (asset) in
                guard let _ = asset else {
                    completion(nil)
                    return
                }
                Utils.dispatchOnMainThread {
                    guard let _ = self else {
                        completion(nil)
                        return
                    }
                    let newItem = VideoPlayerManagerPlayerItem(asset: asset!)
                    completion(newItem)
                    self?.delegate?.newAssetWasCreated?(asset!)
                }
            }, usingOption: option)
        }
    }
    
    fileprivate func createPlayerItemFor(view: UIView, asset: AVAsset, withCompletion completion: @escaping (_ item: AVPlayerItem?) -> Void) {
        //create player item
        Utils.dispatchInBackground {
            let newItem = VideoPlayerManagerPlayerItem(asset: asset)
            completion(newItem)
        }
    }
    
    fileprivate func addSpinnerToView(_ view: UIView) {
        //add spinner to view
        if (spinner == nil) {
            spinner = SIASpinner(frame: getSpinnerFrameFor(view: view))
            spinner?.tintColor = UIColor.white
        } else {
            spinner?.frame = getSpinnerFrameFor(view: view)
        }
        view.addSubview(spinner!)
        view.layoutIfNeeded()
        Utils.dispatchAfter(2.0) {[weak self] in
            guard let _ = self else { return }
            if self!.isLoading {
                self!.spinner?.startAnimation()
            }
        }
        isLoading = true
    }
    
    func getSpinnerFrameFor(view: UIView) -> CGRect {
        let spinnerSize: CGFloat = 90
        return CGRect(x: view.bounds.midX - (spinnerSize / 2), y: view.bounds.midY - (spinnerSize / 2), width: spinnerSize, height: spinnerSize)
    }
    
    func removeVideo() {
        isLoading = false
        if (player.rate <= 0) { // player isnt playing
            shouldPlay = false
            spinner?.stopAnimation()
            return
        }
        
        player.pause()
        player.cancelPendingPrerolls()
        Utils.dispatchInBackground { [weak self] in
            self?.player.seek(to: kCMTimeZero)
        }
        Utils.dispatchOnMainThread { [weak self] in
            self?.playerView.isHidden = true
            self?.spinner?.stopAnimation()
        }
    }
    
    func removePlayerView() {
        playerView.removeFromSuperview()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if ((keyPath == "status" && context == &playerStatusObserverContext && (object as? AVPlayer) == player) || // player is ready
            (keyPath == "status" && context == &itemStatusObserverContext && (object as? AVPlayerItem) == player.currentItem)) // current item is ready
        {
            switch player.status {
            case .failed:
                print("failed to load video")
                guard failureCount < 5 else {
                    Utils.dispatchOnMainThread {[weak self] in
                        self?.spinner?.stopAnimation()
                        self?.isLoading = false
                    }
                   
                    failureCount = 0
                    return
                }
                
                failureCount += 1
                
                guard let view = currentPlayerView else {
                    Utils.dispatchOnMainThread {[weak self] in
                        self?.spinner?.stopAnimation()
                        self?.isLoading = false
                    }
                    failureCount = 0
                    return
                }
                
                guard let url = currentURL else { return }
                self.createPlayerItemFor(view: view, url: url, withCompletion: {[weak self] (item) in
                   // Utils.dispatchOnMainThread { [weak self] in
                        guard let sSelf = self else { return }
                        sSelf.player.replaceCurrentItem(with: item)
                        sSelf.player.currentItem?.addObserver(sSelf, forKeyPath: "status", options: .new, context: &sSelf.itemStatusObserverContext)
                        sSelf.attatchPlayer(to: view)
                    //}
                })
                break
            case .readyToPlay:
                failureCount = 0
                Utils.dispatchOnMainThread {[weak self] in
                    guard let _ = self else { return }
                    print("READY TO PLAY")
                    self!.player.preroll(atRate: 1, completionHandler: { [weak self] (complete) in
                        guard let _ = self else { return }
                        if complete {
                            guard self!.shouldPlay && self!.playerIsInsideTopViewController() else {
                                print("NOT IN TOP VC")
                                self!.currentURL = nil
                                return
                            }
                            self!.playerLayer.frame = (self!.currentPlayerView?.bounds)!
                            self!.spinner?.stopAnimation()
                            self?.isLoading = false
                            print("PLAYING YA DORK")
                            self!.player.play()
                            self!.delegate?.playerDidPlay?()
                        }
                    })
                    self!.playerView.isHidden = false
                }
                break
            default:
                break
            }
        }
    }
    
    private func createNewAsset(_ url: URL, with completion: ((_ asset: AVURLAsset?) -> Void)?) {
        let asset = AVURLAsset(url: url)
        var nullableCompletion = completion // so we can nil it once it's been called once
        asset.loadValuesAsynchronously(forKeys: ["playable"]) { [weak self] in
            guard let _ = self, let _ = nullableCompletion else { return }
            var error: NSError? = nil
            let status = asset.statusOfValue(forKey: "playable", error: &error)
            switch status {
            case .loaded:
                nullableCompletion!(asset)
                nullableCompletion = nil
                break
            case .failed: fallthrough
            case .cancelled: fallthrough
            default:
                //we're fucked
                nullableCompletion!(nil)
                nullableCompletion = nil
                break
            }
        }
    }
    private func removeTimeObserver() {
        Utils.dispatchOnMainThread {[weak self] in
            //self?.player.removeTimeObserver(self?.playerBoundaryTimeObserver)
            self?.stopWhenTimeObserverFires = false
            self?.playerBoundaryTimeObserver = nil
            self?.didPlayObserver = nil
        }
    }
    
    fileprivate func stopAfterDuration(_ duration: Float64) {
        //guard let _ = player.currentItem else { return }
        print("ADDING TIME OBSERVER")
        Utils.dispatchOnMainThread {[weak self] in
            
            self?.stopWhenTimeObserverFires = true
            self?.playerBoundaryTimeObserver = self?.player.addBoundaryTimeObserver(forTimes: [NSValue(time: CMTimeMakeWithSeconds(duration, 1000))],
                                                                                    queue: DispatchQueue.main,
                                                                                    using: {[weak self] in
                                                                                        guard (self?.stopWhenTimeObserverFires)! else { return }
                                                                                        
                                                                                        self?.player.pause()
                                                                                        self?.playerDidReachEnd()
                })
        }
    }
    
    fileprivate func stopSpinnerWhenVideoPlays() {
        //guard let _ = player.currentItem else { return }
        print("ADDING TIME OBSERVER")
        Utils.dispatchOnMainThread {[weak self] in
            
            self?.stopWhenTimeObserverFires = true
            self?.didPlayObserver = self?.player.addBoundaryTimeObserver(forTimes: [NSValue(time: CMTimeMakeWithSeconds(0.4, 1000))],
                                                                                    queue: DispatchQueue.main,
                                                                                    using: {[weak self] in
                                                                                        self?.spinner?.stopAnimation()
            })
        }
    }
    
    func playerDidReachEnd() {
        player.seek(to: kCMTimeZero)
        delegate?.playerDidReachEnd()
    }
    
    deinit {
        player.removeObserver(self, forKeyPath: "status", context: &playerStatusObserverContext)
    }
}

