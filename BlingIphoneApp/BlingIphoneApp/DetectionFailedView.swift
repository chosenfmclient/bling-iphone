//
//  DetectionFailedView.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 2/28/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

import UIKit

protocol DetectionFailedViewDelegate {
    func failedViewDidDismiss()
}


class DetectionFailedView: UIView {

    @IBOutlet weak var actionLabel: UILabel!
    var delegate: DetectionFailedViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tag = DetectionFailedView.viewTag()
    }
    
    static func viewTag() -> Int {
        return 1234
    }
    
    func show(on view: UIView!, forReason reason: DetectionResponseS) {
        actionLabel.bia_rounded()
        let font = UIFont(name: "Roboto",
                          size: 16.0)
        let shadow = NSShadow()
        shadow.shadowColor = UIColor.black
        shadow.shadowBlurRadius = 1.0
        shadow.shadowOffset = CGSize(width: 0.0, height: 0.0)
        let strokeTextAttributes = [
            NSForegroundColorAttributeName : UIColor.white,
            NSShadowAttributeName : shadow as Any,
            NSFontAttributeName : font as Any
            ] as [String : Any]
        actionLabel.attributedText = NSAttributedString(string: actionText(for: reason),
                                                        attributes: strokeTextAttributes)
        alpha = 0
        view.addSubview(self)
        frame = view.bounds
        
        UIView.animate(withDuration: 0.2,
                       animations: {[weak self] in
                        self?.alpha = 1
        }) { [weak self] (completed) in
            self?.dismiss(after: 4.0)
        }
    }

    func dismiss(after delay: TimeInterval = 0) {
        UIView.animate(withDuration: 0.2,
                       delay: delay,
                       animations: {[weak self] in
                        self?.alpha = 0
        }) {[weak self] (no) in
            self?.removeFromSuperview()
            self?.delegate?.failedViewDidDismiss()
        }
    }
    
    private func actionText(for failureReason: DetectionResponseS) -> String {
        
        switch failureReason {
        case .highLight:
            return "Protip: Try lower lighting!"
        case .lowLight:
            return "Protip: Try brighter, natural lighting!"
        case .tooManyEdges, .noChromaColors:
            return "Protip: Try a totally blank wall!"
        case .noFaceDetection:
            return "Move into the frame"
        default:
            print("%d", failureReason)
            return "Protip: use natural light & blank background!"
            
        }
    }
}
