//
//  SIARecordingShareViewController.m
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 18/09/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIARecordingShareViewController.h"
#import "SIAUser.h"
#import <AppsFlyerLib/AppsFlyerTracker.h>

#define ANIMATION_DURATION (0.25)
#define SERVER_URL_GOOGLE_STORAGE @"http://storage.googleapis.com/"
#define MEDIA_STROAGE_URL @"http://media.chosen.fm/"

#define BIO_VIDEO_TYPE_STR @"bio"

static void *uploaderFinishedUploadingThumbObservationContext = &uploaderFinishedUploadingThumbObservationContext;
static void *uploaderFinishedUpladQualityConvertObservationContext = &uploaderFinishedUpladQualityConvertObservationContext;
static void *uploaderPercentUpdatedObservationContext = &uploaderPercentUpdatedObservationContext;
static void *uploaderDataModelThumbnailUrlObservationContext = &uploaderDataModelThumbnailUrlObservationContext;
static void *uploaderDataModelVideoUrlObservationContext = &uploaderDataModelVideoUrlObservationContext;


@interface SIARecordingShareViewController ()
@property (weak, nonatomic) IBOutlet SIASpinner *uploadSpinner;

@property (weak, nonatomic) IBOutlet UIView *activityIndicatorView;
@property (strong, nonatomic) SIAVideo *video;
@property (weak, nonatomic) IBOutlet UIButton *goToVideoHomeButton;
@property (strong, nonatomic) SIAUploadVideo *uploader;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) NSString *thumbnailURLToShare;
@property (strong, nonatomic) NSString *videoURLToUploadForFacebook;
@property (strong, nonatomic) UIView *waitToShareSpinnerView;
@property (nonatomic) BOOL thumbnailUploadFinished;
@property (nonatomic) BOOL videoConvertFinished;
@property (nonatomic) BOOL shareFacebookClickedBeforeThumbUploaded;
@property (nonatomic) BOOL shareFacebookObserverFlag;
@property (nonatomic) BOOL bioThumbnailObserverFlag;
@property (strong, nonatomic) SIAUser *recordingUser;
@property (nonatomic, strong) NSString* videoUrlString;
@property (nonatomic) BOOL shareFacebookPressed;
@property (nonatomic) BOOL alreadyInShare;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (nonatomic) BOOL uploadToFacebookAfterServerUploadCompleted;

@end

@implementation SIARecordingShareViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.uploader = [SIAUploadVideo sharedUploadManager];
    self.uploader.delegate = self;
   // self.previewParent = (SIARecorderPreviewPlayerViewController *)self.parentViewController;
    self.goToVideoHomeButton.layer.cornerRadius = self.shareButton.layer.cornerRadius = self.goToVideoHomeButton.bounds.size.height / 2;
    
    self.goToVideoHomeButton.enabled = self.uploader.isVideoReady;
//    self.goToVideoHomeButton.alpha = 0.5;
    
    if ([_previewParent.recordingItem.recordType isEqualToString:kRecordTypeBio]) {
        
        [_goToVideoHomeButton setTitle:@"GO TO YOUR PROFILE" forState:UIControlStateNormal];
    }

    _shareFacebookObserverFlag = YES;
    
    [self.navigationController clearNavigationItemLeftButton];
    [self.navigationController clearNavigationItemRightButton];
    
    [self.navigationController setNavigationItemTitle:@"Uploading video"];
}

- (void) viewWillAppear:(BOOL)animated {
    

    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarLeftButtonImage:[UIImage imageNamed:@"close.png"] target:self action:@selector(close)];
    self.goToVideoHomeButton.alpha = self.uploader.isVideoReady ? 1 : 0;
    self.activityIndicatorView.alpha = self.uploader.isVideoReady ? 0 : 1;
    [self.uploadSpinner startAnimationWithShouldLayout:YES];

}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self shareViewInTransition];
    
    if (_uploader.dataModel.thumbnail_signed_url){
        _thumbnailURLToShare = [[_uploader.dataModel.thumbnail_signed_url componentsSeparatedByString:@"?"] firstObject];
    }
    
    if (_uploader.isVideoReady) {
        [self performSegueWithIdentifier:@"GoToVideoHome"
                                  sender:nil];
    }
    
   // [Flurry logEvent:FLURRY_VIEW_POSTRECORD_UPLOADING];
    
//    [SIAUser getUserWithCompletionHandler:^(SIAUser *user) {
//        self.recordingUser = user;
//    }];
    
}

- (void)close {
    [self.delegate close];
    if (_shareFacebookObserverFlag) {
        @try {
            [self.uploader removeObserver:self forKeyPath:@"finishedUploadingThumb"];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception : %@" ,exception.description);
        }
        
        @try {
            [self.uploader removeObserver:self forKeyPath:@"finishedUpladQualityConvert"];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception : %@" ,exception.description);
        }
        
        _shareFacebookObserverFlag = NO;
    }
    
    @try {
        [self.uploader removeObserver:self forKeyPath:@"dataModel.thumbnail_signed_url"];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception : %@" ,exception.description);
    }
    
    @try {
        [self.uploader removeObserver:self forKeyPath:@"dataModel.video_signed_url"];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception : %@" ,exception.description);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO
                                            withAnimation:UIStatusBarAnimationFade];
    
    [super viewWillDisappear:animated];
}

- (IBAction)shareFacebook:(id)sender {
    
    //[Flurry logEvent:FLURRY_TAP_SHAREFACEBOOK_POSTRECORD_UPLOADING];
    
    if (self.alreadyInShare) {
        return;
    }
    
    if (sender){
        self.shareFacebookPressed = YES;
    }
    
    if (_uploader.finishedUploadingThumb == YES) {
        _thumbnailUploadFinished = YES;
    }
    
    if ([_previewParent.recordingItem.recordType isEqualToString:BIO_TYPE]){ //not bio - for bio we dont need to wait because we share profile
        self.alreadyInShare = YES;
        _share = [[SIAShare alloc] init];
        [self.previewParent.player pause];
        _share.viewController = self;
        _share.user = self.recordingUser;
        _share.isMyProfile = YES;
        _share.shareType = @"profile";
        _share.isRecordingShare = NO;
        
        [_share shareOnFBWithCompletionBlock:^(BOOL) {
            self.alreadyInShare = NO;
        }];
        
        
    } else {
        self.alreadyInShare = YES;
        
//        for (UIView *subView in _shareView.subviews) {
//            if ([subView isKindOfClass:[SIAButtonSpinner class]]) {
//                [subView removeFromSuperview];
//            }
//        }
        _share = [[SIAShare alloc] init];
        //[self.previewParent.videoPlayer pause];
        _share.viewController = self;
        _thumbnailURLToShare = [_thumbnailURLToShare stringByReplacingOccurrencesOfString:SERVER_URL_GOOGLE_STORAGE withString:MEDIA_STROAGE_URL];
        _share.shareType = @"video";
        _share.isMyVideo = YES;
        _share.isRecordingShare = YES;
        _share.video.videoUrl = [NSURL URLWithString:self.videoUrlString];
        //_share.recordingPreviewHeaderView = ((SIARecorderPreviewPlayerViewController *)self.previewParent).viewHeader;
        
        if ([_previewParent.recordingItem.artist_name isEqualToString:@"(null) (null)"]) {
            NSString *firstName = [[NSUserDefaults standardUserDefaults] objectForKey:kChosenUserFirstNameKeyStr];
            NSString *lastName = [[NSUserDefaults standardUserDefaults] objectForKey:kChosenUserLastNameKeyStr];
            _previewParent.recordingItem.artist_name = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
        }
        
        //share.initialText = @"just recorded a new video";
        
        _share.video = [[SIAVideo alloc] init];
        _share.video.videoId = _uploader.videoId;
        _share.video.type = _previewParent.recordingItem.recordType;
        _share.video.song_name = _previewParent.recordingItem.song_name;
        _share.video.artist_name = _previewParent.recordingItem.artist_name;
        _share.video.thumbnailUrl = [NSURL URLWithString:_thumbnailURLToShare];
        _share.video.user = [[SIAUser alloc] init];
        _share.video.user = self.recordingUser;
        
        
        //share.image = self.selectedThumbnail;
        
        [_share shareOnFBWithCompletionBlock:^(BOOL) {
            self.alreadyInShare = NO;
        }];
    }
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        if (self.uploader.isUploading)
        {
            self.uploadToFacebookAfterServerUploadCompleted = YES;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload to Facebook"
                                                            message:@"Please wait for the upload to the Blingy server to finish before uploading to Facebook."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        else
            [_share composePost];
    }
}

- (void)updateFacebookUploadProgress:(CGFloat)progress {
    [self.progressView setProgress:progress animated:YES];
}

- (void)updateUploadProgress:(NSString *)percentString
{
  //  [self.progressView setProgress:percentString.floatValue / 100.0f animated:YES];
}

- (void)finishUploading{
    [self trackVideoUpload];
    __weak SIARecordingShareViewController *wSelf = self;
    [Utils dispatchOnMainThread:^{
        [wSelf.navigationController setNavigationItemTitle:@"Ready"];
        [wSelf fadeOutView:wSelf.activityIndicatorView];
        [wSelf performSegueWithIdentifier:@"GoToVideoHome" sender:nil];
    }];
}

-(void)trackVideoUpload {
    [AmplitudeEvents logWithEvent: @"record:complete" with: @{@"video_status":self.uploader.status ?: @"",
                                                              @"object_id": self.uploader.videoId ?: @"",
                                                              @"clip_id": self.uploader.clip_id ?: @"",
                                                              @"video_name": self.uploader.song ?: @"",
                                                              @"artist_name": self.uploader.artist ?: @"",
                                                              @"videos_created":[NSString stringWithFormat:@"%d", [BIALoginManager sharedInstance].myUser.total_performances.intValue + 1]}];
    [[AppsFlyerTracker sharedTracker] trackEvent:@"createvideo" withValues:@{@"video_status":self.uploader.status}];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (context == uploaderDataModelThumbnailUrlObservationContext && _uploader.dataModel.thumbnail_signed_url) {
        _thumbnailURLToShare = [[_uploader.dataModel.thumbnail_signed_url componentsSeparatedByString:@"?"] firstObject];
    }
    
    if (context == uploaderPercentUpdatedObservationContext)
    {
        
    }
    
    else if (context == uploaderFinishedUploadingThumbObservationContext ||
             context == uploaderFinishedUpladQualityConvertObservationContext)
    {
        if (_uploader.finishedUploadingThumb) {
            //[_waitToShareView removeFromSuperview];
            _thumbnailUploadFinished = YES;
            if (_shareFacebookClickedBeforeThumbUploaded && _uploader.finishedUpladQualityConvert) {
                [self shareFacebook:nil];
                if (_shareFacebookObserverFlag) {
                    @try {
                        [self.uploader removeObserver:self forKeyPath:@"finishedUploadingThumb"];
                    }
                    @catch (NSException *exception) {
                        NSLog(@"Exception: %@", exception.description);
                    }
                    
                    @try {
                        [self.uploader removeObserver:self forKeyPath:@"finishedUpladQualityConvert"];
                    }
                    @catch (NSException *exception) {
                        NSLog(@"Exception: %@", exception.description);
                    }
                    
                    _shareFacebookObserverFlag = NO;
                }
            }
        }
    }
}

- (IBAction)shareTwitter:(id)sender {
    
   // [Flurry logEvent:FLURRY_TAP_SHARETWITTER_POSTRECORD_UPLOADING];
    
    _share = [[SIAShare alloc] init];
    _share.viewController = self;
    _share.shareType = @"video";
    _share.isMyVideo = YES;
    _share.shareImage = _uploader.thumbnailImage;
    if (_thumbnailURLToShare == nil)
        _thumbnailURLToShare = [[_uploader.dataModel.thumbnail_signed_url componentsSeparatedByString:@"?"] firstObject];
    _share.interfaceOrientationForTwitterComposer = UIInterfaceOrientationMaskLandscape;
    //_share.initialText = @"just recorded a new video";
    
    
    _share.video = [[SIAVideo alloc] init];
    _share.video.videoId = _uploader.videoId;
    _share.video.type = _previewParent.recordingItem.recordType;
    _share.video.song_name = _previewParent.recordingItem.song_name;
    _share.video.artist_name = _previewParent.recordingItem.artist_name;
    _share.video.thumbnailUrl = [NSURL URLWithString:_thumbnailURLToShare];
    _share.video.user = [[SIAUser alloc] init];
    _share.video.user.user_id = [[NSUserDefaults standardUserDefaults] objectForKey:kUserIdKeyStr];
    
    
    //share.image = self.selectedThumbnail;
    [_share shareOnTwitterWithCompletionBlock:^(BOOL) {
        
    }];
}

- (IBAction)goToVideoHome:(id)sender {
    
//    [_previewParent.player pause];
//    
//    [Flurry logEvent:FLURRY_TAP_GOTOVIDEO_POSTRECORD_UPLOADING];
//    
//   // self.previewParent.xButton.hidden = YES;
//    self.goToVideoHomeButton.enabled = NO;
//    self.goToVideoHomeButton.alpha = 0.5;
//    
//    if ([_previewParent.recordingItem.recordType isEqualToString:kRecordTypeBio]) {
//        
//        SIANavigationViewController *navigationController = (SIANavigationViewController *)self.previewParent.presentingViewController;
//        
//        
//        SIAProfileViewController *profileVC;
//        
//        for (UIViewController *VC in navigationController.viewControllers) {
//            if ([VC isKindOfClass:[SIAProfileViewController class]]) {
//                profileVC = (SIAProfileViewController *)VC;
//            }
//        }
//        
//        [profileVC setUploader:self.uploader]; // this sets the upload progress view on profile automatically
//        
//        if ([self.delegate respondsToSelector:@selector(next)])
//        {
//            [self.delegate next];
//        }
//    }
//    
//    else if (self.uploader.isVideoReady) {
//        
//        //TODO: Fix SIAVideo / SIAResponseVideo to be less dumb
//        [SIAVideo getDataForVideoId:self.uploader.videoId withCompletionHandler:^(SIAVideo *video) {
////            if ([self.previewParent.recordingItem.recordType isEqualToString:kRecordTypeReview]) {
////                video = (SIAResponseVideo *)video;
////                ((SIAResponseVideo *)video).parent = self.previewParent.recordingItem.parentVideo;
////            }
//            [self.previewParent uploadCompletedWithVideo:video];
//        }];
//    }
//    
//    else {
//        
////        if ([self.delegate respondsToSelector:@selector(next)])
////        {
////            [self.delegate next];
////        }
//        
//    }
}

- (void) shareViewInTransition {
    
    if (SYSTEM_VERSION_GREATER_THAN_IOS8) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:ANIMATION_DURATION animations:^{
                
                [_shareView layoutIfNeeded];
            }];
        });
    }
    
    else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:ANIMATION_DURATION
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{
                                 
                                 _shareView.center = CGPointMake(self.view.center.x, _shareView.center.y);
                                 
                             } completion:nil];
        });
    }
}

//- (void)presentViewController:(UIViewController *)viewControllerToPresent
//                     animated:(BOOL)flag
//                   completion:(void (^)())completion {
//    [super presentViewController:viewControllerToPresent
//                        animated:flag
//                      completion:completion];
//    if ([NSStringFromClass(viewControllerToPresent.class) isEqualToString:@"MFMailComposeViewController"]) {
//        [self.previewParent.player pause];
//    }
//}
//
//- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)())completion {
//    [super dismissViewControllerAnimated:flag
//                              completion:completion];
//   // [self.previewParent.player play];
//}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"GoToVideoHome"])
    {
        VideoHomeViewController *videoHomeVC = segue.destinationViewController;
        videoHomeVC.videoId = self.uploader.dataModel.video_id;
	    videoHomeVC.backgroundImage.image = self.uploader.thumbnailImage;
        videoHomeVC.thumbnailImage.image = self.uploader.thumbnailImage;
        videoHomeVC.fromRecordFlow = YES;
    }
    [[SIAPushRegistrationHandler sharedHandler] recordingCompleted];
    [Utils dispatchOnMainThread:^{
        [self.previewParent.player pause];
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    }];
}

- (void)presentConnectView {
//    if (((SIAAppDelegate *)[[UIApplication sharedApplication] delegate]).isConnectViewPresented) {
//        return;
//    }
//    ((SIAAppDelegate *)[[UIApplication sharedApplication] delegate]).isConnectViewPresented = YES;
//    UIStoryboard *signinsb = [UIStoryboard storyboardWithName:@"SigninStoryboard" bundle:nil];
//    SIASigninViewController *signinVC = [signinsb instantiateInitialViewController];
//    signinVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    signinVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
////    [signinVC view];
////    signinVC.view.alpha = 0;
////    [self.view addSubview:signinVC.view];
//    
//    
////    mainQueue(^{
////        [UIView animateWithDuration:0.25
////                         animations:^{
////                             signinVC.view.alpha = 1;
////                         } completion:^(BOOL finished) {
//                             [self presentViewController:signinVC
//                                                animated:YES
//                                              completion:^{
//                                                  [signinVC.dismissButton removeTarget:signinVC
//                                                                                action:@selector(dismiss)
//                                                                      forControlEvents:UIControlEventTouchUpInside];
//                                                  [signinVC.dismissButton addTarget:self
//                                                                             action:@selector(dismissConnectView)
//                                                                   forControlEvents:UIControlEventTouchUpInside];
//                                              }];
//                            // self.view.userInteractionEnabled = YES;
////                         }];
////    });
}


@end
