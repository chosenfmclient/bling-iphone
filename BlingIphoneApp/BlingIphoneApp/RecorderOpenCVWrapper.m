//
//  RecorderOpenCVWrapper.m
//  BlingIphoneApp
//
//  Created by Joseph Nahmias on 26/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import "RecorderOpenCVWrapper.h"
#import "RecorderOpenCV.h"
#import <sys/sysctl.h>


@interface RecorderOpenCVWrapper () <BIAPausableCVCameraDelegate, RecorderDetectionDelegate>

@property (nonatomic, strong) RecorderOpenCV* recorder;

@end


@implementation RecorderOpenCVWrapper

-(instancetype)init {
    self = [super init];
    
    if (self) {
        self.usingOldDevice = [self areUsingOldDevice];
        
    }
    return self;
}

-(BOOL)areUsingOldDevice{
    
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = (char *)malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    free(machine);
    
    if ([platform isEqualToString:@"iPhone1,1"] ||    //return @"iPhone 1G";
        [platform isEqualToString:@"iPhone1,2"] ||    //return @"iPhone 3G";
        [platform isEqualToString:@"iPhone2,1"] ||    //return @"iPhone 3GS";
        [platform isEqualToString:@"iPhone3,1"] ||    //return @"iPhone 4";
        [platform isEqualToString:@"iPhone3,2"] ||    //return @"iPhone 4";
        [platform isEqualToString:@"iPhone3,3"] ||    //return @"Verizon iPhone 4";
        [platform isEqualToString:@"iPhone4,1"] ||    //return @"iPhone 4S";
        [platform isEqualToString:@"iPhone5,1"] ||    //return @"iPhone 5 (GSM)";
        [platform isEqualToString:@"iPhone5,2"] ||    //return @"iPhone 5 (GSM+CDMA)";
        [platform isEqualToString:@"iPhone5,3"] ||    //return @"iPhone 5c (GSM)";
        [platform isEqualToString:@"iPhone5,4"] ||    //return @"iPhone 5c (GSM+CDMA)";
        [platform isEqualToString:@"iPhone6,1"] ||    //return @"iPhone 5s (GSM)";
        [platform isEqualToString:@"iPhone6,2"] ||    //return @"iPhone 5s (GSM+CDMA)";
        [platform isEqualToString:@"iPhone7,1"] ||    //return @"iPhone 6 Plus";
        [platform isEqualToString:@"iPhone7,2"] ||    //return @"iPhone 6";
        
        ///IPOD
        
        [platform isEqualToString:@"iPod1,1"] ||     //return @"iPod Touch 1G";
        [platform isEqualToString:@"iPod2,1"] ||     //return @"iPod Touch 2G";
        [platform isEqualToString:@"iPod3,1"] ||     //return @"iPod Touch 3G";
        [platform isEqualToString:@"iPod4,1"] ||     //return @"iPod Touch 4G";
        [platform isEqualToString:@"iPod5,1"] ||     //return @"iPod Touch 5G";
        [platform isEqualToString:@"iPod7,1"] ||     //return @"iPod Touch 6G";
        
        ///IPAD
        
        [platform isEqualToString:@"iPad1,1"] ||     //return @"iPad";
        [platform isEqualToString:@"iPad2,1"] ||     //return @"iPad 2 (WiFi)";
        [platform isEqualToString:@"iPad2,2"] ||     //return @"iPad 2 (GSM)";
        [platform isEqualToString:@"iPad2,3"] ||     //return @"iPad 2 (CDMA)";
        [platform isEqualToString:@"iPad2,4"] ||     //return @"iPad 2 (WiFi)";
        [platform isEqualToString:@"iPad2,5"] ||     //return @"iPad Mini (WiFi)";
        [platform isEqualToString:@"iPad2,6"] ||     //return @"iPad Mini (GSM)";
        [platform isEqualToString:@"iPad2,7"] ||     //return @"iPad Mini (GSM+CDMA)";
        [platform isEqualToString:@"iPad3,1"] ||     //return @"iPad 3 (WiFi)";
        [platform isEqualToString:@"iPad3,2"] ||     //return @"iPad 3 (GSM+CDMA)";
        [platform isEqualToString:@"iPad3,3"] ||     //return @"iPad 3 (GSM)";
        [platform isEqualToString:@"iPad3,4"] ||     //return @"iPad 4 (WiFi)";
        [platform isEqualToString:@"iPad3,5"] ||     //return @"iPad 4 (GSM)";
        [platform isEqualToString:@"iPad3,6"] ||     //return @"iPad 4 (GSM+CDMA)";//
        [platform isEqualToString:@"iPad4,1"] ||     //mini 2
        [platform isEqualToString:@"iPad4,2"] ||     //mini 2
        [platform isEqualToString:@"iPad4,3"] ||     //mini 2
        [platform isEqualToString:@"iPad4,4"] ||     //mini 2
        [platform isEqualToString:@"iPad4,5"] ||
        [platform isEqualToString:@"iPad4,6"] ||
        [platform isEqualToString:@"iPad4,7"] ||    //mini 3
        [platform isEqualToString:@"iPad4,8"] ||
        [platform isEqualToString:@"iPad4,9"] ||
        [platform isEqualToString:@"iPad5,1"] ||
        [platform isEqualToString:@"iPad5,2"])
        
        return YES;
    
    return NO;
    
}

-(void)setupNewRecorderWithView:(UIView *)view {
    self.recorder = [[RecorderOpenCV alloc] initWithView:view];
    self.recorder.videoCamera.recordDelegate = self;
    self.recorder.detectionDelegate = self;
    self.usingFrontCamera = YES;
    [self.recorder startCamera];
}

- (void)setPlayer:(BIAVideoPlayer *)player {
    [self.recorder setPlayer:player];
}

- (void)switchCamera {
    [self.recorder reset];
    [self.recorder.videoCamera switchCameras];
    [self.delegate recorderNeedsRedetect];
}

-(void)startRecording{
    [self.recorder startRecording];
}

-(void)stopRecording{
    [self.recorder stopRecording];
}

-(void)pauseRecording {
    [self.recorder pauseRecording];
}

-(void)resumeRecording {
    [self.recorder resumeRecording];
}

-(void)cancelRecording {
    [self.recorder cancelRecording];
}

-(void)setRecordingSpeedScale:(Float64)scale {
    [self.recorder setRecordingSpeedScale:scale];
}

-(void)startDetection {
    [self.recorder startDetection];
}

- (BOOL)isRecording {
    return self.recorder.videoCamera.isRecording;
}

- (BOOL)isDetecting {
    return self.recorder.isDetecting;
}

- (BOOL)didDetect {
    return self.recorder.didDetect;
}

#pragma mark - SIAPausableCamera Delegate

-(void)recordingDidStart {
    [self.delegate recordingDidStart];
}

-(void)backgroundPlaybackDidStartWithTimeOffset:(CMTime)offset {
    [self.delegate backgroundPlaybackDidStartWithTimeOffset:offset];
}

-(void)recordingDidStop {
    [self.delegate recordingDidStop];
}

-(void)recorderReadyToRecord {
    [self.delegate recorderReadyToRecord];
}

-(void)recorderDidFailToInitialize:(NSError *)error {
    [self.delegate recorderDidFailToInitialize:error];
}

#pragma mark - Detection Delegate

- (void)detectionFinishedWithResponse:(DetectionResponse)response {
    [self.delegate detectionFinishedWithResponse:(DetectionResponseS)response];
}

- (void)recorderNeedsRedetect {
    [self.delegate recorderNeedsRedetect];
}

- (void)recorderDidFailToReadBackgroundVideo:(NSError *)error {
    [self.delegate recorderDidFailToReadBackgroundVideo:error];
}

#pragma mark - Convenience properties

- (NSURL *)recordURL {
    return self.recorder.videoCamera.recordURL;
}

- (AVCaptureSession *)captureSession {
    return self.recorder.videoCamera.captureSession;
}

- (CMTime)currentTime {
    if (self.recorder.videoCamera.captureSession.masterClock) {
        return CMClockGetTime(self.recorder.videoCamera.captureSession.masterClock);
    }
    return kCMTimeInvalid;
    
}

- (void)resetForRedetect {
    [self.recorder resetForRedetect];
}

- (void)reset {
    [self.recorder resetAndResetCapSession:YES];
}

-(void)setUsingFrontCamera:(BOOL)usingFrontCamera{
    _usingFrontCamera = usingFrontCamera;
    self.recorder.usingFrontCamera = usingFrontCamera;
}

- (BOOL)isSetUp {
    return self.recorder != nil;
}

@end
