//
//  BIAVideoPlayer.h
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 07/09/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "SIARecordingItem.h"
#import "SIAVideo.h"
#import "SIAVideoPlayerView.h"


//typedef NS_ENUM(NSInteger, SIASelectedEffectButtonsIndex) {
//    
//    SIASelectedEffectNone = -1,
//    SIASelectedEffect1Index = 0,
//    SIASelectedEffect2Index,
//    SIASelectedEffect3Index
//};
//

typedef NS_ENUM(NSInteger, SIAVideoEffectType) {
    
    SIAVideoEffectTypeNone,
    SIAVideoEffectTypeBlackAndWhite,
    SIAVideoEffectTypeSaturation,
    SIAVideoEffectTypeSwirl
};

typedef NS_ENUM(NSInteger, SIAAudioEffectType) {
    
    SIAAudioEffectTypeNone,
    SIAAudioEffectTypeDelay,
    SIAAudioEffectTypeReverb
};

@protocol BIAVideoPlayerDelegate <NSObject>

@optional
-(void)playerReadyToPlay;
-(void)playerWillPlay:(AVPlayer *)player;
-(void)playerDidPlay:(AVPlayer *)player;
-(void)playerDidFinishPlaying:(AVPlayer*)player;

@end

@interface BIAVideoPlayer : AVPlayer

- (instancetype) initWithRecordingItem:(SIARecordingItem *)recordingItem delegate:(id <BIAVideoPlayerDelegate>)delegate;
- (instancetype)initWithRecordingItem:(SIARecordingItem *)recordingItem;
- (instancetype)initWithVideo: (SIAVideo *)video;
- (instancetype)initWithVideo: (SIAVideo *)video andPlayerView: (SIAVideoPlayerView *)filterView;
@property (strong, nonatomic) SIAVideoPlayerView *filterView;

- (void)setVideoFilter:(SIAVideoEffectType)theFilter;
- (void)unsetVideoFilter:(SIAVideoEffectType)theFilter;

- (void)setAudioFilter:(SIAAudioEffectType)theFilter;
- (void)unsetAudioFilter:(SIAAudioEffectType)theFilter;

- (void)repeatPlayback;
- (void)repeatPlaybackFrom:(CGFloat)startTime
                        to:(CGFloat)endTime;
- (void)stopRepeatingPlayback;

@property (strong, nonatomic) NSMutableArray *videoEffects;
@property (strong, nonatomic) NSMutableArray *audioEffects;
@property (weak, nonatomic) id<BIAVideoPlayerDelegate> delegate;
@property (nonatomic) BOOL playAfterAppBecomesActive;
@property (nonatomic) CGAffineTransform transform;

-(void)releaseAudioMix;
-(void)observeStatusForReadyToPlay;

//- (instancetype)initWithURL:(NSURL *)videoURL;
//+ (instancetype)effectPlayerWithURL:(NSURL *)videoURL;

//- (void)play;
//- (void)pause;

//- (CMTime)currentTime;
//- (void)seekToTime:(CMTime)time;
//- (void)seekToTime:(CMTime)time completionHandler:(void (^)(BOOL))completionHandler;

//@property (nonatomic) BOOL muted;
//@property (nonatomic) float volume;
//
//@property(nonatomic, readonly) AVPlayerStatus status;
//@property(nonatomic, readonly) NSError *error;

@end
