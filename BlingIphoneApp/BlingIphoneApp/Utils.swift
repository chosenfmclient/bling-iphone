//
//  Utils.swift
//  ChosenIphoneApp
//
//  Created by Hen Levy on 28/06/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

import UIKit

@objc class Utils: NSObject {
    class func dispatchAsync(_ closure: @escaping () -> ()) {
        DispatchQueue.main.async(execute: closure)
    }
    
    class func dispatchAfter(_ delay: Double, closure: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    class func dispatchOnQueue(_ queue: DispatchQueue , delay: Double, closure: @escaping () -> ()) {
        queue.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    class func dispatchInBackground(_ closure: @escaping () -> ()) {
        DispatchQueue.global(qos: .background).async(execute: closure)
    }
    
    class func dispatchInDefaultPriority(_ closure: @escaping () -> ()) {
        DispatchQueue.global(qos: .default).async(execute: closure)
    }
    
    class func dispatchOnMainThread(_ closure: @escaping () -> ()) {
        DispatchQueue.main.async(execute: closure)
    }
    
    class func areYouSureAboutThat(_ text: String, confirmText: String, confirmHandler: (() -> Void)?) -> SIAAlertController {
        let statusAlert = SIAAlertController.init(title:"Are you sure?", message: text);
        
        let cancelAction = SIAAlertAction.init(title:"CANCEL", style: SIAAlertActionStyle.cancel, handler:nil);
        statusAlert?.addAction(cancelAction)
        
        let confirmAction = SIAAlertAction.init(title: confirmText, style: SIAAlertActionStyle.confirmation, handler:{(action: SIAAlertAction?) in
            //change status of video
            statusAlert?.dismiss(animated: true, completion: nil)
            if let callableAction = confirmHandler {
                callableAction()
            }
        })
        
        statusAlert?.addAction(confirmAction)
        return statusAlert!
    }
    
    class func alertWithOkButton(_ text: String, titleText: String, confirmHandler: (() -> Void)?) -> SIAAlertController {
        let statusAlert = SIAAlertController.init(title: titleText, message: text);
        
        let confirmAction = SIAAlertAction.init(title: "OK", style: SIAAlertActionStyle.confirmation, handler:{(action: SIAAlertAction?) in
            //change status of video
            statusAlert?.dismiss(animated: true, completion: nil)
            if let callableAction = confirmHandler {
                callableAction()
            }
        })
        
        statusAlert?.addAction(confirmAction)
        return statusAlert!
    }

    class func removeFile(_ fileURL: URL?) {
        let fileManager = FileManager.default
        var fileRemoved = false
        
        guard let filePath = fileURL?.path
            else { return }
        
        defer {
            print(fileRemoved ? "file remove success \(filePath)" : "failed remove file \(filePath)")
        }
        
        do {
            guard let _ = try fileURL?.checkResourceIsReachable()
                else { return }
        } catch { return }
        
        do {
            try fileManager.removeItem(atPath: filePath)
            fileRemoved = true
        } catch {
            return
        }
    }

//    class func myUserObject() -> SIAUser? {
//        return (UIApplication.shared.delegate as! AppDelegate).myUserObject
//    }
    
    class func changeBrightnessOfImage(_ image: UIImage, byValue: Float) -> UIImage? {
        let cgImage = image.cgImage
        let ciImage = CIImage(cgImage: cgImage!)
        let brightnessFilter = CIFilter(name: "CIColorControls", withInputParameters: ["inputImage": ciImage, "inputBrightness":byValue])
        let outCIImage = brightnessFilter!.outputImage
        let context = CIContext(options: nil)
        return UIImage(cgImage: context.createCGImage(outCIImage!, from: ciImage.extent)!)
    }
    
    class func getElapsedTimeStringFor(_ date: Date) -> String {
        return date.getElapsedInterval()
    }
    
    class func viewControllerIsTop(_ vc: UIViewController) -> Bool {
        return vc == (UIApplication.shared.delegate as! AppDelegate).rootNavController()?.topViewController || vc.isBeingPresented
    }
}

extension UIColor {
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        let scanner = Scanner(string: hex.lowercased().replacingOccurrences(of: "#", with: ""))
        scanner.scanLocation = 0
        var rgb: UInt32 = 0
        scanner.scanHexInt32(&rgb)
        self.init(red: CGFloat((rgb & 0xFF0000) >> 16)/255.0, green: CGFloat((rgb & 0xFF00) >> 8)/255.0, blue: CGFloat((rgb & 0xFF))/255.0, alpha: alpha)
    }
    
    static func brighten(_ color: UIColor, by factor: CGFloat) -> UIColor {
        var r = CGFloat(0)
        var g = CGFloat(0)
        var b = CGFloat(0)
        var a = CGFloat(0)
        if color.getRed(&r,
                        green: &g,
                        blue: &b,
                        alpha: &a) {
            // constrain values to 0.0 - 1.0
            let newR = 0.0...1.0 ~= r + factor ? Float(r + factor) : Float(r)
            let newG = 0.0...1.0 ~= g + factor ? Float(g + factor) : Float(g)
            let newB = 0.0...1.0 ~= b + factor ? Float(b + factor) : Float(b)
            
            return UIColor(colorLiteralRed:newR,
                           green: newG,
                           blue: newB,
                           alpha: Float(a))
        }
        else {
            return color
        }
        
    }
}

extension UIImageView {
    func blur(_ style: UIBlurEffectStyle = .dark) {
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(blurEffectView)
    }
}

extension UIButton {
    func bia_setEnabled(_ enabled: Bool) {
        if (!enabled) {
            self.isEnabled = false
            self.alpha = 0.3
        } else {
            self.isEnabled = true
            self.alpha = 1
        }
    }
}

extension UIView {
    func bia_rounded() {
        self.layoutIfNeeded()
        clipsToBounds = true
        if (self is UIImageView) {
            (self as! UIImageView).layer.masksToBounds = true
        }
        self.layer.cornerRadius = self.bounds.size.height / 2.0
    }
    
    func bia_startPulsing() {
        let animation = CABasicAnimation(keyPath: "transform")
        animation.toValue = NSValue(caTransform3D: CATransform3DMakeScale(1.25, 1.25, 0.0)) // maximum scale value
        animation.byValue = NSValue(caTransform3D: CATransform3DMakeScale(0.8, 0.8, 0.0)) // toValue - byValue = minimum scale
        
        
        let flashAnimation = CABasicAnimation(keyPath: "backgroundColor")
        let flashColor = UIColor.brighten(backgroundColor!,
             by: 0.1)
        flashAnimation.toValue = flashColor.cgColor as Any
        
        let group = CAAnimationGroup()
        group.animations = [animation, flashAnimation]
        group.timingFunction = CAMediaTimingFunction(controlPoints: 0.57, 0.24, 0.72, 0.4) // cool curve
        group.autoreverses = true
        group.duration = 0.389
        group.repeatCount = HUGE
        group.isRemovedOnCompletion = false
        
        layer.add(group,
                  forKey: "pulse")
    }
    
    func bia_stopPulsing() {
        layer.removeAnimation(forKey: "pulse")
    }
}

extension UITextField {
    func bia_setLeftImage(named: String) {
        self.leftViewMode = .always
        self.leftView = UIImageView(image: UIImage(named: named))
        self.leftView?.contentMode = .center
        self.leftView?.frame = CGRect(origin: (self.leftView?.frame.origin)!, size: CGSize(width: self.frame.height, height: self.frame.height))
    }
    
    
    func bia_setRightImage(named: String) {
        self.rightViewMode = .always
        self.rightView = UIImageView(image: UIImage(named: named))
        self.rightView?.contentMode = .center
        self.rightView?.frame = CGRect(origin: (self.rightView?.frame.origin)!, size: CGSize(width: self.frame.height, height: self.frame.height))
    }
}

public extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    static func shorthandNumber(from number: UInt) -> String {
        if number == 0 {
            return "0"
        }
        let n = Double(number)
        let order = Int(log10(n))
        var numStr: String
        switch order {
        case 0...2: // 0 - 999
            numStr = "\(Int(n))"
        case 3...5: // 1000 - 999,999
            let reducedLikes = round(10 * n / pow(10, 3)) / 10
            let truncNum: Any = reducedLikes.truncatingRemainder(dividingBy: 1) == 0 ? Int(reducedLikes) : reducedLikes
            numStr = "\(truncNum)k"
        default: // n > 1,000,000
            let reducedLikes = round(10 * n / pow(10, 6)) / 10
            let truncNum: Any = reducedLikes.truncatingRemainder(dividingBy: 1) == 0 ? Int(reducedLikes) : reducedLikes
            numStr = "\(truncNum)M"
        }
        return numStr
    }
}

public extension NSString {
    static func shorthandNumber(from number: UInt) -> NSString {
        if number == 0 {
            return "0" as NSString
        }
        let n = Double(number)
        let order = Int(log10(n))
        var numStr: String
        switch order {
        case 0...2: // 0 - 999
            numStr = "\(Int(n))"
        case 3...5: // 1000 - 999,999
            let reducedLikes = round(10 * n / pow(10, 3)) / 10
            let truncNum: Any = reducedLikes.truncatingRemainder(dividingBy: 1) == 0 ? Int(reducedLikes) : reducedLikes
            numStr = "\(truncNum)k"
        default: // n > 1,000,000
            let reducedLikes = round(10 * n / pow(10, 6)) / 10
            let truncNum: Any = reducedLikes.truncatingRemainder(dividingBy: 1) == 0 ? Int(reducedLikes) : reducedLikes
            numStr = "\(truncNum)M"
        }
        return numStr as NSString
    }
}

extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func removeObject(_ object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}

extension Array {
    func chunk(_ chunkSize: Int) -> [[Element]] {
        return stride(from: 0, to: self.count, by: chunkSize).map({ (startIndex) -> [Element] in
            let endIndex = (startIndex.advanced(by: chunkSize) > self.count) ? self.count-startIndex : chunkSize
            return Array(self[startIndex..<startIndex.advanced(by: endIndex)])
        })
    }
}

extension URL {
    func getQueryDictionary() -> [String: String] {
        guard let queryString = self.query else { return [:] }
        
        let querys = queryString.components(separatedBy: "&")
            .map({ $0.components(separatedBy: "=")})
            .reduce(
                [String : String](),
                { (accum, parts) in
                    var dict = accum
                    guard parts.count >= 2 else { return dict }
                    dict[parts[0]] = parts[1]
                    return dict
                }
        )
        
        return querys
    }
}

extension UIAlertController {
    
    convenience init(title: String?, message: String?, sourceView: UIView? = nil, sourceRect: CGRect? = nil) {
        if let _ = sourceView {
            self.init(title: title,
                      message: message,
                      preferredStyle: .actionSheet)
            popoverPresentationController?.sourceView = sourceView
            if let _ = sourceRect {
                popoverPresentationController?.sourceRect = sourceRect!
            }
            else {
                popoverPresentationController?.sourceRect = sourceView!.bounds
            }
        }
        else {
            self.init(title: title,
                      message: message,
                      preferredStyle: .alert)
        }
    }

}

extension Date {
    
    func getElapsedInterval() -> String {
        
        var interval = (Calendar.current as NSCalendar).components(.year, from: self, to: Date(), options: []).year
        
        if (interval == nil) { return ""}
        if interval! > 0 {
            return interval == 1 ? "\(interval!)" + " " + "year" :
                "\(interval!)" + " " + "years"
        }
        
        interval = (Calendar.current as NSCalendar).components(.month, from: self, to: Date(), options: []).month
        if interval! > 0 {
            return interval == 1 ? "\(interval!)" + " " + "month" :
                "\(interval!)" + " " + "months"
        }
        
        interval = (Calendar.current as NSCalendar).components(.day, from: self, to: Date(), options: []).day
        if interval! > 0 {
            return interval == 1 ? "\(interval!)" + " " + "day" :
                "\(interval!)" + " " + "days"
        }
        
        interval = (Calendar.current as NSCalendar).components(.hour, from: self, to: Date(), options: []).hour
        if interval! > 0 {
            return interval == 1 ? "\(interval!)" + " " + "hour" :
                "\(interval!)" + " " + "hours"
        }
        
        interval = (Calendar.current as NSCalendar).components(.minute, from: self, to: Date(), options: []).minute
        if interval! > 0 {
            return interval == 1 ? "\(interval!)" + " " + "minute" :
                "\(interval!)" + " " + "minutes"
        }
        
        return "a moment"
    }
    
    func isGreaterThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> Date {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: Date = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> Date {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}
