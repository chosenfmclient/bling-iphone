//
//  SIAPerformancesModel.m
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 03/08/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAVideosDataModel.h"
#import "SIAVideo.h"

#define BASE_API @"api/v1/users/%@/videos"
#define POSTFIX_API @"/performances"
#define SORT_TYPE @"sort"
#define FILTER_TYPE @"status"
#define RANK_HIGH_TO_LOW @"rank_high_to_low"
#define DATE_ADDED @"date_added"
#define VIEWS @"most_viewed"
#define FILTER_ALL @"all"
#define FILTER_PRIVATE @"private"
#define FILTER_PUBLIC @"public"


@interface SIAVideosDataModel ()

@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) NSDictionary *dataDictionary;
@property (strong, nonatomic) NSArray *date_added;
@property (strong, readwrite, nonatomic) NSArray *sortedByDateAdded;
@property (strong, nonatomic) NSArray *most_viewed;
@property (strong, readwrite, nonatomic) NSArray *sortedByMostViewd;
@property (strong, nonatomic) NSArray *rank_high_to_low;
@property (strong, readwrite, nonatomic) NSArray *sortedByRankHighToLow;

@end

@implementation SIAVideosDataModel

- (NSString *) pagePath {
    return [NSString stringWithFormat:BASE_API, self.userId];
}

- (NSString *)pathPattern {
    return @"api/v1/users/:id/videos";
}

+ (RKObjectMapping *)objectMapping {
    return [SIAVideo objectMapping];
}

+ (RKMapping *) pageMapping {
    return [SIAPaginatorDataModel responseMappingForTotalKey:@"total_objects"
                                               objectMapping:[SIAVideosDataModel objectMapping]
                                                  modelClass:[SIAVideosDataModel class]];
}

- (NSDictionary *)requestParams {
    NSString *sortTypeString;
    NSString *filterTypeString;
    switch (self.sortType) {
        case SIAVideoSortTypeRankHighToLow:
            sortTypeString = RANK_HIGH_TO_LOW;
            break;
        case SIAVideoSortTypeDateAdded:
            sortTypeString = DATE_ADDED;
            break;
        case SIAVideoSortTypeViews:
            sortTypeString = VIEWS;
            break;
        default:
            sortTypeString = RANK_HIGH_TO_LOW;
            break;
    }
    switch (self.filterType) {
        case SIAVideoFilterTypeAll:
            filterTypeString = FILTER_ALL;
            break;
        case SIAVideoFilterTypePrivate:
            filterTypeString = FILTER_PRIVATE;
            break;
        case SIAVideoFilterTypePublic:
            filterTypeString = FILTER_PUBLIC;
            break;
        default:
            filterTypeString = FILTER_ALL;
            break;
    }
    
    if (!self.videoType) {
        self.videoType = [NSString string];
    }
    
    return @{@"type":self.videoType,
             SORT_TYPE:sortTypeString,
             FILTER_TYPE:filterTypeString};
}

@end
