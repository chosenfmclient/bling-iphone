//
//  SIACommentsTableViewController.h
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/10/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIATableViewContainedViewController.h"
#import "SIATextCommentModel.h"
#import "SIACommentTableViewCell.h"

@interface SIACommentsTableViewController : SIATableViewContainedViewController <SIATextCommentViewDelegate>
@property (nonatomic, readwrite) SIAVideo *video;
@property (nonatomic, readwrite) NSArray<SIATextComment*> *comments;
@end
