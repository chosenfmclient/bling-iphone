//
//  ProfileResponseModel.swift
//  BlingIphoneApp
//
//  Created by Zach on 10/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class ProfileResponseModel: VideoListDataModel {
    var user: SIAUser?
    
    static func objectMapping() -> RKObjectMapping {
        let responseMapping = RKObjectMapping(for: ProfileResponseModel.self)
        
        responseMapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "user",
                                                                  toKeyPath: "user",
                                                                  with: SIAUser.objectMapping()))
        responseMapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "videos",
                                                                  toKeyPath: "videos",
                                                                  with: SIAVideo.objectMapping()))
        return responseMapping!
    }
}
