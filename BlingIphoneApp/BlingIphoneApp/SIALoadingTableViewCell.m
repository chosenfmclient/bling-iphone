//
//  SIALoadingTableViewCell.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 6/16/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import "SIALoadingTableViewCell.h"

@implementation SIALoadingTableViewCell

- (void) layoutSubviews {
    
    if (!self.contentView.subviews.count) {
        _loadingSpinner = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"icon_loading"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        if (_spinnerTintColor) {
            _loadingSpinner.tintColor = _spinnerTintColor;
        }
        [self.contentView addSubview:_loadingSpinner];
        _loadingSpinner.frame = CGRectMake(0, 0, self.contentView.bounds.size.height / 2, self.contentView.bounds.size.height / 2);
        _loadingSpinner.center = self.contentView.center;
        _loadingSpinner.contentMode = UIViewContentModeScaleAspectFit;
        _loadingSpinner.clipsToBounds = YES;
    }
    
    [self runSpinAnimationOnView:_loadingSpinner
                        duration:0.5
                       rotations:1
                          repeat:HUGE_VALF];
    
    if (self.unroundCorners) return;
    [self roundCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)];
    self.backgroundColor = [UIColor clearColor];
}

- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

-(void)removeLoadingAnimation{
    _loadingSpinner.hidden = YES;
}
@end

@implementation SIALoadingTableViewCellBlack

- (void) layoutSubviews {
    [super layoutSubviews];
    self.loadingSpinner.image = [self.loadingSpinner.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.loadingSpinner.tintColor = [[UIColor alloc] initWithHex:@"#808080" alpha:1];
}

@end
