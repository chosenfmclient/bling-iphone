//
//  SIATextCommentView.h
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/9/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    SIATextCommentActionTypeRegion = 10000,
    SIATextCommentActionSendType,
    SIATextCommentActionUpdateType,
    SIATextCommentActionCancelType,
} SIATextCommentAction;


@class SIATextCommentView;
@protocol SIATextCommentViewDelegate <NSObject>
@required
- (void)textCommentView:(SIATextCommentView*)commentView userDidSendComment:(NSString*)comment;
- (BOOL)textCommentViewShouldBeginEditing:(SIATextCommentView*)commentView;
- (void)textCommentViewDidEndEditing:(SIATextCommentView*)commentView;
@optional
- (void)textCommentViewDidBeginEditing:(SIATextCommentView*)commentView;
- (void)textCommentView:(SIATextCommentView*)commentView userDidChangeComment:(NSString*)newComment;
@end

@interface SIATextCommentView : UIView
@property (nonatomic, weak) id <SIATextCommentViewDelegate> delegate;

@property (nonatomic, readwrite) NSString *name;
@property (nonatomic, readwrite) NSString *comment;
@property (nonatomic, readwrite) UIImage  *image;

@property (nonatomic, readwrite) NSString *commentColor;
@property(nonatomic, readonly, getter=isEditing) BOOL editing;
@property(nonatomic, readwrite) BOOL floatTextInput;

@property (strong, nonatomic, nonnull) UIColor *backgroundIdleColor;

@property(nonatomic, assign) UIKeyboardAppearance keyboardAppearance;
- (void)addActionWithType:(SIATextCommentAction)action;
- (void)beginCommentEditing;
- (void)beginCommentEditingWithComment:(NSString*)comment;
- (void)endCommentEditing;
- (instancetype)copy;

- (void)setName:(NSString*)name comment:(NSString*)comment color:(NSString*)commentColor;
- (void)setBackgroundColor:(UIColor *)backgroundColor;
@end
