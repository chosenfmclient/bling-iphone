//
//  VideoUserBarView.swift
//  BlingIphoneApp
//
//  Created by Zach on 26/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

protocol VideoUserBarDelegate: class {
    
    func presentUserBarAlert(_ viewController: UIViewController)
    func profileButtonWasTapped(_ user: SIAUser)
    
    func videoWasDeleted(_ video: SIAVideo)
    func videoWasMadePrivate(_ video: SIAVideo)
    
    func makeCameoWasTapped(_ video: SIAVideo)
    func viewOriginalVideoWasTapped(_ video: SIAVideo)
}

extension VideoUserBarDelegate {
    func viewOriginalVideoWasTapped(_ video: SIAVideo) {
        if video.status == "public" || (video.isMyVideo() && video.isPrivate()){
            (UIApplication.shared.delegate as! AppDelegate).rootNavController()?.switchToVideoHome(forVideoId: video.videoId)
            AmplitudeEvents.log(event: "cameo:view_original",
                                with: ["object_id":video.videoId,
                                       "clip_id": video.clip_id ?? "null",
                                       "artist_name":video.artist_name,
                                       "video_name":video.song_name])
        }
        else {
            guard let topVC = (UIApplication.shared.delegate as! AppDelegate).topViewController() else { return }
            let title = video.status == "deleted" ? "Deleted" : "Private Video"
            let message = video.status == "deleted" ? "Sorry, the original video has been deleted." : "Sorry, the original video is private."
            let alert = UIAlertController(title: title,
                                          message: message,
                                          preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .cancel,
                                          handler: nil))
            
            topVC.present(alert,
                          animated: true,
                          completion: nil)
        }

    }
}


class VideoUserBarView: UIView {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var userProfileButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var blackOverlayView: UIView!
    @IBOutlet weak var seperatorView: UIView!
    
    fileprivate weak var _video: SIAVideo?
    var isHomeScreen = false
    
    
    var view: UIView?
    var position: Int?
    
    weak var delegate: VideoUserBarDelegate?
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        view = (Bundle.main.loadNibNamed("VideoUserBarView", owner: self, options: nil)?[0] as! UIView)
        view?.frame = frame
        self.addSubview(view!)
        
        view?.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        view?.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        view?.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        view?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        view = (Bundle.main.loadNibNamed("VideoUserBarView", owner: self, options: nil)?[0] as! UIView)
        view?.frame = bounds
        self.addSubview(view!)
        
        view?.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        view?.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        view?.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        view?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        blurView.removeFromSuperview()
        blackOverlayView.removeFromSuperview()
    }
    
    weak var video: SIAVideo? {
        set(newValue) {
            _video = newValue
            userNameLabel.text = _video?.user.fullName
            userImageView.sd_setImage(with: _video?.user.imageURL)
        }
        get {
            return _video
        }
    }
    
    override var isAccessibilityElement: Bool {
        set(newValue) {
            self.isAccessibilityElement = newValue
        }
        get {
            return true
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        seperatorView?.isHidden = !isHomeScreen
        userImageView.bia_rounded()
    }
    
    @IBAction func profileButtonWasTapped(_ sender: AnyObject) {
        guard let _ = video else { return }
        delegate?.profileButtonWasTapped(video!.user)
    }
    
    
    @IBAction func moreButtonWasTapped(_ sender: AnyObject) {
            moreButtonTappedForVideo()
    }
    
    func moreButtonTappedForVideo() {
        let alertController = UIAlertController(title: nil,
                                                message: nil,
                                                sourceView: moreButton)
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        if video!.isCameo() {
            alertController.addAction(UIAlertAction(title: "View Original Video",
                                                    style: .default,
                                                    handler: {[weak self] (action) in
                                                        guard let video = self?.video else { return }
                                                        self?.delegate?.viewOriginalVideoWasTapped(video.parent)
            }))
        }
        
        else if video!.status != "secret" && video!.isMyVideo(){
            alertController.addAction(UIAlertAction(title:
                String(format:"Duet with %@", video!.isMyVideo() ?
                    "Yourself" : video!.user.fullName),
                                                    style: .default,
                                                    handler: {[weak self] (action) in
                                                        guard let video = self?.video else { return }
                                                        self?.delegate?.makeCameoWasTapped(video)
            }))
        }
        
        if (video!.isMyVideo()) {
            
            if (video!.status == "public") {
                alertController.addAction(UIAlertAction(title: "Make Private", style: UIAlertActionStyle.default, handler: {[weak self] (action: UIAlertAction) in
                    //private button clicked
                    self?.video!.changeStatus(to: "private", withCompletionHandler:
                        {[weak self] (success: Bool) -> Void in
                            guard let video = self?.video else { return }
                            self?.delegate?.videoWasMadePrivate(video)
                    })
                }))
            } else if (video!.status == "private") {
                alertController.addAction(UIAlertAction(title: "Make Public", style: UIAlertActionStyle.default, handler: {[weak self] (action: UIAlertAction) in
                    //public button clicked
                    self?.video!.changeStatus(to: "public", withCompletionHandler:
                        {(success: Bool) -> Void in
                    })
                }))
            }
            
            alertController.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.default, handler: { [weak self] (action: UIAlertAction) in
                //Delete button clicked
                guard let sSelf = self, let video = sSelf.video else {
                    return
                }
                let statusAlert = Utils.areYouSureAboutThat(String(format: "Are you sure you want to delete this %@?", sSelf.video!.mediaType),
                                                            confirmText: "DELETE",
                                                            confirmHandler: {
                                                                video.delete(
                                                                    completionHandler: { (success: Bool) -> Void in
                                                                        sSelf.delegate?.videoWasDeleted(video)
                                                                })
                })
                sSelf.delegate?.presentUserBarAlert(statusAlert)
            }))
            
        } else {
            guard let video = video else { return }
            alertController.addAction(UIAlertAction(
                title: String(format: video.user.followedByMe ? "Unfollow %@" : "Follow %@", video.user.fullName),
                style: UIAlertActionStyle.default,
                handler: {[weak self] (action: UIAlertAction) in
                    if (video.user.followedByMe) {
                        video.user.unfollow(handler: nil,
                                            withAmplitudePageName: (self?.isHomeScreen) != nil && self!.isHomeScreen ? "home" : "videohome",
                                            withListPosition:self?.position ?? NSNotFound)
                    } else {
                        video.user.follow(handler: nil,
                                          withAmplitudePageName: (self?.isHomeScreen) != nil && self!.isHomeScreen ? "home" : "videohome",
                                          withListPosition: self?.position ?? NSNotFound)
                    }
            }))
            
            alertController.addAction(UIAlertAction(title: "Report", style: UIAlertActionStyle.default, handler: { [weak self] (action: UIAlertAction) in
                //flag button clicked
                guard let sSelf = self else {
                    return
                }
                let flagAlert = SIAFlagAlertController.getFor(video, withPresentationHandler: { [weak sSelf] (alert) in
                    sSelf?.delegate?.presentUserBarAlert(alert!)
                })
                sSelf.delegate?.presentUserBarAlert(flagAlert!)
            }))
        }
        delegate?.presentUserBarAlert(alertController)
    }
}
