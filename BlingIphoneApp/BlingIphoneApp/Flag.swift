//
//  Flag.swift
//  BlingIphoneApp
//
//  Created by Zach on 18/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
@objc class Flag: NSObject {
    init(type: String) {
        self.type = type
    }
    var type: String
}
