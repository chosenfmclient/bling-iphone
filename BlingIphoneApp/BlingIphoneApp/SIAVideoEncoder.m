//
//  SIAVideoEncoder.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 4/13/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAVideoEncoder.h"

@implementation SIAVideoEncoder

+ (SIAVideoEncoder *)encoderForPath:(NSString *)path height:(int)cy width:(int)cx pixelFormat:(OSType)format {
    SIAVideoEncoder* enc = [SIAVideoEncoder alloc];
    [enc initPath:path Height:cy width:cx pixelFormat:format channels:0 samples:0];
    return enc;
}

+ (SIAVideoEncoder*) encoderForPath:(NSString*) path height:(int) cy width:(int) cx pixelFormat:(OSType)format channels: (int) ch samples:(Float64) rate;
{
    SIAVideoEncoder* enc = [SIAVideoEncoder alloc];
    [enc initPath:path Height:cy width:cx pixelFormat:format channels:ch samples:rate];
    return enc;
}


- (void) initPath:(NSString*)path Height:(int) cy width:(int) cx pixelFormat:(OSType)format channels:(int)ch samples:(Float64) rate;
{
    self.path = path;
    
    [[NSFileManager defaultManager] removeItemAtPath:self.path error:nil];
    NSURL* url = [NSURL fileURLWithPath:self.path];
    
    _writer = [AVAssetWriter assetWriterWithURL:url fileType:AVFileTypeQuickTimeMovie error:nil];
    NSDictionary* settings = [NSDictionary dictionaryWithObjectsAndKeys:
                              AVVideoCodecH264, AVVideoCodecKey,
                              [NSNumber numberWithInt: cx], AVVideoWidthKey,
                              [NSNumber numberWithInt: cy], AVVideoHeightKey,
                              nil];
    _videoInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:settings];
    _videoInput.expectsMediaDataInRealTime = YES;
    
    NSDictionary *bufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:format], kCVPixelBufferPixelFormatTypeKey, nil];
    _writerInputPixelBuffer = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:_videoInput sourcePixelBufferAttributes:nil];
    [_writer addInput:_videoInput];
    
    if (ch > 0)
    {
        settings = [NSDictionary dictionaryWithObjectsAndKeys:
                    [ NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                    [ NSNumber numberWithInt: ch], AVNumberOfChannelsKey,
                    [ NSNumber numberWithFloat: rate], AVSampleRateKey,
                    [ NSNumber numberWithInt: 64000 ], AVEncoderBitRateKey,
                    nil];
        _audioInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:settings];
        _audioInput.expectsMediaDataInRealTime = YES;
        [_writer addInput:_audioInput];
    }
}

- (void) finishAtTime:(CMTime)time
withCompletionHandler:(void (^)(NSURL *outputURL))handler
{
    [_writer endSessionAtSourceTime:time];
    [_writer finishWritingWithCompletionHandler:^{
        handler(_writer.outputURL);
    }];
}

- (void) finishWithCompletionHandler:(void (^)(NSURL *outputURL))handler
{
        [_writer finishWritingWithCompletionHandler:^{
            handler(_writer.outputURL);
        }];
}


- (void)cancelWriting {
    
    [_writer cancelWriting];
    
}

- (void) setVideoOrientationFromCaptureOrientation:(AVCaptureVideoOrientation)orientation{
    
    switch (orientation) {
        case AVCaptureVideoOrientationLandscapeLeft: // camera on right
            break;
        case AVCaptureVideoOrientationLandscapeRight: // camera on left
            _videoInput.transform = CGAffineTransformMakeTranslation([self videoWidth], [self videoHeight]);
            _videoInput.transform = CGAffineTransformRotate(_videoInput.transform, M_PI);
            break;
        default:
            _videoInput.transform = CGAffineTransformMakeTranslation([self videoHeight], 0);
            _videoInput.transform = CGAffineTransformRotate(_videoInput.transform, M_PI_2);
            NSLog(@"transform: %@ ", NSStringFromCGAffineTransform(_videoInput.transform));
            break;
    }
}

- (BOOL) encodeVideoFrame:(CVPixelBufferRef)pixelBuffer
         presentationTime:(CMTime)presetationTime
{       
//    if (CMSampleBufferDataIsReady(sampleBuffer))
//    {
    if (_writer.status == AVAssetWriterStatusUnknown)
    {
        CMTime startTime = presetationTime;
        [_writer startWriting];
        [_writer startSessionAtSourceTime:startTime];
        NSLog(@"Writing start time: %f", CMTimeGetSeconds(startTime));
    }
    if (_writer.status == AVAssetWriterStatusFailed)
    {
        NSLog(@"writer error %@", _writer.error);
        return NO;
    }
    
    while (!_videoInput.readyForMoreMediaData) {
        [NSThread sleepForTimeInterval:0.001];
    }
    
    if (_videoInput.readyForMoreMediaData)
    {
        BOOL appendSuccessful = [_writerInputPixelBuffer appendPixelBuffer:pixelBuffer
                                                      withPresentationTime:presetationTime];
       // NSLog(@"APPENDING SAMPLE AT TIME: %f", CMTimeGetSeconds(presetationTime));
        if (!appendSuccessful)
        {
            NSLog(@"Video Encoder append video sample failed");
        }
        return YES;
    } else {
        NSLog(@"*** NOT READY");
    }

//    }
    return NO;
}

- (BOOL) encodeAudioFrame:(CMSampleBufferRef)sampleBuffer
{
    if (CMSampleBufferDataIsReady(sampleBuffer))
    {
        if (_writer.status == AVAssetWriterStatusUnknown)
        {
            ;
        }
        
        if (_writer.status == AVAssetWriterStatusFailed)
        {
            NSLog(@"writer error %@", _writer.error.localizedDescription);
            return NO;
        }
        if (_audioInput.readyForMoreMediaData)
        {
            BOOL appendSuccessful = [_audioInput appendSampleBuffer:sampleBuffer];
            if (!appendSuccessful)
            {
                NSLog(@"Video Encoder append audio sample failed");
            }
            return YES;
        }
    }
    return NO;
}

- (CGFloat)videoWidth {
    return ((NSNumber *)[_videoInput.outputSettings objectForKey:AVVideoWidthKey]).floatValue;
}
- (CGFloat)videoHeight {
    return ((NSNumber *)[_videoInput.outputSettings objectForKey:AVVideoHeightKey]).floatValue;
}

@end
