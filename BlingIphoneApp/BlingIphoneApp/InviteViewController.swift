//
//  InviteViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 22/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

@objc class InviteViewController: SIABaseViewController, UITableViewDelegate, UITableViewDataSource, InvitedDelegate, SearchableUserList {

    //MARK: IBOutlets
    @IBOutlet weak var topSectionView: UIView!
    @IBOutlet weak var inviteAllButton: UIButton!
    @IBOutlet weak var totalContactsLabel: UILabel!
    @IBOutlet weak var contactsTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    //MARK: properties
    var contacts = [DeviceContact]()
    var filteredContacts = [DeviceContact]()
    var currentSearch = ""
    weak var invitedDelegate: InvitedDelegate?
    weak var cameoInviteDelegate: CameoInvites?
    var contactHandler = ContactsHandler()
    var isRegistration = true

    //MARK: View controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if isCameoInvite {
            inviteAllButton.backgroundColor = SIAStyling.defaultStyle().cameoPink()
        }
    }
    
    override func viewDidLayoutSubviews() {
        inviteAllButton?.bia_rounded()
        if !isRegistration {
            topSectionView.backgroundColor = UIColor(colorLiteralRed: 242 / 255, green: 242 / 255, blue: 242 / 255, alpha: 1)
            totalContactsLabel.textColor = UIColor(colorLiteralRed: 128 / 255, green:  128 / 255, blue:  128 / 255, alpha: 1)
        }
    }

    
    //MARK: Table view delegate methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Contact", for: indexPath) as! InviteContactTableViewCell
        cell.isCameoInvite = isCameoInvite
        cell.cameoDelegate = self.cameoInviteDelegate
        if let image = filteredContacts[indexPath.row].image {
            cell.userImageView.image = image
        } else {
            cell.userImageView.image = UIImage(named: "extra_large_userpic_placeholder.png")
        }
        
        cell.userNameLabel.text = filteredContacts[indexPath.row].name
        if !isRegistration {
            cell.userNameLabel.textColor = UIColor.black
        }
        cell.contact = filteredContacts[indexPath.row]
        
        weak var wself = self
        cell.delegate = wself
        
        cell.inviteButton.isEnabled = !filteredContacts[indexPath.row].wasInvited
        let inviteButtonImage = filteredContacts[indexPath.row].wasInvited ? UIImage(named:"icon_invited_friend_grey.png") :
            inviteIconImage
        
        cell.inviteButton.setImage(inviteButtonImage, for: .normal)
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        
        if isRegistration {
            cell.seperatorView.backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 1, alpha: 0.25)
        }
        return cell
    }
    
    var isCameoInvite = false
    var inviteIconImage: UIImage {
        return UIImage(named: "icon_invite_friend_\(colorName)")!
    }
    
    var colorName: String {
        return isCameoInvite ? "pink" : "green"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        (tableView.cellForRow(at: indexPath) as! InviteContactTableViewCell).inviteButtonWasTapped(0 as AnyObject)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredContacts.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    //MARK: IBActions
    @IBAction func inviteAllWasTapped(_ sender: AnyObject) {
        
        guard !isCameoInvite else {
            cameoInviteDelegate?.addContactListToInvitees(contacts)
            reloadSearch()
            contactsTableView.reloadData()
            return
        }
        
        inviteAllButton.isEnabled = false
        contactsTableView.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
        contactHandler.inviteAll(completed: {[weak self] (phoneContacts: [DeviceContact], emailContacts: [DeviceContact]) in
            guard let _ = self else { return }
            self?.contacts = phoneContacts + emailContacts
            self?.reloadSearch()
            self?.invitedDelegate?.contactWasInvited()
            self?.contactsTableView.reloadData()
            self?.contactsTableView.isUserInteractionEnabled = true
            self?.activityIndicator.stopAnimating()
            let amplitudeProps = ["invite_option" : "all",
                                  "invites_sent"  : NSNumber(value: self!.contacts.count)] as [String : Any]
            AmplitudeEvents.log(event: "invite:send", with: amplitudeProps)
        })
    }
    
    //MARK: Contacts
    func getContactsToInvite(_ completion: (() -> Void)? = nil) {
        guard contacts.count < 1 else {
            completion?()
            return
        }
        Utils.dispatchAsync {[weak self] in
            self?.contactHandler.loadContacts {[weak self] (phoneContacts: [DeviceContact], emailContacts: [DeviceContact], friendsOnBling: [SIAUser]) in
                guard let sSelf = self else { return }
                sSelf.contacts = phoneContacts + emailContacts
                sSelf.filteredContacts = sSelf.contacts
                completion?()
                Utils.dispatchOnMainThread {
                    sSelf.activityIndicator.stopAnimating()
                    sSelf.topSectionView.isHidden = false
                    sSelf.totalContactsLabel.text = String(sSelf.contacts.count) + sSelf.headerString
                    sSelf.contactsTableView.reloadData()
                    if sSelf.contacts.count < 1 {
                        sSelf.inviteAllButton.isEnabled = false
                    }
                }
            }
        }
    }
    
    var headerString: String! {
        return isCameoInvite ? " Friends" : " Contacts"
    }
    
    func contactWasInvited() {
        reloadSearch()
        self.contactsTableView.reloadData()
        invitedDelegate?.contactWasInvited()
    }
    
    func reloadSearch() {
        guard !currentSearch.isEmpty else {
            return
        }
        filteredContacts = []
        for contact in contacts {
            guard let name = contact.name else { return }
            if name.lowercased().contains(currentSearch.lowercased()) {
                filteredContacts.append(contact)
            }
        }
        contactsTableView.reloadData()
    }
    
    func newSearch(_ search: String) {
        guard search != currentSearch else { return }
        guard !search.isEmpty else { return }
        
        currentSearch = search
        reloadSearch()
    }
    
    func searchCanceled() {
        currentSearch = ""
        filteredContacts = contacts
        contactsTableView.reloadData()
    }

}

protocol InvitedDelegate: class {
    func contactWasInvited()
}

//MARK: cell class
class InviteContactTableViewCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var inviteButton: UIButton!
    @IBOutlet var seperatorView: UIView!
    var isCameoInvite = false
    weak var contact: DeviceContact?
    weak var delegate: InvitedDelegate?
    weak var cameoDelegate: CameoInvites?
    
    @IBAction func inviteButtonWasTapped(_ sender: AnyObject) {
        guard !isCameoInvite else {
            cameoDelegate?.addContactToInvitees(contact!)
            delegate?.contactWasInvited()
            return
        }
        contact?.invite(completed: { [weak self] in
            self?.delegate?.contactWasInvited()
            AmplitudeEvents.log(event: "invite:send", with: ["invite_option":"selected"])
        })
    }
    
    override func layoutSubviews() {
        userImageView.bia_rounded()
    }
}
