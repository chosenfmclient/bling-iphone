//
//  SIAApnSettingsDataModel.m
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 8/12/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAApnSettingsDataModel.h"

@implementation SIAApnSettingsDataModel

+(SIAApnSettingsDataModel *)modelWithDeviceToken:(NSData *)deviceToken {
    NSString *devToken = deviceToken == nil ? nil : [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    devToken = [devToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"device token ---%@", devToken);
    
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    NSLog(@"bundle id: %@", bundleIdentifier);
    
    NSString *AppVersion = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"];
    NSLog(@"app version: %@", AppVersion);
    SIAApnSettingsDataModel *model = [[SIAApnSettingsDataModel alloc] init];
    model.device_token = devToken;
    model.bundle_id = bundleIdentifier;
    model.app_version = AppVersion;
    return model;
}

+(RKObjectMapping *)requestMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromArray:@[@"device_token", @"bundle_id", @"app_version"]];
    return mapping;
}

+(RKObjectMapping *)responseMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[SIAApnSettingsDataModel class]];
    [mapping addAttributeMappingsFromArray:@[@"device_token", @"bundle_id", @"app_version"]];
    return mapping;
}

@end
