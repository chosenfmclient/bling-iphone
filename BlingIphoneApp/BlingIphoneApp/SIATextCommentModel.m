//
//  SIATextCommentModel.m
//  ChosenIphoneApp
//
//  Created by Joseph Nahmias on 12/01/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIATextCommentModel.h"

#define GET_COMMENTS_PAGE_API_STR @"api/v1/videos/%@/comments"
#define COMMENTS_API @"api/v1/comments"
#define FLAG_API_EXTENTION_STR @"flag"

@interface SIATextCommentModel ()


@end

@implementation SIATextCommentModel

- (instancetype)initWithVideoId:(NSString *)videoId {
    self = [super init];
    if (self) {
        self.videoId = videoId;
    }
    return self;
}

- (instancetype) initWithObjectId:(NSString*) objectId {
    return [self initWithVideoId:objectId];
}

+ (RKObjectMapping *)objectMapping {
    return [SIATextComment objectMapping];
}

+ (RKMapping *) pageMapping {
    return [SIAPaginatorDataModel responseMappingForTotalKey:@"total_comments"
                                               objectMapping:[SIATextCommentModel objectMapping]
                                                  modelClass:[SIATextCommentModel class]];
}

- (NSString *)pagePath {
    return [NSString stringWithFormat:GET_COMMENTS_PAGE_API_STR, _videoId];
}

+ (NSString *)pathPattern {
    return @"api/v1/videos/:id/comments";
}

- (void)setPageArray:(NSArray *)pageArray {
    if (pageArray) {
        self.commentsArray = pageArray;
    }
    super.pageArray = pageArray;
}

- (NSUInteger)totalComments {
    return self.totalObjects.unsignedIntegerValue;
}

+ (void)getCommentsForVideoId:(NSString *)videoId withCompletionHandler:(void (^)(SIATextCommentModel *model))handler {
    
    
    //TODO: Fix the mapping dummy
    [RequestManager
     getRequest:[NSString stringWithFormat:GET_COMMENTS_PAGE_API_STR, videoId]
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         if (![mappingResult.firstObject isKindOfClass:[SIATextCommentModel class]])
         {
             handler(nil);
             return;
         }
         SIATextCommentModel *results = mappingResult.firstObject;
         results.videoId = videoId;
         handler(results);
     }
     failure:^(RKObjectRequestOperation *operation, NSError *error) {
         
         NSLog(@"What do you mean by 'there are no videos?': %@", error);
         handler(nil);
     }
     showFailureDialog:YES];
}

+ (void)postComment:(NSString *)text forVideo:(NSString *)videoId withCompletionHandler:(void (^)(SIATextComment *model))handler {

    if (!videoId || !text)
    {
        handler(nil);
        return;
    }

    CommentRequestModel *requestObject = [[CommentRequestModel alloc] initWithVideo_id:videoId text:text];
    
    [RequestManager
     postRequest:COMMENTS_API
     object:requestObject
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         handler(mappingResult.firstObject);
         NSLog(@"send comment success: %@", videoId);
         //[sia_AppDelegate.pushRegistrationHandler commented];
         //Update comments in game if video is displayed...
         NSDictionary *ui = @{@"comment":text,
                              @"video_id":videoId,
                              @"edited":@(NO)};
         
         [[NSNotificationCenter defaultCenter] postNotificationName:SIACommentUpdateNotification object:mappingResult.firstObject userInfo:ui];
         
         /*TuneEvent *event = [TuneEvent eventWithName:@"rated"];
          event.contentId = videoId;
          [Tune measureEvent:event];*/
         [FBSDKAppEvents logEvent:@"rated"
                       parameters:@{@"video_id":videoId}];
     }
     
     failure:^(RKObjectRequestOperation *operation, NSError *error) {
         handler(nil);
         NSLog(@"send comment failed %@", videoId);
     }
     showFailureDialog:YES];
}

+ (void)editComment:(NSString *)commentId newText:(NSString *)text withCompletionHandler:(void (^)(BOOL success))handler {
    SIATextComment* comment = [[SIATextComment alloc] initWithText:text videoId:nil];
    [RequestManager
     putRequest:[COMMENTS_API stringByAppendingFormat:@"/%@", commentId]
     object:comment
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         handler(YES);
         NSLog(@"edit comment success: %@", commentId);
     }
     failure:^(RKObjectRequestOperation *operation, NSError *error) {
         handler(NO);
         NSLog(@"edit comment failed %@", commentId);
     }
     showFailureDialog:YES];
}

+ (void)flagComment:(NSString *)commentId asType:(SIATextCommetFlagType)type withCompletionHandler:(void (^)(BOOL success))handler {
    Flag* flag  = [[Flag alloc] initWithType:@(type).stringValue];
    [RequestManager
     putRequest:[COMMENTS_API stringByAppendingFormat:@"/%@/%@", commentId, FLAG_API_EXTENTION_STR]
     object:flag
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         handler(YES);
         NSLog(@"flag comment success: %@", commentId);
     }
     
     failure:^(RKObjectRequestOperation *operation, NSError *error) {
         handler(NO);
         NSLog(@"flag comment failed %@", commentId);
     }
     showFailureDialog:YES];
}

+ (void)deleteComment:(NSString *)commentId withCompletionHandler:(void (^)(BOOL success))handler {
    
    [RequestManager
     deleteRequest:[COMMENTS_API stringByAppendingFormat:@"/%@", commentId]
     object:nil
     queryParameters:nil
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         handler(YES);
         NSLog(@"delete comment success: %@", commentId);
     }
     
     failure:^(RKObjectRequestOperation *operation, NSError *error) {
         handler(NO);
         NSLog(@"delete comment failed %@", commentId);
     }
     showFailureDialog:YES];
}


+ (void)testMe:(NSString *)videoId {

    [SIATextCommentModel postComment:@"first try comment" forVideo:videoId withCompletionHandler:^(SIATextComment *comment) {
        NSLog(@"comment id: %@", comment.commentId);
//        [comment flagCommentAsType:CommetFlagTypeViolent withCompletionHandler:^(BOOL success) {
//            NSLog(@"flaged");
//        }];
        [SIATextCommentModel getCommentsForVideoId:videoId withCompletionHandler:^(SIATextCommentModel *model) {
            NSString *commentId = model.commentsArray.firstObject.commentId;
            [model getNextCommentsPageWithCompetionHandler:^(BOOL success, NSArray *newComments) {
                NSLog(@"newComments %@", newComments);
            }];
            [SIATextCommentModel editComment:commentId newText:@"edited Comment" withCompletionHandler:^(BOOL success) {
                [SIATextCommentModel deleteComment:commentId withCompletionHandler:^(BOOL success) {
                    [SIATextCommentModel getCommentsForVideoId:videoId withCompletionHandler:^(SIATextCommentModel *model) {
                        NSString *commentText = model.commentsArray.firstObject.text;
                        NSLog(@"comment text: %@", commentText);
                    }];
                }];
            }];
        }];
    }];

}

@end
