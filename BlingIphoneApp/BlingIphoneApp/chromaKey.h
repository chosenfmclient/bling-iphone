//
// Created by Ben on 27/11/2016.
//

#ifndef BLINGY_OCV_NDK_CHROMAKEY_H
#define BLINGY_OCV_NDK_CHROMAKEY_H

#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"

#define EDGES_RATIO 0.1

#include <stdlib.h>
#include <stdio.h>

using namespace cv;

typedef struct chromaKeyBufferStructTypeDef
{
    Mat& dss;
    Mat& dst;
    int leftSide;
    int rightSide;
} chromaKeyBufferStructure;


typedef enum : int {
    DetectionResponseSuccess = 0,
    DetectionResponseFailure = 1,
    DetectionResponseHighLight = 2,
    DetectionResponseLowLight = 3,
    DetectionResponseTooManyColors = 4,
    DetectionResponseNoChromaColors = 5,
    DetectionResponseTooManyEdges = 6,
    DetectionResponseNoFaceDetection = 7
} DetectionResponse;

class chromaKey
{
    public:
        chromaKey();
        virtual ~chromaKey();
//        void findChromaKeys(Mat mDss);
        DetectionResponse findChromaKeys(Mat mDss, int faceLeft, int faceTop, int faceRight, int faceBottom, bool frontCamera);

        DetectionResponse addChromaKeys(Mat mDss, int faceLeft, int faceTop, int faceRight, int faceBottom, bool frontCamera);

        void addChromaKeysSectioned(Mat& mDss, int left, int top, int right, int bottom);

        void addChromaKey(uchar* dss, int index);

        Mat checkAndAddChromaKeysTopArea(Mat mDss);

        Mat checkAndAddChromaKeysLeftArea(Mat mDss);

        Mat checkAndAddChromaKeysRightArea(Mat mDss);

        bool checkChromaKeysAgainstSkinColor();

        /**
         * returns true if one or more of the chromaKeys has been deducted
         */
        bool checkAndDeductChromaKeysAgainstSkinColor();

        void getChromaKeysBuffer(Mat& mDss, Mat& chromaKeyBuffer, int faceLeft, int faceTop, int faceRight, int faceBottom);
    
    void convertArraysToScalars();
    void getChromaKeysBufferII(Mat mDss, Mat chromaKeyBuffer, int faceLeft, int faceTop, int faceRight, int faceBottom);

        void getChromaKeysSectionBuffer(Mat mDss, Mat chromaKeyBuffer, int leftSide, int rightSide);

        void getChromaKeysSectionBufferWithBlured(Mat bDss, Mat chromaKeyBuffer, int leftSide, int rightSide);

        /**
         * finds the skin color of the present person
         */
        bool saveSkinColorHsl(uchar* dss, unsigned int faceRight, unsigned int faceTop, unsigned int faceWidth, unsigned int faceHeight);

        void resetChromaKeys();

        bool isKeysEmpty();
    
        void skinColorClustering(Mat faceImage);
        void saveSkinColorHsl();
        void drawSkinColor(Mat bg);

        /**
         *  convert rgb to XYZ
         */
        void rgbToXYZ(float red, float green, float blue, float *X, float *Y, float *Z);

        /**
         *  convert XYZ to Rgb
         */
        void XYZToRgb(float X, float Y, float Z, float *red, float *green, float *blue);

        /**
         *  convert XYZ to Lab
         */
        void XYZToLab(float X, float Y, float Z, float *L, float *a, float *b);

        /**
         *  convert Lab to XYZ
         */
        void LabToXYZ(float L, float a, float b, float *X, float *Y, float *Z);

        void getChromaKeysSectionBufferThreaded(Mat& dss, Mat& dst);

        pthread_t getChromaKeysSectionBufferThread(Mat& dss, Mat& dst, int leftSide, int rightSide);
//        void rooting(float base, float power, precision);

        int getArrayLength();
    
        DetectionResponse checkFaceLightness(Mat faceImage,int faceLeft, int faceTop, int faceRight, int faceBottom);
    bool faceLightness;

    private:
        const static int
                increase = 8,
                byteIncrease = increase * 4,

                HUE_DIF_THRESHOLD = 2,             // 0 - 360
                LUMINANCE_DIF_THRESHOLD = 5,        // 0 - 255

                HUE_2_THRESHOLD = 10,
                SATURATION_2_THRESHOLD = 2000,
                LUMINANCE_2_THRESHOLD = 20,

                SKIN_COLOR_THRESHOLD = 5;

        constexpr const static float
                  SATURATION_DIF_THRESHOLD = 0.05f

                , PADDING_MULTIPLIER = 0.4//0.6f
                , PADDING_MULTIPLIER_HALF = .25f
                ;

        constexpr const static float e = 0.008856f;
        constexpr const static float refX = 95.047f, refY = 100.0f, refZ = 108.883f;


        const static int COLOR_THRESHOLD = 35;

        /////////////Brightness////////////////////////////////////////////////////////
//        const static float userBrightness = 2.0f;   // doesn't work, still leaning on hardware brightness auto adjustment

//        const static int optimumLuminance4Chroma = 240, optimumContrast4Chroma = 210;// 0 - 255
        const static int optimumLuminance4Chroma = 200, optimumContrast4Chroma = 200;// 0 - 255
        int BRIGHTNESS;
        float CONTRAST;//2.5f      = 1.0f
        bool isSet;
        int count4Loop;

        ///////////////////////////////////////////////////////////////////////////////

        const static int TOP_AREA_WIDTH = 20, LEFT_AREA_HEIGHT = 50, RIGHT_AREA_HEIGHT = 50;

        /////////////Chroma key bufffers///////////////////////////////////////////////

        constexpr const static float SATURATION_THRESHOLD = 0.1f; // 0.04 // 0.1 // 0.02

        const static int
                HUE_THRESHOLD = 18;//20; // 13 // 5 // 10
//                LUMINANCE_THRESHOLD = 15; // 10
        constexpr const static float LUMINANCE_THRESHOLD = 0.08f; // 0.08 // 10 // 0.05

        const static int CHROMA_KEY_ARRAY_LENGTH = 170; //

        const static int   DIF_HUE_THRESHOLD = 4;
        constexpr const static float DIF_LUMINANCE_THRESHOLD = 0.02f
                         , DIF_SATURATION_THRESHOLD = 0.025f;

//        bool inInitChromaKey;

        float hueTop, hueBot, luminanceTop, luminanceBottom, saturationTop, saturationBottom;
        int chromaKeysArrayLength, baseChromaKeysArrayLength;

        float* mHuesBot;
        float* mHuesTop;
        float* mLuminanceBot;
        float* mSaturationBot;
        float* mLuminanceTop;
        float* mSaturationTop;

        std::vector<Vec3b> chromakeyColorsMin, chromakeyColorsMax;

        /////////////Save skin color///////////////////////////////////////

        const static int skinHslArrayLength = 170;
        constexpr const static float SKIN_SATURATION_THRESHOLD = 0.05f;//0.1
        const static int SKIN_HUE_THRESHOLD = 9;//5
//                       , SKIN_LUMINANCE_THRESHOLD = 10;//10;
        constexpr const static float SKIN_LUMINANCE_THRESHOLD = 0.04f;//10;

        bool foundSkinColor;
        float skinHueValue, skinSaturationValue, skinLightnessValue;

        int skinArrayLength;

        float* skinHuesBot;
        float* skinHuesTop;
        float* skinSaturationBot;
        float* skinSaturationTop;
        float* skinLuminanceBot;
        float* skinLuminanceTop;
        ///////////////////////////////////////////////////////////////////
        //k means skin color
    
        static constexpr float SKIN_FILTER_SATURATION_THRESHOLD = 0.05f;//0.1
        const static int SKIN_FILTER_HUE_THRESHOLD = 9;//5
        //                       , SKIN_LUMINANCE_THRESHOLD = 10;//10;
        static constexpr float SKIN_FILTER_LUMINANCE_THRESHOLD = 0.12f;//10;
    
        const static int clusterCount = 8;
        float skinColors[clusterCount * 3];
        float skinColorsHsl[clusterCount * 3];
        /////////////////////////////////////




        /**
         * find if the hsv is in the found chroma keys
         */
        bool isInChromaKeys(int hue, float luminance, float saturation);

        /**
         * check if the hsv is a common color skin
         */
        bool isSkinColor(int hue, float luminance, float saturation);

        /**
         * check if the rgb is in thresh hold
         */
        bool isColorInThreshold(int red, float green, float blue);

//        /**
//         * check if the hsv is a common color skins
//         */
//        bool isSkinColors(int hue, float luminance, float saturation);

        /**
         *  convert rgb to hsv
         */
        void rgbToHsl(int red, int green, int blue, float *hue, float *saturation, float *luminance);


        /**
         * check if the pixel is in face
         */
        bool isInFace(int c, int r, int faceLeft, int faceTop, int faceRight, int faceBottom);

        bool addSkinColorHsl(uchar* dss, unsigned int faceRight, unsigned int faceTop, unsigned int faceWidth, unsigned int faceHeight);

        void getShapeBrightness(uchar* dss, unsigned int channels, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom);

        void getShapeContrast(uchar* dss, unsigned int channels, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom);

        /**
         * change the brightness
         * set from 1.0 for no change, over for brighter
         *
         * dss - source
         * dsd - destination
         */
        void setBrightness(uchar* dss, uchar* dsd);

        /**
         * change the brightness
         * set from 1.0 for no change, over for brighter
         *
         * dss - source
         * dsd - destination
         */
        void setBrightnessAndContrast(uchar* dss, uchar* dsd);

};

#endif //BLINGY_OCV_NDK_CHROMAKEY_H
