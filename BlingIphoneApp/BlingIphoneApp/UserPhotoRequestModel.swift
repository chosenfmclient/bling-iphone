//
//  UserPhotoRequestModel.swift
//  BlingIphoneApp
//
//  Created by Zach on 20/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit
@objc class UserPhotoRequestModel: NSObject {
    var photo: String
    
    override init() {
        photo = String(format: "%f", Double(Date().timeIntervalSince1970 * 1000)).replacingOccurrences(of: ".", with: "") + ".jpg"
        super.init()
    }
    
    static func objectMapping() -> RKObjectMapping {
        let mapping = RKObjectMapping.request()
        mapping?.addAttributeMappings(from: ["photo"])
        return mapping!
    }
}
