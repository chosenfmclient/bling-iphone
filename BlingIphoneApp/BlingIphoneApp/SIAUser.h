//
//  SIAUser.h
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 20/07/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <RestKit/RestKit.h>
#import <SDWebImage/SDWebImageManager.h>
#import <Foundation/Foundation.h>

FOUNDATION_EXTERN NSString *const SIAUserDidLoadImageNotification;

@interface ExperiencePoints : NSObject

@property(strong, nonatomic) NSString* current;
@property(strong, nonatomic) NSString* required;
@property(strong, nonatomic) NSString* required_total;
@property(strong, nonatomic) NSString* earned;
@property(strong, nonatomic) NSString* lacked;

@end

@interface SIAUser : NSObject

@property(strong, nonatomic) NSString *user_id;
@property(strong, nonatomic) NSString *twitter_id;
@property(strong, nonatomic) NSString *first_name;
@property(strong, nonatomic) NSString *last_name;
@property(strong, nonatomic) NSString *email;
@property(strong, nonatomic) NSString *bio;
@property(strong, nonatomic) NSString *location;
@property(strong, nonatomic) NSString *gender;
@property(strong, nonatomic) NSString *twitter_handle;
@property(strong, nonatomic) NSString *youtube_handle;
@property(strong, nonatomic) NSString *instagram_handle;
@property(strong, nonatomic) NSString *facebook_handle;
@property(nonatomic) BOOL followedByMe;
@property(nonatomic, strong) NSString *following_me;
@property(nonatomic, strong) NSNumber* total_performances;
@property(nonatomic, readonly, strong) NSArray *prformances;
@property(nonatomic, readonly) NSNumber* total_responses;
@property(nonatomic, readonly, strong) NSArray *responses;
@property(nonatomic, readonly, strong) NSArray *activities;
@property(nonatomic, strong) NSNumber* total_followers;
@property(nonatomic, strong) NSNumber* total_following;
@property(nonatomic, strong) NSURL *imageURL;

@property (nonatomic) BOOL wasInvited;

@property (nonatomic, readonly) NSString *fullName;

@property (nonatomic, readwrite, strong) UIImage *image;
- (void)imageWithCompletionHandler:(void(^)(UIImage *image))handler;
+(RKObjectMapping *)objectMapping;
+(RKObjectMapping *)requestMapping;
-(void)getData;
+ (void)getUserWithId: (NSString*) userId  completionHandler: (void (^)(SIAUser *user))handler;
- (void)unfollowWithHandler:(void (^)(BOOL success))handler
      withAmplitudePageName:(NSString*) pageName;
- (void)unfollowWithHandler:(void (^)(BOOL success))handler
      withAmplitudePageName:(NSString*) pageName
           withListPosition:(NSInteger)position;
- (void)followWithHandler:(void(^)(BOOL success))handler
    withAmplitudePageName:(NSString*) pageName;
- (void)followWithHandler:(void (^)(BOOL success))handler
    withAmplitudePageName:(NSString*) pageName
         withListPosition:(NSInteger)position;
@end
