//
//  SIACommentTableViewCell.h
//  
//
//  Created by Michael Biehl on 1/18/16.
//
//

#import <UIKit/UIKit.h>

@interface SIACommentTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentLabel;
@property (nonatomic) IBInspectable CGFloat imageViewCornerRadius;
@property (nonatomic) IBInspectable CGRect imageViewFrame;
@property (nonatomic) BOOL layedOut;
- (void)setSeparator:(UIColor *)separatorColor;

@end
