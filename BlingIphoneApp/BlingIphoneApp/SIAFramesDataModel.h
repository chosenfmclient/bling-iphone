//
//  SIAFramesDataModel.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 3/24/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SIAFrame : NSObject

@property (strong, nonatomic) NSString *name;
@property (nonatomic)         NSInteger frame_id;
@property (strong, nonatomic) NSURL *texture;
@property (strong, nonatomic) NSDictionary *frames;

@end

@interface SIAFramesDataModel : NSObject

@property (strong, nonatomic) NSArray <SIAFrame *> *frames;

+ (void)getFrames:(void(^)(SIAFramesDataModel *model))completion;
+ (NSString *)aspectRatioKeyForAspectRatio:(CGFloat)aspectRatio;

@end
