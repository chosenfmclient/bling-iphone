//
//  SIABasePostRecordViewController.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/17/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIABaseViewController.h"
#import "BIAVideoPlayer.h"

@protocol SIABasePostRecordViewControllerDelegate <NSObject>
- (void)next;
- (void)back;
- (void)close;
- (void)startUploadForPrivate:(BOOL)isPrivate;
- (void)thumbnailWasChosen:(UIImage *)thumbnail;
- (void)frameWasChosen:(NSURL *)frameURL
            frameIndex:(NSUInteger)frameIdx;
- (void)photoFilterWasApplied;
- (void)recordingItemWasChanged:(SIARecordingItem *)recordingItem;
- (void)repeatPlaybackFrom:(CGFloat)startTime to:(CGFloat)endTime;
- (NSString *)photoOrVideo;
@end

@interface SIABasePostRecordViewController : SIABaseViewController

@property (weak, nonatomic) id <SIABasePostRecordViewControllerDelegate> delegate;
@property (nonatomic, copy) void (^completionHandler)(SIARecordingItem *recordingItem);

- (void)next;
- (void)back;

@end
