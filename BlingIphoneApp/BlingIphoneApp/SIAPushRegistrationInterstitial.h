//
//  SIAPushRegistrationInterstitial.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 8/18/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIAPushRegistrationInterstitial : UIView <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *interstitialView;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (copy, nonatomic) void(^completionHandler)(BOOL);

- (void)show;
- (void)dismiss;
@end
