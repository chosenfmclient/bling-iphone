//
//  SIACommentConversation.m
//  ChosenIphoneApp
//
//  Created by Zach on 08/09/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIACommentConversation.h"

@implementation SIACommentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        SIATextCommentView *commentView = [[SIATextCommentView alloc] init];
        self.commentView = commentView;
        self.commentView.alpha = 0.7;
        self.commentView.commentColor = @"black";
        self.commentView.backgroundColor = [UIColor whiteColor];
        self.commentView.opaque = NO;
        
        self.commentView.layer.shouldRasterize = YES;
        self.commentView.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.commentView.layer.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.35].CGColor;
        self.commentView.layer.shadowOffset = CGSizeMake(0, 2);
        self.commentView.layer.shadowRadius = 1.0;
        self.commentView.layer.shadowOpacity = 0.35f;
        
        self.commentView.userInteractionEnabled = NO;
        [self.contentView addSubview:self.commentView];
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
@end


@implementation SIACommentConversation

- (instancetype) initWithTableView: (UITableView*) tableView comments: (NSMutableArray*)comments {
    self = [super init];
    
    self.chatTableView = tableView;
    self.chatTableView.dataSource = self;
    self.chatTableView.delegate = self;
    
    [self.chatTableView registerClass:[SIACommentCell class]
               forCellReuseIdentifier:@"commentCell"];
    self.chatTableView.userInteractionEnabled = NO;

    _comments = comments;
    
    return self;
}

- (void)commentFromArray:(NSArray *)comments {
    [_comments addObjectsFromArray:comments];
    [self.comments enumerateObjectsUsingBlock:^(SIATextComment *comment, NSUInteger idx, BOOL *_Nonnull stop) {
        [comment.user imageWithCompletionHandler:nil];
    }];
    [Utils dispatchOnMainThread:^{
        [self.chatTableView reloadData];
    }];
}

- (void)appendComment:(SIATextComment *)comment {
    [_comments addObject:comment];
    
    if (!_simulationStarted) {
        [self showCommentWithIndex:self.comments.count-1];
    }
}

- (id) compareCommentArrayWith: (SIATextComment*) comment {
    return ^BOOL(SIATextComment *commentFromArray, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([commentFromArray.commentId isEqualToString:comment.commentId])
        {
            return YES;
        }
        return NO;
    };
}

- (void)replaceComment:(SIATextComment*)comment {
    NSUInteger index = [_comments indexOfObjectPassingTest: [self compareCommentArrayWith: comment]];
    if(index != NSNotFound)
    {
        [_comments replaceObjectAtIndex:index withObject:comment];
        NSLog(@"comment replaced in _comments");
    }
    
    
    index = [_commentsForTable  indexOfObjectPassingTest: [self compareCommentArrayWith: comment]];
    if(index != NSNotFound)
    {
        [_commentsForTable replaceObjectAtIndex:index withObject:comment];
        NSLog(@"comment replaced in _commentsForTable");
    }
    
    [Utils dispatchOnMainThread:^{
        [self.chatTableView reloadData];
        NSLog(@"Reload comments");
    }];
}

- (void)insertComment:(SIATextComment *)comment atIndex:(NSUInteger)index {
    if (index >= self.comments.count) {
        [self appendComment:comment];
        return;
    }
    
    [_comments insertObject:comment atIndex:index];
    
    if (!_simulationStarted) {
        [self showCommentWithIndex:index];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_commentsForTable count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SIATextComment *comment = indexPath.row <= _commentsForTable.count ? _commentsForTable[indexPath.row] : nil;
    SIACommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commentCell" forIndexPath:indexPath];
    
    if ([comment.user.user_id isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:kUserIdKeyStr]])
    {
        cell.commentView.name = @"You!";
    }
    else
    {
        cell.commentView.name = comment.user.fullName;
    }
    
    cell.commentView.comment = comment.text;
    cell.commentView.backgroundColor = [UIColor whiteColor];
    cell.commentView.commentColor = @"black";
    
    cell.commentView.layer.cornerRadius = 23.0;
    
    [comment.user imageWithCompletionHandler:^(UIImage *image) {
        cell.commentView.image = image;
    }];
    
    [UIView setAnimationsEnabled:NO];
    [cell.commentView sizeToFit];
    
    [UIView setAnimationsEnabled:YES];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    SIATextComment *comment = _commentsForTable[indexPath.row];
    
    SIATextCommentView *commentView = [[SIATextCommentView alloc] init];
    commentView.name = comment.user.fullName;
    commentView.comment = comment.text;
    
    [commentView sizeToFit];
    
    return commentView.frame.size.height + 5.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectZero];
    headerView.userInteractionEnabled = NO; // all touches within this space must go through to the video layer
    
    return headerView; // empty header above chat, so messages flow in from bottom
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.chatTableView.frame.size.height;            // empty header above chat, so messages flow in from bottom
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger diff = (self.commentsForTable.count-1)-indexPath.row;
    
    [UIView setAnimationsEnabled:NO];
    if(diff > 2)
        cell.alpha = 0.0f;
    [UIView setAnimationsEnabled:YES];
}

- (void)showCommentWithIndex:(NSUInteger)index {
    // mainQueue(^{
    
    //  NSLog(@"Showing comment at index %lu", index);
    @try { // I hate myself for doing this. in fact this is the first time in my entire obj-c career that i've used @try. Good, go fuck yourself.
        if (index >= [self.comments count])
            return;
        
        if([self.commentsForTable containsObject:[self.comments objectAtIndex:index]])
            return;
        
//        if ([self.delegate respondsToSelector:@selector(cardView:willShowComment:withIndex:)])
//            [self.delegate cardView:self willShowComment:self.comments[index] withIndex:index];
        
        if(!self.commentsForTable)
            self.commentsForTable = [NSMutableArray arrayWithCapacity:self.comments.count];
        
        [self.commentsForTable addObject:[self.comments objectAtIndex:index]];
        
        if(index == 0 || index == self.comments.count - 1)
        {
            [self.chatTableView reloadData];
            [self scrollTableToBottom];
            
            return;
        }
        
        [self.chatTableView beginUpdates];
        [self.chatTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:(UITableViewRowAnimationNone)];
        [self.chatTableView endUpdates];
        [self scrollTableToBottom];
        
    } @catch (NSException *exception) {
        if([[exception name] isEqualToString:NSInternalInconsistencyException]) {
            [self.chatTableView reloadData];
            NSLog(@"insert failed, trying to reload data");
        }
        else {
            @throw exception;
        }
    } @finally {
        
    }
    // });
}

- (NSArray*)commentIndexPathsForAnimation
{
    NSArray *indexPaths = [self.chatTableView indexPathsForVisibleRows];
    if (indexPaths.count < 1)
    {
        return @[[NSIndexPath indexPathForRow:0
                                    inSection:0]];
    }
    NSRange range = NSMakeRange(indexPaths.count >= 3 ? indexPaths.count - 3 : 0, MIN(indexPaths.count, 3));
    return [indexPaths subarrayWithRange:range];
}

- (void)scrollTableToBottom
{
    if (self.commentsForTable.count == 0)
        return;
    
    CGFloat offsetY = self.chatTableView.contentSize.height - self.chatTableView.frame.size.height + self.chatTableView.contentInset.bottom;
    
    [UIView animateWithDuration:0.33
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         [self.chatTableView setContentOffset:CGPointMake(0, offsetY)
                                                     animated:NO];
                         [self applyCorrectCellAlpha];
                     }
                     completion:nil];
}

- (void)applyCorrectCellAlpha
{
    NSArray *indexPaths = [self commentIndexPathsForAnimation];
    
    [UIView setAnimationsEnabled:NO];
    for (NSIndexPath *path in [self.chatTableView indexPathsForVisibleRows]) {
        if([indexPaths indexOfObject:path] == NSNotFound && path.row < self.commentsForTable.count)
        {
            [self.chatTableView cellForRowAtIndexPath:path].alpha = 0;
        }
    }
    [UIView setAnimationsEnabled:YES];
    
    CGFloat targetAlpha = .7f;
    for (NSIndexPath *path in [indexPaths reverseObjectEnumerator]) {
        [self.chatTableView cellForRowAtIndexPath:path].alpha = targetAlpha;
        if(targetAlpha > 0.0) {
            targetAlpha -= 0.3;
        }
        else { targetAlpha = 0.0;}
    }
}

- (void)simulateCommentChatStartImmediately {
    [self simulateCommentChatWithInterval:0.0f];
}

- (void)simulateCommentChat {
    [self simulateCommentChatWithInterval:3.0f];
}

- (void)simulateCommentChatWithInterval:(float) waitTime {
    if (!_simulationStarted)
        return;
    [Utils dispatchOnMainThread:^{
        NSTimeInterval interval = _currentCommentIndex == 0 ? waitTime : ((float) rand() / RAND_MAX) * 3 + 1;
        
        if (self.comments.count < 1)
        {
            _simulationStarted = NO;
            return;
        }
        
        [self showCommentWithIndex:_currentCommentIndex];
        
        _currentCommentIndex++;
        [self.simulationTimer invalidate];
        
        if (_currentCommentIndex < [self.comments count])
            self.simulationTimer = [NSTimer scheduledTimerWithTimeInterval:interval
                                                                    target:self
                                                                  selector:@selector(simulateCommentChat)
                                                                  userInfo:nil
                                                                   repeats:NO];
        else
            [self stopSimulatedCommentChat];
    }];
}

- (void)resumeSimulatedCommentChat {
    if (!_simulationPaused || !_simulationStarted) {
        return;
    }
    _simulationPaused = NO;
    [self simulateCommentChatStartImmediately];
}

- (void)pauseSimulatedCommentChat {
    if (_simulationPaused || !_simulationStarted) {
        return;
    }
    _simulationPaused = YES;
    [self.simulationTimer invalidate];
}

- (void)stopSimulatedCommentChat {
//    if ([self.delegate respondsToSelector:@selector(cardViewWillStopChatAnimation:)])
//        [self.delegate cardViewWillStopChatAnimation:self];
    
    _simulationStarted = NO;
    [self.simulationTimer invalidate];
    
//    if ([self.delegate respondsToSelector:@selector(cardViewDidStopChatAnimation:)])
//        [self.delegate cardViewDidStopChatAnimation:self];
}

- (void)startSimulatedCommentChat {
//    if ([self.delegate respondsToSelector:@selector(cardViewWillStartChatAnimation:)])
//        [self.delegate cardViewWillStartChatAnimation:self];
    
    [Utils dispatchAfter:3.0 closure:^{
        if (!_simulationStarted) {
            _currentCommentIndex = 0;
            _simulationStarted = YES;
            [self simulateCommentChat];
            
//            if ([self.delegate respondsToSelector:@selector(cardViewDidStartChatAnimation:)])
//                [self.delegate cardViewDidStartChatAnimation:self];
        }
    }];
}

- (void)dealloc {
    self.chatTableView.dataSource = nil;
    self.chatTableView.delegate = nil;
    self.chatTableView = nil;
}

@end
