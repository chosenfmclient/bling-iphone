//
// Created by Ben on 27/09/2016.
//

#include "edgeDetector.h"
#include <pthread.h>

//#include <android/log.h>
#include "globals.h"


edgeDetector::edgeDetector()
{
    pointOriginal = false;

    countTrue = 0; countFalse = 0;

    defBlurSize = Size(kernel_size, kernel_size);
}

edgeDetector::~edgeDetector()
{

}

void edgeDetector::getEdgesBuffer(Mat bg, Mat dss, Mat dst, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom)
{
    Mat mask(dss.size(), 0);

    cvtColor(dss, mask, COLOR_RGBA2GRAY);

    blur(mask, mask, defBlurSize);
    blur(mask, mask, defBlurSize);

    Mat hsv(dss.size(), 16);
    cvtColor(dss, hsv, COLOR_BGR2HSV);

//    Mat benMask(dss.size(), 0);
//    findBensEdges(hsv.data, benMask.data, hsv.data);

    Canny(mask, mask, lowThreshold, max_lowThreshold, kernel_size);
//    findBensEdges(hsv.data, mask.data, hsv.data);

//    __android_log_print(ANDROID_LOG_ERROR, "tag", "cameraRotation>> %d ", 3);
//
//    benMask.copyTo(stam);
//    return;
//    for (int k = 0, index2; k < HEIGHT; k++)
//    {
//        index2 = WIDTH * k;
//        for(int n = 0; n < WIDTH; n++, index2++)
//        {
//            if(benMask.data[index2] == 255)
//                mask.data[index2] = 255; // White pixel
//        }
//    }
//    int faceWidth = faceBottom - faceTop, faceHeight = faceLeft - faceRight;
    fillEdges(bg.data, dss.data, mask.data, hsv.data, dst.data, faceLeft, faceTop, faceRight, faceBottom);//, faceWidth, faceHeight); // top -> left , right -> top
    //    eUtil.fillEdges(bg.data, dss.data, mask.data, hsv.data, faceTop, faceRight, faceWidth, faceHeight); // top -> left , right -> top

}

//void edgeDetector::getEdgesBuffer(Mat dss, Mat dst, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom)
//{
//    Mat mask(dss.size(), 0);
//
//    cvtColor(dss, mask, COLOR_RGBA2GRAY);
//
//    blur(mask, mask, defBlurSize);
//    blur(mask, mask, defBlurSize);
//
//    Mat hsv(dss.size(), 16);
//    cvtColor(dss, hsv, COLOR_BGR2HSV);
//
//    Mat benMask(dss.size(), 0);
//    findBensEdges(hsv.data, benMask.data, hsv.data);
//
//    Canny(mask, mask, lowThreshold, max_lowThreshold, kernel_size);
//
//
////    for (int k = 0, index2; k < HEIGHT; k++)
////    {
////        index2 = WIDTH * k;
////        for(int n = 0; n < WIDTH; n++, index2++)
////        {
////            if(benMask.data[index2] == 255)
////                mask.data[index2] = 255; // White pixel
////        }
////    }
//    int faceWidth = faceBottom - faceTop, faceHeight = faceLeft - faceRight;
//    fillEdges(bg.data, dss.data, mask.data, hsv.data, dst.data, faceTop, faceRight, faceWidth, faceHeight); // top -> left , right -> top
//}

void edgeDetector::fillEdges(uchar *bg, uchar *dss, uchar *mask, uchar *hsv, uchar *edgesBuffer, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom)//, unsigned int faceWidth, unsigned int faceHeight)//, int column, int row, int cameFromColumn, int cameFromRow)
{
    unsigned int topRow = 0, topColumn = 0;
    int lastLeftRow = 0, lastRightRow = 0;

    if(faceLeft > 0 && faceLeft < HEIGHT)
        getTopOfHead(bg, mask, hsv, faceLeft, faceTop, &topColumn, &topRow);
    else
        getFirstObject(bg, mask, &topColumn, &topRow);

    try
    {
        if(topColumn > WIDTH)   topColumn = WIDTH;
        else if( topColumn < 0) topColumn = 0;

        if(topRow > HEIGHT) topRow = HEIGHT;
        else if(topRow < 0) topRow = 0;

        fillShape(bg, dss, mask, hsv, edgesBuffer, topColumn, topRow, faceBottom, &lastLeftRow, &lastRightRow);
    }
    catch (std::exception e)
    {
    }
}

void edgeDetector::getTopOfHead(uchar* bg, uchar* mask, uchar *hsv, unsigned int faceLeft, unsigned int faceTop, unsigned int *topColumn, unsigned int *topRow)//, int*startCol, int*startRow)
{
    int index2, index;
    int c, r;
    c = faceTop;
    faceLeft -= 10;
    index = (faceLeft * WIDTH * 4) + (c * 4);
    index2 = (faceLeft * WIDTH) + c;

    for (r = faceLeft; r < HEIGHT - 2; r++, index2 += WIDTH, index += WIDTH_X_4)
    {
        if(mask[index2] == WHITE_PIXEL)
            break;
        if(mask[index2 + 1] == WHITE_PIXEL)// above one pixel
        {
            c++;
            break;
        }
        if(mask[index2 - 1] == WHITE_PIXEL)// under one pixel
        {
            c--;
            break;
        }
        if(mask[index2 + 2] == WHITE_PIXEL)// above two pixel
        {
            c+=2;
            break;
        }
        if(mask[index2 - 2] == WHITE_PIXEL)// under two pixel
        {
            c-=2;
            break;
        }

        if(drawDetectionGuilds)
        {
            bg[index] = 0;
            bg[index + 1] = 255;
            bg[index + 2] = 0;
        }

    }
    getToTop(bg, mask, hsv, c, r, true, topColumn, topRow);
}

void edgeDetector::getFirstObject(uchar* bg, uchar* mask, unsigned int *topColumn, unsigned int *topRow)
{
    bool foundObject(false);
    int index2;

    for (int c = WIDTH - 2, r; c > 2; c--) // y axis
    {
        index2 = c - 1;
        for (r = 1; r < HEIGHT; r++, index2 += WIDTH)// x axis
        {
            if (mask[index2] == WHITE_PIXEL)
            {
                foundObject = true;
                break;
            }
        }

        if (foundObject)
        {
            *topRow = r;
            *topColumn = c;
            break;
        }
    }
}

//void helper::getToTop(uchar* bg, uchar* mask, unsigned int column, unsigned int row, bool isGoingRight, unsigned int *topColumn, unsigned int *topRow)
//{
//    int index2, index, doubleWidth = WIDTH + WIDTH;
//
//    int nextIndex, inner = 0;
//    bool notFound(false);
//
//    if(isGoingRight)
//    {
//        while (3 < column && column < WIDTH - 3)
//        {
//            column++;
//            index2 = (WIDTH * row) + column - 1;
//
//            //////////Testing//////////
//            index = (column * 4);
//            for (int i = 0; i < row && index < MAX_SIZE - 3; i++, index += WIDTH_X_4)
//            {
//                bg[index] = 255;
//                bg[index + 1] = 0;
//                bg[index + 2] = 0;
//            }
//            //////////End Testing//////////
//
//
//            if (mask[index2] == WHITE_PIXEL) // the next connecting pixel is on the same line as the last found pixel
//                inner = 0;           // nothing to do here
//            else if (index2 + WIDTH - 1 < MAX_SIZE &&
//                     mask[index2 + WIDTH - 1] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 1;
//                column--;
//            }
//            else if (index2 + WIDTH < MAX_SIZE && mask[index2 + WIDTH] == WHITE_PIXEL) // next pixel
//            {
//                inner = 1;
//            }
//            else if (index2 + doubleWidth < MAX_SIZE &&
//                     mask[index2 + doubleWidth] == WHITE_PIXEL) // next 2 pixels
//            {
//                inner = 2;
//            }
//            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH] == WHITE_PIXEL) // ex pixel
//            {
//                inner = -1;
//            }
//            else if (index2 - doubleWidth > 0 &&
//                     mask[index2 - doubleWidth] == WHITE_PIXEL) // ex 2 pixels
//            {
//                inner = -2;
//            }
//
//            else if (index2 + doubleWidth - 1 < MAX_SIZE && mask[index2 + WIDTH + WIDTH - 1] ==
//                                                              WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 2;
//                column--;
//            }
//            else if (mask[index2 + 1] == WHITE_PIXEL) // upper column// 2 pixels
//            {
//                inner--;
//                column++;
//            }
//            else if (mask[index2 + WIDTH + 1] == WHITE_PIXEL) // upper row next column// 2 pixels
//            {
//                inner--;
//                column++;
//            }
//            else if (!notFound)// no pixel found near, going to the next column
//            {
//                column--;
//                column--;
//                break;
////            notFound = true; // todo: need to reset notFound back to false every time we find neighbour pixel
////            continue;
//            }
////        else break;// no pixel found near, 2nd run, breaking
//
//            if (inner < 0)
//            {
//                inner *= -1;
//                while (index2 - (WIDTH * inner) > 0 && mask[index2 - (WIDTH * inner)] == WHITE_PIXEL)
//                    inner++;
////          inner--;
//                row -= inner;
//            }
//            else if (inner > 0)
//            {
//                while (index2 + (WIDTH * inner) < MAX_SIZE &&
//                                   mask[index2 + (WIDTH * inner)] == WHITE_PIXEL)
//                    inner++;
////            inner--;
//                row += inner;
//            }
////        else
////        {
////            inner = 1;
////            while(nextIndex = index2 + (WIDTH * inner) < MAX_SIZE && mask[nextIndex] == WHITE_PIXEL)
////                inner++;
////            if(inner > 1)
////            {
////                row += (inner - 1);
////            }
////            else
////            {
////                while(nextIndex = index2 - (WIDTH * inner) > 0 && mask[nextIndex] == WHITE_PIXEL)
////                    inner++;
////                if(inner > 1)
////                {
////                    row -= (inner + 1);
////                }
////            }
////        }
//        }
//    }
//    else
//    {
//        while (3 < column && column < WIDTH - 3)
//        {
//            column++;
//            index2 = (WIDTH * row) + column - 1;
//
//            //////////Testing//////////
//            index = (column * 4);
//            for (int i = 0; i < row && index < MAX_SIZE - 3; i++, index += WIDTH_X_4)
//            {
//                bg[index] = 255;
//                bg[index + 1] = 0;
//                bg[index + 2] = 0;
//            }
//            //////////End Testing//////////
//
//
////        else break;// no pixel found near, 2nd run, breaking
//            if (mask[index2] == WHITE_PIXEL) // the next connecting pixel is on the same column as the last found pixel
//                inner = 0;           // nothing to do here
//            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH] == WHITE_PIXEL) // ex pixel
//            {
//                inner = -1;
//            }
//            else if (index2 - WIDTH + 1 > 0 &&
//                     mask[index2 - WIDTH + 1] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = -1;
//                column++;
//            }
//            else if (index2 - WIDTH + 2 > 0 &&
//                      mask[index2 - WIDTH + 2] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = -1;
//                column+=2;
//            }
//            else if (index2 - WIDTH - 1 > 0 &&
//                     mask[index2 - WIDTH - 1] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = -1;
//                column--;
//            }
//
//            else if (index2 + WIDTH > 0 &&
//                     mask[index2 + WIDTH] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 1;
//            }
//            else if (index2 + WIDTH + 1 > 0 &&
//                     mask[index2 + WIDTH + 1] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 1;
//                column++;
//            }
//            else if (index2 + WIDTH + 2 > 0 &&
//                     mask[index2 + WIDTH + 2] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 2;
//                column++;
//            }
//
//            else
//            {
//                column--;
//                break;
//            }
//
//
//
//
////            else if (index2 - doubleWidth > 0 &&
////                     mask[index2 - doubleWidth] == WHITE_PIXEL) // ex 2 pixels
////            {
////                inner = -2;
////            }
////
////
////            else if (index2 + WIDTH < MAX_SIZE && mask[index2 + WIDTH] == WHITE_PIXEL) // next pixel
////            {
////                inner = 1;
////            }
////            else if (index2 + WIDTH + WIDTH < MAX_SIZE &&
////                     mask[index2 + WIDTH + WIDTH] == WHITE_PIXEL) // next 2 pixels
////            {
////                inner = 2;
////            }
////            else if (index2 + WIDTH - 1 < MAX_SIZE &&
////                     mask[index2 + WIDTH - 1] == WHITE_PIXEL) // below row next column// 2 pixels
////            {
////                inner = 1;
////                column--;
////            }
////            else if (index2 + WIDTH + WIDTH - 1 < MAX_SIZE && mask[index2 + WIDTH + WIDTH - 1] ==
////                                                              WHITE_PIXEL) // below row next column// 2 pixels
////            {
////                inner = 2;
////                column--;
////            }
////            else if (mask[index2 + 1] == WHITE_PIXEL) // upper column// 2 pixels
////            {
////                inner--;
////                column++;
////            }
////            else if (mask[index2 + WIDTH + 1] == WHITE_PIXEL) // upper row next column// 2 pixels
////            {
////                inner--;
////                column++;
////            }
////            else if (!notFound)// no pixel found near, going to the next column
////            {
////                column--;
////                column--;
////                break;
//////            notFound = true; // todo: need to reset notFound back to false every time we find neighbour pixel
//////            continue;
////            }
//
//            if (inner <= 0)
//            {
//                inner *= -1;
//                inner++;
//                while (index2 - (WIDTH * inner) > 0 && mask[index2 - (WIDTH * inner)] == WHITE_PIXEL)
//                    inner++;
//                inner--;
////          inner--;
//                row -= inner;
//            }
//            else if (inner > 0)
//            {
//                inner++;
//                while (index2 + (WIDTH * inner) < MAX_SIZE &&
//                                   mask[index2 + (WIDTH * inner)] == WHITE_PIXEL)
//                    inner++;
//                inner--;
//                row += inner;
//            }
////        else
////        {
////            inner = 1;
////            while(nextIndex = index2 + (WIDTH * inner) < MAX_SIZE && mask[nextIndex] == WHITE_PIXEL)
////                inner++;
////            if(inner > 1)
////            {
////                row += (inner - 1);
////            }
////            else
////            {
////                while(nextIndex = index2 - (WIDTH * inner) > 0 && mask[nextIndex] == WHITE_PIXEL)
////                    inner++;
////                if(inner > 1)
////                {
////                    row -= (inner + 1);
////                }
////            }
////        }
//        }
//    }
//
//
//
//    *topColumn = column;
//    *topRow = row;
//
//
//    /////////another test
//    index2 = (WIDTH * row) + column ;
////    index = (WIDTH * row * 4) + ((column - 3) * 4 );
////
//
////    int WIDTH = 640, HEIGHT = 480, index, index2;
//    for (int r = 0; r < HEIGHT; r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for (int c = 0; c < WIDTH; c++, index += 4, index2++)
//        {
////            if (mask.data[index2] == 255)
////            {
////                bg.data[index] = 255;
////                bg.data[index + 1] = 255;
////                bg.data[index + 2] = 255;
////            }
////            else
////            {
////                bg.data[index] = 0;
////                bg.data[index + 1] = 0;
////                bg.data[index + 2] = 0;
////            }
//
//            // we get the image rotated, and so the face detection is rotated as well
//
//            //  faceRight -> top
//            //  faceTop   -> left
//            //  faceLeft  -> bottom
//            //  faveBottom-> right
//
////            if(faceLeft!=0)
//            {
//                if (c == column)// && r >= faceTop && r <= faceBottom)
//                {
//                    bg[index] = 0;
//                    bg[index + 1] = 0;
//                    bg[index + 2] = 255;
//                }
//
//                if (r == row)// && c >= faceLeft && c <= faceRight)
//                {
//                    bg[index] = 0;
//                    bg[index + 1] = 0;
//                    bg[index + 2] = 255;
//                }
//
////                if (c == faceLeft && r >= faceTop && r <= faceBottom)
////                {
////                    bg.data[index] = 0;
////                    bg.data[index + 1] = 0;
////                    bg.data[index + 2] = 255;
////                }
////
////                if (r == faceBottom && c >= faceLeft && c <= faceRight)
////                {
////                    bg.data[index] = 0;
////                    bg.data[index + 1] = 0;
////                    bg.data[index + 2] = 255;
////                }
//            }
//        }
//    }
//    bg[index] = 0;
//    bg[index + 1] = 0;
//    bg[index + 2] = 255;
//
//    bg[index + 4] = 0;
//    bg[index + 1 + 4] = 0;
//    bg[index + 2 + 4 ] = 255;
//
//    bg[index - 4] = 0;
//    bg[index + 1 - 4] = 0;
//    bg[index + 2 - 4 ] = 255;
//
//}

//int h1, h2, h3, h4;
//int s1, s2, s3, s4;
//int v1, v2, v3, v4;
//int r1, c1;

void edgeDetector::getToTop(uchar* bg, uchar* mask, uchar* hsv, unsigned int column, unsigned int row, bool isGoingRight, unsigned int *topColumn, unsigned int *topRow)
{
    int index2, index, doubleWidth = WIDTH + WIDTH;

    int nextIndex, hsvIndex, inner = 0;
    bool notFound(false);
    int point1, point2;
//
    if(isGoingRight)
    {
       /////right side///////
        while (3 < column && column < WIDTH - 3)
        {
            column++;
            index2 = (WIDTH * row) + column;
            
            hsvIndex = (row * WIDTH_X_3) + (column * 3);

            if (index2 - doubleWidth > 0 && mask[index2  - doubleWidth + 2] == WHITE_PIXEL
//                && ( point1 = hsvIndex - (doubleWidth * 3) + 6 + DISTANCE_FROM_LINE_HALF_X_3 * (1 + WIDTH_X_3),
//                (point1 < MAX_SIZE_X_3)
//             && (point2 = hsvIndex - (doubleWidth * 3) + 6 - DISTANCE_FROM_LINE_HALF_X_3 * (1 + WIDTH_X_3),
//                 point2 > 0)
//             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                )
                    ) // ex pixel
            {
                inner = -2;
                column +=2;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH + 2] == WHITE_PIXEL
//                     && (point1 = hsvIndex - WIDTH_X_3 + 6 + DISTANCE_FROM_LINE_HALF_X_3 * (1 + WIDTH_X_3),
//                    point2 = hsvIndex - WIDTH_X_3 + 6 - DISTANCE_FROM_LINE_HALF_X_3 * (1 + WIDTH_X_3),
//                    (point1 < MAX_SIZE_X_3)
//                 && (point2 > 0)
//                 && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//                     )
                    ) // ex pixel
            {
                inner = -1;
                column += 2;
                index2 = (WIDTH * row) + column;
            }
            
            if (index2 - doubleWidth> 0 && mask[index2  - doubleWidth + 1] == WHITE_PIXEL
//                && (
//                point1 = hsvIndex - (doubleWidth * 3) + 3 + DISTANCE_FROM_LINE_HALF_X_3 * (1 + WIDTH_X_3),
//                point2 = hsvIndex - (doubleWidth * 3) + 3 - DISTANCE_FROM_LINE_HALF_X_3 * (1 + WIDTH_X_3),
//                (point1 < MAX_SIZE_X_3)
//             && (point2 > 0)
//             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                )
                    ) // ex pixel
            {
                inner = -2;
                column++;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH + 1] == WHITE_PIXEL
//                     && (
//                    point1 = hsvIndex - WIDTH_X_3 + 3 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    point2 = hsvIndex - WIDTH_X_3 + 3 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    (point1 < MAX_SIZE_X_3)
//                 && (point2 > 0)
//                 && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                     )
                    ) // ex pixel
            {
                inner = -1;
                column++;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 - doubleWidth > 0 && mask[index2 - doubleWidth] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex - (doubleWidth * 3) + DISTANCE_FROM_LINE_HALF_X_3,
//                      point2 = hsvIndex - (doubleWidth * 3) - DISTANCE_FROM_LINE_HALF_X_3,
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                     )
                    ) // ex 2 pixels
            {
                inner = -2;
            }
            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH] == WHITE_PIXEL
//                     && (
//                     point1 = hsvIndex - WIDTH_X_3 + DISTANCE_FROM_LINE_HALF_X_3,
//                     point2 = hsvIndex - WIDTH_X_3 - DISTANCE_FROM_LINE_HALF_X_3,
//                    (point1 < MAX_SIZE_X_3)
//                 && (point2 > 0)
//                 && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//                     )
                    ) // ex pixel
            {
                inner = -1;
            }
            else if (mask[index2 + 2] == WHITE_PIXEL
//                     && (
//                    point1 = hsvIndex + 6 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    point2 = hsvIndex + 6 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    (point1 < MAX_SIZE_X_3)
//                 && (point2 > 0)
//                 && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                     )
                    ) // upper column// 2 pixels
            {
                inner = 0;
                column += 2;
                index2 = (WIDTH * row) + column;
            }
            else if (mask[index2 + 1] == WHITE_PIXEL
//                     && (
//                    point1 = hsvIndex + 3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    point2 = hsvIndex + 3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                     )
                    ) // upper column// 2 pixels
            {
                inner = 0;
                column++;
                index2 = (WIDTH * row) + column;
            }
            else if (mask[index2] == WHITE_PIXEL
//                     && (
//                     point1 = hsvIndex + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     point2 = hsvIndex - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                        (point1 < MAX_SIZE_X_3)
//                     && (point2 > 0)
//                     && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//                     )
                    ) // the next connecting pixel is on the same line as the last found pixel
            {
                inner = 0;           // nothing to do here

//                h1 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2)];
//                h2 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3)];
//                h3 = hsv[(WIDTH_X_3 * row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3)];
//                h4 = hsv[(WIDTH_X_3 * row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2)];
//
//                s1 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2) + 1];
//                s2 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3) + 1];
//                s3 = hsv[(WIDTH_X_3 * row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3) + 1];
//                s4 = hsv[(WIDTH_X_3 * row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2) + 1];
//
//                v1 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2) + 2];
//                v2 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3) + 2];
//                v3 = hsv[(WIDTH_X_3 * row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3) + 2];
//                v4 = hsv[(WIDTH_X_3 * row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2) + 2];

//                __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>a>> %d   %d   %d ", h1, s1, v1);
//                __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>b>> %d   %d   %d ", h2, s2, v2);
//                __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>c>> %d   %d   %d ", h3, s3, v3);
//                __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>d>> %d   %d   %d ", h4, s4, v4);
            }
            else if (index2 + WIDTH < MAX_SIZE && mask[index2 + WIDTH] == WHITE_PIXEL
//                     && (
//                        point1 = hsvIndex + WIDTH_X_3 + DISTANCE_FROM_LINE_HALF_X_3,
//                        point2 = hsvIndex + WIDTH_X_3 - DISTANCE_FROM_LINE_HALF_X_3,
//                        (point1  < MAX_SIZE_X_3)
////                     && (point2  > 0)
//                     && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                     )
                    ) // upper row next column// 2 pixels
            {
                inner = 1;
            }
            else if (index2 + doubleWidth < MAX_SIZE && mask[index2 + WIDTH + 1] == WHITE_PIXEL
//                     && (
//                     point1 = hsvIndex + WIDTH_X_3 + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     point2 = hsvIndex + WIDTH_X_3 + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      (point1 < MAX_SIZE_X_3)
//                   && (point2 > 0)
//                   && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//                     )
                    ) // upper row next column// 2 pixels
            {
                inner = 1;
                column++;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 + WIDTH < MAX_SIZE && mask[index2 + WIDTH + 2] == WHITE_PIXEL
//                     && (point1 = hsvIndex + WIDTH_X_3 + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    point2 = hsvIndex + WIDTH_X_3 + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1  < MAX_SIZE_X_3)
//                  && (point2  > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                                                                          )
                    ) // upper row next column// 2 pixels
            {
                inner = 1;
                column +=2;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 + doubleWidth < MAX_SIZE && mask[index2 + doubleWidth] == WHITE_PIXEL
//                     && (
//                        point1 = hsvIndex + (doubleWidth * 3) + DISTANCE_FROM_LINE_HALF_X_3,
//                        point2 = hsvIndex + (doubleWidth * 3) - DISTANCE_FROM_LINE_HALF_X_3,
//                       (point1 < MAX_SIZE_X_3)
//                    && (point2 > 0 )
//                    && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                     )
                    ) // upper row next column// 2 pixels
            {
                inner = 2;
            }
            else if (index2 + doubleWidth < MAX_SIZE && mask[index2 + doubleWidth + 1] == WHITE_PIXEL
//                     && (
//                    point1 = hsvIndex + (doubleWidth * 3) + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    point2 = hsvIndex + (doubleWidth * 3) + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                     )
                    ) // upper row next column// 2 pixels
            {
                inner = 2;
                column++;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 + doubleWidth < MAX_SIZE && mask[index2 + doubleWidth + 2] == WHITE_PIXEL
//                     && (
//                    point1 = hsvIndex + (doubleWidth * 3) + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    point2 = hsvIndex + (doubleWidth * 3) + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                     )
                    ) // upper row next column// 2 pixels
            {
                inner = 2;
                column +=2;
                index2 = (WIDTH * row) + column;
            }
           else if (index2 + WIDTH - 1 < MAX_SIZE && mask[index2 + WIDTH - 1] == WHITE_PIXEL
//                   && (
//                    point1 = hsvIndex + WIDTH_X_3 - 3 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    point2 = hsvIndex + WIDTH_X_3 - 3 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                    )
                    ) // below row next column// 2 pixels
            {
                inner = 1;
                column--;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 + doubleWidth - 1 < MAX_SIZE && mask[index2 + WIDTH + WIDTH - 1] == WHITE_PIXEL
//                     && (
//                    point1 = hsvIndex + (doubleWidth * 3) - 3 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    point2 = hsvIndex + (doubleWidth * 3) - 3 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                     )
                    ) // below row next column// 2 pixels
            {
                inner = 2;
                column--;
                index2 = (WIDTH * row) + column;
            }
            else// if (!notFound)// no pixel found near, going to the next column
            {
                column--;
                break;
//            notFound = true; // todo: need to reset notFound back to false every time we find neighbour pixel
//            continue;
            }
//        else break;// no pixel found near, 2nd run, breaking

            if (inner < 0 || inner == 0)
            {
                inner *= -1;
                inner++;
                while (index2 - (WIDTH * inner) > 0 && mask[index2 - (WIDTH * inner)] == WHITE_PIXEL
//                       && (point1 = hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * inner),
//                        point2 = hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * inner),
//                       (point2 > 0)
////                       (point1 < MAX_SIZE_X_3)
////                    && (point2 > 0)
//                    && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                       )
                        )
                    inner++;
                inner--;
                row -= inner;
            }
            else if (inner > 0)
            {
                inner++;
                while (index2 + (WIDTH * inner) < MAX_SIZE && mask[index2 + (WIDTH * inner)] == WHITE_PIXEL
//                       && (point1 = hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * inner),
//                        point2 = hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * inner),
//                       (point1 < MAX_SIZE_X_3)
////                    && (point2 > 0)
//                    && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                       )
                        )
                    inner++;
                inner--;
                row += inner;


//                inner++;
//                int inner2 = 0;
//                while (index2 + (WIDTH * inner) < MAX_SIZE &&
//                       mask[index2 + (WIDTH * inner)] == WHITE_PIXEL && (
//                               point1 = hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * inner),
//                                       point2 = hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * inner),
//                                       (point1 < MAX_SIZE_X_3)
//                                       && (point2 > 0)
//                                       && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                       ))
//                    inner++;
//                inner--;



            }
        }
    }
    ////////////left side/////////
    
    else  //isGoingLeft//
    {
        while (3 < column && column < WIDTH - 3)
        {
            column++;
            index2 = (WIDTH * row) + column;
            
            hsvIndex = (row * WIDTH_X_3) + (column * 3);
            
            //            //////////Testing//////////
            //            index = (column * 4);
            //            for (int i = 0; i < row && index < MAX_SIZE - 3; i++, index += WIDTH_X_4)
            //            {
            //                bg[index] = 255;
            //                bg[index + 1] = 0;
            //                bg[index + 2] = 0;
            //            }
            //            //////////End Testing//////////
            
            if (index2 + doubleWidth < MAX_SIZE && mask[index2 + doubleWidth + 2] == WHITE_PIXEL
//                &&
//                (
//                 point1 = hsvIndex + (doubleWidth * 3) + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                 point2 = hsvIndex + (doubleWidth * 3) + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    (point1 < MAX_SIZE_X_3)
//                 && (point2 > 0)
//                 && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//               )
                    ) // ex pixel
            {
                inner = -2;
                column +=2;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 + WIDTH < MAX_SIZE && mask[index2 + WIDTH + 2] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex + WIDTH_X_3 + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      point2 = hsvIndex + WIDTH_X_3 + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                )
                    ) // ex pixel
            {
                inner = -1;
                column += 2;
                index2 = (WIDTH * row) + column;
            }
            if (index2 + doubleWidth < MAX_SIZE && mask[index2  + doubleWidth + 1] == WHITE_PIXEL
//                &&
//                (
//                 point1 = hsvIndex + (doubleWidth * 3) + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                 point2 = hsvIndex + (doubleWidth * 3) + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                (point1 < MAX_SIZE_X_3)
//             && (point2 > 0)
//             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//              )
                    ) // ex pixel
            {
                inner = -2;
                column++;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 + WIDTH < MAX_SIZE && mask[index2 + WIDTH + 1] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex + WIDTH_X_3 + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      point2 = hsvIndex + WIDTH_X_3 + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//              )
                    ) // ex pixel
            {
                inner = -1;
                column++;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 + doubleWidth < MAX_SIZE && mask[index2 + doubleWidth] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex + (doubleWidth * 3) + DISTANCE_FROM_LINE_HALF_X_3,
//                      point2 = hsvIndex + (doubleWidth * 3) - DISTANCE_FROM_LINE_HALF_X_3,
////                     (point1 < MAX_SIZE_X_3)
////                  && (point2 > 0)
////                  &&
//                    isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//              )
                    ) // ex 2 pixels
            {
                inner = -2;
            }
            else if (index2 + WIDTH < MAX_SIZE && mask[index2 + WIDTH] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex + WIDTH_X_3 + DISTANCE_FROM_LINE_HALF_X_3,
//                      point2 = hsvIndex + WIDTH_X_3 - DISTANCE_FROM_LINE_HALF_X_3,
////                     (point1 < MAX_SIZE_X_3)
////                  && (point2 > 0)&&
//                        isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//             )
                    ) // ex pixel
            {
                inner = -1;
            }
            else if (mask[index2 + 2] == WHITE_PIXEL // index is safe for oob
//                     &&
//                     (
//                      point1 = hsvIndex + 6 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      point2 = hsvIndex + 6 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//            )
                    ) // upper column// 2 pixels
            {
                inner = 0;
                column += 2;
                index2 = (WIDTH * row) + column;
            }
            else if (mask[index2 + 1] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex + 3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      point2 = hsvIndex + 3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                    (point1 < MAX_SIZE_X_3)
//                 && (point2 > 0)
//                 && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//            )
                    ) // upper column// 2 pixels
            {
                inner = 0;
                column++;
                index2 = (WIDTH * row) + column;
            }
            else if (mask[index2] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      point2 = hsvIndex - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      (point1 < MAX_SIZE_X_3)
//                   && (point2 > 0)
//                   && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//             )
                    ) // the next connecting pixel is on the same line as the last found pixel
            {
                inner = 0;           // nothing to do here
                
//                h1 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2)];
//                h2 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3)];
//                h3 = hsv[(WIDTH_X_3 * row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3)];
//                h4 = hsv[(WIDTH_X_3 * row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2)];
//                
//                s1 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2) + 1];
//                s2 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3) + 1];
//                s3 = hsv[(WIDTH_X_3 *  row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3) + 1];
//                s4 = hsv[(WIDTH_X_3 * row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2) + 1];
//                
//                v1 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2) + 2];
//                v2 = hsv[(WIDTH_X_3 * row) + (column * 3) - (DISTANCE_FROM_LINE_HALF * WIDTH_X_3) + 2];
//                v3 = hsv[(WIDTH_X_3 * row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3) + 2];
//                v4 = hsv[(WIDTH_X_3 * row) + (column * 3) + (DISTANCE_FROM_LINE_HALF * WIDTH_X_3 * 2) + 2];
//                r1 = row;
//                c1 = column;
                
                //                __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>a>> %d   %d   %d ", h1, s1, v1);
                //                __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>b>> %d   %d   %d ", h2, s2, v2);
                //                __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>c>> %d   %d   %d ", h3, s3, v3);
                //                __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>d>> %d   %d   %d ", h4, s4, v4);
            }
            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex - WIDTH + DISTANCE_FROM_LINE_HALF,
//                      point2 = hsvIndex - WIDTH - DISTANCE_FROM_LINE_HALF,
////                     (point1 < MAX_SIZE_X_3)
////                  && (point2 > 0)
////                  &&
//                    isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//             )
                    ) // upper row next column// 2 pixels
            {
                inner = 1;
            }
            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH + 1] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex - WIDTH_X_3 + 3 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      point2 = hsvIndex - WIDTH_X_3 + 3 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//             )
                    ) // upper row next column// 2 pixels
            {
                inner = 1;
                column++;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH + 2] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex - WIDTH_X_3 + 6 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      point2 = hsvIndex - WIDTH_X_3 + 6 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//            )
                    ) // upper row next column// 2 pixels
            {
                inner = 1;
                column +=2;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 - doubleWidth > 0 && mask[index2 - doubleWidth] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex - (doubleWidth * 3) + DISTANCE_FROM_LINE_HALF_X_3,
//                      point2 = hsvIndex - (doubleWidth * 3) - DISTANCE_FROM_LINE_HALF_X_3,
////                    (point1 < MAX_SIZE_X_3)
////                 &&
//                    (point2 > 0)
//                 && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//             )
                    ) // upper row next column// 2 pixels
            {
                inner = 2;
            }
            else if (index2 - doubleWidth > 0 && mask[index2 - doubleWidth + 1] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex - (doubleWidth * 3) + 3 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      point2 = hsvIndex - (doubleWidth * 3) + 3 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//             )
                    ) // upper row next column// 2 pixels
            {
                inner = 2;
                column++;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 - doubleWidth > 0 && mask[index2 - doubleWidth + 2] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex - (doubleWidth * 3) + 6 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      point2 = hsvIndex - (doubleWidth * 3) + 6 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//             )
                    ) // upper row next column// 2 pixels
            {
                inner = 2;
                column +=2;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 - WIDTH > 0 && index2 - WIDTH - 1 < MAX_SIZE && mask[index2 - WIDTH - 1] == WHITE_PIXEL
                     //&&
//                     (
//                      point1 = hsvIndex - WIDTH_X_3 - 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      point2 = hsvIndex - WIDTH_X_3 - 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//             )
                    ) // below row next column// 2 pixels
            {
                inner = 1;
                column--;
                index2 = (WIDTH * row) + column;
            }
            else if (index2 - doubleWidth - 1 < MAX_SIZE && mask[index2 - doubleWidth - 1] == WHITE_PIXEL
//                     &&
//                     (
//                      point1 = hsvIndex - (doubleWidth * 3) - 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                      point2 = hsvIndex - (doubleWidth * 3) - 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                     (point1 < MAX_SIZE_X_3)
//                  && (point2 > 0)
//                  && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//             )
                    ) // below row next column// 2 pixels
            {
                inner = 2;
                column--;
                index2 = (WIDTH * row) + column;
            }
            else// if (!notFound)// no pixel found near, going to the next column
            {
                column--;
                break;
                //            notFound = true; // todo: need to reset notFound back to false every time we find neighbour pixel
                //            continue;
            }
            //        else break;// no pixel found near, 2nd run, breaking
            
            if (inner < 0 || inner == 0)
            {
                inner *= -1;
                inner++;
                while (index2 + (WIDTH * inner) < MAX_SIZE && mask[index2 + (WIDTH * inner)] == WHITE_PIXEL
//                       &&
//                       (
//                        point1 = hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * inner),
//                        point2 =  hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * inner),
//                       (point1 < MAX_SIZE_X_3)
////                    && (point2 > 0)
//                    && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                    )
                        )
                    inner++;
                inner--;
                row += inner;
            }
            else if (inner > 0)
            {
                inner++;
                while (index2 - (WIDTH * inner) > 0 && mask[index2 - (WIDTH * inner)] == WHITE_PIXEL
//                       &&
//                       (
//                        point1 = hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * inner),
//                        point2 = hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * inner),
////                       (point1 < MAX_SIZE_X_3) &&
//                        (point2 > 0)
//                    && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                       )
                        )
                    inner++;
                inner--;
                row -= inner;
            }
        }
    }


    *topColumn = column;
    *topRow = row;

    if(false)
    {
        /////////another test
        index2 = (WIDTH * row) + column;
//    index = (WIDTH * row * 4) + ((column - 3) * 4 );
//

//    int WIDTH = 640, HEIGHT = 480, index, index2;
        for (int r = 0; r < HEIGHT; r++)
        {
            index = WIDTH * r * 4;
            index2 = WIDTH * r;
            for (int c = 0; c < WIDTH; c++, index += 4, index2++)
            {
//            if (mask.data[index2] == 255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//            else
//            {
//                bg.data[index] = 0;
//                bg.data[index + 1] = 0;
//                bg.data[index + 2] = 0;
//            }

                // we get the image rotated, and so the face detection is rotated as well

                //  faceRight -> top
                //  faceTop   -> left
                //  faceLeft  -> bottom
                //  faveBottom-> right

//            if(faceLeft!=0)
                {
                    if (c == column)// && r >= faceTop && r <= faceBottom)
                    {
                        bg[index] = 0;
                        bg[index + 1] = 0;
                        bg[index + 2] = 255;
                    }

                    if (r == row)// && c >= faceLeft && c <= faceRight)
                    {
                        bg[index] = 0;
                        bg[index + 1] = 0;
                        bg[index + 2] = 255;
                    }

//                if(r == r1)
//                {
//                    bg[index] = 255;
//                    bg[index + 1] = 255;
//                    bg[index + 2] = 0;
//                }
//
//                if (c1 == c)// && r >= faceTop && r <= faceBottom)
//                {
//                    bg[index] = 255;
//                    bg[index + 1] = 255;
//                    bg[index + 2] = 0;
//                }

//                if (c == faceLeft && r >= faceTop && r <= faceBottom)
//                {
//                    bg.data[index] = 0;
//                    bg.data[index + 1] = 0;
//                    bg.data[index + 2] = 255;
//                }
//
//                if (r == faceBottom && c >= faceLeft && c <= faceRight)
//                {
//                    bg.data[index] = 0;
//                    bg.data[index + 1] = 0;
//                    bg.data[index + 2] = 255;
//                }
                }
            }
        }
    }
}

void edgeDetector::fillShape(uchar* bg, uchar* dss, uchar* mask, uchar* hsv, uchar* edgesBuffer, unsigned int topCol, unsigned int topRow, unsigned int lastCol
        , int *lastLeftRow, int *lastRightRow)//, unsigned int faceBottomCol)
{
    int indexLeft, indexRight,
        columnLeft = topCol, columnRight = topCol,
        leftSideRow = topRow, rightSideRow = topRow,
        inner, index, nextIndex, hsvIndex;

    int lastLeftLoopRow = topRow, lastLeftLoopCol = topCol;
    int lastRightLoopRow, lastRightLoopCol;

    bool foundEdge;
    int distance = 0, c = topCol;
    bool isLeftGoingLeft(false), isRightGoingRight(false);

    bool isLeftGoingUp(false);

    unsigned int resTopCol, resTopRow;
    int point1, point2;

    indexLeft = (leftSideRow * WIDTH) + columnLeft;
    hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);
    
    inner = 1;
    while( (indexLeft - (WIDTH * inner) > 0 && mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL
//            && (
//            point1 = hsvIndex - (WIDTH_X_3 * inner) - DISTANCE_FROM_LINE_HALF_X_3,
//            point2 = hsvIndex - (WIDTH_X_3 * inner) + DISTANCE_FROM_LINE_HALF_X_3,
//           (point1 > 0)
////        && (point2 < MAX_SIZE_X_3)
//        && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//            )
           )
           ||
           ( indexLeft - (WIDTH * inner) - 1 > 0 && mask[indexLeft - (WIDTH * inner) - 1] == WHITE_PIXEL
//             &&(
//             point1 = hsvIndex - 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * (inner + DISTANCE_FROM_LINE_HALF)),
//             point2 = hsvIndex - 3 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * (inner - DISTANCE_FROM_LINE_HALF)),
//            (point1 > 0)
//         && (point2 < MAX_SIZE_X_3)
//         && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//            )
           )
           ||
           ( indexLeft - (WIDTH * inner) > 0 && mask[indexLeft - (WIDTH * inner) + 1] == WHITE_PIXEL
//             && (
//             point1 = hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * (inner - DISTANCE_FROM_LINE_HALF)),
//             point2 = hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * (inner + DISTANCE_FROM_LINE_HALF)),
//            (point1 < MAX_SIZE_X_3)
//         && (point2 > 0)
//         && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//            )
           )
//           ||
//           ( indexLeft - (WIDTH * inner) - 2 > 0
//             && mask[indexLeft - (WIDTH * inner) - 2] == WHITE_PIXEL )
//           ||
//           ( indexLeft - (WIDTH * inner) + 2 > 0
//             && mask[indexLeft - (WIDTH * inner) + 2] == WHITE_PIXEL )

            )
    {
        inner++;
    }
    inner--;
    leftSideRow -= inner;

    indexRight = (rightSideRow * WIDTH) + columnRight;
    hsvIndex = (rightSideRow * WIDTH_X_3) + (columnRight * 3);
    
    inner = 1;
    while(( indexRight + (WIDTH * inner) < MAX_SIZE && mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL
//            && (
//            point1 = hsvIndex + (WIDTH_X_3 * inner) + DISTANCE_FROM_LINE_HALF_X_3,
//            point2 = hsvIndex + (WIDTH_X_3 * inner) - DISTANCE_FROM_LINE_HALF_X_3 ,
//           (point1 < MAX_SIZE_X_3)
////        && (point2 > 0)
//        && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//           )
          )
          ||
          (indexRight + (WIDTH * inner) < MAX_SIZE && mask[indexRight + (WIDTH * inner) - 1] == WHITE_PIXEL
//           && (
//            point1 = hsvIndex - 3 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * (inner + DISTANCE_FROM_LINE_HALF)),
//            point2 = hsvIndex - 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * (inner - DISTANCE_FROM_LINE_HALF)),
//           (point1 < MAX_SIZE_X_3)
//        && (point2 > 0)
//        && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//           )
          )
          ||
          (indexRight + (WIDTH * inner) + 1 < MAX_SIZE && mask[indexRight + (WIDTH * inner) + 1] == WHITE_PIXEL
//           && (
//            point1 = hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * (inner + DISTANCE_FROM_LINE_HALF)),
//            point2 = hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * (inner - DISTANCE_FROM_LINE_HALF)),
//           (point1 < MAX_SIZE_X_3)
//        && (point2 > 0)
//        && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                                                                        )
          )
//
//          || (indexRight + (WIDTH * inner) - 2 < MAX_SIZE &&
//              mask[indexRight + (WIDTH * inner) - 2] == WHITE_PIXEL )

//               || (indexRight + (WIDTH * inner) + 2 < MAX_SIZE &&
//                   mask[indexRight + (WIDTH * inner) + 2] == WHITE_PIXEL )
            )
    {
        inner++;
    }
    inner--;
      rightSideRow += inner;

    lastLeftLoopCol = columnLeft;
    lastLeftLoopRow = leftSideRow;

    lastRightLoopCol = columnRight;
    lastRightLoopRow = rightSideRow;

    if(c >= WIDTH - 3)
        return;

    pointOriginal = true;

//    return;
    while (c > 3 && c != lastCol)
    {
        ///////////////////left side/////////////////////

        indexLeft = (leftSideRow * WIDTH) + columnLeft;
        leftSideRow = findLeftSideRow(bg, dss, mask, hsv, edgesBuffer, columnLeft, leftSideRow);

        ///////////////////Right side III/////////////////////

        indexRight = (rightSideRow * WIDTH) + columnRight;
        rightSideRow = findRightSideRow(bg, dss, mask, hsv, edgesBuffer, columnRight, rightSideRow);

        ///////////////////process edges/////////////////////

        if (leftSideRow < rightSideRow)
        {
            *lastLeftRow = leftSideRow;
            *lastRightRow = rightSideRow;

            index = ((leftSideRow * WIDTH) + c) * 4;
            int indexEdges = (leftSideRow * WIDTH) + c;
            
            for (inner = leftSideRow; inner < rightSideRow; inner++, index += WIDTH_X_4, indexEdges+=WIDTH)
            {
                  edgesBuffer[indexEdges] = 255;

//                bg[index] = dss[index];
//                bg[index + 1] = dss[index + 1];
//                bg[index + 2] = dss[index + 2];
            }
        }

        lastLeftLoopCol = columnLeft;
        lastLeftLoopRow = leftSideRow;

        c--;
        columnLeft--;
        columnRight--;
    }
}

bool edgeDetector::isNotInRange(uchar hue1, uchar hue2, uchar sat1, uchar sat2, uchar val1, uchar val2)
{
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "%d called top> first> %d    %d   %d", count, (arg2 + HUE_THRESHOLD)
//            , arg2 - HUE_THRESHOLD
//            , (arg2 + HUE_THRESHOLD < arg1 || arg1 < arg2 - HUE_THRESHOLD));

    return true || (
            hue2 + HUE_THRESHOLD < hue1 || hue1 < hue2 - HUE_THRESHOLD ||
            ((hue2 - HUE_THRESHOLD < 0) &&
             (hue1 > 180 - HUE_THRESHOLD ) &&
             (hue1 - 180 < hue2 - HUE_THRESHOLD)
             )||
            sat2 + SATURATION_THRESHOLD < sat1 || sat1 < sat2 - SATURATION_THRESHOLD ||
            val2 + VALUE_THRESHOLD < val1 || val1 < val2 - VALUE_THRESHOLD
            );
}

int edgeDetector::goToSameRangeIndexLeft(uchar* mask, uchar* hsv, unsigned int column, unsigned int row)
{
    
    int currentIndex , lastIndex;
    int leftSideRow = row;
    bool foundEdge;
    int nextIndex, inner = 0, indexLeft, columnLeft;
    
    currentIndex = (row * WIDTH) + column;
    lastIndex = (row * WIDTH) + (column + 1);
    
    if (sameHsvRange(hsv, currentIndex, lastIndex)) {
        return row;
    }
    
    foundEdge = false;
    indexLeft = currentIndex;
    columnLeft = column;
        
    if(!foundEdge)
    {
        inner = 0;
        do
        {
            inner++;
            nextIndex = indexLeft - (WIDTH * inner);
        
            if (nextIndex > 0 && sameHsvRange(hsv, nextIndex + 2, lastIndex))
            {
                foundEdge = true;
                break;
            }
        } while (inner < MAX_DISTANCE); // above +2
    }
        
    if(!foundEdge)
    {
        inner = 0;
        do
        {
            inner++;
            nextIndex = indexLeft - (WIDTH * inner);
            
            if (nextIndex > 0 && sameHsvRange(hsv, nextIndex + 1, lastIndex))
            {
                foundEdge = true;
                break;
            }
        } while (inner < MAX_DISTANCE); // above +1
    }
        
        
    if(!foundEdge)
    {
        inner = 0;
        nextIndex = indexLeft - (WIDTH * inner);
        while (nextIndex > 0 && sameHsvRange(hsv, nextIndex, lastIndex))
        {
            inner++;
            foundEdge = true;
        }
    }
    
    if(!foundEdge)
    {
        inner = 0;
        do
        {
            inner++;
            nextIndex = indexLeft - (WIDTH * inner);
            if ( nextIndex > 0 &&  (sameHsvRange(hsv, nextIndex - 2, lastIndex) || sameHsvRange(hsv, nextIndex - 1, lastIndex))                    )
            {
                foundEdge = true;
                break;
            }
        } while (inner < MAX_DISTANCE); // below -2, -1
    }
        
        
    /////////////////////going to the other direction
    if (!foundEdge)
    {
        int distance = 0;
        foundEdge = false;
        inner = 0;
        do
        {
            inner++;
            //going right
            nextIndex = indexLeft + (WIDTH * inner);
            if ( nextIndex < MAX_SIZE &&
                (
                    sameHsvRange(hsv, nextIndex, lastIndex)
                ||
                    sameHsvRange(hsv, nextIndex + 1, lastIndex)
                ||
                    sameHsvRange(hsv, nextIndex + 2, lastIndex)
                )
                )
            {
                foundEdge = true;
                break;
            }
        } while (distance++ < MAX_DISTANCE);
        
        if (!foundEdge)
            inner = 0;
        else inner *= -1;
    }
    
    ////////////// handling edges
    
    if (inner > 0) //going left
    {
        inner++;
        indexLeft = (row * WIDTH) + columnLeft;

        
        while (indexLeft - (WIDTH * inner) > 0 && sameHsvRange(hsv, indexLeft - (WIDTH * inner), lastIndex))
        {
            inner++;
        }
        inner--;
        leftSideRow -= inner;
    }
    else if(inner < 0) //going right
    {
        inner *= -1;
        inner++;
        indexLeft = (row * WIDTH) + columnLeft;

        
        while ( indexLeft + (WIDTH * inner) < MAX_SIZE && sameHsvRange(hsv, indexLeft + (WIDTH * inner), lastIndex))
        {
            inner++;
        }
        inner--;
        leftSideRow += inner;
    }
    
    return leftSideRow;

}

int edgeDetector::goToSameRangeIndexRight(uchar* mask, uchar* hsv, unsigned int column, unsigned int row)
{
    
    int currentIndex , lastIndex;
    int RightSideRow = row;
    bool foundEdge;
    int nextIndex, inner = 0, indexRight, columnRight;
    
    currentIndex = (row * WIDTH) + column;
    lastIndex = (row * WIDTH) + (column + 1);
    
    if (sameHsvRange(hsv, currentIndex, lastIndex)) {
        return row;
    }
    
    foundEdge = false;
    indexRight = currentIndex;
    columnRight = column;
    
    if(!foundEdge)
    {
        inner = 0;
        do
        {
            inner++;
            nextIndex = indexRight + (WIDTH * inner);
            
            if (nextIndex < MAX_SIZE && sameHsvRange(hsv, nextIndex + 2, lastIndex))
            {
                foundEdge = true;
                break;
            }
        } while (inner < MAX_DISTANCE); // above +2
    }
    
    if(!foundEdge)
    {
        inner = 0;
        do
        {
            inner++;
            nextIndex = indexRight + (WIDTH * inner);
            
            if (nextIndex < MAX_SIZE && sameHsvRange(hsv, nextIndex + 1, lastIndex))
            {
                foundEdge = true;
                break;
            }
        } while (inner < MAX_DISTANCE); // above +1
    }
    
    
    if(!foundEdge)
    {
        inner = 0;
        nextIndex = indexRight + (WIDTH * inner);
        while (nextIndex < MAX_SIZE && sameHsvRange(hsv, nextIndex, lastIndex))
        {
            inner++;
            foundEdge = true;
        }
    }
    
    if(!foundEdge)
    {
        inner = 0;
        do
        {
            inner++;
            nextIndex = indexRight + (WIDTH * inner);
            if ( nextIndex < MAX_SIZE &&  (sameHsvRange(hsv, nextIndex - 2, lastIndex) || sameHsvRange(hsv, nextIndex - 1, lastIndex))                    )
            {
                foundEdge = true;
                break;
            }
        } while (inner < MAX_DISTANCE); // below -2, -1
    }
    
    
    /////////////////////going to the other direction
    if (!foundEdge)
    {
        int distance = 0;
        foundEdge = false;
        inner = 0;
        do
        {
            inner++;
            //going left
            nextIndex = indexRight - (WIDTH * inner);
            if ( nextIndex > 0 &&
                (
                 sameHsvRange(hsv, nextIndex, lastIndex)
                 ||
                 sameHsvRange(hsv, nextIndex + 1, lastIndex)
                 ||
                 sameHsvRange(hsv, nextIndex + 2, lastIndex)
                 )
                )
            {
                foundEdge = true;
                break;
            }
        } while (distance++ < MAX_DISTANCE);
        
        if (!foundEdge)
            inner = 0;
        else inner *= -1;
    }
    
    ////////////// handling edges
    
    if (inner > 0) //going right
    {
        inner++;
        indexRight = (row * WIDTH) + columnRight;
        
        
        while (indexRight + (WIDTH * inner) < MAX_SIZE && sameHsvRange(hsv, indexRight + (WIDTH * inner), lastIndex))
        {
            inner++;
        }
        inner--;
        RightSideRow -= inner;
    }
    else if(inner < 0) //going left
    {
        inner *= -1;
        inner++;
        indexRight = (row * WIDTH) + columnRight;
        
        
        while ( indexRight - (WIDTH * inner) > 0 && sameHsvRange(hsv, indexRight - (WIDTH * inner), lastIndex))
        {
            inner++;
        }
        inner--;
        RightSideRow += inner;
    }
    
    return RightSideRow;
    
}

bool edgeDetector::sameHsvRange(uchar* hsv, unsigned int currentIndex, unsigned int lastIndex)
{
    
    ///////////////////
    return false;
    /////////////////
    int hsvCurrentIndex, hsvLastIndex;
    
    hsvCurrentIndex = currentIndex * 3;
    hsvLastIndex = lastIndex * 3;
    
    if (hsvCurrentIndex + 2 > MAX_SIZE_X_3){
        return false;
    }
    
            return
                !(
                 hsv[hsvCurrentIndex] > hsv[hsvLastIndex] + NEXT_HUE_THRESHOLD ||
                 hsv[hsvCurrentIndex] < hsv[hsvLastIndex] - NEXT_HUE_THRESHOLD ||
                 hsv[hsvCurrentIndex + 1] > hsv[hsvLastIndex + 1] + NEXT_SATURATION_THRESHOLD ||
                 hsv[hsvCurrentIndex + 1] < hsv[hsvLastIndex + 1] - NEXT_SATURATION_THRESHOLD ||
                 hsv[hsvCurrentIndex + 2] > hsv[hsvLastIndex + 2] + NEXT_VALUE_THRESHOLD ||
                 hsv[hsvCurrentIndex + 2] < hsv[hsvLastIndex + 2] - NEXT_VALUE_THRESHOLD
                 )
                ||
                !(
                  hsv[hsvCurrentIndex] > skinHueValue + SKIN_HUE_THRESHOLD||
                  hsv[hsvCurrentIndex] < skinHueValue - SKIN_HUE_THRESHOLD ||
                  ((skinHueValue - SKIN_HUE_THRESHOLD < 0) &&
                   (hsv[hsvCurrentIndex] > 180 - SKIN_HUE_THRESHOLD ) &&
                   (hsv[hsvCurrentIndex] - 180 < skinHueValue - SKIN_HUE_THRESHOLD)
                   )||
                  hsv[hsvCurrentIndex + 1] > skinSaturationValue + SKIN_SATURATION_THRESHOLD ||
                  hsv[hsvCurrentIndex + 1] < skinSaturationValue - SKIN_SATURATION_THRESHOLD ||
                  hsv[hsvCurrentIndex + 2] > skinValueValue + SKIN_VALUE_THRESHOLD ||
                  hsv[hsvCurrentIndex + 2] < skinValueValue - SKIN_VALUE_THRESHOLD
                )
                ;
}

void edgeDetector::saveSkinColor(uchar* bg, uchar*  mask, uchar*  hsv, unsigned int faceLeft, unsigned int faceTop, unsigned int faceWidth, unsigned int faceHeight){
    
    unsigned int faceCenter, index, index2, c, r, hsvIndex;
    int count = 0;
    
    float hue_values[50];
    float saturation_values[50];
    float values_values[50];
    int counter[50];
    
    float hue, sat, val, skin_hue, skin_sat, skin_val;
    bool foundRange = false;
    int i=0;
    
    faceCenter = faceTop - (faceHeight)*0.2;  //column
    //    faceCenter = faceTop;
    c = faceCenter;    // = column
    faceLeft += (0.3 * faceHeight);
    
    index = (faceLeft * WIDTH*4)+(c * 4);
    index2 = (faceLeft * WIDTH) + c;
    hsvIndex = index2 * 3;
    
    for(int ii = 0; ii < 50; ii++){
        counter[ii] = 0;
        hue_values[ii]=0;
        saturation_values[ii]=0;
        values_values[ii]=0;
    }
    
    for (r = faceLeft ; r < (faceLeft + faceHeight)* 0.6  && index < MAX_SIZE*4 - 3 ; r++, index2 += WIDTH, index += WIDTH_X_4)//, x++ | x axis
    {
        bg[index] = 0;
        bg[index + 1] = 0;
        bg[index + 2] = 0;
        
        foundRange = false;
        hue = hsv[hsvIndex];
        sat = hsv[hsvIndex + 1];
        val = hsv[hsvIndex + 2];
        i = 0;
        
        if (count > 0){
            while (!foundRange && i < count){
                if (!(hue > hue_values[i] + SKIN_HUE_THRESHOLD ||
                      hue < hue_values[i] - SKIN_HUE_THRESHOLD ||
                      sat > saturation_values[i] + SKIN_SATURATION_THRESHOLD ||
                      sat < saturation_values[i] - SKIN_SATURATION_THRESHOLD ||
                      val > values_values[i] + SKIN_VALUE_THRESHOLD ||
                      val < values_values[i] - SKIN_VALUE_THRESHOLD
                      )){
                    counter[i]++;
                    foundRange = true;
                    
                    if (counter[i] == 50){
                        skin_hue = hue_values[i];
                        skin_sat = saturation_values[i];
                        skin_val = values_values[i];
                        //                         return;
                    }
                }
                i++;
            }
        }
        
        if (!foundRange || count == 0){
            count++;
            hue_values[i] = hue;
            saturation_values[i] = sat;
            values_values[i] = val;
        }
        
    }
    
    
//    int ccc = 0;
//    
//    //draw line for calculate skin color range
//    int WIDTH = 640, HEIGHT = 480;
//    for (int r = 0; r < HEIGHT; r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for (int c = 0; c < WIDTH; c++, index += 4, index2++)
//        {
//            
//            if(faceLeft!=0)
//            {
//                if (r == faceLeft && c <= faceCenter && c > faceCenter - 150)
//                {       //green
//                    bg[index] = 70;
//                    bg[index + 1] = 255;
//                    bg[index + 2] = 70;
//                    
//                    hsvIndex = index2 * 3;
//                    
//                }
//            }
//        }
//    }
    
    
    for(i = 0; i < 50; i++){
        if (counter[i] > 49){
            skin_hue = hue_values[i];
            skin_sat = saturation_values[i];
            skin_val = values_values[i];
            
            skinHueValue = skin_hue;
            skinSaturationValue = skin_sat;
            skinValueValue = skin_val;
            foundSkinColor = true;
            return;
        }
    }
    
}

int edgeDetector::findLeftSideRow(uchar* bg, uchar* dss, uchar* mask, uchar* hsv, uchar* edgesBuffer, int columnLeft, int leftSideRow)
{
    //        inner = 0;
//        hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);

    int indexLeft, nextIndex, inner, hsvIndex, point1, point2;
    bool foundEdge, isLeftGoingLeft;
//        if (inner == 0)

    indexLeft = (leftSideRow * WIDTH) + columnLeft;


    {
        foundEdge = false;

        if(!foundEdge)
        {
            inner = 0;
            do
            {
                inner++;
                nextIndex = indexLeft - (WIDTH * inner);
                hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) - (WIDTH_X_3 * inner);
                
                if (nextIndex > 0 && mask[nextIndex + 2] == WHITE_PIXEL
//                    && (
//                        point1 = hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                point2 = hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                (point1 > 0)
//                                && (point2 < MAX_SIZE_X_3)
//                                && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                )
                        )
                {
                    foundEdge = true;
                    columnLeft += 2;
//                        __android_log_print(ANDROID_LOG_ERROR, "tag", "called column> +2222 > %d    %d ", c, columnLeft);
                    break;
                }


            } while (inner < MAX_DISTANCE); // above +2

//            if(foundEdge)
//            {
//                leftSideRow = findLeftSideRow(bg, dss, mask, hsv, edgesBuffer, columnLeft, leftSideRow - inner);
//                return leftSideRow;
//            }
        }

        if(!foundEdge)
        {
            inner = 0;
            do
            {
                inner++;
                nextIndex = indexLeft - (WIDTH * inner);

                hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) - (WIDTH_X_3 * inner);
                
                if (nextIndex > 0 && mask[nextIndex + 1] == WHITE_PIXEL
//                                      && (
//                            point1 = hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                    point2 = hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                    (point1 > 0)
//                                    && (point2 < MAX_SIZE_X_3)
//                                    && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                    )
                        )
                {
                    foundEdge = true;
//                    columnLeft += 1;
//                        __android_log_print(ANDROID_LOG_ERROR, "tag", "called column> +1111> %d    %d ", c, columnLeft);

                    break;
                }
            } while (inner < MAX_DISTANCE_X_3); // same

//            if(foundEdge)
//            {
//                leftSideRow = findLeftSideRow(bg, dss, mask, hsv, columnLeft, leftSideRow);
//                return leftSideRow;
//            }
        }


        /////////////////////////////////////////////
//
//            if(!foundEdge)
//            {
//                inner = -1; // so it starts from -0-
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft - (WIDTH * inner);
//                    hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) - (WIDTH_X_3 * inner);
//
//                    if (nextIndex > 0
//                        &&
//                        (
//                            (   mask[nextIndex] == WHITE_PIXEL && (
//                                point1 = hsvIndex + DISTANCE_FROM_LINE_HALF_X_3,
//                                point2 = hsvIndex - DISTANCE_FROM_LINE_HALF_X_3,
//                               (point2 > 0)
//                            && (point1 < MAX_SIZE_X_3)
//                            && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                                )
//                            )
//                        )
//                       )
//                    {
//                        foundEdge = true;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE_X_3); // same
//            }

        if(!foundEdge)
        {
            inner = 0;
            hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);
            
            while (indexLeft - (WIDTH * inner) > 0 && mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL
//                   && (
//                          (inner == 0
//                        && (
//                                     point1 = hsvIndex - (WIDTH_X_3 * DISTANCE_FROM_LINE) - (WIDTH_X_3 * inner),
//                                             point2 = hsvIndex + (WIDTH_X_3 * DISTANCE_FROM_LINE) - (WIDTH_X_3 * inner),
//                                             (point1 > 0)
//                                             && (point2 < MAX_SIZE_X_3)
//                                             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                             ))
//                        ||
//                        (
//                                point1 = hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * inner),
//                                        point2 = hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * inner),
//                                        (point2 > 0)
//        //                                            && (point2 < MAX_SIZE_X_3)
//                                        && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                        )
//                    )

                    )
            {
                inner++;
                foundEdge = true;
            }
        }

        /////////////////////////////////////////////



        if(!foundEdge)
        {
            inner = 0;
            do
            {
                inner++;
                nextIndex = indexLeft - (WIDTH * inner);
                hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) - (WIDTH_X_3 * inner);
                
                if ( nextIndex > 0 && ((mask[nextIndex - 2] == WHITE_PIXEL
//                                        && (
//                                point1 = hsvIndex - 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                        point2 = hsvIndex - 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                        (point1 > 0)
//                                        && (point2 < MAX_SIZE_X_3)
//                                        && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                        )
                                       )

                        ||
                        (mask[nextIndex - 1] == WHITE_PIXEL
//                         && (
//                                point1 = hsvIndex - 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                        point2 = hsvIndex - 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                        (point1 > 0)
//                                        && (point2 < MAX_SIZE_X_3)
//                                        && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                        )
                        )
                )
                        )
                {
                    foundEdge = true;
//                        inner = 1;
                    break;
                }
            } while (inner < MAX_DISTANCE); // below -2
        }


//            if(!foundEdge)
//            {
//                inner = 0;
//                while (indexLeft - (WIDTH * inner) > 0
//                       &&
//                       (mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL
//                        &&
//                        (
//                                (inner == 0
//                                 && (
//                                  point1 = hsvIndex - (WIDTH_X_3 * DISTANCE_FROM_LINE),
//                                  point2 = hsvIndex + (WIDTH_X_3 * DISTANCE_FROM_LINE),
//                                 (point1 > 0)
//                              && (point2 < MAX_SIZE_X_3)
//                              && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                                ))




//                                ||
//                                (
//                                 point1 = hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner)),
//                                 point2 = hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner)),
//                                (point1 > 0)
//                             && (point2 < MAX_SIZE_X_3)
//                             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                                )
//                        )
//                       )
//                      )
//                {
//                    inner++;
//                    foundEdge = true;
//                }
//            }


        /////////////////////going to the other direction
        if (!foundEdge)
        {
            int distance = 0;
            foundEdge = false;
            inner = 0;
            do
            {
                inner++;
                //dss index
                //going right
                nextIndex = indexLeft + (WIDTH * inner);
                hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) + (WIDTH_X_3 * inner);
                if ( nextIndex < MAX_SIZE
                     &&
                     (
                             (mask[nextIndex] == WHITE_PIXEL
//                              && (
//                                     point1 = hsvIndex + DISTANCE_FROM_LINE_X_3,
//                                             point2 = hsvIndex - DISTANCE_FROM_LINE_X_3,
//                                             (point1 < MAX_SIZE_X_3)
////                                             && (point2 > 0)
//                                             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//                             )
                             )
                             //                          || mask[nextIndex - 1] == WHITE_PIXEL
                             ||( mask[nextIndex + 1] == WHITE_PIXEL
//                                 && (
//                                     point1 = hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                             point2 = hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                             (point1 > 0)
//                                             && (point2 < MAX_SIZE_X_3)
//                                             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//                             )
                             )
                             //                          || mask[nextIndex - 2] == WHITE_PIXEL
                             ||( mask[nextIndex + 2] == WHITE_PIXEL
//                                 && (
//                                     point1 = hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                             point2 = hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                             (point1 > 0)
//                                             && (point2 < MAX_SIZE_X_3)
//                                             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//                             )
                             )
                     )
                        )
                {
                    foundEdge = true;
                    break;
                }
            } while (distance++ < MAX_DISTANCE);

            if (!foundEdge)
                inner = 0;
            else inner *= -1;
        }
    }

    ////////////// handling edges

    if (inner > 0) //going left
    {
        inner++;
        hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);
        indexLeft = (leftSideRow * WIDTH) + columnLeft;

        while (indexLeft - (WIDTH * inner) > 0 && mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL
//               && (
//                       point1 = hsvIndex - DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner),
//                               point2 = hsvIndex + DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner),
//                               (point1 > 0)
////                               && (point2 < MAX_SIZE_X_3)
//                               && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//               )
                )
        {
            inner++;
        }
        inner--;
        leftSideRow -= inner;
        isLeftGoingLeft = true;
    }

    else if(inner < 0) //going right
    {
        inner *= -1;
        inner++;
        hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);
        indexLeft = (leftSideRow * WIDTH) + columnLeft;

        while ( indexLeft + (WIDTH * inner) < MAX_SIZE && mask[indexLeft + (WIDTH * inner)] == WHITE_PIXEL
//                && (
//                        point1 = hsvIndex - DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner),
//                                point2 = hsvIndex + DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner) ,
////                                (point1 > 0) &&
//                            (point2 < MAX_SIZE_X_3)
//                                && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                )
                )
        {
            inner++;
        }
        inner--;
        leftSideRow += inner;
        isLeftGoingLeft = false;
    }
    else if(mask[indexLeft] != WHITE_PIXEL && leftSideRow < HEIGHT - 75 && leftSideRow > 75
            && columnLeft > 75 && columnLeft < WIDTH - 75)
    {
        unsigned int selForTopCol = 0, selForTopRow = 0;
        while(inner < MAX_DISTANCE)
        {
            //dss index like case when inner=0
            inner++;
            nextIndex = indexLeft + (inner * WIDTH);
            hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) + (WIDTH_X_3 * inner);
            if(nextIndex + 3 < MAX_SIZE && mask[nextIndex + 3] == WHITE_PIXEL
//               && (
//                    point1 = hsvIndex + 9 + DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            point2 = hsvIndex + 9 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            (point1 > 0)
//                            && (point2 < MAX_SIZE_X_3)
//                            && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//            )
                    )
            {
                selForTopCol = columnLeft + 3;
                selForTopRow = leftSideRow + inner;
                break;
            }
            if(nextIndex + 4 < MAX_SIZE && mask[nextIndex + 4] == WHITE_PIXEL
//               && (
//                    point1 = hsvIndex + 12 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            point2 = hsvIndex + 12 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            (point1 > 0)
//                            && (point2 < MAX_SIZE_X_3)
//                            && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//            )
                    )
            {
                selForTopCol = columnLeft + 4;
                selForTopRow = leftSideRow + inner;
                break;
            }
            if(nextIndex + 5 < MAX_SIZE && mask[nextIndex + 5] == WHITE_PIXEL
//               && (
//                    point1 = hsvIndex + 15 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            point2 = hsvIndex + 15 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            (point1 > 0)
//                            && (point2 < MAX_SIZE_X_3)
//                            && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//            )
                    )
            {
                selForTopCol = columnLeft + 5;
                selForTopRow = leftSideRow + inner;
                break;
            }
        }

        if(selForTopCol != 0)
        {
            int lastLeftSideRow = leftSideRow, lastRightSideRow = leftSideRow;
            unsigned int resTopCol = 0, resTopRow = 0;

            getToTop(bg, mask, hsv, selForTopCol, selForTopRow, false, &resTopCol, &resTopRow);

//                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ",leftSideRow, leftSideRow);

            fillShape(bg, dss, mask, hsv, edgesBuffer, resTopCol, resTopRow, columnLeft, &lastLeftSideRow, &lastRightSideRow);

//                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ", 0, 0);

            leftSideRow = lastLeftSideRow - 1;

//                inner = 0;
//                inner++;
//                hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);
//                indexLeft = (leftSideRow * WIDTH) + columnLeft;
//
//                while (indexLeft - (WIDTH * inner) > 0
//                       && mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL && (
//                               point1 = hsvIndex - (WIDTH_X_3 * inner) - DISTANCE_FROM_LINE_X_3,
//                                       point2 = hsvIndex - (WIDTH_X_3 * inner) + DISTANCE_FROM_LINE_X_3,
//                                       (point1 > 0)
////                                       && (point2 < MAX_SIZE_X_3)
//                                       && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                       )
//                        )
//                {
//                    inner++;
//                }
//                inner--;
//                leftSideRow -= inner;
//                isLeftGoingLeft = true;
        }
//        else //if(false) //no edge found , no top found
//        {
//           //   leftSideRow = goToSameRangeIndexLeft(mask, hsv, columnLeft, leftSideRow);
//        }

    }


    if (leftSideRow < 0)
        leftSideRow = 0;

    return leftSideRow;

}

int edgeDetector::findRightSideRow(uchar* bg, uchar* dss, uchar* mask, uchar* hsv, uchar* edgesBuffer, int columnRight, int rightSideRow)

{
    int indexRight, nextIndex, inner, hsvIndex, point1, point2;
    bool foundEdge, isRightGoingRight;



    indexRight = (rightSideRow * WIDTH) + columnRight;

//        if (inner == 0)
    {
        foundEdge = false;

        if(!foundEdge)
        {
            inner = 0;
            do
            {
                inner++;
                nextIndex = indexRight + (WIDTH * inner);
                hsvIndex = (rightSideRow * WIDTH_X_3) + (columnRight * 3) + (WIDTH_X_3 * inner);

                if (nextIndex < MAX_SIZE && mask[nextIndex + 2] == WHITE_PIXEL
//                    && (
//                        point1 = hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                point2 = hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                (point2 > 0)
//                                && (point1 < MAX_SIZE_X_3)
//                                && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                )
                        )
                {
                    foundEdge = true;
                    columnRight += 2;
//                        __android_log_print(ANDROID_LOG_ERROR, "tag", "called column> +2222 > %d    %d ", c, columnRight);
                    break;
                }
            } while (inner < MAX_DISTANCE); // above +2

//            if(foundEdge)
//            {
//                rightSideRow = findRightSideRow(bg, dss, mask, hsv, edgesBuffer, columnRight, rightSideRow + inner);
//                return rightSideRow;
//            }
        }

        if(!foundEdge)
        {
            inner = 0;
            do
            {
                inner++;
                nextIndex = indexRight + (WIDTH * inner);
                hsvIndex = (rightSideRow * WIDTH_X_3) + (columnRight * 3) + (WIDTH_X_3 * inner);

                if (nextIndex < MAX_SIZE && (mask[nextIndex + 1] == WHITE_PIXEL
//                                             && (
//                            point1 = hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                    point2 = hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                    (point1 < MAX_SIZE_X_3)
//                                    && (point2 > 0)
//                                    && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                    )
                                            )
                        )
                {
                    foundEdge = true;
//                        columnRight += 1;
//                        __android_log_print(ANDROID_LOG_ERROR, "tag", "called column> +1111> %d    %d ", c, columnRight);

                    break;
                }
            } while (inner < MAX_DISTANCE_X_3); // same
        }

        /////////////////////////////////////////////////////////////////////////////

//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexRight + (WIDTH * inner);
//                    hsvIndex = (rightSideRow * WIDTH_X_3) + (columnRight * 3) + (WIDTH_X_3 * inner);
//
//                    if (nextIndex > 0
//                        &&
//                        (
//                                (   mask[nextIndex] == WHITE_PIXEL && (
//                                        point1 = hsvIndex + DISTANCE_FROM_LINE_HALF_X_3,
//                                        point2 = hsvIndex - DISTANCE_FROM_LINE_HALF_X_3,
//                                        (point2 > 0)
//                                        && (point1 < MAX_SIZE_X_3)
//                                        && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                                    )
//                                )
//                        )
//                            )
//                    {
//                        foundEdge = true;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE_X_3); // same
//            }

//            if(!foundEdge)
//            {
//                inner = 0;
//                while (indexRight + (WIDTH * inner) < MAX_SIZE
//                       &&
//                       (mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL
//                        &&
//                        (
//                                (inner == 0
//                                 && (
//                                         point1 = hsvIndex - (WIDTH_X_3 * DISTANCE_FROM_LINE),
//                                         point2 = hsvIndex + (WIDTH_X_3 * DISTANCE_FROM_LINE),
//                                         (point1 > 0)
//                                         && (point2 < MAX_SIZE_X_3)
//                                         && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                                 ))
//                                ||
//                                (
//                                        point1 = hsvIndex + (WIDTH_X_3 * inner) + DISTANCE_FROM_LINE_HALF_X_3,
//                                        point2 = hsvIndex + (WIDTH_X_3 * inner) - DISTANCE_FROM_LINE_HALF_X_3,
//                                        (point1 > 0)
//                                        && (point2 < MAX_SIZE_X_3)
//                                        && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                                )
//                        )
//                       )
//                        )
//                {
//                    inner++;
//                    foundEdge = true;
//                }
//            }


        /////////////////////////////////////////////////////////////////////////////


        if(!foundEdge)
        {
            inner = 0;
            do
            {
                inner++;
                nextIndex = indexRight + (WIDTH * inner);
                hsvIndex = (rightSideRow * WIDTH_X_3) + (columnRight * 3) + (WIDTH_X_3 * inner);
                if ( nextIndex < MAX_SIZE && ((mask[nextIndex - 2] == WHITE_PIXEL
//                                               && (
//                                point1 = hsvIndex - 6 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                        point2 = hsvIndex - 6 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                        (point2 > 0)
//                                        && (point1 < MAX_SIZE_X_3)
//                                        && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                        )
                                              )

                        ||
                        (mask[nextIndex - 1] == WHITE_PIXEL
//                         && (
//                                point1 = hsvIndex - 3 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                        point2 = hsvIndex - 3 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                        (point2 > 0)
//                                        && (point1 < MAX_SIZE_X_3)
//                                        && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                        )
                        )
                )
                        )
                {
                    foundEdge = true;
//                        inner = 1;
                    break;
                }
            } while (inner < MAX_DISTANCE); // below -2
        }



        /////////////////////going to the other direction
        if (!foundEdge)
        {
            int distance = 0;
            foundEdge = false;
            inner = 0;
            do
            {
                inner++;
                //dss index
                //going right
                nextIndex = indexRight - (WIDTH * inner);
                hsvIndex = (rightSideRow * WIDTH_X_3) + (columnRight * 3) - (WIDTH_X_3 * inner);
                if ( nextIndex > 0 && ((mask[nextIndex] == WHITE_PIXEL
//                                        && (
//                                     point1 = hsvIndex + DISTANCE_FROM_LINE_X_3,
//                                             point2 = hsvIndex - DISTANCE_FROM_LINE_X_3,
////                                             (point1 < MAX_SIZE_X_3)
////                                             &&
//                                             (point2 > 0)
//                                             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//                             )
                             )
                             //                          || mask[nextIndex - 1] == WHITE_PIXEL
                             ||( mask[nextIndex + 1] == WHITE_PIXEL
//                                 && (
//                                     point1 = hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                             point2 = hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                             (point2 > 0)
//                                             && (point1 < MAX_SIZE_X_3)
//                                             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//                             )
                             )
                             //                          || mask[nextIndex - 2] == WHITE_PIXEL
                             ||( mask[nextIndex + 2] == WHITE_PIXEL
//                                 && (
//                                     point1 = hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                             point2 = hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                             (point1 < MAX_SIZE_X_3)
//                                             && (point2 > 0)
//                                             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//                             )
                             )
                             ||( mask[nextIndex - 1] == WHITE_PIXEL
//                                 && (
//                                     point1 = hsvIndex - 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                             point2 = hsvIndex - 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                                             (point1 > 0)
//                                             && (point2 < MAX_SIZE_X_3)
//                                             && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//                             )
                             )
                     )
                        )
                {
                    foundEdge = true;
                    break;
                }
            } while (distance++ < MAX_DISTANCE);

            if (!foundEdge)
                inner = 0;
            else inner *= -1;
        }
    }

    ////////////// handling edges

    if (inner > 0) //going right
    {
        inner++;
        hsvIndex = (rightSideRow * WIDTH_X_3) + (columnRight * 3);
        indexRight = (rightSideRow * WIDTH) + columnRight;

        while (indexRight + (WIDTH * inner) < MAX_SIZE && mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL
//               && (
//                       point1 = hsvIndex + (WIDTH_X_3 * inner) - DISTANCE_FROM_LINE_X_3,
//                               point2 = hsvIndex + (WIDTH_X_3 * inner) + DISTANCE_FROM_LINE_X_3,
////                                   (point1 > 0)
////                                   &&
//                               (point2 < MAX_SIZE_X_3)
//                               && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//               )
                )
        {
            inner++;
        }
        inner--;
        rightSideRow += inner;
        isRightGoingRight = true;
    }
    else if(inner < 0) //going left
    {
        inner *= -1;
        inner++;
        hsvIndex = (rightSideRow * WIDTH_X_3) + (columnRight * 3);
        indexRight = (rightSideRow * WIDTH) + columnRight;

        while ( indexRight - (WIDTH * inner) > 0 && mask[indexRight - (WIDTH * inner)] == WHITE_PIXEL
//                && (
//                        point1 = hsvIndex - (WIDTH_X_3 * inner) - DISTANCE_FROM_LINE_X_3,
//                                point2 = hsvIndex - (WIDTH_X_3 * inner) + DISTANCE_FROM_LINE_X_3,
//                                (point1 > 0)
//                                //                                    && (point2 < MAX_SIZE_X_3)
//                                && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                )
                )
        {
            inner++;
        }
        inner--;
        rightSideRow -= inner;
        isRightGoingRight = false;
    }
    else if(mask[indexRight] != WHITE_PIXEL && rightSideRow < HEIGHT - 75 && rightSideRow > 75
            && columnRight > 75 && columnRight < WIDTH - 75)
    {
        unsigned int selForTopCol = 0, selForTopRow = 0;
        while(inner < MAX_DISTANCE)
        {
            //dss index like case when inner=0
            inner++;
            nextIndex = indexRight - (inner * WIDTH);
            hsvIndex = (rightSideRow * WIDTH_X_3) + (columnRight * 3) - (WIDTH_X_3 * inner);

            if(nextIndex > 0 && mask[nextIndex + 3] == WHITE_PIXEL
//               && (
//                    point1 = hsvIndex + 9 + DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            point2 = hsvIndex + 9 - DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            (point2 > 0)
//                            && (point1 < MAX_SIZE_X_3)
//                            && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//            )
                    )
            {
                selForTopCol = columnRight + 3;
                selForTopRow = rightSideRow + inner;
                break;
            }
            if(nextIndex + 4 < MAX_SIZE && mask[nextIndex + 4] == WHITE_PIXEL
//               && (
//                    point1 = hsvIndex + 12 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            point2 = hsvIndex + 12 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            (point2 > 0)
//                            && (point1 < MAX_SIZE_X_3)
//                            && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//            )
                    )
            {
                selForTopCol = columnRight + 4;
                selForTopRow = rightSideRow + inner;
                break;
            }
            if(nextIndex + 5 < MAX_SIZE && mask[nextIndex + 5] == WHITE_PIXEL
//               && (
//                    point1 = hsvIndex + 15 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            point2 = hsvIndex + 15 - DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF),
//                            (point2 > 0)
//                            && (point1 < MAX_SIZE_X_3)
//                            && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//
//            )
                    )
            {
                selForTopCol = columnRight + 5;
                selForTopRow = rightSideRow + inner;
                break;
            }
        }

        if(selForTopCol != 0)
        {
            int lastLeftSideRow = rightSideRow, lastRightSideRow = rightSideRow;
            unsigned int resTopCol = 0, resTopRow = 0;

            getToTop(bg, mask, hsv, selForTopCol, selForTopRow, true, &resTopCol, &resTopRow);

//                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ",rightSideRow, rightSideRow);

            fillShape(bg, dss, mask, hsv, edgesBuffer, resTopCol, resTopRow, columnRight, &lastLeftSideRow, &lastRightSideRow);

//                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ", 0, 0);

            rightSideRow = lastRightSideRow + 1;

//                inner = 0;
//                inner++;
//                hsvIndex = (rightSideRow * WIDTH_X_3) + (columnRight * 3);
//                indexRight = (rightSideRow * WIDTH) + columnRight;
//
//                while (indexRight + (WIDTH * inner) < MAX_SIZE
//                       && mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL && (
//                               point1 = hsvIndex + (WIDTH_X_3 * inner) - DISTANCE_FROM_LINE_X_3,
//                                       point2 = hsvIndex + (WIDTH_X_3 * inner) + DISTANCE_FROM_LINE_X_3,
////                                   (point1 > 0)
////                                   &&
//                                       (point2 < MAX_SIZE_X_3)
//                                       && isNotInRange(hsv[point1], hsv[point2], hsv[point1 + 1], hsv[point2 + 1], hsv[point1 + 2], hsv[point2 + 2])
//                       )
//                        )
//                {
//                    inner++;
//                }
//                inner--;
//                rightSideRow += inner;
//                isRightGoingRight = true;
        }
//        else //if(false) //no edge found , no top found
//        {
//            //   rightSideRow = goToSameRangeIndexRight(mask, hsv, columnRight, rightSideRow);
//        }
    }

    if (rightSideRow >= HEIGHT)
        rightSideRow = HEIGHT - 1;
    return rightSideRow;
}

void edgeDetector::findBensEdges(uchar* src, uchar* dst, uchar* hsv)
{
    int lastHue = hsv[0], lastSaturation = hsv[1], lastValue = hsv[2];

    for (int r = 1, c, index, index2; r < HEIGHT; r++)
    {
        index = (WIDTH_X_3 * r) - 3;
        index2 = (WIDTH * r) - 1;
        for (c = 1; c < WIDTH; c++, index += 3, index2++)
        {
            if(lastHue - HUE_BEN_THRESHOLD < hsv[index] && hsv[index] < lastHue + HUE_BEN_THRESHOLD
               && lastSaturation - SATURATION_BEN_THRESHOLD < hsv[index + 1] && hsv[index + 1] < lastSaturation + SATURATION_BEN_THRESHOLD
               && lastValue - VALUE_BEN_THRESHOLD < hsv[index + 2] && hsv[index + 2] < lastValue + VALUE_BEN_THRESHOLD)
            {

            }
            else dst[index2] = 255;
            lastHue = hsv[index], lastSaturation = hsv[index + 1], lastValue = hsv[index + 2];
        }
    }
}

//void edgeDetector::findBensEdges(uchar* src, uchar* dst, uchar* hsv)
//{
//    int lastHue = hsv[0], lastSaturation = hsv[1], lastValue = hsv[2];
//
//    for (int r = 1, c, index, index2; r < 100; r++)
//    {
//        index = (WIDTH_X_3 * r) - 3;
//        index2 = (WIDTH * r) - 1;
//        for (c = 1; c < 100; c++, index += 3, index2++)
//        {
//            if(lastHue - HUE_BEN_THRESHOLD > hsv[index] || hsv[index] > lastHue + HUE_BEN_THRESHOLD
//               || lastSaturation - SATURATION_BEN_THRESHOLD > hsv[index + 1] || hsv[index + 1] > lastSaturation + SATURATION_BEN_THRESHOLD
//               || lastValue - VALUE_BEN_THRESHOLD > hsv[index + 2] || hsv[index + 2] > lastValue + VALUE_BEN_THRESHOLD)
//            {
//                dst[index2] = 255;
//            }
//        }
//    }
//}




bool edgeDetector::checkScreen(Mat screenMat, float accuracy, float resizeFactor)
{
    Mat cannyMat, grayscaleMat;

//    screenMat = [BIAScreenChecker matFromPixelBuffer:screen];
    resize(screenMat, screenMat, cv::Size(), resizeFactor, resizeFactor, CV_INTER_AREA);
    grayscaleMat = screenMat;
    Canny(grayscaleMat, cannyMat, 20, 60, 3);

    int count_white = 0;
    for( int y = 0; y < cannyMat.rows; y++ ) {
        for( int x = 0; x < cannyMat.cols; x++ ) {
            if ( cannyMat.at<uchar>(y,x) == 255 ) {
                count_white++;
            }
        }
    }

//    printf("%d white pixels, rate: %f\n", count_white, (float)count_white/cannyMat.size().area());

    if (((float)count_white / cannyMat.size().area()) < accuracy) {
        return true;
    }

    return false;
}

//void* edgeDetector::getEdgesBufferThreadRunner(void *data)//Mat bg, Mat dss, Mat dst, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom, bool booli)
//{
////    __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>>> %d   %d", 5, 25);
//
//    multiDetectionStruct* mDStructure = ((multiDetectionStruct *)data);
////    mds
//
////    bds->bg = bds->dst;
//
//    eUtil.getEdgesBuffer((*mDStructure).bg, (*mDStructure).dss, (*mDStructure).dst, (*mDStructure).faceLeft, (*mDStructure).faceTop, (*mDStructure).faceRight, (*mDStructure).faceBottom);
//
////    getEdgesBufferThread()
//
//    pthread_exit(0);
//}

pthread_t edgeDetector::startThread(Mat bg, Mat dss, Mat dst, unsigned int faceLeft, unsigned int faceTop, unsigned int faceRight, unsigned int faceBottom)
{
//    multiDetectionStruct mDStructure = {bg, dss, dst, faceLeft, faceTop, faceRight, faceBottom};
//
//    //(bg, dss, dst, faceLeft, faceTop, faceRight, faceBottom);
////    threadPackage.bg = bg;
////    threadPackage.dss = dss;
////    threadPackage.dst = dst;
////    threadPackage.faceLeft = faceLeft;
////    threadPackage.faceTop = faceTop;
////    threadPackage.faceRight = faceRight;
////    threadPackage.faceBottom = faceBottom;
//
////    {bg, dss, dst, faceLeft, faceTop, faceRight, faceBottom};
    pthread_t edgeThread;
//
//    pthread_create(&edgeThread, NULL, edgeDetector::getEdgesBufferThreadRunner, (void*) &mDStructure);
//
//
//    pthread_join(edgeThread, 0);


//    imageUtil.WIDTH = 390;
//    int u=imageUtil.WIDTH;
//    int l = MediaInfo::width;
//    *MediaInfo::width = 998;
//    imageUtil.

    return edgeThread;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//void helper::fillShape(uchar* bg, uchar* dss, uchar* mask, uchar* hsv, unsigned int topCol, unsigned int topRow, unsigned int lastCol
//        , int *lastLeftRow, int *lastRightRow)//, unsigned int faceBottomCol)
//{
//    int indexLeft, indexRight,
//            columnLeft = topCol, columnRight = topCol,
//            leftSideRow = topRow, rightSideRow = topRow,
//            inner, index, nextIndex, hsvIndex;
//
//    int lastLeftLoopRow = topRow, lastLeftLoopCol = topCol;
//    int lastRightLoopRow, lastRightLoopCol;
//
//    bool foundEdge;
//    int distance = 0, c = topCol;
//    bool isLeftGoingLeft(false), isRightGoingRight(false);
//
//    bool isLeftGoingUp(false);
//
//    unsigned int resTopCol, resTopRow;
//    int lastLeftSideRow, lastRightSideRow;
//
////    (sizeof(a)/sizeof(*a)) <<
//
//
//    //testing
//    count = 0;
//
////            __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>>> %d   %d", sizeof(mask), sizeof(hsv) / sizeof(*(hsv)));
////
////    int w;
////    int WIDTH = 640, HEIGHT =  480, index, index2;
//
////    int index2;
////    for (int r = 0; r < HEIGHT * 3; r++)
////    {
////        index = WIDTH * r * 4;
////        index2 = WIDTH * r;
////        for (int c = 0; c < WIDTH * 3; c++, index += 4, index2++)
////        {
////            __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa443>>> %d   %d", index2, r);
////            if(hsv[index2] == 9)
////                ;
////            if(hsv[index2+1] == 9)
////                ;
////            if(hsv[index2+2] == 9)
////                ;
////            if(hsv[index2+3] == 9)
////                ;
////        }
////    }
//
//
////    __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa>>> %d   %d", *lastLeftRow, lastCol);
//
//
//    indexLeft = (leftSideRow * WIDTH) + columnLeft;
//    inner = 1;
//    while( (indexLeft - (WIDTH * inner) > 0 &&
//            mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL )
//           ||
//           ( indexLeft - (WIDTH * inner) - 1 > 0
//             && mask[indexLeft - (WIDTH * inner) - 1] == WHITE_PIXEL )
//           ||
//           ( indexLeft - (WIDTH * inner) + 1 > 0
//             && mask[indexLeft - (WIDTH * inner) + 1] == WHITE_PIXEL )
////           ||
////           ( indexLeft - (WIDTH * inner) - 2 > 0
////             && mask[indexLeft - (WIDTH * inner) - 2] == WHITE_PIXEL )
////           ||
////           ( indexLeft - (WIDTH * inner) + 2 > 0
////             && mask[indexLeft - (WIDTH * inner) + 2] == WHITE_PIXEL )
//
//            )
//    {
//        inner++;
//    }
//    inner--;
//    leftSideRow -= inner;
//
//    indexRight = (rightSideRow * WIDTH) + columnRight;
//    inner = 1;
//    while(( indexRight + (WIDTH * inner) < MAX_SIZE &&
//            mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL )
//          ||
//          (indexRight + (WIDTH * inner) - 1 < MAX_SIZE &&
//           mask[indexRight + (WIDTH * inner) - 1] == WHITE_PIXEL )
//          ||
//          (indexRight + (WIDTH * inner) + 1 < MAX_SIZE &&
//           mask[indexRight + (WIDTH * inner) + 1] == WHITE_PIXEL )
////
////          || (indexRight + (WIDTH * inner) - 2 < MAX_SIZE &&
////              mask[indexRight + (WIDTH * inner) - 2] == WHITE_PIXEL )
//
////               || (indexRight + (WIDTH * inner) + 2 < MAX_SIZE &&
////                   mask[indexRight + (WIDTH * inner) + 2] == WHITE_PIXEL )
//            )
//    {
//        inner++;
//    }
//    inner--;
//    rightSideRow += inner;
//
//    lastLeftLoopCol = columnLeft;
//    lastLeftLoopRow = leftSideRow;
//
//    lastRightLoopCol = columnRight;
//    lastRightLoopRow = rightSideRow;
//
//    if(c >= WIDTH - 3)
//        return;
//
//    while (c > 3 && c != lastCol)
//    {
//
//        indexLeft = (leftSideRow * WIDTH) + columnLeft;
//        indexRight = (rightSideRow * WIDTH) + columnRight;
//
//
//
//        ///////////////////left side/////////////////////
//
//        inner = 0;
//        hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);
//
//        while (indexLeft - (WIDTH * inner) > 0
//               &&
//               (mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL
//                &&
//                (
//                        (inner == 0
//                         &&
//                         hsvIndex - ( WIDTH_X_3 * DISTANCE_FROM_LINE) > 0
//                         && hsvIndex + ( WIDTH_X_3 * DISTANCE_FROM_LINE) < MAX_SIZE_X_3
//                         && isNotInRange(hsv[hsvIndex - ( WIDTH_X_3 * DISTANCE_FROM_LINE)], hsv[hsvIndex + ( WIDTH_X_3 * DISTANCE_FROM_LINE)]
//                                ,hsv[hsvIndex - ( WIDTH_X_3 * DISTANCE_FROM_LINE) + 1], hsv[hsvIndex + ( WIDTH_X_3 * DISTANCE_FROM_LINE) + 1]
//                                ,hsv[hsvIndex - ( WIDTH_X_3 * DISTANCE_FROM_LINE) + 2], hsv[hsvIndex + ( WIDTH_X_3 * DISTANCE_FROM_LINE) + 2])
//                        )
//                        ||
//                        (
//                                hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner)) > 0
//                                && hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner)) < MAX_SIZE_X_3
//                                && isNotInRange(hsv[hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner))] , hsv[hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner))]
//                                        ,hsv[hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner)) + 1] , hsv[hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner)) + 1]
//                                        ,hsv[hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner)) + 2] , hsv[hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner)) + 2]
//                                )
//                        )
//                )
//               )
//                )
//        {
//            inner++;
//        }
//
//        if (inner == 0)
//        {
//            foundEdge = false;
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft - (WIDTH * inner);
//
//                    hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) - (WIDTH_X_3 * inner);
//
//                    if (nextIndex > 0
//                        &&
//                        (
//                                (mask[nextIndex] == WHITE_PIXEL
//                                 && hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
//                                 && hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
//                                 && isNotInRange(hsv[hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                                 hsv[hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                                 hsv[hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                                 hsv[hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                                 hsv[hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2],
//                                                 hsv[hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2])
//
////                                 && (isInRange (hsv[ dssIndex - WIDTH - WIDTH - WIDTH]
////                                        , hsv[ dssIndex + WIDTH + WIDTH + WIDTH ])
////                                    ||
////                                     isInRange(hsv[ (leftSideRow * WIDTH_X_4) + (columnLeft*4) + (WIDTH_X_4 * inner) - 3]
////                                        ,hsv[ (leftSideRow * WIDTH_X_4) + (columnLeft*4) + (WIDTH_X_4 * inner) + 3])
////                                    )
//                                )
//                                //                         ||
//                                //                                (mask[nextIndex - 1] == WHITE_PIXEL
//                                //                                 && dssIndex - 4 + DISTANCE_FROM_LINE_HALF_X_4 - (WIDTH_X_4 * DISTANCE_FROM_LINE_HALF) > 0
//                                //                                 && dssIndex - 4 - DISTANCE_FROM_LINE_HALF_X_4 + (WIDTH_X_4 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_4
//                                //                                 && isNotInRange(hsv[dssIndex - 4 + DISTANCE_FROM_LINE_HALF_X_4 - (WIDTH_X_4 * DISTANCE_FROM_LINE_HALF)],
//                                //                                                 hsv[dssIndex - 4 - DISTANCE_FROM_LINE_HALF_X_4 + (WIDTH_X_4 * DISTANCE_FROM_LINE_HALF)])
//                                //                                )
//                                ||
//                                (mask[nextIndex + 1] == WHITE_PIXEL
//                                 && hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
//                                 && hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
//                                 && isNotInRange(hsv[hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                                 hsv[hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                                 hsv[hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                                 hsv[hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                                 hsv[hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2],
//                                                 hsv[hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2])
//                                )
////                      || mask[nextIndex - 2] == WHITE_PIXEL
////                      || mask[nextIndex + 2] == WHITE_PIXEL
//                    )
//                            )
//                    {
//                        foundEdge = true;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // same
//            }
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft - (WIDTH * inner);
//                    hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) - (WIDTH_X_3 * inner);
//
//                    if ( nextIndex > 0 && mask[nextIndex + 2] == WHITE_PIXEL
//                         && hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
//                         && hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
//                         && isNotInRange(hsv[hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                         hsv[hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                         hsv[hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                         hsv[hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                         hsv[hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2],
//                                         hsv[hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2])
//                            )
//                    {
////                        if(nextIndex - WIDTH > 0 &&
////                            (//mask[nextIndex + 3] == WHITE_PIXEL
//////                            ||
////                              (mask[nextIndex - WIDTH] == WHITE_PIXEL
////                               && ( nextIndex - WIDTH - WIDTH > 0
////                                    && mask[nextIndex - WIDTH - WIDTH + 1] == WHITE_PIXEL)
////                             )
////                            ||
////                                    (mask[nextIndex - WIDTH + 2] == WHITE_PIXEL
////                                     && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
////                                            (   mask[nextIndex - WIDTH - WIDTH + 3] < MAX_SIZE
////                                             && mask[nextIndex - WIDTH - WIDTH + 3] == WHITE_PIXEL )
////                                        )
////                                    )
////                            //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                            )
////                          )
////                        if(nextIndex - WIDTH > 0 &&
////                           (//mask[nextIndex + 3] == WHITE_PIXEL
//////                            ||
////                                (mask[nextIndex - WIDTH + 2] == WHITE_PIXEL
////                                 && ( //nextIndex - WIDTH - WIDTH + 3< MAX_DISTANCE &&
////                                        mask[nextIndex - WIDTH - WIDTH + 3] == WHITE_PIXEL)
////                                )
////                                ||
////                                (//nextIndex + WIDTH + 3 < MAX_SIZE &&
////                                        mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                                        && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
////                                                (nextIndex - WIDTH - WIDTH > 0
////                                                 &&    mask[nextIndex - WIDTH - WIDTH + 4] == WHITE_PIXEL
////                                                        //    && mask[nextIndex + WIDTH - WIDTH + 3] == WHITE_PIXEL
////                                                )
////                                        )
////                                )
////                                //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                        )
////                                )
////                                inner = 0;
//                        foundEdge = true;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // above +2
//            }
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft - (WIDTH * inner);
//                    hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) - (WIDTH_X_3 * inner);
//                    if (
//                            ( nextIndex > 0 && mask[nextIndex - 2] == WHITE_PIXEL
//                              && hsvIndex - 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
//                              && hsvIndex - 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
//                              && isNotInRange(hsv[hsvIndex - 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                              hsv[hsvIndex - 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                              hsv[hsvIndex - 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                              hsv[hsvIndex - 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                              hsv[hsvIndex - 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2],
//                                              hsv[hsvIndex - 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2])
//                            )
//
//                            ||
//                            (mask[nextIndex - 1] == WHITE_PIXEL
//                             && hsvIndex - 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
//                             && hsvIndex - 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
//                             && isNotInRange(hsv[hsvIndex - 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                             hsv[hsvIndex - 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                             hsv[hsvIndex - 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                             hsv[hsvIndex - 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                             hsv[hsvIndex - 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2],
//                                             hsv[hsvIndex - 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2])
//                            )
//
//                            )
//
//
//                    {
//                        foundEdge = true;
//                        inner = 1;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // below -2
//            }
//
//
//
//            /////////////////////going to the other direction
//            if (!foundEdge)
//            {
//                distance = 0;
//                foundEdge = false;
//                inner = 0;
//                do
//                {
//                    inner++;
//                    //dss index
//                    //going right
//                    nextIndex = indexLeft + (WIDTH * inner);
//                    hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) + (WIDTH_X_3 * inner);
//                    if ( nextIndex < MAX_SIZE
//                         &&
//                         (
//                                 (mask[nextIndex] == WHITE_PIXEL && (
//                                         hsvIndex + DISTANCE_FROM_LINE_X_3 < MAX_SIZE_X_3
//                                         //                                        && dssIndex - DISTANCE_FROM_LINE_X_3 < MAX_SIZE_X_3
//                                         && isNotInRange(hsv[hsvIndex + DISTANCE_FROM_LINE_X_3],
//                                                         hsv[hsvIndex - DISTANCE_FROM_LINE_X_3],
//                                                         hsv[hsvIndex + DISTANCE_FROM_LINE_X_3 + 1],
//                                                         hsv[hsvIndex - DISTANCE_FROM_LINE_X_3 + 1],
//                                                         hsv[hsvIndex + DISTANCE_FROM_LINE_X_3 + 2],
//                                                         hsv[hsvIndex - DISTANCE_FROM_LINE_X_3 + 2])
//                                 )
//                                 )
//                                 //                          || mask[nextIndex - 1] == WHITE_PIXEL
//                                 ||( mask[nextIndex + 1] == WHITE_PIXEL &&
//                                     (
//                                             hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
//                                             && hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
//                                             && isNotInRange(hsv[hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                                             hsv[hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                                             hsv[hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                                             hsv[hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                                             hsv[hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2],
//                                                             hsv[hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2])
//                                     )
//                                 )
//                                 //                          || mask[nextIndex - 2] == WHITE_PIXEL
//                                 ||( mask[nextIndex + 2] == WHITE_PIXEL &&
//                                     (
//                                             hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
//                                             && hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
//                                             && isNotInRange(hsv[hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                                             hsv[hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                                             hsv[hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                                             hsv[hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                                             hsv[hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2],
//                                                             hsv[hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2])
//                                     )
//                                 )
//                         )
//                            )
//                    {
//                        foundEdge = true;
//                        break;
//                    }
//                } while (distance++ < MAX_DISTANCE);
//
//                if (!foundEdge)
//                    inner = 0;
//                else inner *= -1;
//            }
//        }
//
//        ////////////// handling edges
//
//        if (inner > 0) //going left
//        {
//            inner++;
//            hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);
//            //dssindex - distance*4 - (width*inner)
//            //dssindex + distance*4 - (width*inner)
//
//            while (indexLeft - (WIDTH * inner) > 0
//                   && mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL
//                   && hsvIndex - DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner) > 0
//                   //                                && dssIndex + DISTANCE_FROM_LINE_X_4 - (WIDTH_X_4 * inner) < MAX_SIZE_X_4
//                   && isNotInRange(hsv[hsvIndex - DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner)],
//                                   hsv[hsvIndex + DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner)],
//                                   hsv[hsvIndex - DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner) + 1],
//                                   hsv[hsvIndex + DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner) + 1],
//                                   hsv[hsvIndex - DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner) + 2],
//                                   hsv[hsvIndex + DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner) + 2])
//                    )
//            {
//                inner++;
//            }
//            inner--;
//            leftSideRow -= inner;
//            isLeftGoingLeft = true;
//        }
//        else if(inner < 0) //going right
//        {
//            inner *= -1;
//            inner++;
//            hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);
//            //dssindex - distance*4 + (width*inner)
//            //dssindex + distance*4 + (width*inner)
//            while ( indexLeft + (WIDTH * inner) < MAX_SIZE
//                    && mask[indexLeft + (WIDTH * inner)] == WHITE_PIXEL
//                    //                            && dssIndex - DISTANCE_FROM_LINE_X_4 + (WIDTH_X_4 * inner) > 0
//                    && hsvIndex + DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner) < MAX_SIZE_X_3
//                    && isNotInRange(hsv[hsvIndex - DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner)] ,
//                                    hsv[hsvIndex + DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner)],
//                                    hsv[hsvIndex - DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner) + 1] ,
//                                    hsv[hsvIndex + DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner) + 1],
//                                    hsv[hsvIndex - DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner) + 2] ,
//                                    hsv[hsvIndex + DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner) + 2])
//                    )
//            {
//                inner++;
//            }
//            inner--;
//            leftSideRow += inner;
//            isLeftGoingLeft = false;
//        }
//        else if(false && mask[indexLeft] != WHITE_PIXEL && leftSideRow < HEIGHT - 75 && leftSideRow > 75
//                && c > 75 && c < WIDTH - 75)
//        {
//            unsigned int selForTopCol = 0, selForTopRow = 0;
//            while(inner < MAX_DISTANCE)
//            {
//                //dss index like case when inner=0
//                inner++;
//                nextIndex = indexLeft + (inner * WIDTH);
//                hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) + (WIDTH_X_3 * inner);
//                if(nextIndex + 3 < MAX_SIZE && mask[nextIndex + 3] == WHITE_PIXEL && (
//                        hsvIndex + 9 + DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
//                        && hsvIndex + 9 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
//                        && isNotInRange(hsv[hsvIndex + 9 + DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                        hsv[hsvIndex + 9 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                        hsv[hsvIndex + 9 + DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                        hsv[hsvIndex + 9 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                        hsv[hsvIndex + 9 + DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2],
//                                        hsv[hsvIndex + 9 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2])
//                ))
//                {
//                    selForTopCol = c + 3;
//                    selForTopRow = leftSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 4 < MAX_SIZE && mask[nextIndex + 4] == WHITE_PIXEL && (
//                        hsvIndex + 12 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
//                        && hsvIndex + 12 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
//                        && isNotInRange(hsv[hsvIndex + 12 + DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                        hsv[hsvIndex + 12 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                        hsv[hsvIndex + 12 + DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                        hsv[hsvIndex + 12 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                        hsv[hsvIndex + 12 + DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2],
//                                        hsv[hsvIndex + 12 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2])
//                ))
//                {
//                    selForTopCol = c + 4;
//                    selForTopRow = leftSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 5 < MAX_SIZE && mask[nextIndex + 5] == WHITE_PIXEL && (
//                        hsvIndex + 15 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
//                        && hsvIndex + 15 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
//                        && isNotInRange(hsv[hsvIndex + 15 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                        hsv[hsvIndex + 15 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
//                                        hsv[hsvIndex + 15 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                        hsv[hsvIndex + 15 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 1],
//                                        hsv[hsvIndex + 15 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2],
//                                        hsv[hsvIndex + 15 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) + 2])
//                ))
//                {
//                    selForTopCol = c + 5;
//                    selForTopRow = leftSideRow + inner;
//                    break;
//                }
//            }
//
//            if(selForTopCol != 0)
//            {
//                lastLeftSideRow = leftSideRow;
//                lastRightSideRow = leftSideRow;
//                getToTop(bg, mask, hsv, selForTopCol, selForTopRow, false, &resTopCol, &resTopRow);
//
////                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ",leftSideRow, leftSideRow);
//
//                fillShape(bg, dss, mask, hsv, resTopCol, resTopRow, c, &lastLeftSideRow, &lastRightSideRow);
//
////                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ", 0, 0);
//
//                leftSideRow = lastLeftSideRow - 1;
//            }
//        }
//
//
//        if (leftSideRow < 0)
//            leftSideRow = 0;
//
////        __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ", leftSideRow, rightSideRow);
//
//
//        ///////////////////right side II/////////////////////
//
//        inner = 0;
//        //add dss index
//        //check for inner = 0 and inner != 0
//        while (indexRight + (WIDTH * inner) < MAX_SIZE
//               && mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL)
//        {
//            inner++;
//        }
//
//        if (inner == 0)
//        {
//            distance = 0;
//            foundEdge = false;
//            inner = 0;
//            do
//            {
//                inner++;
//                nextIndex = indexRight + (WIDTH * inner);
//                // check for 0 , 1, -1
//                if ( nextIndex < MAX_SIZE
//                     &&
//                     (mask[nextIndex] == WHITE_PIXEL
//                      || mask[nextIndex - 1] == WHITE_PIXEL
//                      || mask[nextIndex + 1] == WHITE_PIXEL
////                      || mask[nextIndex - 2] == WHITE_PIXEL
////                      || mask[nextIndex + 2] == WHITE_PIXEL
//                )
//                        )
//                {
//                    foundEdge = true;
//                    break;
//                }
//            } while (distance++ < MAX_DISTANCE); // same
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexRight + (WIDTH * inner);
//                    //+2
//                    if ( nextIndex < MAX_SIZE && mask[nextIndex + 2] == WHITE_PIXEL )
//                    {
////                        if(nextIndex + WIDTH + 3 < MAX_SIZE &&
////                           (mask[nextIndex + 3] == WHITE_PIXEL
////                            ||mask[nextIndex + WIDTH] == WHITE_PIXEL
////                            ||mask[nextIndex + WIDTH + 2] == WHITE_PIXEL
////                            || mask[nextIndex + WIDTH + 3] == WHITE_PIXEL)
////                                )
////                        if(nextIndex + WIDTH + 3 < MAX_SIZE &&
////                           (//mask[nextIndex + 3] == WHITE_PIXEL
//////                            ||
////                            (mask[nextIndex + WIDTH + 2] == WHITE_PIXEL
////                               && ( nextIndex + WIDTH + WIDTH + 3< MAX_DISTANCE
////                                    && mask[nextIndex + WIDTH + WIDTH + 3] == WHITE_PIXEL)
////                            )
////                            ||
////                            (//nextIndex + WIDTH + 3 < MAX_SIZE &&
////                             mask[nextIndex + WIDTH + 3] == WHITE_PIXEL
////                             && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
////                                     (nextIndex + WIDTH + WIDTH + 4 < MAX_SIZE
////                                      &&    mask[nextIndex + WIDTH + WIDTH + 4] == WHITE_PIXEL
////                                     //    && mask[nextIndex + WIDTH - WIDTH + 3] == WHITE_PIXEL
////                                     )
////                             )
////                            )
////                                   //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                           )
////                                )
////                            inner = 0;
//                        foundEdge = true;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // above +2
//            }
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    //-2
//                    nextIndex = indexRight + (WIDTH * inner);
//                    if ( nextIndex < MAX_SIZE && mask[nextIndex - 2] == WHITE_PIXEL )
//                    {
//                        foundEdge = true;
//                        inner = 1;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // below -2
//            }
//
//            /////////////////////going to the other direction
//            if (!foundEdge)
//            {
//                distance = 0;
//                foundEdge = false;
//                inner = 0;
//                do
//                {
//                    inner++;
//                    //dss index
//                    //going left
//                    nextIndex = indexRight - (WIDTH * inner);
//                    if ( nextIndex > 0
//                         &&
//                         (mask[nextIndex] == WHITE_PIXEL
//                          //                          || mask[nextIndex - 1] == WHITE_PIXEL
//                          || mask[nextIndex + 1] == WHITE_PIXEL
//                          //                          || mask[nextIndex - 2] == WHITE_PIXEL
//                          || mask[nextIndex + 2] == WHITE_PIXEL
//                         )
//                            )
//                    {
//                        foundEdge = true;
//                        break;
//                    }
//                } while (distance++ < MAX_DISTANCE);
//
//                if (!foundEdge)
//                    inner = 0;
//                else inner *= -1;
//            }
//        }
//
//        ////////////// handling edges
//
//        if (inner > 0) //going right
//        {
//            inner++;
//            //create dss index
//            //going right
//            // dssindex +distance*4 + (width__x_4 *inner)
//            // dssindex -distance*4 + (width__x_4 *inner)
//
//            while (indexRight + (WIDTH * inner) > 0
//                   && mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL)
//            {
//                inner++;
//            }
//            inner--;
//            rightSideRow += inner;
//            isRightGoingRight = true;
//        }
//        else if(inner < 0) //going  left
//        {
//            // dssindex +distance*4 - (width__x_4 *inner)
//            // dssindex -distance*4 - (width__x_4 *inner)
//
//            inner *= -1;
//            inner++;
//            while (indexRight - (WIDTH * inner) > 0
//                   && mask[indexRight - (WIDTH * inner)] == WHITE_PIXEL)
//            {
//                inner++;
//            }
//            inner--;
//            rightSideRow -= inner;
//            isRightGoingRight = false;
//        }
//        else if(false && mask[indexRight] != WHITE_PIXEL && rightSideRow < HEIGHT - 75 && rightSideRow > 75
//                && c > 75 && c < WIDTH - 75)
//        {
//            unsigned int selForTopCol = 0, selForTopRow = 0;
//            while(inner < 5)
//            {
//                inner++;
//                // dss - like inner = 0
//                nextIndex = indexRight - (inner * WIDTH);
//                if(nextIndex + 3 < MAX_SIZE && mask[nextIndex + 3] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 3;
//                    selForTopRow = rightSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 4 < MAX_SIZE && mask[nextIndex + 4] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 4;
//                    selForTopRow = rightSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 5 < MAX_SIZE && mask[nextIndex + 5] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 5;
//                    selForTopRow = rightSideRow + inner;
//                    break;
//                }
//            }
//
//            if(selForTopCol != 0)
//            {
//                unsigned int resTopCol = 0, resTopRow = 0;
//
//                lastLeftSideRow = leftSideRow;
//                lastRightSideRow = rightSideRow;
//
//                getToTop(bg, mask, hsv, selForTopCol, selForTopRow, true, &resTopCol, &resTopRow);
//
//                fillShape(bg, dss, mask, hsv, resTopCol, resTopRow, c, &lastLeftSideRow, &lastRightSideRow);
//                rightSideRow = lastRightSideRow + 1;
//            }
//        }
//
//        if (rightSideRow > HEIGHT)
//            rightSideRow = HEIGHT;
//
//        ///////////////////process edges/////////////////////
//
//        if (leftSideRow < rightSideRow)
//        {
//            *lastLeftRow = leftSideRow;
//            *lastRightRow = rightSideRow;
//
//            index = ((leftSideRow * WIDTH) + c) * 4;
//            for (inner = leftSideRow; inner < rightSideRow; inner++, index += WIDTH_X_4)
//            {
//                bg[index] = dss[index];
//                bg[index + 1] = dss[index + 1];
//                bg[index + 2] = dss[index + 2];
//            }
//        }
//
//        lastLeftLoopCol = columnLeft;
//        lastLeftLoopRow = leftSideRow;
//
//        c--;
//        columnLeft--;
//        columnRight--;
//        count++;
//    }
//
//}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//void helper::fillShape(uchar* bg, uchar* dss, uchar* mask, unsigned int topCol, unsigned int topRow, unsigned int lastCol
//        , int *lastLeftRow, int *lastRightRow)//, unsigned int faceBottomCol)
//{
//    int indexLeft, indexRight,
//            columnLeft = topCol, columnRight = topCol,
//            leftSideRow = topRow, rightSideRow = topRow,
//            inner, index, nextIndex;
//
//    bool foundEdge;
//    int distance = 0, c = topCol;
//    bool isLeftGoingLeft(false), isRightGoingRight(false);
//
//    bool isLeftGoingUp(false);
//
//    unsigned int resTopCol, resTopRow;
//    int lastLeftSideRow, lastRightSideRow;
//
////    __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa>>> %d   %d", *lastLeftRow, lastCol);
//
//    indexLeft = (leftSideRow * WIDTH) + columnLeft;
//    inner = 1;
//    while( (indexLeft - (WIDTH * inner) > 0 &&
//            mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL )
//           ||
//           ( indexLeft - (WIDTH * inner) - 1 > 0
//             && mask[indexLeft - (WIDTH * inner) - 1] == WHITE_PIXEL )
//           ||
//           ( indexLeft - (WIDTH * inner) + 1 > 0
//             && mask[indexLeft - (WIDTH * inner) + 1] == WHITE_PIXEL )
////           ||
////           ( indexLeft - (WIDTH * inner) - 2 > 0
////             && mask[indexLeft - (WIDTH * inner) - 2] == WHITE_PIXEL )
////           ||
////           ( indexLeft - (WIDTH * inner) + 2 > 0
////             && mask[indexLeft - (WIDTH * inner) + 2] == WHITE_PIXEL )
//
//            )
//    {
//        inner++;
//    }
//    inner--;
//    leftSideRow -= inner;
//
//    indexRight = (rightSideRow * WIDTH) + columnRight;
//    inner = 1;
//    while(( indexRight + (WIDTH * inner) < MAX_SIZE &&
//            mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL )
//          ||
//          (indexRight + (WIDTH * inner) - 1 < MAX_SIZE &&
//           mask[indexRight + (WIDTH * inner) - 1] == WHITE_PIXEL )
//          ||
//          (indexRight + (WIDTH * inner) + 1 < MAX_SIZE &&
//           mask[indexRight + (WIDTH * inner) + 1] == WHITE_PIXEL )
////
////          || (indexRight + (WIDTH * inner) - 2 < MAX_SIZE &&
////              mask[indexRight + (WIDTH * inner) - 2] == WHITE_PIXEL )
//
////               || (indexRight + (WIDTH * inner) + 2 < MAX_SIZE &&
////                   mask[indexRight + (WIDTH * inner) + 2] == WHITE_PIXEL )
//            )
//    {
//        inner++;
//    }
//    inner--;
//    rightSideRow += inner;
//
//    while (c > 3 && c != lastCol && c < WIDTH - 3)
//    {
//
//        indexLeft = (leftSideRow * WIDTH) + columnLeft;
//        indexRight = (rightSideRow * WIDTH) + columnRight;
//
//
//        ///////////////////left side/////////////////////
//
//        inner = 0;
//        while (indexLeft - (WIDTH * inner) > 0
//               && mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL)
//        {
//            inner++;
//        }
//
//        if (inner == 0)
//        {
//
//            foundEdge = false;
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft - (WIDTH * inner);
//                    if (nextIndex > 0
//                        &&
//                        (mask[nextIndex] == WHITE_PIXEL
//                         || mask[nextIndex - 1] == WHITE_PIXEL
//                         || mask[nextIndex + 1] == WHITE_PIXEL
////                      || mask[nextIndex - 2] == WHITE_PIXEL
////                      || mask[nextIndex + 2] == WHITE_PIXEL
//                    )
//                            )
//                    {
//                        foundEdge = true;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // same
//            }
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft - (WIDTH * inner);
//                    if ( nextIndex > 0 && mask[nextIndex + 2] == WHITE_PIXEL )
//                    {
////                        if(nextIndex - WIDTH > 0 &&
////                            (//mask[nextIndex + 3] == WHITE_PIXEL
//////                            ||
////                              (mask[nextIndex - WIDTH] == WHITE_PIXEL
////                               && ( nextIndex - WIDTH - WIDTH > 0
////                                    && mask[nextIndex - WIDTH - WIDTH + 1] == WHITE_PIXEL)
////                             )
////                            ||
////                                    (mask[nextIndex - WIDTH + 2] == WHITE_PIXEL
////                                     && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
////                                            (   mask[nextIndex - WIDTH - WIDTH + 3] < MAX_SIZE
////                                             && mask[nextIndex - WIDTH - WIDTH + 3] == WHITE_PIXEL )
////                                        )
////                                    )
////                            //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                            )
////                          )
////                        if(nextIndex - WIDTH > 0 &&
////                           (//mask[nextIndex + 3] == WHITE_PIXEL
//////                            ||
////                                (mask[nextIndex - WIDTH + 2] == WHITE_PIXEL
////                                 && ( //nextIndex - WIDTH - WIDTH + 3< MAX_DISTANCE &&
////                                        mask[nextIndex - WIDTH - WIDTH + 3] == WHITE_PIXEL)
////                                )
////                                ||
////                                (//nextIndex + WIDTH + 3 < MAX_SIZE &&
////                                        mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                                        && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
////                                                (nextIndex - WIDTH - WIDTH > 0
////                                                 &&    mask[nextIndex - WIDTH - WIDTH + 4] == WHITE_PIXEL
////                                                        //    && mask[nextIndex + WIDTH - WIDTH + 3] == WHITE_PIXEL
////                                                )
////                                        )
////                                )
////                                //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                        )
////                                )
////                                inner = 0;
//                        foundEdge = true;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // above +2
//            }
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft - (WIDTH * inner);
//                    if ( nextIndex > 0 && mask[nextIndex - 2] == WHITE_PIXEL )
//                    {
//                        foundEdge = true;
//                        inner = 1;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // below -2
//            }
//
//
//
//            /////////////////////going to the other direction
//            if (!foundEdge)
//            {
//                distance = 0;
//                foundEdge = false;
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft + (WIDTH * inner);
//                    if ( nextIndex < MAX_SIZE
//                         &&
//                         (mask[nextIndex] == WHITE_PIXEL
//                          //                          || mask[nextIndex - 1] == WHITE_PIXEL
//                          || mask[nextIndex + 1] == WHITE_PIXEL
//                          //                          || mask[nextIndex - 2] == WHITE_PIXEL
//                          || mask[nextIndex + 2] == WHITE_PIXEL
//                         )
//                            )
//                    {
//                        foundEdge = true;
//                        break;
//                    }
//                } while (distance++ < MAX_DISTANCE);
//
//                if (!foundEdge)
//                    inner = 0;
//                else inner *= -1;
//            }
//        }
//
//        ////////////// handling edges
//
//        if (inner > 0)
//        {
//            inner++;
//            while (indexLeft - (WIDTH * inner) > 0
//                   && mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL)
//            {
//                inner++;
//            }
//            inner--;
//            leftSideRow -= inner;
//            isLeftGoingLeft = true;
//        }
//        else if(inner < 0)
//        {
//            inner *= -1;
//            inner++;
//            while (indexLeft + (WIDTH * inner) < MAX_SIZE
//                   && mask[indexLeft + (WIDTH * inner)] == WHITE_PIXEL)
//            {
//                inner++;
//            }
//            inner--;
//            leftSideRow += inner;
//            isLeftGoingLeft = false;
//        }
//        else if(mask[indexLeft] != WHITE_PIXEL)
//        {
//            unsigned int selForTopCol = 0, selForTopRow = 0;
//            while(inner < 5)
//            {
//                inner++;
//                nextIndex = indexLeft + (inner * WIDTH);
//                if(nextIndex + 3 < MAX_SIZE && mask[nextIndex + 3] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 3;
//                    selForTopRow = leftSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 4 < MAX_SIZE && mask[nextIndex + 4] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 4;
//                    selForTopRow = leftSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 5 < MAX_SIZE && mask[nextIndex + 5] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 5;
//                    selForTopRow = leftSideRow + inner;
//                    break;
//                }
//            }
//
//            if(selForTopCol != 0)
//            {
//                lastLeftSideRow = leftSideRow;
//                lastRightSideRow = leftSideRow;
//                getToTop(bg, mask, selForTopCol, selForTopRow, false, &resTopCol, &resTopRow);
//
////                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ",leftSideRow, leftSideRow);
//
//                fillShape(bg, dss, mask, resTopCol, resTopRow, c, &lastLeftSideRow, &lastRightSideRow);
//
////                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ", 0, 0);
//
//                leftSideRow = lastLeftSideRow - 1;
//            }
//        }
//
//
//        if (leftSideRow < 0)
//            leftSideRow = 0;
//
////        __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ", leftSideRow, rightSideRow);
//
//
//        ///////////////////right side II/////////////////////
//
//        inner = 0;
//        while (indexRight + (WIDTH * inner) < MAX_SIZE
//               && mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL)
//        {
//            inner++;
//        }
//
//        if (inner == 0)
//        {
//            distance = 0;
//            foundEdge = false;
//            inner = 0;
//            do
//            {
//                inner++;
//                nextIndex = indexRight + (WIDTH * inner);
//                if ( nextIndex < MAX_SIZE
//                     &&
//                     (mask[nextIndex] == WHITE_PIXEL
//                      || mask[nextIndex - 1] == WHITE_PIXEL
//                      || mask[nextIndex + 1] == WHITE_PIXEL
////                      || mask[nextIndex - 2] == WHITE_PIXEL
////                      || mask[nextIndex + 2] == WHITE_PIXEL
//                )
//                        )
//                {
//                    foundEdge = true;
//                    break;
//                }
//            } while (distance++ < MAX_DISTANCE); // same
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexRight + (WIDTH * inner);
//                    if ( nextIndex < MAX_SIZE && mask[nextIndex + 2] == WHITE_PIXEL )
//                    {
////                        if(nextIndex + WIDTH + 3 < MAX_SIZE &&
////                           (mask[nextIndex + 3] == WHITE_PIXEL
////                            ||mask[nextIndex + WIDTH] == WHITE_PIXEL
////                            ||mask[nextIndex + WIDTH + 2] == WHITE_PIXEL
////                            || mask[nextIndex + WIDTH + 3] == WHITE_PIXEL)
////                                )
////                        if(nextIndex + WIDTH + 3 < MAX_SIZE &&
////                           (//mask[nextIndex + 3] == WHITE_PIXEL
//////                            ||
////                            (mask[nextIndex + WIDTH + 2] == WHITE_PIXEL
////                               && ( nextIndex + WIDTH + WIDTH + 3< MAX_DISTANCE
////                                    && mask[nextIndex + WIDTH + WIDTH + 3] == WHITE_PIXEL)
////                            )
////                            ||
////                            (//nextIndex + WIDTH + 3 < MAX_SIZE &&
////                             mask[nextIndex + WIDTH + 3] == WHITE_PIXEL
////                             && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
////                                     (nextIndex + WIDTH + WIDTH + 4 < MAX_SIZE
////                                      &&    mask[nextIndex + WIDTH + WIDTH + 4] == WHITE_PIXEL
////                                     //    && mask[nextIndex + WIDTH - WIDTH + 3] == WHITE_PIXEL
////                                     )
////                             )
////                            )
////                                   //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                           )
////                                )
////                            inner = 0;
//                        foundEdge = true;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // above +2
//            }
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexRight + (WIDTH * inner);
//                    if ( nextIndex < MAX_SIZE && mask[nextIndex - 2] == WHITE_PIXEL )
//                    {
//                        foundEdge = true;
//                        inner = 1;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // below -2
//            }
//
//            /////////////////////going to the other direction
//            if (!foundEdge)
//            {
//                distance = 0;
//                foundEdge = false;
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexRight - (WIDTH * inner);
//                    if ( nextIndex > 0
//                         &&
//                         (mask[nextIndex] == WHITE_PIXEL
//                          //                          || mask[nextIndex - 1] == WHITE_PIXEL
//                          || mask[nextIndex + 1] == WHITE_PIXEL
//                          //                          || mask[nextIndex - 2] == WHITE_PIXEL
//                          || mask[nextIndex + 2] == WHITE_PIXEL
//                         )
//                            )
//                    {
//                        foundEdge = true;
//                        break;
//                    }
//                } while (distance++ < MAX_DISTANCE);
//
//                if (!foundEdge)
//                    inner = 0;
//                else inner *= -1;
//            }
//        }
//
//        ////////////// handling edges
//
//        if (inner > 0)
//        {
//            inner++;
//            while (indexRight + (WIDTH * inner) > 0
//                   && mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL)
//            {
//                inner++;
//            }
//            inner--;
//            rightSideRow += inner;
//            isRightGoingRight = true;
//        }
//        else if(inner < 0)
//        {
//            inner *= -1;
//            inner++;
//            while (indexRight - (WIDTH * inner) > 0
//                   && mask[indexRight - (WIDTH * inner)] == WHITE_PIXEL)
//            {
//                inner++;
//            }
//            inner--;
//            rightSideRow -= inner;
//            isRightGoingRight = false;
//        }
//        else if(mask[indexRight] != WHITE_PIXEL)
//        {
//            unsigned int selForTopCol = 0, selForTopRow = 0;
//            while(inner < 5)
//            {
//                inner++;
//                nextIndex = indexRight - (inner * WIDTH);
//                if(nextIndex + 3 < MAX_SIZE && mask[nextIndex + 3] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 3;
//                    selForTopRow = rightSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 4 < MAX_SIZE && mask[nextIndex + 4] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 4;
//                    selForTopRow = rightSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 5 < MAX_SIZE && mask[nextIndex + 5] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 5;
//                    selForTopRow = rightSideRow + inner;
//                    break;
//                }
//            }
//
//            if(selForTopCol != 0)
//            {
//                unsigned int resTopCol = 0, resTopRow = 0;
//
//                lastLeftSideRow = leftSideRow;
//                lastRightSideRow = rightSideRow;
//
//                getToTop(bg, mask, selForTopCol, selForTopRow, false, &resTopCol, &resTopRow);
//
//                fillShape(bg, dss, mask, resTopCol, resTopRow, c, &lastLeftSideRow, &lastRightSideRow);
//                rightSideRow = lastRightSideRow + 1;
//            }
//        }
//
//        if (rightSideRow > HEIGHT)
//            rightSideRow = HEIGHT;
//
//        ///////////////////process edges/////////////////////
//
//        if (leftSideRow < rightSideRow)
//        {
//            *lastLeftRow = leftSideRow;
//            *lastRightRow = rightSideRow;
//
//            index = ((leftSideRow * WIDTH) + c) * 4;
//            for (inner = leftSideRow; inner < rightSideRow; inner++, index += WIDTH_X_4)
//            {
//                bg[index] = dss[index];
//                bg[index + 1] = dss[index + 1];
//                bg[index + 2] = dss[index + 2];
//            }
//        }
//
//        c--;
//        columnLeft--;
//        columnRight--;
//    }
//
//}

