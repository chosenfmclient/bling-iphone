//
//  ProfileViewController.swift
//  
//
//  Created by Michael Biehl on 9/25/16.
//
//

import UIKit
import SDWebImage

class ProfileViewController: SIABaseViewController,
VideoCollectionHandlerDelegate,
PageRequestHandlerDelegate,
ProfileWasEditedDelegate
{

    //MARK: -- Outlets
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet var menuButtonItem: UIBarButtonItem!
    @IBOutlet var inviteButtonItem: UIBarButtonItem!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet var facebookButtono: UIButton!
    @IBOutlet var twitterButton: UIButton!
    @IBOutlet var youtubeButton: UIButton!
    @IBOutlet var instagramButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet var blueBackgroundView: UIView!
    @IBOutlet weak var profileImageBackgroundView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var spinner: SIASpinner!
    
    @IBOutlet weak var totalVideosButton: UIButton!
    @IBOutlet weak var totalFollowersButton: UIButton!
    @IBOutlet weak var totalFollowingButton: UIButton!
    
    @IBOutlet weak var labelViewWidth: NSLayoutConstraint!
    @IBOutlet weak var numberOfVideosWidth: NSLayoutConstraint!
    
    @IBOutlet weak var socialLinkButtonView: UIView!
    @IBOutlet var socialLinkButtons: [UIButton]!
    
    @IBOutlet weak var facebookButtonAlignXConstraint: NSLayoutConstraint!
    @IBOutlet weak var twitterButtonAlignXConstraint: NSLayoutConstraint!
    @IBOutlet weak var youtubeButtonAlignXConstraint: NSLayoutConstraint!
    @IBOutlet weak var instagramButtonAlignXConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var videoCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoCollectionTopSeperatorView: UIView!
    
    @IBOutlet weak var videoCollectionLoweredTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoCollectionRaisedTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoCollectionView: UICollectionView!
    @IBOutlet var buttonAlignXConstraints: [NSLayoutConstraint]!
    
    @IBOutlet weak var socialLinkButtonViewHeight: NSLayoutConstraint!
    @IBOutlet var noVideosView: UIView!
    
    //MARK: -- Properties
    var videoCollectionHandler: VideoCollectionHandler?
    var pageRequestHandler: PageRequestHandler?
    
    var userId = "me"
    var isMyProfile = false
    var user: SIAUser?
    
    var socialButts: [UIButton] = []
    var profileLoaded = false
    
    var viewDisappeared = false
    var refreshMetaData = false
    
    let followText = "FOLLOW"
    let followImageName = "icon_follow_white"
    let followWhite = UIColor.white

    let followingText = "FOLLOWING"
    let followingImageName = "icon_followed_green"
    let followingGreen = UIColor(hex: "4cd964")
    
    //MARK: -- View controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        profileView.alpha = 0
        spinner.startAnimation(shouldLayout: false)
        
        socialButts = [facebookButtono, twitterButton, instagramButton, youtubeButton]
        
        videoCollectionHandler =
            VideoCollectionHandler(
                collectionView: videoCollectionView,
                viewController: self,
                loweredTopConstraint: videoCollectionLoweredTopConstraint,
                raisedTopConstraint: videoCollectionRaisedTopConstraint,
                delegate: self
        )
        
        videoCollectionView.dataSource = videoCollectionHandler
        videoCollectionView.delegate = videoCollectionHandler
        
        
        if (userId == "me") { // if loaded from bottom bar
            navigationItem.leftBarButtonItem = inviteButtonItem
        } else {
            navigationController.setNavigationBarLeftButtonBack()
        }
        
        if (userId == "me" || userId == BIALoginManager.myUserId()) { //is my profile
            isMyProfile = true
        } else {
            setFollowingUI(isFollowing: false)
            followButton.accessibilityIdentifier = "follow_button"
        }
        
        loadProfileData(for: userId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (isMyProfile) {
            self.navigationItem.rightBarButtonItem = menuButtonItem
        }
        navigationController?.setBottomBarHidden(false, animated: true)
        totalFollowersButton.isEnabled = true
        totalFollowingButton.isEnabled = true
        viewDisappeared = false
        self.navigationController?.setNavigationItemTitle(user?.fullName)
        if (isMyProfile) {
            navigationItem.leftBarButtonItem = inviteButtonItem
        } else {
            navigationController.setNavigationBarLeftButtonBack()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.setBottomBarHidden(false, animated: false)
        if (refreshMetaData) {
            setMetaData(for: user)
            displaySocialHandles(for: user)
            UIView.animate(
                withDuration: 0.3,
                delay: 0,
                options: .curveEaseInOut,
                animations: {[weak self] in
                    self?.profileView.alpha = 1
                },
                completion: nil)
        }
        
        if (isMyProfile && profileLoaded) {
            Utils.dispatchAfter(1, closure: { [weak self] in
                guard let sSelf = self, !sSelf.viewDisappeared else { return }
                sSelf.reloadVideoList()
            })
            reloadVideoList()
        }
        self.navigationController?.setNavigationItemTitle(user?.fullName)
        AmplitudeEvents.log(event: isMyProfile ? "myprofile:pageview" : "profile:pageview")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        viewDisappeared = true
        videoCollectionView?.isUserInteractionEnabled = true
    }
    
    override func viewDidLayoutSubviews() {
        if (!isMyProfile) {
            navigationController?.clearNavigationItemRightButtons()
        }
        profileImageView?.bia_rounded()
        followButton?.bia_rounded()
        editProfileButton?.bia_rounded()
        youtubeButton?.bia_rounded()
        instagramButton?.bia_rounded()
        twitterButton?.bia_rounded()
        facebookButtono?.bia_rounded()
        profileImageBackgroundView?.bia_rounded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: -- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let followingVC = segue.destination as? SIAFollowingViewController {
            followingVC.userId = self.userId
            followingVC.backgroundImage = self.backgroundImageView.image
            if segue.identifier == "followingSegue" {
                followingVC.isFollowing = true
                let totalPeople = self.user?.total_following as? UInt != nil ? self.user?.total_following as? UInt : 0
                followingVC.totalPeople = totalPeople!
            } else {
                followingVC.isFollowing = false
                let totalPeople = self.user?.total_followers as? UInt != nil ? self.user?.total_followers as? UInt : 0
                followingVC.totalPeople = totalPeople!
            }
        }
        
        if let editVC = segue.destination as? EditProfileViewController {
            editVC.user = user
            editVC.delegate = self
        }
        
        if let menuVC = segue.destination as? SIANewMenuViewController {
            menuVC.user = user
            menuVC.editProfileDelegate = self
        }
        
        if let _ = segue.destination as? InviteContainerViewController {
            navigationController?.setNavigationBarHidden(true, animated: false)
        }
        
        if let followerVC = segue.destination as? FollowingFollowerContainerViewController {
            followerVC.userID = user?.user_id
            followerVC.totalFollowers = user?.total_followers.intValue
            followerVC.totalFollowing = user?.total_following.intValue
            if segue.identifier == "followingSegue" {
                followerVC.tab = followerVC.nFollowingTab
            } else if segue.identifier == "followersSegue" {
                followerVC.tab = followerVC.nFollowerTab
            }
        }
    }
    
    //MARK: -- API requests
    func reloadVideoList() {
        guard let userId = user?.user_id else { return }
        RequestManager.getRequest(
            String(format: "api/v1/users/%@/profile", userId),
            queryParameters: nil,
            success: {[weak self] (request, result) in
                guard let sSelf = self, let profile = (result?.firstObject as? ProfileResponseModel) else {
                    return
                }
                if let user = profile.user {
                    sSelf.user = user
                    sSelf.totalVideosButton.setTitle(String.init(format: "%d VIDEOS", user.total_performances.intValue), for: .normal)
                    sSelf.totalFollowersButton.setTitle(String.init(format: "%d FOLLOWERS", user.total_followers.intValue), for: .normal)
                    sSelf.totalFollowingButton.setTitle(String.init(format: "%d FOLLOWING", user.total_following.intValue), for: .normal)
                }
                sSelf.setCollectionView(for: profile.videos)
                sSelf.pageRequestHandler = PageRequestHandler(apiPath: String(format: "api/v1/users/%@/profile", sSelf.userId), delegate: sSelf)
                sSelf.pageRequestHandler?.page = 1
                
            },
            failure: nil
        )
    }
    
    func loadProfileData(for userId: String) {
        Utils.dispatchAsync {
            RequestManager.getRequest(
                String(format: "api/v1/users/%@/profile", userId),
                queryParameters: nil,
                success: {[weak self] (request, result) in
                    guard let sSelf = self, let profile = (result?.firstObject as? ProfileResponseModel), let user = profile.user else {
                        return
                    }
                    
                    sSelf.profileLoaded = true
                    
                    sSelf.user = user
                    
                    sSelf.setCollectionView(for: profile.videos)
                    sSelf.pageRequestHandler = PageRequestHandler(apiPath: String(format: "api/v1/users/%@/profile", sSelf.userId), delegate: sSelf)
                    sSelf.pageRequestHandler?.page = 1
                    
                    sSelf.setMetaData(for: user)
                    sSelf.displaySocialHandles(for: user)
                    
                    //Profile image
                    
                    SDWebImageManager.shared().loadImage(with: user.imageURL, options: .refreshCached, progress: nil, completed: {(image, data, error, cacheType, completed, url) in
                            Utils.dispatchOnMainThread {
                                
                                sSelf.profileImageView.image = image

                                if user.imageURL.absoluteString.contains("static/assets/images") {
                                    Utils.dispatchOnMainThread {
                                        UIView.animate(
                                            withDuration: 0.3,
                                            delay: 0,
                                            options: .curveEaseIn,
                                            animations: { [weak self] in
                                                self?.blueBackgroundView.alpha = 1
                                            },
                                            completion: nil
                                        )
                                    }
                                } else {
                                    UIView.transition(
                                        with: sSelf.backgroundImageView,
                                        duration: 1,
                                        options: .transitionCrossDissolve,
                                        animations: {
                                            sSelf.backgroundImageView.image = image
                                        },
                                        completion: nil
                                    )
                                    sSelf.user?.image = image
                                }
                                
                                //show view
                                sSelf.spinner.stopAnimation()
                                UIView.animate(
                                    withDuration: 0.3,
                                    delay: 0,
                                    options: .curveEaseInOut,
                                    animations: { [weak self] in
                                        self?.profileView.alpha = 1
                                    },
                                    completion: nil)
                            }
                        })
                },
                failure: {[weak self] (request, error) in
                    self?.spinner.stopAnimation()
                }
            )
        }

    }
    //MARK: -- Video collection view methods
    func setCollectionView(for videoList: [SIAVideo]?) {
        var videos = [SIAVideo]()
        if (videoList != nil) {
            videos = videoList!
        }
        
        if (videos.count > 0) {
            self.noVideosView.isHidden = true
            self.videoCollectionTopSeperatorView.isHidden = false
            self.videoCollectionView.isHidden = false
            self.videoCollectionHandler?.setVideoList(videos)
            self.videoCollectionView.reloadData()
            let numRows: CGFloat = videos.count > 3 ? 1.5 : 1.0
            let rowHeight = (UIScreen.main.bounds.width / 3) - 1
            let collectionHeight = (numRows * rowHeight) - 2 // -2 so that the collection can be scrolled slightly
            if (videoCollectionView.bounds.height > collectionHeight) {
                videoCollectionViewHeightConstraint.constant = collectionHeight
                videoCollectionViewHeightConstraint.isActive = true
                videoCollectionLoweredTopConstraint.isActive = false
                videoCollectionHandler?.collectionLoweredTopConstraint = videoCollectionViewHeightConstraint
            }
        } else {
            self.videoCollectionView.isHidden = true
            self.noVideosView.isHidden = false
            self.videoCollectionTopSeperatorView.isHidden = true
        }
    }
    
    func lastCollectionCellWillDisplay() {
        pageRequestHandler?.fetchData()
    }
    
    func dataWasLoaded(page: Int, data: Any?, requestIdentifier: Any?) {
        guard let videos = (data as? ProfileResponseModel)?.videos else {
            pageRequestHandler?.didReachEndOfContent = true
            return
        }
        videoCollectionHandler?.appendToVideoList(videos)
    }
    
    func endOfContentReached(at page: Int, requestIdentifier: Any?) {
        videoCollectionHandler?.shouldDisplayFooter = false
        videoCollectionView.reloadData()
    }
    
    //MARK: -- Profile edit
    func profileWasEdited() {
        refreshMetaData = true
        self.profileView.alpha = 0
    }
    
    //MARK: -- Display data
    func setMetaData(for userObject: SIAUser?) {
        guard let user = userObject else {
            return
        }
        guard let _ = self.navigationController else {
            return
        }
        
        if Utils.viewControllerIsTop(self) {
            self.navigationController.setNavigationItemTitle(user.fullName)
        }
        
        self.totalVideosButton.setTitle(String.init(format: "%d VIDEOS", user.total_performances.intValue), for: .normal)
        
        self.totalFollowersButton.setTitle(String.init(format: "%d FOLLOWERS", user.total_followers.intValue), for: .normal)
        self.totalFollowingButton.setTitle(String.init(format: "%d FOLLOWING", user.total_following.intValue), for: .normal)
        view.layoutIfNeeded()
        
        let widthOfLabelViews =
            self.totalVideosButton.frame.width +
                self.totalFollowersButton.frame.width +
                self.totalFollowingButton.frame.width +
                2 /*seperator width*/ +
                (16 * 2) /*padding width*/
        
        labelViewWidth.constant = widthOfLabelViews
        numberOfVideosWidth.constant = self.totalVideosButton.frame.width
        
        if !isMyProfile && user.followedByMe {
            setFollowingUI(isFollowing: true)
        }
        
        if let _ = user.image {
            profileImageView.image = user.image
            backgroundImageView.image = user.image
            blueBackgroundView.alpha = 0
        }
    }
    
    func displaySocialHandles(for userObject: SIAUser?) {
        guard let user = userObject else {
            return
        }
        guard let _ = self.navigationController else {
            return
        }

        socialLinkButtons = []
        socialLinkButtons.append(contentsOf: socialButts)
        //add all buttons to the view
        for button in socialLinkButtons {
            button.removeFromSuperview()
            socialLinkButtonView.addSubview(button)
        }
        
        socialLinkButtonView.isHidden = false
        socialLinkButtonViewHeight.constant = 30
        
        if (user.facebook_handle == nil || user.facebook_handle.isEmpty) {
            facebookButtono?.removeFromSuperview()
            socialLinkButtons.removeObject(facebookButtono)
        }
        
        if (user.youtube_handle == nil || user.youtube_handle.isEmpty) {
            youtubeButton?.removeFromSuperview()
            socialLinkButtons.removeObject(youtubeButton)
        }
        
        if (user.twitter_handle == nil || user.twitter_handle.isEmpty) {
            twitterButton?.removeFromSuperview()
            socialLinkButtons.removeObject(twitterButton)
        }
        
        if (user.instagram_handle == nil || user.instagram_handle.isEmpty) {
            instagramButton?.removeFromSuperview()
            socialLinkButtons.removeObject(instagramButton)
        }
        
        for (i, button) in socialLinkButtons.enumerated() {
            guard let _ = button.superview else {
                socialLinkButtons.remove(at: i)
                continue
            }
            button.centerYAnchor.constraint(equalTo: button.superview!.centerYAnchor, constant: CGFloat(0)).isActive = true
        }
        
        switch socialLinkButtons.count {
        case 4:
            for (index, button) in socialLinkButtons.enumerated() {
                var constant = CGFloat(0.0)
                let x = button.superview!.bounds.size.width / 6
                switch index {
                case 3:
                    constant = x / 1.5
                    break
                case 2:
                    constant = 2 * x
                    break
                case 1:
                    constant = -(x / 1.5)
                    break
                default:
                    constant = -(2 * x)
                    break
                }
                
                button.centerXAnchor.constraint(equalTo: (button.superview?.centerXAnchor)!, constant: CGFloat(constant)).isActive = true
            }
            break
        case 3:
            for (index, button) in socialLinkButtons.enumerated() {
                var constant = CGFloat(0.0)
                let x = button.superview!.bounds.size.width / 4
                if (index == 0) {
                    constant = x
                } else if (index == 2) {
                    constant = -x
                }
                button.centerXAnchor.constraint(equalTo: (button.superview?.centerXAnchor)!, constant: CGFloat(constant)).isActive = true
            }
            break
        case 2:
            for (index, button) in socialLinkButtons.enumerated() {
                let x = button.superview!.bounds.size.width / 8
                var constant = x
                
                if (index == 0) {
                    constant = -x
                }
                button.centerXAnchor.constraint(equalTo: (button.superview?.centerXAnchor)!, constant: CGFloat(constant)).isActive = true
            }
            break
        case 1:
            socialLinkButtons[0].centerXAnchor.constraint(equalTo: (socialLinkButtons[0].superview?.centerXAnchor)!, constant: 0).isActive = true
            break
        default:
            socialLinkButtonView.isHidden = true
            break
        }
        socialLinkButtonView.layoutSubviews()

    }
    
    //MARK: -- Follow
    func followUser() {
        guard let user = user else { return }
        setFollowingUI(isFollowing: !user.followedByMe, updateCounter: true)
        if (!user.followedByMe) {
            user.follow(handler: {[weak self] (success) in
                guard let _ = self else { return }
                if (!success) {
                    self?.setFollowingUI(isFollowing: false, updateCounter: true)
                }
                }, withAmplitudePageName: AmplitudeEvents.kProfilePageName)
        } else {
            user.unfollow(handler: {[weak self] (success) in
                if (!success) {
                    self?.setFollowingUI(isFollowing: true, updateCounter: true)
                }
                }, withAmplitudePageName:  AmplitudeEvents.kProfilePageName)
        }
        
    }
    
    func setFollowingUI(isFollowing: Bool, updateCounter: Bool = false) {
        followButton.isHidden = false
        editProfileButton.isHidden = true
        if (isFollowing) {
            followButton.setTitle(followingText, for: .normal)
            followButton.setTitleColor(followingGreen, for: .normal)
            followButton.tintColor = followingGreen
            followButton.setImage(UIImage(named: followingImageName), for: .normal)
            followButton.sia_setBackgroundColorAnimated(followWhite)
            followButton.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0)
        } else {
            followButton.setTitle(followText, for: .normal)
            followButton.setTitleColor(followWhite, for: .normal)
            followButton.tintColor = followWhite
            followButton.setImage(UIImage(named: followImageName), for: .normal)
            followButton.sia_setBackgroundColorAnimated(followingGreen)
            followButton.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        }
        if updateCounter {
            guard let user = user else { return }
            let followers = Int(user.total_followers)
            let change = isFollowing ? 1 : -1
            user.total_followers = NSNumber(value: followers + change)
            totalFollowersButton.setTitle(user.total_followers.stringValue + " FOLLOWERS", for: .normal)
        }
    }
    
    //MARK: -- Button actions
    @IBAction func followButtonItemWasTapped(_ sender: AnyObject) {
        followUser()
    }
    
    @IBAction func editProfileButtonWasTapped(_ sender: Any) {
        performSegue(withIdentifier: "editProfileSegue", sender: nil)
    }
    
    @IBAction func userImageViewButtonWasTapped(_ sender: Any) {
        if isMyProfile {
            performSegue(withIdentifier: "editProfileSegue", sender: nil)
        }
    }
    
    @IBAction func followingButtonWasTapped(_ sender: AnyObject) {
        totalFollowingButton.isEnabled = false
    }
    
    @IBAction func followersButtonWasTapped(_ sender: AnyObject) {
        totalFollowersButton.isEnabled = false
    }
    
    @IBAction func totalVideosButtonWasTapped(_ sender: AnyObject) {
        videoCollectionHandler?.raiseCollection()
    }
    
    //MARK: -- Social links
    @IBAction func facebookButtonWasTapped(_ sender: AnyObject) {
        guard let user = user else { return }
        guard !user.facebook_handle.isEmpty else { return }
        let link = getSocialLink(domain: "facebook", handle: user.facebook_handle)
//        let appURL = URL(string: String(format: "fb://profile/%@", user.facebook_handle))
        openSocialLink(webLink: link)
    }
    
    @IBAction func instagramButtonWasTapped(_ sender: AnyObject) {
        guard let user = user else { return }
        guard !user.instagram_handle.isEmpty else { return }
        let link = getSocialLink(domain: "instagram", handle: user.instagram_handle)
        let appURL = URL(string: String(format: "instagram://user?username=%@", user.instagram_handle))

        openSocialLink(webLink: link, appLink: appURL)

    }
    
    @IBAction func youtubeButtonWasTapped(_ sender: AnyObject) {
        guard let user = user else { return }
        guard !user.youtube_handle.isEmpty else { return }
        if let _ = user.youtube_handle {  // youtube should be full link now
            var link = user.youtube_handle!
            if link.contains("http:") {
                link = link.replacingOccurrences(of: "http:", with: "https:")
            }
            openSocialLink(webLink: link)
        }
    }
    
    @IBAction func twitterButtonWasTapped(_ sender: AnyObject) {
        guard let user = user else { return }
        guard !user.twitter_handle.isEmpty else { return }
        let link = getSocialLink(domain: "twitter", handle: String(format: "@%@", user.twitter_handle))
        let appURL = URL(string: String(format: "twitter://user?screen_name=%@", user.twitter_handle))
        
        openSocialLink(webLink: link, appLink: appURL)
    }
    
    func getSocialLink(domain: String, handle: String) -> String {
        if (domain == "youtube") {
            return String(format: "https://%@.com/user/%@", domain, handle)
        }
        
        return String(format: "https://%@.com/%@", domain, handle)
    }
    
    func openSocialLink(webLink: String, appLink: URL? = nil) {
        if let _ = appLink, UIApplication.shared.canOpenURL(appLink!) {
            UIApplication.shared.openURL(appLink!)
        } else if let web = URL(string: webLink) {
            UIApplication.shared.openURL(web)
        }
    }

}
