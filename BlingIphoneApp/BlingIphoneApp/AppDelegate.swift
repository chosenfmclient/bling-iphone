//  AppDelegate.swift
//  BlingIphoneApp
//
//  Created by Zach on 15/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import UserNotifications
import Amplitude_iOS
import Appsee
import Flurry_iOS_SDK
import AppsFlyerLib

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
   // var deeplinkURLToDefer: URL? // set this if a user opened the app from a deeplink before registering
    var deferredDeeplink: DeepLinkDataModel?
    var requestingPublishPermissions: Bool = false
    var hasTwitterAccount = false
    var backgroundFetchCompletion: ((UIBackgroundFetchResult ) -> Void)?
    var remoteDataCompletion: (() -> Void)?
    var facebookIsInitialized = false
    var didLoadHomeScreen = false
    var dontPushDeeplink = false
    var shouldSkipConnectView = false
    
    //saving list of users invited to duets/cameos
    var invitedUsersList = [String : [String]]() // [{video_id}, [{user_ids}]]
    
    func addUserIds(_ userIds: [String], toInvitedListFor videoId: String) {
        guard let idList = invitedUsersList[videoId] else {
            invitedUsersList[videoId] = userIds
            return
        }
        invitedUsersList[videoId] = idList + userIds
    }
    
    func removeUserIds(_ userIds: [String], fromInvitedListFor videoId: String) {
        guard let idList = invitedUsersList[videoId] else { return }
        invitedUsersList[videoId] = idList.filter { !userIds.contains($0) }
    }
    
    func listOfInvitedUsers(for videoId: String) -> [String]? {
        return invitedUsersList[videoId]
    }

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

        self.window = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "NavigationController");
        
        if let options = launchOptions { // don't show splash screen if app was launched from a push notification
            if BIALoginManager.userIsRegistered() {
                (vc as! BIANavigationController).shouldShowSplashScreen = !options.keys.contains(.remoteNotification) && !options.keys.contains(.url)
            }
            shouldSkipConnectView = options.keys.contains(.url)
        }
        
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
        
        //init videoplayer
        let _ = VideoPlayerManager.shared
        
        facebookIsInitialized = true
        
        //Analytics        
        AppsFlyerTracker.shared().appsFlyerDevKey = "nQQP92gyyU4NrR5kcrV4tS"
        AppsFlyerTracker.shared().appleAppID = "1179879437"
        
        Amplitude.instance().initializeApiKey(PCHStringDefinesWrapper.amplitudeAPIKey())
        
        Flurry.startSession(PCHStringDefinesWrapper.flurryAPIKey())
        Flurry.setUserID(BIALoginManager.myUserId())
        
        
        #if !DEBUG
            Appsee.start(PCHStringDefinesWrapper.appseeAPIKey())
        #endif
        downloadInitialVideos()

        if nil == UserDefaults.standard.object(forKey: "install_date") {
            UserDefaults.standard.setValue(Date(), forKey: "install_date")
        }
        
        if nil == UserDefaults.standard.object(forKey: "allow_tutorial_skip") {
            UserDefaults.standard.setValue(true, forKey: "allow_tutorial_skip")
        }
        
        if nil == UserDefaults.standard.object(forKey: PLAY_TUTORIAL) {
            UserDefaults.standard.setValue(true, forKey: PLAY_TUTORIAL)
        }
        
        if BIALoginManager.userIsRegistered() {
            application.registerForRemoteNotifications()
        }
        
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        
        return true
    }
    
    func downloadInitialVideos() {
        RequestManager.getPrimitiveObjectAt(PCHStringDefinesWrapper.preloadedVideosPath(),
                                            queryParameters: nil,
                                            success: {[weak self] (request, object) in
                                                if let videoURLs = object as? [String] {
                                                    self?.downloadVideosInArray(videoURLs.map {
                                                        URL(string:$0)!
                                                    })
                                                }
        },
                                            failure: { (request, error) in
            Appsee.addEvent("error:downloadingInitialHomeFeedFailed")
        })
    }
    
    func downloadVideosInArray(_ array: [URL]!, index: Int = 0) {
        guard index < array.count, index < 10 else { return }
        VideoPlayerAssetManager.shared.downloadFileAt(array[index]) {[weak self] (url, fileExisted) in
            self?.downloadVideosInArray(array,
                                        index: index + 1)
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        SIAPushRegistrationHandler.shared().pauseTimer()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if BIALoginManager.userIsRegistered() && didLoadHomeScreen {
            DeepLinkDataModel.checkForDeferredDeeplinks {[weak self] (deeplink) in
                print("fuck you im a deeplink from api")
                guard let _ = deeplink else { return }
                self?.pushDeeplink(deeplink!)
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        refreshUser()
        FBSDKAppEvents.activateApp()
        AppsFlyerTracker.shared().trackAppLaunch()
        
        SIAPushRegistrationHandler.shared().resumeTimer()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        SIAPushRegistrationHandler.shared().saveCurrentPushDict()
    }
    
    func application(_ application: UIApplication,
                     open url: URL,
                     sourceApplication: String?,
                     annotation: Any) -> Bool {
        
        if url.scheme == "blingy" {
            shouldSkipConnectView = true
            guard BIALoginManager.userIsRegistered() else {
                DeepLinkDataModel.parse(url: url, completion: {[weak self] (deeplonk) in
                    self?.deferredDeeplink = deeplonk
                })
                return true
            }
            print("fuck you im a deeplink from uri scheme")
            handleDeepLink(url: url)
            return true
        } else {
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
    }
    
    func userDidLogin() {
//        if let _ = deeplinkURLToDefer { // this wasn't needed because the deeplink API returned with it once home screen loaded
        //but this function might be useful later??????///???/?/???/??!??!!?!?
//            handleDeepLink(url: deeplinkURLToDefer!)
//            deeplinkURLToDefer = nil
//        }
    }
    
    //MARK: Deep link
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        guard let url =  userActivity.webpageURL else {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
            rootNavController()?.pushViewController(vc!, animated: true)
            return false
        }
        shouldSkipConnectView = true
        if BIALoginManager.userIsRegistered() {
           handleDeepLink(url: url)
        }
        else {
            DeepLinkDataModel.parse(url: url, completion: {[weak self] (deeplonk) in
                self?.deferredDeeplink = deeplonk
            })
        }
        
        return false
    }
    
    func handleDeepLink(url: URL) {
        DeepLinkDataModel.parse(url: url) {[weak self] (deeplink) in
            guard let _ = self else { return }
            guard let _ = deeplink else {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
                self?.rootNavController()?.pushViewController(vc!, animated: true)
                return
            }
            self?.dontPushDeeplink = true
            
            //push deeplink
            DeepLinkDataModel.postShareIDForDeeplinkAnalytics(deeplink!.shareID)
            if self?.rootNavController()?.topViewController is SIABlingRecordViewController {
                // don't open deeplonks if record flow is open
                self?.dontPushDeeplink = false
                return
            }
            guard let dest = deeplink!.destination else {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
                self?.rootNavController()?.pushViewController(vc!, animated: true)
                return
            }
            if (self!.didLoadHomeScreen) {
                self?.rootNavController()?.pushViewController(dest, animated: true)
            } else {
                let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC")
                self?.rootNavController()?.setViewControllers([homeVC, dest], animated: true)
            }
            
            Utils.dispatchAfter(5, closure: { [weak self] in
                self?.dontPushDeeplink = false
                }
            )
        }
    }
    
    func pushDeeplink(_ deeplink: DeepLinkDataModel) {
        guard BIALoginManager.userIsRegistered()  && !dontPushDeeplink else {
            return
        }
        
        DeepLinkDataModel.postShareIDForDeeplinkAnalytics(deeplink.shareID)
        guard let dest = deeplink.destination else {
            return
        }
        
        if rootNavController()?.topViewController is SIABlingRecordViewController {
            // don't open deeplonks if record flow is open
            return
        }
        
        self.rootNavController()?.pushViewController(dest, animated: true)
    }

    // MARK: Push Notifications
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        handlePushNotificationUserInfo(userInfo,
                                       forApplication: application,
                                       fetchHandler: completionHandler)
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
       handlePushNotificationUserInfo(userInfo,
                                      forApplication: application)
    }
    
    func handlePushNotificationUserInfo(_ userInfo: [AnyHashable : Any],
                                        forApplication application: UIApplication,
                                        fetchHandler: ((UIBackgroundFetchResult) -> Void)? = nil) {
        guard application.applicationState == .background || application.applicationState == .inactive else {
            
            guard let data = userInfo["data"] as? [String : Any] else { return }
            var alert: String?
            if let aps = userInfo["aps"] as? [String : Any] {
                alert = aps["alert"] as? String
            }
            RemoteDataHandler.sendPushMetrics(forReceiveType: "active",
                                              alert: alert,
                                              data: data)
            return
        }
        RemoteDataHandler.parseUserInfo(userInfo, fetchHandler)
        rootNavController()?.updateNotificationBadge()
        if let data = userInfo["data"] as? [String : Any] {
            var alert: String?
            if let aps = userInfo["aps"] as? [String : Any] {
                alert = aps["alert"] as? String
            }
            RemoteDataHandler.sendPushMetrics(forReceiveType: "react",
                                              alert: alert,
                                              data: data)
        }
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        guard UIApplication.shared.applicationState == .background || UIApplication.shared.applicationState == .inactive else {
            guard let data = response.notification.request.content.userInfo["data"] as? [String : Any] else { return }
            var alert: String?
            if let aps = response.notification.request.content.userInfo["aps"] as? [String : Any] {
                alert = aps["alert"] as? String
            }
            RemoteDataHandler.sendPushMetrics(forReceiveType: "active", alert: alert, data: data)
            return
        }
        RemoteDataHandler.parseResponse(response, completionHandler)
        rootNavController()?.updateNotificationBadge()
        if let data = response.notification.request.content.userInfo["data"] as? [String : Any] {
            var alert: String?
            if let aps = response.notification.request.content.userInfo["aps"] as? [String : Any] {
                alert = aps["alert"] as? String
            }
            RemoteDataHandler.sendPushMetrics(forReceiveType: "react",
                                              alert: alert,
                                              data: data)
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
    
    func application(_ application: UIApplication,
                     didRegister notificationSettings: UIUserNotificationSettings) {
        SIAPushRegistrationHandler.shared().applicationDidRegister()
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        BIALoginManager.sharedInstance().setApnSettingsWithDeviceToken(deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        BIALoginManager.sharedInstance().setApnSettingsWithDeviceToken(nil)
    }
    
    func remoteDataHandlerDidReceiveRemoteAction(actionType: String,
                                                 withObjectId objectId: String? = nil) {
        
        switch actionType {
        case "record":
            rootNavController()?.switchToRecord(withSearchTerm: objectId) // this is a search term, not an ID
        case "invite":
            rootNavController()?.switchToInvite()
        case "discover":
            rootNavController()?.switchToDiscover(forClipId: objectId);
        case "video":
            rootNavController()?.switchToVideoHome(forVideoId: objectId);
        case "user":
            rootNavController()?.switchToProfileViewController(withUserId: objectId)
        case "comment":
            rootNavController()?.switchToVideoHome(forVideoId: objectId,
                                                   showComments: true)
        case "leaderboard":
            rootNavController()?.switchToLeaderboard()
        case "cameo_invite":
            rootNavController()?.switchToVideoHomeFromCameoInvite(forVideoId: objectId)
        default:
            break
        }
        
        remoteDataWasHandled()
    }
    
    func remoteDataHandlerDidFailToParseData(_ data: [String : Any]) {
        print("Remote Data Fail?????")
        remoteDataWasHandled()
    }
    
    /// This must be called after receving a push notification so we an call the completion handler
    func remoteDataWasHandled() {
        if (remoteDataCompletion != nil) {
            remoteDataCompletion!()
        }
        else if (backgroundFetchCompletion != nil) {
            backgroundFetchCompletion!(.noData)
        }
        
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber > 0 ? UIApplication.shared.applicationIconBadgeNumber - 1 : 0
    }
    
    // MARK: Navigation
    func topViewController() -> UIViewController? {
        return rootNavController()?.topViewController
    }
    
    func rootNavController() -> BIANavigationController? {
        return self.window?.rootViewController as? BIANavigationController
    }
    
    func showNetworkDialog(for statusCode: Int) {
        var message = "Something went wrong connecting to Blingy. Try again"
        var title = "Network Error"
        switch statusCode {
        case -1001:
            title = "Slow Network"
            message = "It looks like you're on a slow network. We were unable to connect."
        case 401:
            message = "Something is wrong with your user credentials. Try again."
            title = "Uh Oh!"
            break
        case 0:
            message = "You are not connected to the internet!"
            title = "No Internet"
        default: break
        }
        let alert = SIAAlertController(title: title, message: message)
        alert?.addAction(SIAAlertAction(title: "OK", style: .cancel, handler: nil))
        self.window?.rootViewController?.present(alert!, animated: true, completion: nil)
    }
    
    func refreshUser(completion: ((_ userIsSet: Bool)-> Void)? = nil) {
        guard (facebookIsInitialized && BIALoginManager.userIsRegistered() && BIALoginManager.sharedInstance().myUser == nil)  else {
            completion?(!BIALoginManager.userIsRegistered() && BIALoginManager.sharedInstance().myUser != nil)
            return
        }
        
        SIAUser.getWithId(nil, completionHandler: { (user: SIAUser?) in
            BIALoginManager.sharedInstance().myUser = user
            completion?(true)
        })
    }
    
    func showRateTheApp() {
        let vc = RateTheAppViewController.create()
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        VideoPlayerManager.shared.player.pause()
        VideoPlayerManager.shared.shouldPlay = false
        rootNavController()?.present(vc, animated: true, completion: nil)
    }
}

