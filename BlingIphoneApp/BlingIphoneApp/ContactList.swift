//
//  ContactEmails.swift
//  BlingIphoneApp
//
//  Created by Zach on 23/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class ContactList: NSObject {
    var contact_emails = [String]()
    var facebook_ids = [String]()
    var twitter_ids = [Int]()
    
    init(emails: [String]) {
        contact_emails = emails
    }
    
    init(facebookIDs: [String]) {
        facebook_ids = facebookIDs
    }
    
    init(twitterIDs: [Int]) {
        twitter_ids = twitterIDs
    }
    
    static func objectMapping() -> (RKObjectMapping) {
        let mapping = RKObjectMapping(for: ContactList.self)
        mapping!.addAttributeMappings(from: ["contact_emails":"contact_emails",
                                             "facebook_ids":"facebook_ids",
                                             "twitter_ids":"twitter_ids",])
        return mapping!.inverse()
    }
}
