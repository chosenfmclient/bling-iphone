//
//  MusicVideoCell.swift
//  ChosenIphoneApp
//
//  Created by Hen Levy on 22/06/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

class MusicVideoCell: UITableViewCell {
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var thumbHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var songDataButton: UIButton!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectButton.bia_rounded()
    }
}
