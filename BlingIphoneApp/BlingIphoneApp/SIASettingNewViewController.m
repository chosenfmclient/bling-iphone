//
//  SIASettingNewViewController.m
//  ChosenIphoneApp
//
//  Created by Roni Shoham on 20/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIASettingNewViewController.h"
#import "SIASettingsDataModel.h"
#import <RestKit/RestKit.h>

#define SIA_TABLE_ABOUT_SECTION (4)
#define SETTINGS_PATH @"api/v1/settings"
#define SETTINGS_PATH_WITH_TOKEN @"/api/v1/settings?access_token=%@"
#define EMAIL_NOTIFICATION @"email_notifications"
#define PUSH_NOTIFICATION @"push_notifications"
#define HIGH_QUALITY_WIFI @"high_quality_wifi_only"
#define ACCESS_TOKEN @"access_token"
#define SETTINGS_TITLE @"Settings"

@interface SIASettingNewViewController ( )

@property (strong, nonatomic) NSArray *data;
@property (weak, nonatomic) IBOutlet UISwitch *playTutorialSwitch;
@property (nonatomic) BOOL sizeChanged;
@end

@implementation SIASettingNewViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _appVersionLabel.text = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"];
    [(BIANavigationController *)self.navigationController setNavigationBarLeftButtonBack];
    [(BIANavigationController *)self.navigationController setNavigationItemTitle:SETTINGS_TITLE];
    self.tableView.superview.frame = CGRectMake(self.tableView.superview.frame.origin.x,
                                                self.tableView.superview.frame.origin.y,
                                                SCREEN_WIDTH,
                                                SCREEN_HEIGHT - self.tableView.superview.frame.origin.y);
    
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)]]; // remove seperator line after last row
    
    [self setFont];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.scrollEnabled = NO;
    [self.saveRecordsAutomaticallySwitch setOn:((NSNumber*)[[NSUserDefaults standardUserDefaults] objectForKey:SAVE_TO_CAMERA_ROLL]).boolValue];
  //  [self.playTutorialSwitch setOn:((NSNumber*)[[NSUserDefaults standardUserDefaults] objectForKey:PLAY_TUTORIAL]).boolValue];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
 //   [Flurry logEvent:FLURRY_VIEW_SETTINGS];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
//    [view setBackgroundColor:SingrDirtyWhite];
//    
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 2, tableView.frame.size.width, 27)];
//    [label setText:@"About"];
//    [label setFont:[UIFont fontWithName:@"Roboto-Medium" size:12.0f]];
//    [label setTextColor:SingrDarkGray];
//    [view addSubview:label];
//
//    return view;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *event;

    if([segue.identifier isEqualToString:@"privacy"])
    {
       // event = FLURRY_TAP_PRIVACY_SETTINGS;
    }
    else
    {
       /// event = FLURRY_TAP_T_C_SETTINGS;
    }

    //[Flurry logEvent:event];
}

- (void)setFont
{
    UIFont *font = [UIFont fontWithName:@"Roboto-Regular" size:16.0];

    [_privacy setFont:font];
    [_privacy setTextColor:SingrGray];
    
    [_termsConditions setFont:font];
    [_termsConditions setTextColor:SingrGray];

    [_appVersion setFont:font];
    [_appVersion setTextColor:SingrGray];
    
    [_saveRecordsAutomaticallyLabel setFont:font];
    [_saveRecordsAutomaticallyLabel setTextColor:SingrGray];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (IBAction)saveRecordsAutomaticallyOnOff:(UISwitch *)sender
{
    [[NSUserDefaults standardUserDefaults]setObject:@(sender.isOn) forKey:SAVE_TO_CAMERA_ROLL];
}

- (IBAction)playTutorialSwitchDidChange:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults]setObject:@(sender.isOn) forKey:PLAY_TUTORIAL];
}


@end
