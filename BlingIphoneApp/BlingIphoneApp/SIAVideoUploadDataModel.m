//
//  SIAVideoUploadDataModel.m
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 8/17/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAVideoUploadDataModel.h"

@implementation SIAVideoUploadDataModel

+(RKObjectMapping *)objectMapping{
    
    NSDictionary *responseMappingDictionary = @{@"thumbnail.method" : @"thumbnail_method",
                                                @"thumbnail.content_type" : @"thumbnail_content_type",
                                                @"thumbnail.signed_url" : @"thumbnail_signed_url",
                                                @"url.method" : @"video_method",
                                                @"url.content_type" :@"video_content_type",
                                                @"url.signed_url" : @"video_signed_url",
                                                @"video_id" : @"video_id",
                                                @"soundtrack_url":@"soundtrack_url"};
    
    
    
    
    RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[SIAVideoUploadDataModel class]];
    [responseMapping addAttributeMappingsFromDictionary:responseMappingDictionary];
    return responseMapping;
}

@end
