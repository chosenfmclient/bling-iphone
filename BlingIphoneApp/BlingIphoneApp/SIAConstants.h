//
//  SIAConstants.h
//  SingrIphoneApp
//
//  Created by Lior Lasry on 26/02/13.
//  Copyright (c) 2013 Lior Lasry. All rights reserved.
//
#import <UIKit/UIKit.h>

#ifndef _SIA_CONSTANTS_H
#define _SIA_CONSTANTS_H

/* Blingy User Info */
FOUNDATION_EXPORT NSString * const kChosenUserKeyStr;
FOUNDATION_EXPORT NSString * const kChosenUserIdKeyStr;
FOUNDATION_EXPORT NSString * const kChosenUserNameKeyStr;
FOUNDATION_EXPORT NSString * const kChosenUserFirstNameKeyStr;
FOUNDATION_EXPORT NSString * const kChosenUserLastNameKeyStr;

/* Facebook User info */
FOUNDATION_EXPORT NSString * const kFBAppId;
FOUNDATION_EXPORT NSString * const kFBUserIdKeyStr;
FOUNDATION_EXPORT NSString * const kFBAccessTokenKeyStr;
FOUNDATION_EXPORT NSString * const kAccessTokenKeyStr;
FOUNDATION_EXPORT NSString * const kAccessTokenDate;
FOUNDATION_EXPORT NSString * const kLoginType;
FOUNDATION_EXPORT NSString * const kFacebookLoginType;
FOUNDATION_EXPORT NSString * const kEmailLoginType;
FOUNDATION_EXPORT NSString * const kAnonymousLoginType;
FOUNDATION_EXPORT NSString * const kSignInViewDismissed;
FOUNDATION_EXPORT NSString * const kFBUserNameKeyStr;

FOUNDATION_EXPORT NSString * const kSave2CameraRoll;
FOUNDATION_EXPORT NSString * const kFirstTimeSave2CameraRoll;

/* General */
FOUNDATION_EXPORT NSString * const kChosenVideoKeyStr;
FOUNDATION_EXPORT NSString * const kChosenProfileKeyStr;
FOUNDATION_EXPORT NSString * const kChosenGameKeyStr;
FOUNDATION_EXPORT NSString * const kTracksKeyStr;
FOUNDATION_EXPORT NSString * const kChosenBadgeKeyStr;
FOUNDATION_EXPORT NSString * const kUserIdKeyStr;
FOUNDATION_EXPORT NSString * const kContentIdKeyStr;
FOUNDATION_EXPORT NSString * const kGameIdKeyStr;
FOUNDATION_EXPORT NSString * const kVideoIdKeyStr;
FOUNDATION_EXPORT NSString * const kShareIdKeyStr;
FOUNDATION_EXPORT NSString * const kNotificationIdKeyStr;
FOUNDATION_EXPORT NSString * const kActivityIdKeyStr;
FOUNDATION_EXPORT NSString * const kSongIdKeyStr;
FOUNDATION_EXPORT NSString * const kTypeKeyStr;
FOUNDATION_EXPORT NSString * const kUrlKeyStr;
FOUNDATION_EXPORT NSString * const kImageKeyStr;
FOUNDATION_EXPORT NSString * const kLinkKeyStr;
FOUNDATION_EXPORT NSString * const kMyVideoKeyStr;
FOUNDATION_EXPORT NSString * const kIsSelectedIntroKeyStr;
FOUNDATION_EXPORT NSString * const kNewKeyStr;
FOUNDATION_EXPORT NSString * const kPromotedKeyStr;
FOUNDATION_EXPORT NSString * const kVideoPointsKeyStr;
FOUNDATION_EXPORT NSString * const kAlbumKeyStr;
FOUNDATION_EXPORT NSString * const kArtistKeyStr;
FOUNDATION_EXPORT NSString * const kScoreKeyStr;
FOUNDATION_EXPORT NSString * const kLikesKeyStr;
FOUNDATION_EXPORT NSString * const kCommentsKeyStr;
FOUNDATION_EXPORT NSString * const kSongNameKeyStr;
FOUNDATION_EXPORT NSString * const kPlayingKeyStr;
FOUNDATION_EXPORT NSString * const kNextKeyStr;
FOUNDATION_EXPORT NSString * const kSkipKeyStr;
FOUNDATION_EXPORT NSString * const kVoteKeyStr;
FOUNDATION_EXPORT NSString * const kPersonalBoardAction;
FOUNDATION_EXPORT NSString * const kUserNotificationAction;
FOUNDATION_EXPORT NSString * const kAppDelegateOpenURLNotification;
FOUNDATION_EXPORT NSString * const kRecordTypeDanceOff;
FOUNDATION_EXPORT NSString * const kRecordTypeBio;
FOUNDATION_EXPORT NSString * const kRecordTypeReview;
FOUNDATION_EXPORT NSString * const kApiPageStr;

/* Video Player */
// Asset keys
FOUNDATION_EXPORT NSString * const kTracksKey;
FOUNDATION_EXPORT NSString * const kPlayableKey;
// PlayerItem keys
FOUNDATION_EXPORT NSString * const kStatusKey;
FOUNDATION_EXPORT NSString * const kBufferEmptyKey;
FOUNDATION_EXPORT NSString * const kLikelyToKeepUpKey;
//FOUNDATION_EXPORT NSString * const kBufferLoadedKey;
// AVPlayer keys
FOUNDATION_EXPORT NSString * const kRateKey;
FOUNDATION_EXPORT NSString * const kCurrentItemKey;

/* intro audition type */
FOUNDATION_EXPORT NSInteger const kUserVideoIntroType;
FOUNDATION_EXPORT NSInteger const kUserVideoAuditionType;


/* custom colors */
FOUNDATION_EXPORT UIColor * SingrGreen;
FOUNDATION_EXPORT UIColor * SingrYellow;
FOUNDATION_EXPORT UIColor * SingrPink;
FOUNDATION_EXPORT UIColor * SingrBlue;
FOUNDATION_EXPORT UIColor * SingrRed;
FOUNDATION_EXPORT UIColor * SingrGray;
FOUNDATION_EXPORT UIColor * SingrDarkGray;
FOUNDATION_EXPORT UIColor * SingrDirtyWhite;
FOUNDATION_EXPORT UIColor * ChosenLightBlue;

//instructions
FOUNDATION_EXPORT NSString * const kRecordHereInstruction;
FOUNDATION_EXPORT NSString * const kLookHereInstruction;
FOUNDATION_EXPORT NSString * const kPrepareMusicInstruction;
FOUNDATION_EXPORT NSString * const kPerformFeedbackInstruction;
FOUNDATION_EXPORT NSString * const kTimerInstruction;
FOUNDATION_EXPORT NSString * const kLyricsHereInstruction;
FOUNDATION_EXPORT NSString * const kFrameYourShotInstruction;
FOUNDATION_EXPORT NSString * const kTextInstruction;
FOUNDATION_EXPORT NSString * const kQuestionInstruction;

//share methods
FOUNDATION_EXPORT NSString * const kFacebookShareMethod;
FOUNDATION_EXPORT NSString * const kTwitterShareMethod;
FOUNDATION_EXPORT NSString * const kInstagramShareMethod;
FOUNDATION_EXPORT NSString * const kSMSShareMethod;
FOUNDATION_EXPORT NSString * const kEmailShareMethod;
FOUNDATION_EXPORT NSString * const kEllentubeShareMethod;

/* custom fonts */
#define kFontOpenSans(x)            [UIFont fontWithName:@"OpenSans"        size: (x) ]
#define kFontOpenSansItalic(x)      [UIFont fontWithName:@"OpenSans-Italic" size: (x) ]
#define kFontOpenSansBold(x)        [UIFont fontWithName:@"OpenSans-Bold"   size: (x) ]
#define kFontOpenSansBoldItalic(x)  [UIFont fontWithName:@"OpenSans-BoldItalic"   size: (x) ]
#define kFontCoconOTBold(x)         [UIFont fontWithName:@"CoconOT-Bold"    size: (x) ]
#define kAppFontRegular(x)          [UIFont fontWithName:@"Roboto-Regular"    size: (x) ]
#define kAppFontItalic(x)           [UIFont fontWithName:@"Roboto-Italic"    size: (x) ]
#define kAppFontMediumItalic(x)     [UIFont fontWithName:@"Roboto-MediumItalic"    size: (x) ]
#define kAppFontBold(x)             [UIFont fontWithName:@"Roboto-Bold"    size: (x) ]
#define kAppFontBoldItalic(x)       [UIFont fontWithName:@"Roboto-BoldItalic"    size: (x) ]
#define kAppFontLight(x)            [UIFont fontWithName:@"Roboto-Light"    size: (x) ]
#define kAppFontLightItalic(x)      [UIFont fontWithName:@"Roboto-LightItalic"    size: (x) ]
#define kAppFontBlack(x)            [UIFont fontWithName:@"Roboto-Black"    size: (x) ]
#define kAppFontBlackItalic(x)      [UIFont fontWithName:@"Roboto-BlackItalic"    size: (x) ]
#define kAppFontMedium(x)           [UIFont fontWithName:@"Roboto-Medium"    size: (x) ]
#define kAppFontThin(x)             [UIFont fontWithName:@"Roboto-Thin"    size: (x) ]
#define kAppFontThinItalic(x)       [UIFont fontWithName:@"Roboto-ThinItalic"    size: (x) ]











/* Server API Defines */
#define API_VERSION_NUMBER                      @"1.18"
#define SERVER_API_VERSION_PARAM                @"&v=" API_VERSION_NUMBER
#define API_VERSION_KEY_OBJECT                  @"v":API_VERSION_NUMBER
#define GAME_ID_KEY                             @"game_id"
#define WINNING_VIDEO_ID_KEY                    @"selected"
#define HOME_SCREEN_API_URI                     @"api/v1/home"
#define HOME_SCREEN_WITH_TOKEN                  @"api/v1/home?access_token=%@" SERVER_API_VERSION_PARAM
#define TOP_ATALENTS_API_URI                    @"api/v1/top-talents"
#define STAR_SPOTTING_API_URI                   @"api/v1/games/star-spotting"
#define STAR_SPOTTING_WITH_TOKEN                @"/api/v1/games/star-spotting?access_token=%@" ## SERVER_API_VERSION_PARAM
#define JUDGE_THE_JUDGE_API_URI                 @"api/v1/games/judge-the-judge"
#define JUDGE_THE_JUDGE_WITH_TOKEN              @"/api/v1/games/judge-the-judge?access_token=%@" ## SERVER_API_VERSION_PARAM


/* General */
#define ORIGINAL_UPLOAD_TYPE                    @"original_upload"
#define COVER_UPLOAD_TYPE                       @"cover_upload"

#define CURRENT_USER_ID             [[NSUserDefaults standardUserDefaults] objectForKey: USER_ID_KEY_STR]
#define CURRENT_USER_FULL_NAME      [[[[NSUserDefaults standardUserDefaults] objectForKey:kChosenUserFirstNameKeyStr] stringByAppendingString:@" "] stringByAppendingString:[[NSUserDefaults standardUserDefaults] objectForKey:kChosenUserLastNameKeyStr]]


/* New Relic */

///* Flurry */
//#define FLURRY_OPEN_APP_EVENT                   @"100" /* @"Open App" */
//#define FLURRY_LOGIN_EVENT                      @"110" /* @"Login" */
//#define FLURRY_RESIGN_ACTIVE_EVENT              @"111" /* @"App closed" */
//
//// Networking
//#define FLURRY_CHECK_NO_NETWORK_ERROR_EVENT     @"1030" /* On network check, no network was discovered */
//
//// Onboarding
//#define FLURRY_ONBOARD_COMPLETED                @"120" /* @"Onboarding completed" */
//#define FLURRY_ONBOARD__EVENT                   @"121" /* define this... */
//
////Find friends
//#define FLURRY_FING_FRIENDS_PANEL_RENDER_EVENT  @"130" /* @"find friends panel render" */
//#define FLURRY_TWITTER_EVENT                    @"131" /* @"twitter" */
//#define FLURRY_FACEBOOK_EVENT                   @"132" /* @"facebook" */
//#define FLURRY_CONTACTS_EVENT                   @"133" /* @"contacts" */
//
//// Create perf video - instrument each step by performance type
//#define FLURRY_SAVE_AUDITION_EVENT              @"140" /* @"save as audition" */
//#define FLURRY_PUBLISH_VIDEO_EVENT              @"141" /* @"publish perf video" */
//
//// Sharing
//#define FLURRY_SHARE_OWN_PERFORMANCE_EVENT      @"150" /* @"Share Own performance" */
//#define FLURRY_SHARE_OTHERS_PERFORMACE_EVENT    @"151" /* @"Share Others performance" */
//#define FLURRY_SHARE_BADGE_EVENT                @"152" /* @"Share Badge" */
//#define FLURRY_SHARE_PROFILE_EVENT              @"153" /* @"Share Profile" */
//#define FLURRY_SHARE_OTHERS_CRITIQUE_EVENT      @"154" /* @"Share Others Critique" */
//#define FLURRY_SHARE_OWN_CRITIQUE_EVENT         @"155" /* @"Share Own Critique" */
//// sharing errors
//#define FLURRY_SHARE_BADGE_FB_ERROR_EVENT       @"1500" /* @"Sharing Bagde to FB Failed" */
//#define FLURRY_SHARE_BADGE_TWITTER_ERROR_EVENT  @"1501" /* @"Sharing Bagde to Twitter Failed" */
//
//// Video Critique
//#define FLURRY_START_CRITIQUE_EVENT             @"160" /* @"Start Critique" */
//#define FLURRY_FINALIZE_CRITIQUE_EVENT          @"161" /* @"Finalize critique" */
//
//// Nav
//#define FLURRY_NAV_PANEL_RENDER_EVENT           @"170" /* @"Nav Panel Render" */
//
//// Review and Respond Panel Render
//#define FLURRY_RESP_REVIEW_PANEL_RENDER_EVENT   @"185" /* @"Review and Respond Panel Render" */
//
//// Self-profile
//#define FLURRY_CREATE_BIO_VIDEO_EVENT           @"190" /* @"'Create bio video' button was pressed" */
//#define FLURRY_UPLOAD_PHOTO_EVENT               @"191" /* @"Upload profile photo" */
//#define FLURRY_EDIT_TEXT_BIO_EVENT              @"192" /* @"Edit text bio" */
//
//#define FLURRY_UPLOAD_PHOTO_ERROR_EVENT         @"1901" /* @"Upload profile photo failed" */
//#define FLURRY_EDIT_TEXT_BIO_ERROR_EVENT        @"1902" /* @"Edit text bio failed" */
//
//// Notifications
//#define FLURRY_NOTIFICATIONS_PAGE_RENDER_EVENT  @"200" /* @"Notifications Page render" */
//#define FLURRY_NOTIFICATION_USER_CLICK_EVENT    @"201" /* @"User Notification click" */
//#define FLURRY_NOTIFICATION_BADGE_CLICK_EVENT   @"202" /* @"Badge Notification click" */
//#define FLURRY_NOTIFICATION_VIDEO_CLICK_EVENT   @"203" /* @"Video Notification click" */
//
//// Following
//// Follow user(s) by type (judge vs. perf) from what panel
////  (game, post game, view & respond panel, profile, etc.)
//#define FLURRY_FOLLOW_JUDGE_FROM_GAME           @"300" /* @"Follow Judge from Game" */
//#define FLURRY_FOLLOW_JUDGE_FROM_POST_GAME      @"301" /* @"Follow Judge from Post Game" */
//#define FLURRY_FOLLOW_JUDGE_FROM_R_A_R_PANEL    @"302" /* @"Follow Judge from R&R Panel" */
//#define FLURRY_FOLLOW_JUDGE_FROM_PROFILE        @"303" /* @"Follow Judge from Profile" */
//
//#define FLURRY_FOLLOW_PERF_FROM_STARSOPT        @"304" /* @"Follow Performer from StarSpotting" */
//#define FLURRY_FOLLOW_PERF_FROM_JUDGE           @"305" /* @"Follow Performer from JudgeTheJudge" */
//#define FLURRY_FOLLOW_PERF_FROM_POST_GAME       @"306" /* @"Follow Performer from Post Game" */
//#define FLURRY_FOLLOW_PERF_FROM_R_A_R_PANEL     @"307" /* @"Follow Performer from R&R Panel" */
//#define FLURRY_FOLLOW_PERF_FROM_PROFILE         @"308" /* @"Follow Performer from Profile" */
//
//// Games
//#define FLURRY_START_STARSPOT_GAME_EVENT        @"401" /* @"Start StarSpotting Game" */
//#define FLURRY_END_STARSPOT_GAME_EVENT          @"403" /* @"End StarSpotting Game" */
//#define FLURRY_STARSPOT_GAME_NEXT_ROUND_EVENT   @"405" /* @"StarSpotting Next Round" */
//#define FLURRY_STARSPOT_GAME_NEW_GAME_EVENT     @"406" /* @"StarSpotting New Game" */
//
//#define FLURRY_START_JUDGE_GAME_EVENT           @"402" /* @"Start Judge the Judge Game" */
//#define FLURRY_END_JUDGE_GAME_EVENT             @"404" /* @"End Judge the Judge Game" */
//#define FLURRY_JUDGE_GAME_NEXT_ROUND_EVENT      @"407" /* @"Judge the Judge Next Round" */
//#define FLURRY_JUDGE_GAME_NEW_GAME_EVENT        @"408" /* @"Judge the Judge New Game" */
//
//#define FLURRY_START_STARSPOT_GAME_ERROR_EVENT  @"4002" /* Star Spotting Start Game Failed */
//#define FLURRY_START_JUDGE_GAME_ERROR_EVENT     @"4003" /* Star Spotting Start Game Failed */
//
//// Review
//#define FLURRY_REWATCH_CLIP_BY_STARSPOT_EVENT   @"410" /* @"Re-watch clip by StarSpotting" */
//#define FLURRY_REWATCH_CLIP_BY_JUDGE_EVENT      @"411" /* @"Re-watch clip by JudgeTheJudge" */
//
//// Swipe
//#define FLURRY_SWIPE_STARSPOT_EVENT             @"420" /* @"Swipe in StarSpotting" */
//#define FLURRY_SWIPE_JUDGE_EVENT                @"421" /* @"Swipe in JudgeTheJudge" */
//#define SONG_TIME_KEY_STR                       @"songTime"
//
//#define FLURRY_RESTKIT_FAILURE                  @"511" /* @"Rest kit failure" */
//
//// APNS
//#define FLURRY_APNS_OPEN_PERSONAL_NOTIFICATION  @"600"
//#define FLURRY_APNS_OPEN_MASS_NOTIFICATION      @"601"

// Ellen Show Events //

/* FLURRY 
 format for defines:
 FLURRY_{EVENT TYPE}_{EVENT TARGET}_{SCREEN}
 
 */

// home
//#define FLURRY_VIEW_HOME                  @"pageView:home"
//#define FLURRY_TAP_GAME_HOME              @"tap:game:home"
//#define FLURRY_TAP_VIDEO_HOME             @"tap:video:home"
//#define FLURRY_TAP_MENU_HOME              @"tap:menu:home"
//#define FLURRY_TAP_NOTIFICATIONS_HOME     @"tap:notifications:home"
//#define FLURRY_TAP_RECORD_HOME            @"tap:record:home"
//
//// notifications
//
//#define FLURRY_VIEW_NOTIFICATIONS              @"pageView:notifications"
//#define FLURRY_TAP_NOTIFICATION_NOTIFICATIONS  @"tap:notification:notifications"
//
//
//
//
//
//
//
//
//
//#define FLURRY_LEADERBOARD_VIEW_EVENT           @"pageView:leaderboard"
//#define FLURRY_PROFILE_VIEW_EVENT               @"pageView:profile"
//#define FLURRY_RECORD_VIEW_EVENT                @"pageView:record"
//#define FLURRY_VIDEO_HOME_PAGE_VIEW_EVENT       @"pageView:videopage"
//#define FLURRY_WDIB_GAME_VIEW_EVENT             @"pageView:game:WDIB"
//#define FLURRY_DANCE_OFF_GAME_VIEW_EVENT        @"pageView:game:DO"




// render
// Full 15 second clip render vs. total number of clips viewed.
// ...

/* system version */
#define SYSTEM_VERSION_IS_IOS7 ([[[UIDevice currentDevice] systemVersion] intValue] == 7)
#define SYSTEM_VERSION_IS_IOS8 ([[[UIDevice currentDevice] systemVersion] intValue] == 8)
#define SYSTEM_VERSION_GREATER_THAN_IOS8 ([[[UIDevice currentDevice] systemVersion] intValue] >= 8)
#define SYSTEM_VERSION_10_OR_GREATER ([[[UIDevice currentDevice] systemVersion] intValue] >= 10)

/* device model */

#define IPAD ([[UIDevice currentDevice].model isEqualToString:@"iPad"])

/* screen size*/
#define SCREEN_WIDTH  ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)

/* app delegate */

#define APP_DELEGATE (AppDelegate *)[[UIApplication sharedApplication]delegate]

/*orientation*/

#define PORTRAIT_STRING       @"portrait"
#define RIGHT_LANDSCAPE_STRING @"right_landscape"
#define LEFT_LANDSCAPE_STRING  @"left_landscape"

/* recording types */

#define KARAOKE_TYPE            @"karaoke"
#define RESPONSE_TYPE           @"response"
#define LIPSYNC_TYPE            @"lipsync"
#define UPLOAD_TYPE             @"upload"
#define COVER_FREESTYLE_TYPE    @"cover_freestyle"
#define ORIGINAL_FREESTYLE_TYPE @"original_freestyle"
#define SAVE_TO_CAMERA_ROLL @"save_to_camera_roll"
#define PLAY_TUTORIAL @"play_tutorial"

/* onboarding phases */

#define ONBOARDING_PHASE_KEY @"onboarding_phase"
#define INTRO_PHASE           1
#define STAR_SPOTTING_PHASE   2
#define TALENT_SCOUT_PHASE    3
#define RESPONSE_VIEW_PHASE   4
#define SHOW_RESPONSE_PHASE   5
#define RESPONSE_PHASE        6
#define NEXT_ROUND_PHASE      7
#define JTJ_PHASE             8
#define CHOOSE_JUDGE_PHASE    9
#define HOME_PHASE            10
#define FINISHED_PHASE        0

#define COACHMARK_LINE_SPACE  19

/* Home screen onboarding */
#define HOME_SCREEN_ONBOARDING_KEY @"home_screen_onboarding"
#define HOME_SCREEN_CHALLENGE_ONBOARDING_KEY @"home_screen_challenge_onboarding"
#define HOME_SCREEN_CHALLENGE_ONBOARDING_WAS_SHOWN_KEY @"home_screen_challenge_was_shown"

/* WDB onboarding */
#define SWIPE_GAME_VOTES_COUNT_KEY  @"swipe_game_votes_count"
#define STAR_SPOTTED_ONBOARDING_KEY @"star_spotted_onboarding"
#define BONUS_TIME_ONBOARDING_KEY   @"bonus_time_onboarding"
#define WHO_DID_BEST_ONBOARDING_KEY @"who_did_best_onboarding"
#define VIDEO_LEADERBOARD_ONBOARDING_KEY @"video_leaderboard_onboarding"
/* notifications settings */
#define REGISTER_FOR_NOTIFICATIONS @"user_notifications"
/* games id's */

#define STS_GAME_ID     (@"1")
#define JTJ_GAME_ID     (@"2")
#define TSC_GAME_ID     (@"3")
#define TSC_RES_GAME_ID (@"4")

/* karaoke file details */

#define KARAOKE_FILE_SCLIENT_TIME (7.)

/* slow network handling */

#define SERVER_REQUEST_TIMEOUT (20)
#define SLOW_NETWORK_MESSAGE @"It looks like you're on a slow connection. Try again later."
#define SLOW_NETWORK_TITLE @"Slow Network"
#define SLOW_CONNECTION_HANDLER [[[UIAlertView alloc] initWithTitle:SLOW_NETWORK_TITLE message:SLOW_NETWORK_MESSAGE delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];

#define RECORDED_FILE_PATH [NSTemporaryDirectory() stringByAppendingString:@"record_1.mov"]

/* Twitter Share */
#define TWITTER_TITLE               @"TWITTER"

#define TWITTER_NO_ACCOUNT_MESSAGE  NSLocalizedString(@"There are no Twitter accounts configured on this device. You can add or create a Twitter account in the device Settings.", nil)
#define TWITTER_NO_ACCOUNT_TITLE    NSLocalizedString(@"No Twitter Accounts", nil)

#define TWITTER_NO_ACCESS_TITLE     @"Access Denied"
#define TWITTER_NO_ACCESS_MESSAGE   @"Blingy access for Twitter was denied. You can grant access to Twitter in the device Settings."

/* free space checking */
#define BYTES_IN_MEGABYTE (1024 * 1024)
#define NOT_ENOUGH_SPACE_MESSAGE_TITLE @"Cannot record video"
#define NOT_ENOUGH_SPACE_MESSAGE_BODY @"There is not enough available temporary storage to record video. You can manage your storage in Settings"
#define IOS_SAFETY_SPACE (200 * BYTES_IN_MEGABYTE)


#endif // SIA_CONSTANTS_H
