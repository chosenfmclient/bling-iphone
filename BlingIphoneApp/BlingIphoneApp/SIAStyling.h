//
//  SIAStyling.h
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/3/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIAStyling : NSObject

+ (instancetype)defaultStyle;

+ (void)view:(UIView*)view withRoundedCorners:(UIRectCorner)corners radii:(CGSize)radii;
+ (UIImage *)imageWithColor:(UIColor *)color;

- (UIColor*)blingMainColor;
- (UIColor*)blingModeColor;

- (UIColor*)registrationFieldPlaceholderColor;
- (UIColor*)registrationFieldTextColor;

- (UIColor *)recordingRedColor;
- (UIColor *)recordingOptionsButtonsRed;
- (UIColor *)recordingModeHighlightColor;

- (UIColor *)cameoPink;
- (UIColor *)cameoNotificationPink;

- (void)textField:(UITextField*)textField withRegistrationStyleAndImage:(UIImage*)image;


- (UIImage *)imageFromView:(UIView*)view;
- (UIImage*)blurredImageFromView:(UIView *)view;
- (UIImage*)blurredImageFromImage:(UIImage*)image;
- (UIImage*)blurredImageFromImage:(UIImage*)image withRadius:(CGFloat)radius;
- (UIColor *)averageColorFromImage:(UIImage*) image;
@end

@interface UIView (SIAStyling)
- (void)spinWithDuration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
@end
