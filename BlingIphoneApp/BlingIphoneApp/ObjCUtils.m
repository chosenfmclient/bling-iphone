//
//  ObjCUtils.m
//  BlingIphoneApp
//
//  Created by Michael Biehl on 3/14/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

#import "ObjCUtils.h"

@implementation ObjCUtils

+ (BOOL)catchException:(void(^)())tryBlock error:(__autoreleasing NSError **)error {
    @try {
        tryBlock();
        return YES;
    }
    @catch (NSException *exception) {
        *error = [[NSError alloc] initWithDomain:exception.name code:0 userInfo:exception.userInfo];
    }
}


@end
