//
//  SIACountdownView.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/4/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIACountdownView : UIView

- (id)initWithFrame:(CGRect)frame
              count:(NSUInteger)count;
- (void) countdownWithHandler:(void(^)(NSUInteger currentCount))count;
- (void) stopCountdown;
@end
