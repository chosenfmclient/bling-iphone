//
//  UIColor+HTMLColor.h
//  ChosenIphoneApp
//
//  Created by Joseph Nahmias on 01/03/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HTMLColor)

+ (UIColor *) colorFromHexString:(NSString *)hexString ;

@end
