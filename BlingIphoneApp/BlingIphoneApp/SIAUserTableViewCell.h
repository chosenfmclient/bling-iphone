//
//  SIAFollowingTableViewCell.h
//  ChosenIphoneApp
//
//  abstract : for customise cell
//
//  Created by Roni Shoham on 09/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIAUserTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *notificationFirstLine;
@property (weak, nonatomic) IBOutlet UILabel *notificationSecondLine;
@property (weak, nonatomic) IBOutlet UILabel *timeStamp;

@end
