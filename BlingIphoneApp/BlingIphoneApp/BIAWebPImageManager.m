//
//  BIAWebPImageManager.m
//  BlingIphoneApp
//
//  Created by Zach on 22/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import "BIAWebPImageManager.h"
#import <SDWebImage/SDWebImageDownloader.h>
#import <YYImage/YYImage.h>
#import "NSString+MD5.h"

@interface BIAWebPImageManager()
@property (strong, nonatomic) NSCache* dataCache;
@property (strong, nonatomic) NSString* cacheDirPath;
@property (strong, nonatomic) NSMutableArray<NSString*>* pendingImageURLs;
@end

@implementation BIAWebPImageManager

+ (id)sharedManager {
    static BIAWebPImageManager *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.queuedReuqests = [NSMutableArray array];
        self.isLoadingImage = false;
        self.dataCache = [[NSCache alloc] init];
        self.dataCache.countLimit = 100;
        self.dataCache.totalCostLimit = 10 * 1024 * 1024; // 5 MB
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        self.cacheDirPath = [paths objectAtIndex:0];
        BOOL isDir = NO;
        NSError *error;
        if (![[NSFileManager defaultManager] fileExistsAtPath:self.cacheDirPath isDirectory:&isDir] && isDir == NO) {
            
            [[NSFileManager defaultManager] createDirectoryAtPath:self.cacheDirPath withIntermediateDirectories:NO attributes:nil error:&error];
        }
    }
    return self;
}

-(void)getImageForURL:(NSURL *)url withCompletion: (void (^)(UIImage*))completion {
    
    if ([self cacheExistsInMemoryForImageAt:url]) {
        UIImage *image = [self.dataCache objectForKey:[self cacheKeyFrom:url]];
        [Utils dispatchOnMainThread:^{
            completion(image);
        }];
        return;
    }
    
    [Utils dispatchInBackground:^{
        if ([self cacheExistsOnDiskForDataAt:url]) {
            NSData *imageData = [NSData dataWithContentsOfFile:[self.cacheDirPath stringByAppendingString:[self cacheKeyFrom:url]]];
            [self constructImageFromData:imageData fromURL:url withCompletion:^(UIImage *image) {
                if (image) {
                    completion(image);
                }
            }];
        } else {
            NSURLSession *sess = [NSURLSession sharedSession];
            NSURLSessionDataTask * dataTask = [sess dataTaskWithURL:url completionHandler:^(NSData *imageData, NSURLResponse *response, NSError *error) {
                if (error || !imageData) {
                    return;
                }
                [self saveDataToDisk:imageData fromURL:url];
                [self constructImageFromData:imageData fromURL:url withCompletion:^(UIImage *image) {
                    if (image) {
                        completion(image);
                    }
                }];
                
            }];
            
            [dataTask resume];
        }
    }];
    
}

-(void)constructImageFromData:(NSData*) imageData fromURL: (NSURL*) url withCompletion: (void (^)(UIImage*))completion {
    NSTimeInterval totalDur = 0;
    NSMutableArray* frames = [NSMutableArray array];
    YYImageDecoder *decoder = [YYImageDecoder decoderWithData:imageData scale:2.0];
    
    for (int i = 0; i < decoder.frameCount; i++) {
        YYImageFrame* yyframe = [decoder frameAtIndex:i decodeForDisplay:YES];
        totalDur += yyframe.duration;
        UIImage* frame = yyframe.image;
        if (frame) {
            [frames addObject:frame];
        } else {
            NSLog(@"failed to add frame");
        }
    }
    
    UIImage *finalImage = [UIImage animatedImageWithImages:frames duration:totalDur];
    
    if (!finalImage) {
        completion(nil);
    }
    
    [Utils dispatchOnMainThread:^{
        if (finalImage != nil)
            [self.dataCache setObject:finalImage forKey:[self cacheKeyFrom:url] cost:imageData.length];
        completion(finalImage);
    }];

}

-(BOOL)cacheExistsInMemoryForImageAt:(NSURL*)url {
    if ([self.dataCache objectForKey:[self cacheKeyFrom:url]]) {
        return YES;
    }
    return NO;
}

-(BOOL)cacheExistsOnDiskForDataAt:(NSURL*)url {
    return [[NSFileManager defaultManager] fileExistsAtPath:[self.cacheDirPath stringByAppendingString:[self cacheKeyFrom:url]]];
}

-(NSString*)cacheKeyFrom:(NSURL *) url {
    return [url.absoluteString MD5String];
}

-(void)saveDataToDisk:(NSData *)data fromURL:(NSURL*)url {
    NSString* path = [self.cacheDirPath stringByAppendingString:[self cacheKeyFrom:url]];
    [[NSFileManager defaultManager] createFileAtPath:path contents:data attributes:nil];
}

@end


@implementation BIAWebPImageRequest
@end
