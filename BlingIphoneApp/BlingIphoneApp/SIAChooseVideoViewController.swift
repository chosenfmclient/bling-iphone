//
//  ChooseVideoViewController.swift
//  ChosenIphoneApp
//
//  Created by Hen Levy on 22/06/2016.
//  Copyright © 2016 SingrFM. All rights reserved.
//

let kScreenSize = UIScreen.main.bounds.size
let kScreenScale = UIScreen.main.scale

import RestKit
import SDWebImage


class SIAChooseVideoViewController: SIABaseViewController,
UITableViewDataSource,
UITableViewDelegate,
DiscoverSearchBarViewDelegate,
VideoPlayerManagerDelegate
{
    @IBOutlet weak var searchBar: DiscoverSearchBarView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var videosTableView: UITableView!
    @IBOutlet weak var spinner: SIASpinner!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var noResultsView: UIView!
    
    let cellIdentifier = "MusicVideoCell"
    let playerItemKeyPath = "status"
    var musicVideos = [MusicVideo]()
    var thumbHeight: CGFloat = 0
    let songInfoHeight: CGFloat = 65
    var didSetup = false
    var currentMusicVideo: MusicVideo?
    var selectedMusicVideo: MusicVideo?
    var currentPlayingTime: CMTime?
    var search: String?
    var oneTimeAppear: (() -> Void)?
    var currentQuery: String?
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
        if (search == nil){
            search = ""
        } else {
            oneTimeAppear = { [weak self] in
                guard let _ = self else { return }
                self!.searchBar.setSearchText(self!.search!)
                self!.oneTimeAppear = nil
            }
        }
        searchQuery(search!)
        AmplitudeEvents.log(event: "record:addvideo:pageview")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        thumbHeight = UIScreen.main.bounds.width / (4/3)
        //            videosTableView.reloadData()
        
        guard let _ = navigationController else { return }
        videosTableView.contentInset = UIEdgeInsets(top: blurView.frame.maxY,
                                                    left: 0,
                                                    bottom: navigationController.bottomBar.bounds.size.height,
                                                    right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController.setBottomBarHidden(false, animated: true)
        navigationController.setSelected(true, forBottomButton: .recordButton)
        navigationController.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        oneTimeAppear?()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        stopPlaying()
    }
    
    func setupNavBar() {
//        navigationController.clearNavigationItemRightButton()
        navigationController.setNavigationItemTitle("Music Video")
    }
    
    // MARK: Search
    
    func cancelWasTapped() {
        searchQuery("")
    }
    
    func searchFieldWasEntered() {
        
    }
    
    func searchWasCleared() {
        searchQuery("")
    }
    
    func searchQuery(_ query: String) {
        guard query != currentQuery else {
            return
        }
        stopPlaying()
        spinner.startAnimation()
        videosTableView.isHidden = true
        noResultsView.isHidden = true
        musicVideos.removeAll()
        videosTableView.reloadData()
        var term: String?
        if !query.isEmpty {
            term = query
        }
        currentQuery = term
        ChooseVideoModel.getBlingVideos(searchTerm: term) {[weak self] (data: [MusicVideo]?, term: String?) -> (Void) in
            guard self?.currentQuery == term else {
                return
            }
            guard let videos = data else {
                Utils.dispatchOnMainThread {[weak self] in
                    self?.noResultsView.isHidden = false
                    self?.spinner.stopAnimation()
                }
                return
            }
            self?.musicVideos = videos
            Utils.dispatchOnMainThread {[weak self] in
                self?.spinner.stopAnimation()
                self?.videosTableView.reloadData()
                self?.videosTableView.isHidden = false;
            }
        }
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return musicVideos.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath as IndexPath) as! MusicVideoCell
        let musicVideo = musicVideos[indexPath.row]
        
        cell.previewImageView.image = nil
        cell.backgroundImageView.image = nil
        cell.songDataButton.tag = indexPath.row
        
        if let newThumbURL = musicVideo.correctSizeThumb(for: thumbHeight) {
            musicVideo.correctSizeThumbURL = newThumbURL
            cell.previewImageView.sd_setImage(with:(newThumbURL as URL),
                                              placeholderImage: UIImage(named: "GrayPlaceholder"),
                                              options:SDWebImageOptions.refreshCached,
                                              completed:{(image: UIImage?, error: Error?, cacheType: SDImageCacheType, imageURL: URL?) in
                                                cell.backgroundImageView.image = image
                                                if indexPath.row == 0 && self.backgroundImageView.image == nil {
                                                    Utils.dispatchOnMainThread {[weak self] in
                                                        self?.backgroundImageView.image = image;
                                                        self?.backgroundImageView.blur()
                                                    }
                                                }
            })
        }
        cell.artistNameLabel.text = musicVideo.artistName
        cell.trackNameLabel.text = musicVideo.trackName
        cell.playButton.alpha = 1.0
        cell.thumbHeightConstraint.constant = thumbHeight
        
        
        if (musicVideo != currentMusicVideo) {
            if (VideoPlayerManager.shared.currentPlayerView == cell.previewImageView) {
                VideoPlayerManager.shared.removePlayerView()
            }
            cell.playButton.setImage(UIImage(named: "video_play"), for: .normal)
        }
        else {
            cell.playButton.setImage(VideoPlayerManager.shared.isLoading ? nil : UIImage(named: "video_play"), for: .normal)
            VideoPlayerManager.shared.shouldPlay = false
            VideoPlayerManager.shared.setNewPlayerView(view: cell.previewImageView,
                                                       videoURL: musicVideo.previewUrl as URL,
                                                       withDelegate: self)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return thumbHeight + songInfoHeight
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard musicVideos.count > indexPath.row else { return }
        
        if currentMusicVideo == musicVideos[indexPath.row] {
            VideoPlayerManager.shared.removeVideo()
        }
    }
    
    // MARK: Actions
    
    @IBAction func playPauseDidTap(sender: UIButton) {
        if let strongSearchBar = searchBar , strongSearchBar.canResignFirstResponder {
            strongSearchBar.resignFirstResponder()
        }
        
        guard let cell = sender.superview?.superview as? MusicVideoCell,
        let indexPath = videosTableView.indexPath(for: cell) else {
            return
        }
        let musicVideo = musicVideos[indexPath.row]
        
        if let _ = currentMusicVideo, musicVideo != currentMusicVideo && currentMusicVideo!.isPlaying {
            if let currentPlayingIndex = musicVideos.index(of: currentMusicVideo!) {
                pause(musicVideo: currentMusicVideo!,
                      cell: videosTableView.cellForRow(at: IndexPath(row: currentPlayingIndex, section: 0)) as! MusicVideoCell?)
            }
        }
        
        if !musicVideo.isPlaying {
            play(musicVideo: musicVideo, cell: cell)
            sender.setImage(nil, for: .normal)
        } else {
            pause(musicVideo: musicVideo, cell: cell)
        }
    }
    
    func play(musicVideo: MusicVideo, cell: MusicVideoCell!) {
        if (musicVideo != currentMusicVideo) {
            currentMusicVideo = musicVideo
            currentMusicVideo?.isPlaying = true
            VideoPlayerManager.shared.setNewPlayerView(view: cell.previewImageView,
                                                       videoURL: musicVideo.previewUrl as URL,
                                                       withDelegate: self)
        }
        else if VideoPlayerManager.shared.playerIsInsideTopViewController() {
            musicVideo.isPlaying = true
            VideoPlayerManager.shared.playerView.isHidden = false
            VideoPlayerManager.shared.player.play()
        }
    }
    
    func playerDidPlay() {
    }
    
    func playerDidReachEnd() {
        stopPlaying()
    }
    
    func pause(musicVideo: MusicVideo, cell: MusicVideoCell?) {
        if let _ = cell {
            cell!.playButton.setImage(UIImage(named: "video_play.png"), for: .normal)
            cell!.playButton.alpha = 1.0
        }
        musicVideo.isPlaying = false
        VideoPlayerManager.shared.shouldPlay = false
        VideoPlayerManager.shared.spinner?.stopAnimation()
        VideoPlayerManager.shared.player.pause()
    }
    
    @IBAction func selectMusicVideoDidTap(sender: UIButton) {
        if let strongSearchBar = searchBar , strongSearchBar.canResignFirstResponder {
            strongSearchBar.resignFirstResponder()
        }
        guard let cell = sender.superview?.superview?.superview as? MusicVideoCell,
            let indexPath = videosTableView.indexPath(for: cell) else {
                return
        }
        VideoPlayerManager.shared.removeVideo()
        stopPlaying()
        selectedMusicVideo = musicVideos[indexPath.row]
        performSegue(withIdentifier: "BlingRecordSegue", sender: nil)
    }


    func handleTarget(button: UIButton!, action: Selector!) {
        if button.allTargets.contains(self) {
            button.removeTarget(self, action: action, for: .touchUpInside)
        }
        button.addTarget(self, action: action, for: .touchUpInside)
    }
    
    func stopPlaying() {
        currentMusicVideo?.isPlaying = false
        VideoPlayerManager.shared.removeVideo()
        guard let lastPlayed = currentMusicVideo else {
            return
        }
        let index = musicVideos.index( where: { $0.trackId == lastPlayed.trackId } )
        guard let lastPlayedIndex = index,
            let cell = videosTableView.cellForRow(at: IndexPath(row: lastPlayedIndex, section: 0)) as? MusicVideoCell else {
                return
        }
        cell.playButton.setImage(UIImage(named: "video_play"), for: .normal)
    }
    
    // MARK: UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let strongSearchBar = searchBar , strongSearchBar.canResignFirstResponder {
            let _ = strongSearchBar.resignFirstResponder()
        }
    }
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SIABlingRecordViewController {
            destination.musicVideo = selectedMusicVideo
        }
        
        if let discoverVC = segue.destination as? DiscoverViewController {
            guard let index = (sender as? UIButton)?.tag else {
                return
            }
            discoverVC.featuredClip = musicVideos[index]
        }
    }
}
