//
//  CameraPermissionSegue.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 2/15/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

import UIKit

class CameraPermissionSegue: UIStoryboardSegue {

    override func perform() {
        
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { (granted) in
            Utils.dispatchOnMainThread {
                //guard let _ = self else { return }
                if granted {
                    self.superPerform()
                    (self.source.navigationController as? BIANavigationController)?.setBottomBarHidden(true, animated: true)
                }
                else {
                    self.showAccessAlert()
                    (self.source as! SIABaseViewController).segueWasCancelled()
                }
            }
        }
    }
    
    private func superPerform() {
        super.perform()
    }
    
    private func showAccessAlert() {
        let alert = UIAlertController(title: "Error",
                                      message: "We are unable to access your camera. If you denied permission to use the camera, please re-enable it by going to Settings -> Privacy -> Camera and tapping the switch for Blin.gy, then tap Retry.")
        let okAction = UIAlertAction(title: "OK",
                                     style: .cancel)
        alert.addAction(okAction)
        source.present(alert,
                       animated: true,
                       completion: nil)
        
    }
}
