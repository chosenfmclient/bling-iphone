//
//  BIAPhotoPickerHandler.h
//  BlingIphoneApp
//
//  Created by Zach on 19/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol BIAPhotoPickerDelegate <NSObject>
@required
-(void) presentPickerViewController: (UIViewController*) vcToPresent;
-(void) photoWasPicked: (UIImage*) image;
@end

@interface BIAPhotoPickerHandler : NSObject <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (weak, nonatomic) id<BIAPhotoPickerDelegate> delegate;
- (void) getChoiceControllerIfPermissionIsGrantedWithCompletion: (void (^)(UIAlertController* alertController))completion;
@end
