//
//  SIAEmailRegistrationdataModel.h
//  ChosenIphoneApp
//
//  Created by Ohad Landau on 4/14/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SIAEmailRegistrationdataModel : NSObject

@property (strong, nonatomic) NSString *first_name;
@property (strong, nonatomic) NSString *last_name;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *device_id;
@property (strong, nonatomic) NSString *app_id;
@property (strong, nonatomic) NSString *secret;
@property (strong, nonatomic) NSString *access_token;
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *expires_in;

+(RKObjectMapping *)objectMapping;

@end


