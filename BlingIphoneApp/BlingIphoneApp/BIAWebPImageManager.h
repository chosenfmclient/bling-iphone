//
//  BIAWebPImageManager.h
//  BlingIphoneApp
//
//  Created by Zach on 22/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface BIAWebPImageRequest : NSObject
@property (nonatomic, strong) void (^completion)(UIImage*);
@property (strong, nonatomic) NSURL* url;
@end

@interface BIAWebPImageManager : NSObject
@property (nonatomic) BOOL isLoadingImage;
@property (strong, nonatomic) NSMutableArray<BIAWebPImageRequest*>* queuedReuqests;

+(BIAWebPImageManager *)sharedManager;
-(void)getImageForURL:(NSURL *)url withCompletion: (void (^)(UIImage*))completion;
@end
