//
//  BIAPhotoPickerHandler.m
//  BlingIphoneApp
//
//  Created by Zach on 19/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import "BIAPhotoPickerHandler.h"


@implementation BIAPhotoPickerHandler
- (void) getChoiceControllerIfPermissionIsGrantedWithCompletion: (void (^)(UIAlertController* alertController))completion {
//    [Flurry logEvent:FLURRY_TAP_EDITPHOTO_PROFILE_EDIT];
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if (authStatus == AVAuthorizationStatusAuthorized) {
        [Utils dispatchOnMainThread:^{
            completion([self getChoiceActionSheet]);
        }];
    }
    else if (authStatus == AVAuthorizationStatusDenied) {
        [Utils dispatchOnMainThread:^{
            [BIAPhotoPickerHandler presentCameraPermissionsError];
        }];
    }
    else if (authStatus == AVAuthorizationStatusNotDetermined) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo
                                 completionHandler:^void(BOOL granted) {
                                     [Utils dispatchOnMainThread:^{
                                         if (granted) {
                                             completion([self getChoiceActionSheet]);
                                         } else {
                                             [BIAPhotoPickerHandler presentCameraPermissionsError];
                                         }
                                     }];
                                 }];
    }
}

- (UIAlertController*) getChoiceActionSheet {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add Profile Photo" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alertController addAction:[UIAlertAction  actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        //take photo
        UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
        
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imgPicker.cameraDevice     = UIImagePickerControllerCameraDeviceFront;
        
        imgPicker.delegate = self;
        
        [self.delegate presentPickerViewController:imgPicker];
    }]];
    
    [alertController addAction:[UIAlertAction  actionWithTitle:@"Choose Existing Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        //choose photo
        UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
        imgPicker.allowsEditing = NO;
        imgPicker.delegate = self;
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self.delegate presentPickerViewController:imgPicker];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    return alertController;
}

+(void) presentCameraPermissionsError {
    [Utils dispatchOnMainThread:^{
        SIAAlertController *alert = [SIAAlertController alertControllerWithTitle:@"Permissions error" message:@"There was an error accessing your Camera. Please go to your iOS privacy tab under settings and accept permissions for Blingy app to use the Camera."];
        alert.actionSize = CGSizeMake(220, 52);
        [alert addAction:[SIAAlertAction actionWithTitle:@"OK" style:SIAAlertActionStyleCancel handler:nil]];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    }];
}


- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    //Crop the image to a square
    CGSize imageSize = image.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    if (width != height) {
        CGFloat newDimension = MIN(width, height);
        CGFloat widthOffset = (width - newDimension) / 2;
        CGFloat heightOffset = (height - newDimension) / 2;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDimension, newDimension), NO, 0.);
        [image drawAtPoint:CGPointMake(-widthOffset, -heightOffset)
                 blendMode:kCGBlendModeCopy
                     alpha:1.];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    if (image.imageOrientation != UIImageOrientationUp)
    {
        UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
        [image drawInRect:(CGRect){0, 0, image.size}];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    // resize image
    CGSize newSize = CGSizeMake(640.0, 640.0); //CGSizeMake(200.0f, 200.0f);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:(CGRect){0, 0, newSize.width, newSize.height}];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    
    [self.delegate photoWasPicked:image];
}
@end
