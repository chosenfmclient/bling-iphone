//
//  SIATableViewDataModel.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 6/7/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SIATableViewDataModelType) {
    SIATableViewDataModelTypeUser,
    SIATableViewDataModelTypeVideo,
    SIATableViewDataModelTypeBadge,
    SIATableViewDataModelTypeGame
};

@interface SIAPaginatorDataModel : NSObject

@property (strong, nonatomic) NSMutableDictionary *pageDict;
@property (strong, nonatomic) NSArray *pageArray;
@property (strong, nonatomic) NSNumber *pageSize;
@property (strong, nonatomic) NSNumber *totalObjects;
@property (nonatomic) NSNumber *currentPage;
@property (strong, nonatomic) NSMutableDictionary *resultsDict;

// fetches the current page and adds it to the pageDict dictionary. Key = page #, value = pageArray
- (void) getPageForPath:(NSString *)path
          requestParams:(NSDictionary *)additionalParams
  withCompletionHandler:(void(^)(id model))handler
           errorHandler:(void(^)(NSError *error))errorHandler;

- (void) getNextPageWithHandler:(void(^)(id model))handler; // subclass this method in model class

+ (RKMapping *) objectMapping;
+ (RKMapping *) pageMapping;
+ (RKMapping *) responseMappingForTotalKey:(NSString *)totalKey
                             objectMapping:(RKMapping *)objectMapping
                                modelClass:(Class)modelClass;
- (NSString *)pagePath;
+ (NSString *)pathPattern; // for restkit
- (NSDictionary *)requestParams;
- (id) initWithObjectId: (NSString*) objectId;
@end
