//
//  EditProfileViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 19/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

@objc protocol ProfileWasEditedDelegate: class {
    func profileWasEdited()
}

@objc class EditProfileViewController: SIABaseViewController, ProfileWasEditedDelegate, BIAPhotoPickerDelegate {

    //MARK: -- Outlets
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var firstNameField: EditProfileTextField!
    @IBOutlet weak var lastNameField: EditProfileTextField!
    @IBOutlet weak var youtubeField: EditProfileTextField!
    @IBOutlet weak var twitterField: EditProfileTextField!
    @IBOutlet weak var instagramField: EditProfileTextField!
    @IBOutlet weak var facebookField: EditProfileTextField!
    
    @IBOutlet weak var saveBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var addProfilePhotoButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK: -- Properties
    weak var user: SIAUser?
    var photoPickerHandler = BIAPhotoPickerHandler()

    var photoImage: UIImage?
    var dataEdited = false
    
    weak var delegate: ProfileWasEditedDelegate?
    var activeTextField: UITextField?
    
    //MARK: -- View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let user = user else {
            assertionFailure("You must pass a user object to edit profile")
            return
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        photoPickerHandler.delegate = self
        
        //init text fields
        firstNameField.textField.placeholder = "First Name"
        firstNameField.textField.autocapitalizationType = .words
        firstNameField.icon.image = UIImage(named: "icon_register_field_user")
        firstNameField.textField.text = user.first_name
        firstNameField.delegate = self
        
        lastNameField.textField.placeholder = "Last Name"
        lastNameField.textField.autocapitalizationType = .words
        lastNameField.icon.image = UIImage(named: "icon_register_field_user")
        lastNameField.textField.text = user.last_name
        lastNameField.delegate = self

        twitterField.textField.placeholder = "Twitter Username"
        twitterField.icon.image = UIImage(named: "icon_social_twitter_grey")
        twitterField.textField.text = user.twitter_handle
        twitterField.delegate = self

        instagramField.textField.placeholder = "Instagram Username"
        instagramField.icon.image = UIImage(named: "icon_social_instagram_grey")
        instagramField.textField.text = user.instagram_handle
        instagramField.delegate = self

        youtubeField.textField.placeholder = "Full Channel / User URL"
        youtubeField.icon.image = UIImage(named: "icon_social_youtube_grey")
        youtubeField.textField.text = user.youtube_handle
        youtubeField.delegate = self

        facebookField.textField.placeholder = "Facebook Username"
        facebookField.icon.image = UIImage(named: "icon_social_facebook_grey")
        facebookField.textField.text = user.facebook_handle
        facebookField.delegate = self
        
        //init profile picture
        if let image = user.image {
            profileImageView.image = image
        } else {
            profileImageView.sd_setImage(with: user.imageURL, placeholderImage: UIImage(named: "extra_large_userpic_placeholder")?.withRenderingMode(.alwaysTemplate))
        }
        profileImageView.tintColor = UIColor(colorLiteralRed: 39 / 255, green: 167 / 255, blue: 222 / 255 , alpha: 1)
        navigationController?.setNavigationItemTitle("Edit Profile")
        
        AmplitudeEvents.log(event: "editprofile:pageview")
    }
    
    func backEvent() {
        AmplitudeEvents.log(event: "editprofile:cancel")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.title = "Edit Profile"
        navigationController?.setNavigationBarLeftButtonXWithAction(#selector(dismissIfUnchanged))
        navigationController?.setBottomBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationItem.rightBarButtonItem = saveBarButtonItem
    }
    
    override func viewDidLayoutSubviews() {
        profileImageView.bia_rounded()
    }
    
    //MARK: -- Navigation
    func dismissIfUnchanged() {
        if dataEdited || photoImage != nil {
            let alert = UIAlertController(title: "Unsaved changes", message: "You have unsaved changes. Would you like to save them?")
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {[weak self] (action) in
                self?.saveBarButtonWasTapped(0 as AnyObject)
                let _ = self?.navigationController.popViewController(animated: true)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { [weak self] (action) in
                let _ = self?.navigationController.popViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    func presentPickerViewController(_ vcToPresent: UIViewController!) {
        present(vcToPresent, animated: true, completion: nil)
    }
    
    func photoWasPicked(_ image: UIImage!) {
        dismiss(animated: true, completion: nil)
        guard let _ = image else { return }
        profileImageView.image = image
        photoImage = image
    }
    
    func profileWasEdited() {
        dataEdited = true
    }

    //MARK: -- Button Actions
    @IBAction func addProfilePhotoButtonWasTapped(_ sender: AnyObject) {
        photoPickerHandler.getChoiceControllerIfPermissionIsGranted {[weak self] (alertController) in
            guard let alert = alertController else {
                return
            }
            alert.popoverPresentationController?.sourceView = self?.addProfilePhotoButton
            self?.present(alert, animated: true, completion: nil)
            AmplitudeEvents.log(event: "editprofile:editpic")
        }
    }
    
    @IBAction func saveBarButtonWasTapped(_ sender: AnyObject) {
        //Validate name
        guard let firstName = firstNameField?.textField.text, firstNameIsValid(firstName)
            else {
                displayFirstNameError()
                return
        }
        
        if let youtubeLink = youtubeField?.textField.text, youtubeLink.characters.count > 0 {
            if !youtubeLinkIsValid(youtubeLink) {
                displayYoutubeLinkError()
                return
            }
        }
        
        delegate?.profileWasEdited()
        if dataEdited {
            dataEdited = false
            updateUserData()
        }
        if let image = photoImage {
            uploadPhoto(image);
            user?.image = image
            photoImage = nil
        }
        AmplitudeEvents.log(event: "editprofile:submit")
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: -- First name validation
    func firstNameIsValid(_ name: String) -> Bool {
        return name.trimmingCharacters(in: .whitespaces).characters.count > 1
    }
    
    func displayFirstNameError() {
        let alert = UIAlertController(
            title: "Uh Oh!",
            message: "You must have a first name. It should be 2 or more characters long")
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: Youtube link validation
    func youtubeLinkIsValid(_ link: String) -> Bool {
        let url = URL(string: link)
        
        if let host = url?.host, let pathComponents = url?.pathComponents {
            return (host == "www.youtube.com" || host == "youtube.com" || host == "m.youtube.com") &&
                (pathComponents.contains("user") || pathComponents.contains("channel"))
        }
        return false
    }
    
    func displayYoutubeLinkError() {
        let alert = UIAlertController(
            title: "Uh Oh!",
            message: "Your YouTube link isn't valid. Please make sure it has the format \n\"http://www.youtube.com/user\" or \n\"http://www.youtube.com/channel\".")
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: -- API Requests
    func updateUserData() {
        guard let user = user else {
            return
        }
        
        user.first_name = firstNameField.textField.text
        user.last_name = lastNameField.textField.text
        user.facebook_handle = facebookField.textField.text
        user.twitter_handle = twitterField.textField.text
        user.instagram_handle = instagramField.textField.text
        user.youtube_handle = youtubeField.textField.text
        
        let requestModel = UserRequestModel(
            firstName: user.first_name,
            lastName: user.last_name,
            facebookHandle: user.facebook_handle,
            youtubeHandle: user.youtube_handle,
            instagramHandle: user.instagram_handle,
            twitterHandle: user.twitter_handle
        )
        
        RequestManager.putRequest(
            "api/v1/users/me",
            object: requestModel,
            queryParameters: nil,
            success: {(res, req) in
                BIALoginManager.sharedInstance().myUser = user.copy() as? SIAUser
            },
            failure: {[weak self] (req, error) in
                self?.profileWasEdited()
            })
    }
    
    func uploadPhoto(_ image: UIImage)  {
        AmplitudeEvents.log(event: "editprofile:picsubmit")
        RequestManager.postRequest(
            "api/v1/users/me/photo",
            object: UserPhotoRequestModel(),
            queryParameters: nil,
            success: { (req, res) in
                guard let metaData = res?.firstObject as? SIAUploadUserImage else {
                    return
                }
                guard let imageData = UIImageJPEGRepresentation(image, 1) else {
                    return
                }
                SIAUploadVideo.sharedUploadManager().uploadData(
                    imageData,
                    toSignedUrl: metaData.image_signed_url,
                    withMethod: metaData.image_method,
                    contentType: metaData.image_content_type,
                    withProgress: false,
                    withThumbnailProgress: false,
                    completionHandler: { (👌) in
                    if (👌) {
                        print("image upload success")
                        RequestManager.putRequest(
                            "api/v1/users/me/photo",
                            object: nil,
                            queryParameters: nil,
                            success: { (req, res) in
                                print("new profile picture success! \\|oO|/")
                            },
                            failure: {[weak self] (req, error) in
                                self?.profileWasEdited()
                            })
                    }
                })
            },
            failure: {[weak self] (req, error) in
                self?.profileWasEdited()
            })
    }
    
    //Raise fields to be visisble when editing
    func keyboardWillShow(_ notification: NSNotification) {
        guard let info = notification.userInfo else { return }
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        scrollView.contentInset = UIEdgeInsets(
            top: 0,
            left: 0,
            bottom: keyboardFrame.height,
            right: 0
        )
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets()
    }
}

//MARK: -- Text fields
class EditProfileTextField: UIView, UITextFieldDelegate {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var textField: UITextField!
    weak var delegate: ProfileWasEditedDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let view = Bundle.main.loadNibNamed("EditProfileTextField", owner: self, options: nil)?[0] as! UIView
        
        self.addSubview(view)
        view.frame = self.bounds
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        delegate?.profileWasEdited()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
