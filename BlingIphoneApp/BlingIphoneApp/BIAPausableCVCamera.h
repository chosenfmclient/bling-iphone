//
//  BIAPausableCVCamera.h
//  BlingIphoneApp
//
//  Created by Michael Biehl on 10/29/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

#import <opencv2/videoio/cap_ios.h>

@protocol BIAPausableCVCameraDelegate <NSObject>

- (void)recordingDidStart;
- (void)backgroundPlaybackDidStartWithTimeOffset:(CMTime)offset;
- (void)recordingDidStop;
- (void)recorderReadyToRecord;
- (void)recorderDidFailToInitialize:(NSError  * _Nullable)error;

@end

@protocol BIACvVideoCameraDelegate <NSObject>

- (void)processImage:(cv::Mat&)image
              atTime:(CMTime)time;

@end

@interface BIAPausableCVCamera : CvVideoCamera

@property (readonly, nonatomic) BOOL isPaused;
@property (readonly, nonatomic) BOOL isRecording;
@property (strong, nonatomic) NSURL *recordURL;
@property (weak, nonatomic) AVPlayer *player;
@property (nonatomic) Float64 recordingSpeedScale;
@property (weak, nonatomic) id <BIAPausableCVCameraDelegate> recordDelegate;
@property (weak, nonatomic) id <BIACvVideoCameraDelegate> cameraDelegate;
@property (readonly, nonatomic) AVCaptureDevice *captureDevice;
@property (readonly, nonatomic) float exposureTargetBias;
@property (readonly, nonatomic) float exposureTargetOffset;
@property (readonly, nonatomic) double exposureDuration;
@property (readonly, nonatomic) float iso;
@property (readonly, nonatomic) AVCaptureWhiteBalanceGains deviceWhiteBalanceGains;
@property (readonly, nonatomic) AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTintValues;
@property (readonly, nonatomic) AVCaptureWhiteBalanceChromaticityValues chromaticityValues;
@property (readonly, nonatomic) float currentFrameBrightness;


- (void)startRecording;
- (void)pauseRecording;
- (void)resumeRecording;
- (void)stopRecording;
- (void)cancelRecording;
- (void)reset;
- (void)reset:(BOOL)resetCaptureSession;

- (void)lockCurrentDeviceAutoAdjustment;
- (void)unlockCurrentDeviceAutoAdjustment;
- (void)adjustExposureWithCompletion:(void (^)(void))completion;
@end
