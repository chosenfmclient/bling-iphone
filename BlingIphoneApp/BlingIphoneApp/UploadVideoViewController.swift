//
//  UploadVideoViewController.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 11/1/16.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class UploadVideoViewController: SIABaseViewController, SIAUploadVideoDelegate, VideoPlayerManagerDelegate {

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var videoPlaybackView: UIView!
    @IBOutlet weak var spinner: SIASpinner!
    @IBOutlet weak var uploadingIndicatorView: UIView!
    
    
    var recordingItem: SIARecordingItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SIAUploadVideo.sharedUploadManager().delegate = self
        guard let _ = recordingItem else { return }
        recordingItem?.prepareForUpload(completionHandler: {[weak recordingItem] (finished) in
            if (finished) {
                recordingItem?.startUploadVideo(convertHandler:nil,
                    uploadHandler: {[weak self] (finished) in
                        if let image = self?.backgroundImageView.image {
                            recordingItem?.uploadThumbnail(image)
                        }
                })
            }
        })
        
        
        VideoPlayerManager.shared.setNewPlayerView(view: videoPlaybackView,
                                                   videoURL: (recordingItem?.recordURL)!,
                                                   withDelegate: self)
        VideoPlayerManager.shared.player.repeatPlayback()
        navigationController.setBottomBarHidden(true, animated: true)
    }
    
    func playerDidReachEnd() {
        
    }
    
    func newAssetWasCreated(_ asset: AVAsset) {
        generateThumbnail(asset)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        spinner.startAnimation()
        navigationController.setNavigationItemTitle("Review")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        VideoPlayerManager.shared.player.stopRepeatingPlayback()
        VideoPlayerManager.shared.removeVideo()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let videoHomeVC = segue.destination as? VideoHomeViewController {
            videoHomeVC.videoId = recordingItem?.uploader.dataModel.video_id
        }
        VideoPlayerManager.shared.removeVideo()
    }

    func generateThumbnail(_ asset: AVAsset!) {
        Utils.dispatchInBackground {
            let generator = AVAssetImageGenerator.init(asset: asset)
            generator.appliesPreferredTrackTransform = true
            let background = UIImage(cgImage: try! generator.copyCGImage(at: kCMTimeZero, actualTime: nil))
            Utils.dispatchOnMainThread { [weak self] in
                self?.backgroundImageView.image = background
                self?.backgroundImageView.blur()
                
                guard let _ = self, let _ = self!.recordingItem else { return }
                if self!.recordingItem!.finishedUploadingVideo {
                    self!.recordingItem!.uploadThumbnail(self!.backgroundImageView.image)
                }
            }
            
        }
    }
    
    func finishUploading() {
        fadeOutView(uploadingIndicatorView)
        
        let alertController = UIAlertController(title: "Thanks!",
                                                message: "We'll check out your video. With any luck, we'll be able to make it available to record on Blingy!")
        
        alertController.addAction(UIAlertAction(title: "OK",
                                                style: .cancel,
                                                handler: {[weak self] (action) in
                                                    self?.performSegue(withIdentifier: "videoHome",
                                                                       sender: nil)
            }))
        present(alertController,
                animated: true,
                completion: nil)
    }
    
    func updateUploadProgress(_ percentString: String!) {
        
    }
}
