//
//  FollowViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 30/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

protocol PushProfileDelegate: class {
    func pushProfile(_ userID: String)
}

class FollowViewController: SIABaseViewController, FollowedDelegate, UITableViewDelegate, UITableViewDataSource, SearchableUserList, PageRequestHandlerDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var topSectionView: UIView!
    @IBOutlet weak var followAllButton: UIButton!
    @IBOutlet weak var totalContactsLabel: UILabel!
    @IBOutlet weak var contactsTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    //MARK: properties
    var friendsOnBling = [SIAUser?]()
    var filteredFriends = [SIAUser?]()
    var contactHandler = ContactsHandler()
    var isRegistration = true
    var pageRequestHandler: PageRequestHandler?
    weak var delegate: PushProfileDelegate?
    weak var flaggingDelegate: CameoInviteFlagging?
    weak var cameoInviteDelegate: CameoInvites?
    var isCameoInvite = false {
        didSet {
            if isCameoInvite {
                pageRequestHandler = PageRequestHandler(apiPath: "api/v1/users/me/following",
                                                        delegate: self)
            }
        }
    }
    var pinkOrGreen: String {
        return isCameoInvite ? "pink" : "green"
    }
    
    var currentSearch = ""
    
    //MARK: View controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if isCameoInvite {
            followAllButton.setTitle("+ Invite All", for: .normal)
            followAllButton.backgroundColor = SIAStyling.defaultStyle().cameoPink()
        }
    }
    
    override func viewDidLayoutSubviews() {
        followAllButton?.bia_rounded()
        if !isRegistration {
            topSectionView.backgroundColor = UIColor(colorLiteralRed: 242 / 255, green: 242 / 255, blue: 242 / 255, alpha: 1)
            totalContactsLabel.textColor = UIColor(colorLiteralRed: 128 / 255, green:  128 / 255, blue:  128 / 255, alpha: 1)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table view delegate methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let _ = friendsOnBling[indexPath.row] else {
            return tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "user", for: indexPath) as! FollowContactTableViewCell
        cell.delegate = self
        cell.cameoDelegate = self.cameoInviteDelegate
        cell.isCameoInvite = isCameoInvite
        if let imageURL = filteredFriends[indexPath.row]?.imageURL {
            cell.userImageView.sd_setImage(
                with: imageURL,
                placeholderImage: UIImage(named: "extra_large_userpic_placeholder.png")
            )
        } else {
            cell.userImageView.image = UIImage(named: "extra_large_userpic_placeholder.png")
        }
        if let user = filteredFriends[indexPath.row] {
            cell.userNameLabel.text = user.fullName
            cell.user = user
        }
        
        let imageCondition = isCameoInvite ? cell.user!.wasInvited : cell.user!.followedByMe
        
        let followButtonImage = UIImage(
            named: imageCondition ? "icon_followed_grey" :
                                    "icon_follow_\(pinkOrGreen)"
        )
        
        cell.followButton.setImage(followButtonImage, for: .normal)
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredFriends.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let userID = (tableView.cellForRow(at: indexPath) as? FollowContactTableViewCell)?.user?.user_id {
            delegate?.pushProfile(userID)
        }
    
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let _ = cell as? SIALoadingTableViewCell {
            pageRequestHandler?.fetchData()
        }
    }
    
    func dataWasLoaded(page: Int, data: Any?, requestIdentifier: Any?) {
        guard let users = (data as? Users)?.data, users.count > 0 else {
            pageRequestHandler?.didReachEndOfContent = true
            endOfContentReached(at: page,
                                requestIdentifier: nil)
            return
        }
        
        flaggingDelegate?.userListWasLoaded(users)
        
        if friendsOnBling.count > 0 && friendsOnBling.last! == nil {
            friendsOnBling.removeLast()
        }
        friendsOnBling = friendsOnBling + users
        friendsOnBling.append(nil)
        filteredFriends = friendsOnBling
        if page == 0 {
            finishLoading()
        }
        else {
            contactsTableView.reloadData()
        }
    }
    
    func endOfContentReached(at page: Int, requestIdentifier: Any?) {
        if page == 0 {
            finishLoading()
            followAllButton.isHidden = true
            // no data view
        }
        
        if friendsOnBling.count > 0, friendsOnBling.last! == nil {
            friendsOnBling.removeLast()
            filteredFriends = friendsOnBling
            contactsTableView.reloadData()
        }
    }
    
    //MARK: IBActions
    @IBAction func followAllWasTapped(_ sender: AnyObject) {
        
        guard !isCameoInvite else {
            cameoInviteDelegate?.addUserListToInvitees(friendsOnBling.filter { $0 != nil } as! [SIAUser])
            reloadSearch()
            contactsTableView.reloadData()
           return
        }
        
        var userIds = [String]()
        for user in friendsOnBling where user != nil {
            if !user!.followedByMe {
                userIds.append(user!.user_id)
            }
        }
        guard userIds.count > 0 else { return }
        
        CoreDataManager.shared.saveFollowAll(for: userIds)
        
        RequestManager.postRequest(
            "api/v1/follow",
            object: UserIDList(list: userIds),
            queryParameters: nil,
            success: { (req, res) in
                //wooo - who cares
            },
            failure: {[weak self] (req, error) in
                guard let sSelf = self else { return }
                sSelf.followAllButton.isEnabled = true
                var ids = [String]()
                for user in sSelf.friendsOnBling where user != nil {
                    if userIds.contains(user!.user_id) {
                        ids.append(user!.user_id)
                    }
                    sSelf.contactsTableView.reloadData()
                }
                CoreDataManager.shared.deleteFollowAll(for: ids)
            }
        )
        reloadSearch()
        contactsTableView.reloadData()
    }
    
    //MARK: Contacts
    func getContactsToFollow(_ completion: (() -> Void)? = nil) {
        guard friendsOnBling.count < 1 else {
            completion?()
            return
        }
        Utils.dispatchAsync {[weak self] in
            self?.contactHandler.loadContacts {[weak self] (phoneContacts: [DeviceContact], emailContacts: [DeviceContact], friendsOnBling: [SIAUser]) in
                guard let sSelf = self else { return }
                sSelf.friendsOnBling = friendsOnBling
                sSelf.filteredFriends = friendsOnBling
                sSelf.finishLoading(completion)
                
            }
        }
    }
    
    var dataLoadedClosure: (() -> ())?
    
    func getFollowingForCameoInvite(_ completion: (() -> Void)? = nil) {
        dataLoadedClosure = completion
        pageRequestHandler?.fetchData()
    }
    
    func finishLoading(_ completion: (() -> Void)? = nil) {
        dataLoadedClosure?()
        Utils.dispatchOnMainThread {[weak self] in
            guard let _ = self else { return }
            self!.activityIndicator.stopAnimating()
            self!.topSectionView.isHidden = false
            var numOfContacts: Int?
            if self!.isCameoInvite {
                numOfContacts = BIALoginManager.sharedInstance().myUser.total_following.intValue
            }
            else if let friends = self?.friendsOnBling.filter({ $0 != nil}).count {
                numOfContacts = friends
            }
            else {
                return
            }
            self!.totalContactsLabel.text = String(numOfContacts!) + self!.headerString
            self!.followAllButton.isHidden = numOfContacts! < 1
            self!.contactsTableView.reloadData()
        }
    }
    
    var headerString: String! {
        return isCameoInvite ? " Friends" : " Friends on Blingy"
    }
    
    func contactWasUpdated() {
        reloadSearch()
        self.contactsTableView.reloadData()
    }
    
    func reloadSearch() {
        guard !currentSearch.isEmpty else { return }
        filteredFriends = []
        for user in friendsOnBling where user != nil {
            if user!.fullName.lowercased().contains(currentSearch.lowercased()) {
                filteredFriends.append(user!)
            }
        }
        contactsTableView.reloadData()
    }
    
    func newSearch(_ search: String) {
        guard search != currentSearch else { return }
        guard !search.isEmpty else { return }
        
        currentSearch = search
        reloadSearch()
    }
    
    func searchCanceled() {
        currentSearch = ""
        filteredFriends = friendsOnBling.filter { $0 != nil } as! [SIAUser]
        contactsTableView.reloadData()
    }
}

protocol FollowedDelegate: class {
    func contactWasUpdated() // or unfollowed
}

//MARK: cell class
class FollowContactTableViewCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    weak var delegate: FollowedDelegate?
    weak var cameoDelegate: CameoInvites?
    var isCameoInvite = false
    weak var user: SIAUser?
    
    @IBAction func followButtonWasTapped(_ sender: AnyObject) {
        guard let user = user else { return }
        guard !isCameoInvite else {
            cameoDelegate?.addUserToInvitees(user)
            delegate?.contactWasUpdated()
            return
        }
        
        if user.followedByMe
        {
            user.unfollow(handler: { (💀) in }, withAmplitudePageName: AmplitudeEvents.kFollowersPageName)
        }
        else
        {
            user.follow(handler: { (💀) in }, withAmplitudePageName: AmplitudeEvents.kFollowersPageName)
        }
        
        delegate?.contactWasUpdated()
    }
    
    override func layoutSubviews() {
        userImageView.bia_rounded()
    }
}
