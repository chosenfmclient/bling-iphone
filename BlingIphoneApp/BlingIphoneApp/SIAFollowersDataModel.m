
//
//  SIAFollowersDataModel.m
//  SingrIphoneApp
//
//  Created by Roni Shoham on 06/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAFollowersDataModel.h"

@implementation SIAFollowersDataModel

- (NSString *)pagePath {
    return [NSString stringWithFormat:FOLLOWERS_API, self.userId];
}
+ (RKMapping *)pageMapping {
    return [SIAPaginatorDataModel responseMappingForTotalKey:@"total_objects"
                                               objectMapping:[SIAUser objectMapping]
                                                  modelClass:[SIAFollowersDataModel class]];
}
@end


