//
//  faceDetector.hpp
//  BlingIphoneApp
//
//  Created by Michael Biehl on 1/22/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

#ifndef FaceDetector_h
#define FaceDetector_h

#include <stdio.h>
#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect_c.h"
#include "opencv2/objdetect/objdetect.hpp"


class FaceDetector
{
    public:
    
    // initialize with the path to the cascade xml files
    FaceDetector(cv::String faceCascadePath, cv::String eyeCascadePath);
    virtual ~FaceDetector();
    
    cv::Rect lastFace;
    
    // this updates the lastFace rect from the Mat image passed in - this allows it to be used asynchronously, updating the lastFace rect when the face detection process finishes.
    void getFaceRect(const cv::Mat& image);
    
};

#endif /* FaceDetector_h */

