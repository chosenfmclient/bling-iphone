//
//  PushLeftSegue.swift
//  BlingIphoneApp
//
//  Created by Zach on 19/12/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit

class PushLeftSegue: UIStoryboardSegue {
    
    override func perform() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        
        source.navigationController?.view.layer.add(transition, forKey: nil)
        source.navigationController?.pushViewController(destination, animated: false)
        (source.navigationController as? BIANavigationController)?.shouldPopRight = true
    }
}
