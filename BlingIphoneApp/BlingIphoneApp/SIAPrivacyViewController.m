//
//  SIAPrivacyViewController.m
//  ChosenIphoneApp
//
//  Created by Roni Shoham on 19/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIAPrivacyViewController.h"

@interface SIAPrivacyViewController ( )

@end

@implementation SIAPrivacyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIFont *font = [UIFont fontWithName:@"Roboto-Bold" size:12.0f];
    _privacyText.font = font;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationItemRightButtonTitle:NSLocalizedString(@"Print", nil) target:self action:@selector(print)];
//    NSString *text = _privacyText.text;
//
//    // Defining attributes for the whole text
//    NSDictionary *textAtrributes = @{ NSFontAttributeName : [UIFont fontWithName:@"Roboto-Regular" size:12.0f],
//        NSForegroundColorAttributeName : [UIColor colorWithRed:44.0f / 255.0f green:48.0f / 255.0f blue:63.0f / 255.0f alpha:1] };
//    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:textAtrributes];
//
//    // Defining attributes for the subtiltes
//    NSDictionary *subtitleAttributes = @{ NSFontAttributeName : [UIFont fontWithName:@"Roboto-Bold" size:12.0f],
//        NSUnderlineStyleAttributeName : [NSNumber numberWithInt:NSUnderlineStyleSingle] };
//    NSArray *subtitles = @[ @"Collection of Information", @"Security.", @"Use of Information Collected.", @"Children Under Age of 13.", @"Unsubscribe or Opt-Out.", @"Changes to Privacy Policy Agreement.", @"Acceptance of Terms.", @"How to Contact Us.", @"Email" ];
//    for(id str in subtitles)
//        [string addAttributes:subtitleAttributes range:[text rangeOfString:str]];
//
//    // Defining attributes for the e-mail adresses
//    NSDictionary *adressAttributes = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:35.0f / 255.0f green:130.0f / 255.0f blue:186.0f / 255.0f alpha:1] };
//    NSArray *adresses = @[ @"unsubscribe@chosen.fm", @"privacy@chosen.fm" ];
//    for(id str in adresses)
//        [string addAttributes:adressAttributes range:[text rangeOfString:str]];
//
//    _privacyText.attributedText = string;
//    _privacyText.textAlignment = 0; // left alignment

    _privacyText.showsVerticalScrollIndicator = NO; // disabling scroll bar
}

- (void)viewDidAppear:(BOOL)animated
{
//    [Flurry logEvent:FLURRY_VIEW_SETTINGS_PRIVACY];
}

- (void)print
{
    if(![UIPrintInteractionController isPrintingAvailable])
        return;
    
    NSMutableString *printBody = [NSMutableString stringWithFormat:@"%@", _privacyText.text];

    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    pic.delegate = self;

    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = @"title: ";
    pic.printInfo = printInfo;

    UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc] initWithText:printBody];
    textFormatter.startPage = 0;
    textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
    textFormatter.maximumContentWidth = 6 * 72.0;
    pic.printFormatter = textFormatter;
    pic.showsPageRange = YES;

    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
        if(!completed && error)
        {
            NSLog(@"Printing could not complete because of error: %@", error);
        }
    };

    [pic presentAnimated:YES completionHandler:completionHandler];
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (IBAction)closeWasTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
