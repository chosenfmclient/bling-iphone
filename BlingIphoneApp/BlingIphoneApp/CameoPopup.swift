//
//  CameoPopup.swift
//  BlingIphoneApp
//
//  Created by Michael Biehl on 3/22/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

import UIKit

protocol CameoPopupDelegate: class {
    func cameoPopupWasDismissed(_ popup: CameoPopup?)
    func goToCameoInvite()

}
class CameoPopup: UIView {

    @IBOutlet weak var inviteButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    var bottomConstraint: NSLayoutConstraint?
    var view: UIView!
weak var delegate: CameoPopupDelegate?

    required override init(frame: CGRect) {
        super.init(frame: frame)
        view = (Bundle.main.loadNibNamed("CameoPopup", owner: self, options: nil)![0] as! UIView)
        view?.frame = CGRect(origin: CGPoint(), size: frame.size)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(forView view: UIView,
                     height: CGFloat = 145,
                     delegate: CameoPopupDelegate?) {
        let frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: height)
        self.init(frame: frame)
        self.delegate = delegate
    }
    
    @IBAction func inviteCameo(_ sender: Any) {
        delegate?.goToCameoInvite()
    }
    
    //MARK: UI
    func show(onView: UIView!,
              delegate: CameoPopupDelegate? = nil ,
              height: CGFloat = 145) {
        
        heightAnchor.constraint(equalToConstant: height).isActive = true
        onView.addSubview(self)
        leadingAnchor.constraint(equalTo: onView.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: onView.trailingAnchor).isActive = true
        bottomConstraint = bottomAnchor.constraint(equalTo: onView.bottomAnchor,
                                                   constant: height)
        bottomConstraint?.isActive = true
        onView.setNeedsLayout()
        onView.layoutIfNeeded()
        bottomConstraint?.constant = 0
        onView.setNeedsLayout()
        alpha = 0
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {[weak onView, weak self] in
                        onView?.layoutIfNeeded()
                        self?.alpha = 1
            })
    }
    
    @IBAction func closeButtonWasTapped(_ sender: Any) {
        dismiss()
    }
    
    func dismiss(animated: Bool = true) {
        bottomConstraint?.constant = bounds.height
        UIView.animate(withDuration: animated ? 0.2 : 0.0,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {[weak self] in
                        self?.superview?.layoutIfNeeded()
            }, completion: {[weak self] (completed) in
                self?.removeFromSuperview()
                self?.delegate?.cameoPopupWasDismissed(self)
        })
    }

}
