//
//  NSString+SIAAdditions.m
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/3/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "NSString+SIAAdditions.h"

#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$" //Should contains one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$" //Should contains one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$" //Should contains one or more number
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$" //Should contains one or more symbol

static NSCache *stringCache;

@implementation NSString (SIAAdditions)

- (int)sia_validateWithPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, self.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:self options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = 0;
    
    // Did we find a matching range
    if(matchRange.location != NSNotFound)
        didValidate = 1;
    
    return didValidate;
}

- (SIAPasswordStrengthType)sia_passwordStrength
{
    int strength = 0;
    
    strength += [self sia_validateWithPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    strength += [self sia_validateWithPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    strength += [self sia_validateWithPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    strength += [self sia_validateWithPattern:REGEX_PASSWORD_ONE_SYMBOL caseSensitive:YES];
    
    if(strength <= 1)
    {
        return SIAPasswordStrengthTypeWeak;
    }
    else if(strength == 2)
    {
        return SIAPasswordStrengthTypeModerate;
    }
    else
    {
        return SIAPasswordStrengthTypeStrong;
    }
}

@end

@implementation NSAttributedString (SIAAdditions)

+ (NSAttributedString *)sia_attributedStringWithHTML:(NSString *)html
{
    
    if (!html) return [[NSAttributedString alloc] init];
    
    if(!stringCache)
        stringCache = [[NSCache alloc] init];
    
    NSNumber *key = @([html hash]);
    NSAttributedString *string = [stringCache objectForKey:key];
    if(!string)
    {
        string = [[NSAttributedString alloc] initWithData:[html dataUsingEncoding:NSUTF8StringEncoding]
                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                      NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                 documentAttributes:nil
                                              error:nil];
        if (string && key)
            [stringCache setObject:string forKey:key];
    }
    
    return string;
}

+ (NSAttributedString *)sia_attributedStringWithHTML: (NSString *)html
                                            andColor: (NSString*) hexColor
                                        andBoldColor: (NSString*) hexBoldColor
                                             andFont: (NSString*) fontName
                                         andFontSize: (NSUInteger) fontSize
{
    
    if (!html) return [[NSAttributedString alloc] init];
    
    if(!stringCache)
        stringCache = [[NSCache alloc] init];
    
    
    NSMutableString *mutableText = html.mutableCopy;
    
    //bold text
    [mutableText replaceOccurrencesOfString:@"<b>"
                                 withString:[NSString stringWithFormat:@"<span style=\"font-family:'%@'; color: #%@;\">", fontName, hexBoldColor]
                                    options:(NSCaseInsensitiveSearch) range:NSMakeRange(0, [mutableText length])];
    //non-bold text
    [mutableText replaceOccurrencesOfString:@"</b>"
                                 withString:@"</span>"
                                    options:(NSCaseInsensitiveSearch) range:NSMakeRange(0, [mutableText length])];
    
    [mutableText insertString:[NSString stringWithFormat: @"<span style=\"font-size: %lupx; font-family:'%@'; opacity:0.4; color: rgba(%@, %@, %@, %@);\">", (unsigned long)fontSize, fontName, @"255", @"255", @"255", @"0.4"] atIndex:0];
    [mutableText appendFormat:@"</span>"];
    
    NSMutableAttributedString *attrStr = [NSAttributedString sia_attributedStringWithHTML:mutableText].mutableCopy;
    NSMutableParagraphStyle *pStyle = [[NSMutableParagraphStyle alloc] init];
    [pStyle setLineBreakMode:NSLineBreakByTruncatingTail];
    [attrStr addAttribute:NSParagraphStyleAttributeName
                    value:pStyle
                    range:NSMakeRange(0, attrStr.length)];
    
    
    NSNumber *key = @([mutableText hash]);
    NSAttributedString *string = [stringCache objectForKey:key];
    if(!string)
    {
        string = [[NSAttributedString alloc] initWithData:[mutableText dataUsingEncoding:NSUTF8StringEncoding]
                                                  options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                            NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                       documentAttributes:nil
                                                    error:nil];
        if (string && key)
            [stringCache setObject:string forKey:key];
    }
    
    return string;
}

@end
