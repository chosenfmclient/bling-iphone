//
// Created by Ben on 04/12/2016.
//

#ifndef BLINGY_OCV_NDK_IMAGEUTIL_H
#define BLINGY_OCV_NDK_IMAGEUTIL_H

#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect_c.h"
#include "opencv2/objdetect/objdetect.hpp"


#include <stdlib.h>
#include <stdio.h>

using namespace cv;

typedef struct loadBgStructureTypeDef
{
    Mat& bg;
    const char* fileName;
} loadBgStructure;

class imageUtil
{
    public:

        static void* loadBgThreadRunner(void* data);

        imageUtil();

        virtual ~imageUtil();

        /**
         * draw src1 to dst and then draw src2 on top of it
         * dst and src1 can be the same mat.
         */
        void mergeBuffers(Mat src1, Mat src2, Mat dst);

        /**
         * draw src to dst - centered
         */
        void mergeBuffer(Mat src, Mat dst);

        /**
         * draw src to dst according to the provided masks - centered
         */
        void mergeBufferAccordingToBuffers(Mat src, Mat mask1, Mat mask2, Mat dst);

        /**
         * draw src to dst according to the provided masks - centered  - FASTER
         */
        void mergeBufferAccordingToBuffersII(Mat src, Mat mask1, Mat mask2, Mat dst);

        void mergeBufferAccordingToBufferII(Mat src, Mat mask1, Mat dst);

        void mergeBufferAccordingToBufferIIWithAlpha(Mat& src, Mat& mask1, Mat& dst);

        /**
         * loads the background from file
         */
        void loadBg(const char *fileName, Mat& bg);

        void loadBgThreaded(Mat& bg, const char* fileName, pthread_t& dstThread);

        /**
         * resize src to fit Width - keep aspect ratio
         */
        void resizeAccordingToWidth270(Mat& src, int width, int height);

        void resizeAccordingToWidth90(Mat& src, int width, int height);

        Mat resizeAccordingToWidth90DetectionPhaseI(Mat src, int width, int height);

        Mat resizeAccordingToWidth90PhaseII(Mat src, int width, int height);

        /**
         * resize src to fit Height - keep aspect ratio
         */
        Mat resizeAccordingToHeight(Mat src, int height);

        /**
         * crop src according to axis
         */
        void cropImage(Mat& src, Mat& dst, int left, int top, int right, int bottom);

        void cropImage(Mat& src, Mat& dst, int x, int y);
//        Mat cropImage(Mat src, int x, int y);

        void drawFace90(Mat bg, Mat mask, int faceLeft, int faceTop, int faceRight, int faceBottom, int red, int green, int blue);

        void drawFace270(Mat bg, Mat mask, int faceLeft, int faceTop, int faceRight, int faceBottom, int red, int green, int blue);

        bool turnOnFaceDetection();

        cv::Rect getFace(Mat& dss);

        cv::Rect getFace270(Mat& dss);

        void initDss(Mat& dss);

        void fadeEdges(Mat& mask);

        void fadeEdgesVertically(Mat& mask);

        void saveWhitePoint(Mat& dss);

        void setBrightnessII(Mat& dss, int db);

        void setImageWidthHeight(int newWidth, int newHeight, float iScaleFactor);

        void setImageWidthHeight(int cWidth, int cHeight);

        /**
         * background noise cleaning
         */
        void noiseCleaning(Mat& mask, int minimumWidth);

        /**
         * background noise cleaning
         * and object holes filling
         */
        void noiseCleaningII(Mat& mask, int minimumWidth);

        /**
         * background noise cleaning
         * and object holes filling
         * and adding chromaKeys
         *
         */
        void noiseCleaningIII(Mat& mDss, Mat& mask, int minimumWidth);

        void noiseCleaninig(Mat& mask, cv::Rect face);

        void keepHeadFull(Mat& dss, Mat& mask, cv::Rect face);
    
        Mat reduceWhitePoint(Mat& dss, cv::Rect face);

        /**
         * rotate dss clockwise according to the angle into dss
         * dss and dst must NOT point to the same object
         */
        void rotateVideo(Mat& dss, Mat& dst, int angle);

        void copyMats(Mat& src, Mat& dst);

        void drawRec(Mat& dss, int left, int top, int right, int bottom, int color);

        Mat getCanny(Mat& dss);

    private:

    int findPixelAverage(const Mat src);
        int findWhitePoint(const Mat src);

        const static int lowThreshold = 20, max_lowThreshold = 60;
        const static int kernel_size = 3;
    cv::Size defBlurSize;

        CascadeClassifier faceCascade;
//        DetectionBasedTracker dd;
        std::vector<cv::Rect> faces;

        Mat lastMat;
        bool tempMatInitialize;

        bool hasEdges(Mat& cannyMask, int left, int top, int right, int bottom);

};


#endif //BLINGY_OCV_NDK_IMAGEUTIL_H
