//
//  ContactsHandler.swift
//  BlingIphoneApp
//
//  Created by Zach on 23/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
import Contacts
import RestKit

@objc class ContactsHandler: NSObject, UITableViewDelegate {
    let contactStore = CNContactStore()
    let keysToFetch = [
        CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
        CNContactEmailAddressesKey,
        CNContactPhoneNumbersKey,
        CNContactImageDataAvailableKey,
        CNContactThumbnailImageDataKey
    ] as [Any]
    
    var contacts: [DeviceContact] = []
    var emailContacts: [DeviceContact] = []
    var phoneContacts: [DeviceContact] = []
    var friendsOnBling: [SIAUser] = []
    var inviting = false
    var dataWasLoaded = false
    var loadingData = false
    var shouldFilterExistingUsers = true
    
    var completions = [(phoneContacts: [DeviceContact], emailContacts: [DeviceContact], friendsOnBling: [SIAUser]) -> Void]()
    
    public func loadContacts(with completion: @escaping (_ phoneContacts: [DeviceContact], _ emailContacts: [DeviceContact], _ friendsOnBling: [SIAUser]) -> Void) {
        
        completions.append(completion)
        
        guard !loadingData else {
            return
        }
        loadingData = true
        
        guard !dataWasLoaded else {
            callLoadCompletions()
            return
        }
        
        
        let fetchRequest = CNContactFetchRequest( keysToFetch: keysToFetch as! [CNKeyDescriptor])
        CNContact.localizedString(forKey: CNLabelPhoneNumberiPhone)
        
        if #available(iOS 10.0, *) {
            fetchRequest.mutableObjects = false
        }
        
        fetchRequest.unifyResults = true
        fetchRequest.sortOrder = .userDefault
        
        var emails: [String] = []
        do {
            try contactStore.enumerateContacts(with: fetchRequest) { (contact, stop) -> Void in
                if (!contact.phoneNumbers.isEmpty || !contact.emailAddresses.isEmpty) {
                    self.contacts.append(DeviceContact(
                        email: contact.emailAddresses.count > 0 ? String(contact.emailAddresses[0].value) : nil,
                        phoneNumber: contact.phoneNumbers.count > 0 ? contact.phoneNumbers[0].value.stringValue : nil,
                        name: contact.givenName + " " + contact.familyName,
                        image: contact.imageDataAvailable && contact.thumbnailImageData != nil ? UIImage(data: contact.thumbnailImageData!) : nil))
                }
                if (!contact.emailAddresses.isEmpty) {
                    emails.append(String(contact.emailAddresses[0].value))
                }
            }
        } catch let e as NSError {
            callLoadCompletions()
            loadingData = false
            print(e.localizedDescription)
        }
        
        //filter contacts so that it is unique
        var hashes = [String]()
        contacts = contacts.filter { (contact) -> Bool in
            defer { hashes.append(contact.cameoIdentity()) }
            return !hashes.contains(contact.cameoIdentity())
        }
        
        guard emails.count > 0 else {
            callLoadCompletions()
            dataWasLoaded = true
            return
        }
        
        if shouldFilterExistingUsers {
            let emailChunks = emails.chunk(500)
            findFriendsOnBlingy(emailChunks, index: 0)
        }
        else {
            phoneContacts = contacts
            callLoadCompletions()
            dataWasLoaded = true
        }
    }
    
    fileprivate func findFriendsOnBlingy(_ emailChunks: [[String]], index: Int, emailsToRemove: [String] = []) {
        if index == 0 {
            friendsOnBling = []
        }
        RequestManager.postRequest(
            "api/v1/find-friends",
            object: ContactList(emails: emailChunks[index]),
            queryParameters: nil,
            success: {[weak self] (request: RKObjectRequestOperation?, result: RKMappingResult?) in
                guard let sSelf = self else { return }
                guard let foundFriends = result?.firstObject as? FindFriends else { return }
                sSelf.friendsOnBling.append(contentsOf: (foundFriends.userData as NSArray as! [SIAUser]))
                
                if index >= emailChunks.count - 1 {
                    sSelf.filterOutContactsOnBling(by: emailsToRemove + foundFriends.emails_to_remove)
                    sSelf.callLoadCompletions()
                    sSelf.dataWasLoaded = true
                } else {
                    sSelf.findFriendsOnBlingy(
                        emailChunks,
                        index: index + 1,
                        emailsToRemove: emailsToRemove + foundFriends.emails_to_remove
                    )
                }
            },
            failure: {[weak self](request: RKObjectRequestOperation?, error: Error?) in
                print("failed to find friends")
                guard let sSelf = self else { return }
                
                if index >= emailChunks.count - 1 {
                    sSelf.callLoadCompletions()
                } else {
                    sSelf.findFriendsOnBlingy(
                        emailChunks,
                        index: index + 1,
                        emailsToRemove: emailsToRemove
                    )
                }
            }
        )
    }
    
    fileprivate func callLoadCompletions() {
        for complete in completions {
            complete(self.phoneContacts, self.emailContacts, self.friendsOnBling)
        }
        completions = []
        loadingData = false
    }
    
    fileprivate func filterOutContactsOnBling(by emails: [String]?) {
        guard let emailsToRemove = emails else {
            return
        }
        for contact in contacts {
            if let email = contact.email
            {
                guard !emailsToRemove.contains(email) else { continue }
                
                if let _ = contact.phoneNumber // if has phone use phone
                {
                    phoneContacts.append(contact)
                } else {
                    emailContacts.append(contact)
                }
            }
            else
            {
                phoneContacts.append(contact)
            }
        }
    }
    
    func inviteAll(completed: @escaping (_ phoneContacts: [DeviceContact], _ emailContacts: [DeviceContact]) -> Void) {
        guard !inviting else {
            return
        }
        inviting = true
        inviteAllByPhone { (success: Bool) in
            self.inviteAllByEmail { (success: Bool) in
                completed(self.phoneContacts, self.emailContacts)
                self.inviting = false
            }
        }
    }
    
    func inviteAllByEmail(completed: @escaping (_ success: Bool) -> Void) {
        var emailInvites = [Invitation]()
        for email in emailContacts {
            guard !email.wasInvited else { continue }
            let invite = Invitation(inviteName: email.name!, inviteEmail: email.email, invitePhone: nil)
            emailInvites.append(invite)
        }
        
        RequestManager.postRequest(
            "api/v1/email-invite",
            object: Invitations(emailList: emailInvites),
            queryParameters: nil,
            success: { (request: RKObjectRequestOperation?, result: RKMappingResult?) in
                for email in self.emailContacts {
                    email.wasInvited = true
                }
                completed(true)
            },
            failure: { (request: RKObjectRequestOperation?, error: Error?) in
                completed(false)
            }
        )

    }
    
    func inviteAllByPhone(completed: @escaping (_ success: Bool) -> Void) {
        var phoneInvites = [Invitation]()
        for phone in phoneContacts {
            guard !phone.wasInvited else { continue }
            let invite = Invitation(inviteName: phone.name!, inviteEmail: nil, invitePhone: phone.phoneNumber)
            phoneInvites.append(invite)
        }
        
        RequestManager.postRequest(
            "api/v1/sms-invite",
            object: Invitations(phoneList: phoneInvites),
            queryParameters: nil,
            success: { [weak self] (request: RKObjectRequestOperation?, result: RKMappingResult?) in
                guard let sSelf = self else { return }
                for phone in sSelf.phoneContacts {
                    phone.wasInvited = true
                }
                completed(true)
            },
            failure: { (request: RKObjectRequestOperation?, error: Error?) in
                completed(false)
            }
        )

    }
}
