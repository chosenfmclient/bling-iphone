//
//  UserRequestModel.swift
//  BlingIphoneApp
//
//  Created by Zach on 20/10/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class UserRequestModel: NSObject {
    var first_name: String
    var last_name: String?
    var facebook_handle: String?
    var youtube_handle: String?
    var instagram_handle: String?
    var twitter_handle: String?
    
    init(firstName: String, lastName: String?, facebookHandle: String?, youtubeHandle: String?, instagramHandle: String?, twitterHandle: String?) {
        first_name = firstName
        last_name = lastName
        facebook_handle = facebookHandle
        youtube_handle = youtubeHandle
        instagram_handle = instagramHandle
        twitter_handle = twitterHandle
    }
    
    static func objectMapping() -> RKObjectMapping {
        let mapping = RKObjectMapping.request()
        mapping?.addAttributeMappings(
            from: ["first_name",
                "last_name",
                "youtube_handle",
                "facebook_handle",
                "twitter_handle",
                "instagram_handle"]
        )
        return mapping!
    }
}
