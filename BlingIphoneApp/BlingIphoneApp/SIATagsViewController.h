//
//  SIATagsViewController.h
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 2/10/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIATagsDataModel.h"
#import "SIAVideoCategoriesDataModel.h"
#import "SIABasePostRecordViewController.h"
#import "SIAPostRecordViewController.h"
typedef NS_ENUM(NSInteger, SIATagsViewType) {
    SIATagsViewTypePreRecord = 1132,
    SIATagsViewTypePostRecord
};

typedef NS_ENUM(NSInteger, SIAStaticTagField) {
    SIAStaticTagFieldCategory = 0,
    SIAStaticTagFieldTitle,
    SIAStaticTagFieldArtist
};

typedef NS_OPTIONS(NSUInteger, SIAStaticTagFields) {
    SIACategoryField = (1 << 0),
    SIATitleField = (1 << 1),
    SIAArtistField = (1 << 2)
};

@interface SIATagsViewController : SIABasePostRecordViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic)         SIATagsViewType tagsViewType;
@property (strong, nonatomic) SIARecordingItem *recordingItem;
@property (strong, nonatomic) NSMutableArray <NSString *> *tagsArray;
@property (strong, nonatomic) SIAVideoCategoriesDataModel *categoriesModel;
@property (weak, nonatomic) IBOutlet UITableView *tagsTable;
@property (weak, nonatomic) IBOutlet UITableView *categoriesTable;
@property (weak, nonatomic) IBOutlet UIButton *addTagButton;
@property (strong, nonatomic) NSString *originalArtist;
@property (strong, nonatomic) NSString *videoTitle;

@property (weak, nonatomic) SIAPostRecordViewController *previewParent;



@end
