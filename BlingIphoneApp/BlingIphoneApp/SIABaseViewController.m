//
//  SIABaseViewController.m
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 18/06/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import "SIABaseViewController.h"
#import "BIALoginManager.h"

@interface SIABaseViewController ()
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *containerHeightConstraint;

@end

@implementation SIABaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (IPAD) {
        
        self.containerHeightConstraint.constant -= 88;
    }
    
    
}
//-(void)viewDidAppear:(BOOL)animated{
//    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
//    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
//}
- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    //    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [(BIANavigationController *)self.navigationController setNavigationBarLeftButtonBack];
    [(BIANavigationController *)self.navigationController clearNavigationBarImage];
}


- (BIANavigationController *)navigationController{
    return (BIANavigationController *)[super navigationController];
}

- (void)fadeOutView:(UIView *)view {
    [self fadeView:view
             alpha:0];
}

- (void)dimView:(UIView *)view {
    [self fadeView:view
             alpha:0.3];
}

- (void)fadeInView:(UIView *)view {
    [self fadeView:view
             alpha:1];
}

- (void)fadeView:(UIView *)view alpha:(CGFloat)alpha {
    [Utils dispatchOnMainThread:^{
        [UIView animateWithDuration:0.15
                         animations:^{
                             view.alpha = alpha;
                         }];
    }];
}

- (void)fadeOutViews:(NSArray <UIView *> *)views {
    [self fadeViews:views
              alpha:0];
}

- (void)fadeInViews:(NSArray <UIView *> *)views {
    [self fadeViews:views
              alpha:1];
}

- (void)fadeViews:(NSArray <UIView *> *)views alpha:(CGFloat)alpha {
    [Utils dispatchOnMainThread:^{
        [UIView animateWithDuration:0.15
                         animations:^{
                             for (UIView *view in views)
                             {
                                 view.alpha = alpha;
                             }
                         }];
    }];
}


- (BOOL)shouldAutorotate{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)segueWasCancelled {
    
}


@end
