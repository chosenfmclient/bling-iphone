//
//  SIABaseViewController.h
//  ChosenIphoneApp
//
//  Created by Sephi Nahmias on 18/06/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIANavigationController.h"

@interface SIABaseViewController : UIViewController

@property(nonatomic,readonly,retain) BIANavigationController *navigationController;

- (void)fadeInView:(UIView *_Nullable)view;
- (void)dimView:(UIView *_Nullable)view;
- (void)fadeOutView:(UIView * _Nullable)view;
- (void)fadeView:(UIView * _Nullable)view alpha:(CGFloat)alpha;
- (void)fadeOutViews:(NSArray <UIView *> * _Nullable)views;
- (void)fadeInViews:(NSArray <UIView *> * _Nullable)views;
- (void)fadeViews:(NSArray <UIView *> * _Nullable)views alpha:(CGFloat)alpha;
- (void)segueWasCancelled;

@end
