//
//  DeviceContact.swift
//  BlingIphoneApp
//
//  Created by Zach on 22/09/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import Foundation
import RestKit

@objc class DeviceContact: NSObject {
    var phoneNumber: String?
    var email: String?
    var name: String?
    var image: UIImage?
    var wasInvited = false
    var inviting = false
    
    override init () {
        super.init()
    }
    
    init(email: String?, phoneNumber: String?, name: String?, image: UIImage?) {
        self.email = email
        self.phoneNumber = phoneNumber
        self.name = name
        self.image = image
    }
    
    func cameoIdentity() -> String {
        return String(format: "%@%@", email != nil ? email! : "", phoneNumber != nil ? phoneNumber! : "").md5()
    }
    
    static func objectMapping() -> RKMapping {
        let mapping = RKObjectMapping(for: DeviceContact.self)
        
        mapping?.addAttributeMappings(from: [
            "email" : "email",
            "phone" : "phoneNumber",
            "has_invited" : "wasInvited"
            ])
        
        return mapping!
    }
    
    func invite(completed: @escaping () -> Void) {
        guard !wasInvited && !inviting  && name != nil else {
            return
        }
        inviting = true
        var api = "api/v1/email-invite"
        var invite = Invitations(emailList: [Invitation(inviteName: name!, inviteEmail: email, invitePhone: phoneNumber)])
        
        if let _ = phoneNumber {
            api = "api/v1/sms-invite"
            invite = Invitations(phoneList: [Invitation(inviteName: name!, inviteEmail: email, invitePhone: phoneNumber)])
        }
        
        RequestManager.postRequest(
            api,
            object: invite,
            queryParameters: nil,
            success: {[weak self] (request: RKObjectRequestOperation?, result: RKMappingResult?) in
                self?.wasInvited = true
                self?.inviting = false
                completed()
            },
            failure: {[weak self] (request: RKObjectRequestOperation?, error: Error?) in
                self?.inviting = false
            }
        )
    }
}
