//
//  SIAAlertController.m
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/7/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import "SIAAlertController.h"
#import "SIAStyling.h"

CGRect SIARectCenteredInRect(CGRect a, CGRect b)
{
    CGRect r;
    r.size = a.size;
    r.origin.x = b.origin.x + (b.size.width - a.size.width) * 0.5;
    r.origin.y = b.origin.y + (b.size.height - a.size.height) * 0.5;
    return r;
}

@interface SIAAlertAnimator : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic, getter=isPresenting) BOOL presenting;
@end

@interface SIAAlertController ( ) <UITableViewDataSource, UITableViewDelegate, UIViewControllerTransitioningDelegate>

@property (nonatomic, readwrite) NSMutableArray<SIAAlertAction *> *actions;

@property (nonatomic, readwrite) UIView *overlayView;
@property (nonatomic, readwrite) UIView *alertView;
@property (nonatomic, readwrite) UILabel *titleLabel;
@property (nonatomic, readwrite) UILabel *messageLabel;
@property (nonatomic, readwrite) UITableView *actionsTableView;
@property (nonatomic, readwrite) UIView *buttonContainerView;
@property (nonatomic, readwrite) UIButton *confirmationButton;
@property (nonatomic, readwrite) UIButton *cancelButton;

@property (nonatomic, readwrite) SIAAlertAction *confirmationAction;
@property (nonatomic, readwrite) SIAAlertAction *cancelAction;

@property (nonatomic, readwrite) SIAAlertAction *selectedAction;

@end

#pragma mark - SIAAlertAnimator
@implementation SIAAlertAnimator

- (instancetype)initPresenting:(BOOL)presenting
{
    self = [super init];
    if(self)
    {
        self.presenting = presenting;
    }

    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return self.isPresenting? 0.25 : 0.25;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    if(self.isPresenting)
        [self presentAnimateTransition:transitionContext];
    else
        [self dismissAnimateTransition:transitionContext];
}

- (void)presentAnimateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    SIAAlertController *alertController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *containerView = [transitionContext containerView];

    alertController.overlayView.alpha = 0.0;
    alertController.alertView.center = alertController.view.center;
    alertController.alertView.transform = CGAffineTransformMakeScale(0.5, 0.5);

    [containerView addSubview:alertController.view];

    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        alertController.overlayView.alpha = 1.0;
        alertController.alertView.alpha = 1.0;
        alertController.alertView.transform = CGAffineTransformMakeScale(1.05, 1.05);
    }
        completion:^(BOOL finished) {
            [UIView animateWithDuration:0.2 animations:^{
                alertController.alertView.transform = CGAffineTransformIdentity;
            }
                completion:^(BOOL finished) {
                    if(finished)
                        [transitionContext completeTransition:YES];
                }];
        }];
}

- (void)dismissAnimateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{

    SIAAlertController *alertController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        alertController.overlayView.alpha = 0.0;
        alertController.alertView.alpha = 0.0;
        alertController.alertView.transform = CGAffineTransformMakeScale(0.9, 0.9);
    }
        completion:^(BOOL finished) {
            if(finished)
                [transitionContext completeTransition:YES];
        }];
}

@end

#pragma mark - SIAAlertAction

@interface SIAAlertAction ( )
@property (nonatomic, copy) void (^actionHandler)(SIAAlertAction *action);
@property (nonatomic, readwrite) NSString *title;
@property (nonatomic, readwrite) SIAAlertActionStyle style;
@end

@implementation SIAAlertAction

+ (instancetype)actionWithTitle:(NSString *)title style:(SIAAlertActionStyle)style handler:(void (^)(SIAAlertAction *))handler
{
    SIAAlertAction *action = [self new];
    action.title = title;
    action.actionHandler = handler;
    action.style = style;

    return action;
}

@end

@implementation SIAAlertController
{
    NSMutableArray<SIAAlertAction *> *_actions;
}

@synthesize actions = _actions;

#pragma mark - SIAAlertController (Init/Alerts)
+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message
{
    SIAAlertController *alert = [[self alloc] init];

    alert.title = title;
    alert.message = message;

    return alert;
}

- (void)addAction:(SIAAlertAction *)action
{
    if(action.style == SIAAlertActionStyleConfirmation)
    {
        self.confirmationAction = action;
    }
    else if(action.style == SIAAlertActionStyleCancel)
    {
        self.cancelAction = action;
    }
    else
    {
        [_actions addObject:action];

        if(self.isViewLoaded)
        {
            [self.actionsTableView reloadData];
            [self layoutSubviews];
        }
    }
}

- (NSArray *)_actionsWithStyle:(SIAAlertActionStyle)style
{
    return nil;
}

#pragma mark - UIViewController

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        self.transitioningDelegate = self;

        _actions = [[NSMutableArray alloc] init];
        _actionSize = CGSizeMake(250, 52);
    }
    return self;
}

- (void)viewDidLoad
{
    self.overlayView = [[UIView alloc] initWithFrame:self.view.frame];
    self.overlayView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [self.view addSubview:self.overlayView];

    self.alertView = [[UIView alloc] initWithFrame:CGRectZero];
    self.alertView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.9];
    [self.view addSubview:self.alertView];

    self.actionsTableView = [[UITableView alloc] initWithFrame:CGRectZero];
    self.actionsTableView.dataSource = self;
    self.actionsTableView.delegate = self;
    self.actionsTableView.backgroundColor = [UIColor clearColor];
    self.actionsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.actionsTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"SIAAlertActionCell"];
    [self.alertView addSubview:self.actionsTableView];

    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel.textColor = [self titleLabelColor];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = kAppFontMedium(16.0);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.text = self.title;
    [self.alertView addSubview:self.titleLabel];

    self.messageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.messageLabel.textColor = [self messageLabelColor];
    self.messageLabel.backgroundColor = [UIColor clearColor];
    self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.messageLabel.numberOfLines = 0;
    self.messageLabel.font = kAppFontRegular(12.0);
    self.messageLabel.textAlignment = NSTextAlignmentCenter;
    self.messageLabel.text = self.message;
    [self.alertView addSubview:self.messageLabel];

    if([self _hasButtons])
    {
        self.buttonContainerView = [[UIView alloc] initWithFrame:CGRectZero];
        self.buttonContainerView.backgroundColor = [UIColor clearColor];

        if(self.confirmationAction)
        {
            self.confirmationButton = [self _buttonWithConfirmationCancelStyleTitle:self.confirmationAction.title];
            [self.confirmationButton addTarget:self action:@selector(_performConfirmationButtonAction) forControlEvents:UIControlEventTouchUpInside];
            
            if([_actions count] > 0)
            {
                [self setConfirmationButtonEnabled:NO];
            }

            [self.buttonContainerView addSubview:self.confirmationButton];
        }

        if(self.cancelAction)
        {
            self.cancelButton = [self _buttonWithConfirmationCancelStyleTitle:self.cancelAction.title];
            [self.cancelButton addTarget:self action:@selector(_performCancelButtonAction) forControlEvents:UIControlEventTouchUpInside];
            [self.buttonContainerView addSubview:self.cancelButton];
        }

        [self.alertView addSubview:self.buttonContainerView];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(_orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];

    [self layoutSubviews];
}

- (void)viewDidAppear:(BOOL)animated
{
    self.actionsTableView.bounces = false;
    self.actionsTableView.scrollEnabled = false;
}

#pragma mark - UITableViewDelegate/UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_actions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SIAAlertActionCell"];
    UIView *backColorView = [[UIView alloc] init];
    backColorView.backgroundColor = [self actionHighlightColor];

    cell.textLabel.numberOfLines = 1;
    cell.textLabel.textColor = [self actionLabelColor];
    cell.textLabel.font = kAppFontRegular(12.0);
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = backColorView;

    cell.textLabel.text = self.actions[indexPath.row].title;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self actionSize].height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SIAAlertAction *action = _actions[indexPath.row];

    if([self _hasButtons])
    {
        self.selectedAction = action;
        [self setConfirmationButtonEnabled:YES];
    }
    else
    {
        if(action.actionHandler)
            action.actionHandler(action);

        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

#pragma mark - SIAAlertController (Misc.)
- (UIButton *)_buttonWithConfirmationCancelStyleTitle:(NSString *)title
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button.layer setCornerRadius:13.0];
    [button setBackgroundColor:[self actionHighlightColor]];
    [button setTitleColor:[self actionLabelColor] forState:(UIControlStateNormal)];
    [button setTitle:title forState:(UIControlStateNormal)];

    return button;
}

- (void)_performConfirmationButtonAction
{
    SIAAlertAction *action = self.selectedAction ?: self.confirmationAction;
    if(self.confirmationAction.actionHandler)
        self.confirmationAction.actionHandler(action);
    else if(action.actionHandler)
        action.actionHandler(action);
}

- (void)_performCancelButtonAction
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 if(self.cancelAction.actionHandler)
                                     self.cancelAction.actionHandler(self.cancelAction);
                             }];
}

- (BOOL)_hasButtons
{
    return self.confirmationAction || self.cancelAction;
}

- (void)_orientationChanged:(NSNotification*)notif
{
    [self layoutSubviews];
}

- (void)setActionSize:(CGSize)actionSize
{
    _actionSize = actionSize;

    if(self.isViewLoaded)
        [self layoutSubviews];
}

- (void)setConfirmationButtonEnabled:(BOOL)enabled
{
    [self.confirmationButton setEnabled:enabled];
    [self.confirmationButton setAlpha:enabled? 1.0 : 0.6];
}

- (NSUInteger)currentlyVisibleActionsCount
{
    NSUInteger numOfActions = MIN([_actions count], [self maximumVisibleActions])?:[_actions count];
    return numOfActions;
}

- (UIColor *)actionHighlightColor
{
    return [[SIAStyling defaultStyle] blingMainColor];
}

- (UIColor *)actionLabelColor
{
    return [UIColor whiteColor];
}

- (UIColor *)titleLabelColor
{
    return [UIColor whiteColor];
}

- (UIColor *)messageLabelColor
{
    return [UIColor whiteColor];
}

#pragma mark - SIAAlertController (Rects)
- (void)layoutSubviews
{
    self.overlayView.frame = self.view.frame;
    self.alertView.frame = [self alertRectInBounds:self.view.frame];
    self.titleLabel.frame = [self titleRectInBounds:self.alertView.frame];
    self.messageLabel.frame = [self messageRectInBounds:self.alertView.frame];
    self.actionsTableView.frame = [self actionsRectInBounds:self.alertView.frame];
    self.buttonContainerView.frame = [self buttonContainerRectInBounds:self.alertView.frame];
    self.cancelButton.frame = [self cancelRectInBounds:self.buttonContainerView.frame];
    self.confirmationButton.frame = [self confirmationRectInBounds:self.buttonContainerView.frame];
}

- (CGRect)alertRectInBounds:(CGRect)bounds
{
    // size of the alert = width of one action, height of all actions + title and buttons + message height
    CGSize actionSize = [self actionSize];
    NSUInteger heightFactor = [self currentlyVisibleActionsCount] + 1;
    
    if([self _hasButtons])
        heightFactor++;

    CGRect rect = CGRectMake(0, 0, actionSize.width, actionSize.height * heightFactor + [self messageRectInBounds:bounds].size.height);
    rect = SIARectCenteredInRect(rect, bounds);

    return rect;
}

- (CGRect)titleRectInBounds:(CGRect)bounds
{
    // size of title = size of one action.
    CGSize actionSize = [self actionSize];

    return CGRectMake(15, 0, actionSize.width - 30, actionSize.height);
}

- (CGRect)messageRectInBounds:(CGRect)bounds
{
    if([self.message length] == 0)
    {
        return CGRectZero;
    }
    
    CGRect titleRect = [self titleRectInBounds:bounds];
    CGSize sizeConstraint = CGSizeMake(self.actionSize.width-30, floor(([self maximumVisibleActions]-2) * self.actionSize.height));
    
    NSDictionary *attributesDictionary = @{NSFontAttributeName:self.messageLabel.font};
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:self.message attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin context:nil];

    return CGRectMake(titleRect.origin.x, titleRect.size.height, titleRect.size.width, requiredHeight.size.height+10);
}

- (CGRect)actionsRectInBounds:(CGRect)bounds
{
    CGRect rect = CGRectZero;
    
    if([_actions count] > 0)
    {
        // size of actions list = width on an action, height of currentlyVisibleActionsCount
        CGSize actionSize = [self actionSize];
        NSUInteger heightFactor = [self currentlyVisibleActionsCount];
        
        rect = CGRectMake(0, actionSize.height + [self messageRectInBounds:bounds].size.height, actionSize.width, actionSize.height * heightFactor);
    }

    return rect;
}

- (CGRect)buttonContainerRectInBounds:(CGRect)bounds
{
    // size of buttons container = size of an action, origin changes if we don't have an action list.
    CGRect rect;
    CGSize actionSize = [self actionSize];

    CGRect actionsRect = [self actionsRectInBounds:bounds];
    rect = CGRectMake(0, actionsRect.size.height + actionSize.height + [self messageRectInBounds:bounds].size.height, actionSize.width, actionSize.height);

    return rect;
}

- (CGRect)confirmationRectInBounds:(CGRect)bounds
{
    CGSize actionSize = [self actionSize];
    CGRect rect;
    
    if(self.cancelAction)
        rect = SIARectCenteredInRect(CGRectMake(actionSize.width/2, 0, actionSize.width/2, 25), CGRectMake(actionSize.width/2, 0, actionSize.width/2, 52));
    else
        rect = SIARectCenteredInRect(CGRectMake(actionSize.width/2, 0, actionSize.width/2, 25),
            CGRectMake(0, 0,
                                         [self buttonContainerRectInBounds:CGRectZero].size.width,
                                         [self buttonContainerRectInBounds:CGRectZero].size.height));

    return CGRectInset(rect, 10, 0);
}

- (CGRect)cancelRectInBounds:(CGRect)bounds
{
    CGSize actionSize = [self actionSize];
    CGRect rect;
    
    if(self.confirmationAction)
        rect = SIARectCenteredInRect(CGRectMake(0, 0, actionSize.width/2, 25), CGRectMake(0, 0, actionSize.width/2, 52));
    else
        rect = SIARectCenteredInRect(CGRectMake(actionSize.width/2, 0, actionSize.width/2, 25),
                                     CGRectMake(0, 0,
                                                [self buttonContainerRectInBounds:CGRectZero].size.width,
                                                [self buttonContainerRectInBounds:CGRectZero].size.height));
    
    return CGRectInset(rect, 10, 0);
}

#pragma mark - Transitioning
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    [self layoutSubviews];
    return [[SIAAlertAnimator alloc] initPresenting:YES];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    return [[SIAAlertAnimator alloc] initPresenting:NO];
}

+ (void)presentMicrophonePermissionsError
{
    [Utils dispatchOnMainThread: ^{
        SIAAlertController *alert = [SIAAlertController alertControllerWithTitle:@"Permissions error" message:@"There was an error accessing your Microphone. Please go to your iOS privacy tab under settings and accept permissions for Blingy app to use the Microphone."];
        alert.actionSize = CGSizeMake(220, 52);
        [alert addAction:[SIAAlertAction actionWithTitle:@"OK" style:SIAAlertActionStyleCancel handler:nil]];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    }];
}

+ (void)presentCameraPermissionsError
{
    [Utils dispatchOnMainThread: ^{
        SIAAlertController *alert = [SIAAlertController alertControllerWithTitle:@"Permissions error" message:@"There was an error accessing your Camera. Please go to your iOS privacy tab under settings and accept permissions for Blingy app to use the Camera."];
        alert.actionSize = CGSizeMake(220, 52);
        [alert addAction:[SIAAlertAction actionWithTitle:@"OK" style:SIAAlertActionStyleCancel handler:nil]];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    }];
}

@end
