//
// Created by Hezi Cohen on 1/19/16.
// Copyright (c) 2016 SingrFM. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SIAInfoCarryingTapGestureRecognizer : UITapGestureRecognizer
@property(nonatomic, strong) NSDictionary *userInfo;
@end