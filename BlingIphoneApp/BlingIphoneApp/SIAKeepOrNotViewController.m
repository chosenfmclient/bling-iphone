//
//  SIAKeepOrNotViewController.m
//  
//
//  Created by Michael Biehl on 2/23/16.
//
//

#import "SIAKeepOrNotViewController.h"

@interface SIAKeepOrNotViewController ()

@end

@implementation SIAKeepOrNotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.keepItButton.layer.cornerRadius = self.giveItAnotherGoButton.layer.cornerRadius = self.keepItButton.bounds.size.height / 2;
}

- (void)viewWillAppear:(BOOL)animated {
}

- (void)viewDidAppear:(BOOL)animated {
   //[Flurry logEvent:FLURRY_VIEW_POSTRECORD_KEEPORNOT];
}

- (IBAction)keepItButtonWasTapped:(id)sender {
    //[Flurry logEvent:FLURRY_TAP_KEEPIT_POSTRECORD_KEEPORNOT];
    [self.delegate next];
}

- (IBAction)giveAnotherGoButtonWasTapped:(id)sender {
    //[Flurry logEvent:FLURRY_TAP_GIVEANOTHERGO_POSTRECORD_KEEPORNOT];
    [self.delegate back];
}

@end
