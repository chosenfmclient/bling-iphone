//
//  SIAAlertController.h
//  ChosenIphoneApp
//
//  Created by Hezi Cohen on 1/7/16.
//  Copyright © 2016 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SIAAlertActionStyle) {
    SIAAlertActionStyleDefault = 0,
    SIAAlertActionStyleCancel,
    SIAAlertActionStyleConfirmation
};

@interface SIAAlertAction : NSObject

@property(nonatomic, readonly) NSString *title;
@property(nonatomic, readonly) SIAAlertActionStyle style;
@property(nonatomic, getter=isEnabled) BOOL enabled;

+ (instancetype)actionWithTitle:(NSString *)title style:(SIAAlertActionStyle)style handler:(void (^)(SIAAlertAction *action))handler;

@end

@interface SIAAlertController : UIViewController

@property(nonatomic, copy) NSString *message;
@property(nonatomic, readwrite) NSUInteger maximumVisibleActions;
@property(nonatomic, readwrite) CGSize actionSize;

@property(nonatomic, readonly) NSArray<SIAAlertAction *> *actions;

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message;
- (void)addAction:(SIAAlertAction *)action;

+ (void)presentMicrophonePermissionsError;
+ (void)presentCameraPermissionsError;

@end
