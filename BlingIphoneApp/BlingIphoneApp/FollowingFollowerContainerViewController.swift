//
//  FollowingFollowerContainerViewController.swift
//  BlingIphoneApp
//
//  Created by Zach on 14/11/2016.
//  Copyright © 2016 Singr FM. All rights reserved.
//

import UIKit
@objc class FollowingFollowerContainerViewController: SIABaseViewController, DiscoverSearchBarViewDelegate, UpdateFollowingDelegate {
    //MARK: Outlets
    @IBOutlet weak var followersTabButton: UIButton!
    @IBOutlet weak var followingTabButton: UIButton!
    @IBOutlet var highlightViewFollowerTabConstraint: NSLayoutConstraint!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var searchBarView: DiscoverSearchBarView!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    
    //MARK: Properties
    let nFollowingTab = 0
    let nFollowerTab = 1
    var tab = 1
    var tabViewControllers = [FollowersViewController]()
    var currentQuery = ""
    var userID: String?
    var totalFollowing: Int?
    var totalFollowers: Int?
    
    //MARK: View controller methods
    override func viewDidLoad() {
        backButton.isHidden = true
        
        //set up tab buttons
        followersTabButton.isEnabled = tab == nFollowingTab
        followingTabButton.isEnabled = tab == nFollowerTab
        if tab == nFollowingTab {
            followersTabButton.alpha = 0.4
            highlightViewFollowerTabConstraint.isActive = false
        } else {
            followingTabButton.alpha = 0.4
            highlightViewFollowerTabConstraint.isActive = true
        }
        
        navigationController?.clearNavigationItemLeftButton()
        
        //Setup the follow and invite view controllers
        
        let followingVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "followersViewController") as! FollowersViewController
        followingVC.isFollowers = false
        followingVC.totalUsers = totalFollowing
        followingVC.userID = userID
        followingVC.delegate = self
        let followersVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "followersViewController") as! FollowersViewController
        followersVC.isFollowers = true
        followersVC.userID = userID
        followersVC.delegate = self
        followersVC.totalUsers = totalFollowers
        
        tabViewControllers = [followingVC, followersVC]
        tabViewControllers[nFollowingTab] = followingVC
        tabViewControllers[nFollowerTab] = followersVC
        
        addChildViewController(followingVC)
        addChildViewController(followersVC)
        
        //insert invite into contrainer
        insertViewControllerToContainer(tabViewControllers[tab])
        
        //set seatch bar
        searchBarView.delegate = self
        searchBarView.textFieldLeftPaddingConstraint.constant = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        backButton.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    @IBAction func backButtonWasTapped(_ sender: AnyObject) {
        navigationController.popViewController(animated: true)
    }
    
    //MARK: Tab button actions
    @IBAction func followerTabButtonWasTapped(_ sender: AnyObject) {
        followingTabButton.alpha = 0.4
        followingTabButton.isEnabled = true
        followersTabButton.alpha = 1
        followersTabButton.isEnabled = false
        
        if currentQuery.isEmpty {
            tabViewControllers[nFollowerTab].searchCanceled()
        } else {
            tabViewControllers[nFollowerTab].newSearch(currentQuery)
        }
        
        changeToTab(nFollowerTab)
    }
    
    @IBAction func followingTabButtonWasTapped(_ sender: AnyObject) {
        followersTabButton.alpha = 0.4
        followersTabButton.isEnabled = true
        followingTabButton.alpha = 1
        followingTabButton.isEnabled = false
        
        if currentQuery.isEmpty {
            tabViewControllers[nFollowingTab].searchCanceled()
        } else {
            tabViewControllers[nFollowingTab].newSearch(currentQuery)
        }
        
        tabViewControllers[nFollowingTab].totalUsers = totalFollowing
        tabViewControllers[nFollowingTab].setHeaderText()
        
        changeToTab(nFollowingTab)
    }
    
    //MARK: Tab set up
    func changeToTab(_ tab: Int) {
        self.tab = tab
        let transition = CATransition()
        transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.containerView.layer.add(transition, forKey: "fade")
        removeViewControllerFromContainer()
        insertViewControllerToContainer(tabViewControllers[tab])
        
        highlightViewFollowerTabConstraint.isActive = tab == nFollowerTab
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: .curveEaseInOut,
            animations: { [weak self] in
                self?.tabView.layoutIfNeeded()
            },
            completion: nil
        )
    }
    
    func insertViewControllerToContainer(_ vc: UIViewController) {
        containerView.addSubview(vc.view)
        vc.view.frame = containerView.bounds
        vc.didMove(toParentViewController: self)
        addChildViewController(vc)
    }
    
    func removeViewControllerFromContainer() {
        for vc in childViewControllers {
            vc.removeFromParentViewController()
        }
        for subView in containerView.subviews {
            subView.removeFromSuperview()
        }
    }
    
    func userWasUnfollowed() {
        guard userID == "me" || userID == BIALoginManager.myUserId() else { return }
        guard let total = totalFollowing, total > 0 else {
            return
        }
        totalFollowing = total - 1
    }
    
    func userWasFollowed(_ user: SIAUser) {
        guard userID == "me" || userID == BIALoginManager.myUserId() else { return }
        
        addFollowedUserIfNeeded(user)
        
        guard let total = totalFollowing else {
            return
        }
        totalFollowing = total + 1
    }
    
    func addFollowedUserIfNeeded(_ user: SIAUser) {
        if tabViewControllers[nFollowingTab].usersList.count == 1 && tabViewControllers[nFollowingTab].usersList[0] == nil {
            return // list wasnt loaded yet
        }
        
        var shouldAddUser = true
        
        for alreadyFollowed in tabViewControllers[nFollowingTab].usersList {
            if alreadyFollowed?.user_id == user.user_id {
                shouldAddUser = false
                break
            }
        }
        guard shouldAddUser else { return }
        
        var addLoadingCell = false
        if tabViewControllers[nFollowingTab].usersList.count > 0 && tabViewControllers[nFollowingTab].usersList.last! == nil {
            tabViewControllers[nFollowingTab].usersList.removeLast()
            addLoadingCell = true
        }
        tabViewControllers[nFollowingTab].usersList.append(user)
        
        if addLoadingCell {
            tabViewControllers[nFollowingTab].usersList.append(nil)
        }

    }
    
    //MARK: Search
    func searchFieldWasEntered() {
        //dont care
    }
    
    func cancelWasTapped() {
        currentQuery = ""
        tabViewControllers[tab].searchCanceled()
    }
    
    func searchQuery(_ query: String) {
        guard currentQuery != query else { return }
        currentQuery = query
        guard !query.isEmpty else {
            tabViewControllers[tab].searchCanceled()
            return
        }
        tabViewControllers[tab].newSearch(query)
    }
    func searchWasCleared() {
    }
}
