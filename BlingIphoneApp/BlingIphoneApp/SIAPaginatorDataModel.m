//
//  SIATableViewDataModel.m
//  ChosenIphoneApp
//
//  Created by Michael Biehl on 6/7/15.
//  Copyright (c) 2015 SingrFM. All rights reserved.
//

#import "SIAPaginatorDataModel.h"

#define ACCESS_TOKEN @"access_token"
#define PAGE @"page"
#define TOTAL_OBJECTS @"total_objects"
#define MODEL_PAGE_SIZE @"page_size"

@implementation SIAPaginatorDataModel

- (id) init {
    self = [super init];
    if (self) {
        self.pageDict = [NSMutableDictionary dictionary];
        self.totalObjects = @(0);
    }    
    return self;
}

- (id) initWithObjectId: (NSString*)objectId {
    self = [self init];
    return self;
}

+ (RKMapping *) objectMapping {
    return [[RKMapping alloc] init];
}

- (NSString *)pagePath {
    return [NSString string];
}

+ (NSString *)pathPattern {
    return [NSString string];
}

- (NSDictionary *)requestParams {
    return nil;
}

+ (RKMapping *) responseMappingForTotalKey:(NSString *)totalKey
                             objectMapping:(RKMapping *)objectMapping
                                modelClass:(Class)modelClass {
    RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:modelClass];
    NSMutableDictionary *attributeMapping = [[NSMutableDictionary alloc] initWithDictionary:@{MODEL_PAGE_SIZE:@"pageSize"}];
    NSDictionary *totalObjectsAttrMapping;
    
    totalObjectsAttrMapping = @{totalKey:@"totalObjects"};
    
    [attributeMapping addEntriesFromDictionary:totalObjectsAttrMapping];
    [responseMapping addAttributeMappingsFromDictionary:attributeMapping];
    
    [responseMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"pageArray" withMapping:objectMapping]];

    
    return responseMapping;
}

- (void) getPageForPath:(NSString *)path
          requestParams:(NSDictionary *)additionalParams
  withCompletionHandler:(void(^)(id model))handler
           errorHandler:(void(^)(NSError *error))errorHandler{
    
    
    NSString *pageString = self.currentPage.stringValue;
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] initWithDictionary:@{PAGE:pageString}];
    if (additionalParams) {
        [requestParameters addEntriesFromDictionary:additionalParams];
    }
    
    __weak SIAPaginatorDataModel* wSelf = self;
    [RequestManager
     getRequest:path
     queryParameters:requestParameters
     success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         SIAPaginatorDataModel *results = mappingResult.firstObject;
         if (results.pageArray) {
             [wSelf.pageDict setObject:results.pageArray forKey:wSelf.currentPage.stringValue];
         }
         handler(results);
     }
     failure:^(RKObjectRequestOperation *operation, NSError *error) {
         errorHandler(error);
//         if (!operation.HTTPRequestOperation.response) {
//             [wSelf getPageForPath:path
//                    requestParams:additionalParams
//            withCompletionHandler:handler
//                     errorHandler:errorHandler];
//         }
         NSLog(@"What do you mean by 'there are no videos?': %@", error);
     }
     showFailureDialog:YES];
}

@end
