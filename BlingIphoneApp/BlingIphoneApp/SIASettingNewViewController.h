//
//  SIASettingNewViewController.h
//  ChosenIphoneApp
//
//  abstract: this is the settings page. the user can change his setting
//
//  Created by Roni Shoham on 20/02/14.
//  Copyright (c) 2014 SingrFM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIASettingNewViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *appVersionLabel;
@property (weak, nonatomic) IBOutlet UILabel *privacy;
@property (weak, nonatomic) IBOutlet UILabel *termsConditions;
@property (weak, nonatomic) IBOutlet UILabel *appVersion;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISwitch *saveRecordsAutomaticallySwitch;
@property (weak, nonatomic) IBOutlet UILabel *saveRecordsAutomaticallyLabel;

@end
