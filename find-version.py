#!/bin/python3
import re
versionKey = False

with open ('BlingIphoneApp.plist') as file:
    for line in file:
        if versionKey:
            version = line.strip()
            version = re.sub('<string>', '', version)
            version = re.sub('</string>', '', version)

            print(version)
            break
        if line.strip() == '<key>bundle-version</key>':
            versionKey = True
